<?php
/**
 * This is the template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * Stemcounter.com Theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php get_template_part('head'); ?>
</head>

<body <?php body_class(); ?>>

	<header class="header black-bg">
        <div class="sidebar-toggle-box">
        	<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation">&nbsp;</div>
        </div>
        
        <div class="site-branding">
        
        </div><!-- .site-branding -->
	</header><!-- .site-header -->

	<?php get_sidebar('test'); ?>

	<div id="content" class="site-content">
