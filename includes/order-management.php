<?php
use Illuminate\Http\Request;
use Stemcounter\Event;
use Stemcounter\Customer;
use Stemcounter\Vendor;
use Stemcounter\Flower;
use Stemcounter\Service;
use Stemcounter\Variation;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Item;
use Stemcounter\Arrangement_Photo;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;
use Stemcounter\Tax_Rate;
use Stemcounter\Invoicing_Category;
use Stemcounter\Scheduled_Payment;
use Stemcounter\Made_Payment;
use Stemcounter\Order;
use Stemcounter\Order_Item;
use Stemcounter\Measuring_Unit;

function sc_render_manage_order_events( $content ) {
	global $view;

	ob_start(); ?>
	<div class="row">
			<?php if ( $content ) : ?>
				<div class="col-lg-12 order-message">
					<?php echo $content; ?>
				</div>
			<?php endif; ?>
	</div><!-- /row -->
	
	<?php if ( empty( $_GET['oid'] ) ) :
		$orders = Order::where(array(
			'user_id' => get_current_user_id()
		))->orderBy('fulfilment_date', 'DESC')->orderBy('created_at', 'DESC')->get();

		$table_len = get_user_meta( get_current_user_id(), 'sc_orders_table_len', true ); ?>
		<div class="order-data">
			<div class="row mt">
				<div class="col-lg-12">
					<div class="content-panel">
						<section id="unseen">
							<table id="ordersTable" class="table table-striped table-advance table-hover no-footer" data-page-length="<?php echo ! empty( $table_len ) ? $table_len : 10; ?>">
								<thead>
									<tr>
										<th colspan="8">
											<a class="generate-order-button cta-link">
												+ Generate Order
											</a>
										</th>
									</tr>
									<tr>
										<th class="order-td-name">Order</th>
										<th class="hidden-xs order-td-created">Created On</th>
										<th class="hidden-xs order-td-created">Fulfilment Date</th>
										<th class="hidden-xs"></th>
										<th class="hidden-xs order-td-status">Status</th>
										<th class="hidden-xs order-td-estimate">Order Estimate</th>
										<?php /*<th class="hidden-xs order-td-email" style="width: 50px"></th> */ ?>
										<th class="hidden-xs order-td-edit" style="width: 50px"></th>
										<th class="hidden-xs order-td-delete" style="width: 50px"></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ( $orders as $order ) : 
										$order_created_date = sc_get_user_date_time( get_current_user_id(), $order->created_at->toDateTimeString() );
										$fulfilment_date = $order->fulfilment_date ? sc_get_user_date_time( get_current_user_id(), $order->fulfilment_date->toDateTimeString() ) : 'N/A';
									?>
										<tr>
											<td class="order-td-name"><a data-order-id="<?php echo $order->id; ?>" href="<?php echo esc_url( add_query_arg( 'oid', $order->id ) ); ?>"><?php echo $order->name; ?></a>
											<div class="visible-xs">
												Created: <?php echo $order_created_date; ?><br />
												Fulfilment Date: <?php echo $fulfilment_date; ?> Est: <?php echo sc_format_price($order->estimate, true); ?><br />
												<select class="change-order-status" name="order-status">
													<?php if ($order->status == 'placed'): ?>
														<option value="planning">Planning</option>
														<option value="placed" selected>Placed</option>
														<option value="finished">Finished</option>
													<?php elseif ($order->status == 'planning'): ?>
														<option value="planning" selected>Planning</option>
														<option value="placed">Placed</option>
														<option value="finished">Finished</option>
													<?php elseif ($order->status == 'finished'): ?>
														<option value="planning">Planning</option>
														<option value="placed">Placed</option>
														<option value="finished" selected>Finished</option>
													<?php endif; ?>
												</select>
												<!-- <?php echo ucfirst($order->status); ?> -->
												<?php /*<button type="button" class="btn btn-primary mayesh-order-btn email-order-button" data-order-id="<?php echo esc_attr( $order->id ); ?>">Email Order to Mayesh</button><br /> */ ?>
												<a href="#" class="edit-order-button" data-order-id="<?php echo $order->id ?>">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="#" class="delete-order-button" data-order-id="<?php echo $order->id ?>">
													<i class="fa fa-trash"></i>
												</a>
											</div>

											</td>
											<td class="hidden-xs order-td-created">
												<div class="hidden"><?php echo strtotime( $order->created_at->toDateTimeString() ); ?></div>
												<?php echo $order_created_date; ?>
											</td>
											<td class="hidden-xs order-td-fulfilment">
												<div class="hidden"><?php echo $order->fulfilment_date ? strtotime( $order->fulfilment_date->toDateTimeString() ) : '0'; ?></div>
												<?php echo $fulfilment_date; ?>
											</td>
											<td class="hidden-xs order-td-status-hidden" data-order-id="<?php echo $order->id; ?>"><?php echo ucfirst($order->status); ?></td>
											<td class="hidden-xs order-td-status">
												<select class="change-order-status" name="order-status">
													<?php if ($order->status == 'placed'): ?>
														<option value="planning">Planning</option>
														<option value="placed" selected>Placed</option>
														<option value="finished">Finished</option>
													<?php elseif ($order->status == 'planning'): ?>
														<option value="planning" selected>Planning</option>
														<option value="placed">Placed</option>
														<option value="finished">Finished</option>
													<?php elseif ($order->status == 'finished'): ?>
														<option value="planning">Planning</option>
														<option value="placed">Placed</option>
														<option value="finished" selected>Finished</option>
													<?php endif; ?>
												</select>
											</td>
											<td class="hidden-xs order-td-estimate"><?php echo sc_format_price($order->estimate, true); ?></td>
											<?php /*<td class="hidden-xs order-td-mail" style="text-align: right;">
												<button type="button" class="btn btn-primary mayesh-order-btn email-order-button" data-order-id="<?php echo esc_attr( $order->id ); ?>">Email Order to Mayesh</button>
											</td> */?>
											<td class="hidden-xs order-td-edit" style="text-align: right;">
												<!-- <button type="button" class="btn btn-primary btn-xs edit-order-button" data-order-id="<?php echo $order->id ?>"><i class="fa fa-pencil"></i></button> -->
												<a href="#" class="edit-order-button" data-order-id="<?php echo $order->id ?>">
													<i class="fa fa-pencil"></i>
												</a>
											</td>
											<td class="hidden-xs order-td-delete" style="text-align: right;">
												<!-- <button type="button" class="btn btn-danger btn-xs delete-order-button" data-order-id="<?php echo $order->id; ?>"><i class="fa fa-trash"></i></button> -->
												<a href="#" class="delete-order-button" data-order-id="<?php echo $order->id ?>">
													<i class="fa fa-trash"></i>
												</a>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody> <!--
								<tfoot>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tfoot>  -->
							</table>
						</section>
					</div><!-- /content-panel -->
				</div><!-- /col-lg-4 -->			
			</div><!-- /row -->
		</div>
	<?php else :
		try {
			global $sc_current_order;

			$sc_current_order = Order::where( array(
				'id' => absint( $_GET['oid'] ),
				'user_id' => get_current_user_id(),
			) )->with( 'items', 'items.variation', 'items.variation.item' )->firstOrFail();

			sc_render_order( $sc_current_order );

			wp_enqueue_script( 'theme-react-single-order' );
		} catch (Exception $e) {
			
		}
	endif; ?>
	<br/>
	<br/>
	<br/>
	<br/>
	<?php
	return ob_get_clean();
}
add_filter( 'sc/app/content/order', 'sc_render_manage_order_events' );

function sc_ajax_add_user_order_form() {
	$states = array( 'booked' );
	$events = Event::where(array(
		'user_id' => get_current_user_id(),
		'status'  => 'published',
	))->whereIn('state', $states, 'and')->orderBy('date', 'asc')->get();
	$events_list = array();

	foreach ($events as $event) {
		array_push($events_list, array(
			'id' => $event->id,
			'name' => $event->name,
			'date' => sc_get_user_date_time(get_current_user_id(), $event->date->toDateTimeString())
		));
	}
	?>
	<form class="form-horizontal style-form add-order-form" method="post">
		<div class="form-shell"></div>
	</form>
	<script type="text/javascript">
	
	jQuery(document).trigger('stemcounter.action.renderOrderForm', {
		node: $('.add-order-form'),
		layout: 'add-order',
		eventsList: <?php echo json_encode($events_list); ?>,
		orderDate: <?php echo json_encode(sc_get_user_date_time(get_current_user_id(), time(), true)); ?>,
		orderFulfilmentDate: '',
		dateFormat: <?php echo json_encode( sc_get_user_js_date_format() ); ?>
	});
	</script>
	<?php
	exit;
}
add_action('wp_ajax_sc_add_user_order_form', 'sc_ajax_add_user_order_form');

function sc_ajax_add_user_order() {
	$request = Request::capture();
	$r = new Ajax_Response();

	$order_name = $request->input( 'order_name' );
	$total_floral = 0;
	$total_service = 0;

	try {
		if (empty($order_name)) throw new Humanreadable_Exception('The Name field is required.');
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	}

	$order_events = $request->input('order_events');
	$order_events = array_filter($order_events, function($event_id) {
		return $event_id > 0;
	});

	try {
		if (empty($order_events)) throw new Humanreadable_Exception('You need at least one Event selected.');
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	}

	try {
		$user_id = intval(get_current_user_id());
		// get all arrangement items only for arrangements included in the proposal
		$shopping_list = array();
		$items = array();

		foreach ( $order_events as $event_id ) {
			$event = Event::where( array(
				'id' => $event_id,
			) )->with(array(
				'arrangements' => function( $query ) {
					$query->where( 'include', '=', 1 )->where( 'addon', '=', 0 )->orderBy( 'order', 'asc' );
				},
			), 'arrangements.items', 'arrangements.items.item_variation' )->firstOrFail();

			foreach ( $event->arrangements as $arrangement ) {
				foreach ( $arrangement->items as $arrangement_item ) {
					$type = sc_get_simple_item_type( $arrangement_item->item, false, $arrangement_item );
					$type_label = sc_get_item_type( $arrangement_item->item, true, $arrangement_item );
					$name = $arrangement_item->get_full_name();
					$quantity = $arrangement_item->quantity * $arrangement->quantity;
					$item_cost = $arrangement_item->cost;

					$items_su = $arrangement_item->item->purchase_qty;
					//if is deleted we need to know how much SU does that flower have
					if ( is_null( $items_su ) ) {
						try {
							$item = Item::where( array(
								'id' => $arrangement_item->item_id,
							) )->withTrashed()->firstOrFail()->toArray();
							$items_su = $item['purchase_qty'];
						} catch (Exception $e) {
						}
					}
					$su_needed = ceil( $quantity / $items_su );
					$total_cost = ( $items_su * $su_needed ) * $item_cost;
					$total_cost = $total_cost;
					$leftovers = ( $items_su * $su_needed ) - $quantity;
					$purchase_unit = $arrangement_item->item->purchase_unit;

					if ( ! isset( $shopping_list[ $event_id ][ $type ][ $name ] ) ) {
						$shopping_list[ $event_id ][ $type ][ $name ] = array(
							'id' => $arrangement_item->item_variation_id ? $arrangement_item->item_variation_id : $arrangement_item->item_id,
							'type' => $type_label,
							'name' => $name,
							'quantity' => $quantity,
							'cost' => $total_cost,
						);

						$shopping_list[ $event_id ][ $type ][ $name ]['items_su'] = $items_su;
						$shopping_list[ $event_id ][ $type ][ $name ]['su_needed'] = $su_needed;
						$shopping_list[ $event_id ][ $type ][ $name ]['leftovers'] = $leftovers;
						$shopping_list[ $event_id ][ $type ][ $name ]['purchase_unit'] = $purchase_unit;
					} else {
						$shopping_list[ $event_id ][ $type ][ $name ]['quantity'] += $quantity;

						$su_needed = ceil( $shopping_list[ $event_id ][ $type ][ $name ]['quantity'] / $items_su );
						$total_cost = ( $items_su * $su_needed ) * $item_cost;
						$total_cost = $total_cost;
						$leftovers = ( $items_su * $su_needed ) - $shopping_list[ $event_id ][ $type ][ $name ]['quantity'];

						$shopping_list[ $event_id ][ $type ][ $name ]['su_needed'] = $su_needed;
						$shopping_list[ $event_id ][ $type ][ $name ]['cost'] = $total_cost;
						$shopping_list[ $event_id ][ $type ][ $name ]['leftovers'] = $leftovers;
						$shopping_list[ $event_id ][ $type ][ $name ]['purchase_unit'] = $purchase_unit;
					}
				}
			}

			if ( array_key_exists( 'flower', $shopping_list[ $event_id ] ) && ! empty( $shopping_list[ $event_id ]['flower'] ) ) {
				ksort( $shopping_list[ $event_id ]['flower'] );
			}

			if ( array_key_exists( 'service', $shopping_list[ $event_id ] ) && ! empty( $shopping_list[ $event_id ]['service'] ) ) {
				ksort( $shopping_list[ $event_id ]['service'] );
			}
		}
			
		$calculated_shopping_list = array(
			'flower' => array(),
			'service' => array()
		);
		foreach ( $shopping_list as $event_id => $flowers_items ) {
			foreach ( $flowers_items as $type => $values ) {
				if ( ! empty( $values ) ) {
					foreach ( $values as $name => $info ) {
						if ( isset( $calculated_shopping_list['flower'][ $name ] ) ) {
							$calculated_shopping_list['flower'][ $name ] = array(
								'quantity' => $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'],
								'cost' => $calculated_shopping_list['flower'][ $name ]['cost'] + $info['cost'],
								'items_su' => $info['items_su'],
								'su_needed' => ceil( ( $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'] ) / $info['items_su'] ),
								'leftovers' => ( $info['items_su'] * ceil( ( $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'] ) / $info['items_su'] ) ) - ( $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'] ),
							);
						} else {
							$calculated_shopping_list['flower'][ $name ] = array(
								'quantity' => $info['quantity'],
								'cost' => $info['cost'],
								'items_su' => $info['items_su'],
								'su_needed' => $info['su_needed'],
								'leftovers' => $info['leftovers']
							);
						}
					}
				}
			}
		}

		foreach ( $calculated_shopping_list['flower'] as $item ) {
			$total_floral += $item['cost'];
		}
		foreach ( $calculated_shopping_list['service'] as $item ) {
			$total_service += $item['cost'];
		}

		$total_combined = $total_floral + $total_service;
	} catch ( Exception $e ) {
		$r->fail( $e->getMessage() );
	}

	try {
		$date_format = get_user_meta( $user_id, '__date_format', true );

		$new_order = new Order();
		$new_order->user_id = $user_id;
		$new_order->event_ids = json_encode( $order_events );
		$new_order->name = $order_name;
		$new_order->note = $request->input( 'order_note' );
		$new_order->status = 'planning';
		$new_order->flowers_items = '';
		$new_order->estimate = $total_combined;
		if ( $request->input( 'fulfilment_date' ) ) {
			$fulfilment_date = DateTime::createFromFormat( $date_format, $request->input( 'fulfilment_date' ) );
			if ( $fulfilment_date ) {
				$new_order->fulfilment_date = date( MYSQL_DATE_FORMAT, $fulfilment_date->getTimestamp() );
			}
		} else {
			$new_order->fulfilment_date = null;
		}
		$new_order->save();
	} catch ( Exception $e ) {
		$r->fail( $e->getMessage() );
	}

	try {
		foreach ( $shopping_list as $event_id => $item_types ) {
			foreach ( $item_types as $item_type => $_items ) {
				if ( empty( $_items ) ) {
					continue;
				}

				foreach ( $_items as $_item ) {
					$new_item = new Order_Item;
					$new_item->order_id = $new_order->id;
					$new_item->event_id = $event_id;
					$new_item->item_id = $_item['id'];
					$new_item->name = $_item['name'];
					$new_item->pricing_category = $_item['type'];
					$new_item->quantity = $_item['quantity'];
					$new_item->cost = $_item['cost'];
					$new_item->items_su = ! empty( $_item['items_su'] ) ? $_item['items_su'] : 1;
					$new_item->su_needed = ! empty( $_item['su_needed'] ) ? $_item['su_needed'] : 1;
					$new_item->leftovers = ! empty( $_item['leftovers'] ) ? $_item['leftovers'] : 0;
					$new_item->discarded = 0;

					$new_item->save();
					$new_item = null;
				}
			}
		}
	} catch ( Exception $e ) {
		$r->fail( $e->getMessage() );
	}

	$r->respond( 'Success' );
}
add_action('wp_ajax_sc_add_user_order', 'sc_ajax_add_user_order');

function sc_ajax_refresh_user_order() {
	$request = Request::capture();
	$r = new Ajax_Response();
	$save_order = filter_var( $request->input( 'save_order' ), FILTER_VALIDATE_BOOLEAN);
	$refresh_checked = empty( $request->input( 'refresh_checked' ) ) ? array() : $request->input( 'refresh_checked' );

	//Get real events in order, based on order id
	try {
		$order = Order::where(array(
			'id' => $request->input('order_id'),
			'user_id' => get_current_user_id()
		))->with('items')->firstOrFail();
		$order_events = json_decode( $order->event_ids );
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again laterr.');
	}

	//Check if we have any real events
	$order_events = array_filter( $order_events, function( $event_id ) {
		return $event_id > 0;
	});
	try {
		if ( empty( $order_events ) ) throw new Humanreadable_Exception( 'You need at least one Event listed.' );
	} catch ( Humanreadable_Exception $e ) {
		$r->fail( $e->getMessage() );
	}

	$total_floral = $total_service = $total_combined = 0;

	//Get items for the list of events
	try {
		$user_id = intval(get_current_user_id());
		// get all arrangement items only for arrangements included in the proposal
		$shopping_list = array();
		$items = array();

		foreach ( $order_events as $event_id ) {
			$event = Event::where( array(
				'id' => $event_id,
			) )->with(array(
				'arrangements' => function( $query ) {
					$query->where( 'include', '=', 1 )->where( 'addon', '=', 0 )->orderBy( 'order', 'asc' );
				},
			), 'arrangements.items', 'arrangements.items.item_variation' )->firstOrFail();

			foreach ( $event->arrangements as $arrangement ) {
				foreach ( $arrangement->items as $arrangement_item ) {
					$type = sc_get_simple_item_type( $arrangement_item->item, false, $arrangement_item );
					$type_label = sc_get_item_type( $arrangement_item->item, true, $arrangement_item );
					$name = $arrangement_item->get_full_name() . "_{$arrangement_item->item_id}";
					$quantity = $arrangement_item->quantity * $arrangement->quantity;
					$item_cost = $arrangement_item->cost;

					$items_su = $arrangement_item->item->purchase_qty;
					//if is deleted we need to know how much SU does that flower have
					if ( is_null( $items_su ) ) {
						try {
							$item = Item::where( array(
								'id' => $arrangement_item->item_id,
							) )->withTrashed()->firstOrFail()->toArray();
							$items_su = $item['purchase_qty'];
						} catch (Exception $e) {
						}
					}
					$su_needed = ceil( $quantity / $items_su );
					$total_cost = ( $items_su * $su_needed ) * $item_cost;
					$total_cost = $total_cost;
					$leftovers = ( $items_su * $su_needed ) - $quantity;

					if ( ! isset( $shopping_list[ $event_id ][ $type ][ $name ] ) ) {
						$shopping_list[ $event_id ][ $type ][ $name ] = array(
							'id' => $arrangement_item->item_variation_id ? $arrangement_item->item_variation_id : $arrangement_item->item_id,
							'type' => $type_label,
							'name' => $arrangement_item->get_full_name(),
							'quantity' => $quantity,
							'cost' => $total_cost,
						);

						$shopping_list[ $event_id ][ $type ][ $name ]['items_su'] = $items_su;
						$shopping_list[ $event_id ][ $type ][ $name ]['su_needed'] = $su_needed;
						$shopping_list[ $event_id ][ $type ][ $name ]['leftovers'] = $leftovers;
					} else {
						$shopping_list[ $event_id ][ $type ][ $name ]['quantity'] += $quantity;

						$su_needed = ceil( $shopping_list[ $event_id ][ $type ][ $name ]['quantity'] / $items_su );
						$total_cost = ( $items_su * $su_needed ) * $item_cost;
						$total_cost = $total_cost;
						$leftovers = ( $items_su * $su_needed ) - $shopping_list[ $event_id ][ $type ][ $name ]['quantity'];

						$shopping_list[ $event_id ][ $type ][ $name ]['su_needed'] = $su_needed;
						$shopping_list[ $event_id ][ $type ][ $name ]['cost'] = $total_cost;
						$shopping_list[ $event_id ][ $type ][ $name ]['leftovers'] = $leftovers;
					}
				}
			}

			if ( array_key_exists( 'flower', $shopping_list[ $event_id ] ) && ! empty( $shopping_list[ $event_id ]['flower'] ) ) {
				ksort( $shopping_list[ $event_id ]['flower'] );
			}

			if ( array_key_exists( 'service', $shopping_list[ $event_id ] ) && ! empty( $shopping_list[ $event_id ]['service'] ) ) {
				ksort( $shopping_list[ $event_id ]['service'] );
			}
		}

		/*$r->add_payload( 'result', $shopping_list );
		$r->respond(); //-----------------------------------------------------------*/
			
		$calculated_shopping_list = array(
			'flower' => array(),
			'service' => array()
		);
		foreach ( $shopping_list as $event_id => $flowers_items ) {
			foreach ( $flowers_items as $type => $values ) {
				if ( ! empty( $values ) ) {
					foreach ( $values as $name => $info ) {
						if ( isset( $calculated_shopping_list['flower'][ $name ] ) ) {
							$calculated_shopping_list['flower'][ $name ] = array(
								'quantity' => $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'],
								'cost' => $calculated_shopping_list['flower'][ $name ]['cost'] + $info['cost'],
								'items_su' => $info['items_su'],
								'su_needed' => ceil( ( $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'] ) / $info['items_su'] ),
								'leftovers' => ( $info['items_su'] * ceil( ( $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'] ) / $info['items_su'] ) ) - ( $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'] ),
							);
						} else {
							$calculated_shopping_list['flower'][ $name ] = array(
								'quantity' => $info['quantity'],
								'cost' => $info['cost'],
								'items_su' => $info['items_su'],
								'su_needed' => $info['su_needed'],
								'leftovers' => $info['leftovers']
							);
						}
					}
				}
			}
		}

		foreach ( $calculated_shopping_list['flower'] as $item ) {
			$total_floral += $item['cost'];
		}
		foreach ( $calculated_shopping_list['service'] as $item ) {
			$total_service += $item['cost'];
		}

		$total_combined = $total_floral + $total_service;

	} catch ( Exception $e ) {
		$r->fail( $e->getMessage() );
	}

	//Save discarded items information, for the refreshing process
	if ( true == $save_order ) {
		$discarded_items = array();
		foreach ( $order->items as $item) {
			if ( 1 == $item['discarded'] ) {
				$discarded_items[ $item['event_id'] . '_' . $item['item_id'] ] = true;
			}
		}

		$order->estimate = $total_combined;
		$order->save();
		//$r->add_payload( 'discarded_items', $discarded_items);
	}

	$collection = collect();
	try {

		//If we have to make changes, delete the old items, which will be updated
		if ( true == $save_order ) {
			$order->items()->each( function ( $item, $k ) use ( $refresh_checked, $collection ) {
				//Delete item with the old info
				if ( isset( $refresh_checked[$item->item_id] ) && $refresh_checked[$item->item_id] ) {
					$item->forceDelete();
				} else {
				//If not for changing and therefore was not deleted, then return it in the response
					$collection->push( $item );
				}
			} );
		}

		foreach ( $shopping_list as $event_id => $item_types ) {
			//Add item with updated info in to the list
			foreach ( $item_types as $item_type => $_items ) {
				if ( empty( $_items ) ) {
					continue;
				}
				foreach ( $_items as $_item ) {
					$new_item = new Order_Item;
					$new_item->order_id = $order->id;
					$new_item->event_id = $event_id;
					$new_item->item_id = $_item['id'];
					$new_item->name = $_item['name'];
					$new_item->pricing_category = $_item['type'];
					$new_item->quantity = $_item['quantity'];
					$new_item->cost = $_item['cost'];
					$new_item->items_su = ! empty( $_item['items_su'] ) ? $_item['items_su'] : 1;
					$new_item->su_needed = ! empty( $_item['su_needed'] ) ? $_item['su_needed'] : 1;
					$new_item->leftovers = ! empty( $_item['leftovers'] ) ? $_item['leftovers'] : 0;
					$new_item->discarded = intval( isset( $discarded_items[ "{$new_item->event_id}_{$new_item->item_id}" ]  ) );
					if ( true == $save_order ) {
						if ( isset( $refresh_checked[$new_item->item_id] ) && $refresh_checked[$new_item->item_id] ) {
							$new_item->save();
							//Add the updated item in the response (the same, which we may be was previously deleted the old info)
							$collection->push( $new_item );
						}
					} else {
						//Add the updateditem in the response for user to preview it
						$collection->push( $new_item );
					}

					$new_item = null;
				}
			}
		}
	} catch ( Exception $e ) {
		$r->fail( $e->getMessage() );
	}
	if ( true ==  $save_order ) {
		$shopping_list = sc_get_shopping_list_from_order_items( $collection );
		$r->add_payload( 'items', $shopping_list );
		$r->respond( 'Order refreshed and saved' );
	
	} else {
		$order->items = $order->items->map( function( $item ) use (&$collection) {
			$new_item_index = $collection->search( function($_item) use ($item){
				return $item->event_id == $_item->event_id && $item->item_id == $_item->item_id;
				//return $item->item_id == $_item->item_id;
			} );

			if ( false !== $new_item_index ) {
				$new_item = $collection->splice( $new_item_index, 1 )->first();
				$item->quantity_new = $new_item->quantity;
			} else {
				$item->quantity_new = 0;
			}

			return $item;
		} );
		//error_log( var_export( $collection, true ) );
		foreach ( $collection as $new_item ) {
			$new_item->extra = true;
			$new_item->quantity_new = $new_item->quantity;
			$order->items->push( $new_item );
		}

		$shopping_list = sc_get_shopping_list_from_order_items( $order->items );
		$r->add_payload( 'items', $shopping_list );
		$r->respond('Latest information retrieved');

	}

}
add_action('wp_ajax_sc_refresh_user_order', 'sc_ajax_refresh_user_order');

function sc_ajax_edit_user_order_form() {
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$states = array( 'inquiry', 'proposal_sent', 'booked', 'completed' );
		$events = Event::where(array(
			'user_id' => get_current_user_id()
		))->whereIn('state', $states, 'and')->get();
		$events_list = array();
		
		foreach ($events as $event) {
			array_push($events_list, array(
				'id' => $event->id,
				'name' => $event->name,
				'date' => sc_get_user_date_time(get_current_user_id(), $event->date->toDateTimeString())
			));
		}

		$order = Order::where(array(
			'id' => $request->input('order_id'),
			'user_id' => get_current_user_id()
		))->firstOrFail();
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}

	$fulfilment_date = $order->fulfilment_date ? sc_get_user_date_time( get_current_user_id(), $order->fulfilment_date->toDateTimeString() ) : '';

	?>
	<form class="form-horizontal style-form edit-order-form" method="post">
		<div class="form-shell"></div>
	</form>
	<script type="text/javascript">
	jQuery(document).trigger('stemcounter.action.renderOrderForm', {
		node: $('.edit-order-form'),
		layout: 'edit-order',
		eventsList: <?php echo json_encode($events_list); ?>,
		orderId: <?php echo json_encode($order->id); ?>,
		orderName: <?php echo json_encode($order->name) ?>,
		orderNote: <?php echo json_encode($order->note) ?>,
		orderDate: <?php echo json_encode(sc_get_user_date_time(get_current_user_id(), $order->created_at->toDateTimeString())); ?>,
		orderFulfilmentDate: <?php echo json_encode( $fulfilment_date ); ?>,
		dateFormat: <?php echo json_encode( sc_get_user_js_date_format() ); ?>,
		eventItems: <?php echo $order->event_ids; ?> //already json
	});
	</script>
	<?php
	exit;
}
add_action('wp_ajax_sc_edit_user_order_form', 'sc_ajax_edit_user_order_form');

function sc_ajax_edit_user_order() {
	$request = Request::capture();
	$r = new Ajax_Response();

	$order_name = $request->input('order_name');

	try {
		if (empty($order_name)) throw new Humanreadable_Exception('The Name field is required.');
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	}

	try {
		$old_order = Order::where(array(
			'id' => $request->input('order_id'),
			'user_id' => get_current_user_id()
		))->firstOrFail();

		$old_order->name = $order_name;
		$old_order->note = $request->input('order_note');

		$date_format = get_user_meta( $old_order->user_id, '__date_format', true );
		if ( $request->input( 'fulfilment_date' ) ) {
			$fulfilment_date = DateTime::createFromFormat( $date_format, $request->input( 'fulfilment_date' ) );
			if ( $fulfilment_date ) {
				$old_order->fulfilment_date = date( MYSQL_DATE_FORMAT, $fulfilment_date->getTimestamp() );
			}
		} else {
			$old_order->fulfilment_date = null;
		}

		$old_order->save();
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}

	$r->respond('Success');
}
add_action('wp_ajax_sc_edit_user_order', 'sc_ajax_edit_user_order');

function sc_ajax_change_user_order_status() {
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$order = Order::where(array(
			'id' => $request->input('order_id')
		))->firstOrFail();

		$order->status = $request->input('order_status'); 
		$order->save();
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}

	$r->respond('Success');
}
add_action('wp_ajax_sc_change_user_order_status', 'sc_ajax_change_user_order_status');

function sc_ajax_delete_user_order() {
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$delete_order = Order::where(array(
			'id' => $request->input('order_id'),
			'user_id' => get_current_user_id()
		));
		$delete_order->delete();
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again later.');
	}

	$r->respond('Success');
}
add_action('wp_ajax_sc_delete_user_order', 'sc_ajax_delete_user_order');

function sc_get_shopping_list_from_order_items( $items, $with_photos = false ) {
	$shopping_list = array(
		'included'  => array(),
		'discarded' => array(),
		'extra'     => array(),
	);

	foreach ( $items as $item ) {
		if ( ! empty( $item->extra ) ) {
			$sl_key = 'extra';
		} else {
			$sl_key = $item->discarded ? 'discarded' : 'included';
		}
		$key = "{$item->pricing_category}_{$item->name}_{$item->item_id}";

		if ( ! isset( $shopping_list[ $sl_key ][ $key ] ) ) {
			$shopping_list[ $sl_key ][ $key ] = array(
				'item_id' => $item->item_id,
				'type' => $item->pricing_category,
				'name' => $item->name,
				'quantity' => floatval( $item->quantity ),
				'cost' => floatval( $item->cost ),
				'items_su' => $item->items_su,
				'su_needed' => $item->su_needed,
				'leftovers' => $item->leftovers,
				'bkdwn' => array( $item->event_id => floatval( $item->quantity ) ),
				'purchase_unit' => $item->variation->item->purchase_unit,
			);
			if ( $with_photos ) {
				if ( $item->variation ) {
					$shopping_list[ $sl_key ][ $key ]['photo'] = sc_get_variation_attachment( $item->variation, $item->variation->item->default_variation );
				} else {
					$shopping_list[ $sl_key ][ $key ]['photo'] = array(
						'imageId' =>'',
						'imageURL' => '',
						'imageW' => '',
						'imageH' => '',
						'imageThumbURL' => '',
					);
				}
			}
			if ( isset( $item->quantity_new ) ) {
				$shopping_list[ $sl_key ][ $key ]['quantity_new'] = $item->quantity_new;
			}
		} else {
			$shopping_list[ $sl_key ][ $key ]['quantity'] += $item->quantity;
			$shopping_list[ $sl_key ][ $key ]['cost'] += $item->cost;
			$shopping_list[ $sl_key ][ $key ]['purchase_unit'] = $item->variation->item->purchase_unit;
			if ( isset( $item->quantity_new ) ) {
				$shopping_list[ $sl_key ][ $key ]['quantity_new'] += $item->quantity_new;
			}
			//Breakdown item quantities to events
			if ( isset( $shopping_list[ $sl_key ][ $key ] [ 'bkdwn' ] [ $item->event_id ] ) ) {
				$shopping_list[ $sl_key ][ $key ] [ 'bkdwn' ] [ $item->event_id ] += floatval( $item->quantity );
			} else {
				$shopping_list[ $sl_key ][ $key ] [ 'bkdwn' ] [ $item->event_id ] = floatval( $item->quantity );
			}
		}
	}

	foreach ( $shopping_list['included'] as $key => $values ) {
		$shopping_list['included'][ $key ]['su_needed'] = ceil( $values['quantity'] / $values['items_su'] );
		$shopping_list['included'][ $key ]['leftovers'] = ( $values['items_su'] * ceil( $values['quantity'] / $values['items_su'] ) ) - $values['quantity'];
	}

	foreach ( $shopping_list['discarded'] as $key => $values ) {
		$shopping_list['discarded'][ $key ]['su_needed'] = ceil( $values['quantity'] / $values['items_su'] );
		$shopping_list['discarded'][ $key ]['leftovers'] = ( $values['items_su'] * ceil( $values['quantity'] / $values['items_su'] ) ) - $values['quantity'];
	}

	foreach ( $shopping_list['extra'] as $key => $values ) {
		$shopping_list['extra'][ $key ]['su_needed'] = ceil( $values['quantity'] / $values['items_su'] );
		$shopping_list['extra'][ $key ]['leftovers'] = ( $values['items_su'] * ceil( $values['quantity'] / $values['items_su'] ) ) - $values['quantity'];
	}

	uasort( $shopping_list['included'], 'sc_shopping_list_sort' );
	uasort( $shopping_list['discarded'], 'sc_shopping_list_sort' );
	uasort( $shopping_list['extra'], 'sc_shopping_list_sort' );

	return $shopping_list;
}

function sc_render_order_js() {
	global $sc_current_order;

	$shopping_list = false;

	if ( ! empty( $_GET['oid'] ) && ! empty( $sc_current_order ) ) {
		$shopping_list = sc_get_shopping_list_from_order_items( $sc_current_order->items );

		$order_events = json_decode( $sc_current_order->event_ids );
		$order_events = is_array( $order_events ) ? array_map( 'intval', $order_events ) : array();
		$states = array( 'inquiry', 'proposal_sent', 'booked', 'completed' );
		$events = Event::where( array(
			'user_id' => get_current_user_id(),
		) )->whereIn( 'id', $order_events, 'and' )->whereIn( 'state', $states, 'and' )->get();
		
		$order_filtered_events = array();
		$_events = array();
		$shopping_list_page = sc_get_permalink_by_template( 'template-shopping.php' );

		foreach ( $order_events as $event_id ) {
			if ( ! $events->where( 'id', "$event_id" )->isEmpty() ) {
				$_events[] = array(
					'id'                => $event_id,
					'name'              => $events->where( 'id', "$event_id" )->first()->name,
					'shopping_list_url' => add_query_arg( 'eid', $event_id, $shopping_list_page ),
				);
			} else {
				$_events[] = array(
					'id'                => $event_id,
					'name'              => 'Deleted Event',
					'shopping_list_url' => '#',
				);
			}
		}

		$order_data = array(
			'id'              => $sc_current_order->id,
			'name'              => $sc_current_order->name,
			'events'          => $_events,
			'notes'           => $sc_current_order->note,
			'created_on'      => sc_get_user_date_time( get_current_user_id(), $sc_current_order->created_at->toDateTimeString() ),
			'fulfilment_date' => $sc_current_order->fulfilment_date ? sc_get_user_date_time( get_current_user_id(), $sc_current_order->fulfilment_date->toDateTimeString() ) : '',
			'dateFormat'      => sc_get_user_js_date_format( get_current_user_id() ),
		);
	}

	$user_measuring_units = get_user_meta( get_current_user_id(), 'sc_user_measuring_units', true );
	$user_measuring_units = Measuring_Unit::whereIn( 'id', $user_measuring_units )->get();

	?>

	<?php if ( ! empty( $_GET['oid'] ) && ! empty( $sc_current_order ) ) : ?>
		<style type="text/css">
			<?php render_itemslist_style( $sc_current_order->user_id ); ?>
		</style>
	<?php endif; ?>

	<script type="text/javascript">
		(function($) {
			$(document).ready(function(){
				<?php if ( $shopping_list ) : ?>

					stemcounter.page.userItems = <?php echo sc_get_user_items_json( $sc_current_order->user_id ); ?>;
					stemcounter.resortUserItems( <?php echo $sc_current_order->user_id; ?> );
					stemcounter.page.measuring_units = <?php echo json_encode( $user_measuring_units ); ?>;

					$(document).trigger( 'stemcounter.action.renderOrder', [ $('.order-data-shopping-list .order-items-wrap'), <?php echo json_encode( $order_data ) ?>, <?php echo json_encode( $shopping_list ); ?>, <?php echo json_encode( sc_get_user_js_date_format() ); ?> ] );
				<?php endif; ?>

				$('.change-order-status').select2().on('change', function() {
					var hiddenTd = $(this)
						.closest('tr')
						.find('.order-td-status-hidden');

					hiddenTd.text(
						$(this).val().slice(0, 1).toUpperCase() + $(this).val().slice(1)
					);

					//update the td with the new information and let DataTable know the value has changed
					$('#ordersTable').DataTable().cell(hiddenTd).data(hiddenTd.text()).draw();

					$.ajax({
						type:"post",
						url: window.stemcounter.ajax_url,
						data: {
							action: 'sc_change_user_order_status',
							order_id: hiddenTd.data('orderId'),
							order_status: $(this).val()
						},
						success:function(response){
							stemcounter.JSONResponse(response, function(r) {
								if (r.success) document.location.reload(); 
							});
						}
					});

				});

				$("#ordersTable").dataTable({
					initComplete: function() {
						this.api().columns(2).every( function () {
			                var column = this;
			                var select = $('<select class="form-control input-sm order-status-dropdown"><option value=""></option></select>')
			                    .prependTo('#ordersTable_filter')
			                    .on( 'change', function () {
			                        var val = $.fn.dataTable.util.escapeRegex(
			                            $(this).val()
			                        );
			                        //searches the hidden <td>
			                        column
			                            .search(val ? '^'+val+'$' : '', true, false)
			                            .draw();
			                    } );

			                    $('<span>Filter:</span>').prependTo('#ordersTable_filter');

			                    select.append('<option value="planning">Planning</option>')
			                    select.append('<option value="placed">Placed</option>');
			                    select.append('<option value="finished">Finished</option>');
           				} );
					},
					order: [[2, 'desc'], [1, 'desc']],
					aoColumns: [null, { bSearchable: false }, { bSearchable: false }, null, { bSearchable: false }, { bSearchable: false }, { bSearchable: false }, { bSearchable: false }]
				});

				$('.generate-order-button').click(function() {
					openAddOrderPopup(0);
				});

				$('#ordersTable').on('click', '.email-order-button', function(e){
					e.preventDefault();
					var $th = $(this),
						order_name = $th.closest('tr').find('.order-td-name > a').text();

					alertify.confirm('Are you sure you want to email order "' + order_name + '" to Mayesh?', function(){
						$.post(window.stemcounter.ajax_url, {
							action: 'sc/order/email',
							oid: $th.data('orderId')
						}, function(r){
							alertify();
						}, 'json');
					});
				});

				$(document).on('click', '.edit-order-button', function() {
					openEditOrderPopup($(this).data('orderId'));
				});

				// $('.edit-order-button').click(function() {
				// 	openEditOrderPopup($(this).data('orderId'));
				// });

				$(document).on('click', '.delete-order-button', function(e){
					e.preventDefault();
					var orderId = $(this).data('orderId');

				// $('.delete-order-button').click(function(e){
				// 	e.preventDefault();
				// 	var orderId = $(this).data('orderId');

					alertify.confirm('Are you sure you wish to delete this order?', function () {
					    jQuery.ajax({
							type:"post",
							url: window.stemcounter.ajax_url,
							data: {
								action: 'sc_delete_user_order',
								order_id: orderId 
							},
							success:function(response){
								stemcounter.JSONResponse(response, function(r) {
									if (r.success) document.location.reload(); 
								});
							}
						});
					});
				});

				$('#ordersTable').on( 'length.dt', function ( e, settings, len ) {
				
					var url = window.stemcounter.aurl({ action: 'sc_datatables_len' }),
						data = {
							'meta_key': 'sc_orders_table_len',
							'meta_value': len
						};

					$.post(url, data, function (response) {
						stemcounter.JSONResponse(response, function (r) {
							if ( ! r.success ) {
								console.log( r );
							}
						});
					});
				});
			});

			function openAddOrderPopup(id, title) {
				title = (typeof title == 'undefined') ? 'Add New Order' : title;
				var url = stemcounter.aurl({
					action: 'sc_add_user_order_form',
				});

				stemcounter.openAjaxModal(title, url, 'new-modal edit-order-modal');
			}

			function openEditOrderPopup(orderId, title) {
				title = (typeof title == 'undefined') ? 'Edit Order' : title;
				var url = stemcounter.aurl({
					action: 'sc_edit_user_order_form',
					order_id: orderId
				});

				stemcounter.openAjaxModal(title, url, 'new-modal edit-order-modal');
			}
		})(jQuery);
	</script>
	<?php
}
add_action( 'sc/app/footer/order', 'sc_render_order_js', 10 );

function sc_ajax_get_order() {
	$request = Request::capture();
	$r = new Ajax_Response();
	
	$user_id = intval(get_current_user_id());
	// get all arrangement items only for arrangements included in the proposal
	
	try {
		$order = Order::where(array(
			'id' => $request->input('order_id'),
			'user_id' => $user_id
		))->with('items')->firstOrFail();

		// var_dump( $order->items );

		ob_start();

		sc_render_order( $order );

		$r->add_payload('html', ob_get_clean());
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again later.');
	}
	$r->respond('Success');
}
add_action('wp_ajax_sc_ajax_get_order', 'sc_ajax_get_order');

function sc_render_order( $order ) { ?>
	<div class="content-panel order-data-shopping-list">
		<!--
		<div class="row">
			<div class="col-lg-12">
				<h3 class="mb"><?php echo $order->name; ?></h3>
				 <a href="#" class="category-refresh"><i class="fa fa-refresh"></i></a>
			</div>
		</div>
		-->
		<div class="order-items-wrap"></div>
	</div><!-- /.content-panel -->
	<?php
}

function sc_ajax_email_order() {
	$request = Request::capture();
	$r = new Ajax_Response();
	
	$user_id = intval( get_current_user_id() );
	$recipient = $request->input( 'recipient' );
	$wholesaler = $request->input( 'wholesaler' );
	$with_photos = $request->input( 'with_photos' );
	$wholesaler = $wholesaler && 'mayesh' == $wholesaler ? 'sanders@mayesh.com' : false;
	$notes = $request->input( 'notes' );
	if ( ! $recipient && ! $wholesaler ) {
		$r->fail( 'You have to specify the recipient of your email.' );
	}
	
	try {
		$order = Order::where( array(
			'id' => $request->input( 'oid' ),
			'user_id' => $user_id,
		) )->with( array( 'items' => function( $query ) {
			$query->where( 'discarded', '=', 0 );
		} ) )->firstOrFail();

		$current_user = get_userdata( $user_id );
		$company_info = get_company_info( $user_id );
		$company_info['email'] = isset( $current_user->user_email ) ? $current_user->user_email : '';
		$additionalInfo = maybe_unserialize( $current_user->additionalInfo ); //Get User Details
		$company_info['phone'] = isset( $additionalInfo['companyMobile'] )  ? $additionalInfo['companyMobile'] : '';
		
		$args = array(
			'order'			=> $order,
			'company_info'	=> $company_info,
			'page_title' 	=> 'Floral Order',
			'notes'			=> stripslashes( $notes ),
			'user'			=> $current_user,
			'with_photos'   => $with_photos,
		);

		add_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );

		$headers = array();
		$headers[] = 'From: "' . $company_info['company'] . '" <noreply@stemcounter.com>';
		$headers[] = 'Reply-To: "' . $company_info['company'] . '" <' . $company_info['email'] . '>';
		if ( $company_info['email'] != $recipient ) {
			$headers[] = 'CC: "' . $company_info['company'] . '" <' . $company_info['email'] . '>';
			$to = sc_prepare_email_recipients_for_mandrill( $recipient ? $recipient : $wholesaler, $company_info['email'], $company_info['company'] );
		} else {
			$to = sc_prepare_email_recipients_for_mandrill( $recipient );
		}
		$headers[] = 'List-Unsubscribe: <' . home_url( '/app/wp-admin/admin-ajax.php?action=sc_email_unsubscribe' ) . '>';

		if ( class_exists( 'wpMandrill' ) && is_array( $to ) ) {
			/*$to[] = array(
				'email' => 'ryan@logosidentity.com',
				'name'  => 'Ryan O\'Neil',
				'type'  => 'bcc',
			);*/

			if ( $wholesaler && $recipient ) {
				$to[] = array(
					'email' => $wholesaler,
					'type'  => 'cc',
				);
			}
		} else {
			// $headers[] = 'Bcc: "Ryan O\'Neil" <ryan@logosidentity.com>';
			if ( $wholesaler ) {
				$headers[] = "Cc: <$wholesaler>";
			}
		}
		$subject = 'New Floral Order';
		$body = sc_render_template( 'templates/html-mail-header', $args, true );
		$body .= sc_render_template( 'templates/template-order', $args, true );
		$body .= sc_render_template( 'templates/html-footer', $args, true );

		if ( ! wp_mail( $to, $subject, $body, $headers ) ) {
			$r->fail('We are currently unable to send through the email. Please try again.');
		}

		remove_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}

	$r->add_payload( 'message', sprintf( 'Order was successfully emailed to %s', $recipient ) );
	$r->respond( 'Success' );
}
add_action( 'wp_ajax_sc/order/email', 'sc_ajax_email_order', 10 );

function sc_ajax_transfer_order_items() {
	$request = Request::capture();
	$r = new Ajax_Response();
	
	$user_id = intval( get_current_user_id() );
	$item_keys = $request->input( 'item_keys' );
	$discard = 'included' == $request->input( 'from' );
	if ( ! $item_keys ) {
		$r->fail( 'Please select one or more items to transfer.' );
	}
	$item_keys = array_map( 'stripslashes', $item_keys );
	// error_log( var_export( $item_keys, true ) );
	
	try {
		$order = Order::where( array(
			'id' => $request->input( 'oid' ),
			'user_id' => $user_id,
		) )->with( array( 'items' ) )->firstOrFail();

		$order->items->map(function($item) use( $discard, $item_keys ){
			if ( ( $discard && $item->discarded ) || ( ! $discard && ! $item->discarded ) ) {
				return $item;
			}

			$key = "{$item->pricing_category}_{$item->name}_{$item->item_id}";

			if ( in_array( $key, $item_keys ) ) {
				$item->discarded = $discard;
				$item->save();
			}

			return $item;
		});

		$shopping_list = sc_get_shopping_list_from_order_items( $order->items );

		$total_combined = wp_list_pluck( $shopping_list['included'], 'cost' );
		$total_combined = array_sum( $total_combined );
		// Make sure to update the order estimate in the DB
		$order->estimate = $total_combined;
		$order->save();

		$r->add_payload( 'items', $shopping_list );
		$r->add_payload( 'message', 'Items were successfully transferred.' );
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}

	$r->respond( 'Success' );
}
add_action( 'wp_ajax_sc/order/transfer_items', 'sc_ajax_transfer_order_items', 10 );


function sc_ajax_bump_su() {
	$request = Request::capture();
	$r = new Ajax_Response();
	
	$su_changes = $request->input( 'su_changes' );
	$order_id = $su_changes['order_id'];

	if ( !$order_id ) {
		$r->fail( 'Order id is missing' );
	}

	$items = $su_changes['items'];
	$user_id = intval( get_current_user_id() );

	foreach($items as $item_id => $item) {

		$step = $item['step'];
		if ( !$step ) {
			$r->fail( 'Step is zero - no action was performed' );
		}

		try {

			//Update bumped item
			$old_item = Order_Item::where( array(
				'order_id' => $order_id,
				'item_id' => $item_id,
			) )->firstOrFail();
			$item_price = $old_item->cost / $old_item->quantity;
			
			$bumped_item = Order_Item::firstOrNew(
				array(
					'order_id' => $order_id,
					'event_id' => 0,
					'item_id' => $item_id,
				)
			);

			if ( 0 >  $bumped_item->su_needed + $step ) {
				$r->fail( 'Minimum purchase units needed limit reached!' );
			}

			if (!is_null($bumped_item) && $bumped_item->exists) {

				$bumped_item->su_needed += $step;
				$bumped_item->quantity = $bumped_item->items_su * $bumped_item->su_needed;
				$bumped_item->cost = $bumped_item->quantity * $item_price;
				$bumped_item->save();
				$r->add_payload( 'message', '"Su needed" updated' );

			} else {

				$bumped_item = $old_item->replicate();
				$bumped_item->event_id = 0;
				$bumped_item->su_needed = 1;
				$bumped_item->quantity = $bumped_item->items_su;
				$bumped_item->save();
				$r->add_payload( 'message', '"Su needed" added' );
			}

		} catch (Exception $e) {
			$r->fail( 'Something went wrong. Please reload page and try again.' );
		}
	}

	try {

		//Update order estimate
		$order = Order::where( array(
			'id' => $order_id,
			'user_id' => $user_id,
		) )->with( array( 'items' ) )->firstOrFail();

		$recalculated_shopping_list = sc_get_shopping_list_from_order_items( $order->items );
		$total_combined = wp_list_pluck( $recalculated_shopping_list['included'], 'cost' );
		$total_combined = array_sum( $total_combined );
		$order->estimate = $total_combined;
		$order->save();
		$r->respond( 'Success' );

	} catch (Exception $e) {
		$r->fail( 'Something went wrong. Please reload page and try again try again!' );
	}
}

add_action( 'wp_ajax_sc_bump_su', 'sc_ajax_bump_su', 10 );


function sc_ajax_split_order() {
	$request = Request::capture();
	$r = new Ajax_Response();

	$user_id = intval( get_current_user_id() );
	$item_keys = $request->input( 'item_keys' );
	if ( ! $item_keys ) {
		$r->fail( 'Please select one or more items to be split away into a new order.' );
	}
	$item_keys = array_map( 'stripslashes', $item_keys );
	// error_log( var_export( $item_keys, true ) );

	$event_ids = array();
	$items_to_transfer = array();

	try {
		$order = Order::where( array(
			'id' => $request->input( 'oid' ),
			'user_id' => $user_id,
		) )->with( array( 'items' ) )->firstOrFail();

		$order_items = $order->items->filter(function($item) use( $item_keys, &$items_to_transfer, &$event_ids ){
			if ( $item->discarded ) {
				return true;
			}

			$key = "{$item->pricing_category}_{$item->name}_{$item->item_id}";

			if ( in_array( $key, $item_keys ) ) {
				$event_ids[ $item->event_id ] = true;
				$items_to_transfer[] = $item;
				return false;
			}

			return true;
		});

		$new_order_name = $request->input( 'new_split_order_name' );
		$new_order_name = ( $new_order_name ) ? $new_order_name : sprintf( 'Split from order "%s"', $order->name );

		$total_combined = 0;
		$new_shopping_list = sc_get_shopping_list_from_order_items( $items_to_transfer );
		$total_combined = wp_list_pluck( $new_shopping_list['included'], 'cost' );
		$total_combined = array_sum( $total_combined );

		$new_order = new Order;
		$new_order->user_id = $user_id;
		$new_order->event_ids = json_encode( array_keys( $event_ids ) );
		$new_order->name = $new_order_name;
		$new_order->note = $order->note;
		$new_order->status = $order->status;
		$new_order->flowers_items = '';
		$new_order->estimate = $total_combined;
		$new_order->fulfilment_date = $order->fulfilment_date;

		$new_order->save();

		foreach ( $items_to_transfer as $item ) {
			$item->order_id = $new_order->id;
			$item->save();
		}

		$shopping_list = sc_get_shopping_list_from_order_items( $order_items );

		$total_combined = wp_list_pluck( $shopping_list['included'], 'cost' );
		$total_combined = array_sum( $total_combined );
		$order->estimate = $total_combined;
		$order->save();

		$r->add_payload( 'new_order_url', add_query_arg( 'oid', $new_order->id, sc_get_permalink_by_template( 'template-order.php' ) ) );
		$r->add_payload( 'items', $shopping_list );
		$r->add_payload( 'message', 'New order created successfully.' );
	} catch (Exception $e) {
		$r->fail( 'Something went wrong. Please try again.' );
	}

	$r->respond( 'Success' );
}
add_action( 'wp_ajax_sc/order/split', 'sc_ajax_split_order', 10 );

function sc_ajax_update_order_notes() {
	$request = Request::capture();
	$r = new Ajax_Response();

	$user_id = intval( get_current_user_id() );
	$notes = stripslashes( $request->input( 'notes' ) );

	try {
		$order = Order::where( array(
			'id' => $request->input( 'oid' ),
			'user_id' => $user_id,
		) )->firstOrFail();

		$order->note = $notes;
		$order->save();
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}

	$r->respond( 'Success' );
}
add_action( 'wp_ajax_sc/order/update_notes', 'sc_ajax_update_order_notes', 10 );

function sc_ajax_update_order_fulfilment_date() {
	$request = Request::capture();
	$r = new Ajax_Response();

	$user_id = intval( get_current_user_id() );
	$date = stripslashes( $request->input( 'date' ) );

	try {
		$order = Order::where( array(
			'id' => $request->input( 'oid' ),
			'user_id' => $user_id,
		) )->firstOrFail();

		$date_format = get_user_meta( $user_id, '__date_format', true );
		if ( $date ) {
			$fulfilment_date = DateTime::createFromFormat( $date_format, $date );
			if ( $fulfilment_date ) {
				$order->fulfilment_date = date( MYSQL_DATE_FORMAT, $fulfilment_date->getTimestamp() );
			}
		} else {
			$order->fulfilment_date = null;
		}

		$order->save();
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}

	$r->respond( 'Success' );
}
add_action( 'wp_ajax_sc/order/update_fulfilment_date', 'sc_ajax_update_order_fulfilment_date', 10 );

function sc_ajax_delete_order_items() {
	$request = Request::capture();
	$r = new Ajax_Response();

	$user_id = intval( get_current_user_id() );
	$item_keys_to_delete = array_map( 'stripslashes', $request->input( 'items_to_delete' ) );
	if ( ! $item_keys_to_delete ) {
		$r->fail('You need to select one or more items to delete.');
	}

	try {
		$order = Order::where( array(
			'id' => $request->input( 'oid' ),
			'user_id' => $user_id,
		) )->with( array( 'items' ) )->firstOrFail();

		$order_items = $order->items->filter( function( $item ) use( $item_keys_to_delete ){
			$key = "{$item->pricing_category}_{$item->name}_{$item->item_id}";

			if ( in_array( $key, $item_keys_to_delete ) ) {
				$item->delete();
				return false;
			} else {
				return true;
			}
		});

		$shopping_list = sc_get_shopping_list_from_order_items( $order_items );

		$total_combined = wp_list_pluck( $shopping_list['included'], 'cost' );
		$total_combined = array_sum( $total_combined );
		$order->estimate = $total_combined;
		$order->save();

		$r->add_payload( 'items', $shopping_list );
		$r->add_payload( 'message', 'Selected order items deleted successfully.' );
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}

	$r->respond( 'Success' );
}
add_action( 'wp_ajax_sc/order/delete_items', 'sc_ajax_delete_order_items', 10 );

function sc_maybe_render_order_email_template() {
	if ( is_page_template( 'template-order.php' ) && ! empty( $_GET['oid'] ) && ! empty( $_GET['email'] ) ) {
		$request = Request::capture();
		$user_id = get_current_user_id();

		try {
			$order = Order::where(array(
				'id' => $request->input('oid'),
				'user_id' => $user_id,
			))->with( array( 'items', 'items.variation', 'items.variation.item', 'items.variation.item.default_variation' ) )->firstOrFail();
		} catch (Exception $e) {
			$r->fail('Something went wrong. Please try again.');
		}
		
		$current_user = get_userdata( $user_id );
		$company_info = get_company_info( $user_id );
		$company_info['email'] = isset( $current_user->user_email ) ? $current_user->user_email : '';
		$additionalInfo = maybe_unserialize( $current_user->additionalInfo ); //Get User Details
		$company_info['phone'] = isset( $additionalInfo['companyMobile'] )  ? $additionalInfo['companyMobile'] : '';

		$args = array(
			'order'			=> $order,
			'company_info'	=> $company_info,
			'page_title' 	=> 'Floral Order',
			'notes'			=> '',
			'user'			=> $current_user,
			'with_photos'   => true,
		);

		sc_render_template( 'templates/html-mail-header', $args );
		sc_render_template( 'templates/template-order', $args );
		sc_render_template( 'templates/html-footer', $args );

		exit;
	}
}
add_action( 'template_redirect', 'sc_maybe_render_order_email_template', 10 );
