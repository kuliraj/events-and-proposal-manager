<?php
use Illuminate\Http\Request;
use Symfony\Component\Translation\Translator;
use Illuminate\Validation\Factory;
use Stemcounter\Recipe;
use Stemcounter\Item;
use Stemcounter\Event;
use Stemcounter\Tax_Rate;
use Stemcounter\Invoicing_Category;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Photo;
use Stemcounter\Arrangement_Recipe;
use Stemcounter\Arrangement_Item;
use Stemcounter\Item_Variation;
use Stemcounter\Recipe_Item;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;

function sc_ajax_edit_recipe() {
	sc_clean_input_slashes();
	$request = Request::capture();

	$user_id = get_current_user_id();
	$id = intval( $request->input('user_recipe_id') );

	if ( $id ) {
		try {
			$recipe = Recipe::where( array(
			'id' => $id,
			) )->with( array(
				'items',
				'items.item' => function($query) {
					$query->withTrashed();
				}
			) )->first();
		} catch ( Exception $e ){
			throw new Exception( 'Failed to edit recipe. Please contact support.', 999 );
		}
	} else {
		$recipe = new Recipe();
	}

	if ( is_null( $request->input( 'event_id' ) ) || ! $id ) {
		$recipe->name = $request->input( 'recipe_name' );
	}

	$recipe->user_id = $user_id;
	$recipe->description = ! empty( $request->input('recipe_description') ) ? $request->input('recipe_description') : '';
	$recipe->save();

	foreach ($recipe->items as $item) {
		$item->delete();
	}

	$recipe_items = array();
	$new_items_count = count( (array) $request->input('item_keypair', array()) );

	for ( $i = 0; $i < $new_items_count; $i++ ) {
		$shy = ( $request->input('item_shy.' . $i ) == 'y' );
		if ( $shy ) {
			continue; // ignore item as it's shy and should not be added
		}
		$keypair = explode( '_', $request->input( 'item_keypair.' . $i ) );
		$item_id = empty( $keypair[0] ) ? 0 : intval( $keypair[0] );
		$variation_id = empty( $keypair[1] ) ? 0 : intval( $keypair[1] );
		try {
			$item = sc_get_user_item_qb( $user_id, $item_id )->firstOrFail();
		} catch ( Exception $e ) {
			continue; // invalid item selected; skip it
		}

		$recipe_item = new Recipe_Item();
		$recipe_item->item()->associate( $item );
		$recipe_item->item_type = 'Stemcounter\Item';
		$recipe_item->item_variation_id = $variation_id;
		$recipe_item->quantity = abs( floatval( $request->input( 'item_quantity.' . $i, 1 ) ) );
		$recipe_items[] = $recipe_item;
	}


	// order items alphabetically
	usort( $recipe_items, function( $a, $b ) {
		return strcmp( strtolower( $a->item->name ), strtolower( $b->item->name ) );
	});
	$recipe->items()->saveMany( $recipe_items );
	
	$response['success'] = true;
	$response['message'] = '';

	wp_send_json( $response );
}
add_action( 'wp_ajax_sc_edit_recipe', 'sc_ajax_edit_recipe' );

function sc_recipe_delete() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$recipe_id = $request->input( 'id', 0 );

	try {
		$recipe = Recipe::where(array(
			'id' => $recipe_id,
		))->delete();

		$items = Recipe_Item::where(array(
			'recipe_id' => $recipe_id,
		))->delete();

	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_recipe_delete', 'sc_recipe_delete', 10 );

function sc_render_recipes_management( $content ) {
	$arrangements = Arrangement::where( array(
		'event_id' => '0',
		'user_id'  => get_current_user_id(),
	) )->get();

	$table_len = get_user_meta( get_current_user_id(), 'sc_recipes_table_len', true );

	ob_start(); ?>
	<div class="row mt">
		<div class="col-md-12">
			<div class="content-panel">
				<table class="recipes-table table table-striped table-advance table-hover" id="listItems" data-page-length="<?php echo ! empty( $table_len ) ? $table_len : 10 ; ?>">
					<thead>
						<tr>
							<th colspan="5">
								<a class="create-new-recipe-btn cta-link">
									+ Add New Recipe
								</a>
							</th>
						</tr>
						<tr>
							<th>Recipe</th>
							<th class="hidden-xs"></th>
						</tr>
						
					</thead>
					<tbody>
						<?php foreach ( $arrangements as $i => $arrangement ) : ?>
							<tr class="recipe" id="recipe-row-<?php echo esc_attr( $arrangement->id ); ?>">
								<td class="hide-input">
									<a href="#" class="recipe-name" data-item-id="<?php echo $arrangement->id; ?>">
										<?php echo esc_html( empty( $arrangement->name ) ? '(no name)' : $arrangement->name ); ?>
									</a>
									<input class="recipe-id hidden" type="text" name="recipe[][recipe_id]" value="<?php echo $arrangement->id; ?>"/>
									<?php
									/* if ( ! empty( $recipe->items ) ) { ?>
										<div class="recipe-items">
											<?php
											foreach ( $recipe->items as $item ) { ?>
													<span><?php echo $item->quantity . ' - ' . $item->item->name . ' ' . ('Default' != $item->item_variation->name ? $item->item_variation->name : ''); ?></span>
											<?php } ?>
										</div>			
									<?php } */ ?>
								</td>
								<td class="recipe-edit-button hidden-xs style="text-align: right;">
									<a href="#" class="edit-item-button" data-item-id="<?php echo $arrangement->id; ?>">
										<i class="fa fa-pencil"></i>
									</a>

								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<!-- <h6 class="footnote">*All items listed here are multiplied by your floral multiple when calculated on your proposal. </h6> -->
			</div><!-- /content-panel -->
		</div><!-- /col-md-12 -->
	</div><!-- /row -->

	<div id="standalone-recipes-wrap"></div>
	<?php
	return ob_get_clean();
}
add_filter( 'sc/app/content/recipes', 'sc_render_recipes_management', 10 );

function sc_get_fake_event_for_user( $user_id = false ) {
	$user_id = $user_id ? $user_id : get_current_user_id();

	$event = new Event;
	$event->id = 0;
	$event->card = 0;
	$event->separate_cc_fee = false;
	$event->user_id = get_current_user_id();
	$event->tax_rate_id = 0;

	if ( ! get_user_meta( get_current_user_id(), 'default_invoice_category_id', true ) ) {
		$event->invoicing_category_id = 0;
		$event->hardgood_multiple = $event->fresh_flower_multiple = 1;
		$event->global_labor_value = $event->flower_labor = $event->hardgood_labor = $event->base_price_labor = $event->fee_labor = $event->rental_labor = 0;
	} else {
		$default_invoice_category = Invoicing_Category::where( array(
			'id' => get_user_meta( get_current_user_id(), 'default_invoice_category_id', true ),
			'user_id' => get_current_user_id()
		) )->first();

		$event->invoicing_category_id = $default_invoice_category->id;
		$event->hardgood_multiple = $default_invoice_category->hardgood_multiple;
		$event->fresh_flower_multiple = $default_invoice_category->fresh_flower_multiple;
		$event->global_labor_value = $default_invoice_category->global_labor_value;
		$event->flower_labor = $default_invoice_category->flower_labor;
		$event->hardgood_labor = $default_invoice_category->hardgood_labor;
		$event->base_price_labor = $default_invoice_category->base_price_labor;
		$event->fee_labor = $default_invoice_category->fee_labor;
		$event->rental_labor = $default_invoice_category->rental_labor;
	}

	return $event;
}

function sc_render_recipes_js() {
	$event = sc_get_fake_event_for_user();
	$user_id = $event->user_id;

	$invoice_categories_list = Invoicing_Category::where( array(
		'user_id' => $user_id
	) )->get()->toArray();
	$item_types = sc_get_mixed_arrangement_item_types();

	$values = array(
		'hardgood_multiple' => $event->hardgood_multiple,
		'fresh_flower_multiple' => $event->fresh_flower_multiple,
		'labor' => array(
			'global_labor_value' => $event->global_labor_value,
			'flower' => $event->flower_labor,
			'hardgood' => $event->hardgood_labor,
			'base_price' => $event->base_price_labor,
			'fee' => $event->fee_labor,
			'rental' => $event->rental_labor
		)
	);

	$name = 'test';
	try {
		$event_invoicing_category = Invoicing_Category::where(array(
			'id' => $event->invoicing_category_id,
			'user_id' => $user_id
		))->firstOrFail();
		$selected_category_id = $event_invoicing_category->id;

		foreach ($invoice_categories_list as $i => $value) {
			if ($value['id'] == $selected_category_id) {
				$invoice_categories_list[$i]["hardgood_multiple"] = $event->hardgood_multiple;
			    $invoice_categories_list[$i]["fresh_flower_multiple"] = $event->fresh_flower_multiple;
			    $invoice_categories_list[$i]["global_labor_value"] = $event->global_labor_value;
			    $invoice_categories_list[$i]["flower_labor"] = $event->flower_labor;
			    $invoice_categories_list[$i]["hardgood_labor"] = $event->hardgood_labor;
			    $invoice_categories_list[$i]["base_price_labor"] = $event->base_price_labor;
			    $invoice_categories_list[$i]["fee_labor"] = $event->fee_labor;
			    $invoice_categories_list[$i]["rental_labor"] = $event->rental_labor;
				break;						
			}
		}
	} catch (Exception $e) {
		$name = 'Deleted Markup Profile';
		array_unshift($invoice_categories_list, array(
			"id" => $event->invoicing_category_id,
		    "user_id" => $user_id,
		    "name" => $name,
		    "hardgood_multiple" => $event->hardgood_multiple,
		    "fresh_flower_multiple" => $event->fresh_flower_multiple,
		    "global_labor_value" => $event->global_labor_value,
		    "flower_labor" => $event->flower_labor,
		    "hardgood_labor" => $event->hardgood_labor,
		    "base_price_labor" => $event->base_price_labor,
		    "fee_labor" => $event->fee_labor,
		    "rental_labor" => $event->rental_labor
		));
		$selected_category_id = $event->invoicing_category_id;
	}

	$date_format = get_user_meta( $user_id, '__date_format', true );
	$js_date_format = sc_get_user_js_date_format( $user_id );

	$_event = array(
		'id'          		=> '0',
		'name'        		=> '',
		'arrangements'		=> array(),
	);

	$react_data = array(
		'event'               => $_event,
		'currencySymbol'      => sc_get_user_saved_pref_currency( $user_id ),
		'itemTypes'           => $item_types,
		'categoryId'          => $selected_category_id,
		'eventInvoiceValues'  => $values,
		'categoriesList'      => $invoice_categories_list,
		'dateFormat'          => $js_date_format,
		'selectedArrangement' => false
	);

	$user_libraries = get_user_meta( $user_id, 'sc_user_libraries', true );
	$user_libraries = is_array( $user_libraries ) ? $user_libraries : array();

	// Filter the data we send to react so that we can move add-on logic out into separate files
	$react_data = apply_filters( 'sc/react/data/standalone-recipes', $react_data, $event, $user_id );

	?>
	<style type="text/css">
		<?php $proposalColor = get_user_meta( $user_id,	'proposal_color', true ); ?>
		div.Select .Select-menu-outer .VirtualizedSelectOption.user-lib-<?php echo $event->user_id; ?>:before {
			content: '\f02d';
			display: inline-block;
			font-family: 'FontAwesome';
			font-size: 14px;
			padding-right: 5px;
			padding-left: 3px;
			color: <?php echo $proposalColor ? $proposalColor : '#f37a76'; ?>;
		}

		<?php foreach ( $user_libraries as $library ):
			$library_logo = get_user_meta( $library, 'sc_user_library_logo', true );
			if ( ! empty( $library_logo ) ) : ?>
				div.Select .Select-menu-outer .VirtualizedSelectOption.user-lib-<?php echo $library; ?> {
					background-image: url('<?php echo $library_logo; ?>');
					background-repeat: no-repeat;
					background-position: 6px 6px;
					background-size: 20px auto;
					padding-left: 31px;
				}
			<?php endif; ?>
		<?php endforeach; ?>
	</style>
	<script type="text/javascript">
		stemcounter.page.userItems = <?php echo sc_get_user_items_json( $user_id, true ) ?>;

		(function($){

			$(document).ready(function(){
				var flowerTable = $("#listItems").dataTable({
					lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
					// "orderCellsTop": false
				});

				var settings = <?php echo json_encode( $react_data ); ?>;
				settings.node = $('#standalone-recipes-wrap');

				$(document).trigger('stemcounter.action.renderStandaloneRecipes', settings);
			});

			$('#listItems').on( 'length.dt', function ( e, settings, len ) {
				
				var url = window.stemcounter.aurl({ action: 'sc_datatables_len' }),
					data = {
						'meta_key': 'sc_recipes_table_len',
						'meta_value': len
					};

				$.post(url, data, function (response) {
					stemcounter.JSONResponse(response, function (r) {
						if ( ! r.success ) {
							console.log( r );
						}
					});
				});
			});

			function editItem(itemId, itemType, variation_id) {
				var url = window.stemcounter.aurl({
					action: 'sc_edit_item_form',
					id: itemId,
					type: itemType,
					variation_id: variation_id
				});

				stemcounter.openAjaxModal('Edit Item', url, 'new-modal edit-item-modal');
			}

			$(document).on('stemcounter.action.renderEditItemForm', function(e,  r, itemId, itemType, variation_id){
				editItem(itemId, itemType, variation_id);
			});
			
			$(document).on('stemcounter.action.renderAddNewItemForm', function(e, caller, lib_user_id, lib_item_id, lib_variation_id){
				lib_user_id = lib_user_id ? lib_user_id : null;
				lib_item_id = lib_item_id ? lib_item_id : null;
				lib_variation_id = lib_variation_id ? lib_variation_id : null;
				createNewItem(caller, lib_user_id, lib_item_id, lib_variation_id);
			});
			
			stemcounter.page.createNewItemCaller = null;
			
			function createNewItem(caller, lib_user_id, lib_item_id, lib_variation_id) {
				stemcounter.page.createNewItemCaller = caller;
				var url = window.stemcounter.aurl({
					action: 'sc_edit_item_form',
					lib_user_id: lib_user_id,
					lib_item_id: lib_item_id,
					variation_id: lib_variation_id,
					added_form_lib: lib_user_id ? 1 : 0
				});

				stemcounter.openAjaxModal('Add New Item', url, 'new-modal edit-item-modal');
			}

		})(jQuery);
	</script>
	<?php
}
add_action( 'sc/app/footer/recipes', 'sc_render_recipes_js', 10 );
add_action( 'sc/app/footer/recipes', 'sc_photoswipe_html', 10 );

function sc_ajax_edit_standalone_recipes() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$event = sc_get_fake_event_for_user();

	$arrangements = $request->input( 'arrangements' );

	$invoicing_settings = $request->input( 'invoicing_settings' );
	$invoicing_category_id = $request->input( 'invoicing_category_id' );
	$user_id = $event->user_id;

	if ( ! empty( $arrangements ) && ! is_null( $arrangements ) ) {
		$_arr = array();
		$inc_recipes = array();
		$inc_items = array();

		foreach ( $arrangements as $arrangement ) {
			try {
				if ( ! empty( $arrangement['duplicated_from'] ) ) {
					$updated_arrangement = Arrangement::where(array(
						'id' 		=> intval( $arrangement['duplicated_from'] ),
						'user_id'	=> $event->user_id,
					))->with(array(
						'recipes',
						'photos',
						'items',
						'items.item',
						'items.item_variation',
						'items.item_variation.item',
					))->first();

					$updated_arrangement = sc_duplicate_arrangement( $updated_arrangement, $event, $inc_recipes, $inc_items );
				} else if ( -1 == $arrangement['id'] ) {
					$updated_arrangement = new Arrangement;
					$updated_arrangement->event_id = $event->id;
					$updated_arrangement->user_id = $event->user_id;
				} else {
					$updated_arrangement = Arrangement::where('id', '=', $arrangement['id'])->first();

					$photos_to_delete = array();
					foreach ($updated_arrangement->photos as $photo) {
						array_push($photos_to_delete, $photo);
					}
					
					$arr_photos = array();
					foreach ( $arrangement['photos'] as $i => $photo_id ) {
						$image = wp_get_attachment_image_src( $photo_id['image_id'] );

						if ( ! is_null( $photo_id['image_id'] ) && ! empty( $image ) ) {
							$arrangement_photo = new Arrangement_Photo();
							$arrangement_photo->arrangement_id = $updated_arrangement->id;
							$arrangement_photo->photo_id = (int) $photo_id['image_id'];
							$arrangement_photo->modal_position = $i;
							$arr_photos[] = $arrangement_photo;
						}
					}
					$updated_arrangement->photos()->saveMany($arr_photos);

					foreach( $photos_to_delete as $photo ) {
						$photo->delete();
					}
				}

				if ( empty( $arrangement['from_recipe'] ) ) {
					$updated_arrangement->name = $arrangement['name'];
					$updated_arrangement->quantity = $arrangement['quantity'];
					$updated_arrangement->note = $arrangement['note'];
					$updated_arrangement->order = $arrangement['order'];
					$updated_arrangement->include = $arrangement['include'];
					$updated_arrangement->tax = $arrangement['tax'];
					$updated_arrangement->addon = $arrangement['addon'];
					$updated_arrangement->is_percentage = intval( $arrangement['is_percentage'] );


					if ( $arrangement['invoicing_category_id'] <= 0 ) {
						$updated_arrangement->invoicing_category_id = NULL;
						$updated_arrangement->fresh_flower_multiple = NULL;
						$updated_arrangement->hardgood_multiple = NULL;
						$updated_arrangement->global_labor_value = NULL;
						$updated_arrangement->flower_labor = NULL;
						$updated_arrangement->hardgood_labor = NULL;
						$updated_arrangement->base_price_labor = NULL;
						$updated_arrangement->fee_labor = NULL;
					} else {
						//otherwise save them
						$updated_arrangement->invoicing_category_id = abs( $arrangement['invoicing_category_id'] );
						$updated_arrangement->fresh_flower_multiple = is_null( $arrangement['fresh_flower_multiple'] ) ? 1 : $arrangement['fresh_flower_multiple'];
						$updated_arrangement->hardgood_multiple = is_null( $arrangement['hardgood_multiple'] ) ? 1 : $arrangement['hardgood_multiple'];
						$updated_arrangement->global_labor_value = is_null( $arrangement['global_labor_value'] ) ? 0 : $arrangement['global_labor_value'];
						$updated_arrangement->flower_labor = is_null( $arrangement['flower_labor'] ) ? 0 : $arrangement['flower_labor'];
						$updated_arrangement->hardgood_labor = is_null( $arrangement['hardgood_labor'] ) ? 0 : $arrangement['hardgood_labor'];
						$updated_arrangement->base_price_labor = is_null( $arrangement['base_price_labor'] ) ? 0 : $arrangement['base_price_labor'];
						$updated_arrangement->fee_labor = is_null( $arrangement['fee_labor'] ) ? 0 : $arrangement['fee_labor'];
						$updated_arrangement->rental_labor = is_null( $arrangement['rental_labor'] ) ? 0 : $arrangement['rental_labor'];
					}

					if ( isset( $arrangement['override_cost'] ) && ! is_null( $arrangement['override_cost'] ) && '' !== $arrangement['override_cost'] ) {
						$updated_arrangement->override_cost = $arrangement['override_cost'];
					} else {
						$updated_arrangement->override_cost = NULL;
					}

					$updated_arrangement->save();

					if ( -1 == $arrangement['id'] ) {
						$new_recipe = new Arrangement_Recipe;
						$new_recipe->arrangement_id = $updated_arrangement->id;
						$new_recipe->name = 'Recipe';
						$new_recipe->save();

						$inc_recipes[] = array( 'id' => $new_recipe->id, 'name' => $new_recipe->name );
					}
				} else {
					$updated_arrangement->save();
				}

				if ( ! empty( $arrangement['duplicate_unique_key'] ) ) {
					$_arr[] = array(
						'key' => $arrangement['duplicate_unique_key'],
						'arr_id' => $updated_arrangement->id,
						'inc_recipes' => $inc_recipes,
						'inc_items'	=> $inc_items,
						'is_percentage'	=> $updated_arrangement->is_percentage,
					);
				}

			} catch (Humanreadable_Exception $e) {
				$r->fail($e->getMessage());
			}
		}
		$r->add_payload( 'inc_arrangements', $_arr );
	}

	$r->respond();
}
add_action( 'wp_ajax_sc/standalone_recipe/edit', 'sc_ajax_edit_standalone_recipes', 10 );

function sc_ajax_load_standalone_recipe() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$event = sc_get_fake_event_for_user();

	try {
		$arrangement = Arrangement::where( array(
			'id' => $request->input( 'arrangement_id' ),
			'event_id' => '0',
			'user_id' => $event->user_id,
		) )->with( array(
			'items',
			'recipes',
			'items.arrangement',
			'items.item_variation',
			'items.item_variation.item',
		) )->orderBy( 'order', 'asc' )->firstOrFail();
	} catch ( Exception $e ) {
		$r->fail( 'This Recipe was not found.' );
	}

	$arrangement->event = $event;
	$arrangement->items->transform(function( $arrangement_item ) use ( $event ) {
		$arrangement_item->arrangement->event = $event;

		return $arrangement_item;
	});

	$arrangement_photos = Arrangement_Photo::where('arrangement_id', '=', $arrangement->id)->get();
	$photos = array();
	foreach ( $arrangement_photos as $photo ) {
		/* if the image exists */
		$image = wp_get_attachment_image_src($photo->photo_id);
		if ( ! empty( $image ) ) {
			$image_id = $photo->photo_id;
			$image_url = wp_get_attachment_image_src( $image_id, 'medium' );
			$image_url = $image_url[0];
			$full_image = wp_get_attachment_image_src( $image_id, 'full' );

			$photos[] = array(
				'image_id' => $image_id,
				'image_url' => $image_url,
				'full_image_src' => $full_image[0],
				'full_image_w' => $full_image[1],
				'full_image_h' => $full_image[2],
			);		
		} else {
			/* delete it from the arrangement_photos if it doesn't */
			Arrangement_Photo::where(array(
				'photo_id' => $photo->photo_id
			))->delete();
		}
	}
	$recipes = array();
	$items = array();

	foreach ( $arrangement->recipes as $arrangement_recipe ) {
		$recipes[] = array(
			'id' => $arrangement_recipe->id,
			'name' => $arrangement_recipe->name,
		);
	}

	foreach ( $arrangement->items as $arrangement_item ) {
		$arrangement_item->cost = ! is_null( $arrangement_item->item_variation->cost ) ? $arrangement_item->item_variation->cost : $arrangement_item->item_variation->item->default_variation->cost;
		$cost_and_markup = $arrangement_item->cost + $arrangement_item->get_markup( false );
		
		$vi = $arrangement_item->toArray();

		$vi['item_type'] = $arrangement_item->item->type;
		$vi['item_variation_name'] = '';

		try {
			//used for deleted items
			$variation = Item_Variation::where(
				array( 'id' => $vi['item_variation_id'] )
			)->firstOrFail()->toArray();

			$vi['item_variation_name'] = ' - ' . $variation['name'];
		} catch (Exception $e) {
		}

		$item_attachment = sc_get_variation_attachment( $arrangement_item->item_variation, $arrangement_item->item->default_variation );

		$key = $arrangement_item->item_id . '_' . $vi['item_variation_id'];

		$type = sc_get_item_type( $arrangement_item->item, true );

		$items[] = array(
			'id'                        => $arrangement_item->id,
			'recipe_id'					=> $arrangement_item->arrangement_recipe_id,
			'name'                      => $arrangement_item->get_full_name(),
			'quantity'                  => $arrangement_item->quantity,
			'invoicing_multiplier'      => $arrangement_item->get_invoicing_multiplier(),
			'cost'                      => sc_format_price( $arrangement_item->cost ),
			'cost_and_markup'           => $cost_and_markup,
			'cost_and_markup_and_labor' => $cost_and_markup + $arrangement_item->get_labor( false ),
			'subtotal'                  => $arrangement_item->get_calculated_price(),
			'type'                      => $type,
			'type_raw'                  => str_replace( ' ', '_', strtolower( $type ) ),
			'item_type'                 => $vi['item_type'],
			'item_id'                   => $vi['item_id'],
			'item_variation_id'         => $vi['item_variation_id'],
			'item_variation_name'       => ! empty( $vi['item_variation_name'] ) ? $vi['item_variation_name'] : '',
			'item_pricing_category'     => $arrangement_item->item->pricing_category,
			'itemKeypair'               => $key,
			'attachment'				=> $item_attachment,
		);
	}

	usort( $items, 'sc_recipe_list_sort' );

	$r->add_payload( 'arrangement', array(
		'id'                    => $arrangement->id,
		'name'                  => $arrangement->name,
		'quantity'              => $arrangement->quantity,
		'order'                 => $arrangement->order,
		'note'                  => $arrangement->note,
		'include'               => $arrangement->include,
		'tax'                   => $arrangement->tax,
		'addon'					=> $arrangement->addon,
		'total'                 => $arrangement->get_final_price(false),
		'override_cost'         => $arrangement->override_cost,
		'subTotal'              => $arrangement->get_final_price(),
		'photos'                => $photos,
		'recipes'				=> $recipes,
		'items'                 => $items,
		'invoicing_category_id' => $arrangement->invoicing_category_id,
		'hardgood_multiple'     => $arrangement->hardgood_multiple,
		'fresh_flower_multiple' => $arrangement->fresh_flower_multiple,
		'global_labor_value'    => $arrangement->global_labor_value,
		'flower_labor'          => $arrangement->flower_labor,
		'hardgood_labor'        => $arrangement->hardgood_labor,
		'base_price_labor'      => $arrangement->base_price_labor,
		'fee_labor'             => $arrangement->fee_labor,
		'rental_labor'          => $arrangement->rental_labor,
		'is_percentage'         => $arrangement->is_percentage,
	) );

	$r->respond();
}
add_action( 'wp_ajax_sc/standalone_recipe/load', 'sc_ajax_load_standalone_recipe', 10/*Action priority - default is 10*/ );

function sc_ajax_standalone_recipe_import() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();
	$user_id = get_current_user_id();

	try {
		$arrangement = Arrangement::where( array(
			'id' => $request->input( 'arrangement_id' ),
			'user_id' => $user_id,
		) )->firstOrFail();
	} catch (Exception $e) {
		error_log( __LINE__ . ': ' . $e->getMessage() );
		$r->fail( 'Something went wrong and we are unable to save this arrangement to your recipes.' );
	}

	$event = sc_get_fake_event_for_user();
	$inc_recipes = $inc_items = $inc_photos = array();

	try {
		$arrangement = sc_duplicate_arrangement( $arrangement, $event, $inc_recipes, $inc_items, $inc_photos );

		$arrangement->event_id = 0;
		$arrangement->addon = 0;
		$arrangement->save();
	} catch (Exception $e) {
		error_log( __LINE__ . ': ' . $e->getMessage() );
		$r->fail( 'Something went wrong and we are unable to save this arrangement to your recipes.' );
	}

	$r->add_payload( 'recipe', sc_format_arrangement_for_user_recipes( $arrangement ) );
	$r->respond();
}
add_action( 'wp_ajax_sc/standalone_recipe/import', 'sc_ajax_standalone_recipe_import', 10 );

function sc_format_arrangement_for_user_recipes( $arrangement ) {
	return array(
		'id' => $arrangement->id,
		'name' => $arrangement->name,
		'note' => $arrangement->note,
		'quantity' => $arrangement->quantity,
		'override_cost' => $arrangement->override_cost,
		'is_percentage' => absint( $arrangement->is_percentage ),
	);
}
