<?php
use Illuminate\Http\Request;
use Stemcounter\Event;
use Stemcounter\Event_Version;
use Stemcounter\Payment;
use Stemcounter\Meta;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;

/**
* 
*/
class SC_Google {
	protected static $started = false;
	protected static $google_client = false;

	public static function start() {
		if ( ! self::$started ) {
			self::add_actions();
			self::add_filters();

			self::$started = true;
		}
	}

	protected static function add_actions() {
		add_action( 'init', array( __CLASS__, 'register_rewrite_rules' ), 10 );

		add_action( 'pre_get_posts', array( __CLASS__, 'maybe_handle_auth_callback' ), 10 );

		add_action( 'sc/studio/integration/google', array( __CLASS__, 'render' ), 10 );

		add_action( 'wp_ajax_sc_edit_user_gcalendar', array( __CLASS__, 'sc_edit_user_gcalendar' ), 10 );

		add_action( 'wp_ajax_sc/google/disconnect', array( __CLASS__, 'sc_edit_google_logout' ), 10 );

		add_action( 'sc/event/save', array( __CLASS__, 'on_event_save' ), 10 );

		add_action( 'wp_ajax_sc_payment_to_calendar', array( __CLASS__, 'sc_payment_to_calendar' ), 10 );

		add_action( 'sc/payment/save', array( __CLASS__, 'on_edit_payment' ), 10 );

		add_action( 'sc/app/footer/studio', array( __CLASS__, 'render_js' ), 10 );
	}

	protected static function add_filters() {
		add_filter( 'sc/studio/integrations', array( __CLASS__, 'register_integration' ), 10 );

		add_filter( 'query_vars', array( __CLASS__, 'add_query_vars' ), 10 );

		add_filter( 'sc/react/data/proposal', array( __CLASS__, 'filter_react_proposal_data' ), 10, 3 );
	}

	public static function register_integration( $integrations ) {
		$integrations[] = array(
			'key' => 'google',
			'label' => 'Google Calendar',
		);

		return $integrations;
	}

	public static function get_client() {
		if ( ! self::$google_client ) {
			self::$google_client = new Google_Client();
			self::$google_client->setApplicationName( 'Client_Calendar_Library' );
			self::$google_client->setDeveloperKey( SC_GOOGLE_API_KEY );  
			self::$google_client->setClientId( SC_GOOGLE_CLIENT_ID );
			self::$google_client->setClientSecret( SC_GOOGLE_CLIENT_SECRET );
			self::$google_client->setRedirectUri( home_url( '/google_auth' ) );
			self::$google_client->setAccessType( 'offline' );   // Gets us our refreshtoken
		}

		return self::$google_client;
	}

	public static function add_query_vars( $query_vars ) {
		$query_vars[] = 'sc_google_auth';

		return $query_vars;
	}

	public static function register_rewrite_rules() {
		add_rewrite_rule( '^google_auth$', array( 'sc_google_auth' => '1' ), 'top' );
	}

	public static function get_user_refresh_token( $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		return get_user_meta( $user_id, '_sc_google_refresh_token', true );
	}

	public static function set_user_refresh_token( $token, $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		return update_user_meta( $user_id, '_sc_google_refresh_token', $token );
	}

	public static function get_user_access_token( $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		$access_token = get_user_meta( $user_id, '_sc_google_access_token', true );
		if ( $access_token && ! isset( $access_token['refresh_token'] ) ) {
			$access_token['refresh_token'] = self::get_user_refresh_token( $user_id );
		}

		return $access_token;
	}

	public static function set_user_access_token( $token, $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		if ( isset( $token['refresh_token'] ) ) {
			self::set_user_refresh_token( $token['refresh_token'], $user_id );
		}

		return update_user_meta( $user_id, '_sc_google_access_token', $token );
	}

	public static function authenticate_client( $token = false ) {
		$token = $token ? $token : self::get_user_access_token();
		if ( ! $token ) {
			return false;
		}

		$client = self::get_client();
		$client->setAccessToken( $token );

		if ( $client->isAccessTokenExpired() ) {
			if ( self::get_user_refresh_token() ) {
				try {
					// reuse the same refresh token
					$client->refreshToken( self::get_user_refresh_token() );
					self::set_user_access_token( $client->getAccessToken() );
				} catch ( Exception $e ) {
					return false;
				}
			} else {
				return false;
			}
		}

		return true;
	}

	public static function maybe_handle_auth_callback( $query ) {
		if ( ! $query->get( 'sc_google_auth' ) ) {
			return;
		}

		if ( isset( $_GET['code'] ) ) {
			$client = self::get_client();
			$client->authenticate( $_GET['code'] );  
			$user_access_token = $client->getAccessToken();

			self::set_user_access_token( $user_access_token );
			if ( ! empty( $user_access_token['refresh_token'] ) ) {
				self::set_user_refresh_token( $user_access_token['refresh_token'] );
			}
		}

		wp_redirect( home_url( '/studio' ) );
		exit;
	}

	public static function revoke_token() {
		$client = self::get_client();
		$user_access_token = self::get_user_access_token();

		$client->revokeToken( $user_access_token );
		self::set_user_access_token( '' );
		self::set_user_refresh_token( '' );
	}

	public static function render() {
		$client = self::get_client();
		$client->setScopes( array( 'https://www.googleapis.com/auth/calendar.readonly' ) );
		$user_access_token = self::get_user_access_token();
		
		if ( $user_access_token ) {
			$calendar_list = self::get_calendar_list();
			$user_gcalendar_id = get_user_meta( get_current_user_id(), 'sc_user_gcalendar_id', true );

			if ( ! empty( $calendar_list ) ) {
			echo 'Select the calendar for your events and payments';
				echo '<div class="google-calendar">';
					echo '<select class="user-calendars">';
						echo '<option value=""' . selected( $user_gcalendar_id, '' ) .'>None Selected</option>';
						foreach ( $calendar_list as $calendar ) {
							echo '<option value="' . $calendar->id . '" '. selected( $user_gcalendar_id, $calendar->id ) .'>' . $calendar->summary . '</option>';
						}
					echo '</select>';
				echo '</div>';
			} else {
				echo 'You don\'t have any Google Calendars you can write into.';
			}

			echo '<a class="calendar_logout" href="' . esc_url( add_query_arg( array( 'action' => 'sc/google/disconnect' ), admin_url( 'admin-ajax.php' ) ) ) . '">Disconnect your Google account</a>';
		} else {
			$client->setScopes( array( 'https://www.googleapis.com/auth/calendar' ) );
			$authUrl = $client->createAuthUrl();
			echo "<a class='login' href='$authUrl'>Connect to Google</a>";
		}
	}

	public static function get_calendar_list() {
		if ( self::authenticate_client() ) {
			$client = self::get_client();
			$client->setScopes( array( 'https://www.googleapis.com/auth/calendar.readonly', 'https://www.googleapis.com/auth/calendar' ) );

			$service = new Google_Service_Calendar( $client );
			$calendars = $service->calendarList->listCalendarList( array(
				'minAccessRole' => 'writer',
			) );
			return $calendars;
		}

		return array();
	}

	public static function sc_edit_user_gcalendar() {
		sc_clean_input_slashes();
		$request = Request::capture();
		$r = new Ajax_Response();

		$calendar_id = $request->input('value');
		if ( ! empty( $calendar_id ) ) {
			update_user_meta( get_current_user_id(), 'sc_user_gcalendar_id', $calendar_id );
			$r->add_payload( 'message', 'Preferred calendar was set.' );
		} else {
			$r->fail( 'Failed to set user calendar!' );
		}

		$r->respond();
	}

	public static function sc_edit_google_logout() {
		sc_clean_input_slashes();
		$request = Request::capture();
		$r = new Ajax_Response();

		self::revoke_token();
		update_user_meta( get_current_user_id(), 'sc_user_gcalendar_id', '' );
		wp_redirect( home_url( '/studio' ) );
		exit;
	}

	public static function add_calendar_event( $calendar_id = false, $event_data = array() ) {
		$event_data = $event_data ? $event_data : array();
		$calendar_id = $calendar_id ? $calendar_id : false;

		if ( $calendar_id && ! empty( $event_data ) ) {
			if ( self::authenticate_client() ) {
				$client = self::get_client();
				$client->setScopes( Google_Service_Calendar::CALENDAR );
				try {
					$service = new Google_Service_Calendar( $client );
					$calendar_event = new Google_Service_Calendar_Event( $event_data );
					// Create calendar event
					return $service->events->insert( $calendar_id, $calendar_event );
				} catch ( Exception $e ) {
					/*var_dump( $e->getMessage() );*/
					throw new Humanreadable_Exception( 'Couldn\'t create your calendar event!' );
				}
			}
		} else {
			return;
		}
	}

	public static function get_calendar_event( $calendar_id = false, $event_id = false ) {
		$calendar_id = $calendar_id ? $calendar_id : false;
		$event_id = $event_id ? $event_id : false;

		if ( $calendar_id && $event_id ) {
			if ( self::authenticate_client() ) {
				$client = self::get_client();
				$client->setScopes( Google_Service_Calendar::CALENDAR );
				try {
					$service = new Google_Service_Calendar( $client );
					$calendar = $service->events->get( $calendar_id, $event_id );
					if ( 'cancelled' != $calendar->status ) {
						return $calendar;
					} else {
						return false;
					}
				} catch ( Exception $e ) {
					/*error_log( $e->getMessage() );*/
					// throw new Humanreadable_Exception( 'Couldn\'t get your calendar event!' );

					return false;
				}
			}
		} else {
			return false;
		}
	}

	public static function update_calendar_event( $calendar_id = false, $event = false ) {
		$calendar_id = $calendar_id ? $calendar_id : false;
		$event = $event ? $event : false;

		if ( $calendar_id && $event ) {
			if ( self::authenticate_client() ) {
				$client = self::get_client();
				$client->setScopes( Google_Service_Calendar::CALENDAR );
				try {
					$service = new Google_Service_Calendar( $client );
					return $service->events->update( $calendar_id, $event->getId(), $event );
				} catch ( Exception $e ) {
					/*error_log( $e->getMessage() );
					throw new Humanreadable_Exception( 'Couldn\'t update your calendar event!' );*/
					return false;
				}
			}
		} else {
			return;
		}
	}

	public static function delete_calendar_event( $calendar_id = false, $event_id = false ) {
		$calendar_id = $calendar_id ? $calendar_id : false;
		$event_id = $event_id ? $event_id : false;

		if ( $calendar_id && $event_id ) {
			if ( self::authenticate_client() ) {
				$client = self::get_client();
				$client->setScopes( Google_Service_Calendar::CALENDAR );
				try {
					$service = new Google_Service_Calendar( $client );
					return $service->events->delete( $calendar_id, $event_id );
				} catch ( Exception $e ) {
					/*var_dump( $e->getMessage() );*/
					/*throw new Humanreadable_Exception( 'Couldn\'t delete your calendar event!' );*/
					return false;
				}
			}
		} else {
			return;
		}

	}

	public static function filter_react_proposal_data( $data, $event, $user_id ) {
		if ( self::authenticate_client() ) {
			$data['extensions']['gcalendar'] = 1;
		}

		return $data;
	}

	public static function on_event_save( $event ) {
		if ( 'booked' == $event->state ) {
			$user_gcalendar_id = get_user_meta( $event->user_id , 'sc_user_gcalendar_id', true );

			if ( ! empty( $user_gcalendar_id ) ) {
				$gcalendar_event = Meta::where( array(
					'type' => 'event',
					'type_id' => $event->id,
					'meta_key' => 'gcalendar_event' 
				) )->orderBy( 'id', 'DESC' )->first();

				$gcalendar_event_data = $gcalendar_event ? json_decode( $gcalendar_event->meta_value ) : false;

				$description = strip_tags( str_ireplace( array(  '<br>', '</p>' ), array( "<br>\n", "</p>\n\n" ), $event->event_note ) );
				$description = trim( preg_replace( '~\n{2,}~', "\n\n", $description ) );

				// @ToDo add event venues if any to the calendar event description, above the event notes
				$calendar_data = array(
					'summary'		=> $event->name,
					'description'	=> $description,
					'start'	=> array(
						'date'	=> date( 'Y-m-d', strtotime( $event->date->toDateTimeString() ) ),
					),
					'end'	=> array(
						'date'	=> date( 'Y-m-d', strtotime( $event->date->toDateTimeString() ) ),
					),
					'colorId'	=> '4',
				);

				if ( ! $gcalendar_event || ( $gcalendar_event && false === ( self::get_calendar_event( $user_gcalendar_id, $gcalendar_event_data->calendar_id ) ) ) ) {
					$calendar_event = self::add_calendar_event( $user_gcalendar_id, $calendar_data );

					if ( $calendar_event ) {
						// saving the calendar event ID and the digested data for comparison later on
						$event_meta = $gcalendar_event ? $gcalendar_event : new Meta();
						$event_meta->type = 'event';
						$event_meta->type_id = $event->id;
						$event_meta->meta_key = 'gcalendar_event';
						$event_meta->meta_value = json_encode( array(
							'calendar_id'	=> $calendar_event->getId(), //id of the calendar event
							'calendar_data' => md5( json_encode( $calendar_data ) ),
						) );

						$event_meta->save();

						add_action( 'sc/event/edit/response', array( __CLASS__, 'add_event_created_message' ), 10 );
					}
				} else if ( $gcalendar_event_data->calendar_data != md5( json_encode( $calendar_data ) ) ) {
					$calendar_event = self::get_calendar_event( $user_gcalendar_id, $gcalendar_event_data->calendar_id );
					
					if ( $calendar_event ) {
						$calendar_event->setSummary( $event->name );
						$calendar_event->setDescription( $description );

						$start = new Google_Service_Calendar_EventDateTime();
						$start->setDate( date( 'Y-m-d', strtotime( $event->date->toDateTimeString() ) ) );
						$calendar_event->setStart( $start );

						$end = new Google_Service_Calendar_EventDateTime();
						$end->setDate( date( 'Y-m-d', strtotime( $event->date->toDateTimeString() ) ) );  
						$calendar_event->setEnd( $end );

						self::update_calendar_event( $user_gcalendar_id, $calendar_event );

						$gcalendar_event->meta_value = json_encode( array(
							'calendar_id'	=> $calendar_event->getId(),
							'calendar_data' => md5( json_encode( $calendar_data ) ),
						) );
						$gcalendar_event->save();

						add_action( 'sc/event/edit/response', array( __CLASS__, 'add_event_updated_message' ), 10 );
					}
				}
			}
		}
	}

	public static function sc_payment_to_calendar() {
		sc_clean_input_slashes();
		$request = Request::capture();
		$r = new Ajax_Response();

		$event_id = intval( $request->input( 'event_id' ) );
		$payment_id = intval( $request->input( 'payment_id' ) );

		try {
			$event = sc_user_can_edit_event( $event_id );
			$payment = Payment::where( array(
				'id' => $payment_id
			))->first();

			if ( ! empty( $event ) && ! empty( $payment ) ) {
				self::on_add_payment( $event, $payment );
			}
		} catch (Exception $e) {
			$r->fail( $e->getMessage() );
		}
		$r->respond();
	}

	public static function on_add_payment( $event, $payment ) {
		$user_gcalendar_id = get_user_meta( $event->user_id, 'sc_user_gcalendar_id', true );

		if ( ! empty( $user_gcalendar_id ) ) {
			$gcalendar_event = Meta::where( array(
				'type' => 'payment',
				'type_id' => $payment->id,
				'meta_key' => 'gcalendar_event' 
			) )->orderBy( 'id', 'DESC' )->first();

			$gcalendar_event_data = $gcalendar_event ? json_decode( $gcalendar_event->meta_value ) : false;
			$payment_amount = $payment->get_amount();
			$note = 'Amount Due: ' . html_entity_decode( sc_format_price( $payment_amount, true, $event->user_id ) );
			if ( $payment->payment_note ) {
				$note .= "\n\n" . $payment->payment_note;
			}

			// @ToDo - Add payment value
			$calendar_data = array(
				'summary'		=> $event->name . ' - Payment Due',
				'description'	=> $note,
				'start'	=> array(
					'date'	=> date( 'Y-m-d', strtotime( $payment->due_date ) ),
				),
				'end'	=> array(
					'date'	=> date( 'Y-m-d', strtotime( $payment->due_date ) ),
				),
				'colorId'	=> '4',
			);

			if ( ! $gcalendar_event || ( $gcalendar_event && false === ( self::get_calendar_event( $user_gcalendar_id, $gcalendar_event_data->calendar_id ) ) ) ) {
				$calendar_event = self::add_calendar_event( $user_gcalendar_id, $calendar_data );

				if ( $calendar_event ) {
					// saving the calendar event ID and the digested data for comparison later on
					$event_meta = $gcalendar_event ? $gcalendar_event : new Meta();
					$event_meta->type = 'payment';
					$event_meta->type_id = $payment->id;
					$event_meta->meta_key = 'gcalendar_event';
					$event_meta->meta_value = json_encode( array(
						'calendar_id'	=> $calendar_event->getId(), //id of the calendar event
						'calendar_data' => md5( json_encode( $calendar_data ) ),
					) );

					$event_meta->save();
				}
			}  else if ( $gcalendar_event_data->calendar_data != md5( json_encode( $calendar_data ) ) ) {
				$calendar_event = self::get_calendar_event( $user_gcalendar_id, $gcalendar_event_data->calendar_id );

				if ( $calendar_event ) {
					$calendar_event->setSummary( $calendar_data['summary'] );
					$calendar_event->setDescription( $calendar_data['description'] );

					$start = new Google_Service_Calendar_EventDateTime();
					$start->setDate( date( 'Y-m-d', strtotime( $payment->due_date ) ) );
					$calendar_event->setStart( $start );

					$end = new Google_Service_Calendar_EventDateTime();
					$end->setDate( date( 'Y-m-d', strtotime( $payment->due_date ) ) );  
					$calendar_event->setEnd( $end );

					self::update_calendar_event( $user_gcalendar_id, $calendar_event );

					$gcalendar_event->meta_value = json_encode( array(
						'calendar_id'	=> $calendar_event->getId(),
						'calendar_data' => md5( json_encode( $calendar_data ) ),
					) );
					$gcalendar_event->save();
				}
			}
		}
	}

	public static function on_edit_payment( $args ) {
		if ( self::authenticate_client() ) {
			$event = $args[0];
			$payment = $args[1];
			if ( ! empty( $event ) && ! empty( $payment ) ) {
				$gcalendar_event = Meta::where( array(
					'type' => 'payment',
					'type_id' => $payment->id,
					'meta_key' => 'gcalendar_event' 
				) )->orderBy( 'id', 'DESC' )->first();

				if ( ! empty( $gcalendar_event ) ) {
					self::on_add_payment( $event, $payment );
				} else {
					return;
				}
			}
		} else {
			return;
		}
	}

	public static function add_event_created_message( $response ) {
		$response->add_message( 'Event created in Google Calendar.' );
	}

	public static function add_event_updated_message( $response ) {
		$response->add_message( 'Event updated in Google Calendar.' );
	}

	public static function render_js() {
		?>
		<script type="text/javascript">
			(function($){
				$(document).ready(function($) {
					$('.google-calendar .user-calendars').on('change', function(event) {
						event.preventDefault();
						
						var url = stemcounter.aurl({ 'action': 'sc_edit_user_gcalendar' }),
							data = {
								value: event.target.value
							};

						$.post(url, data, function(response){
							stemcounter.JSONResponse(response, function(r){
								if ( ! r.success ) {
									console.log( r );
								} else {
									alertify.success( r.payload.message );
								}
							});
						});
					})
				});
			})(jQuery);
		</script>
		<?php	
	}
}

if ( defined( 'SC_GOOGLE_API_KEY' ) && SC_GOOGLE_API_KEY && defined( 'SC_GOOGLE_CLIENT_ID' ) && SC_GOOGLE_CLIENT_ID && defined( 'SC_GOOGLE_CLIENT_SECRET' ) && SC_GOOGLE_CLIENT_SECRET ) {
	SC_Google::start();
}

