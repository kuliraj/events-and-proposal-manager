<?php
use Stemcounter\Event;

class Stem_Counter_Report_User_Events {
	public static $done = false;

	public static function frontend_start() {
		echo '<h4>Generating User Events Report</h4>';
	}

	public static function run( $filename, $options = array() ) {
		$users_per_run = 3;

		$handle = fopen( $filename, 'a+' );
		$users = get_users( array( 'orderby' => 'id', 'order' => 'ASC' ) );
		$already_present = array();

		$has_data = false;
		while ( false !== ( $data = fgetcsv( $handle, 1000, ',' ) ) ) {
			$has_data = true;
			$already_present[] = $data[0];
		}

		foreach ( $users as $i => $user ) {
			if ( in_array( $user->ID, $already_present ) ) {
				unset( $users[ $i ] );
			}
		}

		$users = array_slice( $users, 0, $users_per_run );

		if ( ! $has_data ) {
			fputcsv( $handle, array( 'ID', 'User Email', 'Events Count', 'Events Total $', 'Currency', 'Last Event Created On' ) );
		}

		foreach ( $users as $user ) {
			$data = array(
				$user->ID,
				$user->user_email,
				0,
				0,
				sc_get_user_saved_pref_currency( $user->ID ),
				'N/A',
			);

			// echo "Exporting data for {$user->user_email}...\n";

			Event::where( 'user_id', $user->ID )
				->whereIn( 'state', array( 'inquiry', 'proposal_sent', 'booked', 'completed' ), 'and' )
				->orderBy( 'created_at', 'asc' )->with( array(
					'arrangements' => function( $query ) {
						$query->where( 'include', '=', 1 )->where( 'addon', '=', 0 )->orderBy( 'order', 'asc' );
					},
				), 'arrangements.items', 'arrangements.items.item_variation', 'arrangements.items.item_variation.item' )
				->withTrashed()
				->chunk(10, function( $events ) use ( &$data, $user ){
					$data[2] += $events->count();
					foreach ( $events as $event ) {
						$data[3] += $event->get_total( true );
						$data[5] = date( 'm/d/Y', strtotime( $event->created_at ) );
					}
				});

			fputcsv( $handle, $data );

			$data = null;
		}

		fclose( $handle );

		// Have we gone through all users? If so, then send the completed csv to the browser
		if ( count( $users ) < $users_per_run ) {
			self::$done = true;
		}
	}
}

/*if ( file_exists( $filename ) && ! empty( $_GET['dl'] ) ) {
	header( 'Content-Description: File Transfer' );
	header( 'Content-Type: application/csv' );
	header( 'Content-Disposition: attachment; filename="user-events-report.csv"' );
	header( 'Expires: 0' );
	header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
	header( 'Pragma: public' );
	header( 'Content-Length: ' . filesize( $filename ) );

	$handle = fopen( $filename, 'r');
	$contents = fread( $handle, filesize( $filename ) );
	fclose( $handle );
	echo( $contents );

	unlink( $filename );
	exit;
}*/
