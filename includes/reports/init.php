<?php

class Stem_Counter_Reports {
	protected static $reports = array();

	public static function start() {
		static $started = false;

		if ( ! $started ) {
			self::$reports = array(
				'user-events' => 'Stem_Counter_Report_User_Events',
			);

			self::maybe_handle_cli_request();

			self::add_filters();

			self::add_actions();

			$started = true;
		}
	}

	protected static function maybe_handle_cli_request() {
		if ( 'cli' != php_sapi_name() || class_exists( 'WP_CLI' ) || __FILE__ != $_SERVER['PHP_SELF'] ) {
			return;
		}

		$options = getopt( '', array( 'abspath:', 'report:', 'filename:', 'opts::' ) );

		if ( empty( $options['abspath'] ) ) {
			echo 'No abspath specified.';
			return;
		}

		if ( empty( $options['report'] ) ) {
			echo 'No report specified.';
			exit( 2 );
		}

		if ( empty( $options['filename'] ) ) {
			echo 'No filename specified.';
			exit( 3 );
		}

		sleep( 1 );

		ini_set( 'memory_limit', '512M' );

		if ( ! defined( 'ABSPATH' ) ) {
			if ( ! empty( $options['abspath'] ) ) {
				define( 'WP_USE_THEMES', false );
				
				// Try to see if what we're trying to load is in the current path
				if ( 0 === stripos( dirname( __FILE__ ), dirname( $options['abspath'] ) ) && file_exists( dirname( $options['abspath'] ) . '/app/wp-load.php' ) ) {
					$_SERVER['SERVER_NAME'] = 'dev';
					$_SERVER['HTTP_HOST'] = 'dev';
					require_once( dirname( $options['abspath'] ) . '/app/wp-load.php' );
				}
			}
		}

		if ( defined( 'ABSPATH' ) ) {
			self::includes();
		}

		$dir = self::get_dir();
		$filename = $dir . $options['filename'];
		if ( ! file_exists( $filename . '.pid' ) ) {
			// Create a lock file to indicate process is still running
			$handle = fopen( $filename . '.pid', 'w' );
			fclose( $handle );
		} else {
			$old_pid = intval( trim( file_get_contents( $filename . '.pid' ) ) );
			// proc_terminate( $old_pid );
		}

		require_once( trailingslashit( dirname( __FILE__ ) ) . $options['report'] . '.php' );

		if ( ! empty( $options['opts'] ) ) {
			$options['opts'] = wp_parse_args( $options['opts'] );
		} else {
			$options['opts'] = array();
		}

		$report_class = self::$reports[ $options['report'] ];

		$report_class::run( $filename, $options['opts'] );
		if ( ! $report_class::$done ) {
			$command = self::build_command( $options['report'], basename( $filename ), $options['opts'] );
			// error_log( $command );
			$pid = self::run( $command );
			// error_log( var_export( $pid, true ) );
			if ( $pid ) {
				file_put_contents( $filename . '.pid', $pid );
			} else {
				error_log( 'There is no $pid!' );
			}
		} else {
			// Remove the lock file - our status checker will catch that and notify the user
			unlink( $filename . '.pid' );
		}

		exit;
	}

	protected static function includes(){
		require_once( dirname( dirname( dirname( __FILE__ ) ) ) . '/functions.php' );
	}

	protected static function add_filters() {
		// Add all filters here
	}

	protected static function add_actions() {
		// Add all actions here
		add_action( 'wp_ajax_sc_run_report', array( __CLASS__, 'maybe_init_report' ), 10 );
		add_action( 'wp_ajax_sc/report/check_status', array( __CLASS__, 'check_report_status' ), 10 );
		add_action( 'wp_ajax_sc/report/download', array( __CLASS__, 'download_report' ), 10 );
		add_action( 'admin_menu', array( __CLASS__, 'register_reports_admin_menu' ) );
	}

	public static function get_dir() {
		return dirname( dirname( ABSPATH ) ) . '/reports/';
	}

	protected static function is_job_running( $filename ) {
		$filename = escapeshellarg( $filename );
		exec( "ps aux | grep '$filename'", $res );

		return 2 < count( $res );
	}

	public static function download_report() {
		if ( ! is_super_admin() ) {
			wp_send_json_error();
		}

		$dir = self::get_dir();
		$filename = $dir . $_GET['filename'];

		if ( ! file_exists( $filename ) ) {
			echo 'This report file is no longer available.';
			exit;
		}

		if ( file_exists( $filename . '.pid' ) ) {
			$pid = intval( trim( file_get_contents( $filename . '.pid' ) ) );
			if ( self::is_job_running( basename( $filename ) ) ) {
				echo 'The report is still being generated.';
				exit;
			}
		}

		$report = $_GET['report'];
		$report_class = self::$reports[ $report ];

		if ( method_exists( $report_class, 'download' ) ) {
			// If the class has it's own download method(perhaps it's file format is not CSV, or special headers are needed)
			// Call that. The class will be responsible for cleaning up the downloaded file from the file system
			// For reference check this class' download() method
			$report_class::download( $filename );
		} else {
			self::download( $report, $filename );
		}

		exit;
	}

	protected function download( $report, $filename ) {
		header( 'Content-Description: File Transfer' );
		header( 'Content-Type: application/csv' );
		header( 'Content-Disposition: attachment; filename="' . $report . '-report.csv"' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Pragma: public' );
		header( 'Content-Length: ' . filesize( $filename ) );

		$handle = fopen( $filename, 'r');
		$contents = fread( $handle, filesize( $filename ) );
		fclose( $handle );
		echo( $contents );

		unlink( $filename );
		exit;
	}

	public static function check_report_status( $retry = true ) {
		if ( ! is_super_admin() ) {
			wp_send_json_error();
		}

		$dir = self::get_dir();
		$filename = $dir . $_GET['filename'];

		if ( file_exists( $filename . '.pid' ) ) {
			$pid = intval( trim( file_get_contents( $filename . '.pid' ) ) );
			self::is_job_running( basename( $filename ) );
			if ( self::is_job_running( basename( $filename ) ) ) {
				wp_send_json_success( array( 'done' => false ) );
			} else if ( $retry ) {
				sleep( 2 );
				self::check_report_status( false );
			} else {
				wp_send_json_error( array( 'no_proc' => true, 'message' => 'The report generation process seems to have failed.' ) );
			}
		} else {
			wp_send_json_success( array( 'done' => true ) );
		}
	}

	public static function maybe_init_report() {
		if ( ! is_super_admin() ) {
			return;
		}

		if ( empty( $_GET['report'] ) ) {
			echo 'Please specify a report to run.';
			exit;
		}

		$report = $_GET['report'];

		if ( ! isset( self::$reports[ $report ] ) ) {
			echo 'There is no such report available.';
			exit;
		}

		$dir = self::get_dir();
		$filename = basename( tempnam( $dir, $report ) );
		$filename = $dir . $filename;
		$handle = fopen( $filename, 'w' );
		fclose( $handle );

		$command = self::build_command( $report, basename( $filename ) );
		$pid = self::run( $command );

		if ( $pid ) {
			file_put_contents( $filename . '.pid', $pid );
		}

		require_once( trailingslashit( dirname( __FILE__ ) ) . $report . '.php' );
		$report_class = self::$reports[ $report ];

		?>
		<!DOCTYPE html>
		<html>
		<head>
			<title>Admin Report - <?php echo self::report_label( $report ); ?></title>
			<script src="<?php echo home_url( '/app/wp-includes/js/jquery/jquery.js' ); ?>" type="text/javascript"></script>
		</head>
		<body style="cursor: wait;">
		<?php

		$report_class::frontend_start();

		self::check_status_js( $report, basename( $filename ) ); ?>
		</body>
		</html>
		<?php
		exit;
	}

	protected static function run( $command ) {
		// exec( 'bash -c "nohup ' . $command . '" > /dev/null 2>&1 & echo $!', $res );
		// exec( 'nohup ' . $command . ' > /dev/null 2>&1 & echo $! & disown', $res );
		exec( 'echo ' . $command . ' | at now -M 2>&1 & echo $!', $res );
		// exec( 'disown -h ' . intval( $res[0] ), $disown_res );

		return intval( $res[0] );
	}

	protected static function build_command( $report, $filename, $args = array() ) {
		$command = 'php ' . __FILE__;
		$command .= ' --abspath ' . escapeshellarg( ABSPATH );
		$command .= ' --report ' . escapeshellarg( $report );
		$command .= ' --filename ' . escapeshellarg( $filename );
		if ( $args ) {
			$args = is_array( $args ) ? http_build_query( $args ) : $args;
			$command .= ' --opts ' . escapeshellarg( $args );
		}

		return $command;
	}

	public static function report_label( $report ) {
		$label = str_replace( '-', ' ', $report );
		return ucwords( $label );
	}

	public static function register_reports_admin_menu() {
		add_management_page( 'Reports', 'Reports', 'manage_options', 'sc_reports', array( __CLASS__, 'admin_reports_admin_page' ) );
	}

	public static function admin_reports_admin_page() { ?>
		<div class="wrap">
			<h2>Admin Reports</h2>
			
			<?php foreach ( self::$reports as $report => $class ) :
				$label = self::report_label( $report ) . ' Report'; ?>
				<h3><a href="<?php echo esc_url( add_query_arg( array( 'action' => 'sc_run_report', 'report' => $report ), admin_url( 'admin-ajax.php' ) ) ); ?>"><?php echo $label; ?></a></h3>
			<?php endforeach ?>
		</div>
		<?php
	}

	public static function check_status_js( $report, $filename ) {
		?>
		<pre id="progress" name="progress" style="border: 1px solid #ccc; padding: 5px 10px 10px; border-radius: 3px; white-space: pre-wrap;"></pre>
		<script>
			(function($){
				var interval;

				function check() {
					$.get( <?php echo json_encode( admin_url( 'admin-ajax.php' ) ); ?>, {
						action: 'sc/report/check_status',
						cachebust: (new Date).getTime(),
						report: <?php echo json_encode( $report ) ?>,
						filename: <?php echo json_encode( $filename ) ?>,
					}, function( res ){
						if ( res.success ) {
							if ( res.data.done ) {
								progress.innerText += '\n\n=== All done! Your download will begin shortly.===';

								document.body.style.cursor = '';
								window.location = <?php echo json_encode( add_query_arg( array( 'action' => 'sc/report/download', 'report' => $report, 'filename' => $filename ), admin_url( 'admin-ajax.php' ) ) ); ?>;
							} else {
								progress.innerText += '. ';
								setTimeout( check, 10000 );
							}
						} else {
							document.body.style.cursor = '';
							progress.innerText += '\n\n\n=== ERROR: ' + res.data.message + ' ===';
						}
					}, 'json' ).always(function(){

					});
				}

				setTimeout( check, 5000 );
			})(jQuery);
		</script>
		<?php
	}
}
Stem_Counter_Reports::start();
