<?php
use Illuminate\Http\Request;
use Stemcounter\Event;
use Stemcounter\Customer;
use Stemcounter\Vendor;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Item;
use Stemcounter\Arrangement_Photo;
use Stemcounter\Invoicing_Category;
use Stemcounter\Meta;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;
use Stemcounter\Scheduled_Payment;
use Stemcounter\Made_Payment;
use Stemcounter\Tax_Rate;
use Stemcounter\Email_Template;

function sc_render_onboarding($content) {
	ob_start(); ?>
	<div class="customers-data">
		<div class="row mt">
			<div class="col-lg-12">
				<div class="content-panel form-horizontal style-form">
					<div id="onboarding"></div>
					<div class="customer-properties-sidebar container slide-out-sidebar">
						<a href="#" class="close-btn btn btn-primary button-submit">Close</a>

						<div  class="builder-form">
							<form id="edit-customer-form" class="form-horizontal style-form customer-form" method="post">
							</form>
						</div>

					</div>
				</div>
			</div>
		</div><!-- /row -->
	</div>
	<br/>
	<br/>
	<br/>
	<?php 
	$onboarding_tutorial = get_user_meta(get_current_user_id(), 'onboarding_tutorial', true);
	$onboarding_tutorial = is_array( $onboarding_tutorial ) ? $onboarding_tutorial : array();
	$user_info = wp_get_current_user();
	$step = 0;

	$i = 0;
	foreach ($onboarding_tutorial as $tutorial) {
		if ($tutorial == false) {
			$step = $i;
			break;
		}
		$i++;
	}

	$values = null;
	$sc_user_libraries = get_user_meta( get_current_user_id(), 'sc_user_libraries', true );
	$librarians = get_users( array(
		'role' => 'librarian',
	) );

	foreach ( $librarians as $librarian ) {
		$librarian->data->user_pass = 'FAKE_HASH';
	}


	if ( $step >= 0 ) {
		$values = array(
			'floralMarkup' =>  1,
			'floralCost' =>  1,
			'hardgoodMarkup' =>  1,
			'hardgoodCost' =>  1,
			'labor' =>  0,
			'tax' =>  0,
			'currency' =>  sc_get_user_saved_pref_currency(get_current_user_id()),
			'firstname' => $user_info->user_firstname,
			'lastname' => $user_info->user_lastname,
			'companyname' => $user_info->display_name,
			'librarians' => $librarians,
			'sc_user_libraries' => $sc_user_libraries,
		);
	}
	?>
	<script type="text/javascript">
	(function($) {
		$(document).ready(function() {

			$(document).trigger('stemcounter.action.renderOnboarding', {
				node: $('#onboarding'),
				steps: [{
					type: 'account',
					message: "Welcome to Stemcounter.com! Let's customize your account."
				}, {
					type: 'invoice',
					message: "Great! Your business is set up. Now let's set up your invoicing."
				}, {
					type: 'event',
					message: "Now let's create an event!"
				}],
				values: <?php echo json_encode($values); ?>,
				currentStepNumber: <?php echo json_encode($step); ?>
			});

		});
	})(jQuery);
	</script>
	<?php
	return ob_get_clean();
}
add_filter('sc/app/content/onboarding', 'sc_render_onboarding');

function sc_ajax_user_onboarding_step() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$onboarding = $request->input('onboarding');
	$user_id = get_current_user_id();

	try {
		if (!empty($onboarding['account']) && !is_null($onboarding['account'])) {
			$account = $onboarding['account'];

			if ( !empty ( $onboarding['account']['password'] ) ) {
				$errors = sc_validate_user_password( $onboarding['account']['password'], $onboarding['account']['password_repeat'] );
				if ( !empty( $errors ) ) {
					throw new Humanreadable_Exception( implode('<br>', $errors) );
				}
			}

			if (empty(trim($account['business_name']))) {
				throw new Humanreadable_Exception('Please add your Business Name.');
			}

			if (empty(trim($account['info']))) {
				throw new Humanreadable_Exception('Please tell us where did you hear about us.');
			}

			$user_data = array(
				'ID' => $user_id,
				'first_name' => $account['first_name'],
				'last_name' => $account['last_name']
			);

			if ( isset( $onboarding['account']['password'] ) ) {
				$user_data['user_pass'] = $onboarding['account']['password'];
			}

			wp_update_user( $user_data );
		
			update_user_meta(
				$user_id,
				'company_website',
				$account['business_website']
			);
			update_user_meta(
				$user_id,
				'company_name',
				$account['business_name']
			);
			update_user_meta(
				$user_id,
				'__date_format',
				$account['date_format']
			);
			update_user_meta(
				$user_id,
				'__pref_currency',
				$account['pref_currency']
			);
			update_user_meta(
				$user_id,
				'about_stemcounter_info',
				$account['info']
			);

			if ( defined( 'ONBOARDING_HEARD_FROM_ZAPIER_WEBHOOK' ) && ONBOARDING_HEARD_FROM_ZAPIER_WEBHOOK ) {
				$user = get_userdata( $user_id );
				wp_remote_post(
					ONBOARDING_HEARD_FROM_ZAPIER_WEBHOOK,
					array(
						'blocking' => false,
						'body' => array(
							'user_email' => $user->user_email,
							'heard_from' => $account['info'],
						),
					)
				);
			}

			update_user_meta( $user_id, '_sc_visible_taxes', 1 );
			update_user_meta( $user_id, 'sc_user_measuring_units', array( 'item' ) );
			update_user_meta(
				$user_id, 
				'onboarding_tutorial', 
				array(
					'step-one' => true,
					'step-two' => false,
					'step-three' => false,
				)
			);
		} elseif (!empty($onboarding['invoice']) && !is_null($onboarding['invoice'])) {
			$invoice = $onboarding['invoice'];
			if (empty(trim($invoice['floral_markup'])) && !is_numeric($invoice['floral_markup'])) {
				throw new Humanreadable_Exception('The Floral Markup field is required.');
			}		
			if (empty(trim($invoice['hardgood_markup'])) && !is_numeric($invoice['hardgood_markup'])) {
				throw new Humanreadable_Exception('The Hardgood Markup field is required.');
			}
			if (empty(trim($invoice['tax_rate'])) && !is_numeric($invoice['tax_rate'])) {
				throw new Humanreadable_Exception('The Tax Rate field is required.');
			}
			if (empty(trim($invoice['labor'])) && !is_numeric($invoice['labor'])) {
				throw new Humanreadable_Exception('Please choose what do you do when calculating labor.');
			}
			if (isset( $invoice['labor_percent'] ) && !is_null($invoice['labor_percent']) && empty(trim($invoice['labor_percent'])) && !is_numeric($invoice['labor_percent'])) {
				throw new Humanreadable_Exception('The Labor % field is required.');
			}

			$invoice_category = new Invoicing_Category();
			$invoice_category->user_id = $user_id;
			$invoice_category->name = 'Standard Markup';
			
			update_user_meta(
				$user_id,
				'hardgood_multiple',
				$invoice['hardgood_markup']
			);
			$invoice_category->hardgood_multiple = $invoice['hardgood_markup'];

			update_user_meta(
				$user_id,
				'fresh_flower_multiple',
				$invoice['floral_markup']
			);
			$invoice_category->fresh_flower_multiple = $invoice['floral_markup'];

			update_user_meta(
				$user_id,
				'__labor',
				$invoice['labor_percent']
			);

			if (!is_null($invoice['labor_percent'])) {
				$invoice_category->global_labor_value = $invoice['labor_percent'];
			} else {
				$invoice_category->global_labor_value = 0;
			}
			if ($invoice['floral_selected'] == 'on') {
				$invoice_category->flower_labor = $invoice['labor_percent'];	
			} else {
				$invoice_category->flower_labor = 0;
			}
			if ($invoice['hardgood_selected'] == 'on') {
				$invoice_category->hardgood_labor = $invoice['labor_percent'];
			} else {
				$invoice_category->hardgood_labor = 0;	
			}
			$invoice_category->base_price_labor = 0;	
			$invoice_category->fee_labor = 0;	
			$invoice_category->rental_labor = 0;	

			$invoice_category->save();
			
			update_user_meta(
				$user_id,
				'default_invoice_category_id',
				$invoice_category->id
			);

			// Create the new Tax Rate
			$rate = new Tax_Rate();
			$rate->user_id = $user_id;
			$rate->name = 'Standard Tax Rate';
			$rate->value = $invoice['tax_rate'];
			$rate->save();

			//if it's the user % labor selected
			if (!is_null($invoice['labor_percent'])) {
				if ($invoice['floral_selected'] == 'on') {
					$flower_labor = $invoice['labor_percent'];	
				} else {
					$flower_labor = 0;
				}

				if ($invoice['hardgood_selected'] == 'on') {
					$hardgood_labor = $invoice['labor_percent'];
				} else {
					$hardgood_labor = 0;
				}

				$invoice_options = array(
					'hardgood_multiple' => $invoice['hardgood_markup'],
					'fresh_flower_multiple' => $invoice['floral_markup'],
					'invoicing_category_id' => $invoice_category->id,
					'global_labor_value' => $invoice['labor_percent'],
					'flower_labor' => $flower_labor,
					'hardgood_labor' => $hardgood_labor,
					'base_price_labor' => 0,
					'fee_labor' => 0,
					'rental_labor' => 0
				);
			} else {
				$invoice_options = array(
					'hardgood_multiple' => $invoice['hardgood_markup'],
					'fresh_flower_multiple' => $invoice['floral_markup'],
					'invoicing_category_id' => $invoice_category->id,
					'global_labor_value' => 0,
					'flower_labor' => 0,
					'hardgood_labor' => 0,
					'base_price_labor' => 0,
					'fee_labor' => 0,
					'rental_labor' => 0
				);
			}

			$invoice_options['tax_rate_value'] = ! empty( $invoice['tax_rate'] ) ? $invoice['tax_rate'] : 0;
			$invoice_options['tax_rate_id'] = $rate->id;

			if ( !empty ( $invoice['sc_user_libraries'] ) ) {
				update_user_meta(
					$user_id, 
					'sc_user_libraries', 
					$invoice['sc_user_libraries']
				);
			}

			update_user_meta(
				$user_id, 
				'onboarding_tutorial', 
				array(
					'step-one' => true,
					'step-two' => true,
					'step-three' => false,
				)
			);
		} elseif (!is_null($onboarding['event']) && !empty($onboarding['event'])) {
			update_user_meta(
				$user_id, 
				'onboarding_tutorial', 
				array(
					'step-one' => true,
					'step-two' => true,
					'step-three' => true,
				)
			);
		}
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	}

	$r->respond('Success');
}
add_action( 'wp_ajax_sc_user_onboarding_step', 'sc_ajax_user_onboarding_step' );

function sc_prefill_new_user_labor_settings($user_id) {
	add_user_meta( $user_id, '_sc_add_labor_costs', isset( $_REQUEST['add_labor_costs'] ) ? 'y' : 'n' );
	add_user_meta( $user_id, '_sc_apply_labor_to', Item::TYPE_FLOWER );
	add_user_meta( $user_id, '_sc_apply_labor_to', Item::TYPE_HARDGOOD );
	add_user_meta( $user_id, '_sc_apply_labor_to', Item::TYPE_BASE_PRICE );
}
add_action( 'user_register', 'sc_prefill_new_user_labor_settings' );

function sc_preset_default_items_library( $user_id ) {
	if ( defined( 'SC_DEFAULT_ITEM_LIBRARY_ID' ) ) {
		add_user_meta( $user_id, 'sc_user_libraries', array( SC_DEFAULT_ITEM_LIBRARY_ID ) );
	}

	sc_setup_affiliate( $user_id );

	// Setup a default email template for the new user
	$company_info = get_company_info( $user_id );
	$email_template = new Email_Template();
	$email_template->user_id = $user_id;
	$email_template->label = 'Default Template';
	$email_template->subject = 'Proposal';
	if ( ! empty( $company_info['company'] ) ) {
		$email_template->content = '<p>Here is your proposal. You can reply to this email with any questions that we can answer for you.</p><p><br></p><p><br></p><p>' . $company_info['company'] .'</p>';
	} else {
		$email_template->content = '<p>Here is your proposal. You can reply to this email with any questions that we can answer for you.</p>';
	}
	$email_template->default = 1;
	$email_template->save();
}
add_action( 'user_register', 'sc_preset_default_items_library' );

function new_user_by_getdata() {
	if ( is_page_template( 'template-register.php' ) ) {
		if ( is_user_logged_in() ) { //Redirects to 'User - Events'
			wp_redirect( sc_get_events_page_url() );
			exit;
		}

		if ( isset( $_GET['email']) && isset( $_GET['firstname'] ) ) {
			$url_logon = home_url() . '/logon';

			if ( username_exists( $_GET['email'] ) ) { //Redirect existing registraions
				$url_logon = add_query_arg( 'already_registered', $_GET['email'] , $url_logon );
				wp_safe_redirect( $url_logon );
				echo 'This user already exists';
				exit;

			} else { //Try to register user
				remove_action( 'register_post', 'MeprUsersCtrl::maybe_disable_wp_registration_form', 10 );
				$generated_password = wp_generate_password();

				$user_data = array (
					'user_login' => $_GET['email'],
					'user_email' => $_GET['email'],
					'first_name' => $_GET['firstname'],
					'last_name'  => $_GET['lastname'],
					'display_name' => $_GET['companyname']
				);
				$user_data = array_map( function( $val ) {
					return urldecode( $val );
				}, $user_data );
				$user_data['user_pass'] = $generated_password;

				$registered_user = wp_insert_user($user_data);

				if ( is_wp_error( $registered_user ) ) {
					if ( 'email_exists' == $registered_user->get_error_code() ) {
						$url_logon = add_query_arg( 'already_registered', $_GET['email'] , $url_logon );
					} else {
						$url_logon = add_query_arg( 'autoregister_error', $registered_user->get_error_message() , $url_logon );
					}
					wp_safe_redirect( $url_logon );
					exit();

				} else { //Set user additional data

					$additionalInfo['companyMobile'] = $_GET['phone'];
					$serializeAdditionalInfo = serialize($additionalInfo);
					$getAdditionalInfo = get_user_meta($registered_user, 'additionalInfo', true);
					if (empty($getAdditionalInfo)) {
						add_user_meta($registered_user, 'additionalInfo', $serializeAdditionalInfo);
					} else {
						update_user_meta($registered_user, 'additionalInfo', $serializeAdditionalInfo);
					}

					$notification_email = "
						Your stemcounter acount created successfully. You can go to our login page: {$url_logon}.
						<br /><br />
						Login details follows:<br />
						login email: {$_GET['email']}<br />
						login password: {$generated_password}<br />

						Please, change your password on first login";
					wp_mail( $_GET['email'], 'Yor registration to stemcounter was successfull', $notification_email);

					//Set the memebership
					if(is_plugin_active('memberpress/memberpress.php')) {
						
						$txn = new MeprTransaction();
						$txn->user_id = $registered_user;
						$txn->product_id = 413;
						$txn->trans_num  = uniqid();
						$txn->status     = MeprTransaction::$complete_str;
						$txn->gateway    = MeprTransaction::$free_gateway_str;
						$txn->expires_at = null;
						$txn->store();
					}

					$url_logon = add_query_arg( 'new_registration', $_GET['email'] , $url_logon );
					wp_safe_redirect( $url_logon );
					exit();
				}
			}
			
		}
	}
}
add_action( 'template_redirect', 'new_user_by_getdata' );