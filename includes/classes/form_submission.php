<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form_Submission extends Model {
	protected $table = 'form_submissions';

	protected $fillable = array( 'user_id', 'form_id', 'from_ip', 'seen', 'archived', 'event_id', 'customer_id' );

	protected $hidden = array( 'updated_at', 'from_ip' );

	protected $appends = array( 'customer_name' );

	function form() {
		return $this->belongsTo( 'Stemcounter\Form' );
	}

	function answers() {
		return $this->hasMany( 'Stemcounter\Form_Submission_Answer', 'submission_id' );
	}

	function event() {
		return $this->belongsTo( 'Stemcounter\Event' );
	}

	function customer() {
		return $this->belongsTo( 'Stemcounter\Customer' );
	}

	public function getCustomerNameAttribute() {
		return $this->customer ? $this->customer->name : null;
	}

	public function getSubmittedOnAttribute() {
		return date( sc_get_user_datetime_format( $this->user_id ), strtotime( $this->created_at ) );
	}

	public static function create_from_form( $form ) {
		$submission = new self();

		$submission->user_id = $form->user_id;
		$submission->form_id = $form->id;
		$submission->from_ip = sc_get_ip_address();
		$submission->seen = 0;
		$submission->event_id = null;
		$submission->customer_id = null;

		return $submission;
	}

	public function pluck_event_internal_fields() {
		return $this->answers->where( 'property.property_type', 'event' )->where( 'property.internal_field', '!=', null )->reduce( function( $fields, $answer ) {
			$fields[ $answer->property->internal_field ] = $answer->answer;

			return $fields;
		}, array() );
	}

	public function prepare_for_js() {
		$data = array(
			'id'			=> absint( $this->id ),
			'form'			=> $this->form->name,
			'customer_id'	=> $this->customer_id ? absint( $this->customer_id ) : -1,
			'customer'		=> $this->customer_id ? ( $this->customer_name ? $this->customer_name : $this->customer->email ) : '',
			'submitted_on'	=> $this->submitted_on,
			'event_fields'	=> $this->pluck_event_internal_fields(),
		);

		return $data;
	}

	/**
	 * Attempts to create a new customer
	 * 
	 * If the submission contains a customer email internal field, a customer
	 * with all of the answers will be created(or updated if that email already exists)
	 */
	public function maybe_create_customer() {
		$customer_props = array();
		$customer_email = false;
		$customer_notes = '';
		$customer_note_changes = '';
		$customer_notes_changed = false;

		foreach ( $this->answers as $answer ) {
			if ( 'customer' != $answer->property->property_type ) {
				continue;
			}

			// We need at least the customer email to create a new customer
			if ( 'email' == $answer->property->internal_field && ! empty( $answer->answer ) ) {
				$customer_email = $answer->answer;
			} else if ( 'notes' == $answer->property->internal_field && ! empty( $answer->answer ) ) {
				$customer_notes = $answer->answer;
				$customer_notes_changed = true;
			}

			$customer_props[ $answer->property_id ] = isset( $customer_props[ $answer->property_id ] ) ? $customer_props[ $answer->property_id ] : array(
				'id'		=> $answer->property_id,
				'internal'	=> $answer->property->internal_field,
				'answers'	=> array(),
			);

			$customer_props[ $answer->property_id ]['answers'][] = $answer->answer;
		}

		if ( ! $customer_email ) {
			return null;
		}

		$customer = Customer::firstOrNew( array(
			'user_id'	=> $this->user_id,
			'email'		=> $customer_email,
		) );

		if ( $customer->exists ) {
			$customer_notes = $customer_notes ? $customer_notes : $customer->notes;
			$customer_notes_changed = $customer_notes_changed ? $customer->notes : false;
		} else {
			// Save the customer now so we can access the id
			$customer->first_name = '';
			$customer->last_name = '';
			$customer->phone = '';
			$customer->address = '';
			$customer->notes = '';
			$customer->save();
		}

		foreach ( $customer_props as $prop ) {
			// We need at least the customer email to create a new customer
			if ( in_array( $prop['internal'], array( 'customer_email', 'notes' ) ) ) {
				continue;
			}

			if ( ! empty( $prop['internal'] ) ) {
				$internal = $prop['internal'];
				$value = array_shift( $prop['answers'] );

				// Preserve any changes in the notes
				if ( trim( $customer->$internal ) && $customer->$internal != $value ) {
					$customer_note_changes .= "\n" . Customer::$column_labels[ $internal ] . ": {$customer->$internal}";
				}

				$customer->$internal = $value;
			} else {
				$properties = Customer_Property::where( array(
					'customer_id' => $customer->id,
					'property_id' => $prop['id'],
				) )->get();

				// Create any extra customer properties that we need
				if ( $properties->count() < count( $prop['answers'] ) ) {
					for ( $i = $properties->count(); $i < count( $prop['answers'] ); $i ++ ) {
						$properties->push(
							new Customer_Property( array(
								'type'			=> 'customer',
								'customer_id'	=> $customer->id,
								'property_id'	=> $prop['id'],
								'value'			=> '',
							) )
						);
					}
				}

				foreach ( $properties as $i => $_prop ) {
					if ( isset( $prop['answers'][ $i ] ) ) {
						$_prop->value = $prop['answers'][ $i ];
						$_prop->save();
					} else {
						$_prop->delete();
					}
				}
			}
		}

		if ( trim( $customer_notes_changed ) ) {
			$customer_note_changes .= "\nNotes: \n{$customer_notes_changed}";
		}

		if ( $customer_note_changes != '' ) {
			$customer_notes .= "\n\n--------\nData from before overriding customer information through form submission on {$this->submitted_on}:" . $customer_note_changes;
		}

		$customer->notes = $customer_notes;
		// Save any final changes to the customer
		$customer->save();

		$this->customer_id = $customer->id;
		$this->save();

		return $customer->id;
	}

	/**
	 * Returns the notification email for this form
	 * 
	 * Eventually we might have the option to send one or more notifications
	 * to a static/dynamic email and the logic for doing so would be handled here
	 */
	public function get_notification_email() {
		$user = get_userdata( $this->user_id );

		return $user->user_email;
	}
}
