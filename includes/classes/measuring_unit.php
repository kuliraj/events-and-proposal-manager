<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;

class Measuring_Unit extends Model {
	protected $table = 'measuring_units';

	public $incrementing = false;

	protected $fillable = array('id', 'singular', 'plural', 'short', 'short_plural' );

	function n( $number, $short = false ) {
		if ( 1 == absint( $number ) ) {
			return $short ? $this->short : $this->singular;
		} else {
			return $short ? $this->short_plural : $this->plural;
		}
	}
}