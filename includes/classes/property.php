<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;

class Property extends Model {
	protected $table;

	protected $fillable = array( 'property_id', 'value' );

	protected $hidden = array( 'updated_at', 'created_at' );

	protected $type;

	public function __construct( array $attributes = array() ) {
		$this->table = $this->type . '_properties';
		array_unshift( $this->fillable, $this->type . '_id' );

		parent::__construct( $attributes );
	}

	function property() {
		return $this->belongsTo( 'Stemcounter\Field_Property', 'property_id' );
	}
}