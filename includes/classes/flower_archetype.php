<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flower_Archetype extends Model {
    use SoftDeletes;

    protected $fillable = array('name', 'stems', 'cost');

    protected $dates = ['deleted_at'];

    function variations() {
    	return $this->belongsToMany('Stemcounter\Variation', 'flower_archetype_variations', 'archetype_id', 'variation_id');
    }
}