<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Stemcounter\Property;

class Customer_Property extends Property {
	public function __construct( array $attributes = array() ) {
		$this->type = 'customer';

		parent::__construct( $attributes );
	}

	function customer() {
		return $this->belongsTo( 'Stemcounter\Customer' );
	}
}