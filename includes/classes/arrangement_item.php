<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Event;

class Arrangement_Item extends Model {
    use SoftDeletes;

    protected $table = 'arrangement_items';

    protected $fillable = array('cost', 'quantity');

    protected $dates = ['deleted_at'];

    function arrangement() {
        return $this->belongsTo('Stemcounter\Arrangement');
    }

    function recipe() {
        return $this->belongsTo('Stemcounter\Arrangement_Recipe');
    }

    function item() {
        return $this->belongsTo( 'Stemcounter\Item' )->withTrashed();
    }

    function item_variation() {
        return $this->belongsTo( 'Stemcounter\Item_Variation', 'item_variation_id' )->withTrashed();
    }

    // delete an arrangement item whenever the related flower or service is deleted
    // static function onItemDeleted($model) {
    //     $model->arrangement_items()->delete();
    // }

    function get_full_name() {
        $name = $this->item->name;
        //if the item is deleted but still in the arrangement
        if ( is_null( $name ) ) {
            try {
                $item = Item::where( array(
                    'id' => $this->item_id
                ) )->withTrashed()->firstOrFail()->toArray();
                $name = $item['name'];
            } catch (\Exception $e) {
                $name = '';                
            }
        }

        if ( $this->item_variation && 'Default' != $this->item_variation->name ) {
            $name .= ' - ' . $this->item_variation->name;
        }
        
        return $name;
    }

    function get_invoicing_multiplier($options = array()) {
        $type = sc_get_item_type($this->item, false);
        $multiplier = 1;

        switch ($type) {
            case Item::TYPE_FLOWER:
                if (!empty($options) && isset($options['fresh_flower_multiple'])) {
                    $multiplier = $options['fresh_flower_multiple'];  
                } else if (isset($this->arrangement->fresh_flower_multiple) && !is_null($this->arrangement->fresh_flower_multiple)) {
                    $multiplier = $this->arrangement->fresh_flower_multiple;  
                } else if (isset($this->arrangement) && $this->arrangement->event->exists() && isset($this->arrangement->event->fresh_flower_multiple)) {
                    $multiplier = $this->arrangement->event->fresh_flower_multiple;  
                }
                break;
            case Item::TYPE_HARDGOOD:
                if (!empty($options) && isset($options['hardgood_multiple'])) {
                    $multiplier = $options['hardgood_multiple'];  
                } else if (isset($this->arrangement->hardgood_multiple) && !is_null($this->arrangement->hardgood_multiple)) {
                    $multiplier = $this->arrangement->hardgood_multiple;  
                } else if (isset($this->arrangement) && $this->arrangement->event->exists() && isset($this->arrangement->event->hardgood_multiple)) {
                    $multiplier = $this->arrangement->event->hardgood_multiple;  
                }
                break;
        }

        return $multiplier;
    }

    function get_labor_percent($options = array()) {
        $type = sc_get_item_type($this->item, false);
        $percent = 0;

        switch ($type) {
            case Item::TYPE_FLOWER:
                if (!empty($options) && isset($options['flower_labor'])) {
                    $percent = $options['flower_labor'];    
                } else if (isset($this->arrangement->flower_labor) && !is_null($this->arrangement->flower_labor)) {
                    $percent = $this->arrangement->flower_labor;    
                } else if (isset($this->arrangement) && $this->arrangement->event->exists() && isset($this->arrangement->event->flower_labor)) {
                    $percent = $this->arrangement->event->flower_labor;    
                }
                break;
            case Item::TYPE_HARDGOOD:
                if (!empty($options) && isset($options['hardgood_labor'])) {
                    $percent = $options['hardgood_labor'];    
                } else if (isset($this->arrangement->hardgood_labor) && !is_null($this->arrangement->hardgood_labor)) {
                    $percent = $this->arrangement->hardgood_labor;    
                } else if (isset($this->arrangement) && $this->arrangement->event->exists() && isset($this->arrangement->event->hardgood_labor)) {
                    $percent = $this->arrangement->event->hardgood_labor;    
                }
                break;
            case Item::TYPE_BASE_PRICE:
                if (!empty($options) && isset($options['base_price_labor'])) {
                    $percent = $options['base_price_labor'];    
                } else if (isset($this->arrangement->base_price_labor) && !is_null($this->arrangement->base_price_labor)) {
                    $percent = $this->arrangement->base_price_labor;    
                } else if (isset($this->arrangement) && $this->arrangement->event->exists() && isset($this->arrangement->event->base_price_labor)) {
                    $percent = $this->arrangement->event->base_price_labor;    
                }
                break;
            case Item::TYPE_FEE:
                if (!empty($options) && isset($options['fee_labor'])) {
                    $percent = $options['fee_labor'];    
                } else if (isset($this->arrangement->fee_labor) && !is_null($this->arrangement->fee_labor)) {
                    $percent = $this->arrangement->fee_labor;    
                } else if (isset($this->arrangement) && $this->arrangement->event->exists() && isset($this->arrangement->event->fee_labor)) {
                    $percent = $this->arrangement->event->fee_labor;    
                }
                break;
            case Item::TYPE_RENTAL:
                if (!empty($options) && isset($options['rental_labor'])) {
                    $percent = $options['rental_labor'];    
                } else if (isset($this->arrangement->rental_labor) && !is_null($this->arrangement->rental_labor)) {
                    $percent = $this->arrangement->rental_labor;   
                } else if (isset($this->arrangement) && $this->arrangement->event->exists() && isset($this->arrangement->event->rental_labor)) {
                    $percent = $this->arrangement->event->rental_labor;  
                }
                break;
        }

        return $percent;
    }

    function get_cost_total() {
        $total = price_format($this->cost) * price_format($this->quantity);
        return price_format( $total );
    }

    function get_markup($include_quantity = true, $options = array()) {
        $item_price = $include_quantity ? $this->get_cost_total() : $this->cost;
        $multiplier = $this->get_invoicing_multiplier($options);
        $total = $item_price * $multiplier - $item_price;
        return price_format($total);
    }

    function get_labor($include_quantity = true, $options = array()) {
        $invoice_settings = sc_get_profile_settings();
        $type = sc_get_item_type($this->item, false);
        $total = 0;
        
        $item_price = $include_quantity ? $this->get_cost_total() : $this->cost;
        $item_price += $this->get_markup($include_quantity, $options);
        $total = $item_price * ($this->get_labor_percent($options) / 100);
        
        return price_format($total);
    }

    function get_calculated_price($options = array() ) {

        $item_price = ( $this->get_cost_total() * $this->get_invoicing_multiplier($options) ) * ( 1 + price_format( $this->get_labor_percent($options) ) / 100 );
        
        // $item_price = $this->get_cost_total();
        // $item_price += $this->get_markup(true, $options);
        // $item_price += $this->get_labor(true, $options);

        return price_format($item_price);
    }
}