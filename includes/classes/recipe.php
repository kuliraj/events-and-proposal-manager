<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipe extends Model {
    use SoftDeletes;

    protected $table = 'recipes';

    protected $fillable = array('user_id', 'name', 'description');

    function items() {
        return $this->hasMany('Stemcounter\Recipe_Item');
    }

    function item() {
        return $this->morphTo();
    }

    // delete an arrangement photos whenever the related flower or service is deleted
    /*static function onItemDeleted($model) {
        $model->recipes()->delete();
    }*/
}