<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model {
    use SoftDeletes;

    protected $table = 'notifications';

    protected $fillable = array( 'user_id', 'notification', 'read', 'type' );

    protected $dates = ['deleted_at'];
}