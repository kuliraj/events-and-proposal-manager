<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Payment extends Model {

	protected $table = 'payments';

	protected $fillable = array('user_id', 'event_id', 'due_date', 'payment_percentage', 'payment_date', 'amount', 'payment_note', 'status', 'payment_type' );

	function event() {
		return $this->belongsTo('Stemcounter\Event');
	}

	protected static function boot() {
		parent::boot();

		static::addGlobalScope( 'order', function( Builder $builder ) {
			$builder->orderBy( 'due_date', 'asc' )->orderBy( 'status', 'asc' )->orderBy( 'id', 'asc' );
		} );
	}

	function event_version() {
		return $this->hasOne('Stemcounter\Event_Version', 'event_id', 'event_id')->orderBy( 'id', 'desc' );
	}

	function get_amount() {
		$payment_amount = 0;

		$event = $this->event()->with( 'payments' )->first();
		$payments = $event->payments;
		$max_index = count( $payments ) - 1;

		foreach ( $payments as $i => $payment ) {
			if ( $payment->id == $this->id ) {
				$found_payment = true;

				$event_total = $event->get_total_with_cc_rate( true );
				
				if ( $i == $max_index ) {
					$scheduledPrice = 0;
					$final = 0;
					$cc_value = 0;
					$type;

					foreach ( $payments as $ii => $pmt ) {
						if ( $ii < $max_index ) {
							if ( 'not-paid' == $pmt->status && ! is_null( $pmt->payment_percentage ) ) {
								$scheduledPrice += floatval( sc_format_price( $event_total * ( $pmt->payment_percentage / 100 ) ) );
							} else {
								$scheduledPrice += floatval( sc_format_price( $pmt->amount ) );
							}
						}
					}

					$payment_amount = $event_total - $scheduledPrice;
					$payment_amount = floatval( sc_format_price( $payment_amount ) );
				} elseif ( ! empty( $payment->payment_percentage ) && 'complete' != $payment->status ) {
					$payment_amount = $event_total * ( $payment->payment_percentage / 100 );
				} else {
					$payment_amount = $payment->amount;
				}

				$payment_amount = sc_format_price( $payment_amount );
			}
		}

		return $payment_amount;
	}
}