<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model {
	use SoftDeletes;

	protected $table = 'orders';

	protected $fillable = array('user_id', 'event_ids', 'name', 'note', 'status', 'estimate', 'fulfilment_date');

	protected $dates = ['deleted_at', 'fulfilment_date'];

	public function items() {
		return $this->hasMany('Stemcounter\Order_Item');
	}
}