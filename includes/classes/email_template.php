<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;

class Email_Template extends Model {
	protected $table = 'email_templates';

	protected $fillable = array( 'user_id', 'label', 'subject', 'content', 'default' );
}