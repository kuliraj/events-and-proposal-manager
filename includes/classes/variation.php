<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Variation extends Model {
    use SoftDeletes;

    protected $fillable = array('name');

    protected $dates = ['deleted_at'];

    function archetypes() {
    	return $this->belongsToMany('Stemcounter\Flower_Archetype', 'flower_archetype_variations', 'variation_id', 'archetype_id');
    }

    function flowers() {
    	return $this->belongsToMany('Stemcounter\Flower', 'flower_variations', 'variation_id', 'flower_id');
    }
}