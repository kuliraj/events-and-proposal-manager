<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order_Item extends Model {
	use SoftDeletes;

	protected $table = 'order_items';

	protected $fillable = array('order_id', 'event_id', 'item_id', 'name', 'pricing_category', 'quantity', 'cost', 'items_su', 'su_needed', 'leftovers', 'discarded');

	protected $dates = ['deleted_at'];

	public function order() {
		return $this->belongsTo('Stemcounter\Order');
	}

	public function variation() {
		return $this->belongsTo('Stemcounter\Item_Variation', 'item_id');
	}
}