<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event_Version extends Model {
    use SoftDeletes;

    protected $table = 'event_versions';

    protected $fillable = array('event_id', 'version', 'event_data');

    protected $dates = ['deleted_at'];
}