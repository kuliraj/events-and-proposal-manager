<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Recipe;

class Recipe_Item extends Model {
    use SoftDeletes;

    protected $table = 'recipe_items';

    protected $fillable = array('quantity');

    protected $dates = ['deleted_at'];

    function recipe() {
        return $this->belongsTo('Stemcounter\Recipe');
    }

    function item() {
        return $this->belongsTo( 'Stemcounter\Item' );
    }

    function item_variation() {
        return $this->belongsTo( 'Stemcounter\Item_Variation', 'item_variation_id' );
    }

    static function onItemDeleted($model) {
        $model->recipe_items()->delete();
    }

    function get_full_name() {
        $name = $this->item->name;
        //if the item is deleted but still in the arrangement
        if ( is_null( $name ) ) {
            try {
                $item = Item::where( array(
                    'id' => $this->item_id
                ) )->withTrashed()->firstOrFail()->toArray();

                $name = $item['name'];
            } catch ( Exception $e ) {
                $name = '';                
            }
        }

        if ( $this->item_variation ) {
            $name .= ' - ' . $this->item_variation->name;
        }

        return $name;
    }
}