<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Field_Property extends Model {
	protected $table = 'field_properties';

	protected $fillable = array( 'user_id', 'label', 'placeholder_tag', 'field_type', 'property_type', 'internal_field', 'settings', 'deleted' );

	protected $casts = [
		'settings' => 'object',
	];

	function questions() {
		return $this->hasMany( 'Stemcounter\Form_Question', 'property_id' );
	}

	function customer_properties() {
		return $this->hasMany( 'Stemcounter\Customer_Property', 'property_id' );
	}

	function event_properties() {
		return $this->hasMany( 'Stemcounter\Event_Property', 'property_id' );
	}

	public function sanitize_answer( $answer ) {
		switch ( $this->field_type ) {
			case 'multi-checkbox':
			case 'radio':
				if ( isset( $this->settings->aopt[ $answer ] ) ) {
					return $answer;
				} else {
					return '';
				}
			break;

			case 'checkbox':
				if ( ! empty( $answer ) ) {
					return 1;
				} else {
					return '';
				}
			break;

			case 'textarea':
				return sanitize_textarea_field( $answer );
			break;

			case 'input':
				switch ( $this->settings->format ) {
					case 'string':
					default:
						return sanitize_text_field( $answer );
					break;

					case 'number':
						return floatval( $answer );
					break;
				}
			break;
		}

		return sanitize_text_field( $answer );
	}

	public function validate( $value ) {
		return true;
	}

	public function format_for_display( $value ) {
		switch ( $this->field_type ) {
			case 'multi-checkbox':
			case 'radio':
				if ( ! empty( $this->settings->aopt[ $value ] ) ) {
					return $this->settings->aopt[ $value ];
				} else {
					return '';
				}
			break;

			case 'checkbox':
				if ( ! empty( $value ) ) {
					return 'Yes';
				} else {
					return 'No';
				}
			break;

			case 'input':
				switch ( $this->settings->format ) {
					case 'string':
					default:
						return $value;
					break;

					case 'number':
						return floatval( $value );
					break;
				}
			break;

			default:
				return $value;
			break;
		}
	}

	public static function prepare_collection_for_js( $collection, $value_key ) {
		$collection = $collection->map( function( $model ) use ( $value_key ) {
			$model->values = $model->$value_key->toArray();
			unset( $model->$value_key );

			return $model;
		} );

		$collection = $collection->toArray();

		return $collection;
	}

	public static function maybe_create_internal_properties() {
		$user_id = get_current_user_id();

		// If user is not logged-in, already has the internal properties, or can't access Studio features
		if ( ! $user_id || get_user_meta( $user_id, '_sc_has_internal_properties', true ) || ! \SCAC::can_access( \SCAC::STUDIO ) ) {
			return;
		}

		$internal_props = array(
			array(
				'user_id'			=> $user_id,
				'label'				=> 'First Name',
				'placeholder_tag'	=> 'first_name',
				'field_type'		=> 'input',
				'property_type'		=> 'customer',
				'internal_field'	=> 'first_name',
				'settings'			=> array(
					'default'		=> false,
					'description'	=> '',
					'format'		=> 'string',
				),
			),
			array(
				'user_id'			=> $user_id,
				'label'				=> 'Last Name',
				'placeholder_tag'	=> 'last_name',
				'field_type'		=> 'input',
				'property_type'		=> 'customer',
				'internal_field'	=> 'last_name',
				'settings'			=> array(
					'default'		=> false,
					'description'	=> '',
					'format'		=> 'string',
				),
			),
			array(
				'user_id'			=> $user_id,
				'label'				=> 'Email',
				'placeholder_tag'	=> 'email',
				'field_type'		=> 'input',
				'property_type'		=> 'customer',
				'internal_field'	=> 'email',
				'settings'			=> array(
					'default'		=> false,
					'description'	=> '',
					'format'		=> 'string',
				),
			),
			array(
				'user_id'			=> $user_id,
				'label'				=> 'Phone number',
				'placeholder_tag'	=> 'phone_number',
				'field_type'		=> 'input',
				'property_type'		=> 'customer',
				'internal_field'	=> 'phone',
				'settings'			=> array(
					'default'		=> false,
					'description'	=> '',
					'format'		=> 'string',
				),
			),
			array(
				'user_id'			=> $user_id,
				'label'				=> 'Address',
				'placeholder_tag'	=> 'address',
				'field_type'		=> 'input',
				'property_type'		=> 'customer',
				'internal_field'	=> 'address',
				'settings'			=> array(
					'default'		=> false,
					'description'	=> '',
					'format'		=> 'string',
				),
			),
			array(
				'user_id'			=> $user_id,
				'label'				=> 'Notes',
				'placeholder_tag'	=> 'customer_notes',
				'field_type'		=> 'input',
				'property_type'		=> 'customer',
				'internal_field'	=> 'notes',
				'settings'			=> array(
					'default'		=> false,
					'description'	=> '',
					'format'		=> 'string',
				),
			),
			array(
				'user_id'			=> $user_id,
				'label'				=> 'Name',
				'placeholder_tag'	=> 'event_name',
				'field_type'		=> 'input',
				'property_type'		=> 'event',
				'internal_field'	=> 'name',
				'settings'			=> array(
					'default'		=> false,
					'description'	=> '',
					'format'		=> 'string',
				),
			),
			array(
				'user_id'			=> $user_id,
				'label'				=> 'Date',
				'placeholder_tag'	=> 'event_date',
				'field_type'		=> 'input',
				'property_type'		=> 'event',
				'internal_field'	=> 'date',
				'settings'			=> array(
					'default'		=> false,
					'description'	=> '',
					'format'		=> 'date',
				),
			),
		);

		foreach ( $internal_props as $prop ) {
			Field_Property::create( $prop );
		}

		update_user_meta( $user_id, '_sc_has_internal_properties', 1 );
	}
}
