<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form_Question extends Model {
	protected $table = 'form_questions';

	protected $fillable = array( 'form_id', 'property_id', 'label', 'type', 'required', 'order' );

	protected $hidden = array( 'created_at', 'updated_at' );

	function form() {
		return $this->belongsTo('Stemcounter\Form');
	}

	function property() {
		return $this->belongsTo( 'Stemcounter\Field_Property', 'property_id' );
	}

	public function sanitize_answer( $answer ) {
		return $this->property->sanitize_answer( $answer );
	}

	function validate_answer( $answer ) {
		$answer = trim( $answer );
		if ( $this->required && empty( $answer ) ) {
			return new \WP_Error( 'required', sprintf( 'The %s field is required.', $this->label ) );
		}

		return $this->property->validate( $answer );
	}

	function format_answer( $answer ) {
		return $this->property->format_for_display( $answer );
	}
}
