<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model {
	use SoftDeletes;

	protected $fillable = array( 'user_id', 'first_name', 'last_name', 'phone', 'email', 'address', 'notes' );

	protected $dates = ['deleted_at'];

	public static $column_labels = array(
		'first_name'	=> 'First Name',
		'last_name'		=> 'Last Name',
		'phone'			=> 'Phone',
		'email'			=> 'Email',
		'address'		=> 'Address',
		'notes'			=> 'Notes',
	);

	public function getNameAttribute() {
		return trim( $this->first_name . ' ' . $this->last_name );
	}

	public function format_for_event() {
		return array(
			'id'      => $this->id,
			'name'    => $this->name,
			'address' => $this->address,
			'email'   => $this->email,
			'phone'   => $this->phone,
		);
	}
}