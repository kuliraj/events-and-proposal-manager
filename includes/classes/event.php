<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model {
	use SoftDeletes;

	protected $fillable = array('name', 'note');

	protected $dates = ['deleted_at', 'date'];

	const STATUS_PUBLISHED = 'published';
	const STATUS_ARCHIVED = 'archived';
	static $statuses = array(self::STATUS_PUBLISHED, self::STATUS_ARCHIVED);

	function customer() {
		return $this->belongsTo('Stemcounter\Customer');
	}

	function vendor() {
		return $this->belongsTo('Stemcounter\Vendor');
	}

	function arrangements() {
		return $this->hasMany('Stemcounter\Arrangement');
	}

	function payments() {
		return $this->hasMany('Stemcounter\Payment')->orderBy( 'due_date', 'ASC' )->orderBy( 'due_date', 'asc' )->orderBy( 'status', 'asc' )->where( 'is_live', '=', '0' );
	}

	function live_payments() {
		return $this->hasMany('Stemcounter\Payment')->orderBy( 'due_date', 'ASC' )->orderBy( 'due_date', 'asc' )->orderBy( 'status', 'asc' )->where( 'is_live', '=', '1' );
	}

	function all_payments() {
		return $this->hasMany('Stemcounter\Payment')->orderBy( 'due_date', 'ASC' )->orderBy( 'due_date', 'asc' )->orderBy( 'status', 'asc' );
	}

	function tax_rate() {
		return $this->belongsTo('Stemcounter\Tax_Rate');
	}

	function tax_rate2() {
		return $this->belongsTo('Stemcounter\Tax_Rate', 'tax_rate_id2');
	}

	function discount() {
		return $this->belongsTo('Stemcounter\Discount');
	}

	function latest_version() {
		return $this->hasOne('Stemcounter\Event_Version')->orderBy( 'id', 'DES' );
	}

	function latest_version_signed() {
		// var_dump( $this->hasOne('Stemcounter\Event_Version')->orderBy( 'id', 'DES' ) );
		//var_dump( 'latest_version_signed()' );
		// error_log( var_export( $this->hasOne('Stemcounter\Event_Version')->orderBy( 'id', 'DESC' ), true ) );
		//error_log( var_export( $this->hasOne('Stemcounter\Event_Version')->orderBy( 'id', 'DESC' )->select( array( 'client_signed' ) ), true ) );
		//error_log( var_export( $this->hasOne('Stemcounter\Event_Version')->orderBy( 'version', 'DESC' )->select( array( 'client_signed' ) ), true ) );

		//return $this->hasMany('Stemcounter\Event_Version')->orderBy( 'version', 'DESC' );
		//return $this->hasMany('Stemcounter\Event_Version')->orderBy( 'version', 'DESC' )->select( array( 'client_signed' ) ) ;
		return $this->hasOne('Stemcounter\Event_Version')->orderBy( 'version', 'DESC' )->pluck('client_signed')->first();
	}

	function get_subtotal( $proposal_only = false ) {
		$subtotal = 0;
		foreach ( $this->arrangements as $arrangement ) {
			if ( $proposal_only && ( ! $arrangement->include || $arrangement->addon_enabled() ) ) {
				continue;
			}
			$subtotal += $arrangement->get_final_price();
		}

		return $subtotal;
	}

	function get_tax( $proposal_only = false ) {
		$total = 0;
		$taxable_total = 0;
		
		foreach ($this->arrangements as $arrangement) {
			if ( $proposal_only && ( ! $arrangement->include || $arrangement->addon_enabled() ) ) {
				continue;
			}

			if ( $arrangement->tax_enabled() ) {
				$taxable_total += $arrangement->get_final_price();
			}
		}

		$taxable_total = 0 > $taxable_total ? 0 : $taxable_total;
		$total = price_format( $taxable_total ) * ( price_format( $this->tax_rate_value, 3 ) / 100 );
		
		return price_format( $total );
	}

	function get_tax2( $proposal_only = false ) {
		if ( empty( $this->tax_rate_value2 ) ) {
			return price_format( 0 );
		}

		$total = 0;
		$taxable_total = 0;
		
		foreach ($this->arrangements as $arrangement) {
			if ( $proposal_only && ( ! $arrangement->include || $arrangement->addon_enabled() ) ) {
				continue;
			}

			if ( $arrangement->tax_enabled() ) {
				$taxable_total += $arrangement->get_final_price();
			}
		}

		$taxable_total = 0 > $taxable_total ? 0 : $taxable_total;
		$total = price_format( $taxable_total ) * ( price_format( $this->tax_rate_value2, 3 ) / 100 );
		
		return price_format( $total );
	}

	function get_total( $proposal_only = false ) {
		$total = $this->get_subtotal( $proposal_only ) + $this->get_tax( $proposal_only ) + $this->get_tax2( $proposal_only );

		return price_format($total);
	}

	function get_total_with_cc_rate( $proposal_only = false ) {
		$total = $this->get_total( $proposal_only );
		if ( $this->separate_cc_fee ) {
			$total += sc_format_price( ( $total * sc_format_price( $this->card ) ) / 100 );
		}
 
		return $total;
	}

	function get_delivery( $proposal_only = false ) {
		$total = 0;
		foreach ($this->arrangements as $arrangement) {
			if ( $proposal_only && ( ! $arrangement->include || $arrangement->addon_enabled() ) ) {
				continue;
			}
			if ( 'percentage' == $this->delivery_type ) {
				$total += $arrangement->get_delivery();
			} else {
				$total = $arrangement->get_delivery();
			}
		}
		return price_format($total);
	}

	function get_discount( $proposal_only = false ) {
		$delivery = 0 < $this->get_delivery( $proposal_only ) ? $this->get_delivery( $proposal_only ) : 0;
		if ( 'percentage' == $this->discount_type ) {
			$subtotal = 0;
			
			foreach ( $this->arrangements as $arrangement ) {
				if ( $proposal_only && ( ! $arrangement->include || $arrangement->addon_enabled() ) ) {
					continue;
				}
				$subtotal += $arrangement->get_final_price();
			}
			$subtotal = $subtotal + $delivery;
			$subtotal = price_format( $subtotal );
			return price_format( $subtotal * ( price_format( $this->discount_value ) / 100 ) );
		} else {
			return price_format( $this->discount_value );
		}
	}
}
