<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model {
    use SoftDeletes;

    protected $fillable = array('name', 'description', 'cost', 'inventory');

    protected $dates = ['deleted_at'];

    const TYPE_HARDGOOD = 'hardgood';
    const TYPE_RENTAL = 'rental';
    const TYPE_BASE_PRICE = 'base_price';
    const TYPE_FEE = 'fee';
    public static $types = array(
        self::TYPE_HARDGOOD,
        self::TYPE_BASE_PRICE,
        self::TYPE_FEE,
        self::TYPE_RENTAL,
    );

    function arrangement_items() {
        return $this->morphMany('Stemcounter\Arrangement_Item', 'item');
    }

    function recipe_items() {
        return $this->morphMany('Stemcounter\Recipe_Item', 'item');
    }
}