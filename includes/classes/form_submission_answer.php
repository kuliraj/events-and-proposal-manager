<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Form_Submission_Answer extends Model {
	protected $table = 'form_submission_answers';

	protected $fillable = array( 'user_id', 'form_id', 'submission_id', 'question_id', 'property_id', 'answer' );

	protected $hidden = array( 'created_at', 'updated_at' );

	protected $appends = array( 'label' );

	function submission() {
		return $this->belongsTo( 'Stemcounter\Form_Submission', 'submission_id' );
	}

	function question() {
		return $this->belongsTo( 'Stemcounter\Form_Question', 'question_id' );
	}

	function property() {
		return $this->belongsTo( 'Stemcounter\Field_Property', 'property_id' );
	}

	function getLabelAttribute() {
		if ( $this->question ) {
			return $this->question->label;
		} else if ( $this->property ) {
			return $this->property->label;
		} else {
			return 'N/A';
		}
	}
}
