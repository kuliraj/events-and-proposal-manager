<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flower extends Model {
    use SoftDeletes;

    protected $fillable = array('name', 'cost', 'stems');

    protected $dates = ['deleted_at'];

    const TYPE_FLOWER = 'flower';
    
    function arrangement_items() {
        return $this->morphMany('Stemcounter\Arrangement_Item', 'item');
    }

    function recipe_items() {
        return $this->morphMany('Stemcounter\Recipe_Item', 'item');
    }

    function variations( $withTrashed = false ) {
        if ( ! $withTrashed ) {
    	   return $this->belongsToMany('Stemcounter\Variation', 'flower_variations', 'flower_id', 'variation_id'); 
        } else {
            return $this->belongsToMany('Stemcounter\Variation', 'flower_variations', 'flower_id', 'variation_id')->withTrashed(); 
        }
    }
}