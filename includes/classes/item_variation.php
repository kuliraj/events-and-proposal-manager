<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item_Variation extends Model {
    use SoftDeletes;

    protected $table = 'item_variations';

    protected $fillable = array('name', 'attachment_id', 'cost_pu', 'inventory');

    protected $dates = ['deleted_at'];

    protected $appends = ['cost'];

    function arrangement_items() {
        return $this->morphMany('Stemcounter\Arrangement_Item', 'item');
    }

    function recipe_items() {
        return $this->morphMany('Stemcounter\Recipe_Item', 'item');
    }

    function item( $withTrashed = false ) {
        return $this->belongsTo('Stemcounter\Item')->withTrashed();
    }

    public function getCostAttribute() {
        if ( is_null( $this->attributes['cost_pu'] ) ) {
            return $this->attributes['cost'] = null;
        } else {
            return $this->attributes['cost'] = sc_format_price( $this->attributes['cost_pu'] / $this->item->purchase_qty );
        }
    }
}