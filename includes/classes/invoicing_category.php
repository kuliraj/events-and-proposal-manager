<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoicing_Category extends Model {
	use SoftDeletes;
	
	protected $table = 'invoice_categories';

	protected $fillable = array('user_id', 'name', 'hardgood_multiple', 'fresh_flower_multiple', 'global_labor_value', 'hardgood_labor', 'base_price_labor', 'fee_labor', 'rental_labor');

	protected $dates = ['deleted_at'];
}