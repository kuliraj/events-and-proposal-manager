<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model {
    use SoftDeletes;

    protected $table = 'discounts';

    protected $fillable = array('name', 'value', 'type');

    protected $dates = ['deleted_at'];
}