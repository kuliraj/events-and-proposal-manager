<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Stemcounter\Form_Submission;

class Form extends Model {
	protected $table = 'forms';

	protected $fillable = array( 'user_id', 'user_id', 'name', 'confirmation_msg' );

	protected static function boot() {
		parent::boot();

		// Delete all related questions when the form is deleted
		static::deleting(function($form) {
			$form->questions()->delete();
		});
	}

	function questions() {
		return $this->hasMany('Stemcounter\Form_Question')->orderBy( 'order', 'ASC' );
	}

	function get_form_url() {
		$company_name = get_user_meta( $this->user_id, 'company_name', true );
		$company_name = $company_name ? \sanitize_title_with_dashes( $company_name ) : '-';

		$encrypted_id = sc_encrypted_string( $this->user_id . '_' . $this->id );

		return home_url( "/form/$company_name/$encrypted_id" );
	}

	function prep_for_js() {
		return array(
			'id'				=> $this->id,
			'title'				=> $this->name,
			'confirmation_msg'	=> $this->confirmation_msg,
			'properties'		=> $this->prep_questions_for_js(),
			'url'				=> $this->get_form_url(),
		);
	}

	function prep_questions_for_js() {
		$form_properties = array();
		foreach ( $this->questions as $question ) {
			$q = array(
				'id'			=> $question->id,
				'form_id'		=> $question->form_id,
				'property_id'	=> $question->property_id,
				'default'		=> $question->property->settings->default,
				'title'			=> $question->label,
				'description'	=> $question->property->settings->description,
				'format'		=> $question->property->settings->format,
				'required'		=> $question->required,
				'type'			=> $question->property->field_type,
				'property_type'	=> $question->property->property_type,
				'editing'		=> false,
				'order'			=> absint( $question->order ),
			);

			if ( ! empty( $question->property->settings->aopt ) ) {
				$q['aopt'] = $question->property->settings->aopt;
			}

			$form_properties[] = $q;
		}

		return $form_properties;
	}

	function validate_create_submission( $answers ) {
		$submission = Form_Submission::create_from_form( $this );

		$_answers = array();

		foreach ( $this->questions as $question ) {
			$answer = isset( $answers[ $question->id ] ) ? $answers[ $question->id ] : null;
			// This supports multiple answer questions
			$answer = is_array( $answer ) ? $answer : array( $answer );
			foreach ( $answer as $ans ) {
				$ans = $question->sanitize_answer( $ans );
				$valid = $question->validate_answer( $ans );

				if ( is_wp_error( $valid ) ) {
					return $valid;
				}
			}

			foreach ( $answer as $ans ) {
				$_answers[] = array(
					'user_id'		=> $this->user_id,
					'form_id'		=> $this->id,
					'question_id'	=> $question->id,
					'property_id'	=> $question->property_id,
					'answer'		=> $question->format_answer( $ans ),
				);
			}
		}

		$submission->save();
		$submission->answers()->createMany( $_answers );
		$submission->maybe_create_customer();

		// Allow hooking-in and creating email triggers and other goodies
		do_action( 'sc/form/submission/created', $submission, $this );

		return $submission;
	}
}
