<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meta extends Model {
	protected $table = 'meta';

	protected $fillable = array('type', 'type_id', 'meta_key', 'meta_value', );

	function customer() {
        return $this->hasOne( 'Stemcounter\Customer', 'id', 'meta_value' )->withTrashed();
    }
}