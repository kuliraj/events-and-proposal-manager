<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Stemcounter\Property;

class Event_Property extends Property {
	public function __construct( array $attributes = array() ) {
		$this->type = 'event';

		parent::__construct( $attributes );
	}

	function event() {
		return $this->belongsTo( 'Stemcounter\Event' );
	}
}