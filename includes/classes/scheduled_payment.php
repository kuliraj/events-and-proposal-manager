<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;

class Scheduled_Payment extends Model {

	protected $table = 'scheduled_payments';

	protected $fillable = array('user_id', 'event_id', 'payment_name', 'payment_amount', 'payment_amount_type', 'payment_date');

	function event() {
		return $this->belongsTo('Stemcounter\Event');
	}
}