<?php namespace Stemcounter;

class Ajax_Response {
	private $success = true;
	private $message = '';
	private $payload = array();
	private $default_message = 'Unknown error occurred. Please try again later.';

	function fail($message='') {
		$this->success = false;
		$this->message = $message;
		$this->respond();
	}

	function add_payload($key, $value) {
		$this->payload[$key] = $value;
	}

	function add_message( $message ) {
		if ( $message ) {
			if ( '' != $this->message ) {
				$this->message .= "\n";
			}

			$this->message .= $message;
		}
	}

	function respond( $message = '' ) {
		if ( $message ) {
			$this->message = $message;
		}

		if ( $this->success && $this->message == $this->default_message ) {
			$this->message = '';
		}

		if ( ! $this->success && ! $this->message ) {
			$this->message = $this->default_message;
		}

		$response = array(
			'success' => $this->success,
			'message' => $this->message,
			'payload' => $this->payload,
		);

		wp_send_json( $response );
	}
}