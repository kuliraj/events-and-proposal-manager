<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model {
    use SoftDeletes;

    protected $fillable = array('company_name', 'address', 'notes', 'phone', 'email', 'website');

    protected $dates = ['deleted_at'];
}