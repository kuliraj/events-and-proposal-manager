<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;

class Arrangement_Photo extends Model {

    protected $table = 'arrangement_photos';

    protected $fillable = array('arrangement_id', 'photo_id', 'modal_position');

    function arrangement() {
        return $this->belongsTo('Stemcounter\Arrangement');
    }

    function item() {
        return $this->morphTo();
    }

    // delete an arrangement photos whenever the related flower or service is deleted
    static function onItemDeleted($model) {
        $model->arrangement_photos()->delete();
    }

}