<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;

class Arrangement_Recipe extends Model {

    protected $table = 'arrangement_recipes';

    protected $fillable = array('arrangement_id', 'user_recipe_id');

    function arrangement() {
        return $this->belongsTo('Stemcounter\Arrangement');
    }

    function items() {
        return $this->hasMany('Stemcounter\Arrangement_Item');
    }

    function item() {
        return $this->morphTo();
    }

    // delete an arrangement photos whenever the related flower or service is deleted
    static function onItemDeleted($model) {
        $model->arrangement_recipes()->delete();
    }
}