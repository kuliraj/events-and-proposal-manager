<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delivery extends Model {
    use SoftDeletes;

    protected $table = 'deliveries';

    protected $fillable = array('name', 'value', 'type');

    protected $dates = ['deleted_at'];
}