<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arrangement extends Model {
	use SoftDeletes;

	protected $fillable = array('name', 'quantity', 'include', 'note', 'addon');

	protected $dates = ['deleted_at'];

	function event() {
		return $this->belongsTo('Stemcounter\Event')->withTrashed();
	}

	function recipes() {
		return $this->hasMany('Stemcounter\Arrangement_Recipe');
	}

	function items() {
		return $this->hasMany('Stemcounter\Arrangement_Item');
	}

	function photos() {
		return $this->hasMany('Stemcounter\Arrangement_Photo');
	}

	function tax_rate() {
		return $this->belongsTo('Stemcounter\Tax_Rate');
	}

	function tax_enabled() {
		return ($this->tax == 1);
	}

	function tax2_enabled() {
		return ($this->tax2 == 1);
	}

	function addon_enabled() {
		return ($this->addon == 1);
	}

	function get_cost( $recipe_id = null ) {
		$total = 0;
		foreach ($this->items as $item) {
			if ( ( ! is_null( $recipe_id ) && $recipe_id == $item->arrangement_recipe_id ) || is_null( $recipe_id ) ) {
				$total += $item->get_cost_total();
			}
		}
		return price_format($total);
	}

	function get_markup() {
		$total = 0;
		foreach ($this->items as $item) {
			$total += $item->get_markup();
		}
		return price_format($total);
	}

	function get_labor() {
		$total = 0;
		foreach ($this->items as $item) {
			$total += $item->get_labor();
		}
		return price_format($total);
	}

	function get_card_rate() {
		$total = 0;
		foreach ($this->items as $item) {
			$total += $item->get_calculated_price();
		}
		$total = $total * ( price_format( $this->event->card ) / 100);
	
		return price_format($total);
	}

	function get_calculated_price($include_quantity=true, $options = array(), $recipe_id = null) {
		$price = 0;

		foreach ($this->items as $item) {
			if ( ( ! is_null( $recipe_id ) && $recipe_id == $item->arrangement_recipe_id ) || is_null( $recipe_id ) ) {
				$price += price_format( $item->get_calculated_price( $options ) );	
			}
		}
		if ( ! $this->event->separate_cc_fee ) {
			
			$price += $price * ( price_format( $this->event->card ) / 100);
		}

		if ($include_quantity) {
			$price = price_format( $price ) * price_format( $this->quantity );
		}

		return price_format($price);
	}

	function can_be_applied_on( $applies_on, $checked_ids = array() ) {

		if ( in_array( $this->id, $applies_on ) ) { error_log( '103' );
			return false;
		}

		if ( in_array( $this->id, $checked_ids ) ) { error_log( '107' );
			return true;
		}

		foreach( $applies_on as $arr_id_to_check ) {
			if ( in_array( $arr_id_to_check,  $checked_ids ) ) {
				continue;
			}

			$arrangement = Arrangement::where( 'id', $arr_id_to_check )->first();
			$checked_ids[] = $arr_id_to_check;
			
			if (! $this->can_be_applied_on( $arrangement->applies_on, $checked_ids ) ) { error_log( '120' );
				return false; 
			}
		}
		return true;
	}

	function get_final_price( $include_quantity = true, $arrangements = false ) {
		$price = 0;

		if ( $this->is_percentage ) {
			if ( ! $this->event_id ) {
				return 0;
			}

			$arrangements = $arrangements ? $arrangements : $this->event->arrangements;
			$fixed_price = 0;

			$non_percent_ids = array();
			foreach( $arrangements as $arrangement ) {
				if ( ! $arrangement->is_percentage ) {
					$non_percent_ids[] = $arrangement->id;
				}
			}

			foreach( $arrangements as &$arrangement ) {
				if ( ! $arrangement->include ||					//If is draft
					$arrangement->addon != $this->addon || 		//If current percentage item and the arrangement are not in the same state
					! in_array( $arrangement->id, $this->applies_on ) ) { //If not applied to this arrangement
					continue;
				}

				if ( $arrangement->is_percentage ) {

					if ( $arrangement->id == $this->id ) {
						continue;
					}

					if ( ! $this->can_be_applied_on( $arrangement->applies_on, $non_percent_ids ) ) {
					 	continue;	
					 }
				}
				$fixed_price +=  $arrangement->get_final_price( true );
			}
			$price = $fixed_price * $this->override_cost / 100 ;

		} elseif ( !is_null( $this->override_cost ) ) {
			$price = price_format( $this->override_cost );

			if ($include_quantity) {
				$price = $price * price_format( $this->quantity );
			}
		} else {
			$price = price_format( $this->get_calculated_price($include_quantity) );
		}

		return price_format($price);
	}

	function get_tax() {
		$tax = 0;
		if ($this->tax_enabled() && $this->event->tax_rate_value) {
			$tax += $this->get_final_price() * ( price_format( $this->event->tax_rate_value ) / 100 );
		}

		if ($this->tax2_enabled() && $this->event->tax_rate_value2) {
			$tax += $this->get_final_price() * ( price_format( $this->event->tax_rate_value2 ) / 100 );
		}

		return price_format($tax);
	}

	function get_delivery() {
		$delivery = 0;
		if ( 'amount' == $this->event->delivery_type ) {
			$delivery = price_format( $this->event->delivery );
		} else {
			$delivery = price_format($this->get_final_price()) * ( price_format( $this->event->delivery ) / 100 );
		}

		return price_format($delivery);
	}

	public function getAppliesOnAttribute() {
		if ( $this->attributes['is_percentage'] ) { //error_log( 'read applies_on' );
			$applies_on_ids = Meta::where( array(
				'type' => 'arrangement',
				'type_id' => $this->id,
				'meta_key' => 'applies_on',
			))->pluck( 'meta_value' )->toArray();
			return $this->attributes['applies_on'] = array_map( 'intval', $applies_on_ids );
		} else {
			return $this->attributes['applies_on'] = array();
		}
	}

	public function setAppliesOnAttribute( $new_data ) {
		if ( $this->attributes['is_percentage'] ) {
			$old_data = Meta::where( array(
			'type' => 'arrangement',
				'type_id' => $this->id,
				'meta_key' => 'applies_on',
			))->pluck( 'meta_value' )->toArray();

			$new_data = empty( $new_data )? array() : array_filter( $new_data );
			$old_data = empty( $old_data )? array() : array_filter( $old_data );

			$old_data =  array_map( 'intval', $old_data );
			$new_data =  array_map( 'intval', $new_data );
			$to_delete = array_diff( $old_data, $new_data );
			$to_add = array_diff( $new_data, $old_data );

			if ( ! empty( $to_delete ) ) {
				Meta::where( array(
					'type' => 'arrangement',
					'type_id' => $this->id,
					'meta_key' => 'applies_on',
				))->whereIn( 'meta_value', $to_delete )->delete();
			}

			foreach ( $to_add as $val ) {
				Meta::create( array(
				'type' => 'arrangement',
				'type_id' => $this->id,
				'meta_key' => 'applies_on',
				'meta_value' => $val,
				));
			}

		} else {
			//delete all applies_on ids or not ? YES
		}
	}

}