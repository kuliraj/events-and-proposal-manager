<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tax_Rate extends Model {
    use SoftDeletes;

    protected $table = 'tax_rates';

    protected $fillable = array('name', 'value');

    protected $dates = ['deleted_at'];
}