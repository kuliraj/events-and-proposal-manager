<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model {
    use SoftDeletes;

    const TYPE_FLOWER = 'fresh_flower';
    const TYPE_HARDGOOD = 'hardgood';
    const TYPE_RENTAL = 'rental';
    const TYPE_BASE_PRICE = 'base_price';
    const TYPE_FEE = 'fee';

    protected $fillable = array('name', 'description', 'type', 'pricing_category', 'purchase_qty', 'purchase_unit', 'recipe_unit', 'deleted_at');

    protected $dates = ['deleted_at'];

    function arrangement_items() {
        return $this->morphMany('Stemcounter\Arrangement_Item', 'item');
    }

    function recipe_items() {
        return $this->morphMany('Stemcounter\Recipe_Item', 'item');
    }

    function order_items() {
        return $this->morphMany('Stemcounter\Order_Item', 'item');
    }

    function default_variation() {
        return $this->hasOne('Stemcounter\Item_Variation')->where( array( 'name' => 'Default' ) )->withTrashed();
    }

    /**
     * Returns the ORDER BY SQL for variations default sorting
     * 
     * Sorts variations so that first we have the "Default" variation and then
     * all other variations sorted by name ASC
     * Can be passed directly to Query_Builder::orderByRaw()
     **/
    public static function variation_default_orderby_sql() {
        return "(CASE WHEN name='Default' THEN 0 ELSE 1 END) ASC, (CASE WHEN name!='Default' THEN name END) ASC";
    }

    public static function get_user_items( $user_id, $orderby = true ) {
        $query = self::where( array(
            'user_id' => $user_id,
        ) )->with( array(
            'variations' => function( $query ) {
                $query->orderByRaw( self::variation_default_orderby_sql() );
            },
            'variations.item',
            'default_variation',
            'default_variation.item',
        ) );

        if ( $orderby ) {
            $query->orderBy( 'name', 'asc' );
        }

        return $query;
    }

    public static function get_user_flowers( $user_id ) {
        return self::where( array(
            'user_id' => $user_id,
            'type' => 'consumable',
            'pricing_category' => 'fresh_flower',
        ) )->with( array(
            'variations' => function( $query ) {
                $query->orderByRaw( self::variation_default_orderby_sql() );
            },
            'variations.item',
            'default_variation',
            'default_variation.item',
        ) )->orderBy( 'name', 'asc' );
    }

    public static function get_user_non_flowers( $user_id ) {
        return self::where( array(
            'user_id' => $user_id,
        ) )->where( 'pricing_category', '!=', 'fresh_flower' )->with( array(
            'variations' => function( $query ) {
                $query->orderByRaw( self::variation_default_orderby_sql() );
            },
            'variations.item',
            'default_variation',
            'default_variation.item',
        ) )->orderBy( 'name', 'asc' );
    }

    function variations( $withTrashed = false ) {
        if ( ! $withTrashed ) {
    	   return $this->hasMany('Stemcounter\Item_Variation'); 
        } else {
            return $this->hasMany('Stemcounter\Item_Variation')->withTrashed(); 
        }
    }
}
