<?php namespace Stemcounter;

use Illuminate\Database\Eloquent\Model;

class Made_Payment extends Model {

	protected $table = 'payments_made';

	protected $fillable = array('user_id', 'event_id', 'payment_name', 'payment_amount', 'payment_type', 'payment_date', 'payment_note');

	function event() {
		return $this->belongsTo('Stemcounter\Event');
	}
}