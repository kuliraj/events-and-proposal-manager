<?php
use Illuminate\Http\Request;
use Stemcounter\Humanreadable_Exception;
use Stemcounter\Ajax_Response;
use Stemcounter\Notification;

function sc_handle_notifications_settings() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();
	
	$data = array(
		'email' => $request->input( 'email' ),
		'allow' => $request->input( 'allow' ),
	);

	update_user_meta( get_current_user_id(), '_sc_user_notifications', $data );
	
	$r->respond();	
}
add_action( 'wp_ajax_sc_handle_notifications_form', 'sc_handle_notifications_settings', 10 );

function sc_email_notifications( $user_id, $notification, $type, $subject ) {
	$notifcations_settings = get_user_meta( $user_id, '_sc_user_notifications', true );
	$current_user = get_userdata( $user_id );
	$company_info = get_company_info( $user_id );
	$wpmail = 1;

	if ( empty( $notifcations_settings ) ) {
		$notifcations_settings['allow'] = 'yes';
		$notifcations_settings['email'] = $current_user->user_email;
	}

	$logo = null;
	$company_info['company'] = 'StemCounter';
	$company_info['email'] = '';
	$logo = get_template_directory_uri() . '/img/sc-logo-horizontal.png';

	add_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );

	if ( 'no' != $notifcations_settings['allow'] ) {
		$wpmail = 0;
		$headers = array();
		$body = '';
		
		$headers[] = 'From: "StemCounter.com" <noreply@stemcounter.com>';
		$headers[] = 'Reply-To: "' . $company_info['company'] . '" <' . $company_info['email'] . '>';
		$headers[] = 'List-Unsubscribe: <' . home_url( '/app/wp-admin/admin-ajax.php?action=sc_email_unsubscribe' ) . '>';
		$to = sc_prepare_email_recipients_for_mandrill( ! empty( $notifcations_settings['email'] ) ? $notifcations_settings['email'] : '' );

		$body .= '<div class="wrapper" style="background-color: #f2f2f2; padding: 80px 0 80px 0;">';
			$body .= '<div class="inner-wrapper" style="background-color: #ffffff; padding-top: 20px; border: 1px solid #CCC6BE; width: 580px; margin: 0 auto;" >';
				$body .= '<div style="display: block; text-align: center;">';
					$body .= '<div class="company-logo" style="text-align:center;"><img src="' . $logo . '" width="250" alt=""></div>';
					$body .= '<div style="padding: 0 30px 0 20px;">
								<ul class="company-info" style="list-style: none; line-height: 23px; padding: 20px 0 20px 0; border-bottom: 1px solid #CCC6BE; text-align:center;">
									<li style="font-size: 26px; color: #000000; font-weight: 600;">StemCounter Notification:</li>
								</ul>
							</div>';
				$body .= '</div>';

				$body .= '<div style="color: #000000; padding: 0 40px 10px 40px; display: block; font-size: 20px; font-weight: 100;">' . wpautop( $notification ) . '';
				$body .= '</div>';
				$body .= '<div style="text-align:center; padding: 10px 0 10px 0;"><a style="color: #f37a76; font-size: 14px;" href="' . home_url( '/profile/?notifications' ) . '">Edit Notification Settings</a></div>';
			$body .= '</div>';
		$body .= '</div>';

		$wpmail = wp_mail( $to, $subject, $body, $headers );
	}
	remove_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );
	return $wpmail ? 1 : 0;
}

function sc_admin_notifications_submenu() {
	add_management_page( 'Notifications', 'Notifications', 'manage_options', 'notifications', 'sc_admin_notifications_subpage_content' );
}
add_action( 'admin_menu', 'sc_admin_notifications_submenu' );

function sc_admin_notifications_subpage_content() {
    echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
        	echo '<h2>StemCounter Notifications Center</h2>';
	        echo '<form method="post">';
	        	echo '<p>Subscriber <input type="text" name="single_recepient" placeholder="email@example.com">  	-or-  	<label for="all_rec">Notify all<input type="checkbox" name="all_recepients" value="all" id="all_rec"></label></p>';
	        	echo '<p style="padding-top:10px;">Subject <input type="text" name="notification_subject"></p>';
	        	wp_editor(
	        		'',
	        		'notificationstinimc',
	        		array(
	        			'media_buttons' => false,
	        		)
	        	);
	        	submit_button( 'Notify' );
			echo '</form>';
    echo '</div>';
}

function sc_notifications_post_fields() {
    global $post;
    if ( ( empty( $_POST['single_recepient'] ) || empty( $_POST['all_recepients'] ) ) && empty( $_POST['notificationstinimc'] ) && empty( $_POST['notification_subject'] ) ) {
    	return;
    }

    $content = ! empty( $_POST['notificationstinimc'] ) ? $_POST['notificationstinimc'] : '';
    $subject = ! empty( $_POST['notification_subject'] ) ? $_POST['notification_subject'] : 'StemCounter Notification';
   	if ( ! empty( $_POST['all_recepients'] ) ) {
   		$args = array( 'role' => 'subscriber', 'fields' => array( 'ID' ) );
   		$users = get_users( $args );

    	wp_schedule_single_event( current_time( 'timestamp' ), 'sc_register_multi_notifications', array( $users, $content, $subject ) );
   		
   	} elseif ( ! empty( $_POST['single_recepient'] ) ) {
   		$user = get_user_by( 'email', $_POST['single_recepient'] );
	    //  Create notification about the event
		$notification = new Notification;
		$notification->user_id = $user->ID;
		$notification->type = 'update';
		$notification->read = 0;
		$notification->subject = $subject;
		$notification->notification = stripslashes( $content );

		// email notification
		$notification->email_sent = sc_email_notifications( $notification->user_id, $notification->notification, $notification->type, $notification->subject );
		$notification->save();
   	}

}
add_action( 'admin_init', 'sc_notifications_post_fields' );

function sc_payment_notifications( $event, $payment_amount ) {

	$notification = new Notification;
	$notification->user_id = $event->user_id;
	$notification->type = 'payment';
	$notification->read = 0;
	$notification->subject = 'You\'ve been paid!';
	$notification->notification = 'Payment of ' . sc_format_price( $payment_amount, true ) . ' was made for <a href="' . add_query_arg( 'eid', $event->id, sc_get_proposal_2_0_page_url() ) . '" target="_blank">' . $event->name . '</a>';
	$notification->email_sent = sc_email_notifications( $notification->user_id, $notification->notification, $notification->type, $notification->subject );
	$notification->save();
}
add_action( 'sc/stripe/charge_created', 'sc_payment_notifications', 10 , 2 );

function sc_register_notifications( $users, $content, $subject ) {
	if ( ! $users ) {
		return;
	}

	foreach ( $users as $user ) {
		//  Create notification about the event
		$notification = new Notification;
		$notification->user_id = $user->ID;
		$notification->type = 'update';
		$notification->read = 0;
		$notification->subject = $subject;
		$notification->notification = stripslashes( $content );
		$notification->email_sent = 0;
		$notification->save();
	}
}
add_action( 'sc_register_multi_notifications', 'sc_register_notifications', 10, 3 );

function sc_check_scheduled_emails(){
	wp_schedule_event( current_time( 'timestamp' ), '5min', 'sc_schedule_email_notifications' );
}

function sc_send_scheduled_notifications() {
	$notifications = Notification::where( function ( $query ) {
		$query->where( 'email_sent', '=', 0 )
			->orWhere( 'email_sent', '<', current_time( 'timestamp' ) - HOUR_IN_SECONDS );
	})->where(function ( $query ) {
			$query->where('email_sent', '!=', 1);
	})->take(100)->get();

	$notification_ids = array();
	foreach ( $notifications as $notification ) {
		$notification_ids[] = $notification->id;
	}

	Notification::whereIn( 'id', $notification_ids )->update( ['email_sent' => current_time( 'timestamp' )] );

	foreach ( $notifications as $notification ) {
		$notification->email_sent = sc_email_notifications( $notification->user_id, $notification->notification, $notification->type, $notification->subject );
		$notification->save();
	}
	
}
add_action( 'sc_schedule_email_notifications', 'sc_send_scheduled_notifications' );