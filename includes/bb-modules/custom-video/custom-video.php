<?php

/**
 * @class FLVideoModule
 */
class Custom_Video_Module extends FLBuilderModule {

	/**
	 * @property $data
	 */
	public $data = null;

	/**
	 * @method __construct
	 */
	public function __construct()
	{
		parent::__construct(array(
			'name'          => __('Custom Video', 'fl-builder'),
			'description'   => __('Render a WordPress or embedable video.', 'fl-builder'),
			'category'      => __('Stemcounter Modules', 'fl-builder'),
			'dir'           => SC_CUSTOM_MODULES_DIR . 'custom-video/',
			'url'           => SC_CUSTOM_MODULES_URL . 'custom-video/',
			'editor_export' => true,
			'enabled'       => true,
		));

		$this->add_js('jquery-fitvids');
	}

	/**
	 * @method get_data
	 */
	public function get_data()
	{
		if(!$this->data) {

			$this->data = FLBuilderPhoto::get_attachment_data($this->settings->video);

			if(!$this->data && isset($this->settings->data)) {
				$this->data = $this->settings->data;
			}
			if($this->data) {
				$parts                  = explode('.', $this->data->filename);
				$this->data->extension  = array_pop($parts);
				$this->data->poster     = isset($this->settings->poster_src) ? $this->settings->poster_src : '';
				$this->data->loop       = isset($this->settings->loop) && $this->settings->loop ? ' loop="yes"' : '';
				$this->data->autoplay   = isset($this->settings->autoplay) && $this->settings->autoplay ? ' autoplay="yes"' : '';

				// WebM format
				$webm_data = FLBuilderPhoto::get_attachment_data($this->settings->video_webm);
				$this->data->video_webm = isset($this->settings->video_webm) && $webm_data ? ' webm="'. $webm_data->url .'"' : '';
				
			}
		}

		return $this->data;
	}

	/**
	 * @method update
	 * @param $settings {object}
	 */
	public function update($settings)
	{
		// Cache the attachment data.
		if($settings->video_type == 'media_library') {

			$video = FLBuilderPhoto::get_attachment_data($settings->video);

			if($video) {
				$settings->data = $video;
			}
		}

		return $settings;
	}
}
