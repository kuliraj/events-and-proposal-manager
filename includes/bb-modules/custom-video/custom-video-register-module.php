<?php

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('Custom_Video_Module', array(
	'general'       => array(
		'title'         => __('General', 'fl-builder'),
		'sections'      => array(
			'general'       => array(
				'title'         => '',
				'fields'        => array(
					'video_type'       => array(
						'type'          => 'select',
						'label'         => __('Video Type', 'fl-builder'),
						'default'       => 'wordpress',
						'options'       => array(
							'media_library'     => __('Media Library', 'fl-builder'),
							'embed'             => __('Embed', 'fl-builder')
						),
						'toggle'        => array(
							'media_library'      => array(
								'fields'      => array('video', 'video_webm', 'poster', 'autoplay', 'loop')
							),
							'embed'     => array(
								'fields'      => array('embed_code')
							)
						)
					),
					'video'          => array(
						'type'          => 'video',
						'label'         => __( 'Video (MP4)', 'fl-builder' ),
						'help'          => __('A video in the MP4 format. Most modern browsers support this format.', 'fl-builder'),
					),
					'video_webm' => array(
						'type'          => 'video',
						'label'         => __('Video (WebM)', 'fl-builder'),
						'help'          => __('A video in the WebM format to use as fallback. This format is required to support browsers such as FireFox and Opera.', 'fl-builder'),
						'preview'         => array(
							'type'            => 'none'
						)
					),
					'poster'         => array(
						'type'          => 'photo',
						'label'         => _x( 'Poster', 'Video preview/fallback image.', 'fl-builder' )
					),
					'autoplay'       => array(
						'type'          => 'select',
						'label'         => __('Auto Play', 'fl-builder'),
						'default'       => '0',
						'options'       => array(
							'0'             => __('No', 'fl-builder'),
							'1'             => __('Yes', 'fl-builder')
						),
						'preview'       => array(
							'type'          => 'none'
						)
					),
					'loop'           => array(
						'type'          => 'select',
						'label'         => __('Loop', 'fl-builder'),
						'default'       => '0',
						'options'       => array(
							'0'             => __('No', 'fl-builder'),
							'1'             => __('Yes', 'fl-builder')
						),
						'preview'       => array(
							'type'          => 'none'
						)
					),
					'embed_code'     => array(
						'type'          => 'textarea',
						'label'         => __( 'Video Embed Code', 'fl-builder' ),
						'rows'          => '6'
					),
					'background_laptop' => array(
						'type'				=> 'select',
						'label'				=> __( 'Background laptop image', 'fl-builder' ),
						'default'			=> '0',
						'options'			=> array(
							'0'					=> __( 'No' ),
							'1'					=> __( 'Yes' ),
						),
					),
				)
			)
		)
	)
));