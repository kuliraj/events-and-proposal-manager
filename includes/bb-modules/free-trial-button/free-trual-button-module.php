<?php

/**
 * Custom Study Tips Module Class
 */

class Free_Trial_Button_Module extends FLBuilderModule {

	public function __construct() {

		parent::__construct( array(
			'name'          => __( 'Free Trial Button', 'fl-builder' ),
			'description'   => __( 'A module used for Free Trial Button', 'fl-builder' ),
			'category'      => __( 'Stemcounter Modules', 'fl-builder' ),
			'dir'           => SC_CUSTOM_MODULES_DIR . 'free-trial-button/',
			'url'           => SC_CUSTOM_MODULES_URL . 'free-trial-button/',
			'editor_export' => true,
			'enabled'       => true,
		) );

	}

}