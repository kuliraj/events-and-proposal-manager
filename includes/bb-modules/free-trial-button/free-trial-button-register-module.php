<?php

/**
 * Registers the Custom Study Tips Module
 */

FLBuilder::register_module('Free_Trial_Button_Module', array(
    'general'       => array( // Tab
        'title'         => __('General', 'fl-builder'), // Tab title
        'sections'      => array( // Tab Sections
            'general'       => array( // Section
                'title'         => __('Button Options', 'fl-builder'), // Section Title
                'fields'        => array( // Section Fields
                    'position'     => array(
                        'type'          => 'select',
                        'label'         => __('Button Position', 'fl-builder'),
                        'default'       => 'left',
                        'options'		=> array(
                        	'left'			=> __( 'Left', 'fl-builder' ),
                        	'center'		=> __( 'Center', 'fl-builder' ),
                        	'right'			=> __( 'Right', 'fl-builder' )
                        )
                    )
                )
            )
        )
    )
));