<?php
/**
 * Plugin Name: Beaver Builder Stemcounter Custom Modules
 * Plugin URI: https://paiyakdev.com
 * Description: Custom modules for the Beaver Builder Plugin.
 * Version: 1.0
 * Author: Paiyak Development
 * Author URI: https://paiyakdev.com
 */
define( 'SC_CUSTOM_MODULES_DIR', dirname( __FILE__ ) );
define( 'SC_CUSTOM_MODULES_URL', get_bloginfo( 'template_directory' ) . '/includes/bb-modules/' );

function bb_stemcounter_custom_modules() {
	if ( class_exists( 'FLBuilder' ) ) {

		/* Free Trial Button Module */
		require_once( SC_CUSTOM_MODULES_DIR . '/free-trial-button/free-trual-button-module.php' );
		require_once( SC_CUSTOM_MODULES_DIR . '/free-trial-button/free-trial-button-register-module.php' );

		/* Custom Video Module */
		require_once( SC_CUSTOM_MODULES_DIR . '/custom-video/custom-video.php' );
		require_once( SC_CUSTOM_MODULES_DIR . '/custom-video/custom-video-register-module.php' );	
	}
}
add_action( 'init', 'bb_stemcounter_custom_modules' );

function fl_stemcounter_module_assets() {
    if ( class_exists( 'FLBuilderModel' ) && FLBuilderModel::is_builder_enabled() ) {
        wp_enqueue_style( 'stemcounter-custom-modules', SC_CUSTOM_MODULES_URL . 'assets/css/fields.css' );
    }
}
add_action( 'wp_enqueue_scripts', 'fl_stemcounter_module_assets' );