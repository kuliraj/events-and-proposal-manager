<?php
use Illuminate\Http\Request;
use Symfony\Component\Translation\Translator;
use Illuminate\Validation\Factory;
use Stemcounter\Arrangement;
use Stemcounter\Flower;
use Stemcounter\Service;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Variation;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;
use Stemcounter\Measuring_Unit;

function sc_get_user_item_qb( $user_id, $item_id ) {
	$item_qb = null;

	$search = array(
		'id' => $item_id,
		'user_id' => $user_id,
	);

	$item_qb = Item::where( $search )->with( array( 'variations' => function($query){
		$query->orderByRaw( Item::variation_default_orderby_sql() );
	} ) )->withTrashed();

	return $item_qb;
}

function sc_get_mixed_arrangement_item_types() {
	return array(
		'flower'  => 'Flower',
		'hardgood'		=> 'Hardgood',
		'base_price'	=> 'Base price',
		'fee'			=> 'Fee',
		'rental'		=> 'Rental',
	);
}

function sc_get_item_type( $item, $label = true, $deletedItem = false ) {
	$key = '';

	if ( is_null( $item ) && $deletedItem ) {
	    try {
            $item = Item::where( array(
                'id' => $deletedItem->item_id
            ) )->withTrashed()->firstOrFail()->toArray();
            $key = $item->pricing_category;
	    } catch (Exception $e) {
	        return '';            
	    }
	} elseif ( empty( $item->pricing_category ) ) {
		return '';
	} else {
		$key = $item->pricing_category;
	}

	if ( $label ) {
		$types = sc_get_mixed_arrangement_item_types();
		if ( isset( $types[ $key ] ) ) {
			return $types[ $key ];
		} else {
			return 'fresh_flower' == $key ? 'Flower' : '';
		}
	}

	return $key;
}

function sc_get_simple_arrangement_item_types() {
	return array(
		'flower' => 'Flower',
		'service' => 'Service',
	);
}

function sc_get_simple_item_type($item, $label = true, $deletedItem = false) {
	if (is_null($item) && $deletedItem) {
		if ( 'fresh_flower' == $deletedItem->pricing_category ) {
			return 'flower';
		} else {
			return 'service';
		}
	}

	if ( 'fresh_flower' == $item->pricing_category ) {
		$key = 'flower';
	} else {
		$key = 'service';
	}

	if ( $label ) {
		$types = sc_get_simple_arrangement_item_types();
		$type = $types[ $key ];
		return $type;
	}

	return $key;
}

function sc_ajax_edit_item_form() {
	sc_clean_input_slashes();
	$request = Request::capture();

	$id = $request->input('id');
	$type = $request->input('type');
	$type = $type ? $type : 'consumable';
	$pricing_category = $request->input('pricing_category');
	$pricing_category = $pricing_category ? $pricing_category : 'flower';

	$values = null;
	$item = null;
	$user_id = get_current_user_id();

	if ( ! empty( $request->input( 'lib_item_id' ) ) && ! empty( $request->input( 'lib_user_id' ) ) ) {
		$user_id = intval( $request->input( 'lib_user_id' ) );
		$id = intval( $request->input( 'lib_item_id' ) );
	}

	$item = sc_get_user_item_qb( $user_id, $id )->first();
	
	if ( $item ) {
		$values = $item->toArray();
		$values['id'] = $user_id != get_current_user_id() ? -1 : $values['id'];
		$values['user_id'] = get_current_user_id();
		$values['variations'] = array();
		foreach ( $item->variations as $key => $variation ) {
			if ( ! empty( $request->input( 'variation_id' ) ) && intval( $request->input( 'variation_id' ) ) == $variation->id ) {
				$values['variation_index'] = $key;
			}
			$_variation = array();
			$_variation['id'] = $user_id != get_current_user_id() ? -1 : $variation->id;
			$_variation['name'] = ucfirst( strtolower( $variation->name ) );
			$_variation['cost'] = is_null( $variation->cost ) ? null : sc_format_price( $variation->cost );
			$_variation['cost_pu'] = is_null( $variation->cost_pu ) ? null : sc_format_price( $variation->cost_pu );
			$_variation['inventory'] = $variation->inventory;
			$_variation['attachment'] = sc_get_variation_attachment( $variation );

			$values['variations'][] = $_variation;
		}
	}

	if ( ! is_null( $item ) && ! is_null( $item->type ) && empty( $request->input( 'lib_item' ) ) ) {
		$type = $item->type;
		$pricing_category = $item->pricing_category;
		if ( 'fresh_flower' == $pricing_category ) {
			$pricing_category = 'flower';
		}
	}
	?>
	<form class="form-horizontal style-form edit-item-form" method="post">
		<div class="form-shell"></div>
	</form>
	<script type="text/javascript">
	jQuery(document).trigger('stemcounter.action.renderCreateNewItemForm', {
		defaultType: <?php echo json_encode($type); ?>,
		defaultCategory: <?php echo json_encode( $pricing_category ); ?>,
		values: <?php echo json_encode( $values ); ?>,
		added_form_lib: <?php echo json_encode( null !== $request->input( 'added_form_lib' ) ? intval( $request->input( 'added_form_lib' ) ) : 0 ); ?>
	});
	</script>
	<?php
	exit;
}
add_action('wp_ajax_sc_edit_item_form', 'sc_ajax_edit_item_form');

function sc_get_services_with_inventory() {
	return array( Item::TYPE_HARDGOOD, Item::TYPE_RENTAL );
}

function sc_ajax_edit_item() {
	sc_clean_input_slashes();
	$request = Request::capture();

	try {
		$item_category = $request->input('item_category');
		if ( 'flower' == $item_category ) {
			$item_category = 'fresh_flower';
		}
		$item_type = $request->input('item_type');
		$item = $request->input('values');
		$variation_index = isset( $item['variation_index'] ) ? $item['variation_index'] : false;
		$variation_id = false;

		$search = array(
			'user_id'=>get_current_user_id(),
		);
		if ( $item['id'] ) {
			$search['id'] = $item['id'];
		} else {
			$search['name'] = $item['name'];
		}

		$new_item = Item::firstOrNew($search);
		$new_item->type = $item_type;
		$new_item->pricing_category = $item_category;
		$new_item->description = $item['description'];

		if ( ! isset($search['id']) && $new_item->exists ) {
			throw new Exception('A duplicate item already exists.');
		}

		if ( empty( $item['purchase_qty'] ) || 0 >= intval( $item['purchase_qty'] ) ) {
			throw new Exception('Purchased Quantity field is required!');	
		}		

		if ( ! isset( $item['variations'][0]['cost_pu'] ) || 0 > $item['variations'][0]['cost_pu'] ) {
			throw new Exception('Default variation cost is required!');	
		}

		$new_item->user_id = get_current_user_id();
		$new_item->name = $item['name'];
		if ( 'consumable' != $item_type ) {
			$new_item->purchase_qty = 1; 
			$new_item->purchase_unit = 'item';
			$new_item->recipe_unit = 'item';
		} else {
			$new_item->purchase_unit = empty( $item['purchase_unit'] ) ? 'item' : $item['purchase_unit'];
			$new_item->recipe_unit = empty( $item['purchase_unit'] ) ? 'item' : $item['purchase_unit'];
			$new_item->purchase_qty = floatval( $item['purchase_qty'] );
		}

		$new_item->save();

		$variations_ids = array();
		if ( ! empty( $item['variations'] ) ) {

			foreach ( $item['variations'] as $key => $_variation ) {
				$variation = Item_Variation::firstOrNew( array( 'id' => intval( $_variation['id'] ) ) );
				$variation->item_id = $new_item->id;
				if ( 0 !== $key && 'default' == strtolower( $_variation['name'] ) ) {
					throw new Exception('You can have ONLY one default variation!');
				} else {
					$variation->name = ucfirst( strtolower( $_variation['name'] ) );
				}
				$variation->cost_pu = empty( $_variation['cost_pu'] ) ? null : floatval( $_variation['cost_pu'] );
				$variation->attachment_id = empty( $_variation['attachment'] ) ? null : intval( $_variation['attachment']['imageId'] );
				$variation->inventory = empty( $_variation['inventory'] ) ? null : floatval( $_variation['inventory'] );
				$variation->save();

				if ( false !== $variation_index && $variation_index == $key ) {
					$variation_id = intval( $variation->id );
				}

				$variations_ids[] = intval( $variation->id );
			}

		} else {
			throw new Exception('Your item should have default variation.');	
		}

		foreach ( $new_item->variations as $item_variation ) {
			// Delete removed variations
			if ( ! in_array( $item_variation->id, $variations_ids ) ) {
				Item_Variation::where(array(
					'id' => $item_variation->id
				))->delete();
			}
		}
		// reload the item with all variations
		$new_item = Item::where( 'id', '=', $new_item->id )->with( array( 'variations', 'variations.item', 'default_variation', 'default_variation.item' ) )->first();

		$attachment = array();
		if ( $new_item->default_variation->attachment_id ) {
			$item_photo = wp_get_attachment_image_src( $new_item->default_variation->attachment_id, 'full' );
			$attachment = array(
				'imageId' => $new_item->default_variation->attachment_id,
				'imageURL' => $item_photo[0],
				'imageW' => $item_photo[1],
				'imageH' => $item_photo[2],
			);
		} else {
			$attachment = array(
				'imageId' =>'',
				'imageURL' => '',
				'imageW' => '',
				'imageH' => '',
			);
		}

		$response['success'] = true;
		$response['message'] = 'Item updated';
		$response['payload']['item'] = $new_item->toArray();
		$response['payload']['pricing_category'] = $new_item->pricing_category;
		if ( ! empty( $variation_id ) ) {
			$response['payload']['item_variation_id'] = $variation_id;
		} else {
			$response['payload']['item_variation_id'] = $new_item->default_variation->id;
		}
		$response['payload']['old_item_cost'] = $new_item->default_variation->cost;
		// $response['payload']['variation_index'] = intval( $item['variation_index'] );
		$response['attachment'] = $attachment;

	} catch (Exception $e) {
		$response['success'] = false;
		$response['message'] = $e->getMessage();
		$response['code'] = $e->getCode();
		$response['values'] = $new_item ? $new_item : null;
	}

	wp_send_json( $response );
}
add_action('wp_ajax_sc_edit_item', 'sc_ajax_edit_item');

function sc_ajax_delete_item() {
	sc_clean_input_slashes();
	$request = Request::capture();

	$response = array(
		'success'=>false,
		'code'=>0,
		'message'=>'An unknown error occurred.',
		'payload'=>array(),
	);

	try {
		$item = sc_get_user_item_qb(get_current_user_id(), $request->input('id'))->firstOrFail();
		$item->delete();

		$response['success'] = true;
	} catch (Exception $e) {
		$response['success'] = false;
	}

	wp_send_json( $response );
}
add_action('wp_ajax_sc_delete_item', 'sc_ajax_delete_item');

// Handles cost "apply to master" button
function sc_ajax_sc_apply_item_cost() {
	sc_clean_input_slashes();
	$request = Request::capture();

	$response = array(
		'success'=>false,
		'code'=>0,
		'message'=>'An unknown error occurred.',
		'payload'=>array(),
	);

	$user_id = get_current_user_id();
	$item_id = intval($_REQUEST['item_id']);
	$item_type = $_REQUEST['item_type'];
	$cost = floatval($_REQUEST['cost']);
	$cost = abs($cost);

	$search = array(
		'id'=>$item_id,
		'user_id'=>$user_id,
	);

	try {
		$item = Item::where($search)->firstOrFail();

		$item->cost = $cost;
		$item->save();
		$response['success'] = true;
	} catch (Exception $e) {
		// do nothing
	}

	wp_send_json( $response );
}
add_action('wp_ajax_sc_apply_item_cost', 'sc_ajax_sc_apply_item_cost');

function sc_render_flowers_management( $content ) {
	$items = Item::get_user_flowers( get_current_user_id() )->get();
	$user_measuring_units = get_user_meta( get_current_user_id(), 'sc_user_measuring_units', true );
	$user_measuring_units = Measuring_Unit::whereIn( 'id', $user_measuring_units )->get();
	$table_len = get_user_meta( get_current_user_id(), 'sc_flowers_table_len', true );
	ob_start(); ?>
	<div class="col-lg-2">
	</div>
	<div class="col-lg-10 clearfix" style="display: none">
		<button type="button" class="btn btn-primary btn-lg pull-right create-new-flower" style="margin-top: 10px; margin-bottom:10px;">Add New Flower</button>
	</div>

	<div class="row mt">
		<div class="col-md-12">
			<div class="content-panel">
				<table class="flowers-table table table-striped table-advance table-hover" id="listItems" data-page-length="<?php echo ! empty( $table_len ) ? $table_len : 10 ; ?>">
					<thead>
						<tr>
							<th colspan="5">
								<a class="create-new-flower cta-link">
									+ Add New Flower
								</a>
								<a class="btn btn-primary edit-flowers" href="#">Edit Flowers</a>
							</th>
						</tr>
						<tr>
							<th>Flower</th>
							<th class="hidden-xs">Variations</th>
							<th class="hidden-xs">Stems Per Bunch</th>
							<th class="hidden-xs">Purchase Unit Cost</th>
							<th class="hidden-xs"></th>
						</tr>
						
					</thead>
					<tbody>
						<?php foreach ($items as $i => $item) : ?>
							<?php
							$colors = array();
							foreach ( $item->variations as $variation ) {
								$colors[] = ucfirst( strtolower( $variation->name ) );
							}
							$measuring_unit = '';
							if ( 'consumable' == $item->type ) {
								foreach ( $user_measuring_units as $key => $unit ) {
									if ( $unit->id == $item->purchase_unit ) {
										if ( 1 == $item->purchase_qty ) {
											$measuring_unit = $unit->short;
										} else {
											$measuring_unit = $unit->short_plural;
										}
									}
								}
							} ?>
							<tr class="flower">
								<td class="flower-name hide-input">
									<span><?php echo esc_html($item->name); ?></span>
									<input class="flower-name form-control" type="text" name="flower[][flower_name]" value="<?php echo esc_html( $item->name ); ?>"/>

									<input class="flower-id hidden" type="text" name="flower[][flower_id]" value="<?php echo $item->id; ?>"/>
									<div class="visible-xs">
										<span>Colors: <?php echo esc_attr( implode( ', ', $colors ) ); ?></span>
										<input class="flower-colors form-control" type="text" name="flower[][flower_colors]" value="<?php echo esc_attr( implode( ', ', $colors ) ); ?>"/>

										<span> Stems: <?php echo esc_attr( $item->purchase_qty ) . ' ' . $measuring_unit; ?>
										<input class="flower-stems form-control" type="text" name="flower[][flower_stems]" value="<?php echo esc_attr( $item->purchase_qty ); ?>"/> &nbsp;

										 Price: <?php echo esc_attr(sc_format_price( $item->default_variation->cost_pu ) ); ?></span>
										 <input class="flower-cost form-control" type="text" name="flower[][flower_cost]" value="<?php echo esc_attr( sc_format_price( $item->default_variation->cost_pu ) ); ?>"/>
										<a href="#" class="edit-item-button" data-item-type="flower" data-item-id="<?php echo $item->id; ?>">
											<i class="fa fa-pencil"></i>
										</a>
									</div>

								</td>
								<td class="flower-colors hide-input hidden-xs">
									<span><?php echo esc_attr( implode( ', ', $colors ) ); ?></span>
									<input class="flower-colors form-control" type="text" name="flower[][flower_colors]" value="<?php echo esc_attr( implode( ', ', $colors ) ); ?>"/>
								</td>
								<td class="flower-stems hide-input hidden-xs">
									<span><?php echo esc_attr( $item->purchase_qty ) . ' ' . $measuring_unit; ?></span>
									<input class="flower-stems form-control" type="text" name="flower[][flower_stems]" value="<?php echo esc_attr( $item->purchase_qty ); ?>"/>
								</td>
								<td class="flower-cost hide-input hidden-xs">
									<span><?php echo esc_attr(sc_format_price( $item->default_variation->cost_pu ) ); ?></span>
									<input class="flower-cost form-control" type="text" name="flower[][flower_cost]" value="<?php echo esc_attr( sc_format_price( $item->default_variation->cost_pu ) ); ?>"/>
								</td>
								<td class="flower-edit-button hidden-xs style="text-align: right;">
									<a href="#" class="edit-item-button" data-item-type="flower" data-item-id="<?php echo $item->id; ?>">
										<i class="fa fa-pencil"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<h6 class="footnote">*All items listed here are multiplied by your floral multiple when calculated on your proposal. </h6>
			</div><!-- /content-panel -->
		</div><!-- /col-md-12 -->
	</div><!-- /row -->
	<?php
	return ob_get_clean();
}
add_filter( 'sc/app/content/flowers', 'sc_render_flowers_management', 10 );

function sc_render_services_management( $content ) {
	$items = Item::get_user_non_flowers( get_current_user_id() )->get();
	$item_types = sc_get_mixed_arrangement_item_types();
	$table_len = get_user_meta( get_current_user_id(), 'sc_service_table_len', true );
	ob_start(); ?>
	<div class="col-lg-2">
	</div>
	<!--
	<div class="col-lg-10 clearfix">
		<button type="button" class="btn btn-primary btn-lg pull-right create-new-service" style="margin-top: 10px; margin-bottom:10px;">Add New Item</button>
	</div>
	-->

	<div class="row mt">
		<div class="col-md-12">
			<div class="content-panel">
				<table class="table table-striped table-advance table-hover" id="listItems" data-page-length="<?php echo ! empty( $table_len ) ? $table_len : 10 ; ?>">
					<thead>
						<tr>
							<th colspan="6">
								<a class="create-new-service cta-link">
									+ Add New Item
								</a>
							</th>
						</tr>
						<tr>
							<th>Name</th>
							<th class="hidden-xs">Description</th>
							<th class="hidden-xs">Pricing Category</th>
							<th class="hidden-xs">Cost</th>
							<th class="hidden-xs">Inventory</th>
							<th class="hidden-xs"></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($items as $item) : ?>
							<tr>
								<td><?php echo esc_html($item->name); ?>
									<div class="visible-xs">
										<?php echo esc_html($item->description); ?><br />
										<br />
										Cost: <?php echo esc_attr(sc_format_price($item->default_variation->cost)); ?>
										 &nbsp;
										Inventory: <?php echo esc_attr($item->default_variation->inventory); ?><br />
										<a href="#" class="edit-item-button" data-item-type="<?php echo esc_attr( $item->type ); ?>" data-item-id="<?php echo $item->id; ?>" data-pricing-category="<?php echo esc_attr( $item->pricing_category ); ?>">
											<i class="fa fa-pencil"></i>
										</a>
										<label class="label label-info label-mini"><?php echo esc_attr( $item_types[ $item->pricing_category ] ); ?></label>
									</div>
								</td>
								<td class="hidden-xs"><?php echo esc_html($item->description); ?></td>
								<td class="hidden-xs"><label class="label label-info label-mini"><?php echo esc_attr( $item_types[ $item->pricing_category ] ); ?></label></td>
								<td class="hidden-xs"><?php echo esc_attr(sc_format_price($item->default_variation->cost)); ?></td>
								<td class="hidden-xs"><?php echo esc_attr($item->default_variation->inventory); ?></td>
								<td class="hidden-xs" style="text-align: right;">
									<!-- <button type="button" class="btn btn-primary btn-xs edit-item-button" data-item-type="<?php echo esc_attr($item->type); ?>" data-item-id="<?php echo $item->id; ?>"><i class="fa fa-pencil"></i></button> -->
									<a href="#" class="edit-item-button" data-item-type="<?php echo esc_attr($item->type); ?>" data-item-id="<?php echo $item->id; ?>">
										<i class="fa fa-pencil"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<h6 class="footnote">*Hardgood (sundy) items use your hardgood multiple when calculated on your proposal. No other items use a multiple. </h6>
			</div><!-- /content-panel -->
		</div><!-- /col-md-12 -->
	</div><!-- /row -->
	<?php
	return ob_get_clean();
}
add_filter( 'sc/app/content/services', 'sc_render_services_management', 10 );

function sc_render_flowers_js() {
	$items = Item::where( 'user_id', '=', get_current_user_id() )->where('pricing_category', '=', 'fresh_flower' )->orderby( 'name', 'asc' )->with( 'variations' )->get();
	$user_measuring_units = get_user_meta( get_current_user_id(), 'sc_user_measuring_units', true );
	$measuring_units = Measuring_Unit::whereIn( 'id', $user_measuring_units )->get();
	?>
	<script type="text/javascript">
		stemcounter.page.userItems = {
			'flower': <?php echo $items->toJSON(); ?>,
		};
		stemcounter.page.measuring_units = <?php echo json_encode( $measuring_units ); ?>;

		(function($){

			$(document).ready(function(){

				$('.create-new-flower').on('click', function(e){
					e.preventDefault();

					var url = window.stemcounter.aurl({
						action: 'sc_edit_item_form',
						type: 'consumable',
						pricing_category: 'flower'
					});

					stemcounter.openAjaxModal('Add New Item', url, 'new-modal edit-item-modal');
					//stemcounter.openAjaxModal('Add New Item', url, 'edit-item-modal');
				});

				$('.edit-item-button').on('click', function(e){
					e.preventDefault();

					var url = window.stemcounter.aurl({
						action: 'sc_edit_item_form',
						type: $(this).attr('data-item-type'),
						id: $(this).attr('data-item-id')
					});

					stemcounter.openAjaxModal('Edit Item', url, 'new-modal edit-item-modal');
					//stemcounter.openAjaxModal('Edit Item', url, 'edit-item-modal');
				});

				$(document).on('stemcounter.userItemAdded stemcounter.userItemModified', function() {
			        window.location.reload();
			    });

				var flowerTable = $("#listItems").dataTable({
					lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
					// "orderCellsTop": false
				});

				$('#listItems').on('click', '.edit-flowers', function(e) {
					e.preventDefault();
					$(this).remove();
					$('#listItems thead tr:nth-child(1) th:nth-child(1)').append('<a class="btn btn-primary save-flowers" href="#">Save Flowers</a>');
					$('.flowers-table thead').append('<tr><td colspan="5"><a class="btn btn-primary add-new-flower" href="#">New Flower</a></td></tr>');
					flowerTable.DataTable().page.len(25).draw();
					
					$('.flowers-table').toggleClass('show-input');
				});

				var numberOfNewFlowers = 0;
				$(document).on('click', '.add-new-flower', function(event) {
					event.preventDefault();
					++numberOfNewFlowers;
					
					if (numberOfNewFlowers > 25) flowerTable.DataTable().page.len(50).draw();
					if (numberOfNewFlowers > 50) flowerTable.DataTable().page.len(100).draw();
					if (numberOfNewFlowers > 100) flowerTable.DataTable().page.len(-1).draw();

					//adds new flower row
					$newFlowerRow = $('<tr class="flower new-flower"><td class="flower-name hide-input"><span></span><input class="flower-name form-control" type="text" name="flower[][flower_name]" value=""/><input class="flower-id hidden" type="text" name="flower[][flower_id]" value="-1"/></td><td class="flower-coolrs hide-input"><span></span><input class="flower-colors form-control" type="text" name="flower[][flower_colors]" value=""/></td><td class="flower-stems hide-input"><span>0</span><input class="flower-stems form-control" type="text" name="flower[][flower_stems]" value="0"/></td><td class="flower-cost hide-input"><span>0</span><input class="flower-cost form-control" type="text" name="flower[][flower_name]" value="0"/></td><td class="flower-edit-button" style="text-align: right;"><button type="button" class="btn btn-primary btn-xs edit-item-button" data-item-type="flower" data-item-id="-1"><i class="fa fa-pencil"></i></button></td></tr>');

					flowerTable.DataTable().row.add($newFlowerRow.get()[0]).draw();
				});

				$('#listItems').on('click', '.save-flowers', function(e) {
					e.preventDefault();
					var changedFields = $('.flowers-table .flower-changed');
					var changedFlowers = [];
					
					$.each(changedFields, function(i, el) {
						var $el = $(el);

						changedFlowers.push({
							flower_id: $el.find('input.flower-id').val(),
							flower_name: $el.find('input.flower-name').val(),
							flower_variations: $el.find('input.flower-colors').val(),
							flower_stems: $el.find('input.flower-stems').val(),
							flower_cost: $el.find('input.flower-cost').val()
						});
					});

					$('.flowers-table').block();

					jQuery.ajax({
						type: 'post',
						url: window.stemcounter.ajax_url,
						data: {
							action: 'sc_edit_flowers',
							flowers: changedFlowers
						},
						success: function(response){
							$('.flowers-table').unblock();
							stemcounter.JSONResponse(response, function(r) {
								if (r.success) document.location.reload(); 
							});
						}
					});
					changedFlowers = [];
				});

				//watching for input changes
				$('.flowers-table').on('change', 'input', function(event) {
					var val = $(this).val();
					$(this).closest('tr').addClass('flower-changed');
					if ( ! $(this).closest('tr').hasClass('new-flower') ) {
						$(this).closest('tr').find('input[name="'+$(this).attr('name')+'"]').val(val);
					}
				});
			});

			$('#listItems').on( 'length.dt', function ( e, settings, len ) {
				
				var url = window.stemcounter.aurl({ action: 'sc_datatables_len' }),
					data = {
						'meta_key': 'sc_flowers_table_len',
						'meta_value': len
					};

				$.post(url, data, function (response) {
					stemcounter.JSONResponse(response, function (r) {
						if ( ! r.success ) {
							console.log( r );
						}
					});
				});
			});

		})(jQuery);
	</script>
	<?php
}
add_action( 'sc/app/footer/flowers', 'sc_render_flowers_js', 10 );
add_action( 'sc/app/footer/flowers', 'sc_photoswipe_html', 10 );

function sc_render_services_js() {
	$items = Item::where( 'user_id', '=', get_current_user_id()
	)->where('pricing_category', '!=', 'fresh_flower')->orderby( 'name', 'asc' )->with( 'variations' )->get();
	$user_measuring_units = get_user_meta( get_current_user_id(), 'sc_user_measuring_units', true );
	$measuring_units = Measuring_Unit::whereIn( 'id', $user_measuring_units )->get(); ?>
	<script type="text/javascript">
		stemcounter.page.userItems = {
			'service': <?php echo $items->toJSON(); ?>,
		};
		stemcounter.page.measuring_units = <?php echo json_encode( $measuring_units ); ?>;
		(function($){

			$(document).ready(function(){

				$('.create-new-service').on('click', function(e){
					e.preventDefault();

					var url = window.stemcounter.aurl({
						action: 'sc_edit_item_form',
						type: 'intangible',
						pricing_category: 'base_price'
					});

					stemcounter.openAjaxModal('Add New Item', url, 'new-modal edit-item-modal');
				});

				$('.edit-item-button').on('click', function(e){
					e.preventDefault();

					var url = window.stemcounter.aurl({
						action: 'sc_edit_item_form',
						type: $(this).attr('data-item-type'),
						id: $(this).attr('data-item-id')
					});

					stemcounter.openAjaxModal('Edit Item', url, 'new-modal edit-item-modal');
				});

				$(document).on('stemcounter.userItemAdded stemcounter.userItemModified', function() {
			        window.location.reload();
			    });

				$("#listItems").dataTable({
					//"orderCellsTop": true
				});
			});

			$('#listItems').on( 'length.dt', function ( e, settings, len ) {
			
				var url = window.stemcounter.aurl({ action: 'sc_datatables_len' }),
					data = {
						'meta_key': 'sc_service_table_len',
						'meta_value': len
					};

				$.post(url, data, function (response) {
					stemcounter.JSONResponse(response, function (r) {
						if ( ! r.success ) {
							console.log( r );
						}
					});
				});
			});

		})(jQuery);
	</script>
	<?php
}
add_action( 'sc/app/footer/services', 'sc_render_services_js', 10 );
add_action( 'sc/app/footer/services', 'sc_photoswipe_html', 10 );

function sc_get_variation_attachment( $variation, $default_variation = false ) {
	$attachment = false;
	$attachment_id = false;

	if ( ! empty( $variation->attachment_id ) ) {
		$attachment_id = $variation->attachment_id;
	} elseif ( ! empty( $default_variation->attachment_id ) ) {
		$attachment_id = $default_variation->attachment_id;
	}

	if ( $attachment_id ) {
		$photo = wp_get_attachment_image_src( $attachment_id, 'full' );
		$photo_thumb = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
		if ( $photo ) {
			$attachment = array(
				'imageId' => $attachment_id,
				'imageURL' => $photo[0],
				'imageW' => $photo[1],
				'imageH' => $photo[2],
				'imageThumbURL' => ! empty( $photo_thumb ) ? $photo_thumb[0] : $photo[0],
			);
		}
	}

	if ( ! $attachment ) {
		$attachment = array(
			'imageId' =>'',
			'imageURL' => '',
			'imageW' => '',
			'imageH' => '',
			'imageThumbURL' => '',
		);
	}

	return $attachment;
}


/**
* Allow admins to import flowers to users libraries from a custom subpage
*/
function sc_admin_import_to_users_library() {
	add_management_page( 'Import Items', 'Import Items', 'manage_options', 'import_flowers', 'sc_admin_import_subpage_content' );
}
add_action( 'admin_menu', 'sc_admin_import_to_users_library' );

function sc_admin_import_subpage_content() {
	global $sc_items_to_imort;
    ?>
    <div class="wrap"><div id="icon-tools" class="icon32"></div>
        <h2>StemCounter Import Items</h2>
         <?php if ( $sc_items_to_imort ) { ?>
			<div class="notice notice-success is-dismissible"><p>Processing... Items left to be imported: <?php echo $sc_items_to_imort; ?></p></div>
    	<?php } ?>
		<form method="post" enctype="multipart/form-data">
			<input type="text" name="csv_user_id" placeholder="user ID">
			<h4>Upload file</h4>
			<label for="fileSelect">Filename:</label>
			<input type="file" name="csv_file" id="fileSelect"><br>
			<?php echo submit_button( 'Upload to library' ); ?>
		</form>

		<p>CSV Example: <a href="<?php echo get_template_directory_uri(); ?>/stemcounter-flowers.csv">here</a></p>
	</div>
	<?php if ( isset( $_GET['file_name'] ) && isset( $_GET['user_id'] ) && isset( $_GET['offset'] ) && is_super_admin() && $sc_items_to_imort != 0 ) : ?>
	    <script type="text/javascript">
			setTimeout(function() {
				window.location.href = <?php echo json_encode( add_query_arg( array( 'offset' => $_GET['offset'] + 50, 'file_name' => $_GET['file_name'], 'user_id' => $_GET['user_id'] ) ) ); ?>;
			}, 1500);

		</script>		
	<?php endif;
}

function sc_handle_imported_file() {
	global $post;
	global $sc_items_to_imort;
	$dump = 'Fail failed to upload!';
	$directory = dirname( dirname( ABSPATH ) ) . '/temp-libraries/';

	if ( isset( $_FILES['csv_file'] ) && isset( $_POST['csv_user_id'] ) && is_super_admin() ) {
		$user_id = intval( $_POST['csv_user_id'] );
		$file = $_FILES['csv_file'];

		if ( ! file_exists( $directory ) || ! is_dir( $directory ) ) {
			mkdir( $directory, 0755, true );
		}
		$temp_file = tempnam( $directory, 'sc_' );
		$file_name = basename( $temp_file );
		move_uploaded_file( $file['tmp_name'], "$directory/$file_name" );

		wp_redirect( add_query_arg( array( 'offset' => 0, 'file_name' => $file_name, 'user_id' => $user_id ) ) );
	}

	if ( isset( $_GET['file_name'] ) && isset( $_GET['user_id'] ) && isset( $_GET['offset'] ) && is_super_admin() ) {
		$sc_items_to_imort = 0;
		$user_id = intval( $_GET['user_id'] );
		$file_name = $_GET['file_name'];
		$csvarray = array();
		$row = 0;
		$offset = absint( $_GET['offset'] );
		$max = $offset + 50;
		$remaining = 0;
		$keys = array( 'name' => 0, 'category' => 1, 'purchase_qty' => 2, 'cost_pu' => 3, 'description' => 4, 'attachment_url' => 5 );
		// Open the File.
		if ( ( $handle = fopen( $directory . $file_name, 'r' ) ) !== FALSE ) {
			while ( ( $data = fgetcsv( $handle, 1000, ',' ) ) !== FALSE ) {

				if ( 0 < $row && $row > $offset && $row < $max && ! empty( $data[ $keys['name'] ] ) ) {
					// Populate the multidimensional array.
					$csvarray[] = array(
						'name'			=> $data[ $keys['name'] ],
						'category'		=> $data[ $keys['category'] ],
						'purchase_qty'	=> $data[ $keys['purchase_qty'] ],
						'cost_pu'		=> $data[ $keys['cost_pu'] ],
						'description'	=> $data[ $keys['description'] ],
						'attachment_url'=> $data[ $keys['attachment_url'] ],
					);
				}

				if ( $max >= $row ) {
					$remaining ++;
				}

				$row++;
			}
			fclose( $handle );
		}

		$sc_items_to_imort = $row - $remaining;
		// Write the data from the array into user's item library
		if ( ! empty( $csvarray ) ) {
			$dump = 'Success!';
			$variations = array();

			foreach ( $csvarray as $_item ) {
				if ( strpos( $_item['name'], '-' ) !== false ) {
					$name = explode( '-', $_item['name'] );
					$_item['name'] = trim( $name[1] );
					$variations[ trim( strtolower( $name[0] ) ) ][] = $_item;
				}
			}
			foreach ( $csvarray as $_item ) {
				if ( strpos( $_item['name'], '-' ) == false ) {
					$item_info = sc_pricing_category_type_from_input( $_item['category'] );
					$item = Item::firstOrNew( array(
						'name' => $_item['name'],
						'user_id' => $user_id,
						'pricing_category' => $item_info['category'],
					) );
					$item->user_id = $user_id;
					$item->name = ! empty( $_item['name'] ) ? $_item['name'] : 'Unknown Item Name';
					if ( ! $item->exists ) {
						$item->description = ! empty( $_item['description'] ) ? $_item['description'] : '';
					}

					$item->pricing_category = $item_info['category'];
					$item->type = $item_info['type'];

					$item->purchase_qty = ! empty( $_item['purchase_qty'] ) ? floatval( preg_replace('/[^0-9-.]+/', '', $_item['purchase_qty'] ) ) : 1;
					$item->purchase_unit = 'item';
					$item->recipe_unit = 'item';
					$item->save();

					$def_variation = Item_Variation::firstOrNew( array(
						'name' => 'Default',
						'item_id' => $item->id,
					) );
					$def_variation->item_id = $item->id;
					$def_variation->name = 'Default';
					$def_variation->cost_pu = ! empty( $_item['cost_pu'] ) ? sc_format_price( preg_replace('/[^0-9-.]+/', '', $_item['cost_pu'] ) ) : null;
					$def_variation->attachment_id = null;
					$def_variation->inventory = null;
					$def_variation->save();

					if ( ! empty( $_item['attachment_url'] ) ) {
						$args = array(
							$def_variation->id,
							$_item['attachment_url'],
						);
						wp_schedule_single_event( current_time( 'timestamp' ) + 30, 'sc/scheduled_upload_variation_media', $args );
					}

					if ( array_key_exists( trim( strtolower( $_item['name'] ) ), $variations ) ) {
						foreach ( $variations[ strtolower( $_item['name'] ) ] as $variation ) {
							$_variation = Item_Variation::firstOrNew( array(
								'name'		=> $variation['name'],
								'item_id'	=> $item->id,
							) );
							$_variation->item_id = $item->id;
							$_variation->name = ucfirst( strtolower( $variation['name'] ) );
							$_variation->cost_pu = ! empty( $variation['cost_pu'] ) ? sc_format_price( preg_replace('/[^0-9-.]+/', '', $variation['cost_pu'] ) ) : null;
							$_variation->attachment_id = null;
							$_variation->inventory = null;
							$_variation->save();

							if ( ! empty( $variation['attachment_url'] ) ) {
								$args = array(
									$_variation->id,
									$variation['attachment_url'],
								);
								wp_schedule_single_event( current_time( 'timestamp' ) + 30, 'sc/scheduled_upload_variation_media', $args );
							}
						}
						unset( $variations[ trim( strtolower( $_item['name'] ) ) ] );
					}
				}
			}

			if ( ! empty( $variations ) ) {
				foreach ( $variations as $item_name => $_variations ) {
					$item_info = sc_pricing_category_type_from_input( $variations[ $item_name ][0]['category'] );
					$item = Item::firstOrNew( array(
						'name' => ucfirst( $item_name ),
						'user_id' => $user_id,
						'pricing_category' => $item_info['category'],
					) );
					$item->user_id = $user_id;
					$item->name = ucfirst( $item_name );
					if ( ! $item->exists ) {
						$item->description = ! empty( $variations[ $item_name ][0]['description'] ) ? $variations[ $item_name ][0]['description'] : '';
					}
					$item->type = $item_info['type'];
					$item->pricing_category = $item_info['category'];
					$item->purchase_qty = ! empty( $variations[ $item_name ][0]['purchase_qty'] ) ? floatval( preg_replace('/[^0-9-.]+/', '', $variations[ $item_name ][0]['purchase_qty'] ) ) : 1;
					$item->purchase_unit = 'item';
					$item->recipe_unit = 'item';
					$item->save();

					$def_variation = Item_Variation::firstOrNew( array(
						'name' => 'Default',
						'item_id' => $item->id,
					) );
					$def_variation->item_id = $item->id;
					$def_variation->name = 'Default';
					$def_variation->cost_pu = ! empty( $variations[ $item_name ][0]['cost_pu'] ) ? sc_format_price( preg_replace('/[^0-9-.]+/', '', $variations[ $item_name ][0]['cost_pu'] ) ) : null;
					$def_variation->attachment_id = null;
					$def_variation->inventory = null;
					$def_variation->save();

					if ( ! empty( $variations[ $item_name ][0]['attachment_url'] ) ) {
						$args = array(
							$def_variation->id,
							$variations[ $item_name ][0]['attachment_url'],
						);
						wp_schedule_single_event( current_time( 'timestamp' ) + 30, 'sc/scheduled_upload_variation_media', $args );
					}

					foreach ( $_variations as $_var ) {
						$_variation = Item_Variation::firstOrNew( array(
							'name'		=> $_var['name'],
							'item_id'	=> $item->id,
						) );
						$_variation->item_id = $item->id;
						$_variation->name = ucfirst( strtolower( $_var['name'] ) );
						$_variation->cost_pu = ! empty( $_var['cost_pu'] ) ? sc_format_price( preg_replace('/[^0-9-.]+/', '', $_var['cost_pu'] ) ) : null;
						$_variation->attachment_id = null;
						$_variation->inventory = null;
						$_variation->save();

						if ( ! empty( $_var['attachment_url'] ) ) {
							$args = array(
								$_variation->id,
								$_var['attachment_url'],
							);
							wp_schedule_single_event( current_time( 'timestamp' ) + 30, 'sc/scheduled_upload_variation_media', $args );
						}
					}
				}
			}
		}
	}
}
add_action( 'admin_init', 'sc_handle_imported_file' );

function sc_upload_from_url( $thumb_url = null ) {
	if ( ! ( $thumb_id = get_attachment_id_from_src( $thumb_url ) ) ) {
		$desc = 'imported attachment thumbnail';

		//is image in uploads directory?
		$upload_dir = wp_upload_dir();

		if ( FALSE !== strpos( $thumb_url, $upload_dir['baseurl'] ) ) {
			$wp_filetype = wp_check_filetype( basename( $thumb_url ), null );
			$filename = preg_replace( '/\.[^.]+$/', '', basename( $thumb_url ) );

			$attachment = array(
				'guid' => $thumb_url, 
				'post_mime_type' => $wp_filetype['type'],
				'post_title' => $desc,
				'post_content' => '',
				'post_status' => 'inherit'
			);
			$thumb_id = wp_insert_attachment( $attachment, basename( $thumb_url ) );
			// you must first include the image.php file
			// for the function wp_generate_attachment_metadata() to work
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			$attach_data = wp_generate_attachment_metadata( $thumb_id, basename( $thumb_url ) );
			wp_update_attachment_metadata( $thumb_id, $attach_data );
		} else { //not in uploads so we'll have to sideload it
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
			$tmp = download_url( $thumb_url );

			// Set variables for storage
			// fix file filename for query strings
			preg_match( '/[^\?]+\.(jpg|JPG|jpe|JPE|jpeg|JPEG|gif|GIF|png|PNG)/', $thumb_url, $matches );
			$file_array['name'] = basename( $matches[0] );
			$file_array['tmp_name'] = $tmp;

			// If error storing temporarily, unlink
			if ( is_wp_error( $tmp ) ) {
				@unlink( $file_array['tmp_name'] );
				$file_array['tmp_name'] = '';
			}

			// do the validation and storage stuff
			$thumb_id = media_handle_sideload( $file_array, 0, $desc );

			// If error storing permanently, unlink
			if ( is_wp_error( $thumb_id ) ) {
				@unlink( $file_array['tmp_name'] );
				throw new Humanreadable_Exception( $thumb_id->get_error_message() );
			}
		} //end sideload

	}
	return $thumb_id;
}

function sc_upload_dragged_variation_media() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$thumb_url = $request->input( 'url' );
	if ( empty( $thumb_url ) ) {
		$r->fail('Failed to upload item photo.');
	}
	
	try {
		$thumb_id = sc_upload_from_url( $thumb_url );
		$photo = array();

		if (  $thumb_id ) {
			$image_id = $thumb_id;
			$image_url = wp_get_attachment_image_src( $image_id, 'medium' );
			$image_url = $image_url[0];
			$full_image = wp_get_attachment_image_src( $image_id, 'full' );

			if ( 0 < intval( $request->input( 'variation_id' ) ) ) {
				try {
					$variation = Item_Variation::where(array(
						'id' => intval( $request->input( 'variation_id' ) ),
					))->first();

					$variation->attachment_id = $thumb_id;
					$variation->save();
				} catch (Exception $e) {
					$r->fail( $e->getMessage() );
				}
			}

			$photo = array(
				'imageId' => $image_id,
				'imageThumbURL' => $image_url,
				'imageURL' => $full_image[0],
				'imageW' => $full_image[1],
				'imageH' => $full_image[2],
			);
		}

		$r->add_payload( 'photo', $photo );
	} catch (Humanreadable_Exception $e) {
		if ( is_super_admin() ) {
			$r->fail( 'Failed to upload item photo.' . $e->getMessage() );
		} else {
			$r->fail( 'Failed to upload item photo.' );
		}
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_upload_dragged_variation_media', 'sc_upload_dragged_variation_media' );

function sc_scheduled_upload_variation_media( $variation_id, $attachment_url ) {
	if ( empty( $variation_id ) || empty( $attachment_url ) ) {
		return;
	}

	$attachment_id = sc_upload_from_url( $attachment_url );

	$variation = Item_Variation::where( array(
		'id' => $variation_id,
	) )->first();

	if ( $attachment_id && ! empty( $variation ) ) {
		$variation->attachment_id = $attachment_id;
		$variation->save();
	}
}
add_action( 'sc/scheduled_upload_variation_media', 'sc_scheduled_upload_variation_media', 10, 2 );

function sc_pricing_category_type_from_input( $category ) {
	$info = array(
		'category'	=> 'fresh_flower',
		'type'		=> 'consumable'
	);

	if ( ! empty( $category ) ) {
		if ( 'baseprice' == trim( strtolower( $category ) ) ) {
			$info['category'] = 'base_price';
			$info['type'] = 'intangible';
		} else if ( 'fee' == trim( strtolower( $category ) ) ) {
			$info['category'] = 'fee';
			$info['type'] = 'intangible';
		} else if ( 'rental' == trim( strtolower( $category ) ) ) {
			$info['category'] = 'rental';
			$info['type'] = 'rental';
		} else if ( 'hardgood' == trim( strtolower( $category ) ) ) {
			$info['category'] = 'hardgood';
			$info['type'] = 'consumable';
		} else {
			$info['category'] = 'fresh_flower';
			$info['type'] = 'consumable';
		}
	}

	return $info;
}

function sc_edit_flowers() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();
	$flowers = $request->input('flowers');

	try {
		foreach ( $flowers as $i => $flower ) {
			$flower_exists = Item::where( array(
				'id' => intval( $flower['flower_id'] ),
				'user_id' => get_current_user_id()
			) )->first();

			if ( empty( $flower['flower_name'] ) ) {
				throw new Humanreadable_Exception('The Flower Name fields are required.');
			}
			if ( empty( $flower['flower_cost'] ) && $flower['flower_cost'] <= 0 ) {
				throw new Humanreadable_Exception('The Purchase Unit Cost fields are required.');
			}
			if ( empty( $flower['flower_stems'] ) && $flower['flower_stems'] <= 0 ) {
				throw new Humanreadable_Exception('The Stems Per Bunch fields are required.');
			}
			if ( ! is_null( $flower_exists ) && -1 == intval( $flower['flower_id'] ) ) {
				throw new Humanreadable_Exception('A Flower with the name ' . $flower_exists->name . ' already exists.');
			}

			$search = array( 'user_id' => get_current_user_id() );
			if ( $flower['flower_id'] ) {
				$search['id'] = intval( $flower['flower_id'] );
			}

			$new_item = Item::firstOrNew( $search );
			$new_item->name = $flower['flower_name'];
			$new_item->type = 'consumable';
			$new_item->pricing_category = 'fresh_flower';
			$new_item->description = ! empty( $new_item->description ) ? $new_item->description : '';
			$new_item->user_id = get_current_user_id();
			$new_item->purchase_qty = $flower['flower_stems']; 
			$new_item->purchase_unit = ! empty( $new_item->purchase_unit ) ? $new_item->purchase_unit : 'item';
			$new_item->recipe_unit = ! empty( $new_item->recipe_unit ) ? $new_item->recipe_unit : 'item';
			$new_item->save();

			$variations = explode( ',', $flower['flower_variations'] );
			$variations = array_map( 'strtolower', $variations );
			$variations = array_map( 'trim', $variations);
			$variations = array_filter( $variations ); 

			if ( ( $key = array_search( 'default', $variations ) ) !== false ) {
				unset( $variations[ $key ] );
			}

			foreach ( $new_item->variations as $var ) {
				if ( 'Default' == $var->name ) {
					continue;
				}

				$containing_var = array_search( strtolower( $var->name ), $variations );
				if ( ! $containing_var ) {
					Item_Variation::where(array(
						'id' => $var->id
					))->delete();
					break;
				}
			}

			$def_variation = Item_Variation::firstOrNew( array( 'name' => 'Default', 'item_id' => $new_item->id ) );
			if ( -1 == intval( $flower['flower_id'] ) ) {
				$def_variation->item_id = $new_item->id;
				$def_variation->name = 'Default';
				$def_variation->attachment_id = null;
				$def_variation->inventory = null;
			}
			$def_variation->cost_pu = empty( $flower['flower_cost'] ) ? null : floatval( $flower['flower_cost'] );
			$def_variation->save();
		
			foreach ( $variations as $_variation ) {
				$variation = Item_Variation::firstOrNew( array( 'name' => ucfirst( strtolower( trim( $_variation ) ) ), 'item_id' => $new_item->id ) );
				$variation->item_id = $new_item->id;
				$variation->name = ucfirst( strtolower( trim( $_variation ) ) );
				$variation->cost_pu = empty( $variation->cost_pu ) ? null : floatval( $variation->cost_pu );
				$variation->attachment_id = empty( $variation->attachment_id ) ? null : intval( $variation->attachment_id );
				$variation->inventory = empty( $variation->inventory ) ? null : floatval( $variation->inventory );
				$variation->save();
			}
		}
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	}
	$r->respond('Success');
}
add_action('wp_ajax_sc_edit_flowers', 'sc_edit_flowers');
