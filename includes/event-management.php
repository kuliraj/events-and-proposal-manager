<?php
use Illuminate\Http\Request;
use Stemcounter\Event;
use Stemcounter\Customer;
use Stemcounter\Vendor;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Item;
use Stemcounter\Arrangement_Photo;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;
use Stemcounter\Tax_Rate;
use Stemcounter\Invoicing_Category;
use Stemcounter\Scheduled_Payment;
use Stemcounter\Made_Payment;
use Stemcounter\Meta;
use Stemcounter\Discount;
use Stemcounter\Delivery;
use Stemcounter\Event_Version;
use Stemcounter\Payment;
use Stemcounter\Arrangement_Recipe;
use Stemcounter\Notification;
use Stemcounter\Recipe;
use Stemcounter\Recipe_Item;
use Stemcounter\Order;
use Stemcounter\Order_Item;
use Stemcounter\Event_Property;
use Stemcounter\Email_Template;
use Stemcounter\Measuring_Unit;

function sc_ajax_edit_user_event() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();
	$duplicate = (bool) $request->input('duplicate', false);
	$method = $request->input('method', 'edit');
	try {
		if ( $request->input('event_fields.customer_id.0') && $request->input('event_fields.customer_id.0') == -1) {
			throw new Humanreadable_Exception('Please select a Customer.');
		}
		if (!trim($request->input('event_fields.name'))) {
			throw new Humanreadable_Exception('Please fill in the event name.');
		}
		$date_format = get_user_meta( get_current_user_id(), '__date_format', true );
		$event_date = DateTime::createFromFormat( $date_format, $request->input( 'event_fields.date' ) );

		if ( ! $event_date ) {
			throw new Humanreadable_Exception('Please enter a date for the event.');
		}

		$search = array(
			'user_id'=>get_current_user_id(),
		);
		if ($method == 'edit') {
			$search['id'] = intval($request->input('id', 0));
		} else {
			$search['name'] = $request->input('event_fields.name');
		}
		$event = Event::firstOrNew($search);

		if ($method === 'add') {
			if ($event->exists) {
				throw new Humanreadable_Exception('Event already created.', 1);
			}
			if (!get_user_meta(get_current_user_id(), 'default_invoice_category_id', true)) {
				throw new Humanreadable_Exception('Please select a Default Invoice Category from the Profile page.', 1);
			} else {
				$default_invoice_category = Invoicing_Category::where(array(
					'id' => get_user_meta(get_current_user_id(), 'default_invoice_category_id', true),
					'user_id' => get_current_user_id()
				))->first();
				$event->invoicing_category_id = $default_invoice_category->id;
				$event->hardgood_multiple = $default_invoice_category->hardgood_multiple;
				$event->fresh_flower_multiple = $default_invoice_category->fresh_flower_multiple;
				$event->global_labor_value = $default_invoice_category->global_labor_value;
				$event->flower_labor = $default_invoice_category->flower_labor;
				$event->hardgood_labor = $default_invoice_category->hardgood_labor;
				$event->base_price_labor = $default_invoice_category->base_price_labor;
				$event->fee_labor = $default_invoice_category->fee_labor;
				$event->rental_labor = $default_invoice_category->rental_labor;
				$event->add_scheduled_payments = 1;
			}
		}

		// Create a new copy of the event
		if ( $duplicate ) {
			$event = $event->replicate( array( 'created_at', 'updated_at', 'id' ) );
		} else {
			$invoice_settings = sc_get_profile_settings();
			$event->card = $invoice_settings['card'];
			$event->hide_item_prices = 0;
		}

		if ( 'add' == $method ) {
			$default_tax_rate = Tax_Rate::where( array(
				'user_id' => get_current_user_id(),
				'default' => 1,
			) )->first();

			if ( $default_tax_rate ) {
				$event->tax_rate_id = $default_tax_rate->id;
				$event->tax_rate_value = $default_tax_rate->value;
				$event->tax_rate()->associate( $default_tax_rate );
			}

			$default_tax_rate2 = Tax_Rate::where( array(
				'user_id' => get_current_user_id(),
				'default2' => 1,
			) )->first();

			if ( $default_tax_rate2 ) {
				$event->tax_rate_id2 = $default_tax_rate2->id;
				$event->tax_rate_value2 = $default_tax_rate2->value;
				$event->tax_rate2()->associate( $default_tax_rate2 );
			}
		}

		$event->user_id = get_current_user_id();
		$event->name = trim($request->input('event_fields.name'));
		$event->date = date( MYSQL_DATE_FORMAT, $event_date->getTimestamp() );
		$event->state = $request->input('event_fields.state');
		if ( $method === 'add' ) {
			$event->delivery_id = 0;
			$event->delivery = 0;
			$event->delivery_type = 'amount';
			$event->delivery_tax = 0;
			$event->status = Event::STATUS_PUBLISHED;
			$event->discount_id = 0;
			$event->discount_name = '';
			$event->discount_value = 0;
			$event->discount_type = 'amount';
		}
		$event->private_note = ! empty( $event->private_note ) ? $event->private_note : '';
		$event->event_note = ! empty( $event->event_note ) ? $event->event_note : '';
		$event->contract_note = ! empty( $event->contract_note ) ? $event->contract_note : '';
		$event->save();

		do_action( 'sc/event/save', $event );

		$venue_ids = $request->input('event_fields.venue_ids');
		$venue_types = $request->input('event_fields.venue_types');
		if ($duplicate) {
			$filtered_venue_types = array();
			foreach ($venue_ids as $i => $venue_id) {
				if ($venue_id != -1) {
					$event_meta = new Meta();
					$event_meta->type = 'event';
					$event_meta->type_id = $event->id;
					$event_meta->meta_key = 'linked_vendor';
					$event_meta->meta_value = abs($venue_id);
					$event_meta->save();
					$filtered_venue_types[] = $venue_types[$i];
				}
			}
			$event_meta = new Meta();
			$event_meta->type = 'event';
			$event_meta->type_id = $event->id;
			$event_meta->meta_key = 'venue_types';
			$event_meta->meta_value = json_encode($filtered_venue_types);
			$event_meta->save();
		} else {
			$linked_event_vendors = Meta::where(array(
				'type' => 'event',
				'type_id' => $event->id,
				'meta_key' => 'linked_vendor'
			))->delete();
			$linked_event_venue_types = Meta::where(array(
				'type' => 'event',
				'type_id' => $event->id,
				'meta_key' => 'venue_types'
			))->delete();
			$filtered_venue_types = array();
			if ( $venue_ids ) {
				foreach ( $venue_ids as $i => $venue_id ) {
					if ( $venue_id != -1 ) {
						$event_meta = new Meta();
						$event_meta->type = 'event';
						$event_meta->type_id = $event->id;
						$event_meta->meta_key = 'linked_vendor';
						$event_meta->meta_value = abs( $venue_id );
						$event_meta->save();
						$filtered_venue_types[] = $venue_types[ $i ];
					}
				}
			}
			$event_meta = new Meta();
			$event_meta->type = 'event';
			$event_meta->type_id = $event->id;
			$event_meta->meta_key = 'venue_types';
			$event_meta->meta_value = json_encode($filtered_venue_types);
			$event_meta->save();
		}
		$customer_ids = $request->input( 'event_fields.customer_id' );
		$customer_ids = array_filter( $customer_ids, function( $customer_id ) {
			return -1 != $customer_id;
		} );
		$linked_customers = Meta::where(array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'linked_customer'
		))->get();
		// Create any extra meta that we need
		if ( $linked_customers->count() < count( $customer_ids ) ) {
			for ( $i = $linked_customers->count(); $i < count( $customer_ids ); $i ++ ) {
				$linked_customers->push(
					new Meta( array(
						'type' => 'event',
						'type_id' => $event->id,
						'meta_key' => 'linked_customer',
					) )
				);
			}
		}
		foreach ( $linked_customers as $i => $customer_meta ) {
			if ( isset( $customer_ids[ $i ] ) ) {
				if ( $customer_meta->meta_value != $customer_ids[ $i ] ) {
					$customer_meta->meta_value = $customer_ids[ $i ];
					$customer_meta->save();
				}
			} else {
				$customer_meta->delete();
			}
		}
		//Save event tags
		//TO DO - delete and save only the difference between what is saved and waht is sent
		$event_meta = Meta::where(array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'event_tags'
		))->delete();
		$event_tags = ( $event_tags = $request->input( 'event_fields.tags.event_tags' ) ) ? $event_tags : array();
		foreach( $event_tags as $tag ) {
			$event_meta = new Meta();
			$event_meta->type = 'event';
			$event_meta->type_id = $event->id;
			$event_meta->meta_key = 'event_tags';
			$event_meta->meta_value = $tag;
			$event_meta->save();
		}
		//
		if ( $duplicate ) {
			sc_duplicate_event_arrangements(intval($request->input('id', 0)), $event->id);
			$event_proposal = Event::where( array(
				'id' => $event->id,
				'user_id' => get_current_user_id(),
			))->with(array(
				'arrangements' => function($query) {
					$query->where('include', '=', 1);
				},
			))->first();

			$payments = Payment::where(array(
				'user_id' => get_current_user_id(),
				'event_id' => $request->input( 'id', 0 ),
				'is_live' => 0,
			))->get();

			foreach ( $payments as $payment ) {
				$payment = $payment->replicate();
				$payment->event_id = $event->id;
				$payment->status = 'not-paid';
				$payment->payment_date = null;
				$payment->payment_note = '';
				$payment->save();
			}
		} else if ($method === 'add') {
			$final_payment = new Payment();
			$final_payment->user_id = get_current_user_id();
			$final_payment->event_id = $event->id;
			$final_payment->payment_name = 'Final Payment';
			$final_payment->due_date = $event->date;
			$final_payment->payment_percentage = null;
			$final_payment->payment_date = null;
			$final_payment->amount = 0;
			$final_payment->payment_note = '';
			$final_payment->status = 'not-paid';
			$final_payment->payment_type = '';
			$final_payment->save();
		}
		$onboarding_tutorial = get_user_meta(get_current_user_id(), 'onboarding_tutorial', true);
		foreach ($onboarding_tutorial as $tutorial) {
			if ($tutorial == false) {
				update_user_meta(
					get_current_user_id(),
					'onboarding_tutorial',
					array(
						'step-one' => true,
						'step-two' => true,
						'step-three' => true,
					)
				);
				break;
			}
		}
		$r->add_payload( 'event_id', $event->id );
		$r->add_payload( 'event_url', add_query_arg( 'eid', $event->id, sc_get_proposal_2_0_page_url() ) );
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	} catch (Exception $e) {
		$r->fail($e->getMessage());
	}
	do_action( 'sc/event/edit/response', $r );
	$r->respond();
}
add_action('wp_ajax_sc_edit_user_event', 'sc_ajax_edit_user_event');

function sc_refresh_event_details_invoice_cat() {
	$request = Request::capture();
	$r = new Ajax_Response();

	if ($request->input('category_id') < 0) $r->fail('Category Id bellow 0');

	try {
		$invoice_categories_list = Invoicing_Category::where(array(
			'id' => $request->input('category_id'),
			'user_id' => get_current_user_id()
		))->firstOrFail();

		$values = array(
			'hardgood_multiple' => $invoice_categories_list->hardgood_multiple,
			'fresh_flower_multiple' => $invoice_categories_list->fresh_flower_multiple,
			'labor' => array(
				'global_labor_value' => $invoice_categories_list->global_labor_value,
				'flower' => $invoice_categories_list->flower_labor,
				'hardgood' => $invoice_categories_list->hardgood_labor,
				'base_price' => $invoice_categories_list->base_price_labor,
				'fee' => $invoice_categories_list->fee_labor,
				'rental' => $invoice_categories_list->rental_labor
			)
		);
		$r->add_payload('invoiceCategory', $values);

	} catch (Exception $e) {
		$r->fail('Category does not exist');
	}
	$r->respond();
}
add_action( 'wp_ajax_sc_refresh_event_details_invoice', 'sc_refresh_event_details_invoice_cat' );

// Responds with event data to prefill fields with
function sc_ajax_edit_event_form(){
	sc_clean_input_slashes();
	$request = Request::capture();

	$event = null;
	try {
		$event = Event::where(array(
			'id' => intval($request->input('id', 0)),
			'user_id' => get_current_user_id(),
		))->firstOrFail();
	} catch (Exception $e) {
		// user attempted to edit a non-existing event or an event he does not have access to OR user wants to create a new event
	}

	$event_date = '';
	if (!is_null($event)) {
		$event_date = sc_get_user_date_time(get_current_user_id(), $event->date->toDateTimeString());
	}

	$dateFormat = get_user_meta(get_current_user_id(), '__date_format', true);

	if ($dateFormat == 'd/m/Y') {
		$dateFormat = 'dd/mm/yy';
	} else if ($dateFormat == 'm/d/Y') {
		$dateFormat = 'mm/dd/yy';
	} else if ($dateFormat == 'd.m.Y') {
		$dateFormat = 'dd.mm.yy';
	} else if ($dateFormat == 'm.d.Y') {
		$dateFormat = 'mm.dd.yy';
	}

	$tags = array();

	if ( SCAC::can_access( SCAC::TAGGING ) ) {
		//Load event tags
		if ( $event ) {
			$event_tags_meta = Meta::where(array(
				'type' => 'event',
				'type_id' => $event->id,
				'meta_key' => 'event_tags'
			))->get();

			foreach ( $event_tags_meta as $meta ) {
				$tags[] = $meta->meta_value;
			}
		}

		//Load tags list
		$tagsList = get_user_meta(
			get_current_user_id(),
			'sc_user_tags',
			true
		);
		$tagsList = is_array( $tagsList ) ? wp_list_pluck( $tagsList, 'name' ) : array();
		$tagsList = array_values( array_unique( array_merge( $tagsList, $tags ) ) );
	} else {
		$tagsList = array();
	}

	if ( $request->input( 'duplicate' ) && ! empty( $event ) ) {
		$event->duplicate = 1;
	}

	$tax_rates = Tax_Rate::where(array(
		'user_id' => get_current_user_id()
	))->get()->toArray();

	$customer_ids = array();

	$tax_rate_ids = array();
	/**
	 * Negative values represent deleted or modified tax rates
	 */
	if (!is_null($event)) {

		$tax_rate_ids = array();
		foreach ( $tax_rates as $tax_rate ) {
			/** The value of the tax rate was modified */
			if ( $tax_rate['id'] == $event->tax_rate_id &&
				$tax_rate['value'] != $event->tax_rate_value ) {
				array_push($tax_rates, array(
					'id' => -$tax_rate['id'],
					'user_id' => get_current_user_id(),
					'name' => 'Modified Tax Rate',
					'value' => $event->tax_rate_value
				));
			} elseif ( $tax_rate['id'] == $event->tax_rate_id2 &&
				$tax_rate['value'] != $event->tax_rate_value2 ) {
				array_push( $tax_rates, array(
					'id' => -$tax_rate['id'],
					'user_id' => get_current_user_id(),
					'name' => 'Modified Tax Rate 2',
					'value' => $event->tax_rate_value2
				));
			}

			array_push( $tax_rate_ids, $tax_rate['id'] );
		}

		/**
		 * If the Event Tax Rate is not found in the User Tax Rates
		 * It means that it was deleted
		 */
		if ( (
				! in_array( $event->tax_rate_id, $tax_rate_ids ) &&
				! empty( $tax_rate_ids ) &&
				! is_null( $event->tax_rate_id ) &&
				$event->tax_rate_id != 0
			) || (
				empty( $tax_rate_ids ) && $event->tax_rate_id != 0
			) ) {
			array_push( $tax_rates, array(
				'id' => -$event->tax_rate_id,
				'user_id' => get_current_user_id(),
				'name' => 'Deleted Tax Rate',
				'value' => $event->tax_rate_value
			) );
		}

		if ( (
				! in_array( $event->tax_rate_id2, $tax_rate_ids ) &&
				! empty( $tax_rate_ids ) &&
				! is_null( $event->tax_rate_id2 ) &&
				$event->tax_rate_id2 != 0
			) || (
				empty( $tax_rate_ids ) && $event->tax_rate_id2 != 0
			) ) {
			array_push( $tax_rates, array(
				'id' => -$event->tax_rate_id2,
				'user_id' => get_current_user_id(),
				'name' => 'Deleted Tax Rate 2',
				'value' => $event->tax_rate_value2
			) );
		}

		$event_vendors_info = sc_get_user_event_vendors($event->id);
		$event_vendors['venue'] = $event_vendors_info['event_vendors']['venue'];
		$venues_list = $event_vendors_info['venues_list'];

		$customer_ids = sc_get_event_customers( $event->id, $event->user_id );
		$customer_ids = wp_list_pluck( $customer_ids, 'id' );

		$customers_list = sc_get_user_event_customers($event->id);
	} else {
		$venues_list = sc_get_user_venues_list();
		$customers_list = sc_get_user_customers_list();
	}

	if ( empty( $customer_ids ) ) {
		$customer_ids = array( -1 );
	}

	$js_data = apply_filters( 'sc/event/form/js_params', array(
		'event'			=> $event,
		'tags'			=> $tags,
		'customerIds'	=> $customer_ids,
		'customersList'	=> $customers_list,
		'taxRates'		=> $tax_rates,
		'tagsList'		=> $tagsList,
		'dateFormat'	=> $dateFormat,
	), $event );

	?>
	<div class="edit-event-form-shell"></div>
	<script type="text/javascript">
	var args = {
		node: $('.edit-event-form-shell:first'),
		layout: 'full',
		data: <?php echo json_encode( $js_data ); ?>
	};
	if (args.data.event) {
		args.data.event.date = <?php echo json_encode( $event_date ); ?>;
	}

	jQuery(document).trigger('stemcounter.action.renderEditEventForm', args);
	</script>
	<?php
	exit;
}
add_action('wp_ajax_sc_edit_event_form', 'sc_ajax_edit_event_form');

// Handles event publishing/archiving
function sc_ajax_archive_user_event(){
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$method = $request->input('method', 'archive');

	try {
		$event = Event::where(array(
			'id'=>intval($request->input('id', 0)),
			'user_id'=>get_current_user_id(),
		))->firstOrFail();

		if ($method == 'publish') {
			$event->status = $event::STATUS_PUBLISHED;
		} else {
			$event->status = $event::STATUS_ARCHIVED;
		}
		$event->save();
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	} catch (Exception $e) {
		$r->fail();
	}

	$r->respond();
}
add_action('wp_ajax_sc_archive_user_event_id', 'sc_ajax_archive_user_event');

// Handles event deletion
function sc_ajax_delete_user_event(){
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$event = Event::where(array(
			'id'=>intval($request->input('id', 0)),
			'user_id'=>get_current_user_id(),
		))->firstOrFail();

		$event_id = $event->id;

		Scheduled_Payment::where(array(
			'user_id' => get_current_user_id(),
			'event_id' => $event_id
		))->delete();

		Meta::where(array(
			'type' => 'event',
			'type_id' => $event_id
		))->delete();

		Meta::where(array(
			'type' => 'event',
			'type_id' => $event_id,
			'meta_key' => 'venue_types'
		))->delete();

		Meta::where(array(
			'type' => 'event',
			'type_id' => $event_id,
			'meta_key' => 'linked_vendor'
		))->delete();

		Meta::where(array(
			'type' => 'event',
			'type_id' => $event_id,
			'meta_key' => 'linked_customer'
		))->delete();

		$event->delete();

		$r->add_payload('event_id', $event_id);
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	} catch (Exception $e) {
		$r->fail();
	}

	$r->respond();
}
add_action('wp_ajax_sc_delete_user_event', 'sc_ajax_delete_user_event');

function sc_maybe_render_app_page_content( $content ) {
	// We currently only care about actual Pages
	if ( ! is_page() ) {
		return $content;
	}

	// What template does the current page use?
	$template = get_post_meta( get_the_ID(), '_wp_page_template', true );

	switch ( $template ) {
		case 'template-archive.php':
		case 'template-events.php':
			return apply_filters( 'sc/app/content/events', $content );
			break;

		case 'template-stemcounter.php':
			return apply_filters( 'sc/app/content/event', $content );
			break;

		case 'template-shopping.php':
			return apply_filters( 'sc/app/content/event_shopping', $content );
			break;

		case 'template-proposal.php':
			return apply_filters( 'sc/app/content/event_proposal', $content );
			break;

		case 'template-proposal-2.0.php':
			return apply_filters( 'sc/app/content/event_proposal_2_0', $content );
			break;

		case 'template-flowers.php':
			return apply_filters( 'sc/app/content/flowers', $content );
			break;

		case 'template-services.php':
			return apply_filters( 'sc/app/content/services', $content );
			break;
		case 'template-order.php':
			return apply_filters('sc/app/content/order', $content);
			break;
		case 'template-vendors.php':
			return apply_filters('sc/app/content/vendors', $content);
			break;
		case 'template-customers.php':
			return apply_filters('sc/app/content/customers', $content);
			break;
		case 'template-onboarding.php':
			return apply_filters('sc/app/content/onboarding', $content);
			break;
		case 'template-proposal-2.0_versions.php':
			return apply_filters( 'sc/app/content/event_proposal_2_0_versions', $content );
			break;
		case 'template-recipes.php':
			return apply_filters( 'sc/app/content/recipes', $content );
			break;
		case 'template-studio.php':
			return apply_filters( 'sc/app/content/studio', $content );
			break;
		case 'template-forms.php':
			return apply_filters( 'sc/app/content/forms', $content );
			break;
		default:
			return $content;
			break;
	}
}
add_filter( 'the_content', 'sc_maybe_render_app_page_content', 10 );

function sc_count_events( $group = false ) {
	global $view;

	$view = empty($view) ? 'events' : $view;
	$user_id = get_current_user_id();
	$states = array();

	if ( ! $group ) {
		$states = array( 'inquiry', 'proposal_sent', 'booked', 'completed' );
	} else {
		$states = array( $group );
	}

	$status = ($view == 'archive') ? Event::STATUS_ARCHIVED : Event::STATUS_PUBLISHED;
	$events = Event::where('user_id', $user_id)
		->where('status', $status)
		->whereIn('state', $states, 'and')
		->orderBy('date', 'asc')
		->get();

	if ( ! empty( $events ) ) {
		return count( $events );
	} else {
		return '0';
	}
}

function sc_get_events( $state, $with_details = false ) {
	global $view;

	$view = empty($view) ? 'events' : $view;
	$user_id = get_current_user_id();
	$states = array();

	if ( ! $state ) {
		//$states = array( 'inquiry', 'proposal_sent', 'booked', 'completed' );
		$states = array( 'inquiry', 'proposal_sent', 'booked', 'completed', 'template');
	} else {
		$states = array( $state );
	}

	$status = ($view == 'archive') ? Event::STATUS_ARCHIVED : Event::STATUS_PUBLISHED;

	if ( $with_details ) {
		$events = Event::where('user_id', $user_id)
			->where('status', $status)
			->with( array(
				'arrangements' => function($query){
					$query->orderBy( 'order', 'asc' );
				},
				'arrangements.items',
				'arrangements.recipes',
				'arrangements.items.item_variation',
				'arrangements.items.item_variation.item',
				'live_payments',
			) )
			->whereIn('state', $states, 'and')
			->orderByRaw("(CASE WHEN state='template' THEN 0 ELSE 1 END) ASC")
			->orderBy('date', 'asc')
			->get();
	} else {

		$events = Event::where('user_id', $user_id)
			->where('status', $status)
			->whereIn('state', $states, 'and')
			->orderByRaw("(CASE WHEN state='template' THEN 0 ELSE 1 END) ASC")
			->orderBy('date', 'asc')
			->get();
	}

	return $events;
}


function sc_render_manage_events( $content ) {
	global $view;

	if ( isset( $_GET['event_group'] ) && ! empty( $_GET['event_group'] ) ) {
		$state = $_GET['event_group'];
	} else {
		$state = '';
	}

	if ( ! empty( $state ) && ! in_array( $state, array( 'inquiry', 'proposal_sent', 'booked', 'completed' ) ) ) {
		$events = false;
	} else {
		$events = sc_get_events( $state, false );
	}

	$customer_properties = apply_filters( 'sc/react/data/customer', false, false );

	$table_len = get_user_meta( get_current_user_id(), 'sc_event_table_len', true );
	ob_start(); ?>
	<div class="row" style="display: none">
		<div class="col-lg-2">
			<!--<h3><i class="fa fa-angle-right"></i> Events</h3>-->
		</div>

		<div class="col-lg-10 clearfix">
			<button class="btn btn-primary btn-lg pull-right add-event-button" style="margin-top: 10px; margin-bottom:10px;">
				New Event
			</button>
		</div>
	</div><!-- /row -->

	<?php if ( $content ) : ?>
		<div class="row">
			<?php echo $content; ?>
		</div>
	<?php endif; ?>

	<div class="row mt">
		<?php //if ( 'archive' != $view && 'template' != $state ) {
			if ( 'template' != $state ) { ?>
			<div class="col-md-12">
				<?php $total_inquiries = sc_count_events( 'inquiry' );
				$total_proposals = sc_count_events( 'proposal_sent' );
				$total_booked = sc_count_events( 'booked' );
				$total_completed = sc_count_events( 'completed' ); ?>
				<div class="pipeline-section clearfix">
					<?php do_action( 'sc/events/pipeline/start', $state ); ?>
					<a class="pipeline-block all-events <?php echo ! $state ? 'current' : ''; ?>" href="<?php echo esc_url( remove_query_arg( 'event_group' ) ); ?>">
						<span class="value"><?php echo sc_count_events( '' ); ?></span>
						<span class="desc">
							All
							<?php if ( 'archive' == $view ) {
								echo 'Archived';
							} ?>
						</span>
					</a>
					<span class="pipeline-part pipeline-part-leads">
						<span class="pipeline-label">All Leads: <?php echo $total_inquiries + $total_proposals; ?></span>
						<a class="pipeline-block inquiry-events <?php echo $state == 'inquiry' ? 'current' : ''; ?>" href="<?php echo esc_url( add_query_arg( 'event_group', 'inquiry' ) ); ?>" data-state="inquiry">
							<span class="value"><?php echo $total_inquiries; ?></span>
							<span class="desc">Inquiry</span>
						</a>
						<a class="pipeline-block proposal-events <?php echo $state == 'proposal_sent' ? 'current' : ''; ?>" href="<?php echo esc_url( add_query_arg( 'event_group', 'proposal_sent' ) ); ?>" data-state="proposal_sent">
							<span class="value"><?php echo $total_proposals; ?></span>
							<span class="desc">Proposal Sent</span>
						</a>
					</span>
					<span class="pipeline-part pipeline-part-booked">
						<span class="pipeline-label">All Booked: <?php echo $total_booked + $total_completed; ?></span>
						<a class="pipeline-block booked-events <?php echo $state == 'booked' ? 'current' : ''; ?>" href="<?php echo esc_url( add_query_arg( 'event_group', 'booked' ) ); ?>" data-state="booked">
							<span class="value"><?php echo $total_booked; ?></span>
							<span class="desc">Booked</span>
						</a>
						<a class="pipeline-block completed-events <?php echo $state == 'completed' ? 'current' : ''; ?>" href="<?php echo esc_url( add_query_arg( 'event_group', 'completed' ) ); ?>" data-state="completed">
							<span class="value"><?php echo $total_completed; ?></span>
							<span class="desc">Completed</span>
						</a>
					</span>
				</div>
			</div>
		<?php } ?>
		<div class="col-md-12">
			<div class="content-panel">
				<?php if ( false !== $events ) : ?>
					<table id="eventsTable" class="table table-striped table-advance table-hover user_ev_meta" data-page-length="<?php echo ! empty( $table_len ) ? $table_len : 10 ; ?>">
						<thead>
							<tr>
								<th colspan="5">
									<a class="add-event-button cta-link">
										+ Add New Event
									</a>
									<div class="events-select-ctrl">
										<?php if ( 'archive' != $view ) : ?>
											<a href="#" data-state="archived"><i class="fa fa-folder-open-o" data-state="archived"></i> Archive selected</a>
										<?php else : ?>
											<a href="#" data-state="published"><i class="fa fa-folder-open-o" data-state="published"></i> Publish selected</a>
										<?php endif; ?>
									</div>
								</th>
							</tr>
							<tr>
								<!-- <th class="events-select-all" ><input name="select_all" value="1" type="checkbox"></th> -->
								<th></th>
								<th>EVENTS</th>
								<th class="hidden-xs"><!-- ACTION BUTTONS --></th>
								<th class="hidden-xs">DATE</th>
								<?php if ($view == 'events') : ?>
									<th class="hidden-xs">
										<span class="hidden-xs">STAGE</span>
									</th>
								<?php else:  ?>
									<th class="hidden-xs"></th>
								<?php endif; ?>
							</tr>
						</thead>
						<tbody>
							<?php if ($events->count() == 0) : ?>
								<tr>
									<th colspan="4" style="text-align: center;">No Events</th>
								</tr>
							<?php else : ?>
								</tr>
									<?php foreach ($events as $event) :
									if ( SCAC::can_access( SCAC::TAGGING ) ) {
										$_tags = Meta::where(array(
											'type' => 'event',
											'type_id' => $event->id,
											'meta_key' => 'event_tags'
										))->get();
										$tags = array();
										$tags_classes = array();
										foreach($_tags as $tag) {
											$tag_class = esc_attr( sc_convert_tag_name_to_class( $tag->meta_value ) );
											//$tags[] = '<span class="tag event_tag">' . $tag->meta_value . '</span>';
											$tags[] = '<a href="javascript:void(0)"  class="btn btn-xs btn-default tag event-tag ' .
												$tag_class . '" ' .
												'data-tag="' . esc_attr( $tag->meta_value ) . '" '.
												'data-tag-class="' . $tag_class . '" '.
												'data-eventid="' . esc_attr( $event->id) . '" '.
												'><span class="glyphicon glyphicon-remove"></span> ' .
												$tag->meta_value . '</a>';

											$tags_classes[] = $tag_class;
										}
										$tags = implode(' ', $tags );
										$tags_classes = implode(' ', $tags_classes );
									} else {
										$tags = '';
										$tags_classes = '';
									}

									$permalink = add_query_arg( 'eid', $event->id, sc_get_proposal_2_0_page_url() );
									?>
									<tr class="user_event_<?php echo $event->id; ?> event_state_<?php echo $event->state; ?> user_event_row <?php echo $tags_classes; ?>"  >
										<td class="events-select-single" >
										<?php if ( 'template' !== $event->state ): ?>
											<input type="checkbox" value="<?php echo intval( $event->id ); ?>">
										<?php endif; ?>
										</td>
										<td class="event-name-column">
											<div class="event-title">
												<a href="<?php echo $permalink; ?>" class="event-title">
													<?php echo esc_html($event->name); ?>
												</a>

												<div class="event-popup-info" data-eventid="<?php echo $event->id ?>">
													Loading info ...
												</div>
											</div>
											<div class="tags event-tags"><?php echo $tags; ?></div>
											<?php // The next section is a clone of content from next table cells in table,
												// because of the responsive design needs. If you eidit something, edit it
												// in both places */
											?>
											<div class="visible-xs">
												<?php if('template' !== $event->state) : ?>
													<?php if ($view == 'events') : ?>
														<br />
														<?php echo esc_html(sc_get_user_date_time(get_current_user_id(), $event->date->toDateTimeString())); ?>
													<!-- <a href="<?php echo $permalink; ?>">?</a> -->
														<a href="#" onclick="event_archive('<?php echo $event->id; ?>'); return false;" title="Archive...">
															<i class="fa fa-folder-open-o"></i>
														</a>
													<?php elseif ($view == 'archive') : ?>
														<a href="#" onclick="event_unarchive('<?php echo $event->id; ?>'); return false;" title="Unarchive...">
															<i class="fa fa-folder-open-o"></i>
														</a>
													<?php endif; ?>
												<?php endif; ?>
												<a class="duplicate-event-button" data-id="<?php echo $event->id; ?>" href="#" title="Duplicate...">
													<i class="fa fa-clone"></i>
												</a>
												<a href="#"
													rel="<?php echo $event->id; ?>"
													title="<?php echo esc_attr($event->name); ?>"
													class="delEvent">
													<i class="fa fa-trash-o"></i>
												</a>

											<?php if ($view == 'events') : ?>
												<select class="events-select-group" data-event-id="<?php echo $event->id; ?>" data-selected-state="<?php echo $event->state; ?>">
													<option value="inquiry" <?php selected( $event->state, 'inquiry' ); ?>>Inquiry</option>
													<option value="proposal_sent" <?php selected( $event->state, 'proposal_sent' ); ?>>Proposal Sent</option>
													<option value="booked" <?php selected( $event->state, 'booked' ); ?>>Booked</option>
													<option value="completed" <?php selected( $event->state, 'completed' ); ?>>Completed</option>
													<option value="template" <?php selected( $event->state, 'template' ); ?>>Template</option>
												</select>
											<?php endif;?>

											</div>
											<?php // End of the responsve design coned section ?>



										</td>
										<td  class="hidden-xs command-icons">
											<?php if('template' !== $event->state) : ?>
												<a href="#" class="edit-event-button" data-id="<?php echo $event->id; ?>" title="Edit...">
													<i class="fa fa-pencil"></i>
												</a>
												<?php if ($view == 'events') : ?>
													<!-- <a href="<?php echo $permalink; ?>">?</a> -->
													<a href="#" onclick="event_archive('<?php echo $event->id; ?>'); return false;" title="Archive...">
														<i class="fa fa-folder-open-o"></i>
													</a>
												<?php elseif ($view == 'archive') : ?>
													<a href="#" onclick="event_unarchive('<?php echo $event->id; ?>'); return false;" title="Unarchive...">
														<i class="fa fa-folder-open-o"></i>
													</a>
												<?php endif; ?>
											<?php endif; ?>
											<a class="duplicate-event-button" data-id="<?php echo $event->id; ?>" href="#" title="Duplicate...">
												<i class="fa fa-clone"></i>
											</a>
											<a href="#"
												rel="<?php echo $event->id; ?>"
												title="Delete <?php echo esc_attr($event->name); ?>"
												class="delEvent">
												<i class="fa fa-trash-o"></i>
											</a>

											<!-- hidden section from the old design - for refference only -->
											<div class="btn-group events-button-group" style="display: none">
												<button type="button" class="btn btn-theme03 hidden-xs dropdown-toggle" data-toggle="dropdown">Action</button>
												<button type="button" class="btn btn-theme03 dropdown-toggle" data-toggle="dropdown">
													<span class="caret"></span>
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul class="dropdown-menu" role="menu">
													<?php if ($view == 'events') : ?>
														<li><a href="<?php echo $permalink; ?>">Go</a></li>
														<li><a href="#" onclick="event_archive('<?php echo $event->id; ?>'); return false;">Archive</a></li>
													<?php elseif ($view == 'archive') : ?>
														<li><a href="#" onclick="event_unarchive('<?php echo $event->id; ?>'); return false;">Unarchive</a></li>
													<?php endif; ?>
													<li>
														<a href="#" class="edit-event-button" data-id="<?php echo $event->id; ?>">Edit</a>
													</li>
													<li><a class="duplicate-event-button" data-id="<?php echo $event->id; ?>" href="#">Duplicate</a></li>
													<li class="divider"></li>
													<li>
														<a
															href="#"
															rel="<?php echo $event->id; ?>"
															title="<?php echo esc_attr($event->name); ?>"
															class="delEvent">
															Delete
														</a>
													</li>
												</ul>
											</div>
											<!-- end of hidden section -->
										</td>
										<td  class="hidden-xs">
											<?php if('template' !== $event->state) : ?>
												<span style="display: none"><?php echo strtotime( $event->date ); ?></span>
												<?php echo esc_html(sc_get_user_date_time(get_current_user_id(), $event->date->toDateTimeString())); ?>
											<?php endif; ?>
										</td>
										<?php if ($view == 'events') : ?>
											<td  class="hidden-xs">
											<span style="display: none"><?php echo $event->state; ?></span>
												<select class="events-select-group" data-event-id="<?php echo $event->id; ?>" data-selected-state="<?php echo $event->state; ?>">
													<option value="inquiry" <?php selected( $event->state, 'inquiry' ); ?>>Inquiry</option>
													<option value="proposal_sent" <?php selected( $event->state, 'proposal_sent' ); ?>>Proposal Sent</option>
													<option value="booked" <?php selected( $event->state, 'booked' ); ?>>Booked</option>
													<option value="completed" <?php selected( $event->state, 'completed' ); ?>>Completed</option>
													<option value="template" <?php selected( $event->state, 'template' ); ?>>Template</option>
												</select>
											</td>
										<?php else : ?>
											<td  class="hidden-xs"></td>
										<?php endif; ?>
									</tr>
								<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					<div class="customer-properties-sidebar container slide-out-sidebar">
						<a href="#" class="close-btn btn btn-primary button-submit">Close</a>

						<div  class="builder-form">
							<form id="edit-customer-form" class="form-horizontal style-form customer-form" method="post">
							</form>
						</div>

					</div>
				<?php else :
					do_action( "sc/events/load/{$state}" );
				endif; ?>
			</div><!-- /content-panel -->
		</div><!-- /col-md-12 -->
	</div><!-- /row -->

	<?php if ( false !== $events ) : ?>
		<?php if ( SCAC::can_access( SCAC::TAGGING ) ) :
			$event_tags_meta = get_user_meta( get_current_user_id(), 'sc_user_tags', true );
			$tagsList = array();
			foreach ( $event_tags_meta as $tag ) {
				$tagsList[] = '<a class="btn btn-xs btn-default tag events-tag" data-tag="' . esc_attr( $tag['name'] )	.
				'" data-tag-class="' . esc_attr( sc_convert_tag_name_to_class( $tag['name'] ) ) .
				'" href="javascript: void(0)" title="Show only events having this tag">' .
				$tag['name'] . '</a>';
			}
			$tagsList = join(' ', $tagsList); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="events-tags">
						<?php echo $tagsList; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>

		<div class="row">
			<div class="event-types">
				<?php if ($view == 'events') : ?>
					<a class="pull-left" href="<?php echo sc_get_events_archive_page_url(); ?>">Archived Events</a>
				<?php else : ?>
					<a class="pull-left" href="<?php echo sc_get_events_page_url(); ?>">Unarchived Events</a>
				<?php endif; ?>
				<?php if ( 'template' == $state ): ?>
					<a class="pull-right template-button" href="<?php echo esc_url( remove_query_arg( 'event_group', home_url( '/' ) . 'events/' ) ); ?>">Events</a>
				<?php else: ?>
					<a class="pull-right template-button" href="<?php echo esc_url( add_query_arg( 'event_group', 'template', home_url( '/' ) . 'events/' ) ); ?>">Templates</a>
				<?php endif; ?>
			</div><!-- /col-md-12 -->
		</div><!-- /row -->
		<script type="text/javascript">
		(function($){
			$(document).ready(function(){
				// Array holding selected row IDs
				var rows_selected = [];

				var table = $("#eventsTable").DataTable({
					'rowCallback': function(row, data, dataIndex){
						// Get row ID
						var rowId = data[0];

						// If row ID is in the list of selected row IDs
						if ($.inArray(rowId, rows_selected) !== -1) {
							$(row).find('input[type="checkbox"]').prop('checked', true);
							$(row).addClass('selected');
						}
					},
					"order": [ [3, "asc"] ],
					"fnDrawCallback": function(){
						$('.events-select-group').select2();
					}
					//"orderCellsTop" : true
				});

				var states_labels = {
					'inquiry': 'Inquiry',
					'proposal_sent': 'Proposal Sent',
					'booked': 'Booked',
					'completed': 'Completed',
					'archived': 'Archived'
				};

				function mass_set_state( state ) {
					alertify.confirm(
						'Are you sure you want to mark the <strong>' + rows_selected.length + '</strong> selected event' + ( rows_selected.length == 1 ? '' : 's' ) + ' as "' + states_labels[ state ] + '"?',
						function() {
							var url = window.stemcounter.aurl({ action: 'sc_mass_event_edit' }),
								data = {
									'events': rows_selected,
									'selected_action': state
								};

							$.post(url, data, function (response) {
								stemcounter.JSONResponse(response, function (r) {
									if ( ! r.success ) {
										console.log( r );
									} else {
										for (var i = 0; i < rows_selected.length; i++) {
											if ( 'archived' == state || 'published' == state ) {
												table.row( '.user_event_' + rows_selected[i] ).remove().draw();
											} else {
												if ( 'template' == state ) {
													table.cell( '.user_event_' + rows_selected[i] + ' > td:nth-child(3)' ).data('<td class="hidden-xs command-icons"></td><a class="duplicate-event-button" data-id="' + rows_selected[i] + '" href="#" title="Duplicate..."><i class="fa fa-clone"></i></a><a href="#" rel="'+rows_selected[i]+'" class="delEvent"><i class="fa fa-trash-o"></i></a>'
													);

													table.cell( '.user_event_' + rows_selected[i] + ' > td:nth-child(4)' ).data('<td class="hidden-xs"></td>');
													$('.user_event_' + rows_selected[i] + '').addClass('event_state_template');
												}
												table.cell( '.user_event_' + rows_selected[i] + ' > td:nth-child(5)' ).data(
													'<td class="hidden-xs">\
														<span style="display: none">' + rows_selected[i] + '</span>\
														<select class="events-select-group" data-event-id="'+ rows_selected[i] +'" data-selected-state="'+state+'">\
															<option value="inquiry" '+ ('inquiry' == state ? 'selected' : '') +'>Inquiry</option>\
															<option value="proposal_sent" '+ ('proposal_sent' == state ? 'selected' : '') +'>Proposal Sent</option>\
															<option value="booked" '+ ('booked' == state ? 'selected' : '') +'>Booked</option>\
															<option value="completed"'+ ('completed' == state ? 'selected' : '') +'>Completed</option>\
															<option value="template" '+ ('template' == state ? 'selected' : '') +'>Template</option>\
														</select>\
													</td>'
												);
												table.draw();
											}
										}
										rows_selected = [];
										$('#eventsTable tbody input[type="checkbox"]:checked').trigger('click');
										$('#main-content > .wrapper').removeClass('mass-editing');
										alertify.success( r.payload.message );
									}
								});
							});
						}
					);
				}

				$('#main-content').on('click', '.mass-editing .pipeline-part-leads a, .mass-editing .pipeline-part-booked a, .mass-editing .events-select-ctrl a', function(e){
					e.preventDefault();

					var state = $(this).data('state');
					mass_set_state( state );

				}).on('click', '.mass-editing .events-tags a', function(e){
					//Assign single tag to selected events
					var tag = $(this).attr('data-tag');
					var tag_class = $(this).attr('data-tag-class');

					jQuery.post({
						url: window.stemcounter.ajax_url,
						data: {
							action: 'sc_edit_event_tags',
							selected_action: 'mass_assign',
							selected_events: rows_selected,
							tag: tag
						},
						success:function( response ){
							stemcounter.JSONResponse(response, function(r){
								if (response.success) {
									$( '.user_event_row.selected' ).each( function() {
										var event_id = $( this ).find('.events-select-single input[type=checkbox]').val();
										if ( ! $( this ).find( '.'+ tag_class ).length ) {
											$( this ).find('.event-tags').append( response.payload.html_tag );
											$( this ).find('.event-tags .tag').attr('data-eventid', event_id);
										}
										$( this ).addClass( tag_class );
									});
								}
							});
						}
					});

				}).on('click', '.wrapper:not(.mass-editing) .events-tags a', function(e){
					//Filter rows, based on clicked/selected tags
					$( this ).toggleClass( 'filter' );
					if ( ! $(this).hasClass('filter') ) {
						$(this).blur();
					}

					table.draw();
				}).on('click', '.event-tags .tag', function(e){
					//Delete tag from the specific event's row, when clicked
					e.preventDefault();
					var $_this = $(this);

					jQuery.post({
						url: window.stemcounter.ajax_url,
						data: {
							action: 'sc_edit_event_tags',
							selected_action: 'delete',
							tag: $(this).attr('data-tag'),
							event_id: $(this).attr('data-eventid')
						},
						success:function( response ){
							stemcounter.JSONResponse(response, function(r){
								if (response.success) {
									var tag_class = $_this.attr('data-tag-class');
									$_this.closest('tr.user_event_row').removeClass(tag_class);
									$_this.remove();
									table.draw();
								}
							});
						}
					});

				});
 
 				//Load info for the event title tooltip, on mouse hover
				$('#eventsTable').on('mouseover', '.event-title', function(e) {
					var $tooltip = $( this ).find( '.event-popup-info' );
					if ( ! $tooltip.data('eventid') ) {
						return;
					}

					if ( $tooltip.data( 'sync' ) ) {
						//console.log( 'no need to load' );
					} else {
						$tooltip.data( 'sync', 1 );
						
						jQuery.post({
							url: window.stemcounter.ajax_url,
							data: {
								action: 'sc_get_event_tipinfo',
								event_id: $tooltip.data('eventid')
							},
							success:function( response ){
								stemcounter.JSONResponse(response, function(r){
									if (response.success) {
										$tooltip.html( response.payload.tooltip_html );
									} else {
										$tooltip.data( 'sync', 0 );
									}
								});
							}, 
							error:function(){
								$tooltip.data( 'sync', 0 );
							}
						});
					}
				});

				$('#eventsTable tbody tr').on('click', 'input[type="checkbox"]', function(e){
					var $row = $(this).closest('tr');
					var rowId = $(this).val();
					// Determine whether row ID is in the list of selected row IDs
					var index = $.inArray(rowId, rows_selected);

					// If checkbox is checked and row ID is not in list of selected row IDs
					if(this.checked && index === -1){
						rows_selected.push(rowId);

						// Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
					} else if (!this.checked && index !== -1){
						rows_selected.splice(index, 1);
					}

					if(this.checked){
						$row.addClass('selected');
					} else {
						$row.removeClass('selected');
					}

					if ( rows_selected.length ) {
						$('#main-content > .wrapper').addClass('mass-editing');
					} else {
						$('#main-content > .wrapper').removeClass('mass-editing');
					}

					if ( $('.user_event_row.selected').length ) { //There are selected items
						$('.events-tags .tag').attr('title', 'Assign this tag to selected items');
					} else { //No items selected
						$('.events-tags .tag').attr('title', 'Show only events having this tag');
					}

					/*console.log( rows_selected );*/

					// Prevent click event from propagating to parent
					e.stopPropagation();
				});

				// Handle click on table cells with checkboxes
				$('#eventsTable').on('click', 'tbody td:first-child', function(e){
					$(this).parent().find('input[type="checkbox"]').trigger('click');
				});

				$('#eventsTable').on( 'length.dt', function ( e, settings, len ) {

					var url = window.stemcounter.aurl({ action: 'sc_datatables_len' }),
						data = {
							'meta_key': 'sc_event_table_len',
							'meta_value': len
						};

					$.post(url, data, function (response) {
						stemcounter.JSONResponse(response, function (r) {
							if ( ! r.success ) {
								console.log( r );
							}
						});
					});
				});

				$('.edit-customer-button').click(function(e) {
					e.preventDefault();
					openEditCustomerPopup($(this).data('customerId'));
				});

				/*$('.event-tags .tag').on('click', function(e){
					e.preventDefault();
					var $_this = $(this);

					jQuery.post({
						url: window.stemcounter.ajax_url,
						data: {
							action: 'sc_edit_event_tags',
							selected_action: 'delete',
							tag: $(this).attr('data-tag'),
							event_id: $(this).attr('data-eventid')
						},
						success:function( response ){
							stemcounter.JSONResponse(response, function(r){
								if (response.success) {
									var tag_class = $_this.attr('data-tag-class');
									$_this.closest('tr.user_event_row').removeClass(tag_class);
									$_this.remove();
									table.draw();
								}
							});
						}
					});
				});*/

				$.fn.dataTable.ext.search.push(
					function( settings, data, dataIndex ) {
						if ( $( '#main-content .mass-editing' ).length ) {
							return true;
						}
						if ( $( '.events-tags .filter' ).length ) {
							var found = true;
							$( '.events-tags .filter' ).each( function() {
								var tag_class = $( this ).attr('data-tag-class');
								if ( ! $( table.row( dataIndex ).node() ).is( '.' + tag_class ) ) {
									found = false;
									return false;
								}
							} );
							return found;
						} else {
							return true;
						}
					}
				);


			});
		})(jQuery);

		function openEditCustomerPopup(customerId, title) {
			title = (typeof title == 'undefined') ? 'Edit Customer' : title;
			var url = stemcounter.aurl({
				action: 'sc_edit_user_customer_form',
				customer_id: customerId
			});

			stemcounter.openAjaxModal(title, url);
		}

		stemcounter = stemcounter || {};
		stemcounter.customer_properties = <?php echo json_encode( $customer_properties ); ?>
		</script>
	<?php
	endif;

	return ob_get_clean();
}
add_filter( 'sc/app/content/events', 'sc_render_manage_events' );

function sc_render_event_shopping( $content ) {
	$user_id = intval( get_current_user_id() );
	$request = Request::capture();
	$event_id = intval( $request->input( 'eid' ) );

	// get all arrangement items only for arrangements included in the proposal
	$event = Event::where( array(
		'id' => $event_id,
	) )->with( array(
		'arrangements' => function( $query ) {
			$query->where( 'include', '=', 1 )->where( 'addon', '=', 0 )->orderBy( 'order', 'asc' );
		},
	), 'arrangements.items', 'arrangements.items.item_variation', 'arrangements.items.item_variation.item')->first();

	$shopping_list = array(
		'flower' => array(),
		'service' => array(),
	);

	//Somehow adding arrangements.recipes to ->with('') clause in previous db query do not work,
	// so we get recipe names with another query
	$RecipeNames = array();
	$EventRecipes = Event::where( array(
		'id' => $event_id,
	))->with('arrangements.recipes')->first();
	foreach ($EventRecipes->arrangements as $arrangement) {
		foreach ($arrangement->recipes as $arrangement_recipe) {
			$RecipeNames[$arrangement_recipe->id] = $arrangement_recipe->name;
		}
	}
	//Create array, with unique items and their recipe names which the items are in.
	$breakdown = array();
	foreach ($event->arrangements as $arrangement) {
		foreach ($arrangement->items as $arrangement_item) {
			$unique_item_id = $arrangement_item->item_id . '#' . $arrangement_item->item_variation_id;
			if ( ! isset( $breakdown[ $unique_item_id ] ) ) {
				$breakdown[ $unique_item_id ] = array();
			}

			if ( !isset( $breakdown[$unique_item_id][$arrangement->id] ) ) {
				$breakdown[$unique_item_id][$arrangement->id] = array(
					'quantity' => $arrangement_item->quantity,
					'arrangement_name' => $arrangement->name,
					'arrangement_quantity' => $arrangement->quantity,
					'recipes' => array(),
					);
			} else {
				$breakdown[$unique_item_id][$arrangement->id]['quantity'] += $arrangement_item->quantity;
			}
			if ( !isset( $breakdown[$unique_item_id][$arrangement->id]['recipes'][ $arrangement_item->arrangement_recipe_id ] ) ) {
				$breakdown[$unique_item_id][$arrangement->id]['recipes'][ $arrangement_item->arrangement_recipe_id ] = array (
					'quantity' => $arrangement_item->quantity,
					'name' => $RecipeNames[ $arrangement_item->arrangement_recipe_id ],
				);
			} else {
				$breakdown[$unique_item_id][$arrangement->id]['recipes'][ $arrangement_item->arrangement_recipe_id ]['quantity'] += $arrangement_item->quantity;
			}
		}
	}

	$user_measuring_units = get_user_meta( get_current_user_id(), 'sc_user_measuring_units', true );
	$user_measuring_units = Measuring_Unit::whereIn( 'id', $user_measuring_units )->get();

	foreach ($event->arrangements as $arrangement) {
		foreach ($arrangement->items as $arrangement_item) {
			$type = sc_get_simple_item_type($arrangement_item->item, false, $arrangement_item);
			$type_label = sc_get_item_type($arrangement_item->item, true, $arrangement_item);
			$name = $arrangement_item->get_full_name() . '_' . $arrangement_item->item->id;
			$quantity = $arrangement_item->quantity * $arrangement->quantity;
			$item_cost = sc_format_price( $arrangement_item->cost );

			$items_su = $arrangement_item->item->purchase_qty;
			//if is deleted we need to know how much SU does that flower have
			if ( is_null( $items_su ) ) {
				try {
					$item = Item::where( array(
                        'id' => $arrangement_item->item_id
                    ) )->withTrashed()->firstOrFail()->toArray();
	                $items_su = $item['purchase_qty'];
				} catch (Exception $e) {
					continue;
				}
			}

			$su_needed = ceil( $quantity / $items_su );
			$total_cost = ( $items_su * $su_needed ) * $item_cost;
			$total_cost = $total_cost;
			$leftovers = ( $items_su * $su_needed ) - $quantity;

			$unique_item_id = $arrangement_item->item_id . '#' . $arrangement_item->item_variation_id;
			$variation_id = $arrangement_item->item_id . '_' . $arrangement_item->item_variation_id;
			if (!isset($shopping_list[$type][$name])) {
				$shopping_list[$type][$name] = array(
					'type'=>$type_label,
					'name'=>$arrangement_item->get_full_name(),
					'quantity'=>$quantity,
					'cost'=>$total_cost,
					'breakdown_id' => $unique_item_id,
					'variation_id' => $variation_id,
				);

				$shopping_list[$type][$name]['items_su'] = $items_su;
				$shopping_list[$type][$name]['su_needed'] = $su_needed;
				$shopping_list[$type][$name]['leftovers'] = $leftovers;
			} else {
				$shopping_list[$type][$name]['quantity'] += $quantity;

				$su_needed = ceil($shopping_list[$type][$name]['quantity'] / $items_su);
				$total_cost = ($items_su * $su_needed) * $item_cost;
				$total_cost = $total_cost;
				$leftovers = ($items_su * $su_needed) - $shopping_list[$type][$name]['quantity'];

				$shopping_list[$type][$name]['su_needed'] = $su_needed;
				$shopping_list[$type][$name]['cost'] = $total_cost;
				$shopping_list[$type][$name]['leftovers'] = $leftovers;
			}

			if ( 'consumable' == $arrangement_item->item->type ) {
				foreach ( $user_measuring_units as $key => $unit ) {
					if ( $unit->id == $arrangement_item->item->purchase_unit ) {
						if ( 1 == $shopping_list[$type][$name]['items_su'] ) {
							$shopping_list[$type][$name]['units'] = $unit->short;
						} else {
							$shopping_list[$type][$name]['units'] = $unit->short_plural;
						}

						if ( 1 == $shopping_list[$type][$name]['su_needed'] ) {
							$shopping_list[$type][$name]['needed_units'] = $unit->short;
						} else {
							$shopping_list[$type][$name]['needed_units'] = $unit->short_plural;
						}

						if ( 1 == $shopping_list[$type][$name]['leftovers'] ) {
							$shopping_list[$type][$name]['leftovers_units'] = $unit->short;
						} else {
							$shopping_list[$type][$name]['leftovers_units'] = $unit->short_plural;
						}
					}
				}
			}
		}
	}

	ksort($shopping_list['flower']);
	usort($shopping_list['service'], 'sc_shopping_list_sort');

	ob_start(); ?>
	<style>
		<?php
			render_itemslist_style( $event->user_id );
			do_action( 'sc/event/render_styles', $event );
		?>
	</style>
	<div class="proposal-container new-proposal-container container">
		<div id="event-proposal">
			<div class="proposal-wrapper">
				<div class="proposal-toolbar">
					<div class="row">
						<div class="col-sm-12">
							<div class="pull-left event-breadcrumbs">
								<a href="<?php echo esc_url( sc_get_permalink_by_template( 'template-events.php' ) ); ?>" class="events-link">Events</a> > <span class="event-name"><?php echo $event->name; ?> &mdash; Shopping List</span>
							</div>
							<div class="toolbar-action pull-right"><a href="<?php echo esc_url( add_query_arg( 'eid', $event->id, sc_get_proposal_2_0_page_url() ) ); ?>">Back to the proposal</a></div>
						</div>
					</div>
				</div>

				<div class="container shopping-list-container">
					<div class="row">
						<div class="col-lg-12">
							<div class="recipe-sheet-settings">
								<label><input checked id="arr-su-needed" data-classes-linked=".shopping-td-su-needed" type="checkbox" name="s-units-needed">Purchase units needed</label>
								<label><input checked id="arr-cost" data-classes-linked=".shopping-td-cost" type="checkbox" name="cost">Expected Cost</label>
								<label><input checked id="arr-leftovers" data-classes-linked=".shopping-td-leftovers" type="checkbox" name="leftovers">Leftovers</label>
								<label><input checked id="arr-type" data-classes-linked=".shopping-td-type" type="checkbox" name="notes">Type</label>
								<label><input id="hide_breakdown" data-classes-linked=".breakdown" type="checkbox" name="breakdown">Breakdown</label>
							</div>
						</div>
					</div>
					<div class="row mt">
						<div class="col-lg-12">
							<div class="content-panel">
								<h4><i class="fa fa-angle-right"></i> Floral Order</h4>
								<section id="unseen">
									<table class="table table-bordered table-striped table-condensed">
										<thead>
											<tr>
												<th class="shopping-td-flower">Flower</th>
												<th class="shopping-td-number shopping-td-num numeric" style="width: 15%;">#</th>
												<th class="shopping-td-items-su">Items / PU</th>
												<th class="shopping-td-su-needed">Purchase Units Needed</th>
												<th class="shopping-td-cost">Expected Cost</th>
												<th class="shopping-td-leftovers">Leftovers</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$total_floral = 0;
											foreach ($shopping_list['flower'] as $item) :
												$total_floral += $item['cost'];
												?>
												<tr data-variation-id="<?php echo esc_attr( $item['variation_id'] ) ?>" >
													<td class="shopping-td-flower">
														<?php echo $item['name']; ?>
														<?php foreach ($breakdown[ $item['breakdown_id'] ] as $breakdowned_item) : ?>
															<div class="breakdown shoplist_arrangement hidden">
																<?php echo floatval($breakdowned_item['arrangement_quantity']); ?> x
																<strong><?php echo $breakdowned_item['arrangement_name']; ?></strong> -
																<?php echo $item['name']; ?> x
																<?php echo floatval($breakdowned_item['quantity']); ?>
																(Total <?php echo $breakdowned_item['arrangement_quantity'] * $breakdowned_item['quantity'] ; ?>)
																	<?php foreach ($breakdowned_item['recipes'] as $recipe_item) : ?>
																		<div class="breakdown shoplist_recipe">
																		<strong><?php echo $recipe_item['name']; ?></strong>
																		 -
																		<?php echo $item['name'] ?> x
																		<?php echo floatval($recipe_item['quantity']); ?>
																		</div>
																	<?php endforeach ?>
															</div>
														<?php endforeach ?>
														<span class="btn replace-flower hidden-print"> <i class="fa fa-pencil" aria-hidden="true"></i> </span>
														<span class="select-container"></span>
													</td>
													<td class="shopping-td-number shopping-td-num numeric"><?php echo $item['quantity']; ?></td>
													<td class="shopping-td-items-su"><?php echo $item['items_su'] . ' ' . $item['units']; ?></td>
													<td class="shopping-td-su-needed"><?php echo $item['su_needed'] . ' ' . $item['needed_units']; ?></td>
													<td class="shopping-td-cost"><?php echo sc_format_price($item['cost'], true); ?></td>
													<td class="shopping-td-leftovers"><?php echo sc_format_quantity( $item['leftovers'] ) . ' ' . $item['leftovers_units']; ?></td>
												</tr>
											<?php endforeach; ?>
											<tr>
												<td class="shopping-td-flower"></td>
												<td class="shopping-td-number shopping-td-num numeric"></td>
												<td class="shopping-td-items-su"></td>
												<td class="shopping-td-su-needed"></td>
												<td class="shopping-td-cost">Total: <?php echo sc_format_price($total_floral, true); ?></td>
												<td class="shopping-td-leftovers"></td>
											</tr>
										</tbody>
									</table>
								</section>
							</div><!-- /content-panel -->
						</div><!-- /col-lg-4 -->
					</div><!-- /row -->

					<div class="row mt">
						<div class="col-lg-12">
							<div class="content-panel">
								<h4><i class="fa fa-angle-right"></i> Other items</h4>
								<section id="unseen">
									<table class="table table-bordered table-striped table-condensed">
										<thead>
											<tr>
												<th class="shopping-td-type" style="width: 20%;">Type</th>
												<th class="shopping-td-item">Item</th>
												<th class="shopping-td-number shopping-td-units numeric" style="width: 20%;"># Units</th>
												<th class="shopping-td-cost" style="width: 20%">Expected Cost</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$total_service = 0;
											foreach ($shopping_list['service'] as $item) :
												$total_service += $item['cost'];
												?>
												<tr>
													<td class="shopping-td-type"><?php echo $item['type']; ?></td>
													<td class="shopping-td-item"><?php echo $item['name']; ?>
														<?php foreach ($breakdown[ $item['breakdown_id'] ] as $breakdowned_item) : ?>
															<div class="breakdown shoplist_arrangement hidden">
																<?php echo floatval($breakdowned_item['arrangement_quantity']); ?> x
																<strong><?php echo $breakdowned_item['arrangement_name']; ?></strong> -
																<?php echo $item['name']; ?> x
																<?php echo floatval($breakdowned_item['quantity']); ?>
																(Total <?php echo $breakdowned_item['arrangement_quantity'] * $breakdowned_item['quantity'] ; ?>)
																	<?php foreach ($breakdowned_item['recipes'] as $recipe_item) : ?>
																		<div class="breakdown shoplist_recipe">
																		<strong><?php echo $recipe_item['name']; ?></strong>
																		 -
																		<?php echo $item['name'] ?> x
																		<?php echo floatval($recipe_item['quantity']); ?>
																		</div>
																	<?php endforeach ?>
															</div>
														<?php endforeach ?></td>
													<td class="shopping-td-number shopping-td-units numeric"><?php echo $item['quantity']; ?></td>
													<td class="shopping-td-cost"><?php echo sc_format_price($item['cost'], true); ?></td>
												</tr>
											<?php endforeach; ?>
											<tr>
												<td class="shopping-td-type"></td>
												<td class="shopping-td-flower"></td>
												<td class="shopping-td-number shopping-td-units numeric"></td>
												<td  class="shopping-td-cost">Total: <?php echo sc_format_price($total_service, true); ?></td>
											</tr>
										</tbody>
									</table>
								</section>
							</div><!-- /.content-panel -->
						</div><!-- /.col-lg-4 -->
					</div><!-- /.row -->
				</div><!-- /.shopping-list-container -->
			</div><!-- /.proposal-wrapper -->
		</div><!-- /#event-proposal -->
	</div><!-- /.new-proposal-container -->
	<script type="text/javascript">
	(function($) {
		$(document).ready(function($) {

			function toggle_classes() {
				var selector = ($(this).data('classes-linked'));
				if (this.checked) {
					$(selector).removeClass('hidden');
				} else {
					$(selector).addClass('hidden');
				}
			}

			$('#arr-number').change(toggle_classes);    $('#arr-number').change();
			$('#arr-su-needed').change(toggle_classes); $('#arr-su-needed').change();
			$('#arr-cost').change(toggle_classes);		$('#arr-cost').change();
			$('#arr-leftovers').change(toggle_classes); $('#arr-leftovers').change();
			$('#arr-type').change(toggle_classes);      $('#arr-type').change();
			$('#arr-units').change(toggle_classes);     $('#arr-units').change();
			$('#hide_breakdown').change(toggle_classes);$('#hide_breakdown').change();

			stemcounter.page.userItems = <?php echo sc_get_user_items_json( $event->user_id ); ?>;
			stemcounter.resortUserItems( <?php echo $event->user_id * 1 ?> );

			$(document).trigger('stemcounter.action.shoppingList', {eid: <?php echo $event->id; ?> });
		});

	})(jQuery);
	</script>

	<?php
	return ob_get_clean();
}
add_filter( 'sc/app/content/event_shopping', 'sc_render_event_shopping', 10 );

function sc_render_event_proposal_2_0( $content ) {
	$request = Request::capture();
	$event_id = intval( $request->input( 'eid' ) );

	$event = Event::where( array(
		'id' => $event_id,
	))->with( array(
		'arrangements' => function($query){
			$query->orderBy( 'order', 'asc' );
		},
		'arrangements.items',
		'arrangements.recipes',
		'arrangements.items.item_variation',
		'arrangements.items.item_variation.item',
	) )->first();

	$user_id = $event->user_id;

	try {
		$version = Event_Version::where(array(
			'event_id' => $event_id,
		))->orderBy('created_at', 'desc')->first();
	} catch ( Exception $r ) {
		$r->fail( 'This event version doesn\'t exist.' );
	}

	$is_version = 0;
	$user_recipes = false;

	if ( empty( $GLOBALS['wp']->query_vars['event-uid'] ) ) {
		Arrangement::where( array(
			'event_id' => 0,
			'user_id'  => $user_id,
		) )->get()->each(function( $arrangement ) use ( &$user_recipes ) {
			$user_recipes[] = sc_format_arrangement_for_user_recipes( $arrangement );
		});
	}

	$payments = array();
	$live_payments = false;

	if ( ! empty( $GLOBALS['wp']->query_vars['event-uid'] ) ) {
		// Loading the public view
		$event_data = json_decode( $version->event_data );

		$is_version = 1;
		$_event =  (array) $event_data->event;
		$eventDate = $event_data->eventDate;
		$js_date_format = $event_data->dateFormat;
		if ($js_date_format == 'dd/mm/yy') {
			$date_format = 'd/m/Y';
		} else if ($js_date_format == 'mm/dd/yy') {
			$date_format = 'm/d/Y';
		} else if ($js_date_format == 'dd.mm.yy') {
			$date_format = 'd.m.Y';
		} else if ($js_date_format == 'mm.dd.yy') {
			$date_format = 'm.d.Y';
		}
		$customers = isset( $event_data->customers ) ? $event_data->customers : (array) $event_data->customer;
		// Convert single customer to multiple customers
		if ( isset( $customers['id'] ) || isset( $customers['name'] ) ) {
			$customers = array( $customers );
		}
		$company_info = (array) $event_data->company_info;
		$event_cost = $event_data->eventCost;
		if ( empty( $event_cost->tax_rates ) ) {
			$tax_rates = Tax_Rate::where( array(
				'user_id' => $user_id
			) )->get()->toArray();

			$event_cost->tax_rates = $tax_rates;
		}
		$eventNote = $event_data->eventNote;
		$eventContractNote = $event_data->eventContractNote;
		$venues_list = array();
		$versions = array();
		$invoice_categories_list = array();
		$item_types = array();
		$values = array();
		$selected_category_id = null;
		$signatures = ! empty( $event_data->signatures ) ? sc_get_event_signatures( $user_id, $event_id, $version->id, sc_object_to_array( $event_data->signatures ) ) : sc_get_event_signatures( $user_id, $event_id, $version->id );

		remove_action( 'wp_head', 'sc_render_mixpanel_js', 9999 );
		remove_action( 'wp_head', 'sc_render_drift_js', 9999 );
		remove_action( 'wp_head', 'sc_render_facebook_pixel_js', 999 );

		$_payments = $event->live_payments->all();

		foreach ( $_payments as $payment ) {
			$payments[] = array(
				'paymentId'			=> $payment->id,
				'paymentName'		=> $payment->payment_name,
				'dueDate'			=> sc_get_user_date_time( $user_id, $payment->due_date, false, $date_format ),
				'dueDateRaw'		=> $payment->due_date,
				'cardFee'			=> sc_format_price( $payment->card_fee ),
				'convenienceFee'	=> sc_format_price( $payment->convenience_fee ),
				'amount'			=> ! is_null( $payment->amount ) ? sc_format_price( $payment->amount ) : null,
				'payment_date'		=> is_null( $payment->payment_date ) ? '' : sc_get_user_date_time( $user_id, $payment->payment_date, false, $date_format ),
				'amountPercentage'	=> is_null( $payment->payment_percentage ) ? null : sc_format_price( $payment->payment_percentage ),
				'amountType'		=> ! is_null( $payment->payment_percentage ) && 'complete' != $payment->status ? 'percentage' : 'currency',
				'paymentType'		=> $payment->payment_type,
				'note'				=> $payment->payment_note,
				'status'			=> $payment->status,
			);
		}
	} elseif ( ! empty( $version ) ) {
		// Loading the latest version by default
		$event_data = json_decode( $version->event_data );

		$is_version = 1;
		$_event =  (array) $event_data->event;
		$eventDate = $event_data->eventDate;
		$js_date_format = $event_data->dateFormat;
		if ($js_date_format == 'dd/mm/yy') {
			$date_format = 'd/m/Y';
		} else if ($js_date_format == 'mm/dd/yy') {
			$date_format = 'm/d/Y';
		} else if ($js_date_format == 'dd.mm.yy') {
			$date_format = 'd.m.Y';
		} else if ($js_date_format == 'mm.dd.yy') {
			$date_format = 'm.d.Y';
		}
		$customers = isset( $event_data->customers ) ? $event_data->customers : (array) $event_data->customer;
		// Convert single customer to multiple customers
		if ( isset( $customers['id'] ) || isset( $customers['name'] ) ) {
			$customers = array( $customers );
		}

		$company_info = (array) $event_data->company_info;
		$event_cost = $event_data->eventCost;

		if ( empty( $event_cost->tax_rates ) ) {
			$tax_rates = Tax_Rate::where( array(
				'user_id' => $user_id
			) )->get()->toArray();

			$event_cost->tax_rates = $tax_rates;
		}
		$eventNote = $event_data->eventNote;
		$eventContractNote = $event_data->eventContractNote;

		$event_vendors = array();
		$event_vendors_info = sc_get_user_event_vendors( $event_data->event->id, $user_id );
		$event_vendors['venue'] = $event_vendors_info['event_vendors']['venue'];
		$venues_list = $event_vendors_info['venues_list'];

		$versions = array();
		$_versions = Event_Version::where(array(
			'event_id' => $event_id,
		))->orderBy('id', 'desc')->get();

		foreach ( $_versions as $_version ) {
			array_push($versions, array(
				'version_id' => $_version->id,
				'version_no' => $_version->version,
				'created_at' => date( $date_format, strtotime( $_version->created_at ) ),
				'signed'     => (bool) $_version->client_signed,
			));
		}

		$invoice_categories_list = Invoicing_Category::where(array(
			'user_id' => $user_id
		))->get()->toArray();
		$item_types = sc_get_mixed_arrangement_item_types();

		$values = array(
			'hardgood_multiple' => $event->hardgood_multiple,
			'fresh_flower_multiple' => $event->fresh_flower_multiple,
			'labor' => array(
				'global_labor_value' => $event->global_labor_value,
				'flower' => $event->flower_labor,
				'hardgood' => $event->hardgood_labor,
				'base_price' => $event->base_price_labor,
				'fee' => $event->fee_labor,
				'rental' => $event->rental_labor
			)
		);

		$name = 'test';
		try {
			$event_invoicing_category = Invoicing_Category::where(array(
				'id' => $event->invoicing_category_id,
				'user_id' => $user_id
			))->firstOrFail();
			$selected_category_id = $event_invoicing_category->id;

			foreach ($invoice_categories_list as $i => $value) {
				if ($value['id'] == $selected_category_id) {
					$invoice_categories_list[$i]["hardgood_multiple"] = $event->hardgood_multiple;
				    $invoice_categories_list[$i]["fresh_flower_multiple"] = $event->fresh_flower_multiple;
				    $invoice_categories_list[$i]["global_labor_value"] = $event->global_labor_value;
				    $invoice_categories_list[$i]["flower_labor"] = $event->flower_labor;
				    $invoice_categories_list[$i]["hardgood_labor"] = $event->hardgood_labor;
				    $invoice_categories_list[$i]["base_price_labor"] = $event->base_price_labor;
				    $invoice_categories_list[$i]["fee_labor"] = $event->fee_labor;
				    $invoice_categories_list[$i]["rental_labor"] = $event->rental_labor;
					break;
				}
			}
		} catch (Exception $e) {
			$name = 'Deleted Markup Profile';
			array_unshift($invoice_categories_list, array(
				"id" => $event->invoicing_category_id,
			    "user_id" => $user_id,
			    "name" => $name,
			    "hardgood_multiple" => $event->hardgood_multiple,
			    "fresh_flower_multiple" => $event->fresh_flower_multiple,
			    "global_labor_value" => $event->global_labor_value,
			    "flower_labor" => $event->flower_labor,
			    "hardgood_labor" => $event->hardgood_labor,
			    "base_price_labor" => $event->base_price_labor,
			    "fee_labor" => $event->fee_labor,
			    "rental_labor" => $event->rental_labor
			));
			$selected_category_id = $event->invoicing_category_id;
		}

		$signatures = ! empty( $event_data->signatures ) ? sc_get_event_signatures( $event->user_id, $event_id, $version->id, sc_object_to_array( $event_data->signatures ) ) : sc_get_event_signatures( $user_id, $event_data->event->id );

		$_payments = $event->live_payments->all();

		foreach ( $_payments as $payment ) {
			$payments[] = array(
				'paymentId'			=> $payment->id,
				'paymentName'		=> $payment->payment_name,
				'dueDate'			=> sc_get_user_date_time( $user_id, $payment->due_date, false, $date_format ),
				'dueDateRaw'		=> $payment->due_date,
				'cardFee'			=> sc_format_price( $payment->card_fee ),
				'convenienceFee'	=> sc_format_price( $payment->convenience_fee ),
				'amount'			=> ! is_null( $payment->amount ) ? sc_format_price( $payment->amount ) : null,
				'payment_date'		=> is_null( $payment->payment_date ) ? '' : sc_get_user_date_time( $user_id, $payment->payment_date, false, $date_format ),
				'amountPercentage'	=> is_null( $payment->payment_percentage ) ? null : sc_format_price( $payment->payment_percentage ),
				'amountType'		=> ! is_null( $payment->payment_percentage ) && 'complete' != $payment->status ? 'percentage' : 'currency',
				'paymentType'		=> $payment->payment_type,
				'note'				=> $payment->payment_note,
				'status'			=> $payment->status,
			);
		}
	} else {
		// When there are no event versions yet - load Workroom View
		$company_info = get_company_info();

		$is_version = 0;
		$tax_rates = Tax_Rate::where( array(
			'user_id' => $user_id
		) )->get()->toArray();

		$event->arrangements->sortBy( function($arrangement){
			return $arrangement->order;
		} );

		$current_user = get_userdata( $user_id );

		$logo = '<h3>' . $company_info['company'] . '</h3>';
		if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
			$logo_id = sc_get_profile_logo_id( $user_id );
			$logo = wp_get_attachment_image( $logo_id, 'logo_large', false, array( 'class' => 'proposal-logo' ) );
		}

		$invoice_categories_list = Invoicing_Category::where(array(
			'user_id' => $user_id
		))->get()->toArray();
		$item_types = sc_get_mixed_arrangement_item_types();

		$values = array(
			'hardgood_multiple' => $event->hardgood_multiple,
			'fresh_flower_multiple' => $event->fresh_flower_multiple,
			'labor' => array(
				'global_labor_value' => $event->global_labor_value,
				'flower' => $event->flower_labor,
				'hardgood' => $event->hardgood_labor,
				'base_price' => $event->base_price_labor,
				'fee' => $event->fee_labor,
				'rental' => $event->rental_labor
			)
		);

		$name = 'test';
		try {
			$event_invoicing_category = Invoicing_Category::where(array(
				'id' => $event->invoicing_category_id,
				'user_id' => $user_id
			))->firstOrFail();
			$selected_category_id = $event_invoicing_category->id;

			foreach ($invoice_categories_list as $i => $value) {
				if ($value['id'] == $selected_category_id) {
					$invoice_categories_list[$i]["hardgood_multiple"] = $event->hardgood_multiple;
				    $invoice_categories_list[$i]["fresh_flower_multiple"] = $event->fresh_flower_multiple;
				    $invoice_categories_list[$i]["global_labor_value"] = $event->global_labor_value;
				    $invoice_categories_list[$i]["flower_labor"] = $event->flower_labor;
				    $invoice_categories_list[$i]["hardgood_labor"] = $event->hardgood_labor;
				    $invoice_categories_list[$i]["base_price_labor"] = $event->base_price_labor;
				    $invoice_categories_list[$i]["fee_labor"] = $event->fee_labor;
				    $invoice_categories_list[$i]["rental_labor"] = $event->rental_labor;
					break;
				}
			}
		} catch (Exception $e) {
			$name = 'Deleted Markup Profile';
			array_unshift($invoice_categories_list, array(
				"id" => $event->invoicing_category_id,
			    "user_id" => $user_id,
			    "name" => $name,
			    "hardgood_multiple" => $event->hardgood_multiple,
			    "fresh_flower_multiple" => $event->fresh_flower_multiple,
			    "global_labor_value" => $event->global_labor_value,
			    "flower_labor" => $event->flower_labor,
			    "hardgood_labor" => $event->hardgood_labor,
			    "base_price_labor" => $event->base_price_labor,
			    "fee_labor" => $event->fee_labor,
			    "rental_labor" => $event->rental_labor
			));
			$selected_category_id = $event->invoicing_category_id;
		}

		$company_info = get_company_info( $user_id );
		if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
			$logo_id = sc_get_profile_logo_id( $user_id );
			$logo = wp_get_attachment_image_src( $logo_id, 'medium' );
			$company_info['logo_url'] = $logo[0];
		} else {
			$company_info['logo_url'] = false;
		}

		$additionalInfo = unserialize( $current_user->additionalInfo ); //Get User Details
		$company_info['phone'] = isset( $additionalInfo['companyMobile'] ) ? $additionalInfo['companyMobile'] : '';
		$company_info['email'] = isset( $current_user->user_email ) ? $current_user->user_email : '';

		// include event details from above function if needeed.
		$proposal_arrangements = array();
		$fixed_prices_addon = 0;
		$fixed_prices_live = 0;
		$arrangement_subtotals = [];

		foreach ($event->arrangements as $arrangement) {
			$arrangement_photos = Arrangement_Photo::where('arrangement_id', '=', $arrangement->id)->get();
			$photos = array();
			foreach ($arrangement_photos as $photo) {
				/* if the image exists */
				$image = wp_get_attachment_image_src($photo->photo_id);
				if (!empty($image)) {
					$image_id = $photo->photo_id;
					$image_url = wp_get_attachment_image_src($image_id, 'medium');
					$image_url = $image_url[0];
					$full_image = wp_get_attachment_image_src($image_id, 'full');

					$photos[] = array(
						'image_id' => $image_id,
						'image_url' => $image_url,
						'full_image_src' => $full_image[0],
						'full_image_w' => $full_image[1],
						'full_image_h' => $full_image[2],
					);
				} else {
					/* delete it from the arrangement_photos if it doesn't */
					Arrangement_Photo::where(array(
						'photo_id' => $photo->photo_id
					))->delete();
				}
			}
			$recipes = array();
			$items = array();

			foreach ( $arrangement->recipes as $arrangement_recipe ) {
				$recipes[] = array(
					'id' => $arrangement_recipe->id,
					'name' => $arrangement_recipe->name,
				);
			}


			foreach ( $arrangement->items as $arrangement_item ) {
				$cost_and_markup = $arrangement_item->cost + $arrangement_item->get_markup( false );

				$vi = $arrangement_item->toArray();

				$vi['item_type'] = $arrangement_item->item->type;
				$vi['item_variation_name'] = '';

				try {
					//used for deleted items
					$variation = Item_Variation::where(
						array( 'id' => $vi['item_variation_id'] )
					)->firstOrFail()->toArray();

					$vi['item_variation_name'] = ' - ' . $variation['name'];
				} catch (Exception $e) {
				}

				$item_attachment = sc_get_variation_attachment( $arrangement_item->item_variation, $arrangement_item->item->default_variation );

				$key = $arrangement_item->item_id . '_' . $vi['item_variation_id'];

				$type = sc_get_item_type( $arrangement_item->item, true );

				$items[] = array(
					'id'                        => $arrangement_item->id,
					'recipe_id'					=> $arrangement_item->arrangement_recipe_id,
					'name'                      => $arrangement_item->get_full_name(),
					'quantity'                  => $arrangement_item->quantity,
					'invoicing_multiplier'      => $arrangement_item->get_invoicing_multiplier(),
					'cost'                      => sc_format_price( $arrangement_item->cost ),
					'cost_and_markup'           => $cost_and_markup,
					'cost_and_markup_and_labor' => $cost_and_markup + $arrangement_item->get_labor( false ),
					'subtotal'                  => $arrangement_item->get_calculated_price(),
					'type'                      => $type,
					'type_raw'                  => str_replace( ' ', '_', strtolower( $type ) ),
					'item_type'                 => $vi['item_type'],
					'item_id'                   => $vi['item_id'],
					'item_variation_id'         => $vi['item_variation_id'],
					'item_variation_name'       => ! empty( $vi['item_variation_name'] ) ? $vi['item_variation_name'] : '',
					'item_pricing_category'     => $arrangement_item->item->pricing_category,
					'itemKeypair'               => $key,
					'attachment'				=> $item_attachment,
				);
			}
			usort( $items, 'sc_recipe_list_sort' );

			array_push($proposal_arrangements, array(
				'id'                    => $arrangement->id,
				'name'                  => $arrangement->name,
				'quantity'              => $arrangement->quantity,
				'order'                 => $arrangement->order,
				'note'                  => $arrangement->note,
				'include'               => $arrangement->include,
				'tax'                   => $arrangement->tax,
				'addon'					=> $arrangement->addon,
				'total'                 => $arrangement->get_final_price(false),
				'override_cost'         => $arrangement->override_cost,
				'subTotal'              => $arrangement->get_final_price(),
				'photos'                => $photos,
				'recipes'				=> $recipes,
				'items'                 => $items,
				'invoicing_category_id' => $arrangement->invoicing_category_id,
				'hardgood_multiple'     => $arrangement->hardgood_multiple,
				'fresh_flower_multiple' => $arrangement->fresh_flower_multiple,
				'global_labor_value'    => $arrangement->global_labor_value,
				'flower_labor'          => $arrangement->flower_labor,
				'hardgood_labor'        => $arrangement->hardgood_labor,
				'base_price_labor'      => $arrangement->base_price_labor,
				'fee_labor'             => $arrangement->fee_labor,
				'rental_labor'          => $arrangement->rental_labor,
				'hide_price'			=> $arrangement->hide_price,
				'is_percentage'			=> absint( $arrangement->is_percentage ),
				'is_applied_all'		=> absint( $arrangement->is_applied_all ),
				'applies_on'			=> $arrangement->applies_on,
			));

		}

 		$date_format = get_user_meta( $event->user_id, '__date_format', true );
		$js_date_format = sc_get_user_js_date_format( $event->user_id );

		if ( ! is_null( $event ) ) {

			$tax_rate_ids = array();
			foreach ( $tax_rates as $tax_rate ) {
				/** The value of the tax rate was modified */
				if ( $tax_rate['id'] == $event->tax_rate_id &&
					$tax_rate['value'] != $event->tax_rate_value ) {
					array_push( $tax_rates, array(
						'id' => -$tax_rate['id'],
						'user_id' => $user_id,
						'name' => 'Modified Tax Rate',
						'value' => $event->tax_rate_value
					));
				}

				/** The value of the tax rate was modified */
				if ( $tax_rate['id'] == $event->tax_rate_id2 &&
					$tax_rate['value'] != $event->tax_rate_value2 ) {
					array_push( $tax_rates, array(
						'id' => -$tax_rate['id'],
						'user_id' => $user_id,
						'name' => 'Modified Tax Rate 2',
						'value' => $event->tax_rate_value2
					));
				}

				array_push( $tax_rate_ids, $tax_rate['id'] );
			}

			/**
			 * If the Event Tax Rate is not found in the User Tax Rates
			 * It means that it was deleted
			 */
			if ( (
					! in_array( $event->tax_rate_id, $tax_rate_ids ) &&
					! empty( $tax_rate_ids ) &&
					! is_null( $event->tax_rate_id ) &&
					$event->tax_rate_id != 0
				) || (
					empty( $tax_rate_ids ) && $event->tax_rate_id != 0
				) ) {
				array_push( $tax_rates, array(
					'id' => -$event->tax_rate_id,
					'user_id' => $user_id,
					'name' => 'Deleted Tax Rate',
					'value' => $event->tax_rate_value
				) );
			}

			if ( (
					! in_array( $event->tax_rate_id2, $tax_rate_ids ) &&
					! empty( $tax_rate_ids ) &&
					! is_null( $event->tax_rate_id2 ) &&
					$event->tax_rate_id2 != 0
				) || (
					empty( $tax_rate_ids ) && $event->tax_rate_id2 != 0
				) ) {
				array_push( $tax_rates, array(
					'id' => -$event->tax_rate_id,
					'user_id' => $user_id,
					'name' => 'Deleted Tax Rate 2',
					'value' => $event->tax_rate_value2
				) );
			}
		}

		$event_cost = array(
			'subtotal'		=> $event->get_subtotal( true ),
			'tax' 			=> $event->get_tax( true ),
			'tax2' 			=> $event->get_tax2( true ),
			'tax_rate_id' 	=> $event->tax_rate_id,
			'tax_rate_value'=> $event->tax_rate_value,
			'tax_rate_id2' 	=> $event->tax_rate_id2,
			'tax_rate_value2'=> $event->tax_rate_value2,
			'total' 		=> $event->get_total( true ),
			'tax_rates' 	=> $tax_rates,
		);

		$event_vendors = array();
		$event_vendors_info = sc_get_user_event_vendors( $event->id, $user_id );
		$event_vendors['venue'] = $event_vendors_info['event_vendors']['venue'];
		$venues_list = $event_vendors_info['venues_list'];

		$customers = sc_get_event_customers( $event->id, $event->user_id );

		if ( $event->cover_id ) {
			$event_cover_src = wp_get_attachment_image_src( $event->cover_id, 'full' );
			$event_cover = array(
				'imageId' => $event->cover_id,
				'imageURL' => $event_cover_src[0],
				'fullImageW' => $event_cover_src[1],
				'fullImageH' => $event_cover_src[2],
			);
		} else {
			$event_cover = array(
				'imageId' => '',
				'imageURL' => '',
				'fullImageW' => '',
				'fullImageH' => '',
			);
		}
		$_event = array(
			'id'          		=> $event->id,
			'name'        		=> $event->name,
			'card'       		=> $event->card,
			'hide_item_prices'	=> $event->hide_item_prices,
			'separate_cc_fee' 	=> $event->separate_cc_fee,
			'visible_payments'	=> $event->visible_payments,
			'hidden_contract'	=> $event->hidden_contract,
			'date'       		=> date( $date_format, strtotime( $event->date ) ),
			'date_created'		=> date( $date_format, strtotime( $event->created_at ) ),
			'venues'      		=> $event_vendors['venue'],
			'arrangements'		=> $proposal_arrangements,
			'cover'		  		=> $event_cover,
		);

		$versions = array();
		$_versions = Event_Version::where(array(
			'event_id' => $event_id,
		))->orderBy('id', 'desc')->get();

		foreach ( $_versions as $version ) {
			array_push($versions, array(
				'version_id' => $version->id,
				'version_no' => $version->version,
				'created_at' => date( $date_format, strtotime( $version->created_at ) ),
				'signed'     => (bool) $version->client_signed,
			));
		}

		$eventDate = date( $date_format, strtotime( $event->date ) );
		$eventNote = wpautop( $event->event_note );
		$eventContractNote =  wpautop( $event->contract_note );

		$signatures = sc_get_event_signatures( $user_id, $event->id );

		$attachment_ids = Meta::where( array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'attachments',
		))->pluck( 'meta_value' );

		$_event['attachments'] = array();
		foreach ( $attachment_ids as $id ) { //If you edit this, change it on the other places (versionings) too
			$_event['attachments'][] = array (
				'id' => $id,
				'title' => get_the_title( $id ),
				'url' => wp_get_attachment_url( $id ),
				'icon' => wp_get_attachment_image_url( $id, 'thumbnail', true ),
			);
		}

		$_payments = $event->payments->all();

		foreach ( $_payments as $payment ) {
			$payments[] = array(
				'paymentId'			=> $payment->id,
				'paymentName'		=> $payment->payment_name,
				'dueDate'			=> sc_get_user_date_time( $user_id, $payment->due_date, false, $date_format ),
				'dueDateRaw'		=> $payment->due_date,
				'cardFee'			=> sc_format_price( $payment->card_fee ),
				'convenienceFee'	=> ( 'complete' == $payment->status ) ? $payment->convenience_fee : $payment->get_amount() * ( $payment->card_fee / 100 ),
				'amount'			=> ! is_null( $payment->amount ) ? sc_format_price( $payment->amount ) : null,
				'payment_date'		=> is_null( $payment->payment_date ) ? '' : sc_get_user_date_time( $user_id, $payment->payment_date, false, $date_format ),
				'amountPercentage'	=> is_null( $payment->payment_percentage ) ? null : sc_format_price( $payment->payment_percentage ),
				'amountType'		=> ! is_null( $payment->payment_percentage ) && 'complete' != $payment->status ? 'percentage' : 'currency',
				'paymentType'		=> $payment->payment_type,
				'note'				=> $payment->payment_note,
				'status'			=> $payment->status,
			);
		}
	}

	$proposal_covers = array();
	$post_attach_args = array(
		'post_parent'		=> get_the_ID(),
		'post_type'			=> 'attachment',
		'post_mime_type'	=> 'image',
		'orderby'			=> 'menu_order',
		'order'				=> 'ASC',
	);

	$post_covers = get_posts( $post_attach_args );
	if ( $post_covers ) {
		foreach ( $post_covers as $cover ) {
			$fullcover = wp_get_attachment_image_src( $cover->ID, 'full' );
			$proposal_covers[] = array(
				'imageId' => $cover->ID,
				'imageURL' => $fullcover[0],
				'fullImageW' => $fullcover[1],
				'fullImageH' => $fullcover[2],
				'imageTitle' => get_the_title( $cover->ID ),
			);
		}
	}
	$_event['default_covers'] = $proposal_covers;

	$user_email_templates = array();
	if ( SCAC::can_access( SCAC::STUDIO ) ) {
		$user_email_templates = Email_Template::where( array(
			'user_id'	=> $user_id,
		) )->get();
	} else {
		$user_email_templates[] = Email_Template::where( array(
			'user_id'	=> $user_id,
		) )->first();
	}
	$_event['email_templates'] = $user_email_templates;

	$react_data = array(
		'event'              => $_event,
		'versions'           => $versions,
		'eventDate'          => $eventDate,
		'publicView'         => ! empty( $GLOBALS['wp']->query_vars['event-uid'] ) ? 1 : 0,
		'version'            => $is_version,
		'versionNo'          => ! empty( $GLOBALS['wp']->query_vars['event-uid'] ) ? $version->version : '0',
		'company_info'       => $company_info,
		'customers'          => $customers,
		'privateNotes'       => wpautop( $event->private_note ),
		'eventCost'          => $event_cost,
		'venuesList'         => $venues_list,
		'categoriesList'     => $invoice_categories_list,
		'itemTypes'          => $item_types,
		'eventNote'          => $eventNote,
		'eventContractNote'  => $eventContractNote,
		'currencySymbol'     => sc_get_user_saved_pref_currency( $user_id ),
		'eventInvoiceValues' => $values,
		'categoryId'         => $selected_category_id,
		'dateFormat'         => $js_date_format,
		'eventsURL'          => sc_get_permalink_by_template( 'template-events.php' ),
		'publicLink'         => home_url( '/view-proposal/' . sc_encrypted_string( $event_id ) ),
		'shoppingListUrl'    => add_query_arg( 'eid', $event->id, sc_get_permalink_by_template( 'template-shopping.php' ) ),
		'arrangementListUrl' => add_query_arg( array( 'eid' => $event->id ), home_url( '/recipe-sheet/' ) ),
		'eventBreakdownUrl'  => add_query_arg( array( 'eid' => $event->id ), home_url( '/event-breakdown/' ) ),
		'eventVersionsLink'  => add_query_arg( array( 'eid' => $event->id ), home_url( '/proposal-2-0-versions/' ) ),
		'signatureOptions'   => $signatures,
		'signatures'         => $signatures,
		'isLoggedIn'         => is_user_logged_in() && ( is_super_admin() || $user_id == get_current_user_id() ) ? 'true' : 'false',
		'payments'           => $payments,
		'livePayments'       => $live_payments,
		'userRecipes'        => $user_recipes,
		'extensions'         => array(),
		'showBothRates'      => 2 == sc_get_visible_taxes_count( $user_id ),
		'isAdmin'			 => is_super_admin() ? 1 : 0,
		'spreadSheetURL'	 => add_query_arg( array( 'event_id' => $event->id, 'action' => 'sc_proposal_spreadsheet', ), admin_url( 'admin-ajax.php', is_ssl() ? 'https' : 'http' ) ),
	);

	// Filter the data we send to react so that we can move add-on logic out into separate files
	$react_data = apply_filters( 'sc/react/data/proposal', $react_data, $event, $user_id, $version );
	/*$user_libraries = get_user_meta( $user_id, 'sc_user_libraries', true );
	$user_libraries = is_array( $user_libraries ) ? $user_libraries : array();*/
	ob_start(); ?>
	<div class="proposal-container new-proposal-container container" id="forPrint">
		<div id="event-proposal"></div>
	</div>
	<style>
		<?php
			$print_compay_name = ! empty( $company_info['company'] ) ? $company_info['company'] . ' | '  : '';
			$print_bottom_address = ! empty( $company_info['address'] ) ? $company_info['address'] . ' | ' : '';
			$print_company_phone = ! empty( $company_info['phone'] ) ? $company_info['phone'] . ' | ' : '';
			$print_company_mail = ! empty( $company_info['email'] ) ? $company_info['email'] : '';
			$print_event_name = ! empty( $_event['name'] ) ? $_event['name'] . ' | ' : '';
			$print_event_date = ! empty( $_event['date'] ) ? $_event['date'] . ' | '  : '';
			$print_paper_size = ! empty( get_user_meta( $user_id, '_sc_pdf_settings', true ) ) ? get_user_meta( $user_id, '_sc_pdf_settings', true) : 'US-Letter';

		?>
		@page :first {
			@top { content: '' }
		}
		@page {
			font-family: 'PTSans-Caption', 'Ruda', sans-serif;
			size: <?php echo $print_paper_size; ?> portrait;
			margin: 0.5in;

			@top {
				font-size: 14px;
				content: "<?php echo $print_event_name . $print_event_date; ?>Page " counter(page)
			}

			@bottom {
				font-size: 14px;
				content: "<?php echo $print_compay_name . $print_bottom_address  . $print_company_phone . $print_company_mail;  ?>"
			}
		}

		<?php
			render_itemslist_style( $user_id );
			do_action( 'sc/event/render_styles', $event );
		?>
	</style>
	<script type="text/javascript">
	(function($) {
		<?php if ( empty( $GLOBALS['wp']->query_vars['event-uid'] ) ) : // only if not public link ?>
			stemcounter.page.userItems = <?php echo sc_get_user_items_json( $user_id, true ) ?>;
		<?php endif; ?>
		<?php	$user_measuring_units = get_user_meta( get_current_user_id(), 'sc_user_measuring_units', true );
				$measuring_units = Measuring_Unit::whereIn( 'id', $user_measuring_units )->get(); ?>
		stemcounter.page.measuring_units = <?php echo json_encode( $measuring_units ); ?>;		
		$(document).ready(function($) {
			var data = <?php echo json_encode( $react_data ); ?>;
			data.node = $('#event-proposal');
			$(document).trigger('stemcounter.action.renderProposal', data);

		});

		function editItem(itemId, itemType, variation_id) {
			var url = window.stemcounter.aurl({
				action: 'sc_edit_item_form',
				id: itemId,
				type: itemType,
				variation_id: variation_id
			});

			stemcounter.openAjaxModal('Edit Item', url, 'new-modal edit-item-modal');
		}

		$(document).on('stemcounter.action.renderEditItemForm', function(e,  r, itemId, itemType, variation_id){
			editItem(itemId, itemType, variation_id);
		});

		$(document).on('stemcounter.action.renderAddNewItemForm', function(e, caller, lib_user_id, lib_item_id, lib_variation_id){
			lib_user_id = lib_user_id ? lib_user_id : null;
			lib_item_id = lib_item_id ? lib_item_id : null;
			lib_variation_id = lib_variation_id ? lib_variation_id : null;
			createNewItem(caller, lib_user_id, lib_item_id, lib_variation_id);
		});

		stemcounter.page.createNewItemCaller = null;

		function createNewItem(caller, lib_user_id, lib_item_id, lib_variation_id) {
			stemcounter.page.createNewItemCaller = caller;
			var url = window.stemcounter.aurl({
				action: 'sc_edit_item_form',
				lib_user_id: lib_user_id,
				lib_item_id: lib_item_id,
				variation_id: lib_variation_id,
				added_form_lib: lib_user_id ? 1 : 0
			});

			stemcounter.openAjaxModal('Add New Item', url, 'new-modal edit-item-modal');
		}

	})(jQuery);
	</script>
	<?php

	return ob_get_clean();
}
add_filter( 'sc/app/content/event_proposal_2_0', 'sc_render_event_proposal_2_0', 10 );

function sc_edit_event_proposal() {
	ignore_user_abort( true );

	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$event = sc_user_can_edit_event( $request->input( 'event_id' ) );
	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}

	$arrangements = $request->input( 'arrangements' );
	$payments = $request->input( 'payments' );
	$contract_note = $request->input( 'contract_note' );
	$private_notes = $request->input( 'private_note' );
	$event_notes = $request->input( 'event_note' );
	$invoicing_settings = $request->input( 'invoicing_settings' );
	$invoicing_category_id = $request->input( 'invoicing_category_id' );

	$venue_types = array();

	$user_id = $event->user_id;
	$date_format = get_user_meta( $user_id, '__date_format', true );

	if ( ! empty( $arrangements ) && ! is_null( $arrangements ) ) {
		$_arr = array();

		foreach ( $arrangements as $arrangement ) {
			$inc_recipes = array();
			$inc_items = array();
			$inc_photos = array();

			try {
				if ( ! empty( $arrangement['duplicated_from'] ) ) {
					$updated_arrangement = Arrangement::where( array(
						'id' 		=> intval( $arrangement['duplicated_from'] ),
						'user_id'	=> $event->user_id,
					) )->with( array(
						'recipes',
						'photos',
						'items',
						'items.item',
						'items.item_variation',
						'items.item_variation.item',
					) )->first();

					$updated_arrangement = sc_duplicate_arrangement( $updated_arrangement, $event, $inc_recipes, $inc_items, $inc_photos );
					$updated_arrangement->event_id = $event->id;
				} else if ( -1 == $arrangement['id'] ) {
					$updated_arrangement = new Arrangement;
					$updated_arrangement->event_id = $event->id;
					$updated_arrangement->user_id = $event->user_id;
				} else {
					$updated_arrangement = Arrangement::where('id', '=', $arrangement['id'])->first();

					$photos_to_delete = array();
					foreach ($updated_arrangement->photos as $photo) {
						array_push($photos_to_delete, $photo);
					}

					$arr_photos = array();
					foreach ( $arrangement['photos'] as $i => $photo_id ) {
						$image = wp_get_attachment_image_src( $photo_id['image_id'] );

						if ( ! is_null( $photo_id['image_id'] ) && ! empty( $image ) ) {
							$arrangement_photo = new Arrangement_Photo();
							$arrangement_photo->arrangement_id = $updated_arrangement->id;
							$arrangement_photo->photo_id = (int) $photo_id['image_id'];
							$arrangement_photo->modal_position = $i;
							$arr_photos[] = $arrangement_photo;
						}
					}
					$updated_arrangement->photos()->saveMany( $arr_photos );

					foreach ( $photos_to_delete as $photo ) {
						$photo->delete();
					}
				}

				if ( empty( $arrangement['from_recipe'] ) ) {
					$updated_arrangement->name = $arrangement['name'];
					$updated_arrangement->quantity = $arrangement['quantity'];
					$updated_arrangement->note = $arrangement['note'];
					$updated_arrangement->order = $arrangement['order'];
					$updated_arrangement->include = $arrangement['include'];
					$updated_arrangement->tax = $arrangement['tax'];
					$updated_arrangement->addon = $arrangement['addon'];
					$updated_arrangement->is_percentage = ! empty( $arrangement['is_percentage'] ) ? 1 : 0;
					$updated_arrangement->is_applied_all = ! empty( $arrangement['is_applied_all'] ) ? 1 : 0;
					$updated_arrangement->applies_on = empty ( $arrangement['applies_on'] ) ? null: $arrangement['applies_on'];

					if ( empty( $arrangement['invoicing_category_id'] ) || $arrangement['invoicing_category_id'] <= 0 ) {
						$updated_arrangement->invoicing_category_id = NULL;
						$updated_arrangement->fresh_flower_multiple = NULL;
						$updated_arrangement->hardgood_multiple = NULL;
						$updated_arrangement->global_labor_value = NULL;
						$updated_arrangement->flower_labor = NULL;
						$updated_arrangement->hardgood_labor = NULL;
						$updated_arrangement->base_price_labor = NULL;
						$updated_arrangement->fee_labor = NULL;
					} else {
						//otherwise save them
						$updated_arrangement->invoicing_category_id = abs( $arrangement['invoicing_category_id'] );
						$updated_arrangement->fresh_flower_multiple = is_null( $arrangement['fresh_flower_multiple'] ) ? 1 : $arrangement['fresh_flower_multiple'];
						$updated_arrangement->hardgood_multiple = is_null( $arrangement['hardgood_multiple'] ) ? 1 : $arrangement['hardgood_multiple'];
						$updated_arrangement->global_labor_value = is_null( $arrangement['global_labor_value'] ) ? 0 : $arrangement['global_labor_value'];
						$updated_arrangement->flower_labor = is_null( $arrangement['flower_labor'] ) ? 0 : $arrangement['flower_labor'];
						$updated_arrangement->hardgood_labor = is_null( $arrangement['hardgood_labor'] ) ? 0 : $arrangement['hardgood_labor'];
						$updated_arrangement->base_price_labor = is_null( $arrangement['base_price_labor'] ) ? 0 : $arrangement['base_price_labor'];
						$updated_arrangement->fee_labor = is_null( $arrangement['fee_labor'] ) ? 0 : $arrangement['fee_labor'];
						$updated_arrangement->rental_labor = is_null( $arrangement['rental_labor'] ) ? 0 : $arrangement['rental_labor'];
					}

					if ( isset( $arrangement['override_cost'] ) && ! is_null( $arrangement['override_cost'] ) && '' !== $arrangement['override_cost'] ) {
						$updated_arrangement->override_cost = $arrangement['override_cost'];
					} else {
						$updated_arrangement->override_cost = NULL;
					}

					$updated_arrangement->save();
					// error_log( '$updated_arrangement->save(): ' . $updated_arrangement->name );

					if ( -1 == $arrangement['id'] ) {
						$new_recipe = new Arrangement_Recipe;
						$new_recipe->arrangement_id = $updated_arrangement->id;
						$new_recipe->name = 'Recipe';
						$new_recipe->save();

						$inc_recipes[] = array( 'id' => $new_recipe->id, 'name' => $new_recipe->name );
					}
				} else {
					$updated_arrangement->addon = $arrangement['addon'];
					$updated_arrangement->order = $arrangement['order'];
					$updated_arrangement->save();
				}

				if ( ! empty( $arrangement['duplicate_unique_key'] ) ) {
					$_arr[] = array(
						'key' => $arrangement['duplicate_unique_key'],
						'arr_id' => $updated_arrangement->id,
						'inc_recipes' => $inc_recipes,
						'inc_items'	=> $inc_items,
						'inc_photos' => $inc_photos,
						'total' => ! empty( $arrangement['from_recipe'] ) ? $updated_arrangement->get_final_price( false ) : false,
						'subtotal' => ! empty( $arrangement['from_recipe'] ) ? $updated_arrangement->get_final_price() : false,
						'is_percentage' => absint( $updated_arrangement->is_percentage ),
						'is_applied_all' => absint( $updated_arrangement->is_applied_all ),
						'applies_on' => empty( $updated_arrangement->applies_on ) || ! is_array( $updated_arrangement->applies_on ) ? array(): $updated_arrangement->applies_on,
						'tax' => absint( $updated_arrangement->tax ),
						'invoicing_category_id' => $updated_arrangement->invoicing_category_id,
						'fresh_flower_multiple' => is_null( $updated_arrangement->fresh_flower_multiple ) ? null : floatval( $updated_arrangement->fresh_flower_multiple ),
						'hardgood_multiple' => is_null( $updated_arrangement->hardgood_multiple ) ? null : floatval( $updated_arrangement->hardgood_multiple ),
						'global_labor_value' => is_null( $updated_arrangement->global_labor_value ) ? null : floatval( $updated_arrangement->global_labor_value ),
						'flower_labor' => is_null( $updated_arrangement->flower_labor ) ? null : floatval( $updated_arrangement->flower_labor ),
						'hardgood_labor' => is_null( $updated_arrangement->hardgood_labor ) ? null : floatval( $updated_arrangement->hardgood_labor ),
						'base_price_labor' => is_null( $updated_arrangement->base_price_labor ) ? null : floatval( $updated_arrangement->base_price_labor ),
						'fee_labor' => is_null( $updated_arrangement->fee_labor ) ? null : floatval( $updated_arrangement->fee_labor ),
						'rental_labor' => is_null( $updated_arrangement->rental_labor ) ? null : floatval( $updated_arrangement->rental_labor ),
					);
				}

			} catch (Humanreadable_Exception $e) {
				$r->fail($e->getMessage());
			}
		}
		$r->add_payload( 'inc_arrangements', $_arr );
	}

	if ( $payments ) {
		$updated_payments = array();
		$payments_to_send = array();
		try {
			if ( is_array( $payments ) && 1 == sizeof( $payments ) && '' == $payments[0] ) {
				Payment::where(array(
					'event_id' => $request->input( 'event_id' ),
					'user_id' => $user_id,
					'is_live' => 0,
					'status' => 'not-paid',
				))->delete();
			} else {
				$existing_payments = Payment::where(array(
					'event_id' => $request->input( 'event_id' ),
					'user_id' => $user_id,
					'is_live' => 0,
				))->get();

				$live_payments = Payment::where(array(
					'event_id' => $request->input( 'event_id' ),
					'user_id' => $user_id,
					'is_live' => 1,
				))->get();

				$existing_payments_all = $existing_payments->all();

				foreach ( $existing_payments as $_payment ) {
					$payment_exists = wp_list_filter( $payments, array( 'paymentId' => $_payment->id ) );
					// Delete the payment if it was not sent in the request
					if ( empty( $payment_exists ) && 'not-paid' == $_payment->status ) {
						$_payment->delete();
					} else if ( empty( $payment_exists ) && 'not-paid' != $_payment->status ) {
						// If the payment wasn't sent from the proposal, but exists and is paid
						// we'll send it back to the proposal - this should handle the case
						// where a customer made a live payment while the user is working on their proposal,
						// and we couldn't find a matching workroom payment and therefore created a new
						// workroom payment - this way we won't lose the record of the customer payment
						$payments_to_send[] = array(
							'paymentId'			=> $_payment->id,
							'paymentName'		=> $_payment->payment_name,
							'dueDate'			=> sc_get_user_date_time( $user_id, $_payment->due_date ),
							'dueDateRaw'		=> $_payment->due_date,
							'cardFee'			=> sc_format_price( $_payment->card_fee ),
							'convenienceFee'	=> ( 'complete' == $_payment->status ) ? $_payment->convenience_fee : $_payment->get_amount() * ( $_payment->card_fee / 100 ),
							'amount'			=> ! is_null( $_payment->amount ) ? sc_format_price( $_payment->amount ) : null,
							'payment_date'		=> is_null( $_payment->payment_date ) ? '' : sc_get_user_date_time( $user_id, $_payment->payment_date ),
							'amountPercentage'	=> is_null( $_payment->payment_percentage ) ? null : sc_format_price( $_payment->payment_percentage ),
							'amountType'		=> ! is_null( $_payment->payment_percentage ) && 'complete' != $_payment->status ? 'percentage' : 'currency',
							'paymentType'		=> $_payment->payment_type,
							'note'				=> $_payment->payment_note,
							'status'			=> $_payment->status,
						);
					}
				}

				foreach ( $payments as $i => $payment ) {
					if ( 'complete' == $payment['status'] && ( is_null( $payment['dueDate'] ) || empty( $payment['dueDate'] ) ) ) {
						throw new Humanreadable_Exception( 'Please fill out the due date field.' );
					}

					try {
						if ( -1 == $payment['paymentId'] ) {
							$new_payment = new Payment();
							$new_payment->user_id = $user_id;
							$new_payment->event_id = $request->input('event_id');
							$new_payment->is_live = 0;
						} else {
							$new_payment = array_filter( $existing_payments_all, function( $_pmt ) use ( $payment ){
								return $_pmt->id == $payment['paymentId'];
							} );

							if ( ! empty( $new_payment ) ) {
								$new_payment = array_shift( $new_payment );
							} else {
								$new_payment = new Payment();
								$new_payment->user_id = $user_id;
								$new_payment->event_id = $request->input('event_id');
							}
						}

						// Only update the payment info if the payment doesn't exist
						// Or the payment has not been made through Stripe
						if ( ! $new_payment->exists() || 'stripe' != $new_payment->payment_type ) {
							$new_payment->payment_name = $payment['paymentName'];
							$new_payment->amount = ! empty( $payment['amount'] ) ? floatval( $payment['amount'] ) : null;
							$new_payment->card_fee = floatval( $payment['cardFee'] );

							if ( 'complete' == $payment['status'] && ( 'card' == $payment['paymentType'] || '' == $payment['paymentType'] ) ) {
								$new_payment->convenience_fee = $new_payment->get_amount() * ( floatval( $payment['cardFee'] ) / 100 );
							} else if ( 'complete' != $payment['status'] ) {
								$new_payment->convenience_fee = NULL;
							}

							//$new_payment->convenience_fee = ! empty( $payment['amount'] ) ? floatval( $payment['amount'] ) * floatval( $payment['cardFee'] ) / 100 : null;
							//$new_payment->convenience_fee = $payment->get_amount() * floatval( $payment['cardFee'] ) / 100;
							$new_payment->payment_type = ! empty( $payment['paymentType'] ) ? $payment['paymentType'] : '';
							$new_payment->payment_percentage = ! empty( $payment['amountPercentage'] ) ? $payment['amountPercentage'] : null;
							$new_payment->status = $payment['status'];
							$new_payment->payment_note = ! empty( $payment['note'] ) ? $payment['note'] : '';
							if ( 'not-paid' != $new_payment->status ) {
								if ( ! empty( $new_payment->payment_percentage ) && is_null( $new_payment->amount ) ) {
									$new_payment->amount = $event->get_total( true ) * ( $new_payment->payment_percentage / 100 );
								}
								$new_payment->payment_percentage = null;
							}

							$due_date = DateTime::createFromFormat( $date_format, $payment['dueDate'] );
							if ( $due_date ) {
								$new_payment->due_date = date( 'Y-m-d', $due_date->getTimestamp() );
							}

							if ( ! empty( $payment['payment_date'] ) ) {
								$payment_date = DateTime::createFromFormat( $date_format, $payment['payment_date'] );
								if ( $payment_date ) {
									$new_payment->payment_date = date( MYSQL_DATE_FORMAT, $payment_date->getTimestamp() );
								}
							} else {
								$new_payment->payment_date = null;
							}

							$new_payment->amount = is_null( $new_payment->amount ) ? $new_payment->amount : floatval( $new_payment->amount );
							$new_payment->save();

							$updated_payments[] = $new_payment;
						}

						do_action( 'sc/payment/save', array( $event, $new_payment ) );

						if ( ! empty( $payment['duplicate_unique_key'] ) ) {
							$payments_to_send[] = array(
								'key' => $payment['duplicate_unique_key'],
								'id' => $new_payment->id,
							);
						}
					} catch (Exception $e) {
						throw new Humanreadable_Exception('Payment creation failed.');
					}
				}

				foreach ( $updated_payments as $_payment ) {
					$calc_amount = $_payment->get_amount();

					foreach ( $live_payments as $live_payment ) {
						if ( $_payment->due_date == $live_payment->due_date && $calc_amount == sc_format_price( $live_payment->amount ) ) {
							$live_payment->status = $_payment->status;
							$live_payment->payment_date = $_payment->payment_date;
							$live_payment->save();
						}
					}
				}
			}
		} catch (Humanreadable_Exception $e) {
			$r->fail( $e->getMessage() );
		}

		$r->add_payload( 'inc_payments', $payments_to_send );

		$live_payments = array();
		$_live_payments = Payment::where(array(
			'user_id' => $user_id,
			'event_id' => $event->id,
			'is_live' => 1,
		))->get()->all();

		foreach ( $_live_payments as $payment ) {
			$live_payments[] = array(
				'paymentId'			=> $payment->id,
				'paymentName'		=> $payment->payment_name,
				'dueDate'			=> sc_get_user_date_time( $user_id, $payment->due_date ),
				'dueDateRaw'		=> $payment->due_date,
				'cardFee'			=> sc_format_price( $payment->card_fee ),
				'convenienceFee'	=> ( 'complete' == $payment->status ) ? $payment->convenience_fee : sc_format_price( $payment->convenience_fee ),
				'amount'			=> ! is_null( $payment->amount ) ? sc_format_price( $payment->amount ) : null,
				'payment_date'		=> is_null( $payment->payment_date ) ? '' : sc_get_user_date_time( $user_id, $payment->payment_date ),
				'amountPercentage'	=> is_null( $payment->payment_percentage ) ? null : sc_format_price( $payment->payment_percentage ),
				'amountType'		=> ! is_null( $payment->payment_percentage ) && 'complete' != $payment->status ? 'percentage' : 'currency',
				'paymentType'		=> $payment->payment_type,
				'note'				=> $payment->payment_note,
				'status'			=> $payment->status,
			);
		}

		$r->add_payload( 'livePayments', $live_payments );
	}

	if ( ! $event ) {
		$r->fail( 'Something went wrong and we can not save the event at this time. Please try again later.' );
	}

	$event->name = $request->input('event_name');
	$event->card = floatval( $request->input( 'card' ) );
	$event->hide_item_prices = $request->input( 'hide_item_prices' );
	$event->hide_item_prices = ! empty( $event->hide_item_prices ) ? 1 : 0;
	$event->separate_cc_fee = intval($request->input( 'separate_cc_fee' ));
	$event->visible_payments = intval($request->input( 'visible_payments' ));
	$event->hidden_contract = intval($request->input( 'hidden_contract' ));
	$event_date = DateTime::createFromFormat( $date_format, $request->input( 'event_date' ) );
	if ( $event_date ) {
		$event->date = date( MYSQL_DATE_FORMAT, $event_date->getTimestamp() );
	}

	$event->cover_id = (int) $request->input( 'cover_id', 0 );

	$event->tax_rate_id = empty( $request->input( 'tax_rate_id' ) ) ? 0 : absint( $request->input( 'tax_rate_id' ) );
	$event->tax_rate_value = floatval( $request->input( 'tax_rate_value' ) );
	$event->tax_rate_id2 = empty( $request->input( 'tax_rate_id2' ) ) ? 0 : absint( $request->input( 'tax_rate_id2' ) );
	$event->tax_rate_value2 = floatval( $request->input( 'tax_rate_value2' ) );

	if ( ! is_null( $contract_note ) ) {
		$event->contract_note = $contract_note;
	}

	if ( ! is_null( $event_notes ) ) {
		$event->event_note = $event_notes;
	}

	if ( ! is_null( $private_notes ) ) {
		$event->private_note = $private_notes;
	}

	if ( ! empty( $invoicing_category_id ) ) {
		if ( is_array( $invoicing_category_id ) ) {
			$event->invoicing_category_id = $invoicing_category_id['categoryId'];
		} else {
			$event->invoicing_category_id = $invoicing_category_id;
		}
	}

	if ( ! empty( $invoicing_settings ) ) {
		$event->hardgood_multiple = $invoicing_settings['hardgood_multiple'];
		$event->fresh_flower_multiple = $invoicing_settings['fresh_flower_multiple'];
		$event->global_labor_value = $invoicing_settings['labor']['global_labor_value'];
		$event->flower_labor = $invoicing_settings['labor']['flower'];
		$event->hardgood_labor = $invoicing_settings['labor']['hardgood'];
		$event->base_price_labor = $invoicing_settings['labor']['base_price'];
		$event->fee_labor = $invoicing_settings['labor']['fee'];
		$event->rental_labor = $invoicing_settings['labor']['rental'];
		// $values = array(
		// 	'hardgood_multiple' => $event->hardgood_multiple,
		// 	'fresh_flower_multiple' => $event->fresh_flower_multiple,
		// 	'labor' => array(
		// 		'global_labor_value' => $event->global_labor_value,
		// 		'flower' => $event->flower_labor,
		// 		'hardgood' => $event->hardgood_labor,
		// 		'base_price' => $event->base_price_labor,
		// 		'fee' => $event->fee_labor,
		// 		'rental' => $event->rental_labor
		// 	)
		// );
	}

	$event->save();

	do_action( 'sc/event/save', $event );

	$event = Event::where(array(
		'id' => $request->input('event_id'),
		'user_id' => $user_id
	))->with( array(
		'arrangements' => function($query){
			$query->orderBy( 'order', 'asc' );
		},
		'arrangements.items',
		'arrangements.items.item_variation',
		'arrangements.items.item_variation.item',
	) )->first();

	$tax_rates = Tax_Rate::where( array(
		'user_id' => $user_id
	) )->get()->toArray();

	$tax_rate_ids = array();
	foreach ($tax_rates as $tax_rate) {
		/** The value of the tax rate was modified */
		if ( $tax_rate['id'] == $event->tax_rate_id &&
			$tax_rate['value'] != $event->tax_rate_value ) {
			array_push( $tax_rates, array(
				'id' => -$tax_rate['id'],
				'user_id' => $user_id,
				'name' => 'Modified Tax Rate',
				'value' => $event->tax_rate_value
			) );
		}

		if ( $tax_rate['id'] == $event->tax_rate_id2 &&
			$tax_rate['value'] != $event->tax_rate_value2 ) {
			array_push( $tax_rates, array(
				'id' => -$tax_rate['id'],
				'user_id' => $user_id,
				'name' => 'Modified Tax Rate 2',
				'value' => $event->tax_rate_value2
			) );
		}

		array_push( $tax_rate_ids, $tax_rate['id'] );
	}

	/**
	 * If the Event Tax Rate is not found in the User Tax Rates
	 * It means that it was deleted
	 */
	if ( (
			! in_array( $event->tax_rate_id, $tax_rate_ids ) &&
			! empty( $tax_rate_ids ) &&
			! is_null( $event->tax_rate_id ) &&
			$event->tax_rate_id != 0
		) || (
			empty( $tax_rate_ids ) && $event->tax_rate_id != 0
		) ) {
		array_push( $tax_rates, array(
			'id' => -$event->tax_rate_id,
			'user_id' => $user_id,
			'name' => 'Deleted Tax Rate',
			'value' => $event->tax_rate_value
		) );
	}

	if ( (
			! in_array( $event->tax_rate_id2, $tax_rate_ids ) &&
			! empty( $tax_rate_ids ) &&
			! is_null( $event->tax_rate_id2 ) &&
			$event->tax_rate_id2 != 0
		) || (
			empty( $tax_rate_ids ) && $event->tax_rate_id2 != 0
		) ) {
		array_push( $tax_rates, array(
			'id' => -$event->tax_rate_id2,
			'user_id' => $user_id,
			'name' => 'Deleted Tax Rate 2',
			'value' => $event->tax_rate_value2
		) );
	}

	$venues = $request->input('venues');
	$venue_ids = $venues && is_array( $venues ) ? wp_list_pluck( $venues, 'venue_id' ) : array();
	$venue_types = $venues && is_array( $venues ) ? wp_list_pluck( $venues, 'venue_type' ) : array();

	$linked_event_vendors = Meta::where(array(
		'type' => 'event',
		'type_id' => $event->id,
		'meta_key' => 'linked_vendor'
	))->delete();

	$linked_event_venue_types = Meta::where(array(
		'type' => 'event',
		'type_id' => $event->id,
		'meta_key' => 'venue_types'
	))->delete();

	$filtered_venue_types = array();

	if ( ! empty( $venue_ids ) && is_array( $venue_ids ) ) {
		foreach ($venue_ids as $i => $venue_id) {
			if ($venue_id != -1) {
				$event_meta = new Meta();
				$event_meta->type = 'event';
				$event_meta->type_id = $event->id;
				$event_meta->meta_key = 'linked_vendor';
				$event_meta->meta_value = abs($venue_id);
				$event_meta->save();

				$filtered_venue_types[] = $venue_types[$i];
			}
		}
	}

	$event_meta = new Meta();
	$event_meta->type = 'event';
	$event_meta->type_id = $event->id;
	$event_meta->meta_key = 'venue_types';
	$event_meta->meta_value = json_encode($filtered_venue_types);
	$event_meta->save();

	$signatures = $request->input( 'signatures' );
	if ( ! empty( $signatures ) ) {
		$signature_options_meta = Meta::firstOrNew(array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'signature_opts',
		));

		/*if ( ! $signature_options->exists || ! ( $signature_options = json_decode( $signature_options_meta->meta_value, true ) ) ) {
			$signature_options = array(
				'last_index'   => 1,
				'all'          => array(
					array(
						'key'        => 'sigopt_0',
						'type'       => 'customer',
						'label'      => 'Client Signature',
						'signature'  => false,
					),
				),
			);
		}*/

		$signature_options = $signatures;
		// Drop signature data - it's saved separately
		foreach ( $signature_options['all'] as $i => $sign_opt ) {
			unset( $signature_options['all'][ $i ]['signature'] );
		}

		try {
			$signature_options_meta->meta_value = json_encode( $signature_options );
			$signature_options_meta->save();
		} catch (Exception $e) {
			$r->fail( 'Failed to save Signature Options' );
		}
	}

	//Save attachments in meta
	$attachment_ids = ( empty( $request->input( 'attachments' ) ) ) ? array() : $request->input( 'attachments' );
	$attachment_ids = array_map( function( $att ) {
		return ( $att['id'] );
	}, $attachment_ids );

	$attachments = Meta::where( array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'attachments',
		))->get();

	if ( $attachments->count() < count( $attachment_ids ) ) {
		for ( $i = $attachments->count(); $i < count( $attachment_ids ); $i ++ ) {
			$attachments->push(
				new Meta( array(
					'type' => 'event',
					'type_id' => $event->id,
					'meta_key' => 'attachments',
				) )
			);
		}
	}

	foreach ( $attachments as $i => $attachment_meta ) {
		if ( isset( $attachment_ids[ $i ] ) ) {
			if ( $attachment_meta->meta_value != $attachment_ids[ $i ] ) {
				$attachment_meta->meta_value = $attachment_ids[ $i ];
				$attachment_meta->save();
			}
		} else {
			$attachment_meta->delete();
		}
	}
/*

		$dump = Meta::where( array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'attachments',
		))->get();
		foreach ($dump as $p) {
			error_log( var_export( $p->meta_value, true ) );
		}*/

/*


		$linked_customers = Meta::where(array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'linked_customer'
		))->get();

		// Create any extra meta that we need
		if ( $linked_customers->count() < count( $customer_ids ) ) {
			for ( $i = $linked_customers->count(); $i < count( $customer_ids ); $i ++ ) {
				$linked_customers->push(
					new Meta( array(
						'type' => 'event',
						'type_id' => $event->id,
						'meta_key' => 'linked_customer',
					) )
				);
			}
		}

		foreach ( $linked_customers as $i => $customer_meta ) {
			if ( isset( $customer_ids[ $i ] ) ) {
				if ( $customer_meta->meta_value != $customer_ids[ $i ] ) {
					$customer_meta->meta_value = $customer_ids[ $i ];
					$customer_meta->save();
				}
			} else {
				$customer_meta->delete();
			}
		}




*/

	//refresh the venues list in js
	$r->add_payload('venues_list', sc_get_user_venues_list());

	$event_cost = array(
		'subtotal'			=> $event->get_subtotal( true ),
		'tax'				=> $event->get_tax( true ),
		'tax2'				=> $event->get_tax2( true ),
		'tax_rate_id'		=> intval( $event->tax_rate_id ),
		'tax_rate_value'	=> floatval( $event->tax_rate_value ),
		'tax_rate_id2'		=> intval( $event->tax_rate_id2 ),
		'tax_rate_value2'	=> floatval( $event->tax_rate_value2 ),
		'total'				=> $event->get_total( true ),
		'tax_rates'			=> $tax_rates,
	);

	$r->add_payload('eventCost', $event_cost);
	$r->respond();
}
add_action('wp_ajax_sc_edit_event_proposal', 'sc_edit_event_proposal');

function sc_manage_app_entry_points() {
	if ( ! is_page() ) {
		return;
	}

	$template = get_post_meta( get_the_ID(), '_wp_page_template', true );
	sc_clean_input_slashes();
	$request = Request::capture();

	switch ( $template ) {
		case 'template-events.php':

			break;

		case 'template-stemcounter.php':
			do_action( 'sc/app/load/event', intval( $request->input( 'eid' ) ) );
			break;

		case 'template-shopping.php':
			do_action( 'sc/app/load/event_shopping', intval( $request->input( 'eid' ) ) );
			break;

		case 'template-proposal.php':
			do_action( 'sc/app/load/event_proposal', intval( $request->input( 'eid' ) ), $request->input('view') );
			break;

		case 'template-proposal_2_0.php':
		case 'template-proposal-2.0.php':
			do_action( 'sc/app/load/event_proposal_2_0', intval( $request->input( 'eid' ) ), $request->input('view') );
			break;

		case 'template-flowers.php':
			do_action( 'sc/app/load/flowers' );
			break;

		case 'template-services.php':
			do_action( 'sc/app/load/services' );
			break;

		default:
			break;
	}
}
add_action( 'template_redirect', 'sc_manage_app_entry_points', 10 );

function sc_app_headers() {
	if ( ! is_page() ) {
		return;
	}

	$template = get_post_meta( get_the_ID(), '_wp_page_template', true );
	sc_clean_input_slashes();
	$request = Request::capture();

	switch ( $template ) {
		case 'template-events.php':
		case 'template-archive.php':
			do_action( 'sc/app/head/events' );
			break;

		case 'template-stemcounter.php':
			do_action( 'sc/app/head/event', intval( $request->input( 'eid' ) ) );
			break;

		case 'template-shopping.php':
			do_action( 'sc/app/head/event_shopping', intval( $request->input( 'eid' ) ) );
			break;

		case 'template-proposal.php':
			do_action( 'sc/app/head/event_proposal', intval( $request->input( 'eid' ) ), $request->input('view') );
			break;

		case 'template-proposal_2_0.php':
		case 'template-proposal-2.0.php':
			do_action( 'sc/app/head/event_proposal_2_0', intval( $request->input( 'eid' ) ), $request->input('view') );
			break;

		case 'template-flowers.php':
			do_action( 'sc/app/head/flowers' );
			break;

		case 'template-services.php':
			do_action( 'sc/app/head/services' );
			break;

		default:
			break;
	}
}
add_action( 'wp_head', 'sc_app_headers', 0 );

function sc_app_footers() {
	if ( ! is_page() ) {
		return;
	}

	$template = get_post_meta( get_the_ID(), '_wp_page_template', true );
	sc_clean_input_slashes();
	$request = Request::capture();

	do_action( 'sc/app/footer', $template );

	switch ( $template ) {
		case 'template-archive.php':
		case 'template-events.php':
			do_action( 'sc/app/footer/events' );
			break;

		case 'template-stemcounter.php':
			do_action( 'sc/app/footer/event', intval( $request->input( 'eid' ) ) );
			break;

		case 'template-shopping.php':
			do_action( 'sc/app/footer/event_shopping', intval( $request->input( 'eid' ) ) );
			break;

		case 'template-proposal.php':
			do_action( 'sc/app/footer/event_proposal', intval( $request->input( 'eid' ) ) );
			break;

		case 'template-proposal-2.0.php':
			do_action( 'sc/app/footer/event_proposal_2_0', intval( $request->input( 'eid' ) ) );
			break;

		case 'template-flowers.php':
			do_action( 'sc/app/footer/flowers' );
			break;
		case 'template-order.php':
			do_action ('sc/app/footer/order');
			break;
		case 'template-vendors.php':
			do_action ('sc/app/footer/vendors');
			break;
		case 'template-customers.php':
			do_action ('sc/app/footer/customers');
			break;
		case 'template-services.php':
			do_action( 'sc/app/footer/services' );
			break;
		case 'template-recipes.php':
			do_action( 'sc/app/footer/recipes' );
			break;
		case 'template-studio.php':
			do_action( 'sc/app/footer/studio' );
			break;
		case 'template-forms.php':
			do_action( 'sc/app/footer/forms' );
			break;
		default:
			break;
	}
}
add_action( 'wp_footer', 'sc_app_footers', 5 );

function sc_check_event_ownership( $event_id ) {
	$user_id = intval( get_current_user_id() );
	$url = array(
		'sc/app/load/event' => sc_get_arrangements_page_url(),
		'sc/app/load/event_shopping' => sc_get_shopping_page_url(),
		'sc/app/load/event_proposal' => sc_get_proposal_page_url(),
		'sc/app/load/event_proposal_2_0' => sc_get_proposal_2_0_page_url()
	);
	$action = current_filter();
	$GLOBALS['sc_current_event'] = sc_event_ownership_checkpoint( $event_id, $user_id, true, $url[$action] );
}
add_action( 'sc/app/load/event', 'sc_check_event_ownership', 10 );
add_action( 'sc/app/load/event_shopping', 'sc_check_event_ownership', 10 );
add_action( 'sc/app/load/event_proposal', 'sc_check_event_ownership', 10 );
add_action( 'sc/app/load/event_proposal_2_0', 'sc_check_event_ownership', 10 );

function sc_check_event_proposal_access( $event_id, $view ) {
	$user_id = intval( get_current_user_id() );
	sc_event_ownership_checkpoint( $event_id, $user_id, true, sc_get_proposal_2_0_page_url() );

	// If the current user can't see the current page and is requesting a view(so far only PDF generation)
	// Redirect them to the standard view, so that a proper message can be displayed via 'the_content()'
	if ( ! current_user_can( 'memberpress-authorized' ) && $view ) {
		wp_redirect( esc_url_raw( remove_query_arg( 'view' ) ) );
		exit;
	}
}
add_action( 'sc/app/load/event_proposal', 'sc_check_event_proposal_access', 10, 2 );

function sc_render_arrangements_js( $event_id ) {
	try {
		$event = sc_user_can_edit_event( $event_id );
	} catch (Exception $e) {
		return;
	}
	 ?>
	<script type="text/javascript">
		var eventId = <?php echo json_encode($event_id) ?>;
		stemcounter.page.userItems = <?php echo sc_get_user_items_json( $event->user_id ); ?>;

		(function($){

			function openArrangementPopup(id, title) {
				title = (typeof title == 'undefined') ? 'Add New Arrangement' : title;
				var url = stemcounter.aurl({
					action: 'sc_edit_user_arrangement_form',
					id: id ? id : 0,
					event_id: eventId
				});

				stemcounter.openAjaxModal(title, url);
			}

			stemcounter.page.createNewItemCaller = null;
			function createNewItem(caller) {
				stemcounter.page.createNewItemCaller = caller;
				var url = window.stemcounter.aurl({
					action: 'sc_edit_item_form'
				});

				stemcounter.openAjaxModal('Add New Item', url);
			}

			function editItem(itemId, itemType) {
				var url = window.stemcounter.aurl({
					action: 'sc_edit_item_form',
					id: itemId,
					type: itemType
				});

				stemcounter.openAjaxModal('Edit Item', url);
			}

			$('.add-arrangement-button').on('click', function(e){
				e.preventDefault();
				openArrangementPopup(0);
			});

			$('.edit-arrangement-button').on('click', function(e){
				e.preventDefault();
				var id = $(this).attr('data-id');
				openArrangementPopup(id, 'Edit Arrangement');
			});

			$(document).on('stemcounter.action.renderAddNewItemForm', function(e, caller){
				createNewItem(caller);
			});

			$(document).on('stemcounter.action.renderEditItemForm', function(e,  r, itemId, itemType){
				editItem(itemId, itemType);
			});

			$(document).ready(function() {

				var list = $('#m-container');

				list.isotope({
					itemSelector: '.grid-item',
					percentPosition: true,
					masonry: {
						columnWidth: '.grid-sizer'
					},
					transitionDuration: 0
				});

				list.sortable({
					cursor: 'move',
					start: function(event, ui) {
						ui.item.addClass('grabbing moving').removeClass('isotopey');
						ui.placeholder
							.addClass('starting')
							.removeClass('moving')
							.css({
								top: ui.originalPosition.top,
								left: ui.originalPosition.left
							});
						list.isotope('reloadItems');
					},
					change: function(event, ui) {
						ui.placeholder.removeClass('starting');
						list
							.isotope('reloadItems')
							.isotope({ sortBy: 'original-order'});
					},
					beforeStop: function(event, ui) {
						ui.placeholder.after(ui.item);
					},
					stop: function(event, ui) {
						ui.item.removeClass('grabbing').addClass('isotopey');
						list
							.isotope('reloadItems')
							.isotope({ sortBy: 'original-order' }, function(){
								console.log(ui.item.is('.grabbing'));
								if (!ui.item.is('.grabbing')) {
								  ui.item.removeClass('moving');
								}
						});
						init_ajax_arrangements_order();
					}
				});

				function init_ajax_arrangements_order() {

					var $elements = $('#m-container .arrangement-list-item'),
						element = {};

					$elements.each(function(index, el) {
						element_id = $(this).find('.edit-arrangement-button').attr('data-id');
						element_position = index;

						element[element_id] = element_position;
					});

					$.post(
						stemcounter.ajax_url,
						{
							'action': 'sc_arrangement_order',
							'order':   element,
							'event_id': eventId
						}
					);
				}

				$('.edit-arrangement-button').qtip({
					content: 'Edit Arrangement'
				});

				$('.toggle-arrangement-math-button').qtip({
					content: 'Show Pricing Algorithm'
				});

				$('[data-tooltip]').qtip({
					content: {
						attr: 'data-tooltip'
					}
				});

				$('.toggle-arrangement-math-button').click(function(e){
					e.preventDefault();

					var parent = $(this).closest('.arrangement-list-item');
					parent.toggleClass('arrangement-math-hidden');
					parent.toggleClass('arrangement-math-shown');

					$('#m-container').isotope('layout');
				});

			});

			$(document).on('stemcounter.action.deleteArrangement', function(e, data){
				if (!confirm('Are you sure you wish to delete this arrangement?')) {
					return;
				}

				var url = window.stemcounter.aurl({
					action: 'sc_delete_arrangement',
					id: data.arrangementId
				});

				$.get(url, {}, function(response){
					stemcounter.JSONResponse(response, function(r){
						stemcounter.gaEvent('arrangement', 'deleted');
						window.location.reload();
					});
				});
			});

		})(jQuery);
	</script>
	<?php
}
add_action( 'sc/app/footer/event', 'sc_render_arrangements_js', 10 );

function eventsTable_state_ajax() {
	if ( ! isset( $_REQUEST['state'] )  && ! isset( $_REQUEST['event_id'] ) ) {
		wp_send_json_error( array( 'data' => 'The data received is empty!' ) );
	}

	$state = $_REQUEST['state'];
	$event_id = $_REQUEST['event_id'];
	$search = array( 'user_id' => get_current_user_id(), 'id' => $event_id );

	$r = new Ajax_Response;

	try {
		$event = Event::where( $search )->firstOrFail();
	} catch (Exception $e) {
		$r->fail( 'The event you are trying to edit does not exist.' . $e->getMessage() );
	}

	$event->state = $state;
	$event->save();

	do_action( 'sc/event/save', $event );

	do_action( 'sc/event/edit/response', $r );

	$r->respond();
}
add_action( 'wp_ajax_sc_event_state', 'eventsTable_state_ajax', 10 );

function custom_rewrite_permastructs() {
	add_rewrite_rule( '^view-proposal/([^/]+)/?$', 'index.php?event-uid=$matches[1]', 'top' );
	add_rewrite_rule( '^view-proposal/([^/]+)/receipt/(\d+)/?$', 'index.php?event-uid=$matches[1]&receipt-id=$matches[2]', 'top' );
	add_rewrite_rule( '^view-proposal/([^/]+)/invoice/?$', 'index.php?event-uid=$matches[1]&invoice=1', 'top' );

	add_rewrite_tag( '%event-uid%', '([^/]+)' );
	add_rewrite_tag( '%receipt-id%', '(\d+)' );
	add_rewrite_tag( '%invoice%', '(\d+)' );
}
add_action( 'init', 'custom_rewrite_permastructs' );

function sc_change_event_uid( $wp ) {
	global $wp_query, $post, $wp_the_query;
	if ( ! empty( $wp->query_vars['event-uid'] ) ) {
		$event_id = abs( sc_decrypted_string( $wp->query_vars['event-uid'] ) );
		if ( $event_id ) {
			remove_action( 'wp_head', 'rel_canonical' );
			remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
			remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
			remove_action( 'wp_head', 'sc_get_header_og_image', 0 );
			remove_action( 'wp_head', 'sc_render_facebook_pixel_js', 9999 );

			$GLOBALS['sc_current_event'] = $event = Event::where(array(
				'id'=>$event_id,
			))->with(array(
				'arrangements'=>function($query) {
					$query->where('include', '=', 1)->orderBy( 'order', 'asc' );
				},
			), 'arrangements.items', 'arrangements.items.item_variation', 'arrangements.items.item_variation.item')->first();

			$GLOBALS['user_id'] = $event->user_id;
			if ( ! empty( $wp->query_vars['receipt-id'] ) ) {
				$receipt_id = abs( $wp->query_vars['receipt-id'] );
				if ( $receipt_id ) {

					$receipt = Payment::where(array(
						'id' => $receipt_id,
						'event_id'=> $event_id
					))->first();

					if ( $receipt ) {
						remove_action('wp_head', 'print_emoji_detection_script', 7);

						$_GET['eid'] = $event_id;
						$_GET['rid'] = $receipt_id;

						$args = array(
							'company_info'	=> get_company_info( $event->user_id ),
							'customer'		=> sc_get_user_event_client( $event->id, true ),
							'receipt'		=> $receipt,
							'page_title'    => 'Payment Receipt',
							'event'         => $event,
						);

						sc_render_template( 'templates/html-header', $args, false );

						sc_render_template( 'templates/template-receipts', $args, false );

						sc_render_template( 'templates/html-footer', $args, false );

						exit;
					} else {
						echo 'Something went wrong.';
						exit;
					}
				}
			} else if ( ! empty( $wp->query_vars['invoice'] ) ) {
				remove_action('wp_head', 'print_emoji_detection_script', 7);

				try {
					$version = Event_Version::where(array(
						'event_id' => $event->id,
					))->orderBy('created_at', 'desc')->first();
				} catch (Exception $e) {
					 exit( 'Something went wrong!' );
				}

				$event_data = json_decode( $version->event_data, false );
				$customers = isset( $event_data->customers ) ? $event_data->customers : (array) $event_data->customer;
				// Convert single customer to multiple customers
				if ( isset( $customers['id'] ) || isset( $customers['name'] ) ) {
					$customers = array( $customers );
				}
				$args = array(
					'company_info'	=> $event_data->company_info,
					'customers'		=> $customers,
					'page_title'    => 'Invoice',
					'event'         => $event_data->event,
					'payments'		=> $event_data->payments,
					'user_id'		=> $event->user_id,
					'eventCost'		=> $event_data->eventCost,
					'created_at'	=> $version->created_at,
				);

				sc_render_template( 'templates/html-header', $args, false );

				sc_render_template( 'templates/template-invoices', $args, false );

				sc_render_template( 'templates/html-footer', $args, false );

				exit;
			}
			add_action( 'wp_head', 'sc_public_og_image', 0 );

			do_action( 'sc/app/load/event_proposal_2_0-public' );

			$event->arrangements->sortBy( function($arrangement){
				return $arrangement->order;
			} );

			$company_info = get_company_info( $event->user_id );

			$userdata = get_userdata( $event->user_id );

			$additionalInfo = unserialize($userdata->additionalInfo); //Get User Details
			$phone = isset($additionalInfo['companyMobile'])  ? $additionalInfo['companyMobile'] : '';

			$skip_ownership_check = true;

			$proposal_2_0_page = sc_get_id_by_template( 'template-proposal-2.0.php' );
			if ( ! $proposal_2_0_page ) {
				return;
			}

			remove_action('wp_head', 'print_emoji_detection_script', 7);

			$wp_the_query = $wp_query = new WP_Query( array(
				'page_id' => $proposal_2_0_page,
			) );

			$wp_query->the_post();

			$_GET['eid'] = $event_id;

			add_filter( 'body_class', 'sc_add_public_proposal_class' );

			$content_override = sc_render_event_proposal_2_0( '' );

			include( locate_template( array( 'template-proposal-2.0.php' ) ) );

			exit;
		}
	}
}
add_action( 'parse_request', 'sc_change_event_uid' );

function sc_add_public_proposal_class( $classes ) {
	$classes[] = 'public-proposal';

	return $classes;
}

function arr_shop_page_title($title, $sep) {
	$request = Request::capture();
	$event_id = $request->input('eid');
	$event = Event::where('id', '=', $event_id)->first();

	if ( is_page_template( 'template-stemcounter.php' ) ||
		is_page_template( 'template-shopping.php' ) ||
		is_page_template( 'template-proposal-2.0.php' ) ||
		is_page_template( 'template-recipe-sheet.php' ) ) {

		$title = $event->name . ' | ' . sc_get_user_date_time( $event->user_id, $event->date->toDateTimeString() ) . ' | ' . $title;
	}

	return $title;
}
add_filter('wp_title', 'arr_shop_page_title', 10, 2);

function sc_arrangements_print() {
	if ( ! empty( $_GET['do_print'] ) ) {
	?>
	<script type="text/javascript">
	(function($){
		$( document ).ready(function() {
			window.print();
		});
	})(jQuery);
	</script>
	<?php
	}
}
add_action( 'sc/app/footer/event', 'sc_arrangements_print', 10 );

function sc_on_view_proposal() {
	if ( ! is_user_logged_in() || ! empty( $GLOBALS['wp']->query_vars['event-uid'] ) ) {
		remove_action( 'wp_footer', 'add_intercom_snippet' );
	}
}
add_action( 'sc/app/footer/event_proposal_2_0', 'sc_on_view_proposal', 10 );

function sc_docraptor_config_print() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();
	$print_url = esc_url( home_url( '/view-proposal/' . sc_encrypted_string( $request->event_id ) ) );
	/* Set the event_id as user_id if download is trigger from public link */
 	$user_id = get_current_user_id() ? get_current_user_id() : intval( $request->event_id );

	try {
		$version = Event_Version::where(array(
			'event_id' => intval( $request->event_id ),
		))->orderBy('created_at', 'desc')->first();
	} catch (Exception $e) {
		$r->fail( 'This event version doesn\'t exist.' );
	}

	$event_data = json_decode( $version->event_data );
	$event = $event_data->event;

	$configuration = DocRaptor\Configuration::getDefaultConfiguration();
	$configuration->setUsername( DOCRAPTOR_API_KEY ); // this key works for test documents
	// $configuration->setDebug(true);
	$docraptor = new DocRaptor\DocApi();
	try {
		$doc = new DocRaptor\Doc();
		$doc->setTest( DOCRAPTOR_TEST_DOC );                                                   // test documents are free but watermarked
		// $doc->setDocumentContent("<html><body>Hello World</body></html>");     // supply content directly
		$doc->setDocumentUrl( $print_url ); // or use a url
		$doc->setName("docraptor-php.pdf");                                    // help you find a document later
		$doc->setDocumentType("pdf");                                          // pdf or xls or xlsx
		$doc->setJavascript( true );                                          // enable JavaScript processing
		$doc->setPipeline( '5' );                                              // Set the pipeline version
		// $prince_options = new DocRaptor\PrinceOptions();                    // pdf-specific options
		// $doc->setPrinceOptions($prince_options)
		// $prince_options->setMedia("screen");                                // use screen styles instead of print styles
		// $prince_options->setBaseurl("http://hello.com");                    // pretend URL when using document_content

		$create_response = $docraptor->createDoc($doc);

		$directory = dirname( dirname( ABSPATH ) ) . '/temp-pdfs/' . trailingslashit( $user_id );
		if ( ! file_exists( $directory ) || ! is_dir( $directory ) ) {
			mkdir( $directory, 0755, true );
		}

		$file_name = sanitize_file_name( $event->name . '.pdf' );

		file_put_contents( $directory . $file_name, $create_response );

		$r->add_payload( 'download_link', add_query_arg( array(
			'file' => $file_name,
			'user_id' => $user_id,
			'action' => 'sc/download_pdf',
		), admin_url( 'admin-ajax.php', is_ssl() ? 'https' : 'http' ) ) );

		$args = array( array(
				'file_name'	=> $file_name,
				'user_id'	=> $user_id,
			) );
		wp_schedule_single_event(
			time() + ( 10 * MINUTE_IN_SECONDS ),
			'sc/delete_proposal_pdf',
			$args
		);

	} catch (DocRaptor\ApiException $e) {
		$r->fail($e->getMessage());
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_proposal_print', 'sc_docraptor_config_print', 10 );
add_action( 'wp_ajax_nopriv_sc_proposal_print', 'sc_docraptor_config_print', 10 );

function sc_print_download_link() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$file = dirname( dirname( ABSPATH ) ) . '/temp-pdfs/' . trailingslashit( $request->user_id ) . $request->file;
	if ( file_exists( $file ) ) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment; filename='. $request->file);
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize( $file ));

		ob_clean();
		flush();

		$handle = fopen($file, "r");
		$contents = fread($handle, filesize($file));
		fclose($handle);
		echo($contents);
		/*readfile( $file );*/
	}
	exit;
}
add_action( 'wp_ajax_sc/download_pdf', 'sc_print_download_link', 10 );
add_action( 'wp_ajax_nopriv_sc/download_pdf', 'sc_print_download_link', 10 );

function sc_delete_generated_pdf( $args ) {
	$file = dirname( dirname( ABSPATH ) ) . '/temp-pdfs/' . trailingslashit( $args['user_id'] ) . $args['file_name'];

	if ( file_exists( $file ) ) {
		unlink( $file );
	}
}
add_action( 'sc/delete_proposal_pdf', 'sc_delete_generated_pdf', 10 );

function sc_proposal_crop_cover_image() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();
	$event = null;

	try {
		$event = sc_user_can_edit_event( intval( $request->event_id ) );
	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}

	try {
		$attachment_id = $request->input( 'attachment_id' );
		$wp_upload_dir = wp_upload_dir();
		$attachment_path = get_attached_file( $attachment_id );
		if ( false === stripos( $attachment_path, 's3://' ) ) {
			$attachment_path = trailingslashit( $wp_upload_dir['basedir'] ) . $attachment_path;
		}

		$image = wp_get_attachment_image_src( $attachment_id, 'full' );
		$edit_image = wp_get_image_editor( $attachment_path );
		$crop = $request->input( 'crop' );

		if ( ! is_wp_error( $edit_image ) && ! empty( $crop ) ) {

			$crop_x = $image[1] ? $image[1] * ( $crop['crop_x'] / 100 ) : 0;
			$crop_y = $image[2] ? $image[2] * ( $crop['crop_y'] / 100 ) : 0;
			$crop_width = $image[1] ? $image[1] * ( $crop['width_p'] / 100 ) : 0;
			$crop_height = $image[2] ? $image[2] * ( $crop['height_p'] / 100 ) : 0;

			$edit_image->load();

			// Crop the attachment
			$edit_image->crop( $crop_x, $crop_y, $crop_width , $crop_height );
			// Set path for cropped image
			$new_image_path = wp_unique_filename( dirname( $attachment_path ), basename( $attachment_path ) );
			$temp_name = tempnam( sys_get_temp_dir(), 'cover' );
			// Save the image

			$new_image = $edit_image->save( $temp_name );
			if ( ! is_wp_error( $new_image ) ) {
				$_image = array(
					'name'     => $new_image_path,
					'type'     => get_post_mime_type( $attachment_id ),
					'tmp_name' => $new_image['path'],
					'error'    => 0,
					'size'     => filesize( $new_image['path'] ),
				);

				$result = wp_handle_sideload( $_image, array( 'test_form' => false ) );
				if ( ! isset( $result['error'] ) ) {
					// Get the path to the upload directory.
					// Prepare an array of post data for the attachment.
					$new_attachment = array(
						'guid'           => basename( $result['url'] ),
						'post_mime_type' => $result['type'],
						'post_title'     => get_the_title( $attachment_id ),
						'post_content'   => '',
						'post_status'    => 'inherit',
						'post_author'    => $event->user_id,
					);

					// Insert the attachment.
					$new_attach_id = wp_insert_attachment( $new_attachment, $result['file'] );

					// Make sure that this file is included
					require_once( ABSPATH . 'wp-admin/includes/image.php' );
					// Generate the metadata for the attachment, and update the database record.
					$new_attach_data = wp_generate_attachment_metadata( $new_attach_id, $result['file'] );
					wp_update_attachment_metadata( $new_attach_id, $new_attach_data );

					$event_cover_src = wp_get_attachment_image_src( $event->cover_id, 'full' );

					$cover = array(
						'imageId' => $new_attach_id,
						'imageURL' => $event_cover_src[0],
						'fullImageW' => $event_cover_src[1],
						'fullImageH' => $event_cover_src[2],
					);

					$r->add_payload( 'cover', $cover );
				} else {
					# @unlink( trailingslashit( sys_get_temp_dir() ) . $temp_name );

					throw new Humanreadable_Exception( 'Failed to save cropped image. Error message: ' . $result['error'] );
				}
			}
		} elseif ( is_wp_error( $edit_image ) ) {
			throw new Humanreadable_Exception( 'Could not crop image. Error: ' . $edit_image->get_error_message() );
		} else {
			throw new Humanreadable_Exception( 'An unknown error ocurred while trying to crop the image. Please try again or Contact us if the issue persists.' );
		}
	} catch (Exception $e) {
		$r->fail( 'Failed to set cover.' . $e->getMessage() );
	}

	$r->respond();

}
add_action( 'wp_ajax_sc_proposal_crop_cover', 'sc_proposal_crop_cover_image', 10 );

function sc_proposal_versioning() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$event_id = intval( $request->input( 'event_id' ) );

	try {
		$event = Event::where( array(
			'id' => $event_id,
		))->with( array(
			'arrangements' => function($query){
				$query->orderBy( 'order', 'asc' );
			},
			'arrangements.recipes',
			'arrangements.items',
			'arrangements.items.item_variation',
			'arrangements.items.item_variation.item',
		) )->first();
	} catch (Exception $e) {
		$r->fail( 'This event doesn\'t exist.' );
	}

	$last_versioned_event = Event_Version::where(array(
		'event_id' => $event_id,
	))->orderBy('id', 'desc')->first();

	$versioned_event_data = array();

	$user_id = $event->user_id;
	$current_user = get_userdata( $user_id );
	$company_info = get_company_info( $user_id );

	$event->arrangements->sortBy( function($arrangement){
		return $arrangement->order;
	} );

	$logo = '<h3>' . $company_info['company'] . '</h3>';
	if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
		$logo_id = sc_get_profile_logo_id( $user_id );
		$logo = wp_get_attachment_image( $logo_id, 'logo_large', false, array( 'class' => 'proposal-logo' ) );
	}

	if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
		$logo_id = sc_get_profile_logo_id( $user_id );
		$logo = wp_get_attachment_image_src( $logo_id, 'medium' );
		$company_info['logo_url'] = $logo[0];
	} else {
		$company_info['logo_url'] = false;
	}

	$additionalInfo = unserialize( $current_user->additionalInfo ); //Get User Details
	$company_info['phone'] = isset( $additionalInfo['companyMobile'] ) ? $additionalInfo['companyMobile'] : '';
	$company_info['email'] = isset( $current_user->user_email ) ? $current_user->user_email : '';

	// include event details from above function if needeed.
	$proposal_arrangements = array();
	$fixed_prices_total = 0;
	$arrangement_subtotals = array();

	foreach ($event->arrangements as $arrangement) {
		$arrangement_photos = Arrangement_Photo::where('arrangement_id', '=', $arrangement->id)->get();
		$photos = array();
		foreach ($arrangement_photos as $photo) {
			/* if the image exists */
			$image = wp_get_attachment_image_src($photo->photo_id);
			if (!empty($image)) {
				$image_id = $photo->photo_id;
				$image_url = wp_get_attachment_image_src($image_id, 'medium');
				$image_url = $image_url[0];
				$full_image = wp_get_attachment_image_src($image_id, 'full');

				$photos[] = array(
					'image_id' => $image_id,
					'image_url' => $image_url,
					'full_image_src' => $full_image[0],
					'full_image_w' => $full_image[1],
					'full_image_h' => $full_image[2],
				);
			} else {
				/* delete it from the arrangement_photos if it doesn't */
				Arrangement_Photo::where(array(
					'photo_id' => $photo->photo_id
				))->delete();
			}
		}

		$items = array();
		$recipes = array();

		foreach ( $arrangement->recipes as $arrangement_recipe ) {
			$recipes[] = array(
				'id' => $arrangement_recipe->id,
				'name' => $arrangement_recipe->name,
			);
		}

		foreach ( $arrangement->items as $arrangement_item ) {
			$cost_and_markup = $arrangement_item->cost + $arrangement_item->get_markup( false );

			$vi = $arrangement_item->toArray();
			$vi['item_type'] = $arrangement_item->item->type;
			$vi['item_variation_id'] = $arrangement_item->item_variation->id;
			$vi['item_variation_name'] = 'Default' != $arrangement_item->item_variation->name ? ' - ' . $arrangement_item->item_variation->name : '';

			$item_attachment = sc_get_variation_attachment( $arrangement_item->item_variation, $arrangement_item->item->default_variation );

			$key = $arrangement_item->item_id . '_' . $vi['item_variation_id'];

			$type = sc_get_item_type( $arrangement_item->item, true );

			$items[] = array(
				'id'                        => $arrangement_item->id,
				'name'                      => $arrangement_item->get_full_name(),
				'quantity'                  => $arrangement_item->quantity,
				'recipe_id'					=> $arrangement_item->arrangement_recipe_id,
				'invoicing_multiplier'      => $arrangement_item->get_invoicing_multiplier(),
				'cost'                      => sc_format_price( $arrangement_item->cost ),
				'cost_and_markup'           => $cost_and_markup,
				'cost_and_markup_and_labor' => $cost_and_markup + $arrangement_item->get_labor( false ),
				'subtotal'                  => $arrangement_item->get_calculated_price(),
				'type'                      => $type,
				'type_raw'                  => str_replace( ' ', '_', strtolower( $type ) ),
				'item_type'                 => $vi['item_type'],
				'item_id'                   => $vi['item_id'],
				'item_variation_id'         => $vi['item_variation_id'],
				'item_variation_name'       => ! empty( $vi['item_variation_name'] ) ? $vi['item_variation_name'] : '',
				'item_pricing_category'     => $arrangement_item->item->pricing_category,
				'itemKeypair'               => $key,
				'attachment'				=> $item_attachment,
			);
		}
		usort( $items, 'sc_recipe_list_sort' );

		// error_log( 'array_push($proposal_arrangements: ' . $arrangement->name );

		array_push($proposal_arrangements, array(
			'id'                    => $arrangement->id,
			'name'                  => $arrangement->name,
			'quantity'              => $arrangement->quantity,
			'order'                 => $arrangement->order,
			'note'                  => $arrangement->note,
			'include'               => $arrangement->include,
			'tax'                   => $arrangement->tax,
			'addon'                 => $arrangement->addon,
			'total'                 => $arrangement->get_final_price(false),
			'override_cost'         => $arrangement->override_cost,
			'subTotal'              => $arrangement->get_final_price(),
			'photos'                => $photos,
			'items'                 => $items,
			'invoicing_category_id' => $arrangement->invoicing_category_id,
			'hardgood_multiple'     => $arrangement->hardgood_multiple,
			'fresh_flower_multiple' => $arrangement->fresh_flower_multiple,
			'global_labor_value'    => $arrangement->global_labor_value,
			'flower_labor'          => $arrangement->flower_labor,
			'hardgood_labor'        => $arrangement->hardgood_labor,
			'base_price_labor'      => $arrangement->base_price_labor,
			'fee_labor'             => $arrangement->fee_labor,
			'rental_labor'          => $arrangement->rental_labor,
			'recipes'				=> $recipes,
			'is_percentage'			=> absint( $arrangement->is_percentage ),
			'is_applied_all'		=> absint( $arrangement->is_applied_all ),
			'applies_on'			=> $arrangement->applies_on,
		));

	}

	$date_format = get_user_meta( $user_id, '__date_format', true );
	$js_date_format = sc_get_user_js_date_format( $user_id );

	$tax_rates = Tax_Rate::where( array(
		'user_id' => $user_id
	) )->get()->toArray();

	$event_cost = array(
		'subtotal'			=> $event->get_subtotal( true ),
		'tax'				=> $event->get_tax( true ),
		'tax2'				=> $event->get_tax2( true ),
		'tax_rate_id'		=> $event->tax_rate_id,
		'tax_rate_value'	=> $event->tax_rate_value,
		'tax_rate_id2'		=> $event->tax_rate_id2,
		'tax_rate_value2'	=> $event->tax_rate_value2,
		'total'				=> $event->get_total( true ),
		'tax_rates'			=> $tax_rates,
	);

	$customers = sc_get_event_customers( $event->id, $event->user_id );

	if ( $event->cover_id ) {
		$event_cover_src = wp_get_attachment_image_src( $event->cover_id, 'full' );
		$event_cover = array(
			'imageId' => $event->cover_id,
			'imageURL' => $event_cover_src[0],
			'fullImageW' => $event_cover_src[1],
			'fullImageH' => $event_cover_src[2],
		);
	} else {
		$event_cover = array(
			'imageId' => '',
			'imageURL' => '',
			'fullImageW' => '',
			'fullImageH' => '',
		);
	}

	$event_vendors = array();
	$event_vendors_info = sc_get_user_event_vendors($event->id);
	$event_vendors['venue'] = $event_vendors_info['event_vendors']['venue'];

	$_event = $event->toArray();

	$_event['date']			= date( $date_format, strtotime( $event->date ) );
	$_event['date_created']	= date( $date_format, strtotime( $event->created_at ) );
	$_event['venues']		= $event_vendors['venue'];
	$_event['arrangements']	= $proposal_arrangements;
	$_event['cover']	  	= $event_cover;

	$attachment_ids = Meta::where( array(
		'type' => 'event',
		'type_id' => $event->id,
		'meta_key' => 'attachments',
	))->pluck( 'meta_value' );

	$_event['attachments'] = array();
	foreach ( $attachment_ids as $id ) { //If you edit this, change it on the other places (versionings) too
		$_event['attachments'][] = array (
			'id' => $id,
			'title' => get_the_title( $id ),
			'url' => wp_get_attachment_url( $id ),
			'icon' => wp_get_attachment_image_url( $id, 'thumbnail', true ),
		);
	}

	$payments = array();
	$_payments = Payment::where(array(
		'user_id' => $user_id,
		'event_id' => $event->id,
		'is_live' => 0,
	))->get()->all();

	// Delete existing live payments
	Payment::where(array(
		'user_id' => $user_id,
		'event_id' => $event->id,
		'is_live' => 1,
	))->delete();

	foreach ( $_payments as $payment ) {
		// Save a copy of each draft payment as a live payment with a fixed amount
		$live_payment = $payment->replicate( array( 'created_at', 'updated_at', 'id' ) );
		$live_payment->is_live = 1;
		//$live_payment->amount = ! is_null( $payment->amount ) ? sc_format_price( $payment->amount ) : $payment->get_amount();
		$live_payment->amount = $payment->get_amount();
		$live_payment->convenience_fee = $payment->get_amount() * ( $payment->card_fee / 100 );
		$live_payment->payment_percentage = null;
		$live_payment->save();

		array_push( $payments, array(
			'paymentId'			=> $payment->id,
			'paymentName'		=> $payment->payment_name,
			'dueDate'			=> sc_get_user_date_time( $user_id, $payment->due_date ),
			'dueDateRaw'		=> $payment->due_date,
			'cardFee'			=> sc_format_price( $payment->card_fee ),
			'convenienceFee'	=> ( 'complete' == $payment->status ) ? $payment->convenience_fee : $payment->get_amount() * ( $payment->card_fee / 100 ),
			'amount'			=> ! is_null( $payment->amount ) ? sc_format_price( $payment->amount ) : null,
			'payment_date'		=> is_null( $payment->payment_date ) ? '' : sc_get_user_date_time( $user_id, $payment->payment_date ),
			'amountPercentage'	=> is_null( $payment->payment_percentage ) ? null : sc_format_price( $payment->payment_percentage ),
			'amountType'		=> ! is_null( $payment->payment_percentage ) && 'complete' != $payment->status ? 'percentage' : 'currency',
			'paymentType'		=> $payment->payment_type,
			'note'				=> $payment->payment_note,
			'status'			=> $payment->status,
		) );
	}

	$versioned_event_data = array(
		'event' 			=> $_event,
		'eventDate'			=> date( $date_format, strtotime( $event->date ) ),
		'company_info'		=> $company_info,
		'customers'			=> $customers,
		'eventCost'			=> $event_cost,
		'eventNote'			=> wpautop( $event->event_note ),
		'eventContractNote' => wpautop( $event->contract_note ),
		'currencySymbol'	=> sc_get_user_saved_pref_currency( $user_id ),
		'dateFormat'		=> $js_date_format,
		'payments'			=> $payments,
	);

	$record_version = 1;

	if ( ! empty( $last_versioned_event ) && $record_version <= intval( $last_versioned_event->version ) ) {
		$record_version = $last_versioned_event->version + 1;
	}

	$signature_options = sc_get_event_signatures( $user_id, $event->id );
	// Drop signature data - it's saved separately
	foreach ( $signature_options['all'] as $i => $sign_opt ) {
		unset( $signature_options['all'][ $i ]['signature'] );
	}
	$versioned_event_data['signatures'] = $signature_options;

	$versioned_event_data = apply_filters( 'sc/event/version/create/data', $versioned_event_data, $event );

	try {
		$new_version = new Event_Version;
		$new_version->event_id = $event_id;
		$new_version->version = $record_version;
		$new_version->event_data = json_encode( $versioned_event_data );
		$new_version->save();

	} catch (Exception $e) {
		$r->fail( 'Failed to create record.' );
	}

	$versions = array();
	$_versions = Event_Version::where( array(
		'event_id' => $event_id,
	))->orderBy( 'id', 'desc' )->get();

	foreach ( $_versions as $version ) {
		array_push( $versions, array(
			'version_id'	=> $version->id,
			'version_no'	=> $version->version,
			'created_at'	=> date( $date_format, strtotime( $version->created_at ) ),
			'signed'  		=> (bool) $version->client_signed,
		));
	}

	$r->add_payload( 'versions', $versions );

	$r->respond();

}
add_action( 'wp_ajax_sc_proposal_versioning', 'sc_proposal_versioning', 10 );

function sc_change_event_version() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();
	$event_id = intval( $request->input('event_id') );
	$event_data = array();
	$version = false;

	try{
		$event = Event::where( array(
			'id' => $event_id
		))->with( array(
			'arrangements' => function($query){
				$query->orderBy( 'order', 'asc' );
			},
			'arrangements.recipes',
			'arrangements.items',
			'arrangements.items.item_variation',
			'arrangements.items.item_variation.item',
			'payments',
		) )->first();

	} catch ( Exception $r ) {
		$r->fail( 'This event doesn\'t exist.' );
	}

	if ( -1 !== intval( $request->input('version_id') ) ) {
		// get event JSON data if it's not version # draft
		try {
			$version = Event_Version::where(array(
				'id' => intval( $request->input('version_id') )
			))->firstOrFail();

		} catch ( Exception $r ) {
			$r->fail( 'This event version doesn\'t exist.' );
		}

		$user_id = $event->user_id;
		$event_data = json_decode( $version->event_data, true );

		if ( empty( $event_data['eventCost']['tax_rates'] ) ) {
			$tax_rates = Tax_Rate::where( array(
				'user_id' => $user_id
			) )->get()->toArray();

			$event_data['eventCost']['tax_rates'] = $tax_rates;
		}
		$payments = array();

		if ( ! intval( $request->input('latest_version') ) && array_key_exists( 'scheduledPayments', $event_data ) ) {
			foreach ( $event_data['scheduledPayments'] as $key => $payment ) {
				$amount = null;
				$amount_percent = null;

				if ( 'currency' == $payment['amountType'] ) {
					$amount = sc_format_price( $payment['amount'] );
				} else {
					$amount = sc_format_price( $event_data['eventCost']['total'] * sc_format_price( $payment['amount'] / 100 ) );
				}

				$payments[] = array(
					'paymentId'				=> $payment['paymentId'],
					'paymentName'			=> $payment['paymentName'],
					'dueDate'				=> $payment['date'],
					'dueDateRaw'			=> $payment['date'],
					'cardFee'				=> $payment['cardFee'],
					'convenienceFee'		=> $payment['convenienceFee'],
					'amount'				=> $amount,
					'payment_date'			=> null,
					'amountPercentage'		=> $amount_percent,
					'amountType'			=> $payment['amountType'],
					'paymentType'			=> '',
					'note'					=> '',
					'status'				=> 'not-paid',
				);
			}
		} elseif ( intval( $request->input('latest_version') ) ) {
			$_payments = Payment::where(array(
				'user_id' => $user_id,
				'event_id' => $event->id,
				'is_live' => 1,
			))->get()->all();

			foreach ( $_payments as $payment ) {
				array_push($payments, array(
					'paymentId'			=> $payment->id,
					'paymentName'		=> $payment->payment_name,
					'dueDate'			=> sc_get_user_date_time( $user_id, $payment->due_date ),
					'dueDateRaw'		=> $payment->due_date,
					'cardFee'			=> $payment->card_fee,
					'convenienceFee'	=> sc_format_price( $payment->convenience_fee ),
					'amount'			=> ! is_null( $payment->amount ) ? sc_format_price( $payment->amount ) : null,
					'payment_date'		=> is_null( $payment->payment_date ) ? '' : sc_get_user_date_time( $user_id, $payment->payment_date ),
					'amountPercentage'	=> is_null( $payment->payment_percentage ) ? null : sc_format_price( $payment->payment_percentage ),
					'amountType'		=> ! is_null( $payment->payment_percentage ) && 'complete' != $payment->status ? 'percentage' : 'currency',
					'paymentType'		=> $payment->payment_type,
					'note'				=> $payment->payment_note,
					'status'			=> $payment->status,
				));
			}
		} else {
			$payments = $event_data['payments'];
		}

		$signatures = ! empty( $event_data['signatures'] ) ? sc_get_event_signatures( $event->user_id, $event_id, $version->id, sc_object_to_array( $event_data['signatures'] ) ) : sc_get_event_signatures( $event->user_id, $event_id, $version->id );

		$event_data['payments'] = $payments;
		$event_data['signatures'] = $signatures;
		$event_data['customers'] = isset( $event_data['customers'] ) ? $event_data['customers'] : (array) $event_data['customer'];
		// Convert single customer to multiple customers
		if ( isset( $event_data['customers']['id'] ) || isset( $event_data['customers']['name'] ) ) {
			$event_data['customers'] = array( $event_data['customers'] );
		}

	} else {
		// Loading the Workroom View
		$user_id = $event->user_id;

		$company_info = get_company_info();

		$current_user = get_userdata( $user_id );

		$logo = '<h3>' . $company_info['company'] . '</h3>';
		if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
			$logo_id = sc_get_profile_logo_id( $user_id );
			$logo = wp_get_attachment_image( $logo_id, 'logo_large', false, array( 'class' => 'proposal-logo' ) );
		}

		$invoice_categories_list = Invoicing_Category::where(array(
			'user_id' => $user_id
		))->get()->toArray();
		$item_types = sc_get_mixed_arrangement_item_types();

		$values = array(
			'hardgood_multiple' => $event->hardgood_multiple,
			'fresh_flower_multiple' => $event->fresh_flower_multiple,
			'labor' => array(
				'global_labor_value' => $event->global_labor_value,
				'flower' => $event->flower_labor,
				'hardgood' => $event->hardgood_labor,
				'base_price' => $event->base_price_labor,
				'fee' => $event->fee_labor,
				'rental' => $event->rental_labor
			)
		);

		$name = 'test';
		try {
			$event_invoicing_category = Invoicing_Category::where(array(
				'id' => $event->invoicing_category_id,
				'user_id' => $user_id
			))->firstOrFail();
			$selected_category_id = $event_invoicing_category->id;

			foreach ($invoice_categories_list as $i => $value) {
				if ($value['id'] == $selected_category_id) {
					$invoice_categories_list[$i]["hardgood_multiple"] = $event->hardgood_multiple;
				    $invoice_categories_list[$i]["fresh_flower_multiple"] = $event->fresh_flower_multiple;
				    $invoice_categories_list[$i]["global_labor_value"] = $event->global_labor_value;
				    $invoice_categories_list[$i]["flower_labor"] = $event->flower_labor;
				    $invoice_categories_list[$i]["hardgood_labor"] = $event->hardgood_labor;
				    $invoice_categories_list[$i]["base_price_labor"] = $event->base_price_labor;
				    $invoice_categories_list[$i]["fee_labor"] = $event->fee_labor;
				    $invoice_categories_list[$i]["rental_labor"] = $event->rental_labor;
					break;
				}
			}
		} catch (Exception $e) {
			$name = 'Deleted Markup Profile';
			array_unshift($invoice_categories_list, array(
				"id" => $event->invoicing_category_id,
			    "user_id" => $user_id,
			    "name" => $name,
			    "hardgood_multiple" => $event->hardgood_multiple,
			    "fresh_flower_multiple" => $event->fresh_flower_multiple,
			    "global_labor_value" => $event->global_labor_value,
			    "flower_labor" => $event->flower_labor,
			    "hardgood_labor" => $event->hardgood_labor,
			    "base_price_labor" => $event->base_price_labor,
			    "fee_labor" => $event->fee_labor,
			    "rental_labor" => $event->rental_labor
			));
			$selected_category_id = $event->invoicing_category_id;
		}

		$company_info = get_company_info( $user_id );
		if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
			$logo_id = sc_get_profile_logo_id( $user_id );
			$logo = wp_get_attachment_image_src( $logo_id, 'medium' );
			$company_info['logo_url'] = $logo[0];
		} else {
			$company_info['logo_url'] = false;
		}

		$additionalInfo = unserialize( $current_user->additionalInfo ); //Get User Details
		$company_info['phone'] = isset( $additionalInfo['companyMobile'] ) ? $additionalInfo['companyMobile'] : '';
		$company_info['email'] = isset( $current_user->user_email ) ? $current_user->user_email : '';

		// include event details from above function if needeed.
		$proposal_arrangements = array();
		$fixed_prices_addon = 0;
		$fixed_prices_live = 0;
		$arrangement_subtotals = array();

		foreach ($event->arrangements as $arrangement) {
			$arrangement_photos = Arrangement_Photo::where('arrangement_id', '=', $arrangement->id)->get();
			$photos = array();
			foreach ($arrangement_photos as $photo) {
				/* if the image exists */
				$image = wp_get_attachment_image_src($photo->photo_id);
				if (!empty($image)) {
					$image_id = $photo->photo_id;
					$image_url = wp_get_attachment_image_src($image_id, 'medium');
					$image_url = $image_url[0];
					$full_image = wp_get_attachment_image_src($image_id, 'full');

					$photos[] = array(
						'image_id' => $image_id,
						'image_url' => $image_url,
						'full_image_src' => $full_image[0],
						'full_image_w' => $full_image[1],
						'full_image_h' => $full_image[2],
					);
				} else {
					/* delete it from the arrangement_photos if it doesn't */
					Arrangement_Photo::where(array(
						'photo_id' => $photo->photo_id
					))->delete();
				}
			}

			$recipes = array();
			$items = array();

			foreach ( $arrangement->recipes as $arrangement_recipe ) {
				$recipes[] = array(
					'id' => $arrangement_recipe->id,
					'name' => $arrangement_recipe->name,
				);
			}

			foreach ( $arrangement->items as $arrangement_item ) {
				$cost_and_markup = $arrangement_item->cost + $arrangement_item->get_markup( false );

				$vi = $arrangement_item->toArray();
				$vi['item_type'] = $arrangement_item->item->type;
				$vi['item_variation_name'] = 'Default' != $arrangement_item->item_variation->name ? ' - ' . $arrangement_item->item_variation->name : '';
				$vi['item_variation_id'] = $arrangement_item->item_variation->id;

				$item_attachment = sc_get_variation_attachment( $arrangement_item->item_variation, $arrangement_item->item->default_variation );

				$key = $arrangement_item->item_id . '_' . $vi['item_variation_id'];

				$type = sc_get_item_type( $arrangement_item->item, true );

				$items[] = array(
					'id'                        => $arrangement_item->id,
					'name'                      => $arrangement_item->get_full_name(),
					'quantity'                  => $arrangement_item->quantity,
					'invoicing_multiplier'      => $arrangement_item->get_invoicing_multiplier(),
					'cost'                      => sc_format_price( $arrangement_item->cost ),
					'cost_and_markup'           => $cost_and_markup,
					'cost_and_markup_and_labor' => $cost_and_markup + $arrangement_item->get_labor( false ),
					'subtotal'                  => $arrangement_item->get_calculated_price(),
					'type'                      => $type,
					'type_raw'                  => str_replace( ' ', '_', strtolower( $type ) ),
					'item_type'                 => $vi['item_type'],
					'item_id'                   => $arrangement_item->item->id,
					'item_variation_id'         => $vi['item_variation_id'],
					'item_variation_name'       => $vi['item_variation_name'],
					'item_pricing_category'     => $arrangement_item->item->pricing_category,
					'itemKeypair'               => $key,
					'recipe_id'					=> $arrangement_item->arrangement_recipe_id,
					'attachment'				=> $item_attachment,
				);
			}
			usort( $items, 'sc_recipe_list_sort' );

			array_push($proposal_arrangements, array(
				'id'                    => $arrangement->id,
				'name'                  => $arrangement->name,
				'quantity'              => $arrangement->quantity,
				'order'                 => $arrangement->order,
				'note'                  => $arrangement->note,
				'include'               => $arrangement->include,
				'tax'                   => $arrangement->tax,
				'addon'                 => $arrangement->addon,
				'total'                 => $arrangement->get_final_price(false),
				'override_cost'         => $arrangement->override_cost,
				'subTotal'              => $arrangement->get_final_price(),
				'photos'                => $photos,
				'items'                 => $items,
				'invoicing_category_id' => $arrangement->invoicing_category_id,
				'hardgood_multiple'     => $arrangement->hardgood_multiple,
				'fresh_flower_multiple' => $arrangement->fresh_flower_multiple,
				'global_labor_value'    => $arrangement->global_labor_value,
				'flower_labor'          => $arrangement->flower_labor,
				'hardgood_labor'        => $arrangement->hardgood_labor,
				'base_price_labor'      => $arrangement->base_price_labor,
				'fee_labor'             => $arrangement->fee_labor,
				'rental_labor'          => $arrangement->rental_labor,
				'recipes'				=> $recipes,
				'is_percentage'			=> absint( $arrangement->is_percentage ),
				'is_applied_all'		=> absint( $arrangement->is_applied_all ),
				'applies_on'			=> $arrangement->applies_on,
			));

		}

		$date_format = get_user_meta( $user_id, '__date_format', true );
		$js_date_format = sc_get_user_js_date_format( $user_id );

		$tax_rates = Tax_Rate::where( array(
			'user_id' => $user_id
		) )->get()->toArray();

		$tax_rate_ids = array();
		foreach ($tax_rates as $tax_rate) {
			/** The value of the tax rate was modified */
			if ( $tax_rate['id'] == $event->tax_rate_id &&
				$tax_rate['value'] != $event->tax_rate_value ) {
				array_push( $tax_rates, array(
					'id' => -$tax_rate['id'],
					'user_id' => $user_id,
					'name' => 'Modified Tax Rate',
					'value' => $event->tax_rate_value
				) );
			}

			if ( $tax_rate['id'] == $event->tax_rate_id2 &&
				$tax_rate['value'] != $event->tax_rate_value2 ) {
				array_push( $tax_rates, array(
					'id' => -$tax_rate['id'],
					'user_id' => $user_id,
					'name' => 'Modified Tax Rate 2',
					'value' => $event->tax_rate_value2
				) );
			}

			array_push( $tax_rate_ids, $tax_rate['id'] );
		}

		if ( (
				! in_array( $event->tax_rate_id, $tax_rate_ids ) &&
				! empty( $tax_rate_ids ) &&
				! is_null($event->tax_rate_id ) &&
				$event->tax_rate_id != 0
			) || (
				empty( $tax_rate_ids ) && $event->tax_rate_id != 0
			) ) {
			array_push($tax_rates, array(
				'id' => -$event->tax_rate_id,
				'user_id' => $user_id,
				'name' => 'Deleted Tax Rate',
				'value' => $event->tax_rate_value
			));
		}

		if ( (
				! in_array( $event->tax_rate_id2, $tax_rate_ids ) &&
				! empty( $tax_rate_ids ) &&
				! is_null($event->tax_rate_id2 ) &&
				$event->tax_rate_id2 != 0
			) || (
				empty( $tax_rate_ids ) && $event->tax_rate_id2 != 0
			) ) {
			array_push($tax_rates, array(
				'id' => -$event->tax_rate_id2,
				'user_id' => $user_id,
				'name' => 'Deleted Tax Rate',
				'value' => $event->tax_rate_value2
			));
		}

		$event_cost = array(
			'subtotal'		=> $event->get_subtotal( true ),
			'tax_rate_id'   => $event->tax_rate_id,
			'tax_rate_value'=> $event->tax_rate_value,
			'tax_rate_id2'  => $event->tax_rate_id2,
			'tax_rate_value2'=> $event->tax_rate_value2,
			'tax' 			=> $event->get_tax( true ),
			'tax2' 			=> $event->get_tax2( true ),
			'total' 		=> $event->get_total( true ),
			'tax_rates' 	=> $tax_rates,
		);

		$event_vendors = array();
		$event_vendors_info = sc_get_user_event_vendors( $event->id, $user_id );
		$event_vendors['venue'] = $event_vendors_info['event_vendors']['venue'];
		$venues_list = $event_vendors_info['venues_list'];

		$customers = sc_get_event_customers( $event->id, $event->user_id );

		if ( $event->cover_id ) {
			$event_cover_src = wp_get_attachment_image_src( $event->cover_id, 'full' );
			$event_cover = array(
				'imageId' => $event->cover_id,
				'imageURL' => $event_cover_src[0],
				'fullImageW' => $event_cover_src[1],
				'fullImageH' => $event_cover_src[2],
			);
		} else {
			$event_cover = array(
				'imageId' => '',
				'imageURL' => '',
				'fullImageW' => '',
				'fullImageH' => '',
			);
		}

		$proposal_covers = array();
		$post_attach_args = array(
			'post_parent'		=> sc_get_id_by_template( 'template-proposal-2.0.php' ),
			'post_type'			=> 'attachment',
			'post_mime_type'	=> 'image',
			'orderby'			=> 'menu_order',
			'order'				=> 'ASC',
		);

		$post_covers = get_posts( $post_attach_args );
		if ( $post_covers ) {
			foreach ( $post_covers as $cover ) {
				$fullcover = wp_get_attachment_image_src( $cover->ID, 'full' );
				$proposal_covers[] = array(
					'imageId' => $cover->ID,
					'imageURL' => $fullcover[0],
					'fullImageW' => $fullcover[1],
					'fullImageH' => $fullcover[2],
					'imageTitle' => get_the_title( $cover->ID ),
				);
			}
		}

		$_event = array(
			'id'          		=> $event->id,
			'name'        		=> $event->name,
			'card'       		=> $event->card,
			'hide_item_prices'	=> $event->hide_item_prices,
			'separate_cc_fee' 	=> $event->separate_cc_fee,
			'visible_payments'	=> $event->visible_payments,
			'hidden_contract'	=> $event->hidden_contract,
			'date'       		=> date( $date_format, strtotime( $event->date ) ),
			'date_created'		=> date( $date_format, strtotime( $event->created_at ) ),
			'venues'      		=> $event_vendors['venue'],
			'arrangements'		=> $proposal_arrangements,
			'cover'		  		=> $event_cover,
			'default_covers' 	=> $proposal_covers,
		);

		$signatures = sc_get_event_signatures( $user_id, $event->id );

		$payments = array();
		$_payments = Payment::where(array(
			'user_id' => $user_id,
			'event_id' => $event->id,
			'is_live' => 0,
		))->get()->all();

		foreach ( $_payments as $payment ) {
			$payments[] = array(
				'paymentId'			=> $payment->id,
				'paymentName'		=> $payment->payment_name,
				'dueDate'			=> sc_get_user_date_time( $user_id, $payment->due_date ),
				'dueDateRaw'		=> $payment->due_date,
				'cardFee'			=> sc_format_price( $payment->card_fee ),
				'convenienceFee'	=> ( 'complete' == $payment->status ) ? $payment->convenience_fee : $payment->get_amount() * ( $payment->card_fee / 100 ),
				'amount'			=> ! is_null( $payment->amount ) ? sc_format_price( $payment->amount ) : null,
				'payment_date'		=> is_null( $payment->payment_date ) ? '' : sc_get_user_date_time( $user_id, $payment->payment_date ),
				'amountPercentage'	=> is_null( $payment->payment_percentage ) ? null : sc_format_price( $payment->payment_percentage ),
				'amountType'		=> ! is_null( $payment->payment_percentage ) && 'complete' != $payment->status ? 'percentage' : 'currency',
				'paymentType'		=> $payment->payment_type,
				'note'				=> $payment->payment_note,
				'status'			=> $payment->status,
			);
		}

		$live_payments = array();
		$_live_payments = Payment::where(array(
			'user_id' => $user_id,
			'event_id' => $event->id,
			'is_live' => 1,
		))->get()->all();

		usort( $_live_payments, function( $a, $b ) {
			if ( $a->due_date == $b->due_date ) {
				return $a->id < $b->id ? -1 : 1;
			}
			return ( strtotime( $a->due_date ) < strtotime( $b->due_date ) ) ? -1 : 1;
		} );

		foreach ( $_live_payments as $payment ) {
			$live_payments[] = array(
				'paymentId'			=> $payment->id,
				'paymentName'		=> $payment->payment_name,
				'dueDate'			=> sc_get_user_date_time( $user_id, $payment->due_date ),
				'dueDateRaw'		=> $payment->due_date,
				'amount'			=> ! is_null( $payment->amount ) ? sc_format_price( $payment->amount ) : null,
				'payment_date'		=> is_null( $payment->payment_date ) ? '' : sc_get_user_date_time( $user_id, $payment->payment_date ),
				'amountPercentage'	=> is_null( $payment->payment_percentage ) ? null : sc_format_price( $payment->payment_percentage ),
				'amountType'		=> ! is_null( $payment->payment_percentage ) && 'complete' != $payment->status ? 'percentage' : 'currency',
				'paymentType'		=> $payment->payment_type,
				'note'				=> $payment->payment_note,
				'status'			=> $payment->status,
			);
		}

		$attachment_ids = Meta::where( array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'attachments',
		))->pluck( 'meta_value' );

		$_event['attachments'] = array();
		foreach ( $attachment_ids as $id ) { //If you edit this, change it on the other places (versionings) too
			$_event['attachments'][] = array (
				'id' => $id,
				'title' => get_the_title( $id ),
				'url' => wp_get_attachment_url( $id ),
				'icon' => wp_get_attachment_image_url( $id, 'thumbnail', true ),
			);
		}

		$event_data = array(
			'event'				=> $_event,
			'eventDate'			=> date( $date_format, strtotime( $event->date ) ),
			'dateFormat'		=> $js_date_format,
			'customers'			=> $customers,
			'company_info'		=> $company_info,
			'eventCost'			=> $event_cost,
			'eventNote'			=> wpautop( $event->event_note ),
			'eventContractNote' => wpautop( $event->contract_note ),
			'currencySymbol'	=> sc_get_user_saved_pref_currency( $user_id ),
			'signatures'		=> $signatures,
			'payments'			=> $payments,
			'livePayments'		=> $live_payments,
		);

		$event_data = apply_filters( 'sc/react/data/proposal', $event_data, $event, $user_id, $version );
	}

	$r->add_payload( 'version', $event_data );

	$r->respond();
}
add_action( 'wp_ajax_sc_change_event_version', 'sc_change_event_version', 10 );

function sc_get_event_signatures( $user_id, $event_id, $version_id = false, $signatures = array() ) {
	if ( ! $signatures && $version_id ) {
		return false;
	}

	if ( ! $signatures ) {
		$signatures = $signatures ? $signatures : Meta::where(array(
			'type' => 'event',
			'type_id' => $event_id,
			'meta_key' => 'signature_opts',
		))->first();

		if ( ! $signatures ) {
			$signatures = array(
				'last_index'   => 1,
				'all'          => array(
					array(
						'key'        => 'sigopt_0',
						'type'       => 'customer',
						'label'      => 'Client Signature',
						'signature'  => false,
					),
				),
			);
		} else {
			$signatures = json_decode( $signatures->meta_value, true );
		}

		$customers = sc_get_event_customers( $event_id, $user_id );
		$customer_signatures = wp_list_filter( $signatures['all'], array( 'type' => 'customer' ) );
		if ( count( $customers ) > count( $customer_signatures ) ) {
			for ( $i = max( count( $customer_signatures ), 1 ); $i < count( $customers ); $i ++ ) {
				$signatures['all'][] = array(
					'key'        => 'sigopt_' . $signatures['last_index'],
					'type'       => 'customer',
					'label'      => 'Client Signature',
					'signature'  => false,
				);
				$signatures['last_index'] ++;
			}
		}
	}

	if ( ! empty( $version_id ) ) {
		$recorded_signatures = Meta::where(array(
			'type' => 'event_version',
			'type_id' => $version_id,
			'meta_key' => 'signature',
		))->get();

		if ( $recorded_signatures ) {
			foreach ( $recorded_signatures as $_signature ) {
				$signature = json_decode( $_signature->meta_value, true );

				if ( $signature ) {
					foreach ( $signatures['all'] as $i => $data ) {
						if ( $signature['key'] == $data['key'] ) {
							$signatures['all'][ $i ]['signature'] = array(
								'key'  => $signature['key'],
								'type' => 'typed',
								'data' => $signature['data'],
								'date' => sprintf( __( 'Signed on %s', 'stemcounter' ), sc_get_user_date_time( $user_id, $signature['date'], true ) ),
							);

							break;
						}
					}
				}
			}
		}
	}

	return $signatures;
}

function sc_capture_event_version_signature() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try{
		$event = Event::where( array(
			'id' => intval( $request->input('eid') )
		))->firstOrFail();
	} catch ( Exception $e ) {
		$r->fail( 'Something went wrong and your signature was not saved.' );
	}

	try {
		$version = Event_Version::where(array(
			'event_id' => $event->id,
			'version'  => intval( $request->input('version') ),
		))->firstOrFail();

	} catch ( Exception $e ) {
		// Use slightly different messages without revealing the actual reason
		// for failure - this will make debugging easier in a case of an error
		$r->fail( 'Oh no! We were unable to save your signature!' );
	}

	$signature_data = array(
		'key'         => $request->input( 'key' ),
		'type'        => $request->input( 'type' ),
		'data'        => $request->input( 'data' ),
		'date'        => current_time( 'timestamp' ),
		'signer_info' => array(
			'remote_addr'   => $_SERVER['REMOTE_ADDR'],
			'browser_agent' => $_SERVER['HTTP_USER_AGENT'],
			// If the user is authenticated, make sure to store their ID
			'logged_in_as'  => is_user_logged_in() ? get_current_user_id() : false,
		),
	);

	$event_data = json_decode( $version->event_data );

	$signatures = sc_get_event_signatures( $event->user_id, $event->id, $version->id, sc_object_to_array( $event_data->signatures ) );

	if ( $signatures ) {
		$all_clients_signed = true;

		foreach ( $signatures['all'] as $i => $_signature ) {
			if ( $signature_data['key'] == $_signature['key'] ) {
				if ( ! $_signature['signature'] ) {
					try {
						$signature_meta = new Meta();
						$signature_meta->type = 'event_version';
						$signature_meta->type_id = $version->id;
						$signature_meta->meta_key = 'signature';
						$signature_meta->meta_value = json_encode( $signature_data );
						$signature_meta->save();

						//  Create notification about the event
						$notification = new Notification;
						$notification->user_id = $event->user_id;
						$notification->type = 'signature';
						$notification->read = 0;
						$notification->subject = 'Proposal Signed!';
						$notification->notification = 'Version ' . $version->version . ' of ' .'<a href="' . add_query_arg( 'eid', $event->id, sc_get_proposal_2_0_page_url() ) . '" target="_blank">' . $event->name . '</a>' .' has been signed.';
						// email notification
						$notification->email_sent = sc_email_notifications( $event->user_id, $notification->notification, $notification->type, $notification->subject );
						$notification->save();



						$r->add_payload( 'date', sprintf( __( 'Signed on %s', 'stemcounter' ), sc_get_user_date_time( $event->user_id, $signature_data['date'], true ) ) );
					} catch ( Exception $e ) {
						$r->fail( 'Something went wrong and we were unable to save your signature.' );
					}
				} else {
					$r->fail( 'It seems that you have already signed!' );
				}
			} else if ( ! $_signature['signature'] ) {
				$all_clients_signed = false;
			}
		}
	} else {
		$all_clients_signed = false;
	}

	// Update the version when all customers signed
	if ( $all_clients_signed && ! $version->client_signed ) {
		$version->client_signed = 1;
		$version->save();
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_do_sign_proposal', 'sc_capture_event_version_signature', 10 );
add_action( 'wp_ajax_nopriv_sc_do_sign_proposal', 'sc_capture_event_version_signature', 10 );

function sc_render_template( $template, $args, $return_output = false ) {
	$template = locate_template( array( $template . '.php' ) );
	if ( ! $template ) {
		return;
	}

	extract( $args, EXTR_SKIP );
	if ( $return_output ) {
		ob_start();
	}

	include( $template );

	if ( $return_output ) {
		return ob_get_clean();
	}
}

function sc_proposal_receipt() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try{
		$event = Event::where( array(
			'id' => intval( $request->input('event_id') )
		))->first();

		try{
			$receipt = Payment::where( array(
				'id' => intval( $request->input('receipt_id') )
			))->first();

			$current_user = get_userdata( $event->user_id );
			$company_info = get_company_info( $event->user_id );
			$company_info['email'] = isset( $current_user->user_email ) ? $current_user->user_email : '';
			$customer = sc_get_user_event_client( $event->id, true );

			$args = array(
				'company_info'	=> $company_info,
				'customer'		=> $customer,
				'receipt'		=> $receipt,
				'page_title'    => 'Payment Receipt',
				'event'         => $event,
			);

			add_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );

			if ( ! empty( $request->input('email') ) ) {
				$headers = array();
				$headers[] = 'From: "' . $company_info['company'] . '" <noreply@stemcounter.com>';
				$headers[] = 'Reply-To: "' . $company_info['company'] . '" <' . $company_info['email'] . '>';
				$headers[] = 'List-Unsubscribe: <' . home_url( '/app/wp-admin/admin-ajax.php?action=sc_email_unsubscribe' ) . '>';
				$to = sc_prepare_email_recipients_for_mandrill( $request->input( 'email' ) );
				$subject = 'Payment Receipt';
				$body .= sc_render_template( 'templates/html-mail-header', $args, true );
				$body .= sc_render_template( 'templates/template-receipts', $args, true );
				$body .= '<div class="page-link" style="display:block; text-align: center; margin-top:15px; "><a style="text-decoration:none; color: #f37a76;" href="' . home_url( '/view-proposal/' . sc_encrypted_string( $event->id ) . '/receipt/' . $receipt->id ) . '" target="_none" >View Receipt Online</a></div>';
				$body .= sc_render_template( 'templates/html-footer', $args, true );

				wp_mail( $to, $subject, $body, $headers );
				$r->add_payload( 'message', 'Receipt was sent.' );
			} else {
				$r->fail( 'Customer email is not set!' );
			}

			remove_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );
		} catch ( Exception $r ) {
			$r->fail( 'This payment doesn\'t exist.' );
		}
	} catch ( Exception $r ) {
		$r->fail( 'This event doesn\'t exist.' );
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_proposal_receipt', 'sc_proposal_receipt', 10 );

function sc_proposal_mail() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try{
		$event = Event::where( array(
			'id' => intval( $request->input('event_id') )
		))->first();

		$current_user = get_userdata( $event->user_id );
		$company_info = get_company_info( $event->user_id );
		$company_info['email'] = isset( $current_user->user_email ) ? $current_user->user_email : '';
		$customer = sc_get_user_event_client( $event->id, true );
		$proposalColor = get_user_meta( $event->user_id, 'proposal_color', true );
		$proposalColor = $proposalColor && '#' != $proposalColor ? $proposalColor : '#f59491';

		$logo = null;
		if ( sc_get_profile_logo_on_proposal( $event->user_id ) ) {
			$logo_id = sc_get_profile_logo_id( $event->user_id );
			$logo = wp_get_attachment_image_src( $logo_id, 'medium' );
		}

		add_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );

		if ( ! empty( $request->input('email') ) ) {
			$headers = array();
			$body = '';

			$headers[] = 'From: "' . $company_info['company'] . '" <noreply@stemcounter.com>';
			$headers[] = 'Reply-To: "' . $company_info['company'] . '" <' . $company_info['email'] . '>';
			$cc = false;
			if ( intval( $request->input( 'cc' ) ) && ! empty( $company_info['email'] ) ) {
				$headers[] = 'Cc: "' . $company_info['company'] . '" <' . $company_info['email'] . '>';
				$to = sc_prepare_email_recipients_for_mandrill( $request->input( 'email' ), $company_info['email'], $company_info['company'] );
			} else {
				$to = sc_prepare_email_recipients_for_mandrill( $request->input( 'email' ) );
			}
			$headers[] = 'List-Unsubscribe: <' . home_url( '/app/wp-admin/admin-ajax.php?action=sc_email_unsubscribe' ) . '>';
			$subject = trim( $request->input( 'subject' ) );
			$subject = $subject ? $subject : 'Proposal for ' . $event->name . '';
			$body .= '<html>';
				$body .= '<div class="wrapper" style="background-color: #f2f2f2; padding: 80px 0 80px 0;">';
					$body .= '<div class="inner-wrapper" style="background-color: #ffffff; padding-top: 40px; border: 1px solid #CCC6BE; width: 580px; margin: 0 auto;" >';
						$body .= '<div style="display: block; t	ext-align: center;">';
							$body .= '<div class="company-logo" style="text-align:center;"><img src="' . $logo[0] . '" width="250" alt=""></div>';
							$body .= '<div style="padding: 0 30px 0 30px;">
										<ul class="company-info" style="list-style: none; line-height: 23px; padding: 60px 0 50px 0; border-bottom: 1px solid #CCC6BE; text-align:center;	">
											<li style="font-size: 32px; color: #000000; font-weight: 600;">' .$company_info['company'] . '</li>
											<li style="padding-top:15px; font-size: 22px;"><a href="mailto:' . $company_info['email'] . '" style="text-decoration: underline; color:' . $proposalColor . '; font-size: 22px;">' . $company_info['email'] . '</a></li>
										</ul>
									</div>';
						$body .= '</div>';

						$body .= '<div style="color: #000000; padding: 50px 40px 10px 40px; display: block; font-size: 22px; font-weight: 100;">' . wpautop( $request->input( 'content' ) ) . '';

							$body .= '<p style="width: 100%; text-align: center; padding-top: 30px;"><a href="' . home_url( '/view-proposal/' . sc_encrypted_string( $event->id ) ) . '" style="text-align: center; background-color:' . $proposalColor . '; color: #ffffff; font-size: 18px; border-radius: 6px; text-decoration: none; padding: 15px 25px 15px 25px;" target="_none">View Proposal</a></p>';
							$body .= '<p style="width: 100%; text-align: center; padding-top: 20px;"><a href="' . $company_info['website'] . '" target="_blank"  style="text-decoration: underline; color: #000000; font-size: 20px; padding-top: 20px;">' . $company_info['website'] . '</a></p>';
						$body .= '</div>';
					$body .= '</div>';
				$body .= '</div>';
			$body .= '</html>';

			wp_mail( $to, $subject, $body, $headers );

			$r->add_payload( 'message', 'Proposal was sent.' );
		} else {
			$r->fail( 'Customer email is not set!' );
		}

		remove_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );
	} catch ( Exception $r ) {
		$r->fail( 'This event doesn\'t exist.' );
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_proposal_mail', 'sc_proposal_mail', 10 );

function sc_datatables_len() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	if ( empty( $request->input('meta_key') ) || empty( $request->input('meta_value') ) ) {
		$r->fail( 'Failed to set table length.' );

	}

	if ( ! preg_match( '~^sc_[\w]*_len$~', $request->input('meta_key') ) ) {
		$r->fail( 'Meta key doesn\'t match!' );
	}

	update_user_meta(
		get_current_user_id(),
		$request->input('meta_key'),
		$request->input('meta_value')
	);

	$r->respond();
}
add_action( 'wp_ajax_sc_datatables_len', 'sc_datatables_len', 10 );

function sc_upload_dragged_media() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$event = '0' === $request->input( 'event_id' ) ? sc_get_fake_event_for_user() : sc_user_can_edit_event( $request->input( 'event_id' ) );
	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}

	$thumb_url = $request->input( 'url' );
	if ( empty( $thumb_url ) ) {
		$r->fail('Failed to upload arrangement photo.');
	}

	try {
		$thumb_id = sc_upload_from_url( $thumb_url );

		if (  $thumb_id ) {
			$image_id = $thumb_id;
			$image_url = wp_get_attachment_image_src( $image_id, 'medium' );
			$image_url = $image_url[0];
			$full_image = wp_get_attachment_image_src( $image_id, 'full' );

			$arrangement_photos = Arrangement_Photo::where( 'arrangement_id', '=', intval( $request->input( 'arrangement_id' ) ) )->get();

			foreach ( $arrangement_photos as $photo ) {
				if ( $photo->photo_id != $image_id ) {
					$new_photo = new Arrangement_Photo;
					$new_photo->photo_id = $image_id;
					$new_photo->arrangement_id = intval( $request->input( 'arrangement_id' ) );
					$new_photo->save();
					break;
				}
			}

			$photo = array(
				'image_id' => $image_id,
				'image_url' => $image_url,
				'full_image_src' => $full_image[0],
				'full_image_w' => $full_image[1],
				'full_image_h' => $full_image[2],
			);
		}

		$r->add_payload( 'photo', $photo );
	} catch (Humanreadable_Exception $e) {
		if ( is_super_admin() ) {
			$r->fail( 'Failed to upload arrangement photo.' . $e->getMessage() );
		} else {
			$r->fail( 'Failed to upload arrangement photo.' );
		}
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_upload_dragged_media', 'sc_upload_dragged_media', 10 );

function get_attachment_id_from_src( $image_src ) {
	global $wpdb;

	$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
	$id = $wpdb->get_var($query);
	return $id;
}


function sc_mass_event_edit() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$events_ids = $request->input( 'events' );
	$action = $request->input( 'selected_action' );

	if ( ! empty( $action ) ) {
		try {
			$events = Event::whereIn( 'id', $events_ids )->get();
			if ( ! $events->isEmpty() ) {
				foreach ( $events as $event ) {
					if ( 'archived' == $action || 'published' == $action ) {
						$event->status = $action;
					} else {
						$event->state = $action;
					}
					$event->save();

					// Add events to google calendar if marked as 'booked'
					do_action( 'sc/event/save', $event );
				}
			}
			$r->add_payload( 'message', 'Events were changed successfully' );
		} catch (Humanreadable_Exception $e) {
			if ( is_super_admin() ) {
				$r->fail( 'Failed to edit selected events' . $e->getMessage() );
			} else {
				$r->fail( 'Failed to edit selected events' );
			}
		}
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_mass_event_edit', 'sc_mass_event_edit', 10 );

function sc_edit_event_tags() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();



	$action = $request->input( 'selected_action' );

	if ( ! empty( $action ) ) {
		try {

			if ( 'delete' == $action ) {
				$event_id = $request->input( 'event_id' );
				$tag = $request->input( 'tag' );
				$event_tags_meta = Meta::where(array(
					'type' => 'event',
					'type_id' => $event_id,
					'meta_key' => 'event_tags',
					'meta_value' => $tag,
				))->delete();

			} else if( 'mass_assign' == $action ) {
				$tag = $request->input( 'tag' );
				$tag_class = esc_attr( 'tag-' . sanitize_title_with_dashes( $tag ) );
				$selected_events = $request->input( 'selected_events' );

				foreach( $selected_events as $event_id ) {
					$event_tags = Meta::firstOrCreate( array(
						'type' => 'event',
						'type_id' => $event_id,
						'meta_key' => 'event_tags',
						'meta_value' => $tag,
					));
				}
				$html_tag='<a href="javascript:void(0)"  class="btn btn-xs btn-default tag event-tag ' .
					$tag_class . '" ' .
					'data-tag="' . esc_attr( $tag ) . '" '.
					'data-tag-class="' . $tag_class . '" '.
					'data-eventid="' . esc_attr( $event_id) . '" '.
					'><span class="glyphicon glyphicon-remove"></span> '.
					$tag . '</a>';
				$r->add_payload( 'html_tag', $html_tag );
			}

			$r->add_payload( 'message', 'Tags were edited' );

		} catch (Humanreadable_Exception $e) {
			if ( is_super_admin() ) {
				$r->fail( 'Failed to edit selected tags' . $e->getMessage() );
			} else {
				$r->fail( 'Failed to edit selected tags' );
			}
		}
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_edit_event_tags', 'sc_edit_event_tags', 10 );


function sc_get_event_tipinfo() {
	$request = Request::capture();
	$r = new Ajax_Response();
	$event_id = intval( $request->input( 'event_id' ) );

	$event = Event::where( array(
		'id' => $event_id,
	))->with( array(
		'arrangements' => function($query){
			$query->orderBy( 'order', 'asc' );
		},
		'arrangements.items',
		'arrangements.recipes',
		'arrangements.items.item_variation',
		'arrangements.items.item_variation.item',
		'live_payments',
	) )->first();

	if ( get_current_user_id() != $event->user_id ) {
		$r->fail( 'You are not authorized to acess this data' .  $event->user_id);
	}

	$popup_info = array();
	$popup_info['total'] = sc_format_price( $event->get_total( true ), true );
	$popup_info['vendors'] = array_column( sc_get_user_event_venues( $event->id ), 'name' );
	$popup_info['payment_shedule'] = $event->live_payments;
	$popup_info['is_signed'] = $event->latest_version_signed();

	$tooltip_html = "Total: <strong>{$popup_info['total']}</strong><br />";
	$tooltip_html .= ( 1 === intval( $popup_info['is_signed'] ) ) ? ' signed' : ' not signed';
	$tooltip_html .= '<br />';
	if ( ! empty( $popup_info['vendors'] ) ) {
		$tooltip_html .= 'Vendors:<br /><ul class="payments"><li>' . implode( '</li><li>', $popup_info['vendors'] ) . '<li></ul>';
	} 
	$payments = array();
	foreach( $popup_info['payment_shedule'] as $payment ) {
		$payments[] = "<li>{$payment->due_date}:  " . sc_format_price( $payment->amount, true ) . " ({$payment->status})</li>" ;
	}
	if ( ! empty( $payments ) ) {
		$tooltip_html .= 'Payments:<br />';
		$tooltip_html .= '<ul class="payments">' . implode( '', $payments ) . '</ul>';
	}

	$r->add_payload( 'tooltip_html', $tooltip_html );
	$r->respond();
}

add_action( 'wp_ajax_sc_get_event_tipinfo', 'sc_get_event_tipinfo', 10 );

function sc_public_og_image() {
	$user_id = get_current_user_id();
	if ( ! empty( $GLOBALS['user_id'] ) ) {
		$user_id = $GLOBALS['user_id'];
	}

	$logo = null;
	if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
		$logo_id = sc_get_profile_logo_id( $user_id );
		$logo = wp_get_attachment_image_src( $logo_id, 'large' );

		?>
		<meta name="og:image" content="<?php echo esc_url( $logo[0] ); ?>" />
		<meta name="og:image:width" content="<?php echo esc_attr( $logo[1] ); ?>" />
		<meta name="og:image:height" content="<?php echo esc_attr( $logo[2] ); ?>" />
		<?php
	}
}

function render_itemslist_style( $user_id ) {
	$user_libraries = get_user_meta( $user_id, 'sc_user_libraries', true );
	$user_libraries = is_array( $user_libraries ) ? $user_libraries : array();

	if ( empty( $GLOBALS['wp']->query_vars['event-uid'] ) ) :
		$color = \SCAC::can_access( \SCAC::STUDIO ) ? get_user_meta( get_current_user_id(), 'proposal_color', true ) : false;
		$color = $color ? $color : '#f37a76'; ?>
		div.Select .Select-menu-outer .VirtualizedSelectOption.user-lib-<?php echo $user_id; ?>:before {
			content: '\f02d';
			display: inline-block;
			font-family: 'FontAwesome';
			font-size: 14px;
			padding-right: 5px;
			padding-left: 3px;
			color: <?php echo $color; ?>;
		}

		<?php foreach ( $user_libraries as $library ):
			$library_logo = get_user_meta( $library, 'sc_user_library_logo', true );
			if ( ! empty( $library_logo ) ) : ?>
		div.Select .Select-menu-outer .VirtualizedSelectOption.user-lib-<?php echo $library; ?> {
			background-image: url('<?php echo $library_logo; ?>');
			background-repeat: no-repeat;
			background-position: 6px 6px;
			background-size: 20px auto;
			padding-left: 31px;
		}
			<?php endif;
		endforeach;
	endif;

}


function sc_ajax_replace_flower () {
	global $wpdb;

	sc_clean_input_slashes();
	$request 	= Request::capture();
	$r = new Ajax_Response();

	$type_id 	= intval( $request->input('type_id'), 10  );
	$type 		= $request->input('type');
	$old_iid 	= explode( '_', $request->input('old_iid') );
	$new_iid 	= explode( '_', $request->input('new_iid') );

	if ( empty( $old_iid[1] )  ) {
		$variation = Item_Variation::where('id', $request->input('old_iid') )->first();
		$old_iid = array( $variation->item_id, $request->input('old_iid') );
	}

	if ( empty( $old_iid[0] )  || empty( $old_iid[1] ) || empty( $new_iid[0] ) || empty( $new_iid[1]) ) {
		$r->fail( 'Some of item ids are missing. Please select a valid item!' );
	}


	$variation = Item_Variation::where('id', $new_iid[1])->first();

	if ( 'order' == $type ) {

		//Get real events in order, based on order id
		try {
			$order = Order::where(array(
				'id' => $type_id,
				'user_id' => get_current_user_id()
			))->with('items')->firstOrFail();
			$order_events = json_decode( $order->event_ids );
		} catch (Exception $e) {
			$r->fail('Something went wrong. Please try again laterr.');
		}


		/*$order = Order::select( 'event_ids' )->where( 'id', $type_id )->first();
		if ( empty ( $order->event_ids ) ) {
			$r->fail( 'Did not found events for this order' );
		}*/
		if ( 'Default' !== $variation->name && ! empty ( $variation->name ) ) {
			$new_name = $variation->item->name . ' - ' . $variation->name;
		} else {
			$new_name = $variation->item->name;
		}
		$new_key = "{$new_name}_{$new_iid[0]}";

	} else if ( 'event' == $type ) {
		$order_events = array( $type_id );
	}

	foreach( $order_events as $eid ) {
		$cost = $variation->cost;
		if ( is_null( $cost ) ) {
			$cost = $variation->item->default_variation->cost;
		}

		//Update every event with the new item
		$query = $wpdb->prepare( "UPDATE {$wpdb->prefix}sc_arrangement_items
				SET `item_id`=%d, `item_variation_id`=%d, `item_type`= 'Stencounter\\Item'
				WHERE arrangement_id IN ( SELECT `id` FROM {$wpdb->prefix}sc_arrangements  WHERE event_id=%d)
				AND `item_id` = %d
				AND `item_variation_id` = %d", array( $new_iid[0], $new_iid[1], $eid, $old_iid[0], $old_iid[1] ) );

		//error_log( $query );
		$wpdb->query( $query );

	}


	if ( 'order' == $type ) {
		//Check if we have any real events
		//
		$order_events = array_filter( $order_events, function( $event_id ) {
			return $event_id > 0;
		});
		try {
			if ( empty( $order_events ) ) throw new Humanreadable_Exception( 'You need at least one Event listed.' );
		} catch ( Humanreadable_Exception $e ) {
			$r->fail( $e->getMessage() );
		}

		$total_floral = $total_service = $total_combined = 0;

		//Get items for the list of events
		try {
			$user_id = intval(get_current_user_id());
			// get all arrangement items only for arrangements included in the proposal
			$shopping_list = array();
			$items = array();

			foreach ( $order_events as $event_id ) {
				$event = Event::where( array(
					'id' => $event_id,
				) )->with(array(
					'arrangements' => function( $query ) {
						$query->where( 'include', '=', 1 )->where( 'addon', '=', 0 )->orderBy( 'order', 'asc' );
					},
				), 'arrangements.items', 'arrangements.items.item_variation' )->firstOrFail();

				foreach ( $event->arrangements as $arrangement ) {
					foreach ( $arrangement->items as $arrangement_item ) {
						$type = sc_get_simple_item_type( $arrangement_item->item, false, $arrangement_item );
						$type_label = sc_get_item_type( $arrangement_item->item, true, $arrangement_item );
						$name = $arrangement_item->get_full_name() . "_{$arrangement_item->item_id}";
						$quantity = $arrangement_item->quantity * $arrangement->quantity;
						$item_cost = $arrangement_item->cost;

						$items_su = $arrangement_item->item->purchase_qty;
						//if is deleted we need to know how much SU does that flower have
						if ( is_null( $items_su ) ) {
							try {
								$item = Item::where( array(
									'id' => $arrangement_item->item_id,
								) )->withTrashed()->firstOrFail()->toArray();
								$items_su = $item['purchase_qty'];
							} catch (Exception $e) {
							}
						}
						$su_needed = ceil( $quantity / $items_su );
						$total_cost = ( $items_su * $su_needed ) * $item_cost;
						$total_cost = $total_cost;
						$leftovers = ( $items_su * $su_needed ) - $quantity;

						if ( ! isset( $shopping_list[ $event_id ][ $type ][ $name ] ) ) {
							$shopping_list[ $event_id ][ $type ][ $name ] = array(
								'id' => $arrangement_item->item_variation_id ? $arrangement_item->item_variation_id : $arrangement_item->item_id,
								'type' => $type_label,
								'name' => $arrangement_item->get_full_name(),
								'quantity' => $quantity,
								'cost' => $total_cost,
							);

							$shopping_list[ $event_id ][ $type ][ $name ]['items_su'] = $items_su;
							$shopping_list[ $event_id ][ $type ][ $name ]['su_needed'] = $su_needed;
							$shopping_list[ $event_id ][ $type ][ $name ]['leftovers'] = $leftovers;
						} else {
							$shopping_list[ $event_id ][ $type ][ $name ]['quantity'] += $quantity;

							$su_needed = ceil( $shopping_list[ $event_id ][ $type ][ $name ]['quantity'] / $items_su );
							$total_cost = ( $items_su * $su_needed ) * $item_cost;
							$total_cost = $total_cost;
							$leftovers = ( $items_su * $su_needed ) - $shopping_list[ $event_id ][ $type ][ $name ]['quantity'];

							$shopping_list[ $event_id ][ $type ][ $name ]['su_needed'] = $su_needed;
							$shopping_list[ $event_id ][ $type ][ $name ]['cost'] = $total_cost;
							$shopping_list[ $event_id ][ $type ][ $name ]['leftovers'] = $leftovers;
						}
					}
				}

				if ( array_key_exists( 'flower', $shopping_list[ $event_id ] ) && ! empty( $shopping_list[ $event_id ]['flower'] ) ) {
					ksort( $shopping_list[ $event_id ]['flower'] );
				}

				if ( array_key_exists( 'service', $shopping_list[ $event_id ] ) && ! empty( $shopping_list[ $event_id ]['service'] ) ) {
					ksort( $shopping_list[ $event_id ]['service'] );
				}
			}

			//-----------------------------------------------------------*/

			$calculated_shopping_list = array(
				'flower' => array(),
				'service' => array()
			);
			foreach ( $shopping_list as $event_id => $flowers_items ) {
				foreach ( $flowers_items as $type => $values ) {
					if ( ! empty( $values ) ) {
						foreach ( $values as $name => $info ) {
							if ( isset( $calculated_shopping_list['flower'][ $name ] ) ) {
								$calculated_shopping_list['flower'][ $name ] = array(
									'quantity' => $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'],
									'cost' => $calculated_shopping_list['flower'][ $name ]['cost'] + $info['cost'],
									'items_su' => $info['items_su'],
									'su_needed' => ceil( ( $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'] ) / $info['items_su'] ),
									'leftovers' => ( $info['items_su'] * ceil( ( $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'] ) / $info['items_su'] ) ) - ( $calculated_shopping_list['flower'][ $name ]['quantity'] + $info['quantity'] ),
								);
							} else {
								$calculated_shopping_list['flower'][ $name ] = array(
									'quantity' => $info['quantity'],
									'cost' => $info['cost'],
									'items_su' => $info['items_su'],
									'su_needed' => $info['su_needed'],
									'leftovers' => $info['leftovers']
								);
							}
						}
					}
				}
			}

			foreach ( $calculated_shopping_list['flower'] as $item ) {
				$total_floral += $item['cost'];
			}
			foreach ( $calculated_shopping_list['service'] as $item ) {
				$total_service += $item['cost'];
			}

			$total_combined = $total_floral + $total_service;
		} catch ( Exception $e ) {
			$r->fail( $e->getMessage() );
		}


		//Save discarded items information, for the refreshing process
		$discarded_items = array();
		foreach ( $order->items as $item) {
			if ( 1 == $item['discarded'] ) {
				$discarded_items[ $item['event_id'] . '_' . $item['item_id'] ] = true;
			}
		}

		try {

			//Delete unneccesary items from order
			try {
				// Delete old replaced item and old data for the new item
				$order->items()->whereIn( 'item_id', array( $old_iid[1], $new_iid[1] ) )->where( 'event_id', '!=', 0 )->forceDelete();
			} catch (Exception $e) {
				$r->fail('Something went wrong. Please try again.');
			}

			$updated_manual_items = false;

			//foreach ( $shopping_list as $event_id => $item_types ) {
			foreach ( $shopping_list as $event_id => $item_types ) {

				//Add new items
				foreach ( $item_types as $item_type => $_items ) {
					if ( empty( $_items ) ) {
						continue;
					}

					foreach ( $_items as $_item ) {
						if ( $new_iid[1] != $_item['id'] ) {
							continue;
						}

						$_item['items_su'] = ! empty( $_item['items_su'] ) ? $_item['items_su'] : 1;
						$_item['su_needed'] = ! empty( $_item['su_needed'] ) ? $_item['su_needed'] : 1;

						if ( ! $updated_manual_items ) {
							$single_price = $_item['cost'] / ( $_item['items_su'] * $_item['su_needed'] );
							$wpdb->query( $wpdb->prepare(
								"UPDATE {$wpdb->prefix}sc_order_items
								SET `item_id` = %d,
									`name` = %s,
									`pricing_category` = %s,
									`cost` = cost * %d,
									`items_su` = %d
								WHERE order_id=%d
								AND `item_id` = %d
								AND event_id=0",
								$new_iid[1],
								$_item['name'],
								$_item['type'],
								$single_price,
								$_item['items_su'],
								$order->id,
								$old_iid[1]
							) );
							$updated_manual_items = true;
						}

						$new_item = new Order_Item;
						$new_item->order_id = $order->id;
						$new_item->event_id = $event_id;
						$new_item->item_id = $_item['id'];
						$new_item->name = $_item['name'];
						$new_item->pricing_category = $_item['type'];
						$new_item->quantity = $_item['quantity'];
						$new_item->cost = $_item['cost'];
						$new_item->items_su = ! empty( $_item['items_su'] ) ? $_item['items_su'] : 1;
						$new_item->su_needed = ! empty( $_item['su_needed'] ) ? $_item['su_needed'] : 1;
						$new_item->leftovers = ! empty( $_item['leftovers'] ) ? $_item['leftovers'] : 0;
						$new_item->discarded = intval( isset( $discarded_items[ "{$new_item->event_id}_{$new_item->item_id}" ]  ) );
						$new_item->save();
						$new_item = null;
					}
				}
			}
		} catch ( Exception $e ) {
			$r->fail( $e->getMessage() );
		}

		$order = Order::where(array(
			'id' => $type_id,
			'user_id' => get_current_user_id()
		))->with('items')->firstOrFail();

		$shopping_list = sc_get_shopping_list_from_order_items( $order->items );
		// Update the order cost estimate
		$total_combined = wp_list_pluck( $shopping_list['included'], 'cost' );
		$total_combined = array_sum( $total_combined );
		$order->estimate = $total_combined;
		$order->save();
		$r->add_payload( 'items', $shopping_list );
		$r->respond( 'Order refreshed and saved' );
	}

	$r->respond();

}
add_action( 'wp_ajax_sc/event_management/replace_flower', 'sc_ajax_replace_flower', 10 );

function sc_edit_event_property_fields() {
	sc_clean_input_slashes();
	$request 	= Request::capture();
	$r = new Ajax_Response();

	$property = $request->input( 'property' );
	$event_id = $request->input( 'event_id' );

	try {
		foreach ( $property['values'] as $key => $value ) {
			if ( 0 < intval( $value['id'] ) ) {
				try {
					$event_property = Event_Property::where( array(
						'id' => intval( $value['id'] ),
					) )->first();
				} catch ( Exception $e ){
					$r->fail( $e->getMessage() );
				}
			} else {
				$event_property = new Event_Property();
			}

			$event_property->event_id = intval( $value['event_id'] );
			$event_property->property_id = intval( $property['id'] );
			$event_property->value = $value['value'];
			$event_property->save();

			$property['values'][ $key ]['id'] = $event_property->id;

		}

		if ( 'multi-checkbox' == $property['type'] ) {
			try {
				$event_properties = Event_Property::where( array(
					'event_id' 		=> intval( $event_id ),
					'property_id' => intval( $property['id'] ),
				) )->get();
				foreach ( $event_properties as $_property ) {
					$event_property_exists = wp_list_filter( $property['values'], array( 'id' => $_property->id ) );
					// Delete the question if it was not sent in the request
					if ( empty( $event_property_exists ) ) {
						$_property->delete();
					}
				}
			} catch (Exception $e) {
				$r->fail( $e->getMessage() );
			}
		}

	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_edit_event_properties', 'sc_edit_event_property_fields' );

function sc_proposal_spreadsheet() {
	$event_id = intval( $_GET['event_id'] );

	if ( ! $event_id ) {
		exit;
	}

	try {
		/* if ( -1 != intval( $version_id ) ) {
			$version = Event_Version::where( array( 
				'id'	=> $version_id,
			) )->first();

			$event = json_decode( $version->event_data );
			$event = $event->event
		} */
		$event = Event::where( array(
			'id' => $event_id,
		))->with( array(
			'arrangements' => function( $query ){
				$query->where( 'include', '=', 1 )->where( 'addon', '=', 0 )->orderBy( 'order', 'asc' );
			},
			'arrangements.items',
			'arrangements.recipes',
			'arrangements.items.item_variation',
			'arrangements.items.item_variation.item',
		) )->first();

		$admin_CSV = array();
		$admin_CSV[] = array( 'Item Name', 'Quantity', 'Price', 'Subtotal', 'Taxable', 'Markup' );
		//cells					   A			B		  C			D			E		  F			

		$non_percent_ids = array();
		foreach( $event->arrangements as $key => $arrangement ) {
			if ( ! $arrangement->is_percentage ) {
				$non_percent_ids[] = $arrangement->id;
			}
		}

		// Add proposal items
		foreach( $event->arrangements as $arr_key => $arrangement_item ) {
			// Markup profile
			if ( $arrangement_item->invoicing_category_id ) {
				$invoice_category = Invoicing_Category::where(array(
					'id' => $arrangement_item->invoicing_category_id,
				))->first();

				$markup = $invoice_category->name;
			} else {
				$markup = 'Default';

			}

			$taxable = $arrangement_item->tax ? 'X': '';
			$arrangement_price = 0;
			$arrangement_subtotal = 0;
			if ( $arrangement_item->is_percentage ) {

				$arrangement_price = $arrangement_item->override_cost . '%';
				$percentage_keys = array();
				foreach( $event->arrangements as $key => $arrangement ) {
					if ( ! in_array( $arrangement->id, $arrangement_item->applies_on ) ) {
						continue;
					}

					if ( $arrangement->is_percentage ) {

						if ( $arrangement->id == $arrangement_item->id ) {
							continue;
						}

						if ( ! $arrangement_item->can_be_applied_on( $arrangement->applies_on, $non_percent_ids ) ) {
							continue;	
						}
					}
					$percentage_keys[] .= 'D' . ( $key + 2 );
				}
				$arrangement_subtotal = '=ROUND(SUM(' . implode( ',', $percentage_keys ) . ') * C' . ( $arr_key + 2 ) . ' ,2)';
			} else {
				$arrangement_price = $arrangement_item->get_final_price( false );
				$arrangement_subtotal = '=ROUND(B' . ( $arr_key + 2 ) . '*C' . ( $arr_key + 2 ) . ',2)';
			}

			$admin_CSV[] = array(
				$arrangement_item->name,
				$arrangement_item->quantity,
				$arrangement_price,
				$arrangement_subtotal,
				$taxable,
				$markup
			);
		}

		// Add Tax rates
		$user_tax_rates = Tax_Rate::where( array(
			'user_id' => $event->user_id
		) )->get()->toArray();
		$event_tax_rate = wp_list_filter( $user_tax_rates, array( 'id' => $event->tax_rate_id ) );
		$event_tax_rate2 = wp_list_filter( $user_tax_rates, array( 'id' => $event->tax_rate_id2 ) );
		$tax_rates = array_merge( $event_tax_rate, $event_tax_rate2 );

		$tax_rate_ids = array();
		foreach ( $tax_rates as $tax_rate ) {
			/** The value of the tax rate was modified */
			if ( $tax_rate['id'] == $event->tax_rate_id &&
				$tax_rate['value'] != $event->tax_rate_value ) {
				array_push( $tax_rates, array(
					'id' => -$tax_rate['id'],
					'user_id' => $user_id,
					'name' => 'Modified Tax Rate',
					'value' => $event->tax_rate_value
				) );
			}

			if ( $tax_rate['id'] == $event->tax_rate_id2 &&
				$tax_rate['value'] != $event->tax_rate_value2 ) {
				array_push( $tax_rates, array(
					'id' => -$tax_rate['id'],
					'user_id' => $user_id,
					'name' => 'Modified Tax Rate 2',
					'value' => $event->tax_rate_value2
				) );
			}

			array_push( $tax_rate_ids, $tax_rate['id'] );
		}

		/**
		* If the Event Tax Rate is not found in the User Tax Rates
		* It means that it was deleted
		*/
		if ( (
				! in_array( $event->tax_rate_id, $tax_rate_ids ) &&
				! empty( $tax_rate_ids ) &&
				! is_null( $event->tax_rate_id ) &&
				$event->tax_rate_id != 0
			) || (
				empty( $tax_rate_ids ) && $event->tax_rate_id != 0
			) ) {
			array_push( $tax_rates, array(
				'id' => -$event->tax_rate_id,
				'user_id' => $user_id,
				'name' => 'Deleted Tax Rate',
				'value' => $event->tax_rate_value
			) );
		}

		if ( (
				! in_array( $event->tax_rate_id2, $tax_rate_ids ) &&
				! empty( $tax_rate_ids ) &&
				! is_null( $event->tax_rate_id2 ) &&
				$event->tax_rate_id2 != 0
			) || (
				empty( $tax_rate_ids ) && $event->tax_rate_id2 != 0
			) ) {
			array_push( $tax_rates, array(
				'id' => -$event->tax_rate_id2,
				'user_id' => $user_id,
				'name' => 'Deleted Tax Rate 2',
				'value' => $event->tax_rate_value2
			) );
		}

		$csv_items_length = count( $admin_CSV );
		$admin_CSV[] = array('', '', '');
		$admin_CSV[] = array( 'Tax', '%', 'Total' );
		//cells					A	  B		C
		foreach ( $tax_rates as $tax_key => $rate ) {
			// VAT, 0.2, =SUMIF(E2:E100,"x",D2:D100)*B2
			$tax_total = '=ROUND(SUMIF( E2:E' . $csv_items_length . ', "X", D2:D' . $csv_items_length . ' ) * B' . ( $csv_items_length + $tax_key + 3 ) . ' ,2)';
			$admin_CSV[] = array( $rate['name'], $rate['value'] . '%', $tax_total );
		}

		$tax_values = 1 == count( $tax_rates ) ? ', C' . ( $csv_items_length + 3 ) : ( 2 == count( $tax_rates ) ? ', C' . ( $csv_items_length + 3 ) . ', C' . ( $csv_items_length + 4 ) : '' );
		$event_total = '=ROUND(SUM( D2:D' . $csv_items_length . $tax_values . '),2)';
		$admin_CSV[] = array('', '', '');
		$admin_CSV[] = array( 'Event', '', 'Software Total', 'Spreadsheet Total' );
		$admin_CSV[] = array( '', '', $event->get_total( true ), $event_total );

		$name = $event->name ? $event->name . '.csv' : 'event-spreadsheet.csv';
		header( 'Content-Type: text/csv; charset=utf-8' );
		header( 'Content-Disposition: attachment; filename=' . $name );
		
		$output = fopen('php://output', 'w');
		foreach ( $admin_CSV as $line ) {
			fputcsv( $output, $line, ',' );
		}
		fclose( $output );
	} catch (Exception $e) {
		echo $e->getMessage();
	}

	exit;
}
add_action( 'wp_ajax_sc_proposal_spreadsheet', 'sc_proposal_spreadsheet' );

function sc_save_live_payment() {
	sc_clean_input_slashes();
	$request 	= Request::capture();
	$r = new Ajax_Response();
	
	$payment = $request->input( 'payment' );
	$event_id = $request->input( 'event_id' );
	try {
		$event = sc_user_can_edit_event( $event_id );
	} catch ( Exception $e ) {
		$r->fail( 'Something went wrong. Please refresh the page and try again.' );
	}
	
	try {
		$live_payment = Payment::where( array(
			'id' => $payment['paymentId'],
			'event_id' => $event_id,
			'is_live' => 1,
		) )->first();
	} catch ( Exception $e ) {
		$r->fail( 'Something went wrong. Please refresh the page and try again.' );
	}
		
	$live_payment_amount = $live_payment->amount;
	$found_payment = false;

	foreach ( $event->payments as $_payment ) {
		$payment_amount = $_payment->get_amount();

		if ( $live_payment_amount == $payment_amount && 
				$live_payment->due_date == $_payment->due_date &&
				$live_payment->card_fee == $_payment->card_fee ) {
			$found_payment = $_payment;
			break;
		}
	}

	if ( ! $found_payment ) {
		$found_payment = $live_payment->replicate( array( 'created_at', 'updated_at', 'id' ) );
		$found_payment->is_live = 0;
	}

	// Only update the payment info if the payment doesn't exist
	// Or the payment has not been made through Stripe
	if ( ! $found_payment->exists() || 'stripe' != $found_payment->payment_type ) {
		$date_format = get_user_meta( $event->user_id, '__date_format', true );

		$found_payment->payment_type = $live_payment->payment_type = ! empty( $payment['paymentType'] ) ? $payment['paymentType'] : '';
		$found_payment->status = $live_payment->status = $payment['status'];
		$found_payment->payment_note = $live_payment->payment_note = ! empty( $payment['note'] ) ? $payment['note'] : '';
		$found_payment->amount = $live_payment->amount;

		if ( ! empty( $payment['payment_date'] ) ) {
			$payment_date = DateTime::createFromFormat( $date_format, $payment['payment_date'] );
			if ( $payment_date ) {
				$found_payment->payment_date = $live_payment->payment_date = date( MYSQL_DATE_FORMAT, $payment_date->getTimestamp() );
			}
		} else {
			$found_payment->payment_date = $live_payment->payment_date = null;
		}

		$found_payment->save();
		$live_payment->save();
		do_action( 'sc/payment/save', array( $event, $found_payment ) );
	}

	$r->add_payload( 'message', 'Payment information updated.' );
	$r->respond();
}
add_action( 'wp_ajax_sc/payment/live/save', 'sc_save_live_payment', 10 );
