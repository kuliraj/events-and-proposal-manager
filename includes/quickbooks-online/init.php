<?php
use Illuminate\Http\Request;
use Stemcounter\Event;
use Stemcounter\Event_Version;
use Stemcounter\Payment;
use Stemcounter\Meta;
use Stemcounter\Tax_Rate;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;
use QuickBooksOnline\API\Security\OAuthRequestValidator;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\Core\CoreConstants;
use QuickBooksOnline\API\PlatformService\PlatformService;

use QuickBooksOnline\API\Data\IPPPhysicalAddress;
use QuickBooksOnline\API\Data\IPPInvoice;
use QuickBooksOnline\API\Data\IPPLine;
use QuickBooksOnline\API\Data\IPPLineDetailTypeEnum;
use QuickBooksOnline\API\Data\IPPSalesItemLineDetail;
use QuickBooksOnline\API\Data\IPPItem;
use QuickBooksOnline\API\Data\IPPAccountTypeEnum;
use QuickBooksOnline\API\Data\IPPAccount;
use QuickBooksOnline\API\Data\IPPCustomer;
use QuickBooksOnline\API\Data\IPPDiscountLineDetail;
use QuickBooksOnline\API\Data\IPPTaxRateDetails;
use QuickBooksOnline\API\Data\IPPTaxService;
use QuickBooksOnline\API\Data\IPPTaxAgency;
use QuickBooksOnline\API\Data\IPPTxnTaxDetail;
use QuickBooksOnline\API\Data\IPPTaxLineDetail;
use QuickBooksOnline\API\Data\IPPReferenceType;


/**
* 
*/
class SC_Quickbooks_Online {
	const OAUTH_REQUEST_URL   = 'https://oauth.intuit.com/oauth/v1/get_request_token';
	const OAUTH_ACCESS_URL    = 'https://oauth.intuit.com/oauth/v1/get_access_token';
	const OAUTH_AUTHORISE_URL = 'https://appcenter.intuit.com/Connect/Begin';
	const SC_PREFIX	          = 'SC ';
	const QB_ID_CACHE_ENABLED  = true;

	protected static $started = false;
	protected static $oauth_client = false;
	protected static $data_service = false;

	public static function start() {
		if ( ! self::$started ) {
			self::add_actions();
			self::add_filters();

			self::$started = true;
		}
	}

	protected static function add_actions() {
		add_action( 'init', array( __CLASS__, 'maybe_handle_auth_callback' ), 10 );

		add_action( 'sc/studio/integration/qbo', array( __CLASS__, 'render' ), 10 );

		add_action( 'template_redirect', array( __CLASS__, 'debug_sync' ), 10 );

		add_action( 'template_redirect', array( __CLASS__, 'add_click_handler' ), 10 );

		add_action( 'wp_ajax_sc_studio_sync_invoice', array( __CLASS__, 'sc_studio_sync_invoice' ), 10 );

	}

	public static function debug_sync() {
		if ( !empty($_GET['qbo']) && !empty($_GET['eid']) ) {
			try {
				$result = 'Quickbooks document id: ' . self::invoice_create( $_GET['eid'] );
			} catch ( Exception $e ) {
				$result = $e->getMessage();
			}

			add_action('wp_footer',  function() use ( $result ) {
				if ( $result ) {
					echo '<script type="text/javascript">alert("' . $result . '")</script>';
				}
			}  );
		}
	}

	public static function add_click_handler() {
		add_action('wp_footer',  array( __CLASS__, 'sc_render_js' )  );
	}


	public static function sc_render_js() {
		?>
		<script type="text/javascript">
		$( document ).ready(function() {
			var qb_syncing_ids = [];
			$( "#studio-reports" ).on('click', "td.cell-qbo_sync a" ,  function( e ) {
				e.preventDefault();
				
				var eid = $( this ).closest('tr').attr( 'data-eid' );
				if ( -1 < qb_syncing_ids.indexOf(eid) ) {
					return false;
				}

				var component = $( this ).closest('.report-component');
				var component = component.data( 'ReactComponent' );
				var rows = $.extend( true, [], component.dataTable.state.filteredDataList );
				var old_sync_label = '';
				$.each( rows, function( index, row ) {
					if ( eid == $(this)[0]._data_atts.eid ) { 
						old_sync_label = $(this)[0].qbo_sync;
						$(this)[0].qbo_sync = 'Syncing...';
						$(this)[0]._data_atts.syncing = 1;
					}
				} );
				component.dataTable.setState( {filteredDataList: rows} );
				var $current_row = $( this ).closest("tr[data-syncing='1']")
				$current_row.addClass( 'syncing' );
				
				qb_syncing_ids.push( eid );
				var url = $( this).attr( 'href' ), _this = $( this );

				$.post(url, {
				}, function ( response ) {
					stemcounter.JSONResponse(response, function (r) {
						if (r.success) {
							alertify.success(r.payload.message);
						} else {
							alertify.error(r.message);
						}
					});
				}).always( function( response ) {
					qb_syncing_ids.splice( qb_syncing_ids.indexOf(eid), 1);

					var new_sync_label = old_sync_label;
					stemcounter.JSONResponse(response, function (r) {
						if (r.success) {
							new_sync_label = 'Resync to QBO';
						}
					});

					$.each( rows, function( index, row ) {
						if ( eid == $(this)[0]._data_atts.eid ) { 
							$(this)[0].qbo_sync = new_sync_label;
							$(this)[0]._data_atts.syncing = 0;
						}
						$current_row.removeClass( 'syncing' );
						component.dataTable.setState( {filteredDataList: rows} );
					} );

				} );
				return false
			});
		});
		</script>
		<?php

	}


	protected static function add_filters() {
		add_filter( 'sc/studio/integrations', array( __CLASS__, 'register_integration' ), 15 );

		add_filter( 'sc/react/data/proposal', array( __CLASS__, 'filter_react_proposal_data' ), 10, 3 );
		
		add_filter( 'sc/studio/reports/event/data', array( __CLASS__, 'filter_reports_event_data' ), 10, 3 );

		add_filter( 'sc/studio/reports/event/labels', array( __CLASS__, 'filter_reports_event_labels' ), 10, 3 );
	}


	public static function filter_reports_event_labels( $labels ) {
		if ( ! self::is_connected() ) {
			return $labels;
		}

		$labels[] = array(
			'label'		=> 'QBO sync',
			'key'		=> 'qbo_sync',
			'type'		=> 'url',
		);
		return $labels;
	}

	public static function filter_reports_event_data( $data, $event ) {

		if ( ! self::is_connected() ) {
			return $data;
		}

		$qb_customer_meta = Meta::where(array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'qb_invoice_id' 
		))->first();

		$data['qbo_sync'] = empty( $qb_customer_meta->meta_value ) ? 'Sync to QBO' : 'Resync to QBO';
		$data['qbo_sync_url'] =
			add_query_arg( array(
				'action' => 'sc_studio_sync_invoice',
				'id' => $event->id 
				), admin_url( 'admin-ajax.php' ) );

		return $data;
	}

	public static function register_integration( $integrations ) {
		$integrations[] = array(
			'key' => 'qbo',
			'label' => 'QuickBooks Online',
		);

		return $integrations;
	}

	public static function get_client() {
		if ( ! self::$oauth_client ) {
			self::$oauth_client = new OAuth( SC_QB_OAUTH_KEY, SC_QB_OAUTH_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI );

			if ( WP_DEBUG ) {
				self::$oauth_client->enableDebug();
			}
		}

		return self::$oauth_client;
	}

	public static function get_data_service() {
		if ( ! self::$data_service ) {
			$user_access_token = self::get_user_access_token();

			/**
			 * @todo check if token should be refreshed {@link https://developer.intuit.com/docs/0100_quickbooks_online/0100_essentials/000500_authentication_and_authorization/connect_from_within_your_app#/Managing_OAuth_access_tokens Managing OAuth access tokens}
			 */

			$requestValidator = new OAuthRequestValidator(
				$user_access_token['oauth_token'],
				$user_access_token['oauth_token_secret'],
				SC_QB_OAUTH_KEY,
				SC_QB_OAUTH_SECRET
			);
			$settings = self::get_current_context_settings();
			self::$data_service = DataService::Configure( $settings );
		}

		self::$data_service->useXml();

		return self::$data_service;
	}

	public static function get_grant_url() {
		return add_query_arg(
			array(
				'sc_qbo_action' => 'start_oauth',
			),
			home_url( '/' )
		);
	}

	public static function get_disconnect_url() {
		return add_query_arg(
			array(
				'sc_qbo_action' => 'disconnect',
			),
			home_url( '/' )
		);
	}

	public static function get_oauth_callback_url() {
		return add_query_arg(
			array(
				'sc_qbo_action' => 'handle_oauth',
			),
			home_url( '/' )
		);
	}

	public static function get_user_access_token( $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		return get_user_meta( $user_id, '_sc_qbo_access_token', true );
	}

	public static function set_user_access_token( $token, $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		if ( isset( $token['refresh_token'] ) ) {
			self::set_user_refresh_token( $token['refresh_token'], $user_id );
		}

		return update_user_meta( $user_id, '_sc_qbo_access_token', $token );
	}

	public static function set_oauth_token_secret( $token, $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		return update_user_meta( $user_id, '_sc_qbo_token_secret', $token );
	}

	public static function get_oauth_token_secret( $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		return get_user_meta( $user_id, '_sc_qbo_token_secret', true );
	}

	public static function get_user_realm_id( $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		return get_user_meta( $user_id, '_sc_qbo_realm_id', true );
	}

	public static function set_user_realm_id( $token, $user_id = false ) {
		$user_id = $user_id ? $user_id : get_current_user_id();

		return update_user_meta( $user_id, '_sc_qbo_realm_id', $token );
	}

	public static function maybe_handle_auth_callback( $query ) {
		if ( empty( $_GET['sc_qbo_action'] ) ) {
			return;
		}

		switch ( $_GET['sc_qbo_action'] ) {
			case 'start_oauth':
				try {
					$oauth = self::get_client();
					
					// $oauth->disableSSLChecks(); //To avoid the error: (Peer certificate cannot be authenticated with given CA certificates)
					
					// step 1: get request token from Intuit
					$request_token = $oauth->getRequestToken( self::OAUTH_REQUEST_URL, self::get_oauth_callback_url() );
					self::set_oauth_token_secret( $request_token['oauth_token_secret'] );

					// step 2: send user to intuit to authorize 
					wp_redirect( add_query_arg( 'oauth_token', $request_token['oauth_token'], self::OAUTH_AUTHORISE_URL ) );
					exit;


				} catch(OAuthException $e) {
					echo "Got auth exception";
					echo '<pre>';
					print_r($e);
				}
				break;
			
			case 'handle_oauth':
				if ( isset( $_GET['oauth_token'] ) && isset( $_GET['oauth_verifier'] ) ) {
					$oauth = self::get_client();

					// step 3: request a access token from Intuit
					$oauth->setToken( $_GET['oauth_token'], self::get_oauth_token_secret() );
					$access_token = $oauth->getAccessToken( self::OAUTH_ACCESS_URL );
					$access_token['created_at'] = current_time( 'timestamp' );

					self::set_user_access_token( $access_token );
					// $_SESSION['token'] = serialize( $access_token );
					self::set_user_realm_id( $_REQUEST['realmId'] );
					
					wp_redirect( sc_get_permalink_by_template( 'template-studio.php' ) );
					exit;
				}
				
				break;

			case 'disconnect':
				self::revoke_token();
				self::delete_meta();
				break;

			default:
				# code...
				break;
		}

/*		if ( ! $query->get( 'sc_google_auth' ) ) {
			return;
		}*/

		if ( isset( $_GET['code'] ) ) {
			$client = self::get_client();
			$client->authenticate( $_GET['code'] );  
			$user_access_token = $client->getAccessToken();

			self::set_user_access_token( $user_access_token );
			if ( ! empty( $user_access_token['refresh_token'] ) ) {
				self::set_user_refresh_token( $user_access_token['refresh_token'] );
			}
		}

		wp_redirect( home_url( '/studio' ) );
		exit;
	}

	public static function revoke_token() {
		$client = self::get_client();
		$settings = self::get_current_context_settings();
		$serviceContext = ServiceContext::ConfigureFromPassedArray( $settings );
		$platformService = new PlatformService( $serviceContext );
		$platformService->Disconnect();
		self::set_user_access_token( '' );
		//self::set_user_refresh_token( '' );
	}

	public static function get_current_context_settings() {
		$user_access_token = self::get_user_access_token();
		$settings = array(
			'auth_mode'   =>       CoreConstants::OAUTH1,
			'consumerKey' =>       SC_QB_OAUTH_KEY,
			'consumerSecret' =>    SC_QB_OAUTH_SECRET,
			'accessTokenKey' =>    $user_access_token['oauth_token'],
			'accessTokenSecret' => $user_access_token['oauth_token_secret'],
			'QBORealmID' =>        self::get_user_realm_id(),
			'baseUrl' =>           SC_QB_BASEURL,
		);
		return $settings;
	}

	public static function render() {
		$user_access_token = self::get_user_access_token();
		
		if ( $user_access_token ) {
			echo '<p><a href="' . esc_url( self::get_disconnect_url() ) . '">Disconnect your QuickBooks Online account.</a></p>';
			//$data_service = self::get_data_service();
		} else {
			$authUrl = self::get_grant_url();
			echo '<a class="login" href="' . esc_url( $authUrl ) . '">Connect to QuickBooks Online</a>';
		}
	}

	public static function filter_react_proposal_data( $data, $event, $user_id ) {
		if ( self::get_user_access_token() ) {
			$data['extensions']['qbo'] = 1;
		}

		return $data;
	}

	public static function render_js() {
		?>
		<script type="text/javascript">
			(function($){
				$(document).ready(function($) {
					$('.google-calendar .user-calendars').on('change', function(event) {
						event.preventDefault();
						
						var url = stemcounter.aurl({ 'action': 'sc_edit_user_gcalendar' }),
							data = {
								value: event.target.value
							};

						$.post(url, data, function(response){
							stemcounter.JSONResponse(response, function(r){
								if ( ! r.success ) {
									console.log( r );
								} else {
									alertify.success( r.payload.message );
								}
							});
						});
					})
				});
			})(jQuery);
		</script>
		<?php	
	}

	public static function invoice_create( $event_id ) {

		$dataService = self::get_data_service();

		if ( WP_DEBUG ) {

			exec( 'rm /tmp/*Response-* /tmp/*Request-*' );
			$version = Event_Version::where( array(
				'event_id' => $event_id,
			) )->orderBy( 'id', 'desc' )->first();
			$event_data = json_decode( $version->event_data );

		} else {

			$event = sc_user_can_edit_event( $event_id );
			if ( !$event || !$event->latest_version ) {
				throw new Exception( 'You have to create a client version first.' );
			}
			$event_data = json_decode( $event->latest_version->event_data );
		}
		
		//$entities = $dataService->Query("SELECT * FROM TaxCode");
		//$entities = $dataService->Query("SELECT SalesTaxRateList FROM TaxCode WHERE Id = '31'");
		//$entities = $dataService->Query("SELECT * FROM TaxRate WHERE Name = 'Combined line 1'");
		
		/*$entities = $dataService->Query("SELECT * FROM TaxRate");
		error_log( var_export( $entities, true ) );*/
		// var_dump ( $event_data );
		//return;
		/*$event_data->eventCost->tax2 = 4;
		$event_data->eventCost->tax_rate_id2 = 280;
		$event_data->eventCost->tax_rate_value2 = 10;*/

		$qb_item_id = self::get_qb_item_id_from_meta();
		$customer = is_array( $event_data->customers ) ? $event_data->customers[0] : $event_data->customer;
		$qb_customer_id = self::get_qb_customer_id_from_meta( $customer );
		
		$invoice_content = array();


		//get taxcode id if needed
		if ( ! empty( floatval( $event_data->eventCost->tax ) ) ) {
			$qb_tax_id = self::get_qb_tax_code_id_from_meta( $event_data->eventCost );
		} else {
			$qb_tax_id = self::get_qb_exempt_code_id_from_meta();
		}

		//Add arrangements for caluclation
		foreach ( $event_data->event->arrangements as $arrangement ) {
			if ( empty( $arrangement->include ) || ! empty( $arrangement->addon ) ) {
				continue;
			}

			$items_cost = 0;
			foreach ( $arrangement->items as $item ) {
				$items_cost += sc_format_price( $item->quantity * $item->cost );
			}
			$items_cost = $arrangement->quantity * $items_cost;

			$line = new IPPLine();
			$line->DetailType = 'SalesItemLineDetail';
			$line->Type = 'Service';
			$line->Description = $arrangement->name;
			$line->taxable = true;
			$line->Amount = sc_format_price( $arrangement->subTotal );
				$silDetails = new IPPSalesItemLineDetail();
				$silDetails->ItemRef = $qb_item_id;
				$silDetails->UnitPrice = sc_format_price( $arrangement->subTotal / $arrangement->quantity );
				$silDetails->Qty = $arrangement->quantity; error_log( $arrangement->tax );

				if ( 'US' == self::get_country_from_meta() ) {
					$silDetails->TaxCodeRef = empty( $arrangement->tax ) ? '' : 'TAX';
				} else {
					$silDetails->TaxCodeRef = empty( $arrangement->tax ) ? self::get_qb_exempt_code_id_from_meta() : new IPPReferenceType (array ( 'value' => $qb_tax_id ));
				}
				
			$line->SalesItemLineDetail = $silDetails;
			$invoice_content[] = $line;
		}

		//Add delivery
		if ( ! empty( $event_data->eventCost->delivery ) ) {
			$line = new IPPLine();
			$line->DetailType = 'SalesItemLineDetail';
			$line->Type = 'Service';
			$line->Description = 'Delivery';
			$line->Amount = sc_format_price( $event_data->eventCost->delivery );
				$silDetails = new IPPSalesItemLineDetail();
				$silDetails->ItemRef = $qb_item_id;
				$silDetails->UnitPrice = $event_data->eventCost->delivery;
				$silDetails->Qty = 1;

				if ( 'US' == self::get_country_from_meta() ) {
					$silDetails->TaxCodeRef = empty( $arrangement->tax ) ? '' : 'TAX';
				} else {
					$silDetails->TaxCodeRef = new IPPReferenceType (array ( 'value' => $qb_tax_id ));
				}

				if ( 'percentage' == $event_data->eventCost->delivery_type ) {
					$line->Description = 'Delivery ' . $event_data->eventCost->delivery_value . '%';
				} else {
					$line->Description = 'Delivery';
				}

			$line->SalesItemLineDetail = $silDetails;
			$invoice_content[] = $line;
		}

		//Add discount
		if ( ! empty( floatval( $event_data->eventCost->discount ) ) ) {
			$line = new IPPLine();
			$line->DetailType = 'SalesItemLineDetail';
			$line->Type = 'Service';
			$line->Description = 'Discount';
			$line->taxable = true;
			$line->Amount = -sc_format_price( $event_data->eventCost->discount );
				$silDetails = new IPPSalesItemLineDetail();
				$silDetails->ItemRef = $qb_item_id;
				$silDetails->UnitPrice = -sc_format_price( $event_data->eventCost->discount );
				$silDetails->Qty = 1;

				if ( 'US' == self::get_country_from_meta() ) {
					$silDetails->TaxCodeRef = empty( $arrangement->tax ) ? '' : 'TAX';
				} else {
					$silDetails->TaxCodeRef = new IPPReferenceType (array ( 'value' => $qb_tax_id ));
				}
				//$silDetails->TaxCodeRef = 'TAX';

			/*$line->DetailType = 'DiscountLineDetail';
				$silDetails = new IPPDiscountLineDetail();
				if (false &&  'percentage' == $event_data->eventCost->discount_type ) {
					$silDetails->PercentBased = true;
					$silDetails->DiscountRef = true;
					$silDetails->DiscountPercent = $event_data->eventCost->discount_value;
				} else {
					$line->Amount = $event_data->eventCost->discount;
					$silDetails->PercentBased = false;
					$silDetails->DiscountRef = false;
				}
				$discount_account_id = self::get_qb_discount_account_id_from_meta();
				$silDetails->DiscountAccountRef = $discount_account_id;
			$line->DiscountLineDetail = $silDetails;*/

			$line->SalesItemLineDetail = $silDetails;
			$invoice_content[] = $line;
		}

		$invoice = new IPPInvoice();
		$invoice->CustomerRef = $qb_customer_id;
		$invoice->TxnDate = date('Y-m-d', time());
		$invoice->Line = $invoice_content;

		//Add tax
		if ( ! empty( floatval( $event_data->eventCost->tax ) ) ) {
			$tax_detail = new IPPTxnTaxDetail();
			//$qb_tax_id = self::get_qb_tax_code_id_from_meta( $event_data->eventCost );
			$tax_detail->TxnTaxCodeRef = $qb_tax_id;
			$invoice->TxnTaxDetail =  $tax_detail;
		}
		$invoice->ApplyTaxAfterDiscount = 'true';

		if ( 0 == count( $invoice_content ) ) {
			throw new Exception( 'Please, provide some invoice content' );
		}

		$dataService->useXml();
		$invoice = $dataService->Add( $invoice );

		if ( $invoice->DocNumber ) {
			self::update_sc_meta ( 'event', $event_data->event->id, 'qb_invoice_id', $invoice->DocNumber );
			return (int) $invoice->DocNumber;
		} else {
			return 0;
		}

	}

	/**
	 * Update stemcaounter specific table meta with cosrresponding data
	 *
	 * @param string Meta type
	 * @param int    Meta type id
	 * @param string Meta key
	 * @param string Meta New value of meta
	 * 
	 * @return New Meta value
	 */	

	private static function update_sc_meta ( $type, $type_id, $meta_key, $new_meta_value ) {
		$event_meta = Meta::firstOrNew([
			'type'     => $type,
			'type_id'  => $type_id,
			'meta_key' => $meta_key,
		]);
		$event_meta->meta_value = $new_meta_value;
		$event_meta->save();
	}

	/**
	 * Construct a name of one or more taxes 
	 *
	 * @param array The data for the taxes
	 * 
	 * @return string The name, which inclides names of all the taxes involved
	 */
	public static function sc_implode_tax_names( $taxes ) {
		$names = array();
		foreach( $taxes as $tax ) {
			$names[] = $tax['name'];
		}
	 	
		return implode( ' ', $names ) ;
	}


	/**
	 * Get array of taxes from the event data.
	 *
	 * @param object The stemcounter proposal event cost object
	 * 
	 * @return array The taxes data, taken from stemcounter
	 */

	public static function sc_convert_event_taxes( $eventCost ) {
		$taxes = array();
		if ( $eventCost->tax_rate_value ) {
			$taxes[] =  array( 
				"tax"           => $eventCost->tax,
				"tax_rate_id"   => $eventCost->tax_rate_id,
				"tax_rate_value"=> $eventCost->tax_rate_value,
			);
		}
		if ( $eventCost->tax_rate_value2 ) {
			$taxes[] =  array( 
				"tax"           => $eventCost->tax2,
				"tax_rate_id"   => $eventCost->tax_rate_id2,
				"tax_rate_value"=> $eventCost->tax_rate_value2,
			);
		}

		foreach ( $taxes as $key => $val ) {
			$sc_Tax_Rate = Tax_Rate::where(array(
				'id' => $val['tax_rate_id'],
			))->withTrashed()->first();
			$taxes[$key]["name"] = $sc_Tax_Rate->name;
			
		}
		return $taxes;
	}

	/**
	 * Gets the tax agency id from meta
	 *
	 * 
	 * @return int The tax id
	 */
	
	public static function get_qb_taxagency_id_from_meta () {
		if ( self::QB_ID_CACHE_ENABLED ) {
			$qb_taxagency_id = get_user_meta( get_current_user_id(), 'qb_taxagency_id' , true );
		}

		if ( empty( $qb_taxagency_id ) ) {
			$qb_taxagency =  self::get_qb_taxagency();
			if ( empty ( $qb_taxagency ) ) {
				throw new Exception( 'Can not get tax agency Id' );
			}
			$qb_taxagency_id = $qb_taxagency->Id;

			update_user_meta( get_current_user_id(), 'qb_taxagency_id', $qb_taxagency_id );
		}

		return $qb_taxagency_id;
	}

	/**
	 * Get appropriate tax agency from from quickbooks
	 * If there is no such tax agency, creates it in quickbooks.
	 *
	 * @return object The tax agency itself
	 */

	public static function get_qb_taxagency() {
		$dataService = self::get_data_service();
		$entities = $dataService->Query( "SELECT * FROM TaxAgency WHERE Name LIKE '" . self::SC_PREFIX . "%' " );

		if ( is_array( $entities ) && !empty( $entities[0] ) ) {
			return $entities[0];
		} else {
			$tax_agency = new IPPTaxAgency();
			$tax_agency->DisplayName = self::SC_PREFIX . 'agency';
			return $dataService->Add( $tax_agency );
		}
	}

	/**
	 * Gets the quickbooks tax code id for the given proposal cost.
	 *
	 * @param object The stemcounter proposal event cost object
	 * 
	 * @return int The tax code id
	 */
	
	public static function get_qb_tax_code_id_from_meta ( $eventCost ) {
		$taxes = self::sc_convert_event_taxes( $eventCost );
		$taxcode_name = self::sc_implode_tax_names( $taxes );
		$meta_key = '_sc_qbo_taxcode_' . preg_replace( '~\W~', '-', $taxcode_name);
		$taxcode_id = 0;

		if ( self::QB_ID_CACHE_ENABLED ) {
			$taxcode_id = get_user_meta( get_current_user_id(), $meta_key , true );
		}

		if ( ! $taxcode_id ) {
			$taxcode_id = self::get_qb_tax_code_id( $taxes );

			if ( !$taxcode_id ) {
				throw new Exception( 'Can not get tax code id' );
			}

			update_user_meta( get_current_user_id(), $meta_key, $taxcode_id );
		}

		return $taxcode_id;
	}

	/**
	 * Get the tax code id from quickbooks, which have the same data.
	 * If there is no such tax code, creates it in quickbooks. Tax codes can have multiple
	 * tax rates.
	 * 
	 * @param object The stemcounter proposal event cost object
	 * 
	 * @return object The tax object itself
	 */

	public static function get_qb_tax_code_id( $taxes ) {
		$dataService = self::get_data_service();
		$taxcode_name = self::sc_implode_tax_names( $taxes );

		//Find the same tax code ...
		$escaped_name = self::SC_PREFIX . addslashes( $taxcode_name );
		$entities = $dataService->Query( "SELECT * FROM TaxCode WHERE Name = '{$escaped_name}' " );
		if ( is_array( $entities ) && !empty( $entities ) ) {
			return( $entities[0]->Id );
		}

		//... otherwise create new tax code
		$taxrates = array();
		foreach ( $taxes as $taxrate ) {
			$taxrates[] = new IPPTaxRateDetails( array(
				'TaxRateId' => self::get_qb_tax_rate_id_from_meta( $taxrate ),
			) );
		}

		$tax_service = new IPPTaxService();
		$tax_service->TaxCode = self::SC_PREFIX . $taxcode_name;
		$tax_service->TaxRateDetails = $taxrates;
		// $dataService->useXml();
		$result = $dataService->Add( $tax_service );
		return $result->TaxService->TaxCodeId;
	}

	/**
	 * Gets the quickbooks tax rate id for the given proposal cost.
	 *
	 * @param object The stemcounter proposal event cost object
	 * 
	 * @return int The tax id
	 */
	
	public static function get_qb_tax_rate_id_from_meta ( $taxrate ) {
		if ( self::QB_ID_CACHE_ENABLED ) {
			$qb_taxrate = Meta::where(array(
					'type' => 'tax_rate',
					'type_id' => $taxrate['tax_rate_id'],
					'meta_key' => 'qb_tax_rate_id' 
				))->first();
		}

		if ( ! empty( $qb_taxrate ) ) {
			return $qb_taxrate->meta_value;
		}
		
		$qb_taxrate_id = self::get_qb_tax_rate_id ( $taxrate );

		if (  empty ( $qb_taxrate_id ) ) {
			throw new Exception( 'Can not get tax rate Id' );
		}

		self::update_sc_meta( 'tax_rate', $taxrate['tax_rate_id'], 'qb_tax_rate_id', $qb_taxrate_id );
		return( $qb_taxrate_id );
	}

	/**
	 * Gets tax rate from quickboox for the given stemcounter tax
	 *
	 * @param object The stemcounter tax associative array
	 * 
	 * @return objet The tax rate itself
	 */
	
	public static function get_qb_tax_rate_id ( $taxrate ) {
		$dataService = self::get_data_service();

		//Find the same tax rate
		$escaped_name = self::SC_PREFIX . addslashes( $taxrate['name'] );
		$entities = $dataService->Query("SELECT * FROM TaxRate WHERE Name = '{$escaped_name}'");
		if ( is_array( $entities ) && !empty( $entities[0] ) ) {
			return $entities[0]->Id;
		} 

		$qb_taxagency_id = self::get_qb_taxagency_id_from_meta();

		//Create new tax rate, if there is no any with same name
		$tax_rate = new IPPTaxRateDetails();
		$tax_rate->TaxRateName 		= self::SC_PREFIX . $taxrate['name'];
		$tax_rate->RateValue 		= $taxrate['tax_rate_value'];
		$tax_rate->TaxAgencyId		= $qb_taxagency_id;
		$tax_rate->TaxApplicableOn = "Sales";

		$tax_service = new IPPTaxService();
		$tax_service->TaxCode = self::SC_PREFIX . $taxrate['name'];
		$tax_service->TaxRateDetails = array( $tax_rate );

		// $dataService->useXml();
		$result = $dataService->Add( $tax_service );
		return ( $result->TaxService->TaxRateDetails[0]->TaxRateId );
	}

	/**
	 * Gets the quickbooks customer id for the proposal.
	 *
	 * @param object The stemcounter customer
	 * 
	 * @return int The customer id
	 */
	
	public static function get_qb_customer_id_from_meta ( $sc_customer ) {
		if ( self::QB_ID_CACHE_ENABLED ) {
			$qb_customer_meta = Meta::where(array(
					'type' => 'customer',
					'type_id' => $sc_customer->id,
					'meta_key' => 'qb_customer_id' 
				))->first();
		}

		if ( ! empty( $qb_customer_meta ) ) {
			return $qb_customer_meta->meta_value;
		}
		
		$qb_customer = self::get_qb_customer ( $sc_customer );

		if ( !$qb_customer ) {
			throw new Exception( 'Can not get customer Id' );
		}

		self::update_sc_meta ( 'customer', $sc_customer->id, 'qb_customer_id', $qb_customer->Id );
		return( $qb_customer->Id ) ;
	}

	/**
	 * Get the customer from quickbooks, who have the same data.
	 * If there is no such customer, creates it in quickbooks.
	 *
	 * @param object The stemcounter customer
	 * 
	 * @return object The customer object itself
	 */

	public static function get_qb_customer( $sc_customer ) {
		$dataService = self::get_data_service();
		
		//Find the same customer ...
		$escaped_name = addslashes( $sc_customer->name );
		$entities = $dataService->Query("SELECT * FROM Customer WHERE DisplayName = '{$escaped_name}' ");
		if ( is_array( $entities ) && !empty( $entities[0] ) ) {
			return( $entities[0] );
		}
		
		//... otherwise create new customer
		$customer = new IPPCustomer();
		$customer->RefNum 			= $sc_customer->id;

		$customer->DisplayName 		= $sc_customer->name;
		$customer->ContactName		= $sc_customer->name;
		$customer->PrimaryPhone 	= $sc_customer->phone;
		$customer->PrimaryEmailAddr = $sc_customer->email;

		$address = new IPPPhysicalAddress();
		$address->Line1 = $sc_customer->address;

		$customer->BillAddr = $address;
		$customer->ShipAddr = $address; 

		return $dataService->Add( $customer );
	}


	/**
	 * Gets the exempt tax code id for quickboox.
	 *
	 * @return int The tax code id
	 */
	
	public static function get_qb_exempt_code_id_from_meta () {
		$taxcode_id = 0;

		if ( self::QB_ID_CACHE_ENABLED ) {
			$taxcode_id = get_user_meta( get_current_user_id(), 'qb_sc_exempt_code_id', true );
		}

		if ( ! $taxcode_id ) {
			$taxcode_id = self::get_exempt_code_id();

			if ( !$taxcode_id ) {
				throw new Exception( 'Can not get exempt code id' );
			}

			update_user_meta( get_current_user_id(), 'qb_sc_exempt_code_id', $taxcode_id );
		}

		return $taxcode_id;
	}

	/**
	 * Get the  exempt tax code id from quickbooks.
	 * If there is no such tax code, creates it in quickbooks.
	 * 
	 * @return integer The exempt tax object id
	 */

	public static function get_exempt_code_id() {
		$dataService = self::get_data_service();

		//Find the same tax code ...
		$taxcode_name = self::SC_PREFIX . 'Exempt';
		//$entities = $dataService->Query( "SELECT * FROM TaxCode WHERE Description = '{$taxcode_name}' " );
		$entities = $dataService->Query( "SELECT * FROM TaxCode WHERE Name = '{$taxcode_name}' " );
		if ( is_array( $entities ) && !empty( $entities ) ) {
			return( $entities[0]->Id );
		}

		//... otherwise create new tax code
		$taxrates = array(
			new IPPTaxRateDetails( array(
				'TaxRateId' => self::get_qb_exempt_rate_id_from_meta(),
			) )
		);

		$tax_service = new IPPTaxService();
		$tax_service->TaxCode = $taxcode_name;
		$tax_service->TaxRateDetails = $taxrates;
		// $dataService->useXml();
		$result = $dataService->Add( $tax_service );
		return (int) $result->TaxService->TaxCodeId;
	}

	/**
	 * Read the exempt tax rate id for quickbooks (international companies). If it is empty, tries to read it from quickbooks.
	 *
	 * @return int The exempt tax rate id in quickbooks
	 */

	public static function get_qb_exempt_rate_id_from_meta() {
		if ( self::QB_ID_CACHE_ENABLED ) {
			$exempt_id = get_user_meta( get_current_user_id(), 'qb_sc_exempt_rate_id', true );
		}
		if ( !$exempt_id ) {
			$exempt_id = self::get_exempt_rate_id( );

			if ( !$exempt_id ) {
				throw new Exception( 'Can not get exempt rate Id' );
			}

			update_user_meta( get_current_user_id(), 'qb_sc_exempt_rate_id', $exempt_id );
		}
		return (int) $exempt_id;
	}


	/**
	 * Gets exempt tax rate from quickboox for the given stemcounter tax
	 *
	 * 
	 * @return objet The tax rate itself
	 */
	
	public static function get_exempt_rate_id (  ) {
		$dataService = self::get_data_service();

		//Find the same tax rate
		$tax_name = self::SC_PREFIX . 'Exempt';
		$entities = $dataService->Query("SELECT * FROM TaxRate WHERE Name = '{$tax_name}'");
		if ( is_array( $entities ) && !empty( $entities[0] ) ) {
			return $entities[0]->Id;
		} 

		$qb_taxagency_id = self::get_qb_taxagency_id_from_meta();

		//Create new tax rate, if there is no any with same name
		$tax_rate = new IPPTaxRateDetails();
		$tax_rate->TaxRateName 		= $tax_name;
		$tax_rate->RateValue 		= 0;
		$tax_rate->TaxAgencyId		= $qb_taxagency_id;
		$tax_rate->TaxApplicableOn = "Sales";

		$tax_service = new IPPTaxService();
		$tax_service->TaxCode = $tax_name;
		$tax_service->TaxRateDetails = array( $tax_rate );

		// $dataService->useXml();
		$result = $dataService->Add( $tax_service );
		return ( $result->TaxService->TaxRateDetails[0]->TaxRateId );
	}

	/**
	 * Read the discount account id for quickbooks. If it is empty, tries to read it from quickbooks.
	 *
	 * @return int The discount account id in quickbooks
	 */

	public static function get_qb_discount_account_id_from_meta() {
		if ( self::QB_ID_CACHE_ENABLED ) {
			$discount_account_id = get_user_meta( get_current_user_id(), 'qb_sc_discount_account_id', true );
		}
		if ( !$discount_account_id ) {
			$discount_account = self::get_qb_account( self::SC_PREFIX . 'Discounts Given' );
			$discount_account_id = $discount_account->Id;

			if ( !$discount_account_id ) {
				throw new Exception( 'Can not get discount acount Id' );
			}

			update_user_meta( get_current_user_id(), 'qb_sc_discount_account_id', $discount_account_id );
		}
		return (int) $discount_account_id;
	}

	/**
	 * Read the sc_item's id for quickbooks. If it is empty, tries to read it from quickbooks.
	 *
	 * @return int The sc_item id in quickbooks
	 */

	public static function get_qb_item_id_from_meta() {
		if ( self::QB_ID_CACHE_ENABLED ) {
			$item_id = get_user_meta( get_current_user_id(), 'qb_sc_item_id', true );
		}
		if ( empty( $item_id ) ) {
			$qb_item = self::get_qb_item();
			$item_id = $qb_item->Id;

			if ( !$item_id ) {
				throw new Exception( 'Can not get item Id' );
			}

			update_user_meta( get_current_user_id(), 'qb_sc_item_id', $item_id );
		}
		return (int) $item_id;
	}

	/**
	 * Get the sc_item from quickbooks.
	 * If there is no such item, creates it in quickbooks.
	 *
	 * @return object The item object itself
	 */

	public static function get_qb_item() {
		$dataService = self::get_data_service();

		//Get current sc item ...
		$sc_item_name = self::SC_PREFIX . 'Item';
		$entities = $dataService->Query("SELECT * FROM Item WHERE Name = '{$sc_item_name}' ");

		if ( is_array( $entities ) && !empty( $entities[0] ) ) {
			return( $entities[0] );
		}

		//... otherwise create new item
		$qb_account = self::get_qb_account( self::SC_PREFIX . 'Income' );
		if ( !$qb_account ) {
			throw new Exception( 'Can not get item account Id' );
		}
		$item = new IPPItem();
		$item->Name 			= $sc_item_name;
		$item->IncomeAccountRef = $qb_account->Id;
		$item->Type             = 'Service';
		return $dataService->Add( $item );
	}

	/**
	 * Get existing account from quickbooks.
	 * If there is no such account, creates it in quickbooks.
	 *
	 * @param string Name of the acount to be created
	 * 
	 * @return object The account object itself
	 */

	public static function get_qb_account( $AcountName ) {
		$dataService = self::get_data_service();

		//Get current income account ...
		$entities = $dataService->Query("SELECT * FROM Account WHERE Name = '{$AcountName}' ");
		if ( is_array( $entities ) && !empty( $entities[0] ) ) {
			return( $entities[0] );
		}
	
		//... otherwise create new income account
		$account = new IPPAccount();
		$account->Name 			= $AcountName;
		$account->SubAccount 	= 'false';
		$account->Active 		= 'true';
		$account->AccountType 	= 'Income';
		return ( $dataService->Add( $account ) );
	}

	/**
	 * Read the country of the connected company
	 *
	 * @return string The country of the company (for example US)
	 */

	public static function get_country_from_meta() {
		if ( self::QB_ID_CACHE_ENABLED ) {
			$country = get_user_meta( get_current_user_id(), 'qb_sc_country', true );
		}
		if ( empty( $country ) ) {
			$qb_company = self::get_qb_company();
			if ( empty( $qb_company ) ) {
				throw new Exception( 'Can not get company info' );
			}
			$country = $qb_company->Country;

			update_user_meta( get_current_user_id(), 'qb_sc_country', $country );
		}
		return $country;
	}

	/**
	 * Get the connected company info
	 *
	 * @return object The company info object itself
	 */

	public static function get_qb_company( ) {
		$dataService = self::get_data_service();
		$entities = $dataService->Query('SELECT * FROM CompanyInfo ');
		if ( is_array( $entities ) && !empty( $entities[0] ) ) {
			return( $entities[0] );
		}
		return false;
	}

	/**
	 * Deletes all cached meta in stemcounter database from quickbooks for this user
	 *
	 * @return void
	 */
	
	public static function delete_meta() {
		global $wpdb;

		$wpdb->query(
			$wpdb->prepare(
				'DELETE FROM ' . $wpdb->prefix . "sc_meta WHERE meta_key = %s AND type_id IN ( SELECT id FROM " .$wpdb->prefix . 'sc_events WHERE user_id = %d )',  'qb_invoice_id', get_current_user_id()
			) 
		);

		$wpdb->query(
			$wpdb->prepare(
				'DELETE FROM ' . $wpdb->prefix . "sc_meta WHERE meta_key = %s AND type_id IN ( SELECT id FROM " .$wpdb->prefix . 'sc_customers WHERE user_id = %d )',  'qb_customer_id', get_current_user_id()
			) 
		);

		$wpdb->query(
			$wpdb->prepare(
				'DELETE FROM ' . $wpdb->prefix . "sc_meta WHERE meta_key = %s AND type_id IN ( SELECT id FROM " .$wpdb->prefix . 'sc_tax_rates WHERE user_id = %d )',  'qb_tax_rate_id', get_current_user_id()
			) 
		);

		$wpdb->query(
			$wpdb->prepare( 'DELETE FROM ' . $wpdb->usermeta . ' WHERE meta_key LIKE "_sc_qbo_taxcode_%%" AND user_id = %d',  get_current_user_id())
		);

		delete_user_meta( get_current_user_id(), '_sc_qbo_access_token' );
		delete_user_meta( get_current_user_id(), '_sc_qbo_realm_id' );
		delete_user_meta( get_current_user_id(), 'qb_taxagency_id' );
		delete_user_meta( get_current_user_id(), 'qb_sc_discount_account_id' );
		delete_user_meta( get_current_user_id(), 'qb_sc_item_id' );
		delete_user_meta( get_current_user_id(), 'qb_sc_country' );
		delete_user_meta( get_current_user_id(), 'qb_sc_exempt_rate_id' );
		delete_user_meta( get_current_user_id(), 'qb_sc_exempt_code_id' );
	}


	/**
	 * Syncronize an invoice with quickbooks account va ajax request
	 *
	 * @return void
	 */
	
	public static function sc_studio_sync_invoice() {
		$request = Request::capture();
		$r = new Ajax_Response();

		$sc_id = (int) $request->input('id');
		try {
			$qb_id = self::invoice_create( $sc_id );
		} catch (Exception $e) {
			$r->fail( 'Failed to send invoice: ' . $e->getMessage() );
		}

		if ( 0 < $qb_id ) {
			$r->add_payload( 'message', 'Invoice added. Quickbooks document number: ' . $qb_id );
			$r->respond();
		} else if (0 === $qb_id) {
			$r->add_payload( 'message', 'No invoice number returned. There was error, or autonumbering in quickbooks is not enabled' . $qb_id );
			$r->respond();
		} else {
			$r->fail( 'Failed to send invoice' );
		}
	}

	/**
	 * Check if a user is connected to quickbooks or not
	 *
	 * @return boolean
	 */
	
	public static function is_connected() {
		return ( ! empty( get_user_meta( get_current_user_id(), '_sc_qbo_access_token', true ) ) );
	}

}


if (
	defined( 'SC_QB_APP_KEY' ) && SC_QB_APP_KEY &&
	defined( 'SC_QB_OAUTH_KEY' ) && SC_QB_OAUTH_KEY &&
	defined( 'SC_QB_OAUTH_SECRET' ) && SC_QB_OAUTH_SECRET &&
	defined( 'SC_QB_BASEURL' ) && SC_QB_BASEURL ) {
	SC_Quickbooks_Online::start();
}

