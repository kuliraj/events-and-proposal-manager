<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Event;
use Stemcounter\Service;

if (!Capsule::schema()->hasTable('events')) {
	Capsule::schema()->create('events', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->integer('customer_id')
			->unsigned();
		$table->integer('vendor_id')
			->unsigned();
		$table->integer('tax_rate_id')
			->unsigned()
			->nullable();
		$table->float('tax_rate_value', 8,3)
			->default(0);
		$table->integer('tax_rate_id2')
			->unsigned()
			->nullable();
		$table->float('tax_rate_value2', 8,3);
		$table->string('name');
		$table->enum('status', Event::$statuses)
			->default(Event::$statuses[0]);
		$table->date('date');
		$table->text('private_note');
		$table->text('event_note');
		$table->text('contract_note');
		$table->float('delivery', 6, 2)
			->default( 0 );
		$table->string('delivery_id');
		$table->text('delivery_type');
		$table->integer('delivery_tax')
			->unsigned()
			->default(1);
		$table->float('card', 6, 2)
			->default( 0 );
		$table->string( 'state' )
			->default('inquiry');
		$table->integer('discount_id')
			->unsigned()
			->nullable();
		$table->string('discount_name');
		$table->float('discount_value');
		$table->text('discount_type');
		$table->integer('separate_cc_fee')
			->unsigned()
			->default(0);
		$table->integer('visible_payments')
			->unsigned()
			->default(1);
		$table->integer('hidden_contract')
			->unsigned()
			->default(0);
		$table->integer('hide_item_prices')
			->unsigned()
			->default(0);
		$table->softDeletes();
		$table->timestamps();
		$table->index( array('user_id' ), 'user_id_index' );
	});
}

if (!Capsule::schema()->hasTable('customers')) {
	Capsule::schema()->create('customers', function($table){
		$table->increments('id');
		$table->string('first_name');
		$table->string('last_name');
		$table->string('phone')
			->default('');
		$table->string('email')
			->default('');
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('vendors')) {
	Capsule::schema()->create('vendors', function($table){
		$table->increments('id');
		$table->string('name');
		$table->string('address')
			->default('');
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('arrangements')) {
	Capsule::schema()->create('arrangements', function($table){
		$table->increments('id');
		$table->integer('event_id')
			->unsigned();
		$table->integer('user_id')
			->unsigned();
		$table->string('name');
		$table->integer('quantity')
			->unsigned()
			->default(1);
		$table->integer('include')
			->unsigned()
			->default(1);
		$table->float('override_cost')
			->default(NULL)
			->nullable();
		$table->integer('order')
			->unsigned();
		$table->integer('tax')
			->unsigned()
			->default(1)
			->nullable();
		/*$table->integer('tax2')
			->unsigned()
			->default(0)
			->nullable();*/
		$table->text('note');
		$table->integer('addon')
			->unsigned()
			->default(0);
		$table->softDeletes();
		$table->timestamps();
		$table->index( array( 'event_id', 'user_id' ), 'event_id_user_id_index' );
		$table->boolean('is_applied_all')
			->default(0);
	});
}

if (!Capsule::schema()->hasTable('arrangement_items')) {
	Capsule::schema()->create('arrangement_items', function($table){
		$table->increments('id');
		$table->integer('arrangement_id')
			->unsigned();
		$table->string('item_type');
		$table->integer('item_id')
			->unsigned();
		$table->integer('item_variation_id')
			->unsigned()
			->default(0);
		$table->float('cost');
		$table->float('quantity');
		$table->integer('arrangement_recipe_id')
			->unsigned();
		$table->softDeletes();
		$table->timestamps();
		$table->index( array('arrangement_id' ), 'arrangement_id_index' );
	});
}

if (!Capsule::schema()->hasTable('arrangement_photos')) {
	Capsule::schema()->create('arrangement_photos', function($table){
		$table->increments('id');
		$table->integer('arrangement_id')
			->unsigned();
		$table->integer('photo_id')
			->unsigned();
		$table->integer('modal_position')
			->unsigned()
			->default(0);
		$table->softDeletes();
		$table->timestamps();
		$table->index( array('arrangement_id' ), 'arrangement_id_index' );
	});
}

if (!Capsule::schema()->hasTable('flower_archetypes')) {
	Capsule::schema()->create('flower_archetypes', function($table){
		$table->increments('id');
		$table->string('name');
		$table->float('cost');
		$table->integer('stems')
			->unsigned();
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('variations')) {
	Capsule::schema()->create('variations', function($table){
		$table->increments('id');
		$table->string('name');
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('flower_archetype_variations')) {
	Capsule::schema()->create('flower_archetype_variations', function($table){
		$table->integer('archetype_id')
			->unsigned();
		$table->integer('variation_id')
			->unsigned();
		$table->primary(array('archetype_id', 'variation_id'));
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('items')) {
	Capsule::schema()->create('items', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('name');
		$table->text('description');
		$table->string('type');
		$table->string('pricing_category');
		$table->float('purchase_qty');
		$table->string('purchase_unit');
		$table->string('recipe_unit');
		$table->softDeletes();
		$table->nullableTimestamps();
		$table->index( array('user_id' ), 'user_id_index' );
	});
}

if (!Capsule::schema()->hasTable('item_variations')) {
	Capsule::schema()->create('item_variations', function($table){
		$table->increments('id');
		$table->integer('item_id')
			->unsigned();
		$table->string('name');
		$table->integer('attachment_id')
			->unsigned()
			->nullable();
		$table->float('cost_pu')
			->nullable()
			->unsigned();
		$table->float('inventory')
			->nullable();
		$table->softDeletes();
		$table->nullableTimestamps();
		$table->index( array( 'item_id' ), 'item_id_index' );
	});
}

if (!Capsule::schema()->hasTable('migrated_item_ids')) {
	Capsule::schema()->create('migrated_item_ids', function($table){
		$table->increments('id');
		$table->integer('item_id')
			->unsigned();
		$table->string('type');
		$table->integer('variation_id')
			->nullable();
		$table->integer('new_item_id')
			->unsigned();

		$table->index( array( 'type', 'item_id', 'variation_id' ), 'type_iid_vid' );
	});
}

if (!Capsule::schema()->hasTable('flowers')) {
	Capsule::schema()->create('flowers', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->integer('archetype_id')
			->unsigned()
			->nullable()
			->default(null);
		$table->string('name');
		$table->float('cost');
		$table->integer('stems')
			->unsigned();
		$table->integer('attachment_id')
			->unsigned();
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('flower_variations')) {
	Capsule::schema()->create('flower_variations', function($table){
		$table->integer('flower_id')
			->unsigned();
		$table->integer('variation_id')
			->unsigned();
		$table->primary(array('flower_id', 'variation_id'));
	});
}

if (!Capsule::schema()->hasTable('services')) {
	Capsule::schema()->create('services', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('name');
		$table->text('description');
		$table->enum('type', Service::$types)
			->default(Service::$types[0]);
		$table->float('cost');
		$table->integer('inventory')
			->unsigned()
			->default(0);
		$table->integer('attachment_id')
			->unsigned();
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('tax_rates')) {
	Capsule::schema()->create('tax_rates', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('name');
		$table->float('value');
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('discounts')) {
	Capsule::schema()->create('discounts', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('name');
		$table->float('value');
		$table->text('type');
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('invoice_categories')) {
	Capsule::schema()->create('invoice_categories', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('name');
		$table->float('hardgood_multiple')
			->nullable()
			->default(null);
		$table->float('fresh_flower_multiple')
			->nullable()
			->default(null);
		$table->float('global_labor_value')
			->nullable()
			->default(null);
		$table->float('flower_labor')
			->nullable()
			->default(null);
		$table->float('hardgood_labor')
			->nullable()
			->default(null);
		$table->float('base_price_labor')
			->nullable()
			->default(null);
		$table->float('fee_labor')
			->nullable()
			->default(null);
		$table->float('rental_labor')
			->nullable()
			->default(null);
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('scheduled_payments')) {
	Capsule::schema()->create('scheduled_payments', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->integer('event_id')
			->unsigned();
		$table->string('payment_name');
		$table->float('payment_amount')
			->nullable()
			->default(null);
		$table->string('payment_amount_type');
		$table->date('payment_date');
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('payments_made')) {
	Capsule::schema()->create('payments_made', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->integer('event_id')
			->unsigned();
		$table->float('payment_amount')
			->nullable()
			->default(null);
		$table->string('payment_type');
		$table->date('payment_date');
		$table->text('payment_note');
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('orders')) {
	Capsule::schema()->create('orders', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->text('event_ids');
		$table->string('name');
		$table->text('note');
		$table->string('status');
		$table->text('flowers_items');
		$table->float('estimate');
		$table->date('fulfilment_date')
			->nullable()
			->default( null );
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('order_items')) {
	Capsule::schema()->create('order_items', function($table){
		$table->increments('id');
		$table->integer('order_id')
			->unsigned();
		$table->integer('event_id')
			->unsigned();
		$table->integer('item_id')
			->unsigned()->nullable();
		$table->string('name');
		$table->string('pricing_category');
		$table->float('quantity');
		$table->float('cost');
		$table->float('items_su');
		$table->integer('su_needed');
		$table->integer('leftovers');
		$table->tinyInteger('discarded')
			->default(0);
		$table->softDeletes();
		$table->nullableTimestamps();
	});
}

if (!Capsule::schema()->hasTable('meta')) {
	Capsule::schema()->create('meta', function($table){
		$table->bigIncrements('id');
		$table->string('type');
		$table->bigInteger('type_id')
			->unsigned();
		$table->string('meta_key');
		$table->longText('meta_value');
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('deliveries')) {
	Capsule::schema()->create('deliveries', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('name');
		$table->float('value');
		$table->text('type');
		$table->integer('tax')
			->unsigned()
			->default(1);
		$table->integer('default')
			->default(0);
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('event_versions')) {
	Capsule::schema()->create('event_versions', function($table){
		$table->increments('id');
		$table->integer('event_id')
			->unsigned();
		$table->integer('version')
			->unsigned();
		$table->longText('event_data');
		$table->tinyInteger('client_signed')->default(0);
		$table->softDeletes();
		$table->timestamps();
		$table->index( array( 'event_id' ), 'event_id_index' );
	});
}

if (!Capsule::schema()->hasTable('payments')) {
	Capsule::schema()->create('payments', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->integer('event_id')
			->unsigned();
		$table->string('payment_name');
		$table->date('due_date');
		$table->float('card_fee')
			->default(0);
		$table->float('payment_percentage')
			->nullable()
			->default(null);
		$table->dateTime('payment_date')
			->nullable()
			->default(null);
		$table->float('amount')
			->nullable()
			->default(null);
		$table->float('convenience_fee')
			->nullable()
			->default(null);
		$table->text('payment_note');
		$table->string('status');
		$table->string('payment_type');
		$table->integer('is_live')
			->unsigned()
			->default(0);
		$table->timestamps();
		$table->index( array( 'event_id' ), 'event_id_index' );
		$table->index( array( 'user_id' ), 'user_id_index' );
	});
}

if ( ! Capsule::schema()->hasTable( 'arrangement_recipes' ) ) {
	Capsule::schema()->create('arrangement_recipes', function($table){
		$table->increments('id');
		$table->integer('arrangement_id')
			->unsigned();
		$table->string('name');
		$table->integer('user_recipe_id')
			->unsigned()
			->default(0);
		$table->timestamps();
		$table->index( array( 'arrangement_id' ), 'arrangement_id_index' );
	});
}

if ( ! Capsule::schema()->hasTable( 'notifications' ) ) {
	Capsule::schema()->create('notifications', function($table) {
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->text('notification');
		$table->string('type');
		$table->string('subject');
		$table->integer('read')
			->default(0);
		$table->integer('email_sent')
			->default(0);
		$table->softDeletes();
		$table->timestamps();
	});
}

if ( ! Capsule::schema()->hasTable( 'recipes' ) ) {
	Capsule::schema()->create('recipes', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('name');
		$table->text('description');
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('recipe_items')) {
	Capsule::schema()->create('recipe_items', function($table){
		$table->increments('id');
		$table->integer('recipe_id')
			->unsigned();
		$table->string('item_type');
		$table->integer('item_id')
			->unsigned();
		$table->integer('item_variation_id')
			->unsigned()
			->default(0);
		$table->float('quantity');
		$table->softDeletes();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('forms')) {
	Capsule::schema()->create('forms', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('name');
		$table->text('confirmation_msg');
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('form_submissions')) {
	Capsule::schema()->create('form_submissions', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->integer('form_id')
			->unsigned();
		$table->string('from_ip');
		$table->integer('seen')->default(0);
		$table->integer('archived')
			->default(0);
		$table->integer('event_id')
			->unsigned()
			->nullable();
		$table->integer('customer_id')
			->unsigned()
			->nullable();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('form_submission_answers')) {
	Capsule::schema()->create('form_submission_answers', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->integer('form_id')
			->unsigned();
		$table->integer('submission_id')
			->unsigned();
		$table->integer('question_id')
			->unsigned();
		$table->integer('property_id')
			->unsigned();
		$table->text('answer')
			->nullable();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('field_properties')) {
	Capsule::schema()->create('field_properties', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('label');
		$table->string('placeholder_tag');
		$table->string('field_type');
		$table->string('property_type');
		$table->string('internal_field')->nullable();
		$table->longText('settings');
		$table->integer('deleted')
			->default(0);
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('form_questions')) {
	Capsule::schema()->create('form_questions', function($table){
		$table->increments('id');
		$table->integer('form_id')
			->unsigned();
		$table->integer('property_id')
			->unsigned();
		$table->string('label');
		$table->string('type');
		$table->integer('required')
			->default(0);
		$table->integer('order')
			->unsigned();
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('customer_properties')) {
	Capsule::schema()->create('customer_properties', function($table){
		$table->increments('id');
		$table->integer('customer_id')
			->unsigned();
		$table->integer('property_id')
			->unsigned();
		$table->string('value');
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('event_properties')) {
	Capsule::schema()->create('event_properties', function($table){
		$table->increments('id');
		$table->integer('event_id')
			->unsigned();
		$table->integer('property_id')
			->unsigned();
		$table->string('value');
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('email_templates')) {
	Capsule::schema()->create('email_templates', function($table){
		$table->increments('id');
		$table->integer('user_id')
			->unsigned();
		$table->string('label');
		$table->string('subject');
		$table->longText('content');
		$table->integer('default')
			->default(0);
		$table->timestamps();
	});
}

if (!Capsule::schema()->hasTable('measuring_units')) {
	Capsule::schema()->create('measuring_units', function($table){
		$table->string('id');
		$table->string('singular');
		$table->string('plural');
		$table->string('short');
		$table->string('short_plural');
		$table->primary('id');
	});

	global $wpdb;
	$wpdb->insert(
		$wpdb->prefix . 'sc_measuring_units',
		array(
			'id'			=> 'item',
			'singular'		=> 'Item',
			'plural'		=> 'Items',
			'short'			=> 'item',
			'short_plural'	=> 'items',
		)
	);
	$wpdb->insert(
		$wpdb->prefix . 'sc_measuring_units',
		array(
			'id'			=> 'lft',
			'singular'		=> 'Linear Foot',
			'plural'		=> 'Linear Feet',
			'short'			=> 'lft.',
			'short_plural'	=> 'lft.',
		)
	);
	$wpdb->insert(
		$wpdb->prefix . 'sc_measuring_units',
		array(
			'id'			=> 'oz',
			'singular'		=> 'Ounce',
			'plural'		=> 'Onuces',
			'short'			=> 'oz.',
			'short_plural'	=> 'oz.',
		)
	);
}