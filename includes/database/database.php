<?php
global $wpdb;
define('SC_PREFIX', $wpdb->prefix . 'sc_');

global $illuminate_capsule;
use Illuminate\Database\Capsule\Manager as Capsule;

$illuminate_capsule = new Capsule;

$illuminate_capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => DB_HOST,
    'database'  => DB_NAME,
    'username'  => DB_USER,
    'password'  => DB_PASSWORD,
    'charset'   => (DB_CHARSET ? DB_CHARSET : 'utf8'),
    'collation' => (DB_COLLATE ? DB_COLLATE : 'utf8_general_ci'),
    'prefix'    => SC_PREFIX,
]);

// Set the event dispatcher used by Eloquent models... (optional)
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
$illuminate_capsule->setEventDispatcher(new Dispatcher(new Container));

// Make this Capsule instance available globally via static methods... (optional)
$illuminate_capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$illuminate_capsule->bootEloquent();

if ( WP_DEBUG ) {  //$_GET['debug_queries'] = 1;
    // This can be useful for query debugging
    if (isset( $_GET['debug_queries'] ) ) {
        global $laravel_queries;
    	$illuminate_capsule->connection()->enableQueryLog();
        $illuminate_capsule->connection()->listen(function( $_query ) {
            global $laravel_queries; 
            $query = $_query->sql;
            foreach ( $_query->bindings as $binding ) {
                if ( is_a( $binding, 'Carbon\Carbon' ) ) {
                    $binding = (string) $binding;
                }

                $query = substr_replace( $query, ( gettype( $binding ) == 'string' ? '\'' . $binding . '\'' : $binding ), strpos( $query, '?' ), 1 );
            }
            
            error_log( $query );
            $laravel_queries[] = array( $query, $_query->time );
            //error_log( var_export( $laravel_queries, true ) );
            // error_log($_query->time . "\t" . $query );
            
        });

        function sc_print_sorted_laravel_queries() {
            global $laravel_queries;

            var_dump( $laravel_queries );
        }
        //add_action( 'wp_footer', 'sc_print_sorted_laravel_queries', 10 );
    }
}

require_once('hooks.php');
require_once('schema.php');