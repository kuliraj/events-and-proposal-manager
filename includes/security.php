<?php
use Stemcounter\Event;

function sc_redirect_to_logon() {
	if (!is_user_logged_in()) {
		
		if (!is_page('logon') &&
			!is_page('register') &&
			!is_page('terms') &&
			!is_page('faq') &&
			!is_page_template( 'template-public.php' ) && 
			!is_page_template( 'template-join.php' ) && 
			!is_home() &&
			!is_single() &&
			!is_front_page() &&
			!is_search() &&
			!is_404() &&
			!is_archive() &&
	        !is_page_template('template-pricing.php') &&
	        !is_page_template('template-membership.php')) {
	    		wp_redirect(add_query_arg(array('redirect_to' => set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] )), home_url('logon/')));
			 	 exit;
		}
	}
}
add_action('template_redirect', 'sc_redirect_to_logon');

function sc_event_ownership_checkpoint($event_id, $user_id, $redirect=true, $page_url = '') {
	if (isset($_GET['eid']) && $_GET['eid'] == 'lastevent') {

		try {
			$last_event = Event::where(array(
				'user_id'=>$user_id,
				'deleted_at'=>NULL
			))->orderBy('created_at', 'DESC')->firstOrFail();

			wp_redirect(add_query_arg( array( 'eid' => $last_event->id ), $page_url));
			exit;
			
		} catch (Exception $e) {
			wp_redirect(sc_get_events_page_url());
			exit;
		}
	}

	// check if event exists and user ownership of the event
	try {
		$event_args = array(
			'id' => $event_id,
		);

		if ( ! is_super_admin() ) {
			$event_args['user_id'] = $user_id;
		}

		$event = Event::where( $event_args )->firstOrFail();
	} catch (Exception $e) {
		if ($redirect) {
			wp_redirect(sc_get_events_page_url());
			exit;
		} else {
			// rethrow for outside caching
			throw $e;
		}
	}

	return $event;
}

/**
 * Checks whether the current user can edit an event
 * 
 * @return Event The Event object on success. Throws an Exception otherwise
 */
function sc_user_can_edit_event( $event_id, $user_id = false ) {
	$event_args = array(
		'id' => $event_id,
	);

	$user_id = $user_id ? $user_id : get_current_user_id();

	if ( ! is_super_admin( $user_id ) ) {
		$event_args['user_id'] = $user_id;
	}

	$can_edit = false;

	$event = Event::where( $event_args )->firstOrFail();

	return $event;
}

/**
 * StemCounter Access Control
 * 
 * Granular access control around features
 */
class SCAC {
	const TAGGING = 'sc_tagging';
	const STUDIO = 'sc_studio';
	public static $features = array();

	public static function start() {
		static $started = false;
 
		if ( ! $started ) {
			self::add_filters();
 
			self::add_actions();
 
			self::$features = array(
				self::TAGGING => array(
					'label' => 'Tagging',
					'desc'  => 'Allows users to create and manage custom tags and associate them with their events.',
					'const' => 'TAGGING',
				),
				self::STUDIO => array(
					'label' => 'Studio Features',
					'desc'  => 'Allows users to access studio features.',
					'const' => 'STUDIO',
				),
			);

			$started = true;
		}
	}
 
	protected static function add_filters() {
		// Add all filters here
	}
 
	protected static function add_actions() {
		// Add all actions here
		add_action( 'add_meta_boxes_memberpressproduct', array( __CLASS__, 'register_meta_box' ), 10 );

		add_action( 'save_post', array( __CLASS__, 'maybe_save_meta_box' ), 10 );

		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ), 100 );
	}

	/**
	 * Can the current user access a feature
	 * 
	 * Example usage: 
	 * SCAC::can_access( SCAC::TAGGING )
	 * the above checks to see if the current user can access the Tagging feature
	 */
	public static function can_access( $what, $use_cache = true, $user_id = false ) {
		static $access_cache = array();

		$user_id = $user_id ? $user_id : get_current_user_id();

		if ( ! isset( self::$features[ $what ] ) ) {
			return null;
		}

		if ( ! isset( $access_cache[ $user_id ] ) ) {
			$access_cache[ $user_id ] = array();
		}

		// Check/re-check access permissions if not cached, or forced to skip cache
		if ( ! $use_cache || ! isset( $access_cache[ $user_id ][ $what ] ) ) {
			$access_cache[ $user_id ][ $what ] = false;
			$memberships = self::get_user_memberships( $use_cache, $user_id );

			foreach ( $memberships as $membership_id ) {
				$membership_features = self::get_membership_features( $membership_id, $user_id );
				if ( in_array( $what, $membership_features ) ) {
					$access_cache[ $user_id ][ $what ] = true;
					break;
				}
			}
		}

		return $access_cache[ $user_id ][ $what ];
	}

	public static function enqueue_scripts() {
		$js_conf = array(
			'access' => array(),
			'features' => array(),
		);

		$i = 0;
		foreach ( self::$features as $key => $feature ) {
			$js_conf['access'][ $i ] = (bool) self::can_access( $key );
			$js_conf['features'][ $feature['const'] ] = (string) $i;

			$i ++;
		}

		wp_localize_script( 'theme-generic', 'SCAC_CONF', $js_conf );
	}

	public static function get_user_memberships( $use_cache = true, $user_id = false ) {
		static $memberships;

		if ( ! class_exists( 'MeprUser' ) ) {
			return array();
		}

		$user_id = $user_id ? $user_id : get_current_user_id();

		if ( ! $use_cache || ! isset( $memberships[ $user_id ] ) ) {
			$usr = new MeprUser( $user_id );
			$memberships[ $user_id ] = $usr->active_product_subscriptions();
		}

		return $memberships[ $user_id ];
	}

	public static function get_membership_features( $membership ) {
		$features = get_post_meta( $membership, '_scac_enabled_features', true );
		$features = is_array( $features ) ? $features : array();
		return self::filter_valid_features( $features );
	}

	public static function register_meta_box( $post ) {
		add_meta_box( 
			'sc_ac_membership_meta_box',
			__( 'Allows Access To' ),
			array( __CLASS__, 'render_meta_box' ),
			'memberpressproduct',
			'normal',
			'default'
		);
	}

	public static function render_meta_box( $post ) {
		$features = self::get_membership_features( $post->ID ); ?>
		<p class="help">Check off any features that users with an active subscription to this membership will get access to.</p>

		<?php foreach ( self::$features as $key => $feature ) : ?>
			<p>
				<label for="scac_feature_<?php echo esc_attr( $key ); ?>">
					<input id="scac_feature_<?php echo esc_attr( $key ); ?>" type="checkbox" name="scac_features[]" value="<?php echo esc_attr( $key ); ?>" <?php echo in_array( $key, $features ) ? ' checked="checked"' : ''; ?> /><?php echo $feature['label']; ?>
					<?php if ( ! empty( $feature['desc'] ) ) : ?>
						<small class="help">( <?php echo $feature['desc']; ?> )</small>
					<?php endif; ?>
				</label>
			</p>
		<?php endforeach;

		wp_nonce_field( 'scac_save_membership_features', '_scac_metabox_nonce' );
	}

	public static function maybe_save_meta_box( $post_id ) {
		if ( ! isset( $_POST['_scac_metabox_nonce'] ) || ! wp_verify_nonce( $_POST['_scac_metabox_nonce'], 'scac_save_membership_features' ) ) {
			return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( 'memberpressproduct' != get_post_type( $post_id ) ) {
			return;
		}

		if ( is_multisite() && ms_is_switched() ) {
			return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		if ( empty( $_POST['scac_features'] ) ) {
			delete_post_meta( $post_id, '_scac_enabled_features' );
		} else {
			$features = self::filter_valid_features( $_POST['scac_features'] );

			update_post_meta( $post_id, '_scac_enabled_features', $features );
		}
	}

	public static function filter_valid_features( $features ) {
		foreach ( $features as $i => $feature ) {
			if ( ! isset( self::$features[ $feature ] ) ) {
				unset( $features[ $i ] );
			}
		}

		return array_values( $features );
	}
}
SCAC::start();
