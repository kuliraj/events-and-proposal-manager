<?php
use Illuminate\Http\Request;
use Stemcounter\Event;
use Stemcounter\Event_Version;
use Stemcounter\Payment;
use Stemcounter\Meta;
use Stemcounter\Ajax_Response;

function sc_stripe_filter_react_data_no_stripe( $react_data ) {
	$react_data['stripe'] = false;

	return $react_data;
}

if ( ( ! defined( 'SC_STRIPE_SECRET_KEY' ) || ! SC_STRIPE_SECRET_KEY )
	|| ( ! defined( 'SC_STRIPE_PUBLIC_KEY' ) || ! SC_STRIPE_PUBLIC_KEY ) 
	|| ( ! defined( 'SC_STRIPE_APP_CLIENT_ID' ) || ! SC_STRIPE_APP_CLIENT_ID ) ) {
	add_filter( 'sc/react/data/proposal', 'sc_stripe_filter_react_data_no_stripe', 10 );

	return;
}

function sc_stripe_filter_react_data( $react_data, $event, $user_id ) {
	if ( ! get_user_meta( $user_id, '_stripe_conn_tok', true ) ) {
		$react_data['extensions']['stripe'] = false;
	} else {
		$react_data['extensions']['stripe'] = array(
			'pk' => SC_STRIPE_PUBLIC_KEY,
		);
	}

	return $react_data;
}
add_filter( 'sc/react/data/proposal', 'sc_stripe_filter_react_data', 10, 3 );

function sc_process_stripe_payment() {
	$request = Request::capture();
	$payment_id = $request->input( 'payment_id' );
	$event_id = $request->input( 'event_id' );
	$stripe_data = $request->input( 'stripe' );
	$stripe_payment = $request->input( 'payment_amount' );

	$convenience_fee = $stripe_payment['convenienceFee'];
	$fe_payment_amount = $stripe_payment['amount']; error_log( var_export( $convenience_fee, true ) );

	$r = new Ajax_Response();

	if ( ! $payment_id || ! $stripe_data || ! $event_id ) {
		$r->fail( '1 There was an issue processing your payment. Please refresh the page and try again.' );
	}

	try {
		$event = Event::where( array(
			'id' => $event_id,
		) )->firstOrFail();
	} catch (Exception $e) {
		$r->fail( '2 There was an issue processing your payment. Please refresh the page and try again.' );
	}

	try {
		$payments = Payment::where( array(
			// 'id' => $payment_id,
			'user_id' => $event->user_id,
			'event_id' => $event_id,
			'is_live' => 0,
			'status' => 'not-paid',
		) )->orderBy( 'due_date', 'ASC' )->get()->all();
	} catch (Exception $e) {
		$r->fail( '3 There was an issue processing your payment. Please refresh the page and try again.' );
	}

	try {
		$live_payment = Payment::where( array(
			'id' => $payment_id,
			'user_id' => $event->user_id,
			'event_id' => $event_id,
			'is_live' => 1,
		) )->firstOrFail();
	} catch (Exception $e) {
		$r->fail( '4 There was an issue processing your payment. Please refresh the page and try again.' );
	}

	$max_index = count( $payments ) - 1;
	$found_payment = false;

	\Stripe\Stripe::setApiKey( SC_STRIPE_SECRET_KEY );

	try {
		$payment_amount = 0;

		foreach ( $payments as $i => $payment ) {
			$payment_amount = $payment->get_amount();

			if ( $fe_payment_amount == ( $payment_amount * 100 ) && 
					$live_payment->due_date == $payment->due_date  &&
					$live_payment->card_fee == $payment->card_fee ) {
				$found_payment = $payment;
				break;
			}
		}

		if ( ! $found_payment ) {
			$found_payment = $live_payment->replicate( array( 'created_at', 'updated_at', 'id' ) );
			$found_payment->is_live = 0;
		}

		$payment_amount = sc_format_price( $fe_payment_amount / 100 );
		$convenience_fee = sc_format_price( $convenience_fee / 100 );
		$user_currency = sc_get_user_saved_pref_currency( $event->user_id );

		$idempotency_meta = Meta::firstOrNew( array(
			'type' => 'payment',
			'type_id' => $live_payment->id,
			'meta_key' => 'stripe_req_counter',
		) );
		$idempotency_meta->meta_value = $idempotency_meta->exists ? $idempotency_meta->meta_value : 0;
		$idempotency_key = "{$event_id}_{$live_payment->id}_" . ( $payment_amount * 100 ) . "_{$idempotency_meta->meta_value}";
		if ( $idempotency_meta->exists ) {
			$idempotency_meta->save();
		}

		error_log( var_export( array(
			'amount' => $payment_amount * 100 + $convenience_fee * 100,
			'currency' => strtolower( $user_currency ),
			'source' => $stripe_data['id'],
		), true ) );

		$charge = \Stripe\Charge::create( array(
			'amount' => $payment_amount * 100 + $convenience_fee * 100,
			'currency' => strtolower( $user_currency ),
			'source' => $stripe_data['id'],
		), array(
			'idempotency_key' => $idempotency_key,
			'stripe_account' => get_user_meta( $event->user_id, '_stripe_conn_user_id', true ),
		) );
	} catch (Exception $e) {
		$idempotency_meta = Meta::firstOrNew( array(
			'type' => 'payment',
			'type_id' => $live_payment->id,
			'meta_key' => 'stripe_req_counter',
		) );
		$idempotency_meta->meta_value = $idempotency_meta->exists ? $idempotency_meta->meta_value + 1 : 1;
		$idempotency_meta->save();

		$r->fail( 'There was an issue processing your payment. Please refresh the page and try again. ' . $e->getMessage() );
	}

	if ( ! $found_payment ) {
		$r->fail( 'There was an issue processing your payment. Please refresh the page and try again. ' . $e->getMessage() );
	}

	$live_payment->status = $found_payment->status = 'complete';
	$live_payment->payment_date = $found_payment->payment_date = date( 'Y-m-d H:i:s', $charge['created'] );
	$live_payment->payment_type = $found_payment->payment_type = 'stripe';
	$live_payment->amount = $found_payment->amount = $payment_amount;
	$live_payment->convenience_fee = $found_payment->convenience_fee = $convenience_fee;
	$live_payment->save();
	$found_payment->save();
	error_log( var_export( $found_payment->toArray(), true ) );

	$stripe_data['charge_id'] = $charge['id'];
	$stripe_data['charge_created'] = $charge['created'];

	$payment_meta = new Meta;
	$payment_meta->type = 'payment';
	$payment_meta->type_id = $live_payment->id;
	$payment_meta->meta_key = 'stripe_data';
	$payment_meta->meta_value = json_encode( $stripe_data );
	$payment_meta->save();

	$payment_meta = new Meta;
	$payment_meta->type = 'payment';
	$payment_meta->type_id = $found_payment->id;
	$payment_meta->meta_key = 'stripe_data';
	$payment_meta->meta_value = json_encode( $stripe_data );
	$payment_meta->save();

	do_action( 'sc/stripe/charge_created', $event, $payment_amount );

	try {
		$version = Event_Version::where(array(
			'event_id' => $event_id,
		))->orderBy('created_at', 'desc')->first();

	} catch ( Exception $r ) {
		$r->fail( 'This event version doesn\'t exist.' );
	}
	$event_data = json_decode( $version->event_data );

	$_event =  (array) $event_data->event;
	$eventDate = $event_data->eventDate;
	$js_date_format = $event_data->dateFormat;
	if ($js_date_format == 'dd/mm/yy') {
		$date_format = 'd/m/Y';
	} else if ($js_date_format == 'mm/dd/yy') {
		$date_format = 'm/d/Y';
	} else if ($js_date_format == 'dd.mm.yy') {
		$date_format = 'd.m.Y';
	} else if ($js_date_format == 'mm.dd.yy') {
		$date_format = 'm.d.Y';
	}

	$_payment = array(
		'paymentId'        => $found_payment->id,
		'paymentName'      => $found_payment->payment_name,
		'dueDate'          => sc_get_user_date_time( $event->user_id, $found_payment->due_date, false, $date_format ),	
		'amount'           => ! is_null( $found_payment->amount ) ? sc_format_price( $found_payment->amount ) : null,
		'cardFee'      	   => $found_payment->card_fee,
		'convenienceFee'   => ! is_null( $found_payment->convenience_fee ) ? sc_format_price( $found_payment->convenience_fee ) : null,
		'payment_date'     => is_null( $found_payment->payment_date ) ? '' : sc_get_user_date_time( $event->user_id, $found_payment->payment_date , false, $date_format ),
		'amountPercentage' => is_null( $found_payment->payment_percentage ) ? null : sc_format_price( $found_payment->payment_percentage ),
		'amountType'       => ! is_null( $found_payment->payment_percentage ) && 'complete' != $found_payment->status ? 'percentage' : 'currency',
		'paymentType'      => $found_payment->payment_type,
		'note'             => $found_payment->payment_note,
		'status'           => $found_payment->status,
	);

	$r->add_payload( 'payment', $_payment );
	$r->respond();
}
add_action( 'wp_ajax_sc/payment/process/stripe', 'sc_process_stripe_payment', 10 );
add_action( 'wp_ajax_nopriv_sc/payment/process/stripe', 'sc_process_stripe_payment', 10 );

function sc_stripe_enqueue_scripts() {
	wp_enqueue_script( 'stripe-checkout', 'https://checkout.stripe.com/checkout.js', array(), '' );
}

function sc_stripe_proposal_init() {
	add_action( 'wp_enqueue_scripts', 'sc_stripe_enqueue_scripts', 5 );
}
add_action( 'sc/app/load/event_proposal_2_0', 'sc_stripe_proposal_init', 10 );
add_action( 'sc/app/load/event_proposal_2_0-public', 'sc_stripe_proposal_init', 10 );

function sc_add_mp_stripe_tab( $mepr_current_user ) {
	$account_url = MeprUtils::get_permalink(); // $mepr_options->account_page_url();
	$delim = preg_match('#\?#',$account_url) ? '&' : '?'; ?>
	<span class="mepr-nav-item <?php MeprAccountHelper::active_nav( 'sc-stripe' ); ?>">
		<a href="<?php echo MeprHooks::apply_filters( 'mepr-account-nav-sc-stripe-link', $account_url . $delim . 'action=sc-stripe' ); ?>" id="mepr-account-sc-stripe">Stripe</a>
	</span>
	<?php
}
add_action( 'mepr_account_nav', 'sc_add_mp_stripe_tab', 10 );

function sc_intercept_stripe_error_get_param() {
	// WordPress will strip the 'error' key from the $_GET global
	// Since this is sent from Stripe, we'll just save it in
	// the 'stripe_error' key so that we can use it later on
	if ( isset( $_GET['error'] ) ) {
		$_GET['stripe_error'] = $_GET['error'];
	}
}
add_action( 'init', 'sc_intercept_stripe_error_get_param', 10 );

function sc_render_mp_stripe_tab( $action ) {
	if ( 'sc-stripe' != $action ) {
		return;
	}

	$error = false;
	$user_id = get_current_user_id();

	if ( isset( $_GET['code'] ) ) { // Redirect w/ code
		$code = $_GET['code'];

		$token_request_body = array(
			'grant_type' => 'authorization_code',
			'client_id' => SC_STRIPE_APP_CLIENT_ID,
			'code' => $code,
			'client_secret' => SC_STRIPE_SECRET_KEY,
		);

		$req = curl_init( 'https://connect.stripe.com/oauth/token' );
		curl_setopt( $req, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $req, CURLOPT_POST, true );
		curl_setopt( $req, CURLOPT_POSTFIELDS, http_build_query( $token_request_body ) );

		// TODO: Additional error handling
		$respCode = curl_getinfo( $req, CURLINFO_HTTP_CODE );
		$resp = json_decode( curl_exec( $req ), true );
		curl_close( $req );

		if ( ! empty( $resp['error'] ) ) {
			// var_dump( $resp );
			switch ( $resp['error'] ) {
				case 'invalid_grant':
					$error = 'Your Stripe authorization code has expired. Please click the button below again in order to generate a new code.';
					break;
				
				default:
					$error = 'The following error occurred while processing your authorization from Stripe: ' . esc_html( $resp['error_description'] );
					break;
			}
		} else {
			update_user_meta( $user_id, '_stripe_connect_data', $resp );
			update_user_meta( $user_id, '_stripe_conn_tok', $resp['access_token'] );
			update_user_meta( $user_id, '_stripe_conn_refresh_tok', $resp['refresh_token'] );
			update_user_meta( $user_id, '_stripe_conn_pk', $resp['stripe_publishable_key'] );
			update_user_meta( $user_id, '_stripe_conn_user_id', $resp['stripe_user_id'] );
		}
	} else if ( isset( $_GET['stripe_error'] ) ) { // Error
		switch ( $_GET['stripe_error'] ) {
			case 'invalid_grant':
				$error = 'Your Stripe authorization code has expired. Please click the button below again in order to generate a new code.';
				break;
			
			case 'access_denied':
				$error = 'You have cancelled connecting your Stripe account with StemCounter.';
				break;

			default:
				$error = 'The following error occurred while processing your authorization from Stripe: ' . esc_html( $_GET['error_description'] );
				break;
		}
		// echo $_GET['error_description'];
	}

	if ( ! empty( $error ) ) {
		echo '<div class="alert alert-danger">' . $error . '</div>';
	}

	if ( ! empty( $_GET['sc_stripe'] ) ) {
		$notice = false;

		switch ( $_GET['sc_stripe'] ) {
			case 'disconnected':
				$notice = 'Your Stripe account has been successfully disconnected. If you did that on accident, you can re-connect your Stripe account below.';

				break;
			
			default:
				# code...
				break;
		}

		if ( $notice ) {
			echo '<div class="alert alert-info">' . $notice . '</div>';
		}
	} ?>
	<div class="sc-stripe-wrap">
		<?php if ( get_user_meta( $user_id, '_stripe_conn_tok', true ) ) : ?>
			<h4>You are all set to accept payments through Stripe!</h4>

			<p>Your customers will now be able to pay for individual payments by going to the Public Link for their Proposal.</p>

			<p><small>Want to disconnect your Stripe account? Click <a href="<?php echo esc_url( add_query_arg( array( 'action' => 'sc/stripe/disconnect', 'nonce' => wp_create_nonce( 'sc/stripe/disconnect' ) ), admin_url( 'admin-ajax.php' ) ) ); ?>">here</a>.</small></p>
		<?php else : ?>
			<h4>Start accepting online payments with Stripe!</h4>
			<p>Before we get started, we want to clarify the following points:</p>
			<ul>
				<li><a href="https://stripe.com" target="_blank">Stripe</a> is the largest online tool for vendors accepting payments. In order to accept payments, you need to have an account with Stripe. If you don't have one already - don't worry, you'll be able to set it up from here.</li>
				<li>As the maintainer of the recipient Stripe account, you will be in charge of dealing with chargebacks and any customer service issues/questions (regarding the payments).</li>
				<li>Stripe works the same as other credit card processors and takes a fee of 2.9%+0.30 for each payment. Your client won't see this. All fees will be taken out of the payments by Stripe.</li>
				<li>This feature comes included with your Stemcounter Core subscription and we don't charge you any additional fees for using Stripe.</li>
			</ul>
			<p>With that out of the way, and if you understand and agree to the above terms, you can proceed by linking your Stripe account with StemCounter.</p>
			<p><a href="https://connect.stripe.com/oauth/authorize?response_type=code&amp;client_id=<?php echo esc_attr( SC_STRIPE_APP_CLIENT_ID );?>&amp;scope=read_write&amp;redirect_uri=<?php echo esc_url( add_query_arg( array( 'action' => 'sc-stripe' ), get_permalink( sc_get_memberpress_acct_page() ) ) ); ?>" class="btn btn-primary">Link my Stripe Account</a></p>
		<?php endif; ?>
	</div>
	<?php
}
add_action( 'mepr_account_nav_content', 'sc_render_mp_stripe_tab', 10 );

function sc_disconnect_stripe_account() {
	if ( ! wp_verify_nonce( $_GET['nonce'], 'sc/stripe/disconnect' ) ) {
		echo 'Bad request';
		exit;
	}

	$user_id = get_current_user_id();

	if ( ! get_user_meta( $user_id, '_stripe_connect_data', true ) || ! get_user_meta( $user_id, '_stripe_conn_user_id', true ) ) {
		wp_redirect( get_permalink( sc_get_memberpress_acct_page() ) );
		exit;
	}

	// Disconnect by doing this - https://stripe.com/docs/connect/reference#post-deauthorize
	$request_body = array(
		'client_id' => SC_STRIPE_APP_CLIENT_ID,
		'stripe_user_id' => get_user_meta( $user_id, '_stripe_conn_user_id', true ),
	);

	$req = curl_init( 'https://connect.stripe.com/oauth/deauthorize' );
	curl_setopt( $req, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $req, CURLOPT_POST, true );
	curl_setopt( $req, CURLOPT_POSTFIELDS, http_build_query( $request_body ) );
	curl_setopt( $req, CURLOPT_HTTPHEADER, array( 'Authorization: Bearer ' . SC_STRIPE_SECRET_KEY ) );

	// TODO: Additional error handling
	$respCode = curl_getinfo( $req, CURLINFO_HTTP_CODE );
	$resp = json_decode( curl_exec( $req ), true );
	curl_close( $req );

	if ( ! empty( $resp['error'] ) ) {
		// Perhaps do some error handling here in the future?
		// If deauthorization fails, then we most-likely have no use in
		// the user being authorized in the first place
		error_log( 'sc_disconnect_stripe_account() failed. Response: ' . var_export( $resp, true ) );
	}

	// Actually delete all of their Stripe info
	delete_user_meta( $user_id, '_stripe_connect_data' );
	delete_user_meta( $user_id, '_stripe_conn_tok' );
	delete_user_meta( $user_id, '_stripe_conn_refresh_tok' );
	delete_user_meta( $user_id, '_stripe_conn_pk' );
	delete_user_meta( $user_id, '_stripe_conn_user_id' );

	// Take the user back to their account page.
	wp_redirect( add_query_arg( array( 'action' => 'sc-stripe', 'sc_stripe' => 'disconnected' ), get_permalink( sc_get_memberpress_acct_page() ) ) );
	exit;
}
add_action( 'wp_ajax_sc/stripe/disconnect', 'sc_disconnect_stripe_account', 10 );

function sc_get_memberpress_acct_page() {
	if ( class_exists( 'MeprOptions' ) ) {
		$mepr_options = MeprOptions::fetch();

		return $mepr_options->account_page_id;
	} else {
		return false;
	}
}
