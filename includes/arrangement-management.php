<?php
use Illuminate\Http\Request;
use Symfony\Component\Translation\Translator;
use Illuminate\Validation\Factory;
use Stemcounter\Event;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Item;
use Stemcounter\Arrangement_Photo;
use Stemcounter\Invoicing_Category;
use Stemcounter\Arrangement_Recipe;
use Stemcounter\Ajax_Response;
use Stemcounter\Recipe;
use Stemcounter\Recipe_Item;

// @todo flower archetype integration (upon creation, copy variations from archetype)

// @todo for tax migration use:
// $user_meta = get_user_meta($user_id, 'wp_s2member_custom_fields');
// $sales_tax = empty($user_meta[0]['sales_tax__']) ? 0 : $user_meta[0]['sales_tax__'];

function sc_duplicate_event_arrangements( $from_event_id, $to_event_id ) {
	$arrangements = Arrangement::where(array(
		'event_id' => $from_event_id,
	) )->with( 'recipes', 'photos', 'items', 'items.item' )->get();

	foreach ( $arrangements as $arrangement ) {
		// This copies the arrangement to the new event
		$new_arrangement = $arrangement->replicate();
		$new_arrangement->event_id = $to_event_id;
		$new_arrangement->save();

		foreach ( $arrangement->photos as $photo ) {
			$_new_photo = $photo->replicate();
			$_new_photo->arrangement_id = $new_arrangement->id;
			$_new_photo->save();
		}

		foreach ( $arrangement->recipes as $recipe ) {
			$new_recipe = $recipe->replicate();
			$new_recipe->arrangement_id = $new_arrangement->id;
			$new_recipe->save();

			foreach ( $arrangement->items as $arrangement_item ) {
				if ( $arrangement_item->arrangement_recipe_id == $recipe->id ) {
					$_new_arr_item = $arrangement_item->replicate();
					$_new_arr_item->arrangement_id = $new_arrangement->id;
					$_new_arr_item->arrangement_recipe_id = $new_recipe->id;
					$_new_arr_item->save();
				} else {
					continue;
				}
			}
		}
	}
}

function sc_ajax_edit_user_arrangement_form() {
	sc_clean_input_slashes();
	$request = Request::capture();

	// This is for a standalone arrangement(does not belong to an event)
	if ( '0' === $request->input( 'event_id' ) ) {
		$event = sc_get_fake_event_for_user();
		$args = array(
			'id' => $request->input( 'id' ),
			'user_id' => $event->user_id,
		);
	} else {
		try {
			$event = sc_user_can_edit_event( $request->input('event_id') );
			$args = array(
				'id' => $request->input( 'id' ),
				'event_id' => $event->id,
			);
		} catch (Exception $e) {
			echo 'Security error - please contact support.';
			exit;
		}
	}

	$user_id = $event->user_id;
	$values = null;

	$arrangement = Arrangement::where( $args )->with( array(
		'photos',
		'recipes',
		'items',
		'items.item' => function($query) {
			$query->withTrashed();
		},
		'items.item.default_variation' => function($query) {
			$query->withTrashed();
		},
		'items.item_variation' => function($query) {
			$query->withTrashed();
		},
		'items.item_variation.item',
	) )->first();

	if ( $arrangement ) {
		$values = $arrangement->toArray();
		$values['items'] = array();
		$recipe_id = ! empty( $request->input( 'recipe_id' ) ) ? intval( $request->input( 'recipe_id' ) ) : null;
		$recipe = Arrangement_Recipe::where ( array(
			'id' => $recipe_id,
			'arrangement_id' => $arrangement->id
		) )->first();
		$arrangement->event = $event;

		foreach ( $arrangement->items as &$arrangement_item ) {
			$arrangement_item->arrangement = $arrangement;
			if ( $recipe_id != $arrangement_item->arrangement_recipe_id ) {
				continue;
			}

			$vi = $arrangement_item->toArray();
			unset( $vi['arrangement'], $vi['item_variation']['item'], $vi['default_variation']['item'] );
			$vi['item_type'] = $arrangement_item->item->type;
			$vi['item_variation_id'] = $arrangement_item->item_variation->id;
			$vi['item_variation_name'] = 'Default' != $arrangement_item->item_variation->name ? ' - ' . $arrangement_item->item_variation->name : '';
			$vi['item_pricing_category'] = $arrangement_item->item->pricing_category;

			$vi['attachment'] = sc_get_variation_attachment( $arrangement_item->item_variation, $arrangement_item->item->default_variation );
			
			$values['items'][] = $vi;
			usort( $values['items'], 'sc_recipe_list_sort' );
		}

		if (!empty($values['photos'])) {

			$photos = array();
			foreach ($values['photos'] as $photo) {

				/* if the image exists */
				$image = wp_get_attachment_image_src($photo['photo_id']);
				if (!empty($image)) {
					$image_id = $photo['photo_id'];
					$image_url = wp_get_attachment_image_src($image_id, 'medium');
					$image_url = $image_url[0];

					$photos['photos'][$photo['modal_position']] = array(
						'image_id' => $image_id,
						'image_url' => $image_url,
					);		
				} else {
					/* delete it from the arrangement_photos if it doesn't */
					Arrangement_Photo::where(array(
						'photo_id' => $photo['photo_id']
					))->delete();
				}
			}

			$values['photos'] = $photos['photos'];
		}

		$hardgood_multiple = is_null($arrangement->hardgood_multiple) ? $event->hardgood_multiple : $arrangement->hardgood_multiple;
		$fresh_flower_multiple = is_null($arrangement->fresh_flower_multiple) ? $event->fresh_flower_multiple : $arrangement->fresh_flower_multiple;

		$global_labor_value = is_null($arrangement->global_labor_value) ? $event->global_labor_value : $arrangement->global_labor_value;
		$flower_labor = is_null($arrangement->flower_labor) ? $event->flower_labor : $arrangement->flower_labor;
		$hardgood_labor = is_null($arrangement->hardgood_labor) ? $event->hardgood_labor : $arrangement->hardgood_labor;
		$base_price_labor = is_null($arrangement->base_price_labor) ? $event->base_price_labor : $arrangement->base_price_labor;
		$fee_labor = is_null($arrangement->fee_labor) ? $event->fee_labor : $arrangement->fee_labor;
		$rental_labor = is_null($arrangement->rental_labor) ? $event->rental_labor : $arrangement->rental_labor;

		$values['invoice'] = array(
			'hardgood_multiple' => $hardgood_multiple,
			'fresh_flower_multiple' => $fresh_flower_multiple,
			'labor' => array(
				'global_labor_value' => $global_labor_value,
				'flower' => $flower_labor,
				'hardgood' => $hardgood_labor,
				'base_price' => $base_price_labor,
				'fee' => $fee_labor,
				'rental' => $rental_labor
			)
		);

		$values['totals'] = array(
			'currencySymbol' => sc_get_user_saved_pref_currency( $user_id ),
			'cost' => $arrangement->get_cost( $recipe_id ),
			'total' => $arrangement->get_calculated_price( false, array(), $recipe_id ),
			'total_item_price' => sc_format_price( $arrangement->get_calculated_price( false, array() ) ),
			'override'=> ( is_null( $arrangement->override_cost ) ? '' : $arrangement->override_cost )
		);
	} else {
		$values = array();
		$hardgood_multiple = $event->hardgood_multiple;
		$fresh_flower_multiple = $event->fresh_flower_multiple;

		$global_labor_value = $event->global_labor_value;
		$flower_labor = $event->flower_labor;
		$hardgood_labor = $event->hardgood_labor;
		$base_price_labor = $event->base_price_labor;
		$fee_labor = $event->fee_labor;
		$rental_labor = $event->rental_labor;

		$values['invoice'] = array(
			'hardgood_multiple' => $hardgood_multiple,
			'fresh_flower_multiple' => $fresh_flower_multiple,
			'labor' => array(
				'global_labor_value' => $global_labor_value,
				'flower' => $flower_labor,
				'hardgood' => $hardgood_labor,
				'base_price' => $base_price_labor,
				'fee' => $fee_labor,
				'rental' => $rental_labor
			)
		);
	}
	?>
	<form class="form-horizontal style-form edit-arrangement-form" method="post">
		<div class="form-shell"></div>
	</form>

	<?php
	$item_types = sc_get_mixed_arrangement_item_types();
	$invoice_categories_list = Invoicing_Category::where(array(
		'user_id' => $user_id
	))->get()->toArray();
	//Default Event Category List Item
	if ($event) {
		$selected_category_id = 'ERROR';
		$selected_category_id = -$event->invoicing_category_id;
		array_unshift($invoice_categories_list, array(
			"id" => -$event->invoicing_category_id,
		    "user_id" => $user_id,
		    "name" => 'Default Event Markup',
		    "hardgood_multiple" => $event->hardgood_multiple,
		    "fresh_flower_multiple" => $event->fresh_flower_multiple,
		    "global_labor_value" => $event->global_labor_value,
		    "flower_labor" => $event->flower_labor,
		    "hardgood_labor" => $event->hardgood_labor,
		    "base_price_labor" => $event->base_price_labor,
		    "fee_labor" => $event->fee_labor,
		    "rental_labor" => $event->rental_labor
		));

		if ($arrangement && !is_null($arrangement->invoicing_category_id)) {
			$name = 'test';
			try {
				$arrangement_invoicing_category = Invoicing_Category::where(array(
					'id' => $arrangement->invoicing_category_id,
					'user_id' => $user_id
				))->firstOrFail();
				$selected_category_id = $arrangement_invoicing_category->id;

				foreach ($invoice_categories_list as $i => $value) {
					if ($value['id'] == $selected_category_id) {
						$invoice_categories_list[$i]["hardgood_multiple"] = $arrangement->hardgood_multiple;
					    $invoice_categories_list[$i]["fresh_flower_multiple"] = $arrangement->fresh_flower_multiple;
					    $invoice_categories_list[$i]["global_labor_value"] = $arrangement->global_labor_value;
					    $invoice_categories_list[$i]["flower_labor"] = $arrangement->flower_labor;
					    $invoice_categories_list[$i]["hardgood_labor"] = $arrangement->hardgood_labor;
					    $invoice_categories_list[$i]["base_price_labor"] = $arrangement->base_price_labor;
					    $invoice_categories_list[$i]["fee_labor"] = $arrangement->fee_labor;
					    $invoice_categories_list[$i]["rental_labor"] = $arrangement->rental_labor;
						break;						
					}
				}
			} catch (Exception $e) {
				$name = 'Deleted Markup Profile';
				array_unshift($invoice_categories_list, array(
					"id" => $arrangement->invoicing_category_id,
				    "user_id" => $user_id,
				    "name" => $name,
				    "hardgood_multiple" => $arrangement->hardgood_multiple,
				    "fresh_flower_multiple" => $arrangement->fresh_flower_multiple,
				    "global_labor_value" => $arrangement->global_labor_value,
				    "flower_labor" => $arrangement->flower_labor,
				    "hardgood_labor" => $arrangement->hardgood_labor,
				    "base_price_labor" => $arrangement->base_price_labor,
				    "fee_labor" => $arrangement->fee_labor,
				    "rental_labor" => $arrangement->rental_labor
				));
				$selected_category_id = $arrangement->invoicing_category_id;
			}
		}
	}


	$user_recipes = array();

	if ( ! (bool) $request->input( 'standaloneRecipes' ) ) {
		$_user_recipes = Recipe::where(array(
			'user_id' => $user_id,
		) )->orderby( 'name', 'asc' )->with(array(
			'items',
			'items.item' => function($query) {
				$query->withTrashed();
			},
			'items.item_variation',
			'items.item_variation.item',
			'items.item.default_variation',
			'items.item_variation.item',
		) )->get();

		if ( $_user_recipes ) {
			foreach ( $_user_recipes as $key => $_recipe ) {
				$user_recipes[ $key ] = $_recipe->toArray();
				$user_recipes[ $key ]['items'] = array();
				foreach ( $_recipe->items as $recipe_item ) {
					$r_items = $recipe_item->toArray();
					$r_items['cost'] = ! is_null( $recipe_item->item_variation->cost ) ? $recipe_item->item_variation->cost : $recipe_item->item->default_variation->cost;
					$r_items['item_type'] = $recipe_item->item->type;
					$r_items['item_variation_id'] = $recipe_item->item_variation->id;
					$r_items['item_variation_name'] = 'Default' != $recipe_item->item_variation->name ? ' - ' . $recipe_item->item_variation->name : '';

					$r_items['attachment'] = sc_get_variation_attachment( $recipe_item->item_variation, $recipe_item->item->default_variation );

					$user_recipes[ $key ]['items'][] = $r_items;
				}
			}
		}
	} ?>
	<script type="text/javascript">
	jQuery(document).trigger('stemcounter.action.renderArrangementForm', {
		eventId: <?php echo json_encode($event->id); ?>,
		userId: <?php echo json_encode( $user_id ); ?>,
		values: <?php echo json_encode( $values ); ?>,
		itemTypes: <?php echo json_encode( $item_types ) ?>,
		categoriesList: <?php echo json_encode( $invoice_categories_list ); ?>,
		categoryId: <?php echo json_encode( $selected_category_id ); ?>,
		itemsOnly: <?php echo intval( $request->input( 'items_only', 0 ) ); ?>,
		recipe_id: <?php echo json_encode( $recipe_id ); ?>,
		recipe_name: <?php echo json_encode( $recipe->name ); ?>,
		user_recipes: <?php echo json_encode( $user_recipes ); ?>,
		user_recipe_id: <?php echo json_encode( $recipe->user_recipe_id ); ?>,
		currencySymbol: <?php echo json_encode( sc_get_user_saved_pref_currency( $user_id ) ); ?>,
		standaloneRecipes: <?php echo (bool) $request->input( 'standaloneRecipes' ) ? 'true' : 'false' ?>
	});
	</script>
	<?php
	exit;
}
add_action('wp_ajax_sc_edit_user_arrangement_form', 'sc_ajax_edit_user_arrangement_form');

function sc_ajax_edit_arrangement() {
	global $wpdb;
	
	sc_clean_input_slashes();
	$request = Request::capture();

	$response = array(
		'success' => false,
		'code' => 0,
		'message' => 'An unknown error occurred.',
		'payload' => array(),
	);

	try {

		try {
			$event = '0' === $request->input( 'event_id' ) ? sc_get_fake_event_for_user() : sc_user_can_edit_event( $request->input( 'event_id' ) );
		} catch (Exception $e) {
			throw new Exception( 'Invalid event. Please contact support.', 999 ); // @todo remove the 999 code; temporary fix until code is merged
		}

		$user_id = $event->user_id;
		$current_recipe_id = intval( $request->input( 'recipe_id' ) );

		$items_only = $request->input( 'items_only', 0 );

		if ( ! $items_only ) {
			$factory = new Factory( new Translator( 'en' ) );

			$validator = $factory->make( $request->all(), array(
				'name' => 'required',
				'quantity' => 'required|numeric|min:1',
			), array(
				'required' => 'Please fill in all required fields.',
				'quantity.numeric' => 'Arrangement number must be at least 1.',
				'quantity.min' => 'Arrangement number must be at least 1.',
			) );
		}

		if ( ! $items_only && $validator->fails() ) {
			$response['message'] = $validator->errors()->first();
		} else {
			$search = array(
				'event_id'=>$request->input('event_id'),
			);

			if ( $request->input( 'id' ) ) {
				$search['id'] = $request->input( 'id' );
			} else {
				$search['name'] = $request->input( 'name' );
			}

			$arrangement = Arrangement::firstOrNew( $search );

			if ( ! isset( $search['id'] ) && $arrangement->exists ) {
				throw new Exception('A duplicate arrangement already exists.', 999); // @todo remove the 999 code; temporary fix until code is merged
			}

			$arrangement->event_id = $request->input( 'event_id' );

			$override_cost = $request->input( 'override_cost' );
			if ( $override_cost === '' ) {
				$override_cost = NULL; // return to default final price
			} else {
				$override_cost = abs( floatval( $override_cost ) );
				$override_cost = sc_format_price( $override_cost );
			}
			$arrangement->override_cost = $override_cost;
			// $arrangement->override_cost = -1;

			// If we're saving everything and not just the items
			if ( ! $items_only ) {
				$arrangement->name = $request->input( 'name' );
				$arrangement->quantity = intval( $request->input( 'quantity' ) );
				$arrangement->include = $request->input( 'include' ) ? 1 : 0;

				//if it's the default event value select it reset the arrangment values to null
				if ($request->input( 'category_id' ) < 0) {
					$arrangement->invoicing_category_id = NULL;
					$arrangement->fresh_flower_multiple = NULL;
					$arrangement->hardgood_multiple = NULL;
					$arrangement->global_labor_value = NULL;
					$arrangement->flower_labor = NULL;
					$arrangement->hardgood_labor = NULL;
					$arrangement->base_price_labor = NULL;
					$arrangement->fee_labor = NULL;
				} else {
					//otherwise save them
					$arrangement->invoicing_category_id = abs( $request->input( 'category_id' ) );
					$arrangement->fresh_flower_multiple = is_null( $request->input( 'fresh_flower_multiple' ) ) ? 1 : $request->input( 'fresh_flower_multiple' );
					$arrangement->hardgood_multiple = is_null( $request->input( 'hardgood_multiple' ) ) ? 1 : $request->input( 'hardgood_multiple' );
					$arrangement->global_labor_value = is_null( $request->input( 'global_labor_value' ) ) ? 0 : $request->input( 'global_labor_value' );
					$arrangement->flower_labor = is_null( $request->input( 'flower_labor' ) ) ? 0 : $request->input( 'flower_labor' );
					$arrangement->hardgood_labor = is_null( $request->input( 'hardgood_labor' ) ) ? 0 : $request->input( 'hardgood_labor' );
					$arrangement->base_price_labor = is_null( $request->input( 'base_price_labor' ) ) ? 0 : $request->input( 'base_price_labor' );
					$arrangement->fee_labor = is_null( $request->input( 'fee_labor' ) ) ? 0 : $request->input( 'fee_labor' );
					$arrangement->rental_labor = is_null( $request->input( 'rental_labor' ) ) ? 0 : $request->input( 'rental_labor' );
				}

				$arrangement->tax = $request->input( 'tax' ) ? 1 : 0;
				if ( ! $arrangement->exists ) {
					$arrangement->order = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$wpdb->prefix}sc_arrangements
						WHERE event_id=%d", $request->input( 'event_id' ) ) );
				}
				$arrangement->note = $request->input( 'note' );

				$arrangement->save();

				foreach ( $arrangement->photos as $photo ) {
					$photo->delete();
				}
				
				$arrangement_photos = array(
					$request->input('image_1_id'),
					$request->input('image_2_id'),
					$request->input('image_3_id'),
					$request->input('image_4_id')
				);

				$arr_photos = array();
				foreach ($arrangement_photos as $i => $photo_id) {
					$image = wp_get_attachment_image_src($photo_id);
					if (!is_null($photo_id) && !empty($image)) {
						$arrangement_photo = new Arrangement_Photo();
						$arrangement_photo->arrangement_id = $arrangement->id;
						$arrangement_photo->photo_id = (int)$photo_id;
						$arrangement_photo->modal_position = $i;
						$arr_photos[] = $arrangement_photo;
					}
				}

				$arrangement->photos()->saveMany($arr_photos);
			}

			$recipe = Arrangement_Recipe::where( array(
				'id' => $current_recipe_id,
				'arrangement_id' => $arrangement->id
			) )->first();
			if ( null !== $recipe ) {
				$recipe->name = $request->input( 'recipe_name' );
				$recipe->user_recipe_id = $request->input( 'user_recipe_id', 0 );
				$recipe->save();
			}

			$arrangement->save();

			// Delete current recipe items
			// to avoid rework on how we handle items
			foreach ( $arrangement->items as $item ) {
				if ( $current_recipe_id == $item->arrangement_recipe_id ) {
					$item->delete();
				}
			}

			$arrangement_items = array();
			$items = $request->input( 'items' );
			foreach ( $items as $_item ) {
				if ( ( ! empty( $_item['shy'] ) && 'n' != $_item['shy'] ) || empty( $_item['keypair'] ) ) {
					continue; // ignore item as it's shy and should not be added
				}
				$keypair = explode( '_', $_item['keypair'] );
				$item_id = empty( $keypair[0] ) ? 0 : intval( $keypair[0] );
				$variation_id = empty( $keypair[1] ) ? 0 : intval( $keypair[1] );
				try {
					$item = sc_get_user_item_qb( $user_id, $item_id )->firstOrFail();
				} catch (Exception $e) {
					continue; // invalid item selected; skip it
				}

				$arrangement_item = new Arrangement_Item();
				$arrangement_item->item()->associate( $item );
				$arrangement_item->item_variation_id = $variation_id;
				$arrangement_item->item_type = 'Stemcounter\Item';
				$arrangement_item->cost = abs( floatval( sc_format_price( $_item['cost'] ) ) );
				$arrangement_item->quantity = abs( floatval( $_item['quantity'] ) );
				$arrangement_item->arrangement_recipe_id = $current_recipe_id;
				$arrangement_items[] = $arrangement_item;
			}

			// order items alphabetically
			usort( $arrangement_items, function( $a, $b ) {
				return strcmp( strtolower( $a->item->name ), strtolower( $b->item->name ) );
			} );
			$arrangement->items()->saveMany( $arrangement_items );

			$response['success'] = true;
			$response['message'] = '';

			if ( $items_only ) {
				$arrangement = Arrangement::where( array(
					'id'       => $request->input( 'id' ),
					'event_id' => $request->input( 'event_id' ),
				) )->with( array(
					'items',
					'items.item' => function( $query ) {
						$query->withTrashed();
					},
					'items.item_variation',
					'items.item_variation.item',
					'recipes'
				) )->first();
				$values = $arrangement->toArray();
				$arrangement->event = $event;
				$arrangement->items->transform(function( $arrangement_item ) use ( $arrangement ) {
					$arrangement_item->arrangement = $arrangement;

					return $arrangement_item;
				});

				$values['recipes'] = array();
				foreach ( $arrangement->recipes as $arrangement_recipe ) {
					$values['recipes'][] = array(
						'id' => $arrangement_recipe->id,
						'name' => $arrangement_recipe->name,
					);
				}
				
				$values['items'] = array();

				foreach ( $arrangement->items as $arrangement_item ) {
					$cost_and_markup = $arrangement_item->cost + $arrangement_item->get_markup( false );
			
					$vi = $arrangement_item->toArray();

					$vi['item_type'] = $arrangement_item->item->type;
					$vi['item_variation_id'] = $arrangement_item->item_variation->id;
					$vi['item_variation_name'] = 'Default' != $arrangement_item->item_variation->name ? ' - ' . $arrangement_item->item_variation->name : '';

					$item_attachment = sc_get_variation_attachment( $arrangement_item->item_variation, $arrangement_item->item->default_variation );

					$key = $arrangement_item->item_id . '_' . $vi['item_variation_id'];

					$type = sc_get_item_type( $arrangement_item->item, true, $arrangement_item );
					
					$values['items'][] = array(
						'id'                        => $arrangement_item->id,
						'recipe_id'					=> $arrangement_item->arrangement_recipe_id,
						'name'                      => $arrangement_item->get_full_name(),
						'quantity'                  => $arrangement_item->quantity,
						'invoicing_multiplier'      => $arrangement_item->get_invoicing_multiplier(),
						'cost'                      => $arrangement_item->cost,
						'cost_and_markup'           => $cost_and_markup,
						'cost_and_markup_and_labor' => $cost_and_markup + $arrangement_item->get_labor( false ),
						'subtotal'                  => $arrangement_item->get_calculated_price(),
						'type'                      => $type,
						'type_raw'                  => str_replace( ' ', '_', strtolower( $type ) ),
						'item_type'                 => $vi['item_type'],
						'item_variation_id'         => $vi['item_variation_id'],
						'item_variation_name'       => $vi['item_variation_name'],
						'item_pricing_category'     => $arrangement_item->item->pricing_category,
						'itemKeypair'               => $key,
						'attachment'				=> $item_attachment,
					);
				}

				$hardgood_multiple = is_null( $arrangement->hardgood_multiple ) ? $event->hardgood_multiple : $arrangement->hardgood_multiple;
				$fresh_flower_multiple = is_null( $arrangement->fresh_flower_multiple ) ? $event->fresh_flower_multiple : $arrangement->fresh_flower_multiple;

				$global_labor_value = is_null( $arrangement->global_labor_value ) ? $event->global_labor_value : $arrangement->global_labor_value;
				$flower_labor = is_null( $arrangement->flower_labor ) ? $event->flower_labor : $arrangement->flower_labor;
				$hardgood_labor = is_null( $arrangement->hardgood_labor ) ? $event->hardgood_labor : $arrangement->hardgood_labor;
				$base_price_labor = is_null( $arrangement->base_price_labor ) ? $event->base_price_labor : $arrangement->base_price_labor;
				$fee_labor = is_null( $arrangement->fee_labor ) ? $event->fee_labor : $arrangement->fee_labor;
				$rental_labor = is_null( $arrangement->rental_labor ) ? $event->rental_labor : $arrangement->rental_labor;

				$values['totals'] = array(
					'currencySymbol' => sc_get_user_saved_pref_currency( $user_id ),
					'cost' => $arrangement->get_cost(),
					'total' => $arrangement->get_calculated_price( false ),
					'override' => ( is_null( $arrangement->override_cost ) ? '' : $arrangement->override_cost ),
				);

				$response['payload'] = $values;
			} else {
				$arrangement = Arrangement::where( array(
					'id'       => $arrangement->id,
					'event_id' => $request->input( 'event_id' ),
				) )->with( array(
					'photos',
					'items',
					'items.item' => function( $query ) {
						$query->withTrashed();
					},
					'items.item_variation',
					'items.item_variation.item',
				) )->first();
				$values = $arrangement->toArray();
				$values['items'] = array();
				foreach ( $arrangement->items as $arrangement_item ) {
					$vi = $arrangement_item->toArray();

					$vi['item_type'] = $arrangement_item->item->type;
					$vi['item_variation_id'] = $arrangement_item->item_variation->id;
					$vi['item_variation_name'] = 'Default' != $arrangement_item->item_variation->name ? ' - ' . $arrangement_item->item_variation->name : '';

					$vi['attachment'] = sc_get_variation_attachment( $arrangement_item->item_variation, $arrangement_item->item->default_variation );
					$values['items'][] = $vi;
				}

				if ( ! empty( $values['photos'] ) ) {
					$photos = array();
					foreach ( $values['photos'] as $photo ) {
						/* if the image exists */
						$image = wp_get_attachment_image_src( $photo['photo_id'] );
						if ( ! empty( $image ) ) {
							$image_id = $photo['photo_id'];
							$image_url = wp_get_attachment_image_src( $image_id, 'medium' );
							$image_url = $image_url[0];

							$photos['photos'][ $photo['modal_position'] ] = array(
								'image_id' => $image_id,
								'image_url' => $image_url,
							);
						} else {
							/* delete it from the arrangement_photos if it doesn't */
							Arrangement_Photo::where( array(
								'photo_id' => $photo['photo_id']
							) )->delete();
						}
					}

					$values['photos'] = $photos['photos'];
				}

				$hardgood_multiple = is_null( $arrangement->hardgood_multiple ) ? $arrangement->event->hardgood_multiple : $arrangement->hardgood_multiple;
				$fresh_flower_multiple = is_null( $arrangement->fresh_flower_multiple ) ? $arrangement->event->fresh_flower_multiple : $arrangement->fresh_flower_multiple;

				$global_labor_value = is_null( $arrangement->global_labor_value ) ? $arrangement->event->global_labor_value : $arrangement->global_labor_value;
				$flower_labor = is_null( $arrangement->flower_labor ) ? $arrangement->event->flower_labor : $arrangement->flower_labor;
				$hardgood_labor = is_null( $arrangement->hardgood_labor ) ? $arrangement->event->hardgood_labor : $arrangement->hardgood_labor;
				$base_price_labor = is_null( $arrangement->base_price_labor ) ? $arrangement->event->base_price_labor : $arrangement->base_price_labor;
				$fee_labor = is_null( $arrangement->fee_labor ) ? $arrangement->event->fee_labor : $arrangement->fee_labor;
				$rental_labor = is_null( $arrangement->rental_labor ) ? $arrangement->event->rental_labor : $arrangement->rental_labor;

				$values['invoice'] = array(
					'hardgood_multiple' => $hardgood_multiple,
					'fresh_flower_multiple' => $fresh_flower_multiple,
					'labor' => array(
						'global_labor_value' => $global_labor_value,
						'flower' => $flower_labor,
						'hardgood' => $hardgood_labor,
						'base_price' => $base_price_labor,
						'fee' => $fee_labor,
						'rental' => $rental_labor,
					),
				);

				$values['invoicing_category_id'] = $arrangement->invoicing_category_id;
				$values['hardgood_multiple']     = $arrangement->hardgood_multiple;
				$values['fresh_flower_multiple'] = $arrangement->fresh_flower_multiple;
				$values['global_labor_value']    = $arrangement->global_labor_value;
				$values['flower_labor']          = $arrangement->flower_labor;
				$values['hardgood_labor']        = $arrangement->hardgood_labor;
				$values['base_price_labor']      = $arrangement->base_price_labor;
				$values['fee_labor']             = $arrangement->fee_labor;
				$values['rental_labor']          = $arrangement->rental_labor;

				$values['totals'] = array(
					'currencySymbol' => sc_get_user_saved_pref_currency( $user_id ),
					'cost' => $arrangement->get_cost(),
					'total' => $arrangement->get_calculated_price(false),
					'override' => ( is_null( $arrangement->override_cost ) ? '' : $arrangement->override_cost ),
				);

				$response['payload'] = $values;
			}
		}

	} catch (Exception $e) {

		$response['success'] = false;
		if ( $e->getCode() == 999 ) {
			$response['message'] = $e->getMessage();
		} else {
			$response['message'] = $e->getMessage();
			// $response['message'] = 'There was a problem with your request. Please try again later.';
		}

		$response['code'] = $e->getCode();
	}

	wp_send_json( $response );
}
add_action( 'wp_ajax_sc_edit_arrangement', 'sc_ajax_edit_arrangement' );

function sc_ajax_delete_arrangement() {
	sc_clean_input_slashes();
	$request = Request::capture();

	$response = array(
		'success'=>false,
		'code'=>0,
		'message'=>'An unknown error occurred.',
		'payload'=>array(),
	);

	try {
		$arrangement = Arrangement::findOrFail( $request->input( 'id' ) );
		if ( $arrangement->user_id != get_current_user_id() ) {
			throw new Exception( 'Invalid arrangement event.' );
		}

		foreach ( $arrangement->photos as $photo ) {
			$photo->delete();
		}
		
		foreach ( $arrangement->recipes as $recipe ) {
			$recipe->delete();
		}		

		$arrangement->delete();

		$response['success'] = true;
		$response['message'] = '';
	} catch (Exception $e) {
		$response['success'] = false;
		$response['message'] = 'There was a problem with your request. Please try again later.' . $e->getMessage();
		$response['code'] = $e->getCode();
	}

	wp_send_json( $response );
}
add_action('wp_ajax_sc_delete_arrangement', 'sc_ajax_delete_arrangement');

/* Deletes all records in the arrangement_photos that have the image_id */
function sc_delete_permanently_media_library_image($image_id) {
	$image = wp_get_attachment_image_src($image_id);

	if (!empty($image)) {
		$photos = Arrangement_Photo::where(array(
			'photo_id' => $image_id
		))->delete();
	}
}
add_action('delete_attachment', 'sc_delete_permanently_media_library_image');

function sc_ajax_calculate_arrangement_totals() {
	sc_clean_input_slashes();
	$request = Request::capture();

	$response = array(
		'success' => false,
		'code' => 0,
		'message' => 'An unknown error occurred.',
		'payload' => array(),
	);

	$standaloneRecipes = (bool) $request->input( 'standaloneRecipes' );

	try {
		$arrangement = new Arrangement();
		$arrangement->quantity = intval( $request->input( 'quantity' ) );
		$arrangement->event = '0' === $request->input( 'event_id' ) ? sc_get_fake_event_for_user() : sc_user_can_edit_event( $request->input( 'event_id' ) );
		$arrangement->user_id = $arrangement->event->user_id;

		$items = $request->input( 'items' );
		foreach ( $items as $_item ) {
			if ( empty( $_item['keypair'] ) ) {
				continue;
			}
			
			$keypair = explode( '_', $_item['keypair'] );
			$item_id = empty( $keypair[0] ) ? 0 : intval( $keypair[0] );
			$variation_id = empty( $keypair[1] ) ? 0 : intval( $keypair[1] );
			
			try {
				$item = sc_get_user_item_qb( $arrangement->event->user_id, $item_id )->withTrashed()->firstOrFail();
				// fail if the item does not have the entered variation assigned (spoof attempt)
				$item->variations()->withTrashed()->findOrFail( $variation_id ); 
			} catch (Exception $e) {
				continue; // invalid item selected; skip it
			}

			$arrangement_item = new Arrangement_Item();
			$arrangement_item->item()->associate( $item );
			$arrangement_item->item_type = 'Stemcounter\Item';
			$arrangement_item->item_variation_id = $variation_id;
			$arrangement_item->cost = abs( floatval( sc_format_price( $_item['cost'] ) ) );
			if ( $standaloneRecipes ) {
				$arrangement_item->cost = $arrangement_item->item_variation->cost;
				if ( is_null( $arrangement_item->cost ) ) {
					$arrangement_item->cost = $arrangement_item->item_variation->item->default_variation->cost;
				}
			}
			$arrangement_item->quantity = abs( floatval( $_item['quantity'] ) );
			$arrangement->items->add( $arrangement_item );
		}

		$options = array(
			'hardgood_multiple' => $request->input( 'hardgood_multiple' ),
			'fresh_flower_multiple' => $request->input( 'fresh_flower_multiple' ),
			'global_labor_value' => $request->input( 'global_labor_value' ),
			'flower_labor' => $request->input( 'flower_labor' ),
			'hardgood_labor' => $request->input( 'hardgood_labor' ),
			'base_price_labor' => $request->input( 'base_price_labor' ),
			'fee_labor' => $request->input( 'fee_labor' ),
			'rental_labor' => $request->input( 'rental_labor' ),
		);

		$whole_arrangement = Arrangement::where( array(
			'id'       => $request->input( 'id' ),
			'event_id' => $request->input( 'event_id' ),
		) )->with( array(
			'items',
			'items.item' => function( $query ) {
				$query->withTrashed();
			},
			'recipes'
		) )->first();
		$whole_arrangement->event = $arrangement->event;
		$whole_arrangement->items->transform(function( $arrangement_item ) use ( $arrangement ) {
			$arrangement_item->arrangement = $arrangement;

			return $arrangement_item;
		});
		$database_total_arrangement_price = sc_format_price( $whole_arrangement->get_calculated_price( false, $options ) );
		$database_current_recipe_price = sc_format_price( $whole_arrangement->get_calculated_price( false, $options, $request->input( 'recipe_id' ) ) );
		$modal_current_total = sc_format_price( $arrangement->get_calculated_price( false, $options ) );
		$total_item_price = $database_total_arrangement_price - $database_current_recipe_price + $modal_current_total;

		$response['payload'] = array(
			'cost' => sc_format_price( $arrangement->get_cost() ),
			'total' => $modal_current_total,
			'total_item_price' => $total_item_price,
		);

		$response['message'] = 'Success.';
		$response['success'] = true;
	} catch (Exception $e) {
		$response['success'] = false;
		$response['message'] = 'There was a problem with your request. Please try again later.';
		$response['code'] = $e->getCode();
	}

	wp_send_json( $response );
}
add_action('wp_ajax_sc_calculate_arrangement_totals', 'sc_ajax_calculate_arrangement_totals');

function sc_ajax_get_all_arrangement_items() {
	sc_clean_input_slashes();
	$request = Request::capture();

	$response = array(
		'success'=>false,
		'code'=>0,
		'message'=>'An unknown error occurred.',
		'payload'=>array(),
	);

	try {
		$event = sc_user_can_edit_event( $request->input( 'event_id' ) );

		$arrangements = $event->arrangements()->with( array( 'items', 'items.item', 'items.item.default_variation', 'items.item_variation', 'items.item_variation.item' ) )->get();
		$items = array();
		foreach ($arrangements as $arrangement) {
			foreach ($arrangement->items as $arrangement_item) {
				$vi = $arrangement_item->toArray();

				$vi['item_type'] = $arrangement_item->item->type;
				$vi['item_variation_id'] = $arrangement_item->item_variation->id;
				$vi['item_variation_name'] = 'Default' != $arrangement_item->item_variation->name ? ' - ' . $arrangement_item->item_variation->name : '';

				// reset cost to item original and not override price
				$vi['cost'] = ! is_null( $arrangement_item->item_variation->cost ) ? $arrangement_item->item_variation->cost : $arrangement_item->item->default_variation->cost;
				$vi['quantity'] = 0;
				$items[] = $vi;
			}
		}

		$signatures = array();
		$unique_items = array();
		foreach ($items as $item) {
			$signature = $item['item_type'] . '|' . $item['item_id'] . '|' . $item['item_variation_id'];
			if (isset($signatures[$signature])) {
				continue; // diplicate found
			}
			$signatures[$signature] = $signature;

			$unique_items[] = $item;
		}
		
		$response['payload'] = $unique_items;
		$response['success'] = true;
	} catch (Exception $e) {
		$response['success'] = false;
		$response['message'] = 'There was a problem with your request. Please try again later.';
		$response['code'] = $e->getCode();
	}

	wp_send_json( $response );
}
add_action('wp_ajax_sc_get_all_arrangement_items', 'sc_ajax_get_all_arrangement_items');


function sc_render_event_arrangements( $content ) {
	$request = Request::capture();

	$user_id = intval( get_current_user_id() );
	$event_id = intval( $request->input( 'eid' ) );

	$event = Event::where(array(
		'id' => $event_id,
	))->with(array(
		'arrangements' => function($query){
			$query->orderBy( 'order', 'asc' );
		},
		'arrangements.items',
		'arrangements.items.item_variation',
		'arrangements.items.item_variation.item',
	))->first();

	$event->arrangements->sortBy( function($arrangement){
		return $arrangement->order;
	} );

	ob_start(); ?>
	<div class="row event-actions arrangements-page">
		<div class="col-lg-10">
			<h4>Event: "<?php echo esc_html( $event->name ); ?>"</h4>
		</div>
		<div class="col-lg-2">
			<button type="button" align="right" style="margin-top: 10px; margin-bottom:10px; width:100%;" class="btn btn-primary btn-lg add-arrangement-button">
				Add Arrangement
			</button>
		</div>
	</div>

	<div class="container-fluid">
		<div id="m-container" class="row">
			
			<?php if ( $event->arrangements->count() == 0 ) : ?>
				<h5>You have no arrangements, yet.</h5>
				<br />
			<?php else : ?>
				<?php $i = 0;
				foreach ( $event->arrangements as $i => $arrangement ) : ?>
					<div class="col-md-6 grid-item arrangement-list-item arrangement-math-hidden isotopey">
						<div class="content-panel content-panel-stem-count col-xs-12">
							<div class="row">
								<div class="pull-right col-xs-3 arrangement-list-item-actions">
									<button class="btn btn-primary btn-xs edit-bqt toggle-arrangement-math-button">
										<i class="fa fa-calculator"></i>
									</button>
									<button class="btn btn-primary btn-xs edit-bqt edit-arrangement-button" data-id="<?php echo esc_attr( $arrangement->id ); ?>">
										<i class="fa fa-pencil"></i>
									</button>
								</div> 
								<h4 class="col-xs-9"><?php echo esc_html( $arrangement->name ); ?></h4>
							</div>
							<h5>Quantity: <?php echo esc_html( $arrangement->quantity ); ?></h5>
							
							<div class="hidden-arrangement-photos">
								<?php 
								$arrangement_photos = Arrangement_Photo::where('arrangement_id', '=', $arrangement->id)->get();

								$photos = array();
								foreach ($arrangement_photos as $photo) {
									/* if the image exists */
									$image = wp_get_attachment_image_src($photo->photo_id);
									if (!empty($image)) {
										$image_id = $photo->photo_id;
										$image_url = wp_get_attachment_image_src($image_id, 'medium');
										$image_url = $image_url[0];

										$photos[$photo['modal_position']] = array(
											'image_id' => $image_id,
											'image_url' => $image_url,
										);		
									} else {
										/* delete it from the arrangement_photos if it doesn't */
										Arrangement_Photo::where(array(
											'photo_id' => $photo['photo_id']
										))->delete();
									}
								}

								?>
								<table class="hidden-arrangement-photos">
									<tbody>
										<tr>
										<?php foreach ($photos as $_i => $photo): ?>
											<td>
												<style type="text/css">
													.hidden-arrangement-photo-<?php echo $i . '-' . $_i; ?> {
														position: relative;
													    max-width: 142px;
													    width: 142px;
													    height: 142px;
													    max-height: 142px;
													    overflow: hidden;
													}
													.hidden-arrangement-photo-<?php echo $i . '-' . $_i; ?>:before {
														content: '';
														position: absolute;
														top: 50%;
														left: 50%;
														-ms-transform: translate(-50%, -50%);
    													-webkit-transform: translate(-50%, -50%);
														transform: translate(-50%, -50%);
														display: list-item;
													    list-style-image: url('<?php echo $photo['image_url']; ?>');
													    list-style-position: inside;
													}
												</style>
												<div class="hidden-arrangement-photo col-sm-3">
													<div class="hidden-arrangement-photo-<?php echo $i . '-' . $_i; ?>"></div>
												</div>
											</td>
										<?php endforeach; ?>
										</tr>
									</tbody>
								</table>
							</div>
							<table class="table table-striped table-advance table-hover arrangement-tables">
								<thead>
									<tr>
										<th><i class="fa fa-bullhorn"></i> Item</th>
										<th style="width: 75px;">Quantity</th>
										<th>Cost</th>
										<th class="arrangement-math">Multiplier</th>
										<th class="arrangement-math">Labor</th>
										<th class="arrangement-math" style="width: 80px;">Subtotal</th>
										<th class="non-arrangement-math" style="width: 80px;">Type</th>
									</tr>
								</thead>

								<tbody>
									<?php foreach ( $arrangement->items as $arrangement_item ) : ?>
										<?php
										$invoicing_multiplier = $arrangement_item->get_invoicing_multiplier();

										$cost = $arrangement_item->cost;
										$cost_and_markup = $cost + $arrangement_item->get_markup(false);
										$cost_and_markup_and_labor = $cost_and_markup + $arrangement_item->get_labor(false);
										$subtotal = $arrangement_item->get_calculated_price();
										?>
										<tr>
											<td><?php echo esc_html($arrangement_item->get_full_name()); ?></td>
											<td><?php echo esc_html($arrangement_item->quantity); ?></td>
											<td><?php echo esc_html(sc_format_price($arrangement_item->cost, true)); ?></td>
											<td class="arrangement-math" data-tooltip="<?php echo esc_attr(sc_format_price($cost_and_markup, true)); ?>">x<?php echo esc_html($invoicing_multiplier); ?></td>
											<td class="arrangement-math" data-tooltip="<?php echo esc_attr(sc_format_price($cost_and_markup_and_labor, true)); ?>"><?php echo $arrangement_item->get_labor_percent(); ?>%</td>
											<td class="arrangement-math"><?php echo sc_format_price($subtotal, true); ?></td>
											<td class="non-arrangement-math">
												<span class="label label-info label-mini"><?php echo esc_html(sc_get_item_type($arrangement_item->item, true, $arrangement_item)); ?></span>
											</td>           
										</tr>
									<?php endforeach; ?>
								</tbody>
								
								<?php
								$subtotal = $arrangement->get_cost() + $arrangement->get_markup() + $arrangement->get_labor();
								$calculated_price = $arrangement->get_calculated_price(false);
								$final_price = $arrangement->get_final_price(false);
								$final_price_style = '';
								if ($final_price < $calculated_price) {
									$final_price_style = 'color: red; font-weight: bold;';
								}
								?>
								<tfoot>
									<tr>
										<td class="arrangement-math"></td>
										<td class="arrangement-math"></td>
										<td colspan="3" style="text-align: right;">
											<span class="arrangement-math">Wholesale Cost:<br /></span>
											<span>Subtotal:</span><br />
											<span class="arrangement-math">Card Rate:<br /></span>
											<span>Final price:</span><br />
											<span class="arrangement-math">Taxable:<br /></span>
											<span>Include:</span>
										</td>
										<td style="text-align: left;">
											<span class="arrangement-math"><?php echo sc_format_price($arrangement->get_cost(), true); ?><br /></span>
											<span><?php echo sc_format_price($subtotal, true); ?></span><br />
											<span class="arrangement-math">+ <?php echo sc_format_price($arrangement->get_card_rate(), true); ?><br /></span>
											<span style="<?php echo esc_attr($final_price_style); ?>"><?php echo sc_format_price($final_price, true); ?></span><br />
											<span class="arrangement-math"><?php echo ($arrangement->tax_enabled()) ? 'Yes' : 'No'; ?><br /></span>
											<span><?php echo ($arrangement->include == 1) ? 'Yes' : 'No'; ?></span>
										</td>
									</tr>
								</tfoot>
							</table>
							<div class="hidden-arrangement-note">
								<h4>Notes:</h4>
								<?php echo nl2br($arrangement->note); ?>
							</div>
						</div>
					</div>
					
				<?php $i ++; endforeach; ?>
			<?php endif; ?>
			<div class="grid-sizer col-md-6"></div>
		</div>
	</div>
	<?php
	return ob_get_clean();
}
add_filter( 'sc/app/content/event', 'sc_render_event_arrangements' );


function sc_arrangements_order_ajax() {
	$request = Request::capture();
	$order = $request->input( 'order', array() );
	$event_id = intval( $request->input( 'event_id' ) );

	if ( empty( $order ) ) {
		wp_send_json_error( array( 'response' => 'The object apears to be empty!' ) );
	}

	try {
		$event = Event::where(array(
			'id' => $event_id,
			'user_id' => get_current_user_id(),
		))->with( 'arrangements' )->firstOrFail();

		foreach ($event->arrangements as $arrangement) {
			$arrangement->order = intval( $order[ $arrangement->id ] );
			$arrangement->save();
		}

		wp_send_json_success();
	} catch (Exception $e) {
		wp_send_json_error( array( 'response' => 'Security error - please contact support.' ) );
	}
}
add_action( 'wp_ajax_sc_arrangement_order', 'sc_arrangements_order_ajax' );

function sc_get_user_items_json( $user_id = false, $with_libraries = false ) {
	$user_id = $user_id ? $user_id : intval( get_current_user_id() );
	
	$items = Item::get_user_items( $user_id, false );

	if ( $with_libraries ) {
		//Get User Library Items
		$user_libraries = get_user_meta( $user_id, 'sc_user_libraries', true );
		$user_libraries = is_array( $user_libraries ) ? $user_libraries : array();
		if ( ! empty( $user_libraries ) ) {
			$items->orWhere(function($query) use ($user_libraries){
				$query->whereIn( 'user_id', $user_libraries );
			});

			array_unshift( $user_libraries, $user_id );
			$user_libraries = implode( ',', array_map( 'intval', $user_libraries ) );

			$items->orderByRaw( "FIELD (user_id, {$user_libraries})" );
		}
	}

	$items->orderBy( 'name', 'asc' );
	return $items->get()->toJSON();
}

function sc_create_arrangement_recipe() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$arrangement_id = intval( $request->input( 'arrangement_id' ) );

	try {
		$recipe = new Arrangement_Recipe;
		$recipe->arrangement_id = $arrangement_id;
		$recipe->name = 'Recipe';
		$recipe->save();

		$r->add_payload( 'name', $recipe->name );
		$r->add_payload( 'recipe_id', $recipe->id );
	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_arrangement_recipe', 'sc_create_arrangement_recipe', 10 );

function sc_arrangement_recipe_delete() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$recipe_id = $request->input( 'recipe_id', 0 );

	try {
		$recipe = Arrangement_Recipe::where(array(
			'id' => $recipe_id,
		))->delete();

		$items = Arrangement_Item::where(array(
			'arrangement_recipe_id' => $recipe_id,
		))->delete();

		$r->add_payload( 'recipe_id', $recipe_id );
	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_arrangement_recipe_delete', 'sc_arrangement_recipe_delete', 10 );

function sc_duplicate_arrangement( $_arrangement, $event, &$inc_recipes = array(), &$inc_items = array(), &$inc_photos = array() ) {
	$updated_arrangement = $_arrangement->replicate();
	$updated_arrangement->save();

	foreach ( $_arrangement->photos as $photo ) {
		$_new_photo = $photo->replicate();
		$_new_photo->arrangement_id = $updated_arrangement->id;
		$_new_photo->save();

		$image_id = $photo->photo_id;
		$image_url = wp_get_attachment_image_src( $image_id, 'medium' );
		$image_url = $image_url[0];
		$full_image = wp_get_attachment_image_src( $image_id, 'full' );

		$inc_photos[] = array(
			'image_id' => $image_id,
			'image_url' => $image_url,
			'full_image_src' => $full_image[0],
			'full_image_w' => $full_image[1],
			'full_image_h' => $full_image[2],
		);
	}

	foreach ( $_arrangement->recipes as $recipe ) {
		$new_recipe = $recipe->replicate();
		$new_recipe->arrangement_id = $updated_arrangement->id;
		$new_recipe->save();
		$inc_recipes[] = array( 'id' => $new_recipe->id, 'name' => $new_recipe->name );

		foreach ( $_arrangement->items as $arrangement_item ) {
			if ( $arrangement_item->arrangement_recipe_id == $recipe->id ) {
				$_new_arr_item = $arrangement_item->replicate();
				$_new_arr_item->arrangement_id = $updated_arrangement->id;
				$_new_arr_item->arrangement_recipe_id = $new_recipe->id;
				$_new_arr_item->save();
			} else {
				continue;
			}
		}
	}
	
	$updated_arrangement = Arrangement::where(array(
		'id' 		=> $updated_arrangement->id,
		'user_id'	=> $event->user_id,
	))->with(array(
		'recipes',
		'photos',
		'items',
		'items.item',
		'items.item_variation',
		'items.item_variation.item',
	))->first();

	$updated_arrangement->event = $event;

	foreach ( $updated_arrangement->items as $arrangement_item ) {
		$arrangement_item->arrangement = $updated_arrangement;
		$cost_and_markup = $arrangement_item->cost + $arrangement_item->get_markup( false );
		
		$vi = $arrangement_item->toArray();
		$vi['item_type'] = $arrangement_item->item->type;
		$vi['item_variation_name'] = 'Default' != $arrangement_item->item_variation->name ? ' - ' . $arrangement_item->item_variation->name : '';
		$vi['item_variation_id'] = $arrangement_item->item_variation->id;
		
		$key = $arrangement_item->item_id . '_' . $vi['item_variation_id'];

		$type = sc_get_item_type( $arrangement_item->item, true );

		$item_attachment = sc_get_variation_attachment( $arrangement_item->item_variation, $arrangement_item->item->default_variation );

		$inc_items[] = array(
			'id'                        => $arrangement_item->id,
			'recipe_id'					=> $arrangement_item->arrangement_recipe_id,
			'name'                      => $arrangement_item->get_full_name(),
			'quantity'                  => $arrangement_item->quantity,
			'invoicing_multiplier'      => $arrangement_item->get_invoicing_multiplier(),
			'cost'                      => sc_format_price( $arrangement_item->cost ),
			'cost_and_markup'           => $cost_and_markup,
			'cost_and_markup_and_labor' => $cost_and_markup + $arrangement_item->get_labor( false ),
			'subtotal'                  => $arrangement_item->get_calculated_price(),
			'type'                      => $type,
			'type_raw'                  => str_replace( ' ', '_', strtolower( $type ) ),
			'item_type'                 => $vi['item_type'],
			'item_id'                   => $vi['item_id'],
			'item_variation_id'         => $vi['item_variation_id'],
			'item_variation_name'       => ! empty( $vi['item_variation_name'] ) ? $vi['item_variation_name'] : '',
			'item_pricing_category'     => $arrangement_item->item->pricing_category,
			'itemKeypair'               => $key,
			'attachment'				=> $item_attachment,
		);
	}

	unset( $updated_arrangement->event );

	return $updated_arrangement;
}
