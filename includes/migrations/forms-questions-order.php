<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Form;

if ( ! Capsule::schema()->hasColumn( 'form_questions', 'order' ) ) {
	Capsule::schema()->table( 'form_questions', function( $table ){
		$table->integer('order')
			->unsigned()
			->after('required');
	} );
	echo 'Added the Questions order column.<br />';

	$forms = Form::with( array(
		'questions' => function( $query ) {
			$query->orderBy( 'id', 'ASC' );
		},
	) )->get();

	foreach ( $forms as $form ) {
		printf( 'Updating order for questions in form #%d ...<br/>', $form->id );

		foreach ( $form->questions as $i => $question ) {
			$question->order = $i;
			$question->save();
		}
	}
}