<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Event_Version;
global $wpdb;
global $illuminate_capsule;

$migration_ver = 2;

if ( empty( $_GET['offset'] )  && $_GET['offset'] != 0 ) {
 echo '<br/>=======OFFSET IS EMPTY=======<br/>';
 exit;
}

if ( empty( $_GET['number'] ) ) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$versions = Event_Version::take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'asc')->get();

if ( $versions->count() ) {
	echo 'Migrating versions ...<br />';
	foreach ( $versions as &$version ) {
		$event_data = json_decode( $version->event_data );
		
		if ( $event_data ) {
			$event = $event_data->event;
			$eventCost = $event_data->eventCost;

			echo '<br />- Version id: ' . $version->id . ' for event id: ' . $version->event_id . '<br />';
			if ( $migration_ver <= intval( $event->migration_ver ) ) {
				echo 'Already migrated<br />';
				continue;
			}

			if ( ! empty( $eventCost->delivery_id ) || 0 < intval( $eventCost->delivery )  ) {
				$arrangement = array(
					'id' 				=> -1,
					'addon' 			=> 0,
					'quantity'			=> 1,
					'include' 			=> 1,
					'note' 				=> '',
					'photos'			=> array(),
					'items'				=> array(),
					'order'				=> count( $event->arrangements ),
					//'is_percentage'		=> ( 'amount' == $eventCost->delivery_type || $eventCost->delivery_tax ) ? 0 : 1,
					'is_percentage'		=> 0,
					'total'				=> $eventCost->delivery,
					'subTotal'			=> $eventCost->delivery,
					'override_cost'		=> $eventCost->delivery,
					'tax'				=> absint( $eventCost->delivery_tax ),
					'name'				=> "Delivery",
					'invoicing_category_id' => null,
					'hardgood_multiple'	=> null,
					'fresh_flower_multiple'	=> null,
					'global_labor_value'	=> null,
					"flower_labor"		=>	null,
					"hardgood_labor"	=> null,
					"base_price_labor"	=> null,
					"fee_labor"			=> null,
					"rental_labor"		=> 0,
					"recipes"			=> array(),
				);
				echo "Delivery {$eventCost->delivery_type}: {$arrangement['override_cost']}</br>";
				$event->arrangements[] = $arrangement;
				$eventCost->subtotal += $arrangement['subTotal'];
			} else {
				//echo 'No delivery</br>';
			}

			if ( ! empty( $eventCost->discount_id ) || 0 < intval( $eventCost->discount ) ) {
				$arrangement = array(
					'id' 				=> -1,
					'addon' 			=> 0,
					'quantity'			=> 1,
					'include' 			=> 1,
					'note' 				=> '',
					'photos'			=> array(),
					'items'				=> array(),
					'order'				=> count( $event->arrangements ),
					'is_percentage'		=> 0,
					'total'				=> -1 * $eventCost->discount,
					'subTotal'			=> -1 * $eventCost->discount,
					'override_cost'		=> -1 * $eventCost->discount,
					'name'				=> 'Discount',
					'invoicing_category_id' => null,
					'hardgood_multiple'	=> null,
					'fresh_flower_multiple'	=> null,
					'global_labor_value'	=> null,
					"flower_labor"		=>	null,
					"hardgood_labor"	=> null,
					"base_price_labor"	=> null,
					"fee_labor"			=> null,
					"rental_labor"		=> 0,
					"recipes"			=> array(),
				);
				echo "Discount {$eventCost->discount_type}: {$arrangement['override_cost']}</br>";
				$event->arrangements[] = $arrangement;
				$eventCost->subtotal += $arrangement['subTotal'];
				
			} else {
				//echo 'No discount</br>';
			}

			$event->migration_ver = $migration_ver;
			$event_data->event = $event;
			$event_data->eventCost = $eventCost;

			$version->event_data = json_encode( $event_data );
			$version->save();
		}
	}
} else {
	
	echo "Migration done";
	exit;
}
?>

<script type="text/javascript">
setTimeout(function() {
	window.location.href = <?php echo json_encode( add_query_arg( 'offset', $_GET['offset'] + $_GET['number'] ) ); ?>;
}, 1500);
</script>

<?php
exit;

//app/wp-admin/admin-ajax.php?action=sc_migration&migration=delivery-discounts-line-items1&offset=0&number=100