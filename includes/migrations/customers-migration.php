<?php
use Stemcounter\Customer;
use Stemcounter\Event;
use Stemcounter\Meta;
use Illuminate\Database\Capsule\Manager as Capsule;

ini_set('memory_limit', '-1');
global $wpdb;

if ( ! Capsule::schema()->hasColumn( 'customers', 'user_id' ) ) {

	if ( ! Capsule::schema()->hasColumn( 'customers', 'user_id' ) ) {
		Capsule::schema()->table( 'customers', function( $table ){
			$table->integer('user_id')->after('id');
		} );
	}

	if ( ! Capsule::schema()->hasColumn( 'customers', 'address' ) ) {
		Capsule::schema()->table( 'customers', function( $table ){
			$table->text('address')->after('email');
		} );
	}

	if ( ! Capsule::schema()->hasColumn( 'customers', 'notes' ) ) {
		Capsule::schema()->table( 'customers', function( $table ){
			$table->text('notes')->after('address');
		} );
	}
	
	$wpdb->query("UPDATE `{$wpdb->prefix}sc_customers` as customers 
		INNER JOIN `{$wpdb->prefix}sc_events` as events ON events.customer_id = customers.id
		SET customers.user_id = events.user_id");
}


if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$customers = Customer::take($_GET['number'])->offset($_GET['offset'])->withTrashed()->orderBy('user_id', 'asc')->get();

if ($customers->isEmpty()) {
	echo '<br/>=======CUSTOMER MIGRATION ENDED=======<br/>';
} else {
	echo '<br/>Migrating Customers...<br/>';
	foreach ($customers as $customer) {
		try {
			$customer_exists = Customer::where('id', '=', $customer->id)->first();

			if (is_null($customer_exists)) {
				continue;
			}

			$customer_duplicate_names = Customer::where('id', '>', $customer->id)
		        ->where('first_name', '=', $customer->first_name)
		        ->where('last_name', '=', $customer->last_name)
		        ->where('email', '=', $customer->email)
		        ->where('user_id', '=', $customer->user_id)
		        ->orderBy('id', 'asc')->get();
		        
			if (!$customer_duplicate_names->isEmpty()) {
				foreach ($customer_duplicate_names as $d_customer) {
					$event = Event::where('customer_id', '=', $d_customer->id)->first();

					if (!is_null($event)) {
						$event_meta = new Meta();
						$event_meta->type = 'event';
						$event_meta->type_id = $event->id;
						$event_meta->meta_key = 'linked_customer';
						$event_meta->meta_value = $customer->id;
						$event_meta->save();
					}
					$d_customer->delete();
				}
			}

			$event = Event::where('customer_id', '=', $customer->id)->first();

			if (!is_null($event)) {
				$event_meta = new Meta();
				$event_meta->type = 'event';
				$event_meta->type_id = $event->id;
				$event_meta->meta_key = 'linked_customer';
				$event_meta->meta_value = $customer->id;
				$event_meta->save();
			}
		} catch (Exception $e) {
			echo $e;
		}
	}
}

if ($customers->isEmpty()) {
	$wpdb->query("ALTER TABLE `{$wpdb->prefix}sc_events`
		DROP COLUMN `customer_id`");
	Customer::where(array(
		'first_name' => 'DELETECUSTOMER',
		'last_name' => 'DELETECUSTOMER',
		'email' => 'DELETECUSTOMER'
	))->delete();
	echo '<br/>=======MIGRATION ENDED=======<br/>';
	exit;
}

?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;