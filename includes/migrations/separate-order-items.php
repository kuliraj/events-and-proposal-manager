<?php
use Stemcounter\Order;
use Stemcounter\Order_Item;

use Illuminate\Database\Capsule\Manager as Capsule;

// app/wp-admin/admin-ajax.php?action=sc_migration&migration=separate-order-items&offset=0&number=30

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$orders = Order::with( 'items' )->take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'asc')->get();

// var_dump( $orders );

if ( $orders->isEmpty() ) {
	echo '<br/>		.	MIGRATION ENDED		.	<br/>';
	exit;	
} else {
	echo '<br/>...browsing through orders. Please wait!<br/>';
	foreach ( $orders as $order ) {
		// var_dump( $order );
		if ( ! $order->items->isEmpty() ) {
			printf( '==== IGNORING ORDER %d, SINCE IT ALREADY HAS ASSOCIATED ITEMS ====<br />', $order->id );
			continue;
		}

		printf( '<br />== Migrating Order %d ==<br />', $order->id );

		$events = json_decode( $order->flowers_items, true );
		$item_keys = array( 'flower', 'service' );
		$imported_items = 0;
		// var_dump( $events );
		foreach ( $events as $event_id => $item_types ) {
			foreach ( $item_types as $item_type => $_items ) {
				if ( empty( $_items ) ) {
					continue;
				}

				foreach ( $_items as $_item ) {
					$new_item = new Order_Item;
					$new_item->order_id = $order->id;
					$new_item->event_id = $event_id;
					$new_item->item_id = null;
					$new_item->name = $_item['name'];
					$new_item->pricing_category = $_item['type'];
					$new_item->quantity = $_item['quantity'];
					$new_item->cost = $_item['cost'];
					$new_item->items_su = ! empty( $_item['items_su'] ) ? $_item['items_su'] : 1;
					$new_item->su_needed = ! empty( $_item['su_needed'] ) ? $_item['su_needed'] : 1;
					$new_item->leftovers = ! empty( $_item['leftovers'] ) ? $_item['leftovers'] : 0;
					$new_item->discarded = 0;

					$new_item->save();
					$new_item = null;

					$imported_items ++;
				}
			}
		}

		printf( 'Migrated %d items to the new table structure.<br />', $imported_items );

		// $event->arrangements->sortBy( function($arrangement){
		// 	return $arrangement->order;
		// } );

		/*foreach ( $arrangement->recipes as $i => $recipe ) {
			if (empty($recipe->name)) {
			$recipe->name = 'Recipe ' . ( $i + 1 );
				$recipe->save();
				printf( 'Recipe <strong>%d / %d</strong> name set to <strong>%s</strong><br />', $recipe->id, $recipe->arrangement_id, $recipe->name );
			} else {
				printf( 'Recipe <strong>%d / %d</strong> name already set to <strong>%s</strong>, skipping...<br />', $recipe->id, $recipe->arrangement_id, $recipe->name );
			}
		}*/
	}

} ?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;