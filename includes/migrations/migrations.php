<?php
function sc_execute_migration() {
	if (!current_user_can('manage_options')) {
		echo 'Insufficient permissions.';
		exit;
	}

	$available_migrations = array(
		// 'new-database-schema', // no longer needed
		'labor-control',
		'arrangement-price-override',
		'multiple-tax-rates',
		'multiple-tax-rates-2',
		'arrangement-override-fix',
		'invoicing-migration',
		'invoicing-migration-2',
		'payments',
		'arrangement-order',
		'event_state',
		'vendors-vendors',
		'vendors-events',
		'vendors-duplicate-names',
		'customers-migration',
		'onboarding-migration',
		'order-bug-fix',
		'payments-bug-fix',
		'tax-rate-bug-fix',
		'arrangement-values-bug-fix',
		'arrangement-override-price-fix',
		'create-final-payments',
		'event_cover',
		'event_notes',
		'indexing',
		'discounts',
		'deliveries',
		'separate-cc-fee',
		'event-deliveries-fix',
		'events-versioning',
		'signature-support',
		'event-payments',
		'multiple-recipes',
		'item-photo',
		'arrangement-addon',
		'recipe-names',
		'default-delivery',
		'separate-order-items',
		'order-fulfilment-date',
		'user-recipes',
		'new-item-structure',
		'new-item-structure-migrate-ids',
		'tax-rate-value-length',
		'event-hidden-payments',
		'create-db-indexes2',
		'add-user-id-to-arrangements',
		'standalone-recipes-update',
		'tags-color-migrations',
		'arrangement-hide-price',
		'multiple-event-tax-rates',
		'default-tax-rate',
		'arrangements-negative-prices',
		'arrangements_percents',
		'event-hide-item-prices',
		'payment-inequity',
		'disable-meta-soft-deletes',
		'delivery-discounts-line-items1',
		'delivery-discounts-line-items2',
		'profile-to-recipe-deliveries',
		'profile-to-recipe-discounts',
		'event-hidden-contract',
		'forms-confirmation-msg-col',
		'cardfee-to-payments-versions',
		'cardfee-to-payments-events',
		'forms-questions-order',
		'percent-item-applies-all',
		'deleted-field-properties',
		'default_email_template',
	);

	$migration = empty($_REQUEST['migration']) ? '' : $_REQUEST['migration'];
	if (!in_array($migration, $available_migrations)) {
		echo 'Invalid migration specified.';
		exit;
	}

	include_once($migration . '.php');
	exit;
}
add_action('wp_ajax_sc_migration', 'sc_execute_migration');

// Migration address:
// app/wp-admin/admin-ajax.php?action=sc_migration&migration=MIGRATION_NAME&offset=0&number=100

function sc_migration_is_cli() {
	global $is_cli;

	return empty( $is_cli ) ? false : true;
}

function sc_web_cli_out( $web_output, $cli_output = false ) {
	global $is_cli;

	if ( empty( $is_cli ) ) {
		echo $web_output . '<br />';
	} else {
		if ( $cli_output === false ) {
			$cli_output = strip_tags( $web_output );
		}
		//\WP_CLI::log( \WP_CLI::colorize( 'text %gGreen text%n some more %rRed text%n...' ) );
		\WP_CLI::log( $cli_output );
	}
}