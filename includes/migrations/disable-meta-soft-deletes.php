<?php

use Illuminate\Database\Capsule\Manager as Capsule;
global $wpdb;

// Delete all metas that have been soft deleted
printf( "Removed <strong>%d</strong> soft-deleted meta rows.<br />", $wpdb->query( "DELETE FROM {$wpdb->prefix}sc_meta WHERE deleted_at IS NOT NULL" ) );

if ( Capsule::schema()->hasColumn( 'meta', 'deleted_at' ) ) {
	Capsule::schema()->table( 'meta', function( $table ){
		$table->dropColumn( 'deleted_at' );
	} );

	echo 'Dropped the <code>deleted_at</code> column from the meta table.<br />';
} else {
	echo 'The <code>deleted_at</code> column has already been dropped from the meta table.<br />';
}

echo 'All done!';
