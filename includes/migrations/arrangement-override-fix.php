<?php
use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->table('arrangements', function($table){
	$table->float('override_cost')->default(0)->change();
});

echo 'Migration done.';
exit;