<?php
use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'arrangements', 'addon' ) ) {
	Capsule::schema()->table( 'arrangements', function( $table ){
		$table->integer('addon')
			->unsigned()
			->default(0);
	} );
}
echo 'Migration done.';
exit;