<?php
use Stemcounter\Event;
use Stemcounter\Scheduled_Payment;
use Illuminate\Database\Capsule\Manager as Capsule;

ini_set('memory_limit', '-1');

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$events = Event::take($_GET['number'])->offset($_GET['offset'])->orderBy('user_id', 'asc')->get();

if ($events->isEmpty()) {
	echo '<br/>=======SCHEDULED PAYMENTS MIGRATION ENDED=======<br/>';
	exit;
} else {
	echo '<br/>Migrating Scheduled Payments...<br/>';
	foreach ($events as $event) {
		$scheduled_payments = Scheduled_Payment::where(array(
			'event_id' => $event->id
		))->get();

		if (!$scheduled_payments->isEmpty()) {

			$total_scheduled_amount = 0;

			foreach ($scheduled_payments as $i => $payment) {
				if($i !== count($scheduled_payments) - 1) {
					if ($payment->payment_amount_type === 'currency') {
						$total_scheduled_amount += $payment->payment_amount;
					} else if ($payment->payment_amount_type === 'percentage') {
						$total_scheduled_amount += $event->get_total() * ($payment->payment_amount / 100);
					}
				}
			}

			$final_scheduled_payment = $scheduled_payments[count($scheduled_payments) - 1];
			$final_scheduled_payment->payment_amount = $event->get_total() - $total_scheduled_amount;
			$final_scheduled_payment->save();

		}
	}
}

?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;