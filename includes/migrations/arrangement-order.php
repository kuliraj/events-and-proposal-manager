<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Event;

if ( ! Capsule::schema()->hasColumn( 'arrangements', 'order' ) ) {
	Capsule::schema()->table( 'arrangements', function( $table ){
		$table->integer('order')
			->unsigned();
	} );

	$events = Event::orderBy('date', 'asc')
		->with('arrangements')
		->get();

	foreach ( $events as $event ) {
		foreach ($event->arrangements as $i => $arrangement) {
			$arrangement->order = $i;
			$arrangement->save();
		}
	}
}

echo 'Migration done.';
exit;