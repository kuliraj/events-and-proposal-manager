<?php
use Illuminate\Database\Capsule\Manager as Capsule;

if ( ! Capsule::schema()->hasColumn( 'arrangement_recipes', 'user_recipe_id' ) ) {
	Capsule::schema()->table( 'arrangement_recipes', function( $table ){
		$table->integer('user_recipe_id')
			->unsigned()
			->default(0);
	} );
}

echo 'Migration done.';
exit;