<?php
use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'flowers', 'attachment_id' ) ) {
	Capsule::schema()->table( 'flowers', function( $table ){
		$table->integer( 'attachment_id' )
			->unsigned();
	} );
}

if ( ! Capsule::schema()->hasColumn( 'services', 'attachment_id' ) ) {
	Capsule::schema()->table( 'services', function( $table ){
		$table->integer( 'attachment_id' )
			->unsigned();
	} );
}

echo 'Migration done.';
exit;