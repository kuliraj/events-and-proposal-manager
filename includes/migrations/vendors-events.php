<?php
use Stemcounter\Event;
use Stemcounter\Vendor;
use Stemcounter\Meta;
use Illuminate\Database\Capsule\Manager as Capsule;

ini_set('memory_limit', '-1');

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$events = Event::take($_GET['number'])->offset($_GET['offset'])->orderBy('user_id', 'asc')->get();

if ($events->isEmpty()) {
	echo '<br/>=======EVENTS MIGRATION ENDED=======<br/>';
} else {
	echo '<br/>Migrating Events...<br/>';
	foreach ($events as $event) {
		$vendor = Vendor::where('id', '=', $event->vendor_id)->exists();
		if ($vendor) {
			$event_meta = new Meta();
			$event_meta->type = 'event';
			$event_meta->type_id = $event->id;
			$event_meta->meta_key = 'linked_vendor';
			$event_meta->meta_value = $event->vendor_id;
			$event_meta->save();	
		} else {
			$event_meta = new Meta();
			$event_meta->type = 'event';
			$event_meta->type_id = $event->id;
			$event_meta->meta_key = 'linked_vendor';
			$event_meta->meta_value = 'no_venue';
			$event_meta->save();				
		}
	}
}

if ($events->isEmpty()) {
	echo '<br/>=======MIGRATION ENDED=======<br/>';
	exit;
}

?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;