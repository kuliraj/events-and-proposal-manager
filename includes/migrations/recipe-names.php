<?php
use Stemcounter\Event;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Item;
use Stemcounter\Arrangement_Recipe;

use Illuminate\Database\Capsule\Manager as Capsule;

if ( ! Capsule::schema()->hasColumn( 'arrangement_recipes', 'name' ) ) {
	Capsule::schema()->table( 'arrangement_recipes', function( $table ){
		$table->string('name');
	} );
}

// app/wp-admin/admin-ajax.php?sc_migration&migration=recipe-names&offset=0&number=100

if ( empty( $_GET['eid'] ) ) {
	if (empty($_GET['offset']) && $_GET['offset'] != 0) {
		echo '<br/>=======OFFSET IS EMPTY=======<br/>';
		exit;
	}
	if (empty($_GET['number'])) {
		echo '<br/>=======NUMBER IS EMPTY=======<br/>';
		exit;
	}
}

$arrangements = Arrangement::with('recipes')->take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'desc')->get();

if ( $arrangements->isEmpty() ) {
	echo '<br/>		.	MIGRATION ENDED		.	<br/>';
	exit;	
} else {
	echo '<br/>...browsing through arrangements. Please wait!<br/>';
	foreach ($arrangements as $arrangement) {
		// $event->arrangements->sortBy( function($arrangement){
		// 	return $arrangement->order;
		// } );

		foreach ( $arrangement->recipes as $i => $recipe ) {
			if (empty($recipe->name)) {
			$recipe->name = 'Recipe ' . ( $i + 1 );
				$recipe->save();
				printf( 'Recipe <strong>%d / %d</strong> name set to <strong>%s</strong><br />', $recipe->id, $recipe->arrangement_id, $recipe->name );
			} else {
				printf( 'Recipe <strong>%d / %d</strong> name already set to <strong>%s</strong>, skipping...<br />', $recipe->id, $recipe->arrangement_id, $recipe->name );
			}
		}
	}

}

 ?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;