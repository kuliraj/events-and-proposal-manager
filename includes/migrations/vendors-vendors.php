<?php
use Stemcounter\Event;
use Stemcounter\Vendor;
use Stemcounter\Meta;
use Illuminate\Database\Capsule\Manager as Capsule;

ini_set('memory_limit', '-1');

if ( ! Capsule::schema()->hasColumn( 'vendors', 'company_name' ) ) {
	Capsule::schema()->table( 'vendors', function( $table ){
		$table->renameColumn('name', 'company_name');
	} );

	if ( ! Capsule::schema()->hasColumn( 'vendors', 'user_id' ) ) {
		Capsule::schema()->table( 'vendors', function( $table ){
			$table->integer('user_id')->after('id');
		} );
	}

	if ( ! Capsule::schema()->hasColumn( 'vendors', 'phone' ) ) {
		Capsule::schema()->table( 'vendors', function( $table ){
			$table->string('phone')->after('company_name');
		} );
	}

	if ( ! Capsule::schema()->hasColumn( 'vendors', 'email' ) ) {
		Capsule::schema()->table( 'vendors', function( $table ){
			$table->string('email')->after('phone');
		} );
	}

	if ( ! Capsule::schema()->hasColumn( 'vendors', 'address' ) ) {
		Capsule::schema()->table( 'vendors', function( $table ){
			$table->text('address')->after('email');
		} );
	}
	
	if ( ! Capsule::schema()->hasColumn( 'vendors', 'notes' ) ) {
		Capsule::schema()->table( 'vendors', function( $table ){
			$table->text('notes')->after('address');
		} );
	}

	if ( ! Capsule::schema()->hasColumn( 'vendors', 'website' ) ) {
		Capsule::schema()->table( 'vendors', function( $table ){
			$table->text('website')->after('notes');
		} );
	}

	global $wpdb;
	$wpdb->query("UPDATE `{$wpdb->prefix}sc_vendors` as vendors 
		INNER JOIN `{$wpdb->prefix}sc_events` as events ON events.vendor_id = vendors.id
		SET vendors.user_id = events.user_id");

	$wpdb->query("DELETE FROM `{$wpdb->prefix}sc_vendors`
		WHERE TRIM(company_name) = '' AND TRIM(address) = ''");
}


if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$vendors = Vendor::take($_GET['number'])->offset($_GET['offset'])->orderBy('user_id', 'asc')->get();

if ($vendors->isEmpty()) {
	echo '<br/>=======VENDOR MIGRATION ENDED=======<br/>';
} else {
	echo '<br/>Migrating Vendors...<br/>';
	foreach ($vendors as $vendor) {
		$vendor_meta = new Meta();
		$vendor_meta->type = 'vendor';
		$vendor_meta->type_id = $vendor->id;
		$vendor_meta->meta_key = 'tag';
		$vendor_meta->meta_value = 'venue';
		$vendor_meta->save();
	}
}

if ($vendors->isEmpty()) {
	echo '<br/>=======MIGRATION ENDED=======<br/>';
	exit;
}

?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;






