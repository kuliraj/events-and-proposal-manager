<?php
use Stemcounter\Event;
use Stemcounter\Vendor;
use Stemcounter\Meta;
use Illuminate\Database\Capsule\Manager as Capsule;

ini_set('memory_limit', '-1');

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$vendors = Vendor::take($_GET['number'])->offset($_GET['offset'])->orderBy('user_id', 'asc')->get();

if ($vendors->isEmpty()) {
	echo '<br/>=======VENDOR NAMES MIGRATION ENDED=======<br/>';
} else {
	echo '<br/>Migrating Vendor Names...<br/>';
	foreach ($vendors as $vendor) {
		try {
			$vendor_duplicate_names = Vendor::where('id', '>', $vendor->id)
		        ->where('company_name', '=', $vendor->company_name)
		        ->where('user_id', '=', $vendor->user_id)
		        ->orderBy('id', 'asc')->get();
		        
			if (!$vendor_duplicate_names->isEmpty()) {
				$i = 2;
				foreach ($vendor_duplicate_names as $d_vendor) {
					$d_vendor->company_name = $d_vendor->company_name . ' ' . $i;
					$d_vendor->save();
					$i ++;
				}
			}
		} catch (Exception $e) {
			echo $e;
		}
	}
}


if ($vendors->isEmpty()) {
	global $wpdb;
	$wpdb->query("ALTER TABLE `{$wpdb->prefix}sc_events` 
		DROP COLUMN vendor_id");


	echo '<br/>=======MIGRATION ENDED=======<br/>';
	exit;
}

?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;
