<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Arrangement;
use Stemcounter\Discount;
global $wpdb;
global $illuminate_capsule;


if ( empty( $_GET['number'] ) ) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$discounts = Discount::take($_GET['number'])->orderBy('id', 'asc')->get();

if ( $discounts->count() ) {

	echo 'Migrating discounts from profile to recipes ...<br />';
	foreach ( $discounts as $discount ) {


		try {
			echo '<br />- Discount id: ' . $discount->id . '<br />';
		
			$new_arrangement = new Arrangement;
			$new_arrangement->event_id = 0;
			$new_arrangement->user_id = $discount->user_id;
			$new_arrangement->addon = 0;
			$new_arrangement->quantity = 1;
			$new_arrangement->include = 1;
			$new_arrangement->note = '';
			$new_arrangement->order = 0;
			$new_arrangement->is_percentage = ( 'amount' == $discount->type) ? 0 : 1;
			$new_arrangement->override_cost = -$discount->value;
			$new_arrangement->tax = 1;
			$new_arrangement->name = 'Discount - ' . $discount->name;
			
			$new_arrangement->invoicing_category_id = null;
			$new_arrangement->hardgood_multiple = null;
			$new_arrangement->fresh_flower_multiple = null;
			$new_arrangement->global_labor_value	= null;
			$new_arrangement->flower_labor		=	null;
			$new_arrangement->hardgood_labor	= null;
			$new_arrangement->base_price_labor	= null;
			$new_arrangement->fee_labor			= null;
			$new_arrangement->rental_labor		= 0;
		
			$new_arrangement->save();
			$discount->delete();
		
		} catch ( Exception $e ) {
			echo( $e->getMessage() );
		}
	}

} else {
	echo 'Migration done!';
	exit;
}

?>

<script type="text/javascript">
setTimeout(function() {
	window.location.href = <?php echo json_encode( add_query_arg( 'cachebust', microtime() ) ); ?>;
}, 1500);
</script>

<?php
exit;

//app/wp-admin/admin-ajax.php?action=sc_migration&migration=profile-to-recipe-discounts&number=100