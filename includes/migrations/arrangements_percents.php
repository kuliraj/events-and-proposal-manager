<?php
use Illuminate\Database\Capsule\Manager as Capsule;

try {
	Capsule::schema()->table( 'arrangements', function( $table ) {
		$table->boolean( 'is_percentage' )->default( false );;
	} );
} catch (Exception $e) {
	echo 'Problem with creating new column `is_percentage`.<br />';
	var_dump( $e->getMessage() );
	exit;
}
echo 'Migration done.';