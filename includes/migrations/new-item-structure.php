<?php
use Stemcounter\Flower;
use Stemcounter\Variation;
use Stemcounter\Service;
use Stemcounter\Item;
use Stemcounter\Item_Variation;

use Illuminate\Database\Capsule\Manager as Capsule;

// app/wp-admin/admin-ajax.php?action=sc_migration&migration=new-item-structure&offset=0&number=100

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

global $wpdb;

$had_flowers = false;
$had_services = false;

$flowers_cursor = Flower::with( array( 'variations' => function($query){
	$query->withTrashed();
} ) )->take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'asc')->withTrashed()->cursor();
$services_cursor = Service::take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'asc')->withTrashed()->cursor();

function sc_get_migrated_item_id( $type, $old_id, $variation_id = null ) {
	global $wpdb;

	if ( $variation_id ) {
		$variation_id = $wpdb->prepare( ' AND variation_id = %d', $variation_id );
	} elseif ( is_null( $variation_id ) ) {
		$variation_id = ' AND variation_id IS NULL';
	}

	return $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}sc_migrated_item_ids
		WHERE type = %s AND item_id = %d $variation_id", $type, $old_id ) );
}

echo '<br/>...migrating flowers. Please wait!<br/><table><thead>
<tr>
	<td>ID</td>
	<td>Migrated?</td>
	<td>Migrated Variations</td>
	<td>Created Extra Variation?</td>
</tr></thead><tbody>';

foreach ( $flowers_cursor as $flower ) {
	$had_flowers = true;

	$item = new Item;
	$item->user_id = $flower->user_id;
	$item->name = $flower->name;
	$item->description = '';
	$item->type = 'consumable';
	$item->pricing_category = 'fresh_flower';
	$item->purchase_qty = max( $flower->stems, 1 );
	$item->purchase_unit = 'each';
	$item->recipe_unit = 'each';
	if ( ! is_null( $flower->deleted_at ) && '0000-00-00 00:00:00' != $flower->deleted_at ) {
		$item->deleted_at = $flower->deleted_at;
	}

	$migrated = sc_get_migrated_item_id( 'flower', $flower->id );
	$migrated_variations = 0;

	$extra_variation = 'No';

	if ( ! $migrated ) {
		$item->save();
		$item_id = $item->id;
		$wpdb->insert( "{$wpdb->prefix}sc_migrated_item_ids", array(
			'type' => 'flower',
			'item_id' => $flower->id,
			'variation_id' => null,
			'new_item_id' => $item->id,
		) );
	} else {
		$item_id = $migrated->item_id;
	}/* elseif ( $flower->variations ) {
		$_item = Item::with( 'variations' )->find( $migrated_ids[ "flower_{$flower->id}" ] );
		var_dump( $flower->variations->toArray() );
		var_dump( $_item->variations->toArray() );

		echo "\n\n\n\n\n\n";
	}*/

	if ( ! sc_get_migrated_item_id( 'flower', $flower->id, -1 ) ) {
		$variation = new Item_Variation;
		$variation->name = 'Default';
		$variation->item_id = $item_id;
		$variation->attachment_id = $flower->attachment_id;
		$variation->cost_pu = $flower->cost * $item->purchase_qty;
		$variation->inventory = 0;
		if ( ! is_null( $flower->deleted_at ) && '0000-00-00 00:00:00' != $flower->deleted_at ) {
			$variation->deleted_at = $flower->deleted_at;
		}

		$variation->save();

		$wpdb->insert( "{$wpdb->prefix}sc_migrated_item_ids", array(
			'type' => 'flower',
			'item_id' => $flower->id,
			'variation_id' => -1,
			'new_item_id' => $variation->id,
		) );

		$extra_variation = 'Yes';
	} else {
		$extra_variation = 'No(already migrated)';
	}

	if ( $flower->variations && ! $flower->variations->isEmpty() ) {
		foreach ( $flower->variations as $_variation ) {
			if ( sc_get_migrated_item_id( 'flower', $flower->id, $_variation->id ) ) {
				continue;
			}

			$variation = new Item_Variation;
			$variation->name = $_variation->name;
			$variation->item_id = $item_id;
			$variation->attachment_id = null;
			$variation->cost_pu = null;
			$variation->inventory = null;
			if ( ! is_null( $_variation->deleted_at ) && '0000-00-00 00:00:00' != $_variation->deleted_at ) {
				$item->deleted_at = $_variation->deleted_at;
			}

			$variation->save();

			$wpdb->insert( "{$wpdb->prefix}sc_migrated_item_ids", array(
				'type' => 'flower',
				'item_id' => $flower->id,
				'variation_id' => $_variation->id,
				'new_item_id' => $variation->id,
			) );
			$migrated_variations ++;
		}
	}

	printf(
		'<tr>
			<td>%d</td>
			<td>%s</td>
			<td>%d</td>
			<td>%s</td>
		</tr>',
		$flower->id,
		$migrated ? 'No(already migrated)' : 'Yes',
		$migrated_variations,
		$extra_variation
	);
}

echo '</tbody></table>';

echo '<br/>...migrating services. Please wait!<br/><table><thead>
<tr>
	<td>ID</td>
	<td>Migrated?</td>
	<td>Created Extra Variation?</td>
</tr></thead><tbody>';

foreach ( $services_cursor as $service ) {
	$had_services = true;
	$migrated = sc_get_migrated_item_id( 'service', $service->id );
	$extra_variation = 'No(already migrated)';

	if ( ! $migrated ) {
		$item = new Item;
		$item->user_id = $service->user_id;
		$item->name = $service->name;
		$item->description = $service->description;
		switch ( $service->type ) {
			case 'base_price':
			case 'fee':
				$item->type = 'intangible';
				break;
			case 'rental':
				$item->type = 'rental';
				break;
			
			case 'hardgood':
			default:
				$item->type = 'consumable';
				break;
		}
		$item->pricing_category = $service->type;
		$item->purchase_qty = 1;
		$item->purchase_unit = 'each';
		$item->recipe_unit = 'each';
		if ( ! is_null( $service->deleted_at ) && '0000-00-00 00:00:00' != $service->deleted_at ) {
			$item->deleted_at = $service->deleted_at;
		}

		$item->save();

		$wpdb->insert( "{$wpdb->prefix}sc_migrated_item_ids", array(
			'type' => 'service',
			'item_id' => $service->id,
			'variation_id' => null,
			'new_item_id' => $item->id,
		) );
	}

	if ( ! sc_get_migrated_item_id( 'service', $service->id, -1 ) ) {
		$extra_variation = 'Yes';

		$variation = new Item_Variation;
		$variation->name = 'Default';
		$variation->item_id = $item->id;
		$variation->attachment_id = $service->attachment_id;
		$variation->cost_pu = $service->cost;
		$variation->inventory = $service->inventory;
		if ( ! is_null( $service->deleted_at ) && '0000-00-00 00:00:00' != $service->deleted_at ) {
			$variation->deleted_at = $service->deleted_at;
		}

		$variation->save();

		$wpdb->insert( "{$wpdb->prefix}sc_migrated_item_ids", array(
			'type' => 'service',
			'item_id' => $service->id,
			'variation_id' => -1,
			'new_item_id' => $variation->id,
		) );
	}

	printf(
		'<tr>
			<td>%d</td>
			<td>%s</td>
			<td>%s</td>
		</tr>',
		$service->id,
		$migrated ? 'No(already migrated)' : 'Yes',
		$extra_variation
	);
}

echo '</tbody></table>';

if ( ! $had_flowers && ! $had_services ) {
	echo '<br/>		.	MIGRATION ENDED		.	<br/>';
	exit;
}

?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 500);

</script>
<?php
exit;
