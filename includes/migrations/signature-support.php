<?php
use Illuminate\Database\Capsule\Manager as Capsule;

try {
	if ( ! Capsule::schema()->hasColumn( 'event_versions', 'client_signed' ) ) {
		Capsule::schema()->table( 'event_versions', function( $table ){
			$table->tinyInteger('client_signed')->default(0);

			echo 'New column added successfully!';
		} );
	} else {
		echo 'Migration has already been ran!';
	}
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}
