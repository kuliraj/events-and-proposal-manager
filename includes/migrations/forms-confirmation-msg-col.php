<?php

use Illuminate\Database\Capsule\Manager as Capsule;

if ( ! Capsule::schema()->hasColumn( 'forms', 'confirmation_msg' ) ) {
	Capsule::schema()->table( 'forms', function( $table ){
		$table->text('confirmation_msg')
			->after('name');
	} );
	echo 'Added the Thank You message column.<br />';
}

if ( ! Capsule::schema()->hasColumn( 'form_submissions', 'archived' ) ) {
	Capsule::schema()->table( 'form_submissions', function( $table ){
		$table->integer('archived')
			->default(0)
			->after('seen');
	} );
	echo 'Added the submission archived column.<br />';
}