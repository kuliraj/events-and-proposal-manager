<?php
use Stemcounter\Event;
use Stemcounter\Scheduled_Payment;
use Stemcounter\Made_Payment;
use Illuminate\Database\Capsule\Manager as Capsule;

ini_set('memory_limit', '-1');

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
 echo '<br/>=======OFFSET IS EMPTY=======<br/>';
 exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$users = get_users(array(
	'orderby' => 'ID',
	'number' => $_GET['number'],
	'offset' => $_GET['offset']
));

if (empty($users)) {
	echo '<br/>=======NO MORE USERS=======<br/>';
	exit;
}

foreach ($users as $user) {
	try {
		$user_events = Event::where( array(
			'user_id' => $user->ID,
		))->with(array(
			'arrangements' => function($query) {
				$query->where('include', '=', 1);
			},
		))->get();

		$created = false;

		foreach ($user_events as $event) {
			$scheduled_payments = Scheduled_Payment::where(array(
				'event_id' => $event->id
			))->get();

			if ( $scheduled_payments->isEmpty() ) {
				$created = true;

				echo '<br/> Creating Final Payment for event - ' . $event->name;
				$final_scheduled_payment = new Scheduled_Payment();
				$final_scheduled_payment->user_id = $user->ID;
				$final_scheduled_payment->event_id = $event->id;
				$final_scheduled_payment->payment_name = 'Final Payment';
				$final_scheduled_payment->payment_amount = $event->get_total();
				$final_scheduled_payment->payment_amount_type = 'currency';
				$final_scheduled_payment->payment_date = $event->date;
				$final_scheduled_payment->save();
				echo 'Done. ';
			}
		}

		} catch (Exception $e) {
			echo '<br/>====================================';
			echo $e->getMessage();
			echo '<br/>====================================';
		}	

	if ( $created ) {
		echo "<br/> Created Final Payments for {$user->user_login} <br/>";
	} else {
		echo "<br/> No Final Payments were created for {$user->user_login} <br/>";
	}
}
?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;