<?php
use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'events', 'delivery_id' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->integer('delivery_id')
			->unsigned()
			->nullable();
	} );
}
if ( ! Capsule::schema()->hasColumn( 'events', 'delivery_type' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->text('delivery_type');
	} );
}

if ( ! Capsule::schema()->hasColumn( 'events', 'delivery_tax' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->integer('delivery_tax')
			->unsigned()
			->default(1);
	} );
}

global $wpdb;
$wpdb->query( "UPDATE {$wpdb->prefix}sc_events SET delivery_type = 'percentage'" );

echo 'Migration done.';
exit;