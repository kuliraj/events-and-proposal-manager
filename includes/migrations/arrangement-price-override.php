<?php
use Illuminate\Database\Capsule\Manager as Capsule;

if (!Capsule::schema()->hasColumn('arrangements', 'override_cost')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('override_cost')->after('include');
	});
}

echo 'Migration done.';
exit;