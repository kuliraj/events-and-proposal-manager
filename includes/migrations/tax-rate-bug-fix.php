<?php
global $wpdb;

$wpdb->query( 
	"ALTER TABLE {$wpdb->prefix}sc_tax_rates
	 MODIFY COLUMN value DOUBLE (8,3)" );

echo 'Migration done.';