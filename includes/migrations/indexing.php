<?php
use Illuminate\Database\Capsule\Manager as Capsule;

if ( Capsule::schema()->hasTable('meta') ) {
	Capsule::schema()->table('meta', function($table) {
		$table->index( array( 'deleted_at', 'type', 'type_id', 'meta_key' ) );
	});
}

if ( Capsule::schema()->hasTable('arrangement_items') ) {
	Capsule::schema()->table('arrangement_items', function($table) {
		$table->index( array( 'deleted_at', 'arrangement_id' ) );
	});
}

echo 'Migration done.';
exit;