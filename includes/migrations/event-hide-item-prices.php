<?php
use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'events', 'hide_item_prices' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->integer('hide_item_prices')
			->unsigned()
			->default(0)
			->after('visible_payments');
	} );
}

if ( Capsule::schema()->hasColumn( 'arrangements', 'hide_price' ) ) {
	Capsule::schema()->table( 'arrangements', function( $table ){
		$table->dropColumn( 'hide_price' );
	} );
}

echo 'Migration done.';
exit;