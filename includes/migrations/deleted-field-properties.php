<?php
use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'field_properties', 'deleted' ) ) {
	Capsule::schema()->table( 'field_properties', function( $table ){
		$table->integer( 'deleted' )
			->default( 0 );
	} );
}
echo 'Migration done.';
exit;