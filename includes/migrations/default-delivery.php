<?php
use Illuminate\Database\Capsule\Manager as Capsule;

if ( ! Capsule::schema()->hasColumn( 'deliveries', 'default' ) ) {
	Capsule::schema()->table( 'deliveries', function( $table ){
		$table->integer('default')
			->default(0);
	} );
}

global $wpdb;
$wpdb->query( "UPDATE {$wpdb->prefix}sc_deliveries SET default = 0" );

echo 'Migration done.';
exit;