<?php
use Illuminate\Database\Capsule\Manager as Capsule;

if ( ! Capsule::schema()->hasColumn( 'events', 'separate_cc_fee' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->integer('separate_cc_fee')
			->unsigned()
			->default(0);
	} );
}

echo 'Migration done.';
exit;