<?php
use Stemcounter\Event;
use Stemcounter\Scheduled_Payment;
use Stemcounter\Made_Payment;
use Stemcounter\Payment;

use Illuminate\Database\Capsule\Manager as Capsule;

if ( empty( $_GET['eid'] ) ) {
	if (empty($_GET['offset']) && $_GET['offset'] != 0) {
		echo '<br/>=======OFFSET IS EMPTY=======<br/>';
		exit;
	}
	if (empty($_GET['number'])) {
		echo '<br/>=======NUMBER IS EMPTY=======<br/>';
		exit;
	}
}

if ( ! empty( $_GET['eid'] ) ) {
	$events = Event::where( array( 'id' => $_GET['eid'] ) )->get();
} else {
	$events = Event::take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'desc')->get();
}

if ( $events->isEmpty() ) {
	echo '<br/>		.	MIGRATION ENDED		.	<br/>';
	exit;	
} else {
	echo '<br/>...browsing through the payments.<br/>';
	foreach ( $events as $event ) {
		$payments = Payment::where(array(
			'user_id'	=> $event->user_id,
			'event_id'	=> $event->id,
		))->get();

		if ( $payments->isEmpty() ) {
			$payments = array();

			echo 'doesnt have payments<br/>';
			$scheduled_payments = Scheduled_Payment::where(array(
				'user_id' => $event->user_id,
				'event_id' => $event->id,
			))->get();

			// var_dump( count( $scheduled_payments ) );

			$made_payments = Made_Payment::where(array(
				'user_id' => $event->user_id,
				'event_id' => $event->id,
			))->get();

			$made_total = 0;
			$payment_key = 1;
			$event_cost = $event->get_total( true );
			$last_pmt_date = false;

			if ( ! $made_payments->isEmpty() ) {
				foreach ( $made_payments as $i => $made_payment ) {
					$made_total += price_format( $made_payment->payment_amount );
					$last_pmt_date = strtotime( $made_payment->payment_date . ' 10:00:00' );
					$payments[ $i ] = array(
						'payment_name' => 'Payment ' . $payment_key,
						'due_date' => $made_payment->payment_date,
						'payment_date' => $made_payment->payment_date,
						'payment_percentage' => null,
						'amount' => price_format( $made_payment->payment_amount ),
						'payment_note' => $made_payment->payment_note,
						'status' => 'complete',
						'payment_type' => $made_payment->payment_type,
					);

					$payment_key ++;
				}

				// Only one scheduled payment - just add another unpaid payment
				// with the remaining balance
				if ( 1 == count( $scheduled_payments ) || count( $made_payments ) >= count( $scheduled_payments ) ) {
					if ( $event_cost != $made_total ) {
						$payments[] = array(
							'payment_name' => 'Final Payment',
							'due_date' => $event->date,
							'payment_date' => null,
							'payment_percentage' => null,
							'amount' => price_format( $event_cost - $made_total ),
							'payment_note' => '',
							'status' => 'not-paid',
							'payment_type' => '',
						);
					}
				} else {
					$_made_total = $made_total;
					// printf( '<p>%s</p>', $_made_total );

					foreach ( $scheduled_payments as $i => $scheduled_payment ) {
						if ( 'percentage' == $scheduled_payment->payment_amount_type ) {
							$scheduled_amount = price_format( $event_cost * price_format( $scheduled_payment->payment_amount ) / 100 );
						} else {
							$scheduled_amount = price_format( $scheduled_payment->payment_amount );
						}

						if ( $_made_total == $scheduled_amount ) {
							$_made_total = 0;
							continue;	
						}
						
						if ( $_made_total > 0 ) {
							if ( $_made_total > $scheduled_amount ) {
								$_made_total -= $scheduled_amount;
								continue;
							} else {
								$last_pmt_date = $last_pmt_date < strtotime( $scheduled_payment->payment_date . ' 10:00:00' ) ? strtotime( $scheduled_payment->payment_date . ' 10:00:00' ) : $last_pmt_date;

								// printf( '<p>Event cost: %s</p>', $event_cost );
								// printf( '<p>scheduled_amount->payment_amount: %s</p>', $scheduled_payment->payment_amount );
								// printf( '<p>percentage calc: %s * %s</p>', $event_cost, price_format( $scheduled_payment->payment_amount ) / 100 );
								// printf( '<p>scheduled amount: %s</p>', price_format( $event_cost * price_format( $scheduled_payment->payment_amount ) / 100 ) );
								// printf( '<p>Scheduled amount: %s, amount: %s</p>', $scheduled_amount, price_format( $scheduled_amount - $_made_total ) );

								$payments[] = array(
									'payment_name' => 'Payment ' . $payment_key,
									'due_date' => date( 'Y-m-d', $last_pmt_date ),
									'payment_date' => null,
									'payment_percentage' => null,
									'amount' => price_format( $scheduled_amount - $_made_total ),
									'payment_note' => '',
									'status' => 'not-paid',
									'payment_type' => '',
								);

								$_made_total = 0;

								$payment_key ++;
							}
						} else {
							$last_pmt_date = $last_pmt_date < strtotime( $scheduled_payment->payment_date . ' 10:00:00' ) ? strtotime( $scheduled_payment->payment_date . ' 10:00:00' ) : $last_pmt_date;
							$payments[] = array(
								'payment_name' => 'Payment ' . $payment_key,
								'due_date' => date( 'Y-m-d', $last_pmt_date ),
								'payment_date' => null,
								'payment_percentage' => null,
								'amount' => price_format( $scheduled_amount ),
								'payment_note' => '',
								'status' => 'not-paid',
								'payment_type' => '',
							);

							$payment_key ++;
						}
					}
				}
			} else {
				// create records based on scheduled_payments
				foreach ( $scheduled_payments as $i => $scheduled_payment ) {
					if ( 'percentage' == $scheduled_payment->payment_amount_type ) {
						$scheduled_amount = price_format( $event_cost * price_format( price_format( $scheduled_payment->payment_amount ) / 100 ) );
					} else {
						$scheduled_amount = price_format( $scheduled_payment->payment_amount );
					}

					// var_dump( $scheduled_payments, $scheduled_amount );
					
					$last_pmt_date = $last_pmt_date < strtotime( $scheduled_payment->payment_date . ' 10:00:00' ) ? strtotime( $scheduled_payment->payment_date . ' 10:00:00' ) : $last_pmt_date;
					$payments[] = array(
						'payment_name' => 'Payment ' . $payment_key,
						'due_date' => date( 'Y-m-d', $last_pmt_date ),
						'payment_date' => null,
						'payment_percentage' => 'percentage' == $scheduled_payment->payment_type ? $scheduled_payment->payment_amount : null,
						'amount' => 'percentage' == $scheduled_payment->payment_type ? null : price_format( $scheduled_amount ),
						'payment_note' => '',
						'status' => 'not-paid',
						'payment_type' => '',
					);

					$payment_key ++;
				}
			}

			if ( ! empty( $payments ) ) {
				 
				$total_payments = 0;
				for ( $i=0; $i < count( $payments ) - 1; $i++ ) { 
					if ( ! is_null( $payments[ $i ]['payment_percentage'] ) ) {
						$total_payments += price_format( $event_cost * price_format( price_format( $payments[ $i ]['payment_percentage'] ) / 100 ) );
					} else {
						$total_payments += price_format( $payments[ $i ]['amount'] );
					}
				}
				// Set final payment value.
				$payments[ count( $payments ) - 1 ]['amount'] = price_format( $event_cost - $total_payments );
				// Set the name of the last payment
				$payments[ count( $payments ) - 1 ]['payment_name'] = 'Final Payment';
				$payments[ count( $payments ) - 1 ]['payment_percentage'] = null;

				// var_dump( $payments );
				foreach ( $payments as $payment_data ) {
					$payment = new Payment;

					$payment->event_id = $event->id;
					$payment->user_id = $event->user_id;

					foreach ( $payment_data as $key => $value ) {
						$payment->$key = $value;
					}

					$payment->save();
				}

				echo 'Created ' . count( $payments ) . ' payments!<br />';
			}
		} else {
			echo '<h4>Event already has ' . count( $payments ) . ' payments!</h4>';
		}
	}
}

if ( ! empty( $_GET['eid'] ) ) {
	exit;
}

 ?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;