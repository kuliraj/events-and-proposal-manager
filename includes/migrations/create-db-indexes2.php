<?php
use Illuminate\Database\Capsule\Manager as Capsule;

try {
	Capsule::schema()->table( 'arrangement_items', function( $table ){
		$table->index( array('arrangement_id' ), 'arrangement_id_index' );
		echo 'Created arrangement_items table index!<br /><br />';
	} );
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}

try {
	Capsule::schema()->table( 'event_versions', function( $table ){
		$table->index( array( 'event_id' ), 'event_id_index' );
		echo 'Created event_versions table index!<br /><br />';
	} );
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}

try {
	Capsule::schema()->table( 'arrangements', function( $table ){
		$table->index( array( 'event_id' ), 'event_id_index' );
		echo 'Created arrangements table index!<br /><br />';
	} );
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}

try {
	Capsule::schema()->table( 'item_variations', function( $table ){
		$table->index( array( 'item_id' ), 'item_id_index' );
		echo 'Created item_variations table index!<br /><br />';
	} );
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}

try {
	Capsule::schema()->table( 'arrangement_recipes', function( $table ){
		$table->index( array( 'arrangement_id' ), 'arrangement_id_index' );
		echo 'Created item_variations table index!<br /><br />';
	} );
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}

try {
	Capsule::schema()->table( 'payments', function( $table ){
		$table->index( array( 'event_id' ), 'event_id_index' );
		$table->index( array( 'user_id' ), 'user_id_index' );
		echo 'Created payments table index!<br /><br />';
	} );
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}

try {
	Capsule::schema()->table( 'items', function( $table ){
		$table->index( array('user_id' ), 'user_id_index' );
		echo 'Created items table index!<br /><br />';
	} );
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}

try {
	Capsule::schema()->table( 'arrangement_photos', function( $table ){
		$table->index( array('arrangement_id' ), 'arrangement_id_index' );
		echo 'Created arrangement_photos table index!<br /><br />';
	} );
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}

try {
	Capsule::schema()->table( 'events', function( $table ){
		$table->index( array('user_id' ), 'user_id_index' );
		echo 'Created events table index!<br /><br />';
	} );
} catch (Exception $e) {
	var_dump( $e->getMessage() );
}

