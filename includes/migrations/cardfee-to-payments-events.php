<?php 
use Stemcounter\Payment;
use Stemcounter\Event;
use Stemcounter\Meta;
use Stemcounter\Arrangement;
use Illuminate\Database\Capsule\Manager as Capsule;

if ( empty( $is_cli ) ) {

	if ( Capsule::schema()->hasColumn( 'payments', 'card_fee' ) ) {
		echo 'Migration already performed!';
		exit;
	}

	if ( empty( $_GET['offset'] )  && $_GET['offset'] != 0 ) {
		echo '<br/>=======OFFSET IS EMPTY=======<br/>';
		exit;
	}

	if ( empty( $_GET['number'] ) ) {
		echo '<br/>=======NUMBER IS EMPTY=======<br/>';
		exit;
	}

	$events = Event::where('separate_cc_fee', 1)
			->take($_GET['number'])
			->offset($_GET['offset'])
			->with('payments')
			->orderBy('date', 'asc')
			->get();
} else {
	$events = Event::where('separate_cc_fee', 1)
			->with('payments')
			->orderBy('date', 'asc')
			->get();
}

if ( $events->count() ) {
	echo 'Migrating events ...<br />';

	$list = array();
	foreach ( $events as $event ) {

		echo '<br />- Event id: ' . $event->id;

		//Avoid double migration
		$Migrated = Meta::where( array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'migrated1',
			))->first();

		if ( ! empty( $Migrated->meta_value)  ) {
			echo ' Already migrated';
			$list[] = array( $event->id, 'migrated' );
			continue;
		}

		Meta::create( array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'migrated1',
			'meta_value' => 1,
		));

		//Collect payments		
		if ( empty( $event->payments ) ) {
			$nonpaid_payments = 'no payments';
		} else {
			$nonpaid_payments = array();
			foreach( $event->payments as $payment ) {
				if ('not-paid' == $payment->status ) {
					$nonpaid_payments[] = $payment->get_amount();
				}
			}
			$nonpaid_payments = 'non-paid payments: ' . implode( ', ', $nonpaid_payments );
		}
		
		$current_order = $event->arrangements->max( 'order' ) + 1;
		$user_data = get_userdata( $event->user_id );
		$total = $event->get_total( true );
		$total_with_cc = $event->get_total_with_cc_rate( true );
		$convenience_fee = $total_with_cc - $total;

		if ( 0 < $convenience_fee ) {

			//Gathering report data
			$row = array(
				$event->id,
				'workroom',
				$user_data->user_email,
				$user_data->display_name,
				$event->name,
				$total,
				$total_with_cc,
				$event->updated_at,
				$event->status,
				$event->date,
				$nonpaid_payments,
			);
			$list[] = $row;

			//The real migration
			$new_arrangement = new Arrangement;
			$new_arrangement->event_id = $event->id;
			$new_arrangement->user_id = $event->user_id;
			$new_arrangement->addon = 0;
			$new_arrangement->quantity = 1;
			$new_arrangement->include = 1;
			$new_arrangement->note = '';
			$new_arrangement->order = $current_order;
			$new_arrangement->is_percentage = 0;
			$new_arrangement->override_cost = $convenience_fee;
			$new_arrangement->tax = 0;
			$new_arrangement->name = 'Card convenience fee';
			$new_arrangement->invoicing_category_id = null;
			$new_arrangement->hardgood_multiple = null;
			$new_arrangement->fresh_flower_multiple = null;
			$new_arrangement->global_labor_value	= null;
			$new_arrangement->flower_labor		=	null;
			$new_arrangement->hardgood_labor	= null;
			$new_arrangement->base_price_labor	= null;
			$new_arrangement->fee_labor			= null;
			$new_arrangement->rental_labor		= 0;

			$new_arrangement->save();
		}

		$event->card = 0;
		$event->save();
	}


	//Write report data
	$fp = fopen('report-events.csv', 'a');
	foreach ($list as $fields) {
		fputcsv($fp, $fields);
	}
	fclose($fp);
}
 else {

	Capsule::schema()->table( 'payments', function( $table ){
		$table->float('card_fee')
			->default(0);
	} );
	Capsule::schema()->table( 'payments', function( $table ){
		$table->float('convenience_fee')
			->nullable()
			->default(null);
	} );
	echo 'Migration done!';
	exit;
}
?>

<script type="text/javascript">
setTimeout(function() {
	window.location.href = <?php echo json_encode( add_query_arg( 'offset', $_GET['offset'] + $_GET['number'] ) ); ?>;
}, 1500);
</script>

<?php
exit;

//app/wp-admin/admin-ajax.php?action=sc_migration&migration=cardfee-to-payments-events&number=50&offset=0