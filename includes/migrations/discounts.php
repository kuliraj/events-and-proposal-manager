<?php
use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'events', 'discount_id' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->integer('discount_id')
			->unsigned()
			->nullable();
	} );
}
if ( ! Capsule::schema()->hasColumn( 'events', 'discount_name' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->string('discount_name');
	} );
}
if ( ! Capsule::schema()->hasColumn( 'events', 'discount_value' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->float('discount_value');
	} );
}
if ( ! Capsule::schema()->hasColumn( 'events', 'discount_type' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->text('discount_type');
	} );
}
echo 'Migration done.';
exit;