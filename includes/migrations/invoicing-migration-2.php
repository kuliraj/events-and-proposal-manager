<?php 
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Invoicing_Category;

global $wpdb;
//======================================================================
/* Events */
if (! Capsule::schema()->hasColumn('events', 'invoicing_category_id')) {
	Capsule::schema()->table('events', function($table){
		$table->integer('invoicing_category_id')
			->default(0);
	});
}
/*Multiples*/
if (! Capsule::schema()->hasColumn('events', 'hardgood_multiple')) {
	Capsule::schema()->table('events', function($table){
		$table->float('hardgood_multiple', 6, 2)
			->default(1);
	});
}
if (! Capsule::schema()->hasColumn('events', 'fresh_flower_multiple')) {
	Capsule::schema()->table('events', function($table){
		$table->float('fresh_flower_multiple', 6, 2)
			->default(1);
	});
}
/*Labor*/
if (! Capsule::schema()->hasColumn('events', 'global_labor_value')) {
	Capsule::schema()->table('events', function($table){
		$table->float('global_labor_value', 6, 2)
			->default(0);
	});
}
if (! Capsule::schema()->hasColumn('events', 'flower_labor')) {
	Capsule::schema()->table('events', function($table){
		$table->float('flower_labor', 6, 2)
			->default(0);
	});
}
if (! Capsule::schema()->hasColumn('events', 'hardgood_labor')) {
	Capsule::schema()->table('events', function($table){
		$table->float('hardgood_labor', 6, 2)
			->default(0);
	});
}
if (! Capsule::schema()->hasColumn('events', 'hardgood_labor')) {
	Capsule::schema()->table('events', function($table){
		$table->float('hardgood_labor', 6, 2)
			->default(0);
	});
}
if (! Capsule::schema()->hasColumn('events', 'base_price_labor')) {
	Capsule::schema()->table('events', function($table){
		$table->float('base_price_labor', 6, 2)
			->default(0);
	});
}
if (! Capsule::schema()->hasColumn('events', 'fee_labor')) {
	Capsule::schema()->table('events', function($table){
		$table->float('fee_labor', 6, 2)
			->default(0);
	});
}
if (! Capsule::schema()->hasColumn('events', 'rental_labor')) {
	Capsule::schema()->table('events', function($table){
		$table->float('rental_labor', 6, 2)
			->default(0);
	});
}
//=======================================================================
/* Arrangements */

/*Multiples*/
if (! Capsule::schema()->hasColumn('arrangements', 'invoicing_category_id')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->integer('invoicing_category_id')
			->nullable()
			->default(NULL);
	});
}
if (! Capsule::schema()->hasColumn('arrangements', 'hardgood_multiple')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('hardgood_multiple', 6, 2)
			->nullable()
			->default(NULL);
	});
}
if (! Capsule::schema()->hasColumn('arrangements', 'fresh_flower_multiple')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('fresh_flower_multiple', 6, 2)
			->nullable()
			->default(NULL);
	});
}

/*Labor*/
if (! Capsule::schema()->hasColumn('arrangements', 'global_labor_value')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('global_labor_value', 6, 2)
			->nullable()
			->default(NULL);
	});
}
if (! Capsule::schema()->hasColumn('arrangements', 'flower_labor')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('flower_labor', 6, 2)
			->nullable()
			->default(NULL);
	});
}
if (! Capsule::schema()->hasColumn('arrangements', 'hardgood_labor')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('hardgood_labor', 6, 2)
			->nullable()
			->default(NULL);
	});
}
if (! Capsule::schema()->hasColumn('arrangements', 'hardgood_labor')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('hardgood_labor', 6, 2)
			->nullable()
			->default(NULL);
	});
}
if (! Capsule::schema()->hasColumn('arrangements', 'base_price_labor')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('base_price_labor', 6, 2)
			->nullable()
			->default(NULL);
	});
}
if (! Capsule::schema()->hasColumn('arrangements', 'fee_labor')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('fee_labor', 6, 2)
			->nullable()
			->default(NULL);
	});
}
if (! Capsule::schema()->hasColumn('arrangements', 'rental_labor')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->float('rental_labor', 6, 2)
			->nullable()
			->default(NULL);
	});
}

$users = get_users();
foreach ( $users as $user ) {
	$global_hardgood_multiple = get_user_meta(
		$user->ID,
		'hardgood_multiple',
		true
	);

	$global_fresh_flower_multiple = get_user_meta(
		$user->ID,
		'fresh_flower_multiple',
		true
	);

	try {
		$user_meta = sc_get_user_meta($user->ID);
		$labor_cost = empty($user_meta['__labor']) ? 0 : $user_meta['__labor'];
		$add_labor_costs = get_user_meta($user->ID, '_sc_add_labor_costs', true);
		$add_labor_costs = (bool)($add_labor_costs == 'y');
		$apply_labor_to = get_user_meta($user->ID, '_sc_apply_labor_to', false);
		
		/* Invoice Category */
		$invoice_category = new Invoicing_Category();
		$invoice_category->user_id = $user->ID;
		$invoice_category->name = 'Standard Markup';
		$invoice_category->hardgood_multiple = $global_hardgood_multiple;
		$invoice_category->fresh_flower_multiple = $global_fresh_flower_multiple;
		$invoice_category->global_labor_value = $labor_cost;

		$flower_labor_set = false;
		$hardgood_labor_set = false;
		$base_price_labor_set = false;
		$fee_labor_set = false;
		$rental_labor_set = false;
		foreach ($apply_labor_to as $labor_name) {
			if ($labor_name == 'flower') {
				$flower_labor_set = true;
				$invoice_category->flower_labor = $labor_cost;	
			}
			if ($labor_name == 'hardgood') {
				$hardgood_labor_set = true;
				$invoice_category->hardgood_labor = $labor_cost;	
			}
			if ($labor_name == 'base_price') {
				$base_price_labor_set = true;
				$invoice_category->base_price_labor = $labor_cost;	
			}
			if ($labor_name == 'fee') {
				$fee_labor_set = true;
				$invoice_category->fee_labor = $labor_cost;	
			}
			if ($labor_name == 'rental') {
				$rental_labor_set = true;
				$invoice_category->rental_labor = $labor_cost;	
			}
		}

		if (!$flower_labor_set) $invoice_category->flower_labor = 0;
		if (!$hardgood_labor_set) $invoice_category->hardgood_labor = 0;
		if (!$base_price_labor_set) $invoice_category->base_price_labor = 0;
		if (!$fee_labor_set) $invoice_category->fee_labor = 0;
		if (!$rental_labor_set) $invoice_category->rental_labor = 0;

		$invoice_category->save();

		update_user_meta(
			$user->ID,
			'default_invoice_category_id',
			$invoice_category->id
		);

		/* Events */
		$wpdb->update(
			$wpdb->prefix . 'sc_events',
			array(
				'invoicing_category_id' => $invoice_category->id,
				'global_labor_value' => $invoice_category->global_labor_value,
				'hardgood_multiple' => $global_hardgood_multiple,
				'fresh_flower_multiple' => $global_fresh_flower_multiple,
				'flower_labor' => $invoice_category->flower_labor,
				'hardgood_labor' => $invoice_category->hardgood_labor,
				'base_price_labor' => $invoice_category->base_price_labor,
				'fee_labor' => $invoice_category->fee_labor,
				'rental_labor' => $invoice_category->rental_labor
			),
			array('user_id' => $user->ID)
		);

	} catch (Exception $e) {
		echo '<br/>' . $e->getMessage() . '<br/>';
	}
	

	echo "\n\n Created Default Invoice Category for {$user->user_login}\n\n";
}
exit;