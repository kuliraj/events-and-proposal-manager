<?php 
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Tax_Rate;

if ( ! Capsule::schema()->hasColumn( 'events', 'tax_rate_value' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->float('tax_rate_value', 6, 2)
			->after('tax_rate_id');
	} );
}

global $wpdb;

$users = get_users();
foreach ( $users as $user ) {
	$tax_rates = Tax_Rate::where(array(
		'user_id' => $user->ID
	))->get();
	
	foreach ($tax_rates as $tax_rate) {

		$data = array(
			'tax_rate_value' => $tax_rate->value
		);

		$affected_rows = $wpdb->update(
			$wpdb->prefix . 'sc_events',
			$data,
			array(
				'user_id' => $user->ID,
				'tax_rate_id' => $tax_rate->id
			)
		);

		echo "Migrating tax rates for user {$user->user_login}\n\n";
	}


}
exit;