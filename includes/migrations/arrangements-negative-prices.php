<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Event_Version;
global $wpdb;
global $illuminate_capsule;


if ( empty( $_GET['offset'] )  && $_GET['offset'] != 0 ) {
 echo '<br/>=======OFFSET IS EMPTY=======<br/>';
 exit;
}

if ( empty( $_GET['number'] ) ) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

if ( ! $illuminate_capsule->getConnection()->getDoctrineColumn($wpdb->prefix . 'sc_arrangements', 'override_cost')->getNotnull() 	) {

	echo "Column `override_cost` is already nullable. No changes will be made.";
	exit;	

}

$Versions = Event_Version::take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'asc')->get();

if ( $Versions->count() ) {
	foreach ( $Versions as &$version ) {
		$event_data = json_decode( $version->event_data );
		if ( $event_data ) {
			echo 'Version id: ' . $event_data->event->id . '<br />';
			foreach ( $event_data->event->arrangements as &$arrangement ) {
				echo '&nbsp;' . $arrangement->id;
				if ( -1 == $arrangement->override_cost ) {
					$arrangement->override_cost = NULL;
					echo ' -1 to null';
				} else {
					echo ' value not changed - ' . var_export( $arrangement->override_cost, true );
				}
				echo '<br />';
			}
			$version->event_data = json_encode( $event_data );
			$version->save();
		}
	}
	//$Versions->save();
} else {
	$wpdb->query( "ALTER TABLE `{$wpdb->prefix}sc_arrangements` MODIFY `override_cost` double(8, 2) NULL DEFAULT NULL" );
	$wpdb->query( "UPDATE `{$wpdb->prefix}sc_arrangements` SET `override_cost` = NULL WHERE `override_cost` = -1;" );

	echo '<br />Migration done.';
	exit;
}


?>

<script type="text/javascript">
setTimeout(function() {
	window.location.href = <?php echo json_encode( add_query_arg( 'offset', $_GET['offset'] + $_GET['number'] ) ); ?>;
}, 1500);
</script>

<?php
exit;

// app/wp-admin/admin-ajax.php?action=sc_migration&migration=arrangements-negative-prices&number=100&offset=0