<?php
use Stemcounter\Event;
use Stemcounter\Event_Version;
use Stemcounter\Flower;
use Stemcounter\Service;
use Stemcounter\Variation;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Item;
use Stemcounter\Arrangement_Photo;
use Stemcounter\Customer;
use Stemcounter\Scheduled_Payment;
use Illuminate\Database\Capsule\Manager as Capsule;

ini_set('memory_limit', '-1');
ini_set('max_execution_time', '-1');

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$events = Event::with('arrangements', 'arrangements.items', 'arrangements.items.item_variation')->take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'desc')->get();

if ( $events->isEmpty() ) {
	echo '<br/>=======VERSIONING MIGRATION ENDED=======br/>';
	exit;
} else {
	echo '<br/>Creating Event Versions...<br/>';
	foreach ($events as $event) {
		
		$event_versions = Event_Version::where(array(
			'event_id' => $event->id,
		))->get();
		
		if ( ! $event_versions->isEmpty() ) {
			continue;
		}

		$user_id = $event->user_id;
		$current_user = get_userdata( $user_id );
		$company_info = get_company_info( $user_id );

		$event->arrangements->sortBy( function($arrangement){
			return $arrangement->order;
		} );

		$logo = '<h3>' . $company_info['company'] . '</h3>';
		if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
			$logo_id = sc_get_profile_logo_id( $user_id );
			$logo = wp_get_attachment_image( $logo_id, 'logo_large', false, array( 'class' => 'proposal-logo' ) );
		}

		if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
			$logo_id = sc_get_profile_logo_id( $user_id );
			$logo = wp_get_attachment_image_src( $logo_id, 'medium' );
			$company_info['logo_url'] = $logo[0];
		} else {
			$company_info['logo_url'] = false;
		}

		$additionalInfo = unserialize( $current_user->additionalInfo ); //Get User Details
		$company_info['phone'] = isset( $additionalInfo['companyMobile'] ) ? $additionalInfo['companyMobile'] : '';
		$company_info['email'] = isset( $current_user->user_email ) ? $current_user->user_email : '';

		// include event details from above function if needeed.
		$proposal_arrangements = array();

		foreach ($event->arrangements as $arrangement) {
			$arrangement_photos = Arrangement_Photo::where('arrangement_id', '=', $arrangement->id)->get();
			$photos = array();
			foreach ($arrangement_photos as $photo) {
				/* if the image exists */
				$image = wp_get_attachment_image_src($photo->photo_id);
				if (!empty($image)) {
					$image_id = $photo->photo_id;
					$image_url = wp_get_attachment_image_src($image_id, 'medium');
					$image_url = $image_url[0];
					$full_image = wp_get_attachment_image_src($image_id, 'full');

					$photos[] = array(
						'image_id' => $image_id,
						'image_url' => $image_url,
						'full_image_src' => $full_image[0],
						'full_image_w' => $full_image[1],
						'full_image_h' => $full_image[2],
					);		
				} else {
					/* delete it from the arrangement_photos if it doesn't */
					Arrangement_Photo::where(array(
						'photo_id' => $photo->photo_id
					))->delete();
				}
			}

			$items = array();

			foreach ( $arrangement->items as $arrangement_item ) {
				$cost_and_markup = $arrangement_item->cost + $arrangement_item->get_markup( false );
				
				$vi = $arrangement_item->toArray();
				if (is_a($arrangement_item->item, 'Stemcounter\Service')) {
					$vi['item_type'] = $arrangement_item->item->type;
					$vi['item_variation_id'] = 0;
				} else {
					$vi['item_variation_name'] = '';
					try {
						//used for deleted items
						$variation = Variation::where(
							array('id' => $vi['item_variation_id'])
						)->firstOrFail()->toArray();

						$vi['item_variation_name'] = ' - ' . $variation['name'];
					} catch (Exception $e) {
					}
					$vi['item_type'] = 'flower';
				}

				$key = 'flower' == $vi['item_type'] ? 'flower' : 'service';
				$key .= '|' . $arrangement_item->item_id . '|';
				$key .= $vi['item_variation_id'] ? $vi['item_variation_id'] : 0;

				$type = sc_get_item_type( $arrangement_item->item, true );

				$items[] = array(
					'id'                        => $arrangement_item->id,
					'name'                      => $arrangement_item->get_full_name(),
					'quantity'                  => $arrangement_item->quantity,
					'invoicing_multiplier'      => $arrangement_item->get_invoicing_multiplier(),
					'cost'                      => $arrangement_item->cost,
					'cost_and_markup'           => $cost_and_markup,
					'cost_and_markup_and_labor' => $cost_and_markup + $arrangement_item->get_labor( false ),
					'subtotal'                  => $arrangement_item->get_calculated_price(),
					'type'                      => $type,
					'type_raw'                  => str_replace( ' ', '_', strtolower( $type ) ),
					'item_type'                 => $vi['item_type'],
					'item_variation_id'         => $vi['item_variation_id'],
					'item_variation_name'       => ! empty( $vi['item_variation_name'] ) ? $vi['item_variation_name'] : '',
					'itemKeypair'               => $key,
				);
			}

			array_push($proposal_arrangements, array(
				'id'                    => $arrangement->id,
				'name'                  => $arrangement->name,
				'quantity'              => $arrangement->quantity,
				'order'                 => $arrangement->order,
				'note'                  => $arrangement->note,
				'include'               => $arrangement->include,
				'tax'                   => $arrangement->tax,
				'total'                 => $arrangement->get_final_price(false),
				'override_cost'         => $arrangement->override_cost,
				'subTotal'              => $arrangement->get_final_price(),
				'photos'                => $photos,
				'items'                 => $items,
				'invoicing_category_id' => $arrangement->invoicing_category_id,
				'hardgood_multiple'     => $arrangement->hardgood_multiple,
				'fresh_flower_multiple' => $arrangement->fresh_flower_multiple,
				'global_labor_value'    => $arrangement->global_labor_value,
				'flower_labor'          => $arrangement->flower_labor,
				'hardgood_labor'        => $arrangement->hardgood_labor,
				'base_price_labor'      => $arrangement->base_price_labor,
				'fee_labor'             => $arrangement->fee_labor,
				'rental_labor'          => $arrangement->rental_labor,
			));
		}

		$date_format = $js_date_format = get_user_meta($user_id, '__date_format', true);
		if ($date_format == 'd/m/Y') {
			$js_date_format = 'dd/mm/yy';
		} else if ($date_format == 'm/d/Y') {
			$js_date_format = 'mm/dd/yy';
		} else if ($date_format == 'd.m.Y') {
			$js_date_format = 'dd.mm.yy';
		} else if ($date_format == 'm.d.Y') {
			$js_date_format = 'mm.dd.yy';
		}

		$scheduled_payments = array();
		$_scheduled_payments = Scheduled_Payment::where(array(
			'user_id' => $user_id,
			'event_id' => $event->id
		))->get();
		foreach ($_scheduled_payments as $scheduled_payment) {
			array_push($scheduled_payments, array(
				'paymentId' => $scheduled_payment->id,
				'paymentName' => $scheduled_payment->payment_name,
				'amount' => $scheduled_payment->payment_amount,
				'amountType' => $scheduled_payment->payment_amount_type,
				'date' => sc_get_user_date_time($user_id, $scheduled_payment->payment_date)
			));
		}

		$event_cost = array(
			'subtotal' => $event->get_subtotal( true ),
			'tax' => $event->get_tax( true ),
			'tax_rate_id' => $event->tax_rate_id,
			'tax_rate_value' => $event->tax_rate_value,
			'delivery_value' => $event->delivery,
			'delivery_type'	=> $event->delivery_type,
			'delivery'		=> $event->get_delivery(true),
			'total' => $event->get_total( true ),
			'discount_value' => $event->discount_value,
			'discount_type'	=> $event->discount_type,
			'discount'		=> $event->get_discount(true),
		);

		
		$customer = sc_get_user_event_client( $event->id, true );
			
		if ( $event->cover_id ) {
			$event_cover_src = wp_get_attachment_image_src( $event->cover_id, 'full' );
			$event_cover = array(
				'imageId' => $event->cover_id,
				'imageURL' => $event_cover_src[0],
				'fullImageW' => $event_cover_src[1],
				'fullImageH' => $event_cover_src[2],
			);
		} else {
			$event_cover = array(
				'imageId' => '',
				'imageURL' => '',
				'fullImageW' => '',
				'fullImageH' => '',
			);
		}

		$event_vendors = array();
		$event_vendors_info = sc_get_user_event_vendors($event->id);
		$event_vendors['venue'] = $event_vendors_info['event_vendors']['venue'];
		
		$_event = array(
			'id'          		=> $event->id,
			'name'        		=> $event->name,
			'card'       		=> $event->card,
			'separate_cc_fee' 	=> $event->separate_cc_fee,
			'date'       		=> date( $date_format, strtotime( $event->date ) ),
			'date_created'		=> date( $date_format, strtotime( $event->created_at ) ),
			'venues'      		=> $event_vendors['venue'],
			'arrangements'		=> $proposal_arrangements,
			'cover'		  		=> $event_cover,
		);

		$versioned_event_data = array(
			'event' 			=> $_event,
			'eventDate'			=> date( $date_format, strtotime( $event->date ) ),
			'company_info'		=> $company_info,
			'customer'			=> $customer,
			'eventCost'			=> $event_cost,
			'scheduledPayments' => $scheduled_payments,
			'eventNote'			=> wpautop( $event->event_note ),
			'eventContractNote' => wpautop( $event->contract_note ),
			'currencySymbol'	=> sc_get_user_saved_pref_currency( $user_id ),
			'dateFormat'		=> $js_date_format,
		);

		$new_version = new Event_Version;
		$new_version->event_id = $event->id;
		$new_version->version = 1;
		$new_version->event_data = json_encode( $versioned_event_data );
		$new_version->save();

		echo '<br/>';
		echo 'Created Version of event with ID - ' . $event->id;
	}
}

?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;