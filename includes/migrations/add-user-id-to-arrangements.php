<?php

use Illuminate\Database\Capsule\Manager as Capsule;

// /app/wp-admin/admin-ajax.php?action=sc_migration&migration=add-user-id-to-arrangements

global $wpdb;

if ( ! Capsule::schema()->hasColumn( 'arrangements', 'user_id' ) ) {
	Capsule::schema()->table( 'arrangements', function( $table ){
		$table->integer( 'user_id' )
			->unsigned()
			->after( 'event_id' );
	} );

	echo 'Added user_id column to the arrangements table!<br />';
}

$result = $wpdb->query( "UPDATE {$wpdb->prefix}sc_arrangements
	LEFT JOIN {$wpdb->prefix}sc_events ON {$wpdb->prefix}sc_events.id = {$wpdb->prefix}sc_arrangements.event_id
	SET {$wpdb->prefix}sc_arrangements.user_id = {$wpdb->prefix}sc_events.user_id" );

printf( 'Updated %d arrangements.<br />', $result );

try {
	Capsule::schema()->table( 'arrangements', function( $table ){
		$table->index( array( 'event_id', 'user_id' ), 'event_id_user_id_index' );
		$table->dropIndex( 'event_id_index' );
		echo 'Updated the arrangements table index!<br /><br />';
	} );
} catch ( Exception $e ) {
	echo 'The following error occurred while trying to update the arrangements table index:<br />';
	echo $e->getMessage();
	exit;
}

echo 'Success!';
