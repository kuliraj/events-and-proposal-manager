<?php
use Stemcounter\Flower;
use Stemcounter\Variation;
use Stemcounter\Service;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Arrangement_Item;
use Stemcounter\Recipe_Item;
use Stemcounter\Order_Item;

use Illuminate\Database\Capsule\Manager as Capsule;

global $wpdb;

// app/wp-admin/admin-ajax.php?action=sc_migration&migration=new-item-structure-migrate-ids&offset=0&number=30

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$dryrun = ! empty( $_GET['dryrun'] ) && 'no' == $_GET['dryrun'] ? false : true;

function sc_get_migrated_item_id( $type, $old_id, $variation_id = null ) {
	global $wpdb;

	if ( $variation_id ) {
		$variation_id = $wpdb->prepare( ' AND variation_id = %d', $variation_id );
	} elseif ( is_null( $variation_id ) ) {
		$variation_id = ' AND variation_id IS NULL';
	}

	$old_id = $old_id ? $wpdb->prepare( ' AND item_id = %d ', $old_id ) : '';

	return $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}sc_migrated_item_ids
		WHERE type = %s {$old_id} $variation_id", $type ) );
}

$had_arr_items = false;
$had_recipe_items = false;
$had_order_items = false;

$cursor = Arrangement_Item::take( $_GET['number'] )->offset( $_GET['offset'] )->orderBy( 'id', 'asc' )->withTrashed()->cursor();

echo 'Migrating Arrangement Items... Please wait.<br /><br />';

foreach ( $cursor as $arrangement_item ) {
	$had_arr_items = true;
	if ( ! sc_get_migrated_item_id( 'arrangement_item', $arrangement_item->id ) ) {
		$type = 'Stemcounter\Flower' == $arrangement_item->item_type ? 'flower' : 'service';
		$new_id = sc_get_migrated_item_id(
			$type,
			$arrangement_item->item_id,
			null
		);

		$new_variation_id = sc_get_migrated_item_id(
			$type,
			$arrangement_item->item_id,
			$arrangement_item->item_variation_id ? $arrangement_item->item_variation_id : -1
		);

		if ( $new_id && ! $new_variation_id ) {
			$new_variation = Item::where( array( 'id' => $new_id->new_item_id ) )->withTrashed()->with( array( 'default_variation' => function($query) {
				$query->withTrashed();
			} ) )->first();
			if ( $new_variation && $new_variation->default_variation ) {
				$new_variation_id = new stdClass;
				$new_variation_id->new_item_id = $new_variation->default_variation->id;
			}
		}

		// var_dump( $type, $new_id, $new_variation_id, $arrangement_item->item_variation_id ? $arrangement_item->item_variation_id : -1 );

		if ( ! $new_id || ! $new_variation_id ) {
			printf( 'No migrated item found for Arrangement Item with ID %d', $arrangement_item->id );
			exit;
		} elseif ( ! $dryrun ) {
			$arrangement_item->item_id = $new_id->new_item_id;
			$arrangement_item->item_variation_id = $new_variation_id->new_item_id;

			$arrangement_item->save();

			$wpdb->insert( "{$wpdb->prefix}sc_migrated_item_ids", array(
				'type' => 'arrangement_item',
				'item_id' => $arrangement_item->id,
				'variation_id' => null,
				'new_item_id' => $arrangement_item->id,
			) );
			printf( 'Migrated Arrangement Item #%d<br />', $arrangement_item->id );
		} else {
			printf( 'Dry run migration of Arrangement Item #%d looks good.<br />', $arrangement_item->id );
		}
	} else {
		printf( 'Skipping already migrated Arrangement Item #%d<br />', $arrangement_item->id );
	}
}

$cursor = Recipe_Item::with( array( 'item_variation' ) )->take( $_GET['number'] )->offset( $_GET['offset'] )->orderBy( 'id', 'asc' )->withTrashed()->cursor();

echo '<br /><br />Migrating Recipe Items... Please wait.<br /><br />';

foreach ( $cursor as $recipe_item ) {
	$had_recipe_items = true;
	if ( ! sc_get_migrated_item_id( 'recipe_item', $recipe_item->id ) ) {
		$type = 'Stemcounter\Flower' == $recipe_item->item_type ? 'flower' : 'service';
		$new_id = sc_get_migrated_item_id(
			$type,
			$recipe_item->item_id,
			null
		);

		$new_variation_id = sc_get_migrated_item_id(
			$type,
			$recipe_item->item_id,
			$recipe_item->item_variation_id ? $recipe_item->item_variation_id : -1
		);

		if ( $new_id && ! $new_variation_id ) {
			$new_variation = Item::where( array( 'id' => $new_id->new_item_id ) )->withTrashed()->with( array( 'default_variation' => function($query) {
				$query->withTrashed();
			} ) )->first();
			if ( $new_variation && $new_variation->default_variation ) {
				$new_variation_id = new stdClass;
				$new_variation_id->new_item_id = $new_variation->default_variation->id;
			}
		}

		if ( ! $new_id || ! $new_variation_id ) {
			printf( 'No migrated item found for Recipe Item with ID %d', $recipe_item->id );
			// exit;
		} elseif ( ! $dryrun ) {
			$recipe_item->item_id = $new_id->new_item_id;
			$recipe_item->item_variation_id = $new_variation_id->new_item_id;

			$recipe_item->save();

			$wpdb->insert( "{$wpdb->prefix}sc_migrated_item_ids", array(
				'type' => 'recipe_item',
				'item_id' => $recipe_item->id,
				'variation_id' => null,
				'new_item_id' => $recipe_item->id,
			) );
			printf( 'Migrated Recipe Item #%d<br />', $recipe_item->id );
		} else {
			printf( 'Dry run migration of Recipe Item #%d looks good.<br />', $recipe_item->id );
		}
	} else {
		printf( 'Skipping already migrated Recipe Item #%d<br />', $recipe_item->id );
	}
}

$cursor = Order_Item::take( $_GET['number'] )->offset( $_GET['offset'] )->orderBy( 'id', 'asc' )->withTrashed()->cursor();

echo '<br /><br />Migrating Order Items... Please wait.<br /><br />';

foreach ( $cursor as $order_item ) {
	$had_order_items = true;

	if ( is_null( $order_item->item_id ) ) {
		printf( 'Skipping Order Item #%d because it does not have an item_id.<br />', $order_item->id );

		continue;
	}

	if ( ! sc_get_migrated_item_id( 'order_item', $order_item->id ) ) {
		$type = 'Flower' == $order_item->pricing_category ? 'flower' : 'service';
		$new_id = false;
		if ( 'flower' == $type && false !== stripos( $order_item->name, '-' ) ) {
			$is_variation = true;
			$new_id = sc_get_migrated_item_id(
				$type,
				null,
				$order_item->item_id
			);
		}

		if ( ! $new_id ) {
			$is_variation = false;
			$new_id = sc_get_migrated_item_id(
				$type,
				$order_item->item_id,
				null
			);
		}

		if ( ! $new_id ) {
			printf( 'No migrated item found for Order Item with ID %d', $order_item->id );
			exit;
		} elseif ( ! $dryrun ) {
			$order_item->item_id = $new_id->new_item_id;

			$order_item->save();

			$wpdb->insert( "{$wpdb->prefix}sc_migrated_item_ids", array(
				'type' => 'order_item',
				'item_id' => $order_item->id,
				'variation_id' => null,
				'new_item_id' => $order_item->id,
			) );
			printf( 'Migrated Order Item #%d<br />', $order_item->id );
		} else {
			printf( 'Dry run migration of Order Item #%d looks good.<br />', $order_item->id );
		}
	} else {
		printf( 'Skipping already migrated Order Item #%d<br />', $order_item->id );
	}
}

if ( ! $had_arr_items && ! $had_recipe_items && ! $had_order_items ) {
	echo 'All Done!<br />';
	if ( $dryrun ) {
		echo 'Dryrun has completed successfully! In order to run the real migration next, go to:<br />' . add_query_arg( array( 'action' => 'sc_migration', 'migration' => 'new-item-structure-migrate-ids', 'number' => $_GET['number'], 'offset' => '0', 'dryrun' => 'no' ) );
	}
	exit;
}

?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 500);

</script>
<?php
exit;
