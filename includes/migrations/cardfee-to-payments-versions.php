<?php 
use Stemcounter\Payment;
use Stemcounter\Event;
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Event_Version;

$migration_ver = 3;

if ( ! sc_migration_is_cli() ) {

	if ( false && Capsule::schema()->hasColumn( 'payments', 'card_fee' ) ) {
		echo 'Migration already performed!';
		exit;
	}

	if ( empty( $_GET['offset'] )  && $_GET['offset'] != 0 ) {
		echo '<br/>=======OFFSET IS EMPTY=======<br/>';
		exit;
	}

	if ( empty( $_GET['number'] ) ) {
		echo '<br/>=======NUMBER IS EMPTY=======<br/>';
		exit;
	}

	$versions = Event_Version::where('event_data', 'LIKE', '%separate_cc_fee":"1"%')->take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'asc')->get();
} else {
	$versions = Event_Version::where('event_data', 'LIKE', '%separate_cc_fee":"1"%')->orderBy('id', 'asc')->get();
}

if ( $versions->count() ) {
	sc_web_cli_out( 'Migrating versions ...' );
	$list = array();

	foreach ( $versions as &$version ) {
		$event_data = json_decode( $version->event_data );
		if ( $event_data ) {
			$event = $event_data->event;
			$eventCost = $event_data->eventCost;

			if ( ! $event->separate_cc_fee ) {
				continue;
			}

			sc_web_cli_out( '- Version id: ' . $version->id . ' for event id: ' . $version->event_id, 
				'- Version id: ' . $version->id . ' for event id: ' . $version->event_id );
			if ( ! empty( $event->migration_ver ) && $migration_ver <= intval( $event->migration_ver ) ) {
				sc_web_cli_out('Already migrated');
				continue;
			}
				
			$total_with_cc = $eventCost->total + sc_format_price( ( $eventCost->total * sc_format_price( $event->card  ) ) / 100 );
			$convenience_fee = $total_with_cc - $eventCost->total;

			if ( 0 != $convenience_fee ) {
				//Gathering report data
				$row = array(
					$event->id,
					$version->version,
					'#', //$user_data->user_email,
					'#', //$user_data->display_name,
					$event->name,
					$eventCost->total,
					$total_with_cc,
					$version->updated_at,
					'#', //$event->status,
					$event->date,
					'#', //$nonpaid_payments,
				);
				$list[] = $row;

				//The real migration
				$arrangement = array(
					'id' 				=> -1,
					'addon' 			=> 0,
					'quantity'			=> 1,
					'include' 			=> 1,
					'note' 				=> '',
					'photos'			=> array(),
					'items'				=> array(),
					'order'				=> count( $event->arrangements ),
					'is_percentage'		=> 0,
					'total'				=> $convenience_fee,
					'subTotal'			=> $convenience_fee,
					'override_cost'		=> $convenience_fee,
					'name'				=> "Card convenience fee",
					'invoicing_category_id' => null,
					'hardgood_multiple'	=> null,
					'fresh_flower_multiple'	=> null,
					'global_labor_value'	=> null,
					"flower_labor"		=>	null,
					"hardgood_labor"	=> null,
					"base_price_labor"	=> null,
					"fee_labor"			=> null,
					"rental_labor"		=> 0,
					"recipes"			=> array(),
				);
				
				$event->arrangements[] = $arrangement;
				$event->migration_ver = $migration_ver;
				$eventCost->subtotal += $arrangement['total'];
				$eventCost->total += $arrangement['total'];

				$event_data->event = $event;
				$event_data->eventCost = $eventCost;

				$version->event_data = json_encode( $event_data );
				$version->save();
			}

		} else {
			sc_web_cli_out( 'No event data' );
		}
	}

	//Write report data
	$fp = fopen('report-versions.csv', 'a');
	foreach ($list as $fields) {
		fputcsv($fp, $fields);
	}
	fclose($fp);
}
 else {
	echo 'Migration done!';
	exit;
}

if ( ! sc_migration_is_cli() ) { ?>

<script type="text/javascript">
setTimeout(function() {
	window.location.href = <?php echo json_encode( add_query_arg( 'offset', $_GET['offset'] + $_GET['number'] ) ); ?>;
}, 1500);
</script>

<?php
	exit;
}

//app/wp-admin/admin-ajax.php?action=sc_migration&migration=cardfee-to-payments-versions&number=50&offset=0