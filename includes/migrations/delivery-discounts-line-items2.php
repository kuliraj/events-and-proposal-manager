<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Event;
use Stemcounter\Meta;
use Stemcounter\Arrangement;
global $wpdb;
global $illuminate_capsule;

if ( empty( $_GET['offset'] )  && $_GET['offset'] != 0 ) {
 echo '<br/>=======OFFSET IS EMPTY=======<br/>';
 exit;
}

if ( empty( $_GET['number'] ) ) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$events = Event::take($_GET['number'])->
	with( array( 'arrangements', ) )->
	offset($_GET['offset'])->
	orderBy('id', 'asc')->
	get();

if ( $events->count() ) {
	echo 'Migrating events ...<br />';
	foreach ( $events as $event ) {
		echo '<br />- Event id: ' . $event->id . '<br />';
		$current_order = $event->arrangements->max( 'order' ) + 1;

		$Migrated = Meta::where( array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'migrated',
			))->first();

		if (! empty( $Migrated )  ) {
			echo 'Already migrated';
			continue;
		}

		Meta::create( array(
			'type' => 'event',
			'type_id' => $event->id,
			'meta_key' => 'migrated',
			'meta_value' => 1,
		));

		//Get collections of ids
		$curent_arrangements_ids = array();
		$curent_fixed_ids = array();
		foreach( $event->arrangements as $arrangement ) {
			if ( ! $arrangement->addon && $arrangement->include ) {
				$curent_arrangements_ids[] = $arrangement->id;
				if ( ! $arrangement->is_percentage ) {
					$curent_fixed_ids[] = $arrangement->id;
				}
			}
		}

		//Set data for percent arrangements
		foreach( $event->arrangements as $arrangement ) {
			if ( $arrangement->is_percentage ) {
				$arrangement->applies_on = $curent_fixed_ids; //--
			}
		}

		if ( 0 < $event->get_delivery( true ) ) {
			$new_arrangement = new Arrangement;
			$new_arrangement->event_id = $event->id;
			$new_arrangement->user_id = $event->user_id;
			$new_arrangement->addon = 0;
			$new_arrangement->quantity = 1;
			$new_arrangement->include = 1;
			$new_arrangement->note = '';
			$new_arrangement->order = $current_order; $current_order++;
			$new_arrangement->is_percentage = ( 'amount' == $event->delivery_type || $event->delivery_tax) ? 0 : 1;
			$new_arrangement->override_cost = $new_arrangement->is_percentage ? $event->delivery : $event->get_delivery( true );
			$new_arrangement->tax = absint( $event->delivery_tax );
			$new_arrangement->name = 'Delivery';
			
			$new_arrangement->invoicing_category_id = null;
			$new_arrangement->hardgood_multiple = null;
			$new_arrangement->fresh_flower_multiple = null;
			$new_arrangement->global_labor_value	= null;
			$new_arrangement->flower_labor		=	null;
			$new_arrangement->hardgood_labor	= null;
			$new_arrangement->base_price_labor	= null;
			$new_arrangement->fee_labor			= null;
			$new_arrangement->rental_labor		= 0;
			
			$new_arrangement->save(); //--

			if ( $new_arrangement->is_percentage ) {
				$new_arrangement->applies_on = $curent_arrangements_ids; //--
			}
			$curent_arrangements_ids[] = $new_arrangement->id;

			$new_arrangement->save(); //--

			echo "Delivery {$event->delivery_type}: {$new_arrangement->override_cost}</br>";
		} else {
			//echo 'No delivery</br>';
		}

		if ( 0 < $event->get_discount( true ) ) {

			$new_arrangement = new Arrangement;
			$new_arrangement->event_id = $event->id;
			$new_arrangement->user_id = $event->user_id;
			$new_arrangement->addon = 0;
			$new_arrangement->quantity = 1;
			$new_arrangement->include = 1;
			$new_arrangement->note = '';
			$new_arrangement->order = $current_order; $current_order++;
			$new_arrangement->is_percentage = ( 'amount' == $event->discount_type ) ?  0 : 1;
			$new_arrangement->override_cost = ( $new_arrangement->is_percentage ) ? -1 * $event->discount_value : -1 * $event->get_discount( true );
			$new_arrangement->tax = 1;
			$new_arrangement->name = 'Discount';
			
			$new_arrangement->invoicing_category_id = null;
			$new_arrangement->hardgood_multiple = null;
			$new_arrangement->fresh_flower_multiple = null;
			$new_arrangement->global_labor_value	= null;
			$new_arrangement->flower_labor		=	null;
			$new_arrangement->hardgood_labor	= null;
			$new_arrangement->base_price_labor	= null;
			$new_arrangement->fee_labor			= null;
			$new_arrangement->rental_labor		= 0;
			
			$new_arrangement->save(); //--

			if ( $new_arrangement->is_percentage ) {
				$new_arrangement->applies_on = $curent_arrangements_ids; //--
			}
			$new_arrangement->save(); //--

			echo "Discount {$event->discount_type}: {$new_arrangement->override_cost}</br>";
		} else {
			//echo 'No discount</br>';
		}
	}
} else {

	/*Meta::where( array(
		'type' => 'event',
		'meta_key' => 'migrated',
	))->delete();*/

	echo 'Migration done!';
	exit;
}

?>

<script type="text/javascript">
setTimeout(function() {
	window.location.href = <?php echo json_encode( add_query_arg( 'offset', $_GET['offset'] + $_GET['number'] ) ); ?>;
}, 1500);
</script>

<?php
exit;

//app/wp-admin/admin-ajax.php?action=sc_migration&migration=delivery-discounts-line-items2&offset=0&number=100