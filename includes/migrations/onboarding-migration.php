<?php
if ( ! sc_migration_is_cli() ) {

	sc_web_cli_out( 'Nope, not a cli...' );
} else {
	$users = get_users(array(
		'orderby' => 'ID'
	));
}

foreach ( $users as $user ) {
	try {
		sc_web_cli_out( 'Migrating users onboadring meta...' );

		$onboarding_tutorial = get_user_meta( $user->ID, 'onboarding_tutorial', true );
		$taken_tutorial = empty( $onboarding_tutorial ) ? false : true;

		if ( is_array( $onboarding_tutorial ) ) {
			foreach ( $onboarding_tutorial as $tutorial ) {
				if ( $tutorial == false ) {
					$taken_tutorial = false;
					break;
				}
			}
		}

		if ( ! $taken_tutorial ) {
			update_user_meta(
				$user->ID, 
				'onboarding_tutorial', 
				array(
					'step-one' => false,
					'step-two' => false,
					'step-three' => false,
				)
			);
		} else {
			update_user_meta(
				$user->ID, 
				'onboarding_tutorial', 
				array(
					'step-one' => true,
					'step-two' => true,
					'step-three' => true,
				)
			);
		}
	} catch (Exception $e) {
		sc_web_cli_out( $e->getMessage() );
	}
}

sc_web_cli_out( 'Migration done!' );
