<?php
use Stemcounter\Event;
use Stemcounter\Arrangements;
use Stemcounter\Arrangement_Item;
use Stemcounter\Arrangement_Recipe;

use Illuminate\Database\Capsule\Manager as Capsule;

if ( ! Capsule::schema()->hasColumn( 'arrangement_items', 'arrangement_recipe_id' ) ) {
	Capsule::schema()->table( 'arrangement_items', function( $table ){
		$table->integer('arrangement_recipe_id')
			->unsigned()
			->nullable();
	} );
}

if ( empty( $_GET['eid'] ) ) {
	if (empty($_GET['offset']) && $_GET['offset'] != 0) {
		echo '<br/>=======OFFSET IS EMPTY=======<br/>';
		exit;
	}
	if (empty($_GET['number'])) {
		echo '<br/>=======NUMBER IS EMPTY=======<br/>';
		exit;
	}
}

if ( ! empty( $_GET['eid'] ) ) {
	$events = Event::where( array( 'id' => $_GET['eid'] ) )->with('arrangements', 'arrangements.items', 'arrangements.items.item_variation')->get();
} else {
	$events = Event::with('arrangements', 'arrangements.items', 'arrangements.items.item_variation')->take($_GET['number'])->offset($_GET['offset'])->orderBy('id', 'desc')->get();
}

if ( $events->isEmpty() ) {
	echo '<br/>		.	MIGRATION ENDED		.	<br/>';
	exit;	
} else {
	echo '<br/>...browsing through arrangements.<br/>';
	foreach ($events as $event) {
		$event->arrangements->sortBy( function($arrangement){
			return $arrangement->order;
		} );

		foreach ( $event->arrangements as $arrangement ) {
			$arrangement_recipes = Arrangement_Recipe::where( array(
				'arrangement_id'	=> $arrangement->id,	
			))->get();

			if ( ! $arrangement_recipes->isEmpty() ) {
				// If arrangement has recipe, skip
				echo '<h4>Arrangement with id: ' . $arrangement->id . ' has recipe!</h4><br />';
				continue;
			} else {
				// New recipe for every arrangement
				$recipe = new Arrangement_Recipe;
				$recipe->arrangement_id = $arrangement->id;
				$recipe->save();

				foreach ( $arrangement->items as $arrangement_item ) {
					$arrangement_item->arrangement_recipe_id = $recipe->id;
					$arrangement_item->save();
				}
			}
			echo 'Recipe created for arrangengement with id: ' . $arrangement->id . '<br />';
		}
	}

}

if ( ! empty( $_GET['eid'] ) ) {
	exit;
}

 ?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;