<?php 
use Stemcounter\Payment;
use Stemcounter\Event;
use Stemcounter\Meta;
use Stemcounter\Arrangement;
use Illuminate\Database\Capsule\Manager as Capsule;


if ( Capsule::schema()->hasColumn( 'arrangements', 'is_applied_all' ) ) {
	sc_web_cli_out( 'Migration already performed!' );
	exit;
}

Capsule::schema()->table( 'arrangements', function( $table ){
	$table->boolean('is_applied_all')
		->default(0);
} );
sc_web_cli_out(  'Migration done!' );
exit;

//app/wp-admin/admin-ajax.php?action=sc_migration&migration=percent-item-applies-all