<?php

use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Recipe;
use Stemcounter\Arrangement_Item;
use Stemcounter\Recipe;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Recipe_Item;

// /app/wp-admin/admin-ajax.php?action=sc_migration&migration=standalone-recipes-update&number=50

/*if ( empty( $_GET['offset'] ) && $_GET['offset'] != 0 ) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}*/

if ( empty( $_GET['number'] ) ) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

global $wpdb;

$recipes = Recipe::with( array(
	'items' => function( $query ){
		$query->withTrashed();
	},
	'items.item' => function( $query ){
		$query->withTrashed();
	},
	'items.item.default_variation' => function( $query ){
		$query->withTrashed();
	},
	'items.item_variation' => function( $query ){
		$query->withTrashed();
	},
) )->take( $_GET['number'] )->orderBy( 'id', 'asc' )->get();

if ( $recipes->isEmpty() ) {
	echo '<br/>.......... MIGRATION ENDED ..........<br/>';
	exit;	
}

echo 'Migrating recipes...<br /><br /><br />';

foreach ( $recipes as $recipe ) {
	$arrangement = new Arrangement;
	$arrangement->event_id = 0;
	$arrangement->user_id = $recipe->user_id;
	$arrangement->name = $recipe->name;
	$arrangement->quantity = 1;
	$arrangement->include = 1;
	$arrangement->override_cost = -1;
	$arrangement->tax = 1;
	$arrangement->note = $recipe->description;
	$arrangement->order = $recipe->id;
	$arrangement->invoicing_category_id = null;
	$arrangement->hardgood_multiple = null;
	$arrangement->fresh_flower_multiple = null;
	$arrangement->global_labor_value = null;
	$arrangement->flower_labor = null;
	$arrangement->hardgood_labor = null;
	$arrangement->base_price_labor = null;
	$arrangement->fee_labor = null;
	$arrangement->rental_labor = null;
	$arrangement->addon = 0;

	try {
		$arrangement->save();
	} catch (Exception $e) {
		printf( 'The following error occurred while trying to save the Arrangement for recipe with ID <code>%d</code>: <pre>%s</pre><br />', $recipe->id, $e->getMessage() );
		continue;
	}

	$arrangement_recipe = new Arrangement_Recipe;
	$arrangement_recipe->name = $recipe->name;
	$arrangement_recipe->arrangement_id = $arrangement->id;

	try {
		$arrangement_recipe->save();
	} catch (Exception $e) {
		printf( 'The following error occurred while trying to save the new Arrangement_Recipe for recipe with ID <code>%d</code>: <pre>%s</pre><br /><br />', $recipe->id, $e->getMessage() );
		// Delete the arrangement as we failed here
		$arrangement->forceDelete();
		continue;
	}

	foreach ( $recipe->items as $recipe_item ) {
		$arrangement_item = new Arrangement_Item;
		$arrangement_item->arrangement_id = $arrangement->id;
		$arrangement_item->item_type = 'Stemcounter\Item';
		$arrangement_item->item_id = $recipe_item->item_id;
		$arrangement_item->item_variation_id = $recipe_item->item_variation_id;
		$arrangement_item->cost = $recipe_item->item_variation->cost;
		if ( is_null( $arrangement_item->cost ) ) {
			// If even the default variation has no cost, set it to 0
			$arrangement_item->cost = ! is_null( $recipe_item->item->default_variation->cost ) ? $recipe_item->item->default_variation->cost : 0;
		}
		$arrangement_item->quantity = $recipe_item->quantity;
		$arrangement_item->arrangement_recipe_id = $arrangement_recipe->id;

		try {
			$arrangement_item->save();
		} catch (Exception $e) {
			printf( 'The following error occurred while trying to save the new Arrangement_Item with Recipe_Item ID <code>%d</code>: <pre>%s</pre><br /><br />', $recipe_item->id, $e->getMessage() );
			continue;
		}
	}

	try {
		$recipe->delete();
	} catch (Exception $e) {
		printf( 'Failed to soft-delete Recipe with ID <code>%d</code>: <pre>%s</pre><br /><br />', $recipe->id, $e->getMessage() );
		continue;
	}

	printf( 'Successfully migrated Recipe <code>%d</code><br /><br /><br />', $recipe->id );
}
?>

<script>
	setTimeout(function(){
		window.location = <?php echo json_encode( add_query_arg( 'cachebust', microtime() ) ); ?>;
	}, 500);
</script>