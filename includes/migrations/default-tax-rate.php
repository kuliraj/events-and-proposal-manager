<?php

global $wpdb;

use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Tax_Rate;

if ( ! Capsule::schema()->hasColumn( 'tax_rates', 'default' ) ) {
	Capsule::schema()->table( 'tax_rates', function( $table ){
		$table->integer('default')
			->default(0)
			->after('value');
	} );
}

if ( ! Capsule::schema()->hasColumn( 'tax_rates', 'default2' ) ) {
	Capsule::schema()->table( 'tax_rates', function( $table ){
		$table->integer('default2')
			->default(0)
			->after('default');
	} );
}

$wpdb->show_errors();

$tax_rate_ids = $wpdb->get_col( "SELECT id FROM {$wpdb->prefix}sc_tax_rates WHERE deleted_at IS NULL GROUP BY user_id" );
$tax_rate_ids = implode( $tax_rate_ids, ',' );

$tax_rates = $wpdb->query( "UPDATE {$wpdb->prefix}sc_tax_rates SET `default`=1 WHERE id IN ($tax_rate_ids)" );

echo "Migrated $tax_rates tax rates.";
