<?php

if ( ! class_exists( 'WP_CLI' ) ) {
    return;
}

/**
 * Allows you to run StemCounter migrations
 */
class SC_Command_Migration extends WP_CLI_Command {
    /**
    * Runs a StemCounter migration
    *
    * ## OPTIONS
    *
    * <migration>
    * : The name of a migration to run
    *
    * ## EXAMPLES
    *
    *     wp stemcounter migration run sample-migration
    *
    * @when after_wp_load
    */
    public function run( $args, $assoc_args ) {
        list( $migration ) = $args;

        if ( ! file_exists( dirname( __FILE__ ) . '/' . $migration . '.php' ) ) {
            \WP_CLI::error( \WP_CLI::colorize( 'No such migration: %r' . $file . '%n' ) );
        }
        
        global $is_cli;
        $is_cli = true;

        require_once( dirname( __FILE__ ) . '/' . $migration . '.php' );
    }
}

WP_CLI::add_command( 'stemcounter migration', 'SC_Command_Migration' );