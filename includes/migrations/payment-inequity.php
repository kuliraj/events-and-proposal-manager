<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Event;
use Stemcounter\Payment;
global $wpdb;

if ( ! Capsule::schema()->hasColumn( 'payments', 'is_live' ) ) {
	Capsule::schema()->table( 'payments', function( $table ){
		$table->integer('is_live')
			->unsigned()
			->after('payment_type')
			->default(0);
	} );
}

if ( empty( $_GET['offset'] ) && $_GET['offset'] != 0 ) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}

if ( empty( $_GET['number'] ) ) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

// Get x number of events that have at least one version
$events = Event::take( $_GET['number'] )
			->offset( $_GET['offset'] )->orderBy( 'id', 'asc' )
			->with( array( 'latest_version', 'payments' ) )
			->has( 'latest_version' )
			->has( 'payments' )
			->get();

if ( ! $events->isEmpty() ) {
	foreach ( $events as $event ) {
		printf( 'Migrating the payments for event %d<br />', $event->id );
		$version_data = json_decode( $event->latest_version->event_data );
		$event_total = sc_format_price( $version_data->eventCost->total );
		if ( ! empty( $version_data->event->separate_cc_fee ) && ! empty( $version_data->event->card ) ) {
			$event_total += sc_format_price( ( $event_total * sc_format_price( $version_data->event->card ) ) / 100 );
		}
		$scheduled_amount = 0;
		$total_payments = $event->payments->count() - 1;
		$event->payments = $event->payments->sort( function( $a, $b ) {
			if ( $a->due_date == $b->due_date ) {
				return $a->id < $b->id ? -1 : 1;
			}

			return ( strtotime( $a->due_date ) < strtotime( $b->due_date ) ) ? -1 : 1;
		} );
		$i = 0;

		foreach ( $event->payments as $payment ) {
			$_payment = $payment->replicate( array( 'created_at', 'updated_at', 'id' ) );
			$_payment->is_live = 1;
			// Live payments always use fixed amounts
			$_payment->payment_percentage = null;
			// If it's the last payment, we need to set amount to remaining balance
			if ( $i == $total_payments ) {
				$_payment->amount = $event_total - $scheduled_amount;
				$_payment->amount = floatval( sc_format_price( $_payment->amount ) );
			} else if ( ! empty( $payment->payment_percentage ) && 'complete' != $payment->status ) {
				$_payment->amount = floatval( sc_format_price( $event_total * ( $payment->payment_percentage / 100 ) ) );
			} else {
				$_payment->amount = floatval( sc_format_price( $payment->amount ) );
			}

			$scheduled_amount += $_payment->amount;

			// Save the new payment to the DB
			try {
				$_payment->save();
			} catch ( Exception $e ) {
				printf( '<script>alert("Failed to migrate payment #%d for event #%d. The error was: \"%s\"\nPress OK to resume the migration.");</script>', $payment->id, $event->id, esc_js( $e->getMessage() ) );
			}
			$i ++;
		}
	} ?>
	<script type="text/javascript">
		setTimeout(function() {
			window.location.href = <?php echo json_encode( add_query_arg( 'offset', $_GET['offset'] + $_GET['number'] ) ); ?>;
		}, 1500);
	</script>
	<?php
} else {
	echo 'Migration done.';
	exit;
}

/*$wpdb->query( "INSERT INTO {$wpdb->prefix}sc_payments ( user_id, event_id, payment_name, due_date, payment_percentage, payment_date, amount, payment_note, status, payment_type, is_live )
	SELECT user_id, event_id, payment_name, due_date, payment_percentage, payment_date, amount, payment_note, status, payment_type, '1' AS is_live FROM {$wpdb->prefix}sc_payments
	WHERE is_live=0" );*/

