<?php
use Stemcounter\Order;
use Stemcounter\Order_Item;

use Illuminate\Database\Capsule\Manager as Capsule;

if ( ! Capsule::schema()->hasColumn( 'orders', 'fulfilment_date' ) ) {
	Capsule::schema()->table( 'orders', function( $table ){
		$table->date('fulfilment_date')
			->nullable()
			->default( null );
	} );
	echo 'Table added successfully.';
} else {
	echo 'Column already exists.';
}