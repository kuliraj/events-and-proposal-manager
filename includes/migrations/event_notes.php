<?php
use Illuminate\Database\Capsule\Manager as Capsule;

global $wpdb;

/* create table */
if ( ! Capsule::schema()->hasColumn( 'events', 'event_note' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->text('event_note');
	} );
}
/* rename tables */
if ( Capsule::schema()->hasColumn( 'events', 'note' ) ) {
	$wpdb->query( "ALTER TABLE {$wpdb->prefix}sc_events CHANGE note private_note TEXT" );

}

if ( Capsule::schema()->hasColumn( 'events', 'proposal_note' ) ) {
	$wpdb->query( "ALTER TABLE {$wpdb->prefix}sc_events CHANGE proposal_note contract_note TEXT" );
}

echo 'Migration done.';
exit;