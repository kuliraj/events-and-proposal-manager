<?php

if ( ! sc_migration_is_cli() ) {
	echo 'Sorry - WP CLI only :(';
	exit;
}

$users = get_users( array( 'fields' => 'ID' ) );
remove_action( 'affwp_set_affiliate_status', 'affwp_notify_on_approval', 10 );
foreach ( $users as $user_id ) {
	$result = sc_setup_affiliate( absint( $user_id ) );

	if ( is_wp_error( $result ) ) {
		sc_web_cli_out(
			sprintf( 'Failed to create affiliate for user %d. Error was: %s', $user_id, $result->get_error_message() ),
			WP_CLI::colorize( sprintf( 'Failed to create affiliate for user %%r%d%%n. Error was: %%r%s%%n', $user_id, $result->get_error_message() ) )
		);
	} else {
		sc_web_cli_out(
			sprintf( 'Created affiliate(#%d) for user #%d', $result, $user_id ),
			WP_CLI::colorize( sprintf( 'Created affiliate(%%g#%d%%n) for user %%g#%d%%n', $result, $user_id ) )
		);
	}
}

sc_web_cli_out(
	'Done.'
);
