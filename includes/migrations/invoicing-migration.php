<?php 
use Illuminate\Database\Capsule\Manager as Capsule;

if ( ! Capsule::schema()->hasColumn( 'events', 'delivery' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->float('delivery', 6, 2)
			->default( 0 );
	} );
}

if ( ! Capsule::schema()->hasColumn( 'events', 'card' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->float('card', 6, 2)
			->default( 0 );
	} );
}

global $wpdb;

$users = get_users();
foreach ( $users as $user ) {
	$global_CC_info = get_user_meta(
		$user->ID,
		'__charge_card_rate',
		true
	);

	$global_delivery_info = get_user_meta(
		$user->ID,
		'__delivery_cost',
		true
	);

	$data = array(
		'delivery'	=> $global_delivery_info,
		'card'		=> $global_CC_info,
	);

	$affected_rows = $wpdb->update( $wpdb->prefix . 'sc_events', $data, array( 'user_id' => $user->ID ) );

	echo "$affected_rows events migrated for user {$user->user_login}\n\n";
}
exit;