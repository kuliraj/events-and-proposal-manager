<?php
use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'events', 'state' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->string( 'state' )
			->default('inquiry');
	} );
}
global $wpdb;
$wpdb->query( "UPDATE {$wpdb->prefix}sc_events SET state = 'inquiry'" );
echo 'Migration done.';
exit;