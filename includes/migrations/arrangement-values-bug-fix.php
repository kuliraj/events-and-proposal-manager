<?php
ini_set('memory_limit', '-1');
global $wpdb;

$wpdb->query("UPDATE `{$wpdb->prefix}sc_arrangements`
SET global_labor_value = IF ( flower_labor != 0.00 AND flower_labor IS NOT NULL
	, flower_labor,
	IF ( hardgood_labor != 0.00 AND hardgood_labor IS NOT NULL
	, hardgood_labor,
		IF ( base_price_labor != 0.00 AND base_price_labor IS NOT NULL
		, base_price_labor,
			IF ( fee_labor != 0.00 AND fee_labor IS NOT NULL
			, fee_labor,
				IF ( rental_labor != 0.00 AND rental_labor IS NOT NULL
				, rental_labor,
				0.00 )
			)
		)
	)
)
WHERE global_labor_value = 0.00");

echo '<br/>=======MIGRATION ENDED=======<br/>';
exit;
