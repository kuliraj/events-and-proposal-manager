<?php
global $wpdb;

$metas = $wpdb->get_results( "SELECT * FROM {$wpdb->usermeta} WHERE meta_key='sc_user_tags'");
$wpdb->show_errors();

foreach ( $metas as $meta ) {
	$value = maybe_unserialize( $meta->meta_value );
	if ( is_array( $value ) ) {
		$changed = false;
		foreach ( $value as $key => $_value ) {
			if ( ! is_array( $_value ) ) {
				$changed = true;
				$value[ $key ] = array( 'name' => $_value, 'color' => '' );
			}
		}
		
		if ( $changed ) {
			$value = serialize( $value );
			
			// Update row in DB
			if ( $wpdb->query( $wpdb->prepare("UPDATE {$wpdb->usermeta} SET meta_value=%s WHERE umeta_id=%d", $value, $meta->umeta_id ) ) ) {
				printf( 'Migrated tags for user %d<br />', $meta->user_id );
			} else {
				printf( '<br />FAILED TO MIGRATE TAGS FOR USER %d<br /><br />', $meta->user_id );
			}
		}
	}
}

exit;