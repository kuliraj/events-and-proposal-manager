<?php
use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'arrangements', 'hide_price' ) ) {
	Capsule::schema()->table( 'arrangements', function( $table ){
		$table->integer('hide_price')
			->unsigned()
			->default(0);
	} );
}
echo 'Migration done.';
exit;