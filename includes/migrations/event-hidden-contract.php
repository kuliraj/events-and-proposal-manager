<?php 
use Stemcounter\Event;
use Illuminate\Database\Capsule\Manager as Capsule;

if ( ! Capsule::schema()->hasColumn( 'events', 'hidden_contract' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->integer('hidden_contract')
			->unsigned()
			->default(0);
	} );
	echo 'Migration done!';
} else {
	echo 'Migration already performed!';
}

//app/wp-admin/admin-ajax.php?action=sc_migration&migration=event-hidden-contract