<?php
use Stemcounter\Flower;
use Stemcounter\Variation;
use Stemcounter\Service;
use Stemcounter\Event;
use Stemcounter\Customer;
use Stemcounter\Vendor;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Item;
use Stemcounter\Arrangement_Photo;

function sc_m_format_float($value) {
	return floatval(preg_replace('/[^0-9\.]/', '', $value));
}

function sc_migrate_flowers($batch_index=0) {
	global $wpdb;

	$per_batch = 30;

	$raw_items = $wpdb->get_results($wpdb->prepare('
		SELECT *
		FROM `' . $wpdb->prefix . 'user_flowers`
		LIMIT %d, %d
	', $batch_index * $per_batch, $per_batch));

	$migrated = 0;
	if (!$raw_items) {
		return -1;
	}
	foreach ($raw_items as $raw_item) {
		$item = Flower::firstOrNew(array(
			'user_id'=>$raw_item->user_id,
			'name'=>$raw_item->flower,
		));
		if ($item->exists) {
			continue; // already migrated
		}

		$item->user_id = $raw_item->user_id;
		$item->name = ucfirst($raw_item->flower);
		$item->stems = intval($raw_item->stems);
		$item->cost = sc_m_format_float($raw_item->cost);
		$item->save();

		$variations = array();
		$colors = array_map('ucfirst', array_unique(array_filter(array_map('trim', explode(',', $raw_item->color)))));
		foreach ($colors as $color_name) {
			$variation = Variation::firstOrCreate(array(
				'name'=>$color_name,
			));
			$variations[] = $variation->id;
		}
		$item->variations()->sync($variations);

		$migrated ++;
	}
	return $migrated;
}

function sc_migrate_hardgoods($batch_index=0) {
	global $wpdb;

	$per_batch = 30;

	$raw_items = $wpdb->get_results($wpdb->prepare('
		SELECT *
		FROM `' . $wpdb->prefix . 'user_hardgood`
		LIMIT %d, %d
	', $batch_index * $per_batch, $per_batch));

	$migrated = 0;
	if (!$raw_items) {
		return -1;
	}
	foreach ($raw_items as $raw_item) {
		$item = Service::firstOrNew(array(
			'user_id'=>$raw_item->user_id,
			'name'=>$raw_item->user_item,
			'type'=>Service::TYPE_HARDGOOD,
		));
		if ($item->exists) {
			continue; // already migrated
		}

		$item->type = Service::TYPE_HARDGOOD;
		$item->user_id = $raw_item->user_id;
		$item->name = ucfirst($raw_item->user_item);
		$item->description = ucfirst($raw_item->user_description);
		$item->inventory = intval($raw_item->user_item_avl);
		$item->cost = sc_m_format_float($raw_item->user_cost);
		$item->save();

		$migrated ++;
	}
	return $migrated;
}

function sc_migrate_my_items($batch_index=0) {
	global $wpdb;

	$per_batch = 30;

	$raw_items = $wpdb->get_results($wpdb->prepare('
		SELECT *
		FROM `' . $wpdb->prefix . 'user_items`
		LIMIT %d, %d
	', $batch_index * $per_batch, $per_batch));

	$types = array(
		'Base Price'=>Service::TYPE_BASE_PRICE,
		'Fee'=>Service::TYPE_FEE,
		'Hardgood'=>Service::TYPE_HARDGOOD,
		'Rental'=>Service::TYPE_RENTAL,
	);

	$migrated = 0;
	if (!$raw_items) {
		return -1;
	}
	foreach ($raw_items as $raw_item) {
		$type = isset($types[$raw_item->user_item_type]) ? $types[$raw_item->user_item_type] : Service::TYPE_HARDGOOD;

		$item = Service::firstOrNew(array(
			'user_id'=>$raw_item->user_id,
			'name'=>$raw_item->user_item,
			'type'=>$type,
		));
		if ($item->exists) {
			continue; // already migrated
		}

		$item->type = $type;
		$item->user_id = $raw_item->user_id;
		$item->name = ucfirst($raw_item->user_item);
		$item->description = ucfirst($raw_item->user_description);
		$item->inventory = intval($raw_item->user_item_avl);
		$item->cost = sc_m_format_float($raw_item->user_cost);
		$item->save();

		$migrated ++;
	}
	return $migrated;
}

function sc_migrate_events($batch_index=0) {
	global $wpdb;

	$per_batch = 5;

	$raw_items = $wpdb->get_results($wpdb->prepare('
		SELECT *
		FROM `' . $wpdb->prefix . 'user_events`
		LIMIT %d, %d
	', $batch_index * $per_batch, $per_batch));

	$migrated = 0;
	if (!$raw_items) {
		return -1;
	}
	foreach ($raw_items as $raw_item) {
		$item = Event::firstOrNew(array(
			'user_id'=>$raw_item->user,
			'name'=>$raw_item->event_name,
		));
		if ($item->exists) {
			continue; // already migrated
		}

		$item->user_id = $raw_item->user;
		$item->name = ucfirst($raw_item->event_name);
		$item->created_at = date('Y-m-d H:i:s', strtotime($raw_item->creation_date));
		if ($raw_item->archived == 1) {
			$item->status = Event::STATUS_ARCHIVED;
		}
		if (isset($raw_item->event_date) && strtotime($raw_item->event_date) > 0) {
			$item->date = date('Y-m-d', strtotime($raw_item->event_date));
		} else {
			$item->date = date('Y-m-d');
		}
		$details = @json_decode($raw_item->details);
		$customer = new Customer();
		$customer->first_name = @$details->firstName ? $details->firstName : '';
		$customer->last_name = @$details->lastName ? $details->lastName : '';
		if ($details && isset($details->contact_info)) {
			$customer->phone = @$details->contact_info->phone ? $details->contact_info->phone : '';
			$customer->email = @$details->contact_info->email ? $details->contact_info->email : '';
		}
		$customer->save();
		$item->customer()->associate($customer);

		$vendor = new Vendor();
		$vendor->name = @$details->Venue ? $details->Venue : '';;
		$vendor->address = @$details->address ? $details->address : '';;
		$vendor->save();
		$item->vendor()->associate($vendor);

		$item->note = @$details->event_note ? $details->event_note : '';

		$item->save();

		$event_order = @json_decode($raw_item->event_order);
		if ($event_order) {
			foreach ($event_order as $raw_arrangement) {
				$arrangement = new Arrangement();
				$arrangement->name = @$raw_arrangement->arrangement ? $raw_arrangement->arrangement : '';
				$arrangement->note = @$raw_arrangement->arrangement_note ? $raw_arrangement->arrangement_note : '';
				$arrangement->quantity = intval(@$raw_arrangement->qty);
				$arrangement->include = intval(@$raw_arrangement->includeProposal) ? 1 : 0;
				$item->arrangements()->save($arrangement);

				foreach ($raw_arrangement->items as $name => $raw_arrangement_item) {
					try {
						if (@$raw_arrangement_item->type == 'flower') {
							try {
								$ai_item = Flower::where(array(
									'user_id'=>$item->user_id,
									'name'=>$name,
								))->firstOrFail();
							} catch (Exception $e) {
								$variation_stripped_name = explode('-', $name);
								$variation_stripped_name = implode('-', array_slice($variation_stripped_name, 0, -1));
								$ai_item = Flower::where(array(
									'user_id'=>$item->user_id,
									'name'=>$variation_stripped_name,
								))->firstOrFail();
							}
						} else {
							$ai_item = Service::where(array(
								'user_id'=>$item->user_id,
								'name'=>$name,
							))->firstOrFail();
						}
					} catch (Exception $e) {
						continue;
					}

					$variation = null;
					if (is_a($ai_item, 'Stemcounter\Flower')) {
						$variation_name = array_map('trim', explode('-', $name));
						$variation_name = $variation_name[count($variation_name) - 1];
						if ($variation_name) {
							try {
								$variation = Variation::where(array(
									'name'=>$variation_name,
								))->firstOrFail();
							} catch (Exception $e) {
								// do nothing; $variation is already null
							}
						}
					}

					$arrangement_item = new Arrangement_Item();
					$arrangement_item->cost = sc_m_format_float(@$raw_arrangement_item->cost);
					$arrangement_item->quantity = sc_m_format_float(@$raw_arrangement_item->qty);
					
					$arrangement_item->item()->associate($ai_item);

					if ($variation) {
						$arrangement_item->item_variation()->associate($variation);
					}

					$arrangement->items()->save($arrangement_item);
				}
			}
		}

		$migrated ++;
	}
	return $migrated;
}

// Execute
$step_index = isset($_GET['step_index']) ? abs(intval($_GET['step_index'])) : 0;
$batch_index = isset($_GET['batch_index']) ? abs(intval($_GET['batch_index'])) : 0;
$steps = array(
	'sc_migrate_flowers',
	'sc_migrate_hardgoods',
	'sc_migrate_my_items',
	'sc_migrate_events',
);

if (isset($steps[$step_index])) {
	$step = $steps[$step_index];
	$result = $step($batch_index);

	$next_step_index = $step_index;
	$next_batch_index = $batch_index;
	if ($result == -1) {
		$next_step_index ++;
		$next_batch_index = 0;
	} else {
		$next_batch_index ++;
	}
	$next_step_url = '?sc_migrate&step_index=' . $next_step_index . '&batch_index=' . $next_batch_index;
	?>
	<p>
		<strong>Please wait ...</strong><br />
		Step <?php echo ($step_index + 1); ?> of <?php echo count($steps); ?>: Batch <?php echo ($batch_index + 1); ?>
	</p>
	<script type="text/javascript">
	setTimeout(function(){
		window.location = <?php echo json_encode($next_step_url); ?>;
	}, 1000);
	</script>
	<?php
} else {
	echo 'Migration done.';
}