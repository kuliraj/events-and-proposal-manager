<?php
use Stemcounter\Event;
use Stemcounter\Order;
use Stemcounter\Flower;
use Illuminate\Database\Capsule\Manager as Capsule;

ini_set('memory_limit', '-1');

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

$orders = Order::take($_GET['number'])->offset($_GET['offset'])->orderBy('user_id', 'asc')->get();

if ($orders->isEmpty()) {
	echo '<br/>=======ORDER MIGRATION ENDED=======<br/>';
	exit;
} else {
	echo '<br/>Migrating Orders...<br/>';
	foreach ($orders as $order) {
		$order_events = json_decode($order->event_ids);

		try {
			$shopping_list = array();

			foreach ($order_events as $event_id) {
				$event = Event::where(array(
					'id' => $event_id,
				))->with(array(
					'arrangements' => function($query) {
						$query->where('include', '=', 1);
					},
				), 'arrangements.items', 'arrangements.items.item_variation' )->firstOrFail();

				foreach ($event->arrangements as $arrangement) {
					foreach ($arrangement->items as $arrangement_item) {
						$type = sc_get_simple_item_type($arrangement_item->item, false, $arrangement_item);
						$type_label = sc_get_item_type($arrangement_item->item, true, $arrangement_item);
						$name = $arrangement_item->get_full_name();
						$quantity = $arrangement_item->quantity * $arrangement->quantity;
						$item_cost = $arrangement_item->cost;

						if ($type == 'flower') {
							$items_su = $arrangement_item->item->stems;
							//if is deleted we need to know how much SU does that flower have
							if (is_null($items_su)) {
								try {
									$flower = Flower::where(array(
				                        'id' => $arrangement_item->item_id
				                    ))->withTrashed()->firstOrFail()->toArray();
			    	                $items_su = $flower['stems'];
								} catch (Exception $e) {
								}
							}
							$su_needed = ceil($quantity / $items_su);
							$total_cost = ($items_su * $su_needed) * $item_cost;
							$total_cost = $total_cost;
							$leftovers = ($items_su * $su_needed) - $quantity;
						} else if ($type == 'service'){
							$total_cost = $quantity * $item_cost;
						}

						if (!isset($shopping_list[$event_id][$type][$name])) {
							$shopping_list[$event_id][$type][$name] = array(
								'type' => $type_label,
								'name' => $name,
								'quantity' => $quantity,
								'cost' => $total_cost,
							);

							if ($type == 'flower') {
								$shopping_list[$event_id][$type][$name]['items_su'] = $items_su;
								$shopping_list[$event_id][$type][$name]['su_needed'] = $su_needed;
								$shopping_list[$event_id][$type][$name]['leftovers'] = $leftovers;
							}

						} else {
							$shopping_list[$event_id][$type][$name]['quantity'] += $quantity;

							if ($type == 'flower') {
								$su_needed = ceil($shopping_list[$event_id][$type][$name]['quantity'] / $items_su);
								$total_cost = ($items_su * $su_needed) * $item_cost;
								$total_cost = $total_cost;
								$leftovers = ($items_su * $su_needed) - $shopping_list[$event_id][$type][$name]['quantity'];

								$shopping_list[$event_id][$type][$name]['su_needed'] = $su_needed;
								$shopping_list[$event_id][$type][$name]['cost'] = $total_cost;
								$shopping_list[$event_id][$type][$name]['leftovers'] = $leftovers;
							} else if ($type == 'service') {
								$shopping_list[$event_id][$type][$name]['quantity'] += $quantity; 
								$shopping_list[$event_id][$type][$name]['cost'] += $total_cost; 
							}
						}
					}
				}

				ksort($shopping_list[$event_id]['flower']);
				ksort($shopping_list[$event_id]['service']);
			}
				
			$calculated_shopping_list = array(
				'flower' => array(),
				'service' => array()
			);
			foreach ($shopping_list as $event_id => $flowers_items) {
				foreach ($flowers_items as $type => $values) {
					if ($type == 'flower' && !empty($values)) {
						foreach ($values as $name => $info) {
							if (isset($calculated_shopping_list['flower'][$name])) {
								$calculated_shopping_list['flower'][$name] = array(
									'quantity' => $calculated_shopping_list['flower'][$name]['quantity'] + $info['quantity'],
									'cost' => $calculated_shopping_list['flower'][$name]['cost'] + $info['cost'],
									'items_su' => $info['items_su'],
									'su_needed' => ceil(($calculated_shopping_list['flower'][$name]['quantity'] + $info['quantity']) / $info['items_su']),
									'leftovers' => ($info['items_su'] * ceil(($calculated_shopping_list['flower'][$name]['quantity'] + $info['quantity']) / $info['items_su'])) - ($calculated_shopping_list['flower'][$name]['quantity'] + $info['quantity'])
								);
							} else {
								$calculated_shopping_list['flower'][$name] = array(
									'quantity' => $info['quantity'],
									'cost' => $info['cost'],
									'items_su' => $info['items_su'],
									'su_needed' => $info['su_needed'],
									'leftovers' => $info['leftovers']
								);
							}
						}
					} else if ($type == 'service' && !empty($values)) {
						foreach ($values as $name => $info) {
							if (isset($calculated_shopping_list['service'][$name])) {
								$calculated_shopping_list['service'][$name] = array(
									'quantity' => $calculated_shopping_list['service'][$name]['quantity'] + $info['quantity'],
									'cost' => $calculated_shopping_list['service'][$name]['cost'] + $info['cost']
								);
							} else {
								$calculated_shopping_list['service'][$name] = array(
									'quantity' => $info['quantity'],
									'cost' => $info['cost']
								);
							}
						}
					}
				}
			}

			foreach ($calculated_shopping_list['flower'] as $item) {
				$total_floral += $item['cost'];
			}
			foreach ($calculated_shopping_list['service'] as $item) {
				$total_service += $item['cost'];
			}

			$total_combined = $total_floral + $total_service;
		} catch (Exception $e) {
			echo $e->getMessage();
		}

		$order->estimate = $total_combined;
		$order->flowers_items = json_encode($shopping_list);
		$order->save();
	}
}

?>
<script type="text/javascript">
function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

setTimeout(function() {
	window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
}, 1500);

</script>
<?php
exit;