<?php 
use Stemcounter\Email_Template;

if ( ! sc_migration_is_cli() ) {

	sc_web_cli_out( 'Nope, not a cli...' );
} else {
	$users = get_users(array(
		'orderby' => 'ID'
	));
}

foreach ( $users as $user ) {
	try {
		sc_web_cli_out( 'Migrating email templates...' );
		$user_email_templates = Email_Template::where( array(
			'user_id' => $user->ID,
		) )->get();
		
		if ( ! $user_email_templates->isEmpty() ) {
			continue;
		} else {
			$company_info = get_company_info( $user->ID );

			$user_email_template = new Email_Template();
			$user_email_template->user_id = $user->ID;
			$user_email_template->label = 'Default Template';
			$user_email_template->subject = 'Proposal';
			if ( ! empty( $company_info['company'] ) ) {
				$user_email_template->content = '<p>Here is your proposal. You can reply to this email with any questions that we can answer for you.</p><p><br></p><p><br></p><p>' . $company_info['company'] .'</p>';
			} else {
				$user_email_template->content = '<p>Here is your proposal. You can reply to this email with any questions that we can answer for you.</p>';
			}
			$user_email_template->default = 1;
			$user_email_template->save();
		}
	} catch (Exception $e) {
		sc_web_cli_out( $e->getMessage() );
	}
}

sc_web_cli_out( 'Migration done!' );