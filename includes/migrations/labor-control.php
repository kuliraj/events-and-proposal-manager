<?php
$item_types = array_keys(sc_get_mixed_arrangement_item_types());
$batch_index = isset($_GET['batch_index']) ? abs(intval($_GET['batch_index'])) : 0;
$per_batch = 100;

$users = get_users(array(
	'offset'=>($per_batch * $batch_index),
	'number'=>$per_batch,
	'fields'=>array('ID'),
));
$stats = count_users();
$total_batches = ceil($stats['total_users'] / $per_batch);

if (!empty($users)) {
	foreach ($users as $u) {
		update_user_meta($u->ID, '_sc_add_labor_costs', 'y');

		$values = get_user_meta($u->ID, '_sc_apply_labor_to', false);
		if (!empty($values)) {
			continue; // values already set for this user; skip
		}
		foreach ($item_types as $item_type) {
			add_user_meta($u->ID, '_sc_apply_labor_to', $item_type);
		}
	}
	$next_batch_url = add_query_arg('batch_index', $batch_index + 1);
	?>
	<p>
		<strong>Please wait ...</strong><br />
		Batch <?php echo ($batch_index + 1); ?> of <?php echo $total_batches; ?>
	</p>
	<script type="text/javascript">
	setTimeout(function(){
		window.location = <?php echo json_encode($next_batch_url); ?>;
	}, 1000);
	</script>
	<?php
} else {
	echo 'Migration done.';
}
exit;