<?php 
use Stemcounter\Email_Template;

if ( ! sc_migration_is_cli() ) {
	sc_web_cli_out( 'Sorry - this migration is CLI only...' );
	exit;
} else {
	$users = get_users( array(
		'orderby' => 'ID'
	) );
}

global $wpdb;
sc_web_cli_out( 'Updating purchase units to "item"...' );
$result = $wpdb->query( "UPDATE {$wpdb->prefix}sc_items SET purchase_unit = 'item' WHERE purchase_unit = 'each'" );
sc_web_cli_out( 'Updated ' . $result . '.' );

sc_web_cli_out( 'Updating recipe units to "item"...' );
$result = $wpdb->query( "UPDATE {$wpdb->prefix}sc_items SET recipe_unit = 'item' WHERE recipe_unit = 'each'" );
sc_web_cli_out( 'Updated ' . $result . '.' );

foreach ( $users as $user ) {
	sc_web_cli_out( 'Setting default measuring units for ' . $user->user_email );
	$units = get_user_meta( $user->ID, 'sc_user_measuring_units', true );
	if ( ! $units ) {
		update_user_meta( $user->ID, 'sc_user_measuring_units', array( 'item' ) );
	}
}

sc_web_cli_out( 'Migration done!' );