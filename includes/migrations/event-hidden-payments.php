<?php
use Stemcounter\Event;
use Stemcounter\Payment;
use Stemcounter\Event_Version;

use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'events', 'visible_payments' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->integer('visible_payments')
			->unsigned()
			->default(1);
	} );
}

if (empty($_GET['offset']) && $_GET['offset'] != 0) {
	echo '<br/>=======OFFSET IS EMPTY=======<br/>';
	exit;
}
if (empty($_GET['number'])) {
	echo '<br/>=======NUMBER IS EMPTY=======<br/>';
	exit;
}

global $wpdb;
$events = $wpdb->get_col( $wpdb->prepare( "SELECT id FROM {$wpdb->prefix}sc_events LIMIT %d, %d", $_GET['offset'], $_GET['number'] ) );
$events = implode( ',' , $events );
$wpdb->query(
	"UPDATE `{$wpdb->prefix}sc_events` as e
		SET `visible_payments`= (
			SELECT IF( COUNT(*) > 1, 1, 0 ) AS c
				FROM {$wpdb->prefix}sc_payments
					WHERE event_id = e.id
		)
		WHERE id IN ($events)"
);
echo '<br/>';
echo 'Events stack with ID\'s was migrated: ' . $events . '<br />';

$versions = Event_Version::take( $_GET['number'] )->offset( $_GET['offset'] )->orderBy( 'id', 'asc' )->get();

echo 'Migrating Event Versions... Please wait.<br /><br />';

if ( ! $versions->isEmpty() ) {
	foreach ( $versions as $version ) {
		$versioned_event = json_decode( $version->event_data );
		if ( isset( $versioned_event->event->visible_payments ) ) {
			echo '<br/>';
			echo 'Version with ID - ' . $versioned_event->event->id . ' has already been migrated';
			continue;
		}
		$payments_num = 0;
		if ( isset( $versioned_event->payments ) ) {
			$payments_num = count( $versioned_event->payments );
		} elseif ( isset( $versioned_event->scheduledPayments ) ) {
			$payments_num = count( $versioned_event->scheduledPayments );
		}

		if ( 1 < $payments_num ) {
			$versioned_event->event->visible_payments = 1;
		} else {
			$versioned_event->event->visible_payments = 0;
		}

		echo 'Version migrated for event with id: ' . $versioned_event->event->id . '<br />';

		$version->event_data = json_encode( $versioned_event );
		$version->save();
	}
	?>
	<script type="text/javascript">
	function updateURLParameter(url, param, paramVal){
	    var newAdditionalURL = "";
	    var tempArray = url.split("?");
	    var baseURL = tempArray[0];
	    var additionalURL = tempArray[1];
	    var temp = "";
	    if (additionalURL) {
	        tempArray = additionalURL.split("&");
	        for (i=0; i<tempArray.length; i++){
	            if(tempArray[i].split('=')[0] != param){
	                newAdditionalURL += temp + tempArray[i];
	                temp = "&";
	            }
	        }
	    }

	    var rows_txt = temp + "" + param + "=" + paramVal;
	    return baseURL + "?" + newAdditionalURL + rows_txt;
	}

	setTimeout(function() {
		window.location.href = updateURLParameter(window.location.href, 'offset', <?php echo $_GET['offset'] + $_GET['number']; ?>);
	}, 500);

	</script>
	<?php
} else {
	echo 'Migration done.';
}
exit;
