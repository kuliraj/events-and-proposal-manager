<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Tax_Rate;

if ( ! Capsule::schema()->hasColumn( 'events', 'tax_rate_id2' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->integer('tax_rate_id2')
			->unsigned()
			->after('tax_rate_value')
			->nullable();
	} );
}

if ( ! Capsule::schema()->hasColumn( 'events', 'tax_rate_value2' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->float('tax_rate_value2', 8, 3)
			->after('tax_rate_id2')
			->nullable();
	} );
}

/*if ( ! Capsule::schema()->hasColumn( 'arrangements', 'tax2' ) ) {
	Capsule::schema()->table( 'arrangements', function( $table ){
		$table->float('tax2', 6, 2)
			->after('tax')
			->unsigned()
			->default(0)
			->nullable();
	} );
}*/

echo 'Success.';
