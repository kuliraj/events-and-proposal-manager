<?php
use Illuminate\Database\Capsule\Manager as Capsule;
if ( ! Capsule::schema()->hasColumn( 'events', 'cover_id' ) ) {
	Capsule::schema()->table( 'events', function( $table ){
		$table->integer( 'cover_id' )
			->default( 0 );
	} );
}
echo 'Migration done.';
exit;