<?php
use Illuminate\Database\Capsule\Manager as Capsule;
use Stemcounter\Tax_Rate;
use Stemcounter\Event;

if (!Capsule::schema()->hasColumn('events', 'tax_rate_id')) {
	Capsule::schema()->table('events', function($table){
		$table->integer('tax_rate_id')
			->unsigned()
			->nullable()
			->after('vendor_id');
	});
}

if (!Capsule::schema()->hasColumn('arrangements', 'tax')) {
	Capsule::schema()->table('arrangements', function($table){
		$table->integer('tax')
			->unsigned()
			->default(1)
			->after('override_cost');
	});
}

$batch_index = isset($_GET['batch_index']) ? abs(intval($_GET['batch_index'])) : 0;
$per_batch = 20;

$users = get_users(array(
	'offset'=>($per_batch * $batch_index),
	'number'=>$per_batch,
	'fields'=>array('ID'),
));
$stats = count_users();
$total_batches = ceil($stats['total_users'] / $per_batch);

if (!empty($users)) {
	foreach ($users as $u) {
		$user_meta = get_user_meta($u->ID, 'wp_s2member_custom_fields');
		$sales_tax = empty($user_meta[0]['sales_tax__']) ? 0 : $user_meta[0]['sales_tax__'];
		
		$tax_rate = Tax_Rate::firstOrNew(array(
			'user_id'=>$u->ID,
			'name'=>'Regular Tax',
		));
		$tax_rate->user_id = $u->ID;
		$tax_rate->name = 'Regular Tax';
		$tax_rate->value = floatval($sales_tax);
		$tax_rate->save();

		Event::where(array(
			'user_id'=>$u->ID,
		))->update(array(
			'tax_rate_id'=>$tax_rate->id,
		));
	}
	$next_batch_url = add_query_arg('batch_index', $batch_index + 1);
	?>
	<p>
		<strong>Please wait ...</strong><br />
		Batch <?php echo ($batch_index + 1); ?> of <?php echo $total_batches; ?>
	</p>
	<script type="text/javascript">
	setTimeout(function(){
		window.location = <?php echo json_encode($next_batch_url); ?>;
	}, 1000);
	</script>
	<?php
} else {
	echo 'Migration done.';
}
exit;