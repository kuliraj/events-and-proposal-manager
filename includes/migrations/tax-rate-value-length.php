<?php

ini_set('memory_limit', '-1');
global $wpdb;

$wpdb->query("ALTER TABLE `{$wpdb->prefix}sc_events` CHANGE `tax_rate_value` `tax_rate_value` DOUBLE(8,3) NOT NULL");

echo '<br/>=======MIGRATION ENDED=======<br/>';
exit;