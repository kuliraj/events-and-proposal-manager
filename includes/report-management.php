<?php
use Illuminate\Http\Request;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;
use Stemcounter\Event;
use Stemcounter\Event_Version;
use Stemcounter\Meta;
use Stemcounter\Tax_Rate;
use Stemcounter\Payment;

function sc_user_report() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$report_type = $request->input( 'report_type' );
	if ( empty( $report_type ) ) {
		throw new Humanreadable_Exception( 'Report type must be selected' );
	}

	$date_format = get_user_meta( get_current_user_id(), '__date_format', true );

	$_from = DateTime::createFromFormat( $date_format, $request->input( 'date_start' ) );
	if ( $_from ) {
		$_from = $_from->getTimestamp();
		$from = date( MYSQL_DATE_FORMAT, $_from );
	} else {
		$_from = $from = false;
	}

	$_to = DateTime::createFromFormat( $date_format, $request->input( 'date_end' ) );
	if ( $_to ) {
		$_to = $_to->getTimestamp();
		$to = date( MYSQL_DATE_FORMAT, $_to );
	} else {
		$_to = $to = false;
	}

	if ( 'payments' == $report_type ) {
		$_payments = Payment::where( 'user_id', '=', get_current_user_id() )
			->whereBetween( 'due_date', array( $from, $to ) )
			->with( array(
				'event' => function( $query ) {
					$query->whereIn( 'state', array( 'booked', 'completed' ) );
				},
				'event.latest_version',
			) )
			->has( 'event.latest_version' )->get();

		if ( ! $_payments->isEmpty() ) {
			$payments_labels = array();
			$payments = array();
			$events_processed = array();

			foreach ( $_payments as $_payment ) {
				if ( ! empty( $events_processed[ $_payment->event_id ] ) ) {
					continue;
				}
				$events_processed[ $_payment->event_id ] = true;
				$latest_version = json_decode( $_payment->event->latest_version->event_data );
				foreach ( $latest_version->payments as $payment ) {
					$payment_date = DateTime::createFromFormat( $date_format, $payment->dueDate );
					$payment_date = $payment_date ? $payment_date->getTimestamp() : 0;
					if ( $payment_date >= $_from && $payment_date <= $_to ) {
						if ( ! empty( $payment->amountPercentage ) && is_null( $payment->amount ) ) {
							$payment->amount = $latest_version->eventCost->total * ( $payment->amountPercentage / 100 );
						}
						$payments[] = array(
							'due_date'		=> $payment->dueDate,
							'due_date_raw'	=> $payment_date,
							'amount'		=> floatval( $payment->amount ),
							'amount_raw'	=> floatval( $payment->amount ),
							'event_name'	=> $latest_version->event->name,
							'event_name_url'=> add_query_arg( 'eid', $latest_version->event->id, sc_get_proposal_2_0_page_url() ),
							'status'		=> $payment->status,
						);
					}
				}
			}

			$payments_labels = array(
				array(
					'label'		=> 'Due Date',
					'key'		=> 'due_date',
					'type'		=> 'string',
				),
				array(
					'label'		=> 'Amount',
					'key'		=> 'amount',
					'type'		=> 'number',
				),
				array(
					'label'		=> 'Event Name',
					'key'		=> 'event_name',
					'type'		=> 'url',
				),
				array(
					'label'		=> 'Status',
					'key'		=> 'status',
					'type'		=> 'string',
				),
			);
			$r->add_payload( 'data_labels', $payments_labels );
			$r->add_payload( 'data', $payments );
		} else {
			$r->add_payload( 'message', 'No records found for the given time period' );
		}
	} else if ( 'events' == $report_type ) {
		$_events = Event::where( 'user_id', '=', get_current_user_id() )
			->whereBetween( 'date', array( $from, $to ) )
			->whereIn( 'state', array( 'booked', 'completed' ) )
			->with('latest_version')
			->has( 'latest_version' )->get();

		if ( ! $_events->isEmpty() ) {
			$events_labels = array();
			$events = array();

			foreach ( $_events as $_event ) {
				$latest_version = json_decode( $_event->latest_version->event_data );
				$event_date = DateTime::createFromFormat( $date_format, $latest_version->event->date );
				$event_date = $event_date ? $event_date->getTimestamp() : 0;
				$data = array(
					'event_name'	=> $latest_version->event->name,
					'event_name_url'=> add_query_arg( 'eid', $latest_version->event->id, sc_get_proposal_2_0_page_url() ),
					'event_date'	=> $latest_version->event->date,
					'event_date_raw'=> $event_date,
					'total'			=> floatval( $latest_version->eventCost->total ),
					'total_raw'		=> floatval( $latest_version->eventCost->total ),
					'tax'			=> floatval( $latest_version->eventCost->tax ),
					'tax_raw'		=> floatval( $latest_version->eventCost->tax ),
					'_data_atts'	=> array( 'eid' => $latest_version->event->id, ),
				);
				$data = apply_filters( 'sc/studio/reports/event/data', $data, $latest_version->event );
				$events[] = $data;
			}

			$events_labels = array(
				array(
					'label'		=> 'Event Name',
					'key'		=> 'event_name',
					'type'		=> 'url',
				),
				array(
					'label'		=> 'Event Date',
					'key'		=> 'event_date',
					'type'		=> 'string',
				),
				array(
					'label'		=> 'Total',
					'key'		=> 'total',
					'type'		=> 'number',
				),
				array(
					'label'		=> 'Taxes',
					'key'		=> 'tax',
					'type'		=> 'number',
				),
			);
			
			$events_labels = apply_filters('sc/studio/reports/event/labels', $events_labels);

			$r->add_payload( 'data_labels', $events_labels );
			$r->add_payload( 'data', $events );
		} else {
			$r->add_payload( 'message', 'No records found for the given time period' );
		}
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_user_report', 'sc_user_report', 10 );