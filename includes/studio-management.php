<?php
use Illuminate\Http\Request;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;
use Stemcounter\Meta;
use Stemcounter\Form;
use Stemcounter\Form_Question;
use Stemcounter\Field_Property;
use Stemcounter\Event_Property;
use Stemcounter\Customer_Property;
use Stemcounter\Form_Submission;
use Stemcounter\Form_Submission_Answer;

function sc_render_studio() {
	ob_start();

	// Add the id's of any new tabs here
	$tabs = array( 'studio-settings', 'studio-integrations', 'studio-workflows', 'studio-tags', 'studio-reports' );

	$active_tab = ! empty( $_GET['tab'] ) && in_array( $_GET['tab'], $tabs ) ? $_GET['tab'] : 'studio-settings';

	/*
	 * Integrations should hook to this filter and add an array with the following structure:
	 * array(
	 * 		'key' => 'integration_id',
	 * 		'label' => 'Label shown to the user',
	 * )
	 */
	$integrations = apply_filters( 'sc/studio/integrations', array() ); ?>
	<div class="content-panel-studio">

		<!-- Nav tabs -->
		<ul class="horizontal-tab-nav nav" role="tablist" id="myTabs">
			<li role="presentation" class="<?php echo 'studio-settings' == $active_tab ? 'active' : ''; ?>">
				<a href="#studio-settings" aria-controls="studio-settings" role="tab" data-toggle="tab">Settings</a>
			</li>
			<li role="presentation" class="<?php echo 'studio-integrations' == $active_tab ? 'active' : ''; ?>">
				<a href="#studio-integrations" aria-controls="studio-integrations" role="tab" data-toggle="tab">Integrations</a>
			</li>
			<li role="presentation" class="<?php echo 'studio-workflows' == $active_tab ? 'active' : ''; ?>">
				<a href="#studio-workflows" aria-controls="studio-workflows" role="tab" data-toggle="tab">Workflows</a>
			</li>
			<?php if ( SCAC::can_access( SCAC::TAGGING ) ) : ?>
				<li role="presentation" class="<?php echo 'studio-tags' == $active_tab ? 'active' : ''; ?>">
					<a href="#studio-tags" aria-controls="studio-tags" role="tab" data-toggle="tab">Tags</a>
				</li>
			<?php endif; ?>
			<li role="presentation" class="<?php echo 'studio-reports' == $active_tab ? 'active' : ''; ?>">
				<a href="#studio-reports" aria-controls="studio-reports" role="tab" data-toggle="tab">Reports</a>
			</li>
		</ul>

		<div class="tab-content">

			<div class="form-panel tab-pane <?php echo 'studio-settings' == $active_tab ? 'active' : ''; ?>" id="studio-settings" role="tabpanel">
				<div class="row">Loading...</div>
			</div>

			<div class="form-panel tab-pane <?php echo 'studio-integrations' == $active_tab ? 'active' : ''; ?>" id="studio-integrations" role="tabpanel">
				<?php foreach ( $integrations as $integration ) : ?>
					<div class="pane">
						<label class="control-label"><?php echo $integration['label'] ?></label>
						<div class="value">
							<?php do_action( 'sc/studio/integration/' . $integration['key'] ); ?>
						</div>
					</div>
				<?php endforeach ?>
			</div>

			<div class="form-panel tab-pane <?php echo 'studio-workflows' == $active_tab ? 'active' : ''; ?>" id="studio-workflows" role="tabpanel">
				<div class="row">
					<div class="col-sm-12">
						<div class="pane">
							<ul>
								<li>
									<h3>When an event is marked as booked, send to my google calendar.</h3>
								</li>
								<li>
									<h3>When a form is submitted, send me an email.</h3>
								</li>
							</ul>
						</div>
					</div>
					<div class="pane-row">
					</div>
				</div>
			</div>

			<?php if ( SCAC::can_access( SCAC::TAGGING ) ) : ?>
				<div class="form-panel tab-pane <?php echo 'studio-tags' == $active_tab ? 'active' : ''; ?>" id="studio-tags" role="tabpanel">
					<div class="row"></div>
				</div>
			<?php endif; ?>

			<div class="form-panel tab-pane <?php echo 'studio-reports' == $active_tab ? 'active' : ''; ?>" id="studio-reports" role="tabpanel"> </div>
		</div> <!-- /tab-content -->
	</div> <!-- content-panel -->
	<?php
	return ob_get_clean();
}
add_filter( 'sc/app/content/studio', 'sc_render_studio' );

function sc_render_studio_js() {
	$user_id = get_current_user_id();
	$logo_id = sc_get_profile_logo_id( $user_id );

	Field_Property::maybe_create_internal_properties();

	if ( $logo_id ) {
		$logo = wp_get_attachment_image_src( $logo_id, 'large', false );
		$logo = array(
			'src' => $logo[0],
			'w' => $logo[1],
			'h' => $logo[2],
		);
	} else {
		$logo = false;
	}

	$date_format = get_user_meta( $user_id, '__date_format', true );
	$js_date_format = sc_get_user_js_date_format( $user_id );

	$settings = array(
		'color' => get_user_meta( $user_id, 'proposal_color', true ),
		'white_label_acc' => get_user_meta( $user_id, 'sc_white_lable_acc', true ),
		'logo' => $logo,
		'style' => get_user_meta( $user_id, 'proposal_style', true ),
		'font' => get_user_meta( $user_id, 'proposal_font', true ),

	);
	$report_settings = array(
		'dateFormat' => $js_date_format,
	); ?>

	<script>
		(function($){
			$(document).ready(function(){
				var settings = <?php echo json_encode( $settings ); ?>;
				settings.node = $('#studio-settings > .row');
				$(document).trigger('stemcounter.action.renderSettingsForm', settings);

				settings = <?php echo json_encode( $report_settings ); ?>;
				settings.node = $('#studio-reports');
				$(document).trigger('stemcounter.action.renderReport', settings);

				<?php if ( SCAC::can_access( SCAC::TAGGING ) ) :
					$tags = get_user_meta( get_current_user_id(), 'sc_user_tags', true );
					$tags = is_array( $tags ) ? $tags : array(); ?>
					$(document).trigger('stemcounter.action.renderEditTagsForm', {
						node: $('#studio-tags > .row'),
						tags: <?php echo json_encode( $tags ); ?>,
					});
					<?php
				endif; ?>
			});
		})(jQuery);
	</script>
	<?php
}
add_action( 'sc/app/footer/studio', 'sc_render_studio_js', 10 );

function sc_edit_tags() {
	$request = Request::capture();
	$r = new Ajax_Response();

	if ( ! SCAC::can_access( SCAC::TAGGING ) ) {
		$r->fail( 'You do not have access to this feature.' );
	}

	$tags = $request->input( 'tags' );

	update_user_meta( get_current_user_id(), 'sc_user_tags', $tags );

	$r->respond( 'Tags saved.' );
}
add_action( 'wp_ajax_sc_edit_tags', 'sc_edit_tags' );

function sc_studio_settings() {
	$request = Request::capture();
	$r = new Ajax_Response();

	$color = $request->input( 'proposalColor' );
	update_user_meta( get_current_user_id(), 'proposal_color', $color );
	$color = get_user_meta( get_current_user_id(), 'proposal_color', true );
	$r->add_payload( 'proposalColor', $color );

	$white_label_acc = get_user_meta( get_current_user_id(), 'sc_white_lable_acc', true );
	$white_label_acc = $request->input( 'choiseLogo' );
	$white_label_acc = $white_label_acc ? 'yes' : '';
	update_user_meta( get_current_user_id(), 'sc_white_lable_acc', $white_label_acc );

	if ( $white_label_acc ) {
		ob_start();
		sc_studio_maybe_render_whitelabel_css();
		$r->add_payload( 'whitelabel_css', ob_get_clean() );
	} else {
		$r->add_payload( 'whitelabel_css', false );
	}

	$old_proposal_style = get_user_meta( get_current_user_id(), 'proposal_style', true );
	$proposal_style = $request->input( 'proposalStyle' );
	update_user_meta( get_current_user_id(), 'proposal_style', $proposal_style, $old_proposal_style );
	$proposal_style = get_user_meta( get_current_user_id(), 'proposal_style', true );
	$r->add_payload( 'proposal_style', $proposal_style );

	$old_proposal_font = get_user_meta( get_current_user_id(), 'proposal_font', true );
	$proposal_font = $request->input( 'proposalFont' );
	update_user_meta( get_current_user_id(), 'proposal_font', $proposal_font, $old_proposal_font );
	$proposal_font = get_user_meta( get_current_user_id(), 'proposal_font', true );
	$r->add_payload( 'proposal_font', $proposal_font );

	$r->respond();
}
add_action( 'wp_ajax_sc_studio_settings', 'sc_studio_settings' );

function sc_studio_maybe_render_whitelabel_css() {

	$user_id = get_current_user_id();
	if ( ! empty( $GLOBALS['sc_current_event'] ) ) {
		$user_id = $GLOBALS['sc_current_event']->user_id;
	}

	if ( ! get_user_meta( $user_id, 'sc_white_lable_acc', true ) ) {
		return;
	}

	$logo_id = sc_get_profile_logo_id( $user_id );
	$new_logo = wp_get_attachment_image_src( $logo_id, 'medium', false );
	$new_logo = $new_logo ? $new_logo : '';
	$new_logo_height = $new_logo ? $new_logo[2] / ( $new_logo[1] / 193 ) : '';
	$color = get_user_meta( $user_id, 'proposal_color', true );
	$dark = sc_adjust_brightness( $color, -50 );

	if ( $new_logo ) {
		$new_logo_css = '.sidebar-top {
			padding: 5px 5px 5px 10px;
		}
		#sidebar-logo {
			height: ' . absint( $new_logo_height ) . 'px;
			background-image: url("' . esc_url( $new_logo[0] ) . '");
			background-repeat: no-repeat;
			background-size: contain;
			background-position: left bottom;
		}';
	} else {
		$new_logo_css = '';
	}

	if ( $color ) {
		$color_css = '.sc-primary-color,
		ul.horizontal-tab-nav li.active a,
		.proposal-container .proposal-section .arrangement-items .arrangement-items-edit,
		.proposal-container .proposal-section .arrangement-items .arrangement-items-edit a,
		#edit_deliveries .delivery-setting label.active,
		#edit_discounts .discount-setting label.active,
		.stemcounter-new-design .discount-setting .control-label .control-label.active,
		.page-template-template-profile #profile-markup .control-label.active,
		.labor-settings .labor-type.active,
		.mepr-active-nav-tab a,
		.new-proposal-container .proposal-toolbar .event-name,
		.proposal-container .proposal-settings .section-heading .expandable-heading,
		.proposal-container .proposal-toolbar .proposal-toolbar-dropdown .proposal-dropdown-btn.active,
		.proposal-container .event-details-section .venue-type .delete-venue,
		.proposal-container .event-details-section .venue-details .add-venue,
		.proposal-container .total-costs .modal-tax-rate,
		.proposal-container .total-costs .modal-tax-rate span,
		.proposal-container .payments-module .single-payment .send-receipt,
		.proposal-container .payments-module .single-payment .send-to-calendar,
		.new-modal .show-all-arrangement-items-button,
		.recipes-table .recipe-name,
		.mandatory::after,
		.proposal-container .proposal-state-edit .arrangement-item-tr .arr-item-attachment,
		ul.sidebar-menu li a.active,
		ul.sidebar-menu li.current-menu-item a,
		.modal .public-link-wrapper .public-link-label:hover i,
		.ajax-modal-loader,
		#modal-arrangement-settings .no-tax-label input:checked + span:before,
		#modal-arrangement-settings .include-on-proposal-label input:checked + span:before,
		#modal-arrangement-settings .no-tax-label input:checked + span + span,
		#modal-arrangement-settings .include-on-proposal-label input:checked + span + span,
		.edit-item-modal .edit-item-form .var-pointer,
		table.react-datatable thead tr th a,
		.proposal-container .printing-for-client:hover {
			color: ' . $color . ';
		}
		a.sc-primary-color:hover,
		a.sc-primary-color:focus,
		a.sc-primary-color:active,
		.edit-item-modal .item-variation .remove-variation:hover:after,
		.new-proposal-container .proposal-toolbar .events-link:hover,
		.proposal-container .proposal-header .proposal-cover-dropdown:hover .proposal-cover-image,
		.proposal-container .proposal-toolbar .proposal-toolbar-dropdown .dropdown-content a:hover i,
		.edit-item-modal .add-item-variation:hover:after,
		#modal-arrangement-settings .delete-arrangement:hover {
			color: ' . $dark . ';
		}

		.modal-header, .stemcounter-new-design .modal-header,
		.modal-header,
		input.styled-radio:checked + label,
		#event-proposal .proposal-toolbar .finalize-wrap a,
		.new-proposal-container .proposal-toolbar .edit-toggle,
		.new-proposal-container .button-toggle,
		::selection,
		.new-modal .modal-body .save-user-recipe,
		body.proposal-style-2 {
			background: ' . $color . ';
		}

		ul.sidebar-menu li.show-to-free-users,
		ul.sidebar-menu li.show-to-free-users.current-menu-item,
		.pipeline-part.pipeline-part-leads .pipeline-label,
		.btn-primary,
		.stemcounter-new-design .modal .btn-submit,
		.stemcounter-new-design .modal .button-submit,
		.stemcounter-new-design .modal .btn-primary,
		.loading-bar:before,
		.stemcounter-new-design .modal .btn-submit:hover,
		.stemcounter-new-design .modal .button-submit:hover,
		.stemcounter-new-design .modal .btn-primary:hover {
			background-color: ' . $color . ';
		}

		.events-tags .tag:hover,
		.events-tags .tag:focus,
		.events-tags .tag.filter {
			border-color: ' . $color . ';
			color: ' . $color . ';
		}

		.pagination>.active>a,
		.pagination>.active>span {
			background-color: ' . $color . ';
			border-color: ' . $color . ';
		}

		a.cta-link {
			color: ' . $color . ';
			border-bottom: 1px solid ' . $color . ';
		}
		.mepr-account-change-password a, .mepr-account-actions a {
			color: ' . $color . ';
			border-bottom: 1px solid ' . $color . ';
		}

		.modal-footer .btn:hover,
		.modal-footer .btn:focus {
			border: 1px solid ' . $color . ';
		}

		.modal-footer .btn-primary:hover,
		.stemcounter-new-design .modal-footer .btn-primary:hover,
		.modal-footer .btn-primary:focus,
		.stemcounter-new-design .modal-footer .btn-primary:focus,
		.btn-primary:hover,
		.pipeline-part.pipeline-part-booked .pipeline-label,
		ul.sidebar-menu li.show-to-free-users a:hover,
		ul.sidebar-menu li a:hover,
		ul.sidebar-menu li a:focus,
		.pagination>.active>a:focus,
		.pagination>.active>a:hover,
		.pagination>.active>span:focus,
		.pagination>.active>span:hover,
		ul.sidebar-menu li.show-to-free-users.show-to-free-users:hover,
		ul.sidebar-menu li.show-to-free-users.current-menu-item:hover  {
			background-color: ' . $dark . ';
			color: #ffffff;
		}
		#sliding-panel .panel-toggler {
			background-color: ' . $color . ';
			color: #ffffff;
		}
		.builder-form textarea:focus, .builder-form textarea.form-control:focus, .builder-form input.form-control:focus, .builder-form input[type=text]:focus, .builder-form input[type=number]:focus,  #modal-property textarea:focus,  #modal-property textarea.form-control:focus,  #modal-property input.form-control:focus,  #modal-property input[type=text]:focus,  #modal-property input[type=number]:focus, .builder-property  input.form-control:focus, .builder-property input[type=text]:focus, .builder-property input[type=number]:focus {
			-webkit-box-shadow: inset 0 -2px 0 ' . $color . ';
			box-shadow: inset 0 -2px 0 ' . $color . ';
		}
		table#customersTable tr.active {
			color: ' . $color . ';
		}';
	}

	echo '<style id="studio-whitelabel-css">
		' . $new_logo_css . '
		' . $color_css . '
	</style>';
}
add_action( 'wp_head', 'sc_studio_maybe_render_whitelabel_css', 999 );

function sc_studio_save_customer_source( $customer, $request = false ) {
	if ( $request && SCAC::can_access( SCAC::STUDIO ) ) {
		$customer_source = $request->input('customer_source');
		$customer_source_meta = Meta::firstOrNew(array(
			'type' => 'customer',
			'type_id' => $customer->id,
			'meta_key' => 'customer_source',
		));
		$customer_source_meta->meta_value = $customer_source;
		$customer_source_meta->save();
	}
}
add_action( 'sc/customer/save', 'sc_studio_save_customer_source', 10, 2 );

function sc_studio_update_customer_source( $settings, $customer ) {
	if ( SCAC::can_access( SCAC::STUDIO ) ) {
		$customer_source = Meta::where(array(
			'type' => 'customer',
			'type_id' => $customer->id,
			'meta_key' => 'customer_source',
		))->first();

		$settings['customerSource'] = $customer_source ? $customer_source->meta_value : '';
	}

	return $settings;
}
add_action( 'sc/customer/edit/settings', 'sc_studio_update_customer_source', 10, 2 );

function sc_render_color_for_tags() {
	if ( ! SCAC::can_access( SCAC::TAGGING ) ) {
		return;
	}

	$tags = get_user_meta( get_current_user_id(), 'sc_user_tags', true );
	if ( ! $tags || ! is_array( $tags ) ) {
		return;
	}
	echo '<style>'; ?>
		<?php foreach ( $tags as $tag ) :
			if ( empty( $tag['color'] ) ) {
				continue;
			}
			$dark_color = sc_adjust_brightness( $tag['color'], -50 );
			$class = sc_convert_tag_name_to_class( $tag['name'] );
			$color_luma = sc_get_hex_luma( $tag['color'] );
			$dark_color_luma = sc_get_hex_luma( $dark_color );
			$text_color = 120 > $color_luma ? 'white' : 'black';
			$text_color_hover = 120 > $dark_color_luma ? 'white' : 'black'; ?>
			table#eventsTable a.tag.<?php echo $class; ?>,
			.events-tags a.tag[data-tag-class="<?php echo $class; ?>"] {
				background-color: <?php echo $tag['color']; ?>;
				border-color: <?php echo $tag['color']; ?>;
				color: <?php echo $text_color; ?>;
			}
			table#eventsTable a.tag.<?php echo $class; ?>:hover,
			table#eventsTable a.tag.<?php echo $class; ?>:focus,
			.events-tags a.tag[data-tag-class="<?php echo $class; ?>"]:hover,
			.events-tags a.tag[data-tag-class="<?php echo $class; ?>"]:focus,
			.events-tags a.tag[data-tag-class="<?php echo $class; ?>"].filter {
				background-color: <?php echo $dark_color; ?>;
				border-color: <?php echo $dark_color; ?>;
				color: <?php echo $text_color_hover; ?>;
			}
		<?php
		endforeach; ?>
	<?php
	echo '</style>';
}
add_action( 'sc/app/head/events', 'sc_render_color_for_tags', 10 );

function sc_convert_tag_name_to_class( $tag ) {
	return 'tag-' . sanitize_title_with_dashes( $tag );
}

function sc_studio_filter_body_class( $classes ) {
	if ( is_page_template( 'template-proposal-2.0.php' ) ) {
		$user_id = get_current_user_id();
		if ( ! empty( $GLOBALS['sc_current_event'] ) ) {
			$user_id = $GLOBALS['sc_current_event']->user_id;
		}

		if ( SCAC::can_access( SCAC::STUDIO, true, $user_id ) ) {
			$proposal_style = get_user_meta( $user_id, 'proposal_style', true );
			if ( $proposal_style ) {
				$classes[] = 'event-new-design proposal-style-' . $proposal_style;
			}
		}
	}

	return $classes;

}
add_filter( 'body_class', 'sc_studio_filter_body_class', 10 );

function sc_studio_enqueue_scripts() {
	if ( is_page_template( 'template-proposal-2.0.php' ) ) {
		$user_id = get_current_user_id();
		if ( ! empty( $GLOBALS['sc_current_event'] ) ) {
			$user_id = $GLOBALS['sc_current_event']->user_id;
		}

		if ( SCAC::can_access( SCAC::STUDIO, true, $user_id ) ) {
			$proposal_style = get_user_meta( $user_id, 'proposal_style', true );
			if ( $proposal_style ) {
				wp_enqueue_style( 'proposal-style-' . $proposal_style );
			}
			$proposal_font = get_user_meta( $user_id, 'proposal_font', true );
			remove_action('wp_enqueue_scripts', 'FLBuilderFonts::combine_google_fonts', 99);
			if ( $proposal_font ) {
				wp_enqueue_style( 'font-' . $proposal_font );
			}
		}
	}
}
add_action( 'wp_enqueue_scripts', 'sc_studio_enqueue_scripts', 20 );

function sc_render_event_mangement_style( $event ) {
	if ( SCAC::can_access( SCAC::STUDIO, true, $event->user_id ) ) :
		$proposalColor = get_user_meta( $event->user_id, 'proposal_color', true );
		$dark = sc_adjust_brightness( $proposalColor, -50 );
		if ( $proposalColor && '#' != $proposalColor ) : ?>
			.proposal-container .event-details-section .customer-info a {
				color: <?php echo $proposalColor; ?>;
			}

			body.stemcounter-new-design .btn-primary {
				background-color: <?php echo $proposalColor; ?>;
			}

			.main-content .proposal-container .proposal-contract button.sign-button {
				background: <?php echo $proposalColor; ?>;
			}

			body.stemcounter-new-design .btn-primary:hover,
			.main-content .proposal-container .proposal-contract button.sign-button:hover {
				background: <?php echo $dark; ?>;
			}
			div.Select .Select-menu-outer .VirtualizedSelectOption.user-lib-<?php echo $event->user_id; ?>:before {
				color: <?php echo $proposalColor ? $proposalColor : '#f37a76'; ?>;
		<?php endif;
	endif;
}
add_action( 'sc/event/render_styles', 'sc_render_event_mangement_style', 10 );

function sc_property_edit() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$user_id = get_current_user_id();
	$id = intval( $request->input('id') );
	$prop = $request->input('property');

	if ( ! empty( $prop ) ) {
		if ( 0 < $id ) {
			try {
				$property = Field_Property::where( array(
					'id' => $id,
				) )->first();
			} catch ( Exception $e ){
				throw new Humanreadable_Exception( 'Failed to edit property. Please contact support.' );
			}
			$r->add_payload( 'message', 'Property was edited successfully' );
		} else {
			$property = new Field_Property();

			$r->add_payload( 'message', 'Property was created successfully' );
		}

		if ( empty( $prop['title'] ) || empty( $prop['property_type'] ) ) {
			throw new Humanreadable_Exception('Please fill in the required fields.');
		}

		$property->user_id = $user_id;
		$property->label = $prop['title'];
		$property->field_type = $prop['type'];
		$property->property_type = $prop['property_type'];
		$property->placeholder_tag = $prop['placeholder_tag'];
		$settings = array(
			'default' 		=> $prop['default'],
			'description'	=> $prop['description'],
			'format' 		=> $prop['format'],
		);

		if ( ! empty( $prop['aopt'] ) ) {
			$settings['aopt'] = $prop['aopt'];
		}
		$property->settings = $settings;
		$property->save();

		$r->add_payload( 'property_id', $property->id );
	} else {
		throw new Humanreadable_Exception('Something went wrong with your request.');
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_property_edit', 'sc_property_edit' );

function sc_property_delete() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$field_property = Field_Property::where( array(
			'id'		=> intval( $request->input('id') ),
			'user_id'	=> get_current_user_id(),
		) )->first();

		$field_property->deleted = 1;
		$field_property->save();

		$r->add_payload( 'message', 'Property field was deleted successfully' );
	} catch (Exception $e) {
		$r->fail('Something went wrong with your request.');
	}
	$r->respond();
}
add_action( 'wp_ajax_sc_property_delete', 'sc_property_delete' );

function sc_form_edit() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$schema = $request->input('schema');
	if( ! empty( $schema ) ) {
		$form_id = intval( $schema['id'] );
		try {
			if ( 0 < $form_id ) {
				try {
					$form = Form::where( array(
						'id' => $form_id,
					) )->with( 'questions' )->first();
				} catch ( Exception $e ){
					throw new Humanreadable_Exception( 'Failed to edit form. Please contact support.' );
				}
			} else {
				$form = new Form();
			}
			$form->name = $schema['title'];
			$form->confirmation_msg = ! empty( $schema['confirmation_msg'] ) ? $schema['confirmation_msg'] : '';
			$form->user_id = get_current_user_id();
			$form->save();

			$schema['id'] = $form->id;
			$schema['url'] = $form->get_form_url();

			foreach ( $form->questions as $_question ) {
				$question_exists = wp_list_filter( $schema['properties'], array( 'id' => $_question->id ) );
				// Delete the question if it was not sent in the request
				if ( empty( $question_exists ) ) {
					$_question->delete();
				}
			}

			foreach ( $schema['properties'] as $key => $property ) {
				$question = Form_Question::firstOrNew( array(
					'id'			=> intval( $property['id'] ),
					'form_id' => $form->id,
				) );
				$question->form_id = $form->id;
				$question->property_id = intval( $property['property_id'] );
				$question->label = $property['title'];
				$question->type = $property['type'];
				$question->required = intval( $property['required'] );
				$question->order = absint( $property['order'] );
				$question->save();
				$schema['properties'][ $key ]['id'] = $question->id;
			}
			$r->add_payload( 'schema', $schema );
			$r->add_payload( 'message', 'Form was created successfully' );
		} catch (Exception $e) {
			$r->fail('Something went wrong with your request.');
		}
	}
	$r->respond();
}
add_action( 'wp_ajax_sc/form/edit', 'sc_form_edit' );

function sc_form_delete() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		Form::where( array(
			'id'			=> intval($request->input('id') ),
			'user_id'	=> get_current_user_id(),
		) )->first()->delete();

		$r->add_payload( 'message', 'Form was deleted successfully' );
	} catch (Exception $e) {
		$r->fail('Something went wrong with your request.');
	}
	$r->respond();
}
add_action( 'wp_ajax_sc_form_delete', 'sc_form_delete' );

function sc_register_forms_rewrite_rules() {
	// Note how we're setting "&error=404" - this will automatically fall back to the default 404
	// page if the requested form is invalid
	add_rewrite_rule( '^form/[^/]*/([^/]+)/?$', 'index.php?form-uid=$matches[1]&error=404', 'top' );

	add_rewrite_tag( '%form-uid%', '([^/]+)' );
}
add_action( 'init', 'sc_register_forms_rewrite_rules', 10 );

function sc_maybe_register_studio_user_actions() {
	if ( ! is_user_logged_in() ) {
		return;
	}

	if ( \SCAC::can_access( \SCAC::STUDIO ) ) {
		add_filter( 'nav_menu_item_args', 'sc_maybe_show_form_submissions_count', 10, 2 );

		add_action( 'sc/events/load/form_submissions', 'sc_render_form_submissions' );
	}

	if ( \SCAC::can_access( \SCAC::TAGGING ) ) {

	}
}
add_action( 'init', 'sc_maybe_register_studio_user_actions', 10 );

function sc_maybe_intercept_form_render_request( $query ) {
	$form_uid = ! empty( $query->query_vars['form-uid'] ) ? $query->query_vars['form-uid'] : false;

	if ( empty( $form_uid ) ) {
		return;
	}

	$form_uid = sc_decrypted_string( $form_uid );

	$form_uid = explode( '_', $form_uid );
	if ( 2 > count( $form_uid ) ) {
		return;
	}

	$uid = absint( $form_uid[0] );
	$form_id = absint( $form_uid[1] );

	// Form author doesn't have studio access at the moment - bummer :(
	if ( ! \SCAC::can_access( \SCAC::STUDIO, true, $uid ) ) {
		return;
	}

	$form = Form::where( array(
		'id' => $form_id,
	) )->with( 'questions', 'questions.property' )->first();

	if ( empty( $form ) ) {
		return;
	}

	unset( $query->query_vars['error'] );
	$query->query_vars['_sc_form'] = $form;
}
add_action( 'parse_request', 'sc_maybe_intercept_form_render_request', 10 );

function sc_maybe_render_form() {
	$form_uid = get_query_var( 'form-uid' );
	$form = get_query_var( '_sc_form' );

	if ( empty( $form_uid ) || is_404() || empty( $form ) ) {
		return;
	}

	$GLOBALS['sc_current_event'] = $form;
	$GLOBALS['user_id'] = $form->user_id;

	add_filter( 'wp_title', function( $title ) use ( $form ) {
		return $form->name;
	}, 10000 );

	$logo_id = sc_get_profile_logo_id( $form->user_id );
	$logo = wp_get_attachment_image_src( $logo_id, 'medium' );
	$company_name = get_user_meta( $form->user_id, 'company_name', true );

	$args = array(
		'form'			=> $form,
		'user'			=> get_userdata( $form->user_id ),
		'logo'			=> $logo ? $logo[0] : false,
		'company'		=> $company_name,
		'dateFormat'	=> sc_get_user_js_date_format( $form->user_id ),
	);

	remove_action( 'wp_head', 'sc_get_header_og_image', 0 );
	remove_action( 'wp_head', 'sc_render_drift_js', 9999 );
	add_action( 'wp_head', 'sc_public_og_image', 0 );
	add_filter( 'body_class', 'sc_filter_view_form_body_class' );

	sc_render_template( 'templates/view-form', $args, false );
	exit;
}
add_action( 'template_redirect', 'sc_maybe_render_form', 10 );

function sc_filter_view_form_body_class( $classes ) {
	$classes[] = 'view-form';
	if ( -1 != array_search( 'blog', $classes ) ) {
		array_splice( $classes, array_search( 'blog', $classes ), 1 );
	}

	return $classes;
}

function sc_handle_new_form_submission_ajax() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$form_id = $request->input( 'form_id' );

	if ( empty( $form_id ) ) {
		$r->fail( 'Something went wrong. Please reload the page.' );
	}

	$form = Form::where( array(
		'id' => $form_id,
	) )->with( 'questions', 'questions.property' )->first();

	if ( empty( $form ) || ! \SCAC::can_access( \SCAC::STUDIO, true, $form->user_id ) ) {
		$r->fail( 'Something went wrong. Please reload the page.' );
	}

	$answers = $request->input( 'questions' );

	$result = $form->validate_create_submission( $answers );
	if ( ! is_wp_error( $result ) ) {
		$r->respond( $form->confirmation_msg );
	} else {
		$r->fail( 'Please correct the following error before proceeding: ' . $result->get_error_message() );
	}
}
add_action( 'wp_ajax_sc/form/submission/new', 'sc_handle_new_form_submission_ajax', 10 );
add_action( 'wp_ajax_nopriv_sc/form/submission/new', 'sc_handle_new_form_submission_ajax', 10 );

function sc_maybe_show_form_submissions_count( $args, $item ) {
	if ( ! empty( $args->theme_location ) && 'main-menu' == $args->theme_location ) {
		if ( ! empty( $item->classes ) && is_array( $item->classes ) && in_array( 'sc-forms', $item->classes ) ) {
			$not_seen_submissions = Form_Submission::where( array(
				'user_id' => get_current_user_id(),
				'seen' => 0,
				'archived' => 0,
			) )->count();

			if ( 0 < absint( $not_seen_submissions ) ) {
				$args->link_after = $args->link_after . '<span class="label label-info label-pill">' . $not_seen_submissions . '</span>';
			}
		} else {
			$args->link_after = '<span>' == $args->link_before ? '</span>' : '';
		}
	}

	return $args;
}

function sc_render_form_submissions() {
	$submissions = Form_Submission::where( array(
		'user_id' => get_current_user_id(),
		'archived' => ! empty( $_GET['archived'] ) ? 1 : 0,
	) )->with( 'form' )->orderBy( 'seen', 'ASC' )->orderBy( 'created_at', 'DESC' )->get();

	$date_format = sc_get_user_datetime_format();

	$args = array(
		'submissions'	=> $submissions,
		'date_format'	=> $date_format,
		'is_archived' => ! empty( $_GET['archived'] ),
	);

	sc_render_template( 'templates/view-submissions', $args, false );
}

function sc_get_form_submission_ajax() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$submission_id = $request->input( 'submission_id' );

	$submission = Form_Submission::where( array(
		'user_id'	=> get_current_user_id(),
		'id'		=> $submission_id,
	) )->with( 'answers', 'answers.question', 'answers.property' )->first();

	if ( ! $submission ) {
		$r->fail( 'There was no submission with this ID.' );
	}

	if ( ! $submission->seen ) {
		$submission->seen = 1;
		$submission->save();
	}

	$r->add_payload( 'submission', $submission->toArray() );
	$r->respond();
}
add_action( 'wp_ajax_sc/form/submission/get', 'sc_get_form_submission_ajax', 10 );

function sc_maybe_add_form_submissions_to_event_form( $args, $event ) {
	if ( ! SCAC::can_access( SCAC::STUDIO ) || ( $event && ! $event->duplicate ) ) {
		return $args;
	}

	$submissions = Form_Submission::where( array(
		'user_id' => get_current_user_id(),
		'archived' => 0,
	) )->with( 'form', 'customer' )->orderBy( 'created_at', 'DESC' )->get();

	$args['submissions'] = array();
	foreach ( $submissions as $submission ) {
		$args['submissions'][] = $submission->prepare_for_js();
	}

	return $args;
}
add_filter( 'sc/event/form/js_params', 'sc_maybe_add_form_submissions_to_event_form', 10, 2 );

function sc_maybe_add_event_properties_from_submission( $event ) {
	if ( ! SCAC::can_access( SCAC::STUDIO ) ) {
		return;
	}

	$request = Request::capture();
	$submission_id = $request->input( 'event_fields.from_submission' );
	if ( empty( $submission_id ) ) {
		return;
	}

	$submission = Form_Submission::where( array(
		'user_id'	=> $event->user_id,
		'id'		=> $submission_id,
	) )->with( 'answers', 'answers.property' )->first();

	if ( ! $submission ) {
		return;
	}

	// Create the event properties - excluding internal ones
	foreach ( $submission->answers->where( 'property.property_type', 'event' )->where( 'property.internal_field', null ) as $answer ) {
		Event_Property::create( array(
			'event_id'		=> $event->id,
			'property_id'	=> $answer->property->id,
			'value'			=> $answer->answer,
		) );
	}

	$submission->event_id = $event->id;
	$submission->archived = 1;
	$submission->save();
}
add_action( 'sc/event/save', 'sc_maybe_add_event_properties_from_submission', 5 );

function sc_maybe_add_event_properties_to_react( $react_data, $event, $user_id, $version ) {
	if ( SCAC::can_access( SCAC::STUDIO ) && empty( $version ) ) {
		$event_properties = Field_Property::where( array(
			'property_type'		=> 'event',
			'internal_field'	=> null,
			'user_id'			=> $user_id,
		) )->with( array(
			'event_properties' => function( $query ) use ( $event ) {
				$query->where( 'event_id', $event->id );
			}
		) )->get();
		$event_properties = Field_Property::prepare_collection_for_js( $event_properties, 'event_properties' );
		$react_data['event']['properties'] = $event_properties;

		foreach ( $react_data['event']['properties'] as $key => $property ) {
			if ( intval( $property['deleted'] ) && empty( $property['values'] ) ) {
				unset( $react_data['event']['properties'][ $key ] );
			}
		}

		$react_data['event']['properties'] = array_values( $react_data['event']['properties'] );
	}

	return $react_data;
}
add_filter( 'sc/react/data/proposal', 'sc_maybe_add_event_properties_to_react', 10, 4 );

function sc_maybe_save_event_properties_in_version( $version, $event ) {
	if ( SCAC::can_access( SCAC::STUDIO ) ) {
		$event_properties = Field_Property::where( array(
			'property_type' => 'event',
			'internal_field' => null,
		) )->with( array(
			'event_properties' => function( $query ) use ( $event ) {
				$query->where( 'event_id', $event->id );
			}
		) )->get();

		$event_properties = Field_Property::prepare_collection_for_js( $event_properties, 'event_properties' );
		foreach ( $event_properties as $i => $property ) {
			foreach ( $property['values'] as $ii => $val ) {
				// We don't need to save all of the other data
				$property['values'][ $ii ] = array(
					'value' => $val['value'],
				);
			}
			$event_properties[ $i ]['values'] = $property['values'];
		}

		$version['event']['properties'] = $event_properties;
	}

	return $version;
}
// add_filter( 'sc/event/version/create/data', 'sc_maybe_save_event_properties_in_version', 10, 2 );

function sc_send_email_on_new_form_submission( $submission, $form ) {
	$to_email = $submission->get_notification_email();

	if ( ! $to_email ) {
		return;
	}

	$submission_answers = collect();

	foreach ( $submission->answers as $answer ) {
		if ( 0 >= $submission_answers->where( 'question_id', $answer->question_id )->count() ) {
			$answers = $submission->answers->where( 'question_id', $answer->question_id )->pluck( 'answer' );
			$answer->answer = $answers;
			$submission_answers->push( $answer );
		}
	}

	$submission->answers = $submission_answers;

	$args = array(
		'page_title'	=> sprintf( '%s - New Submission', $form->name ),
		'submission'	=> $submission,
		'form'			=> $form,
	);

	$headers = array();
	$headers[] = 'From: "StemCounter.com" <noreply@stemcounter.com>';
	$headers[] = 'List-Unsubscribe: <' . home_url( '/app/wp-admin/admin-ajax.php?action=sc_email_unsubscribe' ) . '>';
	$to = sc_prepare_email_recipients_for_mandrill( $to_email );
	$subject = sprintf( 'New Submission from your form - %s', $form->name );

	$body = '';
	$body .= sc_render_template( 'templates/html-mail-header', $args, true );
	$body .= sc_render_template( 'templates/mail-view-submission', $args, true );
	$body .= sc_render_template( 'templates/html-footer', $args, true );

	wp_mail( $to, $subject, $body, $headers );
}
add_action( 'sc/form/submission/created', 'sc_send_email_on_new_form_submission', 50, 2 );

function sc_render_studio_forms() {
	ob_start();

	// Add the id's of any new tabs here
	$tabs = array( 'submissions', 'forms' );

	$active_tab = ! empty( $_GET['tab'] ) && in_array( $_GET['tab'], $tabs ) ? $_GET['tab'] : 'submissions'; ?>
	<div class="content-panel-studio">

		<!-- Nav tabs -->
		<ul class="horizontal-tab-nav nav" role="tablist" id="myTabs">
			<li role="presentation" class="<?php echo 'submissions' == $active_tab ? 'active' : ''; ?>">
				<a href="#tab-submissions" aria-controls="tab-submissions" role="tab" data-toggle="tab">Submissions <?php echo ! empty( $_GET['archived'] ) ? '(archived)' : ''; ?></a>
			</li>
			<li role="presentation" class="<?php echo 'forms' == $active_tab ? 'active' : ''; ?>">
				<a href="#tab-forms" aria-controls="tab-forms" role="tab" data-toggle="tab">Forms</a>
			</li>
		</ul>

		<div class="tab-content">

			<div class="form-panel tab-pane <?php echo 'submissions' == $active_tab ? 'active' : ''; ?>" id="tab-submissions" role="tabpanel">
				<div class="row">
					<?php sc_render_form_submissions(); ?>
				</div>
			</div>

			<div class="form-panel tab-pane <?php echo 'tab-forms' == $active_tab ? 'active' : ''; ?>" id="tab-forms" role="tabpanel"></div>

		</div> <!-- /tab-content -->
	</div> <!-- content-panel -->
	<?php

	return ob_get_clean();
}
add_filter( 'sc/app/content/forms', 'sc_render_studio_forms' );

function sc_render_forms_js() {
	$user_id = get_current_user_id();

	Field_Property::maybe_create_internal_properties();

	$date_format = get_user_meta( $user_id, '__date_format', true );
	$js_date_format = sc_get_user_js_date_format( $user_id );

	$user_properties = Field_Property::where( array(
		'user_id' => $user_id,
		'deleted' => 0
	) )->orderBy('label', 'ASC')->get();
	$_properties = array();
	foreach ( $user_properties as $property ) {
		$prop_settings = $property->settings;
		$_properties[] = array(
			'id'				=> $property->id,
			'default'			=> $prop_settings->default,
			'title'				=> $property->label,
			'placeholder_tag'	=> $property->placeholder_tag,
			'description'		=> $prop_settings->description,
			'format'			=> $prop_settings->format,
			'type'				=> $property->field_type,
			'property_type'		=> $property->property_type,
			'internal_field'	=> $property->internal_field,
		);
		if ( ! empty( $prop_settings->aopt ) ) {
			$_properties[ ( count($_properties) - 1 ) ]['aopt'] = $prop_settings->aopt;
		}
	}

	$user_forms = Form::where( array(
		'user_id' => $user_id,
	) )->with('questions', 'questions.property')->orderBy('name', 'ASC')->get();
	$_user_forms = array();
	foreach ( $user_forms as $form ) {
		$_user_forms[] = $form->prep_for_js();
	} ?>
	<script>
		(function($){
			$(document).ready(function(){
				$(document).trigger('stemcounter.action.renderICFController', {
					dateFormat: <?php echo json_encode( $js_date_format ); ?>,
					userProperties: <?php echo json_encode( $_properties ); ?>,
					userForms: <?php echo json_encode( $_user_forms ); ?>,
					node: $('#tab-forms'),
				});
			});
		})(jQuery);
	</script>
	<?php
}
add_action( 'sc/app/footer/forms', 'sc_render_forms_js', 10 );

function sc_archive_submission_ajax() {
	$r = new Ajax_Response();

	$submission_id = ! empty( $_POST['submission'] ) ? absint( $_POST['submission'] ) : false;
	if ( ! $submission_id ) {
		$r->fail( 'Something is not right. Please refresh the page and try again.' );
	}

	$submission = Form_Submission::where( array(
		'user_id'	=> get_current_user_id(),
		'id'		=> $submission_id,
	) )->first();

	if ( ! $submission ) {
		$r->fail( 'Something is not right. Please refresh the page and try again.' );
	}

	$submission->archived = 1;
	$submission->save();

	$r->respond();
}
add_action( 'wp_ajax_sc/form/submission/archive', 'sc_archive_submission_ajax', 10 );

function sc_unarchive_submission_ajax() {
	$r = new Ajax_Response();
	$request = Request::capture();
	$submission_id = intval( $request->input( 'submission' ) );

	if ( ! $submission_id ) {
		$r->fail( 'Something went wrong. Please refresh the page and try again.' );
	}

	try {
		$submission = Form_Submission::where( array(
			'user_id'	=> get_current_user_id(),
			'id'		=> $submission_id,
		) )->first();

		$submission->archived = 0;
		$submission->save();
	} catch (Exception $e) {
		$r->fail( 'Something went wrong. Please refresh the page and try again.' );
	}

	$r->respond();
}
add_action( 'wp_ajax_sc/form/submission/unarchive', 'sc_unarchive_submission_ajax' );

function sc_maybe_add_customer_properties_to_react( $react_data, $customer ) {
	if ( SCAC::can_access( SCAC::STUDIO ) ) {
		if ( ! $react_data && ! $customer ) {
			$customer_properties = Field_Property::where( array(
				'property_type'		=> 'customer',
				'internal_field'	=> null,
				'user_id'			=> get_current_user_id(),
				'deleted'			=> 0,
			) )->with( array(
				'customer_properties' => function( $query ) {
					$query->where( 'customer_id', '=', null );
				}
			) )->get();
			return Field_Property::prepare_collection_for_js( $customer_properties, 'customer_properties' );
		} else {
			$customer_properties = Field_Property::where( array(
				'property_type'		=> 'customer',
				'internal_field'	=> null,
				'user_id'			=> get_current_user_id(),
			) )->with( array(
				'customer_properties' => function( $query ) use ( $customer ) {
					$query->where( 'customer_id', $customer->id );
				}
			) )->get();

			$customer_properties = Field_Property::prepare_collection_for_js( $customer_properties, 'customer_properties' );
			$react_data['properties'] = $customer_properties;
			
			foreach ( $react_data['properties'] as $key => $property ) {
				if ( intval( $property['deleted'] ) && empty( $property['values'] ) ) {
					unset( $react_data['properties'][ $key ] );
				}
			}

			$react_data['properties'] = array_values( $react_data['properties'] );
		}
	}

	return $react_data;
}
add_filter( 'sc/react/data/customer', 'sc_maybe_add_customer_properties_to_react', 10, 4 );
