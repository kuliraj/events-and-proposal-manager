<?php

function stemcounter_registration_shortcode( $atts, $content = null ) {
	extract( shortcode_atts(array(
		'memberships' => '',
		'default' => '',
	), $atts, 'stemcounter-registration' /* Shortcode name for attribute filtering */ ) );
	
	$mepr_options = MeprOptions::fetch();
	$user = new MeprUser( get_current_user_id() );
	$gateway = $mepr_options->payment_method( 'nyu2r7-5rg' );
	$mepr_current_user = wp_get_current_user();

	if ( ! $memberships ) {
		$memberships = get_posts( array(
			'numberposts' => -1,
			'post_type'   => 'memberpressproduct',
			'post_status' => 'publish'
		) );
	} else {
		$memberships = explode( ',', $memberships );
		$memberships = get_posts( array(
			'post__in'    => $memberships,
			'post_type'   => 'memberpressproduct',
			'post_status' => 'publish',
			'orderby'     => 'post__in',
			'order'       => 'ASC',
		) );
	}

	$_post = get_post();

	global $post;

	$post = $memberships[0];

	MeprAppCtrl::load_scripts();

	wp_register_script( 'mepr-checkout-js', MEPR_JS_URL . '/checkout.js', array( 'jquery', 'jquery.payment' ), MEPR_VERSION );

	$gateway->enqueue_payment_form_scripts();

	$post = $_post;
	$errors = ! empty( $_POST['errors'] ) ? $_POST['errors'] : array();

	foreach ( $memberships as $i => $membership ) {
		$memberships[ $i ]->product = new MeprProduct( $membership->ID );
	}

	$selected_membership = isset( $_GET['membership_id'] ) ? $_GET['membership_id'] : $default;

	$coupon_code = sc_maybe_get_coupon_code_from_referral();

	ob_start(); ?>
	<div class="reg-content">
	<div class="risk-free-icon"></div>
	<div class="reg-col-left">

	<div class="mp_wrapper scmpreg-wrapper">
		<form class="mepr-checkout-form mepr-form mepr-card-form mepr-signup-form" method="post" novalidate id="payment-form">
			<input type="hidden" id="sc_mepr_process_signup_form" name="sc_mepr_process_signup_form" value="Y" />

			<div class="mepr-stripe-errors"></div>
			<?php MeprView::render('/shared/errors', get_defined_vars()); ?>
			<div class="mp-form-row mepr_mmbership_id">
				<label for="mepr_product_id">Membership</label>
				<select id="mepr_product_id" name="mepr_product_id">
					<?php foreach ( $memberships as $membership ) :
						$selected = $selected_membership == $membership->ID ? ' selected ' : ''; ?>
						<option value="<?php echo esc_attr( $membership->ID ); ?>"<?php echo $selected ?>>
							<?php echo get_the_title( $membership->ID ); ?> / <?php echo MeprProductsHelper::format_currency( $membership->product, true, null, false ); ?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
			<?php if(MeprUtils::is_user_logged_in()): ?>
				<input type="hidden" name="logged_in_purchase" value="1" />	
			<?php endif; ?>	

			<?php
			/*if ( MeprUtils::is_user_logged_in() && $mepr_options->show_fields_logged_in_purchases ) {
				MeprUsersHelper::render_custom_fields( $product );
			} elseif ( ! MeprUtils::is_user_logged_in() ) { // We only pass the 'signup' flag on initial Signup
				MeprUsersHelper::render_custom_fields( $product, true );
			}*/ ?>
			<?php if ( MeprUtils::is_user_logged_in() ) : ?>	
				<input type="hidden" name="user_email" id="user_email" value="<?php echo stripslashes( $mepr_current_user->user_email ); ?>" />
			<?php else : ?>	
				<input type="hidden" class="mepr-geo-country" name="mepr-geo-country" value="" />	

				<?php if ( ! $mepr_options->username_is_email ) : ?>
					<div class="mp-form-row mepr_username">
						<div class="mp-form-label">
							<label>
								<?php _e('Username:*', 'memberpress'); ?></label>
						</div>
						<input type="text" name="user_login" id="user_login" class="mepr-form-input" value="<?php echo (isset($user_login))?esc_attr(stripslashes($user_login)):''; ?>" required />
					</div>
				<?php endif; ?>	
				<div class="mp-form-row mepr_email">
					<input type="email" name="user_email" id="user_email" class="mepr-form-input" value="<?php echo (isset($user_email))?esc_attr(stripslashes($user_email)):''; ?>" required placeholder="<?php esc_attr_e('Email:*', 'memberpress'); ?>" />
				</div>
				<div class="mp-form-row mepr_first_name">
					<input type="text" name="first_name" id="first_name" class="mepr-form-input" value="<?php echo (isset($first_name))?esc_attr(stripslashes($first_name)):''; ?>" required placeholder="<?php esc_attr_e('First Name:*', 'memberpress'); ?>" />
				</div>
				<div class="mp-form-row mepr_last_name">
					<input type="text" name="last_name" id="last_name" class="mepr-form-input" value="<?php echo (isset($last_name))?esc_attr(stripslashes($last_name)):''; ?>" required placeholder="<?php esc_attr_e('Last Name:*', 'memberpress'); ?>" />
				</div>
				<div class="mp-form-row">
					<div class="mp-form-label">
						<!-- <label>	
						<?php _e('Password:*', 'memberpress'); ?></label>
					-->
					</div>
					<input type="password" name="mepr_user_password" id="mepr_user_password" class="mepr-form-input mepr-password" value="<?php echo (isset($mepr_user_password))?esc_attr(stripslashes($mepr_user_password)):''; ?>" required placeholder="<?php esc_attr_e('Password:*', 'memberpress'); ?>" />
				</div>
				<div class="mp-form-row">
					<div class="mp-form-label">
						<!-- <label>	
						<?php _e('Password Confirmation:*', 'memberpress'); ?></label>
					-->
					</div>
					<input type="password" name="mepr_user_password_confirm" id="mepr_user_password_confirm" class="mepr-form-input mepr-password-confirm" value="<?php echo (isset($mepr_user_password_confirm))?esc_attr(stripslashes($mepr_user_password_confirm)):''; ?>" required placeholder="<?php esc_attr_e('Password confirmation:*', 'memberpress'); ?>" />
				</div>
				<?php MeprHooks::do_action('mepr-after-password-fields'); ?>	
			<?php endif; ?>

			<?php MeprHooks::do_action( 'mepr-user-signup-fields' ); ?>	

			<input type="hidden" name="mepr_transaction_id" value="" />
			<input type="hidden" class="card-name" value="<?php echo $user->get_full_name(); ?>" />
			<?php if($mepr_options->show_address_fields && $mepr_options->require_address_fields): ?>
				<input type="hidden" class="card-address-1" value="<?php echo get_user_meta($user->ID, 'mepr-address-one', true); ?>" />
				<input type="hidden" class="card-address-2" value="<?php echo get_user_meta($user->ID, 'mepr-address-two', true); ?>" />
				<input type="hidden" class="card-city" value="<?php echo get_user_meta($user->ID, 'mepr-address-city', true); ?>" />
				<input type="hidden" class="card-state" value="<?php echo get_user_meta($user->ID, 'mepr-address-state', true); ?>" />
				<input type="hidden" class="card-zip" value="<?php echo get_user_meta($user->ID, 'mepr-address-zip', true); ?>" />
				<input type="hidden" class="card-country" value="<?php echo get_user_meta($user->ID, 'mepr-address-country', true); ?>" />
			<?php endif; ?>

			<div style="display: none;">
				<div class="mp-form-row mepr_payment_method">
					<?php echo MeprOptionsHelper::payment_methods_dropdown( 'mepr_payment_method' ); ?>
				</div>
			</div>

			<div class="mp-form-row">
				<div class="mp-form-label">
					<label><?php _e('Credit Card Number', 'memberpress'); ?></label>
					<span class="cc-error"><?php _e('Invalid Credit Card Number', 'memberpress'); ?></span>
				</div>
				<input type="tel" class="mepr-form-input card-number cc-number validation" pattern="\d*" autocomplete="cc-number" required>
			</div>

			<input type="hidden" name="mepr-cc-type" class="cc-type" value="" />

			<div class="mp-form-row">
				<div class="mp-form-label">
					<label><?php _e('Expiration', 'memberpress'); ?></label>
					<span class="cc-error"><?php _e('Invalid Expiration', 'memberpress'); ?></span>
				</div>
				<input type="tel" class="mepr-form-input cc-exp validation" pattern="\d*" autocomplete="cc-exp" placeholder="mm/yy" required>
				<?php //$gateway->months_dropdown('','card-expiry-month',isset($_REQUEST['card-expiry-month'])?$_REQUEST['card-expiry-month']:'',true); ?>
				<?php //$gateway->years_dropdown('','card-expiry-year',isset($_REQUEST['card-expiry-year'])?$_REQUEST['card-expiry-year']:''); ?>
			</div>

			<div class="mp-form-row">
				<div class="mp-form-label">
					<label><?php _e('CVC', 'memberpress'); ?></label>
					<span class="cc-error"><?php _e('Invalid CVC Code', 'memberpress'); ?></span>
				</div>
				<input type="tel" class="mepr-form-input card-cvc cc-cvc validation" pattern="\d*" autocomplete="off" required>
			</div>

			<?php // MeprHooks::do_action('mepr-stripe-payment-form', false); ?>

			<div class="coupon-field-wrap mp-form-row">
				<div class="mp-form-label"<?php echo $coupon_code ? ' style="display: none;"' : ''; ?>><a href="#">I have a coupon code</a></div>

				<div class="coupon-field-wrap"<?php echo $coupon_code ? '' : ' style="display: none;"'; ?>>
					<div class="mp-form-label">
						<label for="mepr_coupon_code"><?php _e( 'Coupon Code', 'stemcounter' ); ?></label>
					</div>
					<input type="text" class="mepr-form-input" name="mepr_coupon_code" id="mepr_coupon_code" value="<?php echo $coupon_code ? esc_attr( $coupon_code ) : ''; ?>" />
				</div>
			</div>

			<div class="mepr_spacer">&nbsp;</div>

			<noscript>
				<p class="mepr_nojs">
					<?php _e('Javascript is disabled in your browser. You will not be able to complete your purchase until you either enable JavaScript in your browser, or switch to a browser that supports it.', 'memberpress'); ?></p>
				<div class="mepr_spacer">&nbsp;</div>
			</noscript>

			<?php if ( ! MeprUtils::is_user_logged_in() ) : ?>	
				<?php if ( $mepr_options->require_tos ) : ?>
					<div class="mp-form-row mepr_tos">
						<label for="mepr_agree_to_tos" class="mepr-checkbox-field mepr-form-input" required>
							<input type="checkbox" name="mepr_agree_to_tos" id="mepr_agree_to_tos" <?php checked(isset($mepr_agree_to_tos)); ?>	
							/>
							<!-- <a href="<?php echo stripslashes( $mepr_options->tos_url ); ?>" target="_blank"><?php echo stripslashes( $mepr_options->tos_title ); ?></a> * -->
 							I have read and agree to the 
 							<a href="<?php echo stripslashes( $mepr_options->tos_url ); ?>" target="_blank">Terms Of Service</a> and 
 							<a href="<?php echo esc_url( get_permalink( get_page_by_path( 'privacypolicy' ) ) ) ?>" target="_blank">Privacy Policy</a> *
						</label>
					</div>
				<?php endif; ?>	

				<?php // This thing needs to be hidden in order for this to work so we do it explicitly as a style ?>	
				<input type="text" id="mepr_no_val" style="display: none;" name="mepr_no_val" class="mepr-form-input" autocomplete="off" />	
			<?php endif; ?>

			<input type="submit" class="mepr-submit button button-primary button-large btn btn-theme btn-block" value="Sign Up" />
			<img src="<?php echo admin_url( 'images/loading.gif' ); ?>" style="display: none;" class="mepr-loading-gif" />
			
		</form>
	</div>
	</div><!-- reg-col-left -->

	<div class="reg-col-right">
		<div class="join_plans fl-module">
			<div class="mepr-price-menu minimal_gray_horizontal minimal_horizontal">
				<div class="mepr-price-boxes mepr-1-col">
					<?php foreach ( $memberships as $membership ) :
						MeprGroupsHelper::group_page_item( $membership->product ); ?>
						<div class="price-total price-total-<?php echo $membership->ID;?>">
							<div class="price-error price-error-<?php echo $membership->ID;?>"></div>
							Total: 
							<?php if ( $coupon_code ) :
								$valid = MeprCoupon::is_valid_coupon_code( $coupon_code, $membership->ID );
								if ( $valid ) {
									$sub = new MeprSubscription();
									$sub->user_id = get_current_user_id();
									$sub->load_product_vars( $membership->product, $coupon_code, true );
									$sub->maybe_prorate(); // sub to sub
									$price = MeprSubscriptionsHelper::format_currency( $sub );
									$error = '';
								} else {
									$price = MeprProductsHelper::format_currency( $membership->product, true, null, true );
									$error = 'The coupon you entered is not valid for this membership ';
								} ?>
								<span class="mepr-price-plain price-plain-<?php echo $membership->ID; ?>">
									<?php echo $price; ?>
								</span>
							<?php else : ?>
								<span class="mepr-price-plain price-plain-<?php echo $membership->ID;?>">
									<?php echo MeprProductsHelper::format_currency( $membership->product, true, null, false) ?>
								</span>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div><!-- reg-col-right -->

	</div> <!-- reg-content -->

	<script type="text/javascript">
	$(function() {
		$('[data-toggle="tooltip"]').tooltip();

		//add checkbox
		  $("#custom-reg-field-hardgood-multiple").hide();
		  $("#custom-reg-field-hardgood-multiple").val('1');
		  $("#custom-reg-field-hardgood-multiple").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forHardgood">');

		  $("#custom-reg-field-fresh-flower-multiple").hide();
		  $("#custom-reg-field-fresh-flower-multiple").val('1');
		  $("#custom-reg-field-fresh-flower-multiple").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forFreshFlower">');

		  $("#custom-reg-field---charge-card-rate").hide();
		  $("#custom-reg-field---charge-card-rate").val('0');
		  $("#custom-reg-field---charge-card-rate").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forChangeCard">');

		  $("#custom-reg-field-sales-tax--").hide();
		  $("#custom-reg-field-sales-tax--").val('0');
		  $("#custom-reg-field-sales-tax--").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forSalesTax">');

		  $("#custom-reg-field---delivery-cost").hide();
		  $("#custom-reg-field---delivery-cost").val('0');
		  $("#custom-reg-field---delivery-cost").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forDeliveryCost">');

		  $("#custom-reg-field---labor").hide();
		  $("#custom-reg-field---labor").val('0'); 
		  $("#custom-reg-field---labor").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forLabor" name="add_labor_costs">');

		  $("#forHardgood").click(function(){
		    if($('#forHardgood').prop("checked")==true){
		      $("#custom-reg-field-hardgood-multiple").show();
		    }else{
		      $("#custom-reg-field-hardgood-multiple").hide();
		      $("#custom-reg-field-hardgood-multiple").val('1');
		    }
		  });

		  $("#forFreshFlower").click(function(){
		    if($('#forFreshFlower').prop("checked")==true){
		      $("#custom-reg-field-fresh-flower-multiple").show();
		    }else{
		      $("#custom-reg-field-fresh-flower-multiple").hide();
		      $("#custom-reg-field-fresh-flower-multiple").val('1');
		    }
		  });

		  $("#forChangeCard").click(function(){
		    if($('#forChangeCard').prop("checked")==true){
		      $("#custom-reg-field---charge-card-rate").show();
		    }else{
		      $("#custom-reg-field---charge-card-rate").hide();
		      $("#custom-reg-field---charge-card-rate").val('0');
		    }
		  });

		  $("#forSalesTax").click(function(){
		    if($('#forSalesTax').prop("checked")==true){
		      $("#custom-reg-field-sales-tax--").show();
		    }else{
		      $("#custom-reg-field-sales-tax--").hide();
		      $("#custom-reg-field-sales-tax--").val('0');
		    }
		  });

		  $("#forDeliveryCost").click(function(){
		    if($('#forDeliveryCost').prop("checked")==true){
		      $("#custom-reg-field---delivery-cost").show();
		    }else{
		      $("#custom-reg-field---delivery-cost").hide();
		      $("#custom-reg-field---delivery-cost").val('0')
		    }
		  });

		  $("#forLabor").click(function(){
		    if($('#forLabor').prop("checked")==true){
		      $("#custom-reg-field---labor").show();
		    }else{
		      $("#custom-reg-field---labor").hide();
		      $("#custom-reg-field---labor").val('0')
		    }
		  });
		});

		(function($){

		$(document).ready(function(){
			var inputs = '\
			#custom-reg-field-hardgood-multiple,\
			#custom-reg-field-fresh-flower-multiple,\
			#custom-reg-field---charge-card-rate,\
			#custom-reg-field-sales-tax--,\
			#custom-reg-field---delivery-cost,\
			#custom-reg-field---labor\
			';
			stemcounter.floatInput($(inputs));

			$('.coupon-field-wrap a').on('click', function(e){
				e.preventDefault();

				$(this).closest('.mp-form-label').slideUp().next('.coupon-field-wrap').slideDown(function(){
					$('#mepr_coupon_code').focus();
				});
			});

			$('.mepr-checkout-form').on('submit', function(e){
				if ( ! $(this).find('.mepr-stripe-token').length ) {
					e.preventDefault();
				}
			});

			function show_plan( plan_id ) {
				$('.mepr-price-box').removeClass('show');
				$('.price-total').removeClass('show');
				
				$( '#mepr-price-box-' + plan_id ).addClass('show');
				$( '.price-total-' + plan_id ).addClass('show');
				
			}

			show_plan( $('#mepr_product_id').val() );

			$('#mepr_product_id').on('click, change', function(e) {
				show_plan( $(this).val() );
				coupon_code_check();
			});

			$('#mepr_coupon_code').on('change', coupon_code_check );

			function coupon_code_check( ) {
				var plan_id = $('#mepr_product_id').val();
				var coupon = $('#mepr_coupon_code').val();
				$( '.price-plain-' + plan_id ).html( ' ... ' );

				$.post(stemcounter.ajax_url, {
					action: 'sc_get_membership_prices',
					'coupon': coupon
				}, function( response ) {
					stemcounter.JSONResponse(response, function (r) {
						if (r.success) {

							if ( !r.mepr_prices[ plan_id ] ) {
								alertify.error( 'Error - membership not found' );
								return false;
							}
							$( '.price-plain-' + plan_id ).html( r.mepr_prices[ plan_id ].price );
							if ( !coupon ) {
								return;
							}
							if ( r.mepr_prices[ plan_id ].valid ) {
								alertify.success( 'Coupon code accepted' );
							} else {
								$('#mepr_coupon_code').val( '' );
								alertify.error( 'Coupon code not accepted' );
							}
							/*
							$( '.price-error' ).html( '' );
							$.each(r.mepr_prices, function(index, value) {
								$( '.price-plain-' + index ).html( value.price );
								
								if ( !value.valid ) {
									//$( '.price-error-' + index ).html( value.error );
									$('#mepr_coupon_code').val( '' );
									alertify.error( 'Inavlid coupon code' );
								}
							})*/
						} else {
							alertify.error( 'Error checking coupon code' );
						}
					});
				});
			}

		});

	})(jQuery);
	</script>
	<?php

	return ob_get_clean();
}
add_shortcode( 'stemcounter-registration', 'stemcounter_registration_shortcode' );

function sc_parse_memberpress_standalone_request() {
	if ( isset( $_POST ) && isset( $_POST['sc_mepr_process_signup_form'] ) ) {
		$ran = true;
		$checkout_ctrl = MeprCtrlFactory::fetch( 'checkout' );

		unset( $_POST['errors'] );
		$mepr_options = MeprOptions::fetch();

		// Validate the form post
		$errors = MeprHooks::apply_filters('mepr-validate-signup', MeprUser::validate_signup($_POST, array()));
		if ( ! empty( $errors ) ) {
			$_POST['errors'] = $errors; //Deprecated?
			$_REQUEST['errors'] = $errors;
			
			return;
		}

		// Check if the user is logged in already
		$is_existing_user = MeprUtils::is_user_logged_in();

		sc_start_transaction();

		// error_log( __FILE__ . ': ' . __LINE__ );

		if ( $is_existing_user ) {
			$usr = MeprUtils::get_currentuserinfo();
		} else { // If new user we've got to create them and sign them in
			remove_action( 'affwp_set_affiliate_status', 'affwp_notify_on_approval', 10 );
			$usr = new MeprUser();
			$usr->user_login = ( $mepr_options->username_is_email ) ? $_POST['user_email'] : $_POST['user_login'];
			$usr->user_email = $_POST['user_email'];
			$usr->first_name = $_POST['first_name'];
			$usr->last_name = $_POST['last_name'];

			//Have to use rec here because we unset user_pass on __construct
			$usr->rec->user_pass = $_POST['mepr_user_password'];

			try {
				$usr->store();

				$_user = get_userdata( $usr->ID );
				// error_log( $_user->user_login );

				// Log the new user in
				wp_set_current_user( $usr->ID, $usr->user_login );
				wp_set_auth_cookie( $usr->ID, true );
				do_action( 'wp_login', $_user->user_login, $_user );
				
				MeprEvent::record('login', $usr); //Record the first login here
			} catch( MeprCreateException $e ) {
				$_POST['errors'] = array(__( 'The user was unable to be saved.', 'memberpress'));  //Deprecated?
				$_REQUEST['errors'] = array(__( 'The user was unable to be saved.', 'memberpress'));
				sc_memberpress_signup_failed( $is_existing_user );
				
				return;
			}
		}

		// error_log( __FILE__ . ': ' . __LINE__ );

		// Create a new transaction and set our new membership details
		$txn = new MeprTransaction();
		$txn->user_id = $usr->ID;

		// Get the membership in place
		$txn->product_id = $_POST['mepr_product_id'];
		$prd = $txn->product();

		// If we're showing the fields on logged in purchases, let's save them here
		if(!$is_existing_user || ($is_existing_user && $mepr_options->show_fields_logged_in_purchases)) {
			MeprUsersCtrl::save_extra_profile_fields($usr->ID, true, $prd, true);
			$usr = new MeprUser($usr->ID); //Re-load the user object with the metadata now (helps with first name last name missing from hooks below)
		}

		// Set default price, adjust it later if coupon applies
		$price = $prd->adjusted_price();

		// Default coupon object
		$cpn = (object)array('ID' => 0, 'post_title' => null);

		// Adjust membership price from the coupon code
		if(isset($_POST['mepr_coupon_code']) && !empty($_POST['mepr_coupon_code'])) {
			// Coupon object has to be loaded here or else txn create will record a 0 for coupon_id
			$cpn = MeprCoupon::get_one_from_code($_POST['mepr_coupon_code']);

			if(($cpn !== false) || ($cpn instanceof MeprCoupon)) {
				$price = $prd->adjusted_price($cpn->post_title);
			}
		}

		// error_log( __FILE__ . ': ' . __LINE__ );

		$txn->set_subtotal($price);

		// Set the coupon id of the transaction
		$txn->coupon_id = $cpn->ID;

		// Figure out the Payment Method
		if( isset( $_POST['mepr_payment_method'] ) && ! empty( $_POST['mepr_payment_method'] ) ) {
			$txn->gateway = $_POST['mepr_payment_method'];
		} else {
			$txn->gateway = MeprTransaction::$free_gateway_str;
		}

		// Let's checkout now
		if ( $txn->gateway === MeprTransaction::$free_gateway_str ) {
			$signup_type = 'free';
		} elseif ( ( $pm = $txn->payment_method() ) && ( $pm instanceof MeprBaseRealGateway ) ) {
			// Create a new subscription
			if ( $prd->is_one_time_payment() ) {
				$signup_type = 'non-recurring';
			} else {
				$signup_type = 'recurring';

				$sub = new MeprSubscription();
				$sub->user_id = $usr->ID;
				$sub->gateway = $pm->id;
				$sub->load_product_vars($prd, $cpn->post_title, true);
				$sub->maybe_prorate(); // sub to sub
				$sub->store();

				$txn->subscription_id = $sub->ID;
			}
		} else {
			$_POST['errors'] = array(__('Invalid payment method', 'memberpress'));
			
			sc_memberpress_signup_failed( $is_existing_user );
			return;
		}

		// error_log( __FILE__ . ': ' . __LINE__ );

		$txn->store();

		// error_log( __FILE__ . ': ' . __LINE__ );

		if ( empty( $txn->id ) ) {
			// Don't want any loose ends here if the $txn didn't save for some reason
			if ( $signup_type === 'recurring' && ( $sub instanceof MeprSubscription ) ) {
				$sub->destroy();
			}

			$_POST['errors'] = array(__('Sorry, we were unable to create a transaction.', 'memberpress'));
			
			sc_memberpress_signup_failed( $is_existing_user );
			// error_log( __FILE__ . ': ' . __LINE__ );
			return;
		}

		// error_log( __FILE__ . ': ' . __LINE__ );

		// We want to rollback the transaction on payment failure
		add_action( 'mepr_payment_failure', function( $a ) use ( $is_existing_user ){
			// error_log( 'mepr_payment_failure' );
			
			sc_memberpress_signup_failed( $is_existing_user );
		} );

		// error_log( __FILE__ . ': ' . __LINE__ );

		try {
			if ( ( 'free' !== $signup_type ) && ( $pm instanceof MeprBaseRealGateway ) ) {
				$pm->process_signup_form( $txn );
			}

			// DEPRECATED: These 2 actions here for backwards compatibility ... use mepr-signup instead
			MeprHooks::do_action('mepr-track-signup',   $txn->amount, $usr, $prd->ID, $txn->id);
			MeprHooks::do_action('mepr-process-signup', $txn->amount, $usr, $prd->ID, $txn->id);

			// Signup type can be 'free', 'non-recurring' or 'recurring'
			MeprHooks::do_action("mepr-{$signup_type}-signup", $txn);
			MeprHooks::do_action('mepr-signup', $txn);

			// error_log( __FILE__ . ': ' . __LINE__ );

			$_POST['mepr_process_payment_form'] = 'Y';
			$_POST['mepr_transaction_id'] = $txn->id;
			sc_process_payment_form( $txn, $pm );

			// error_log( __FILE__ . ': ' . __LINE__ );

			// If rollback was already called due to payment failure
			// Nothing will break if we send an extra COMMIT
			// error_log( __FILE__ . ': ' . __LINE__ );
			sc_commit_transaction();
			// error_log( __FILE__ . ': ' . __LINE__ );
			if ( empty( $_POST['errors'] ) ) {
				wp_redirect( sc_get_permalink_by_template( 'template-events.php' ) );
			}
		} catch( Exception $e ) {
			// error_log( __FILE__ . ': ' . __LINE__ );
			$_POST['errors'] = array( $e->getMessage() );
			sc_memberpress_signup_failed( $is_existing_user );
		}

		unset( $_POST['mepr_process_payment_form'] );
	}
}
add_action( 'init', 'sc_parse_memberpress_standalone_request', 0 );

function sc_memberpress_signup_failed( $is_existing_user ) {
	// error_log( 'sc_memberpress_signup_failed()' );

	if ( ! $is_existing_user ) {
		// error_log( __FILE__ . ': ' . __LINE__ );
		wp_clear_auth_cookie();
		wp_set_current_user( null );
	}

	unset( $_POST['mepr_process_payment_form'] );
	sc_rollback_transaction();
}

function sc_process_payment_form( $txn, $pm ) {
	if ( $txn->rec != false ) {
		$mepr_options = MeprOptions::fetch();
		if (  $pm instanceof MeprBaseRealGateway ) {
			$errors = $pm->validate_payment_form(array());

			if ( empty( $errors ) ) {
				// process_payment_form either returns true
				// for success or an array of $errors on failure
				try {
					$pm->process_payment_form( $txn );
				} catch( Exception $e ) {
					MeprHooks::do_action( 'mepr_payment_failure', $txn );
					throw $e;
				}
			}

			if ( empty( $errors ) ) {
				// We're good to go
				return true;
			} else {
				throw new Exception( $errors[0] );
			}
		}
	}

	throw new Exception( __( 'Sorry, an unknown error occurred.', 'memberpress' ) );
}

function sc_get_membership_prices() {
	$coupon = ( empty( $_POST['coupon'] ) ) ? false : $_POST['coupon'];
	$memberships = get_posts( array(
		'numberposts' => -1,
		'post_type'   => 'memberpressproduct',
		'post_status' => 'publish'
	) );

	$data = array();
	foreach ( $memberships as $membership ) {
		$product = new MeprProduct( $membership->ID );
		$valid = $coupon ? MeprCoupon::is_valid_coupon_code( $coupon, $membership->ID ) : true;
		//$error = ( $valid ) ? '' : 'The coupon you entered is not valid for this membership ';
		if ( $valid ) {
			$sub = new MeprSubscription();
			$sub->user_id = get_current_user_id();
			$sub->load_product_vars( new MeprProduct( $membership->ID ), $_POST['coupon'], true );
			$sub->maybe_prorate(); // sub to sub
			$price = MeprSubscriptionsHelper::format_currency( $sub );
			$error = '';
		} else {
			$price = MeprProductsHelper::format_currency( $product, true, null, true );
			$error = 'The coupon you entered is not valid for this membership ';
		}
		$data[ $membership->ID ] = array( 'valid' => $valid, 'price' => $price, 'error' => $error );
	}

	$response['success'] = true;
	$response['mepr_prices'] = $data;
	wp_send_json( $response );
}

add_action('wp_ajax_sc_get_membership_prices', 'sc_get_membership_prices');
add_action('wp_ajax_nopriv_sc_get_membership_prices', 'sc_get_membership_prices');