<?php
use Illuminate\Http\Request;
use Stemcounter\Event;
use Stemcounter\Vendor;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;
use Stemcounter\Meta;

function sc_render_manage_vendors( $content ) {
	$vendors = Vendor::where(array(
		'user_id' => get_current_user_id()
	))->get();

	$table_len = get_user_meta( get_current_user_id(), 'sc_vendor_table_len', true );
	ob_start(); ?>
	<div class="row">
			<?php if ( $content ) : ?>
				<div class="col-lg-12 order-message">
					<?php echo $content; ?>
				</div>
			<?php endif; ?>
		<!-- <div class="col-lg-12 clearfix">
			<button class=" btn btn-primary btn-lg pull-right add-vendor-button" style="margin-top: 10px; margin-bottom:10px;">New Venue</button>
		</div>
		-->
	</div><!-- /row -->
	
	<div class="vendors-data">
		<div class="row mt">
			<div class="col-lg-12">
				<div class="content-panel">
					<section id="unseen">
						<table id="vendorsTable" class="table table-striped table-advance table-hover no-footer" data-page-length="<?php echo ! empty( $table_len ) ? $table_len : 10; ?>">
							<thead>
								<tr>
									<th colspan="7">
										<a class="add-vendor-button cta-link">
											+ New Vendor
										</a>
									</th>
								</tr>
								<tr>
									<th class="vendor-td-name">Vendor Name</th>
									<th class="hidden-xs vendor-td-website">Website</th>
									<th class="hidden-xs vendor-td-phone">Phone</th>
									<th class="hidden-xs vendor-td-address">Address</th>
									<th class="hidden-xs vendor-td-tags">Tags</th>
									<th class="hidden-xs vendor-td-edit" style="width: 50px"></th>
									<th class="hidden-xs vendor-td-delete" style="width: 50px"></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($vendors as $vendor): 
									$tags = Meta::where(array(
										'type' => 'vendor',
										'type_id' => $vendor->id,
										'meta_key' => 'tag'
									))->get();
								?>
									<tr>
										<td class="vendor-td-name"><?php echo $vendor->company_name; ?>
											<div class="visible-xs">
												Site: <?php echo $vendor->website; ?><br />
												Phone: <?php echo $vendor->phone; ?><br />
												Address: <?php echo $vendor->address; ?><br />
												<a href="#" class="edit-vendor-button" data-vendor-id="<?php echo $vendor->id; ?>">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="#" class="delete-vendor-button" data-vendor-id="<?php echo $vendor->id; ?>">
													<i class="fa fa-trash"></i>
												</a>
												<?php if (!empty($tags)): ?>
													<?php foreach ($tags as $i => $tag): 
														if ($i !== 0 && $i % 4 == 0) echo '<br/>'; ?>

														<span class="label label-info label-mini"><?php echo sc_get_vendor_tags($tag->meta_value); ?></span>
													<?php endforeach ?>
												<?php endif ?>

											</div>
										</td>
										<td class="hidden-xs vendor-td-website"><?php echo $vendor->website; ?></td>
										<td class="hidden-xs vendor-td-phone"><?php echo $vendor->phone; ?></td>
										<td class="hidden-xs vendor-td-address"><?php echo $vendor->address; ?></td>
										<td class="hidden-xs ">
											<?php if (!empty($tags)): ?>
												<?php foreach ($tags as $i => $tag): 
													if ($i !== 0 && $i % 4 == 0) echo '<br/>'; ?>

													<span class="label label-info label-mini"><?php echo sc_get_vendor_tags($tag->meta_value); ?></span>
												<?php endforeach ?>
											<?php endif ?>
										</td>
										<td class="hidden-xs order-td-edit" style="text-align: right;">
											<!-- <button type="button" class="btn btn-primary btn-xs edit-vendor-button" data-vendor-id="<?php echo $vendor->id; ?>"><i class="fa fa-pencil"></i></button> -->
											<a href="#" class="edit-vendor-button" data-vendor-id="<?php echo $vendor->id; ?>">
												<i class="fa fa-pencil"></i>
											</a>
										</td>
										<td class="hidden-xs order-td-delete" style="text-align: right;">
											<!-- <button type="button" class="btn btn-danger btn-xs delete-vendor-button" data-vendor-id="<?php echo $vendor->id; ?>"><i class="fa fa-trash"></i></button> -->
											<a href="#" class="delete-vendor-button" data-vendor-id="<?php echo $vendor->id; ?>">
												<i class="fa fa-trash"></i>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</section>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-4 -->			
		</div><!-- /row -->
	</div>
	<br/>
	<br/>
	<br/>
	<script type="text/javascript">
		(function($){
			$(document).ready(function(){
				$("#vendorsTable").dataTable({
					//"orderCellsTop": true
				});
			});

			function openAddVendorPopup(id, title) {
				title = (typeof title == 'undefined') ? 'Add New Vendor' : title;
				var url = stemcounter.aurl({
					action: 'sc_add_user_vendor_form',
				});

				stemcounter.openAjaxModal(title, url, 'new-modal edit-venue-modal');
			}

			function openEditVendorPopup(vendorId, title) {
				title = (typeof title == 'undefined') ? 'Edit Vendor' : title;
				var url = stemcounter.aurl({
					action: 'sc_edit_user_vendor_form',
					vendor_id: vendorId
				});

				stemcounter.openAjaxModal(title, url, 'new-modal edit-venue-modal');
			}

			$('.add-vendor-button').click(function() {
				openAddVendorPopup(0);
			});

			$('.edit-vendor-button').click(function() {
				openEditVendorPopup($(this).data('vendorId'));
			});

			$('.delete-vendor-button').click(function(e){
				e.preventDefault();
				var vendorId = $(this).data('vendorId');

				alertify.confirm('Are you sure you wish to delete this vendor?', function () {
				    jQuery.ajax({
						type:"post",
						url: window.stemcounter.ajax_url,
						data: {
							action: 'sc_delete_user_vendor',
							vendor_id: vendorId 
						},
						success: function(response){
							stemcounter.JSONResponse(response, function(r) {
								if (r.success) document.location.reload(); 
							});
						}
					});
				});
			});

			$('#vendorsTable').on( 'length.dt', function ( e, settings, len ) {
			
				var url = window.stemcounter.aurl({ action: 'sc_datatables_len' }),
					data = {
						'meta_key': 'sc_vendor_table_len',
						'meta_value': len
					};

				$.post(url, data, function (response) {
					stemcounter.JSONResponse(response, function (r) {
						if ( ! r.success ) {
							console.log( r );
						}
					});
				});
			});

		})(jQuery);
	</script>
	<?php
	return ob_get_clean();
}
add_filter( 'sc/app/content/vendors', 'sc_render_manage_vendors' );



function sc_ajax_add_user_vendor_form() {
	$request = Request::capture();
	$r = new Ajax_Response();
	?>
	<form class="form-horizontal style-form add-vendor-form" method="post">
		<div class="form-shell"></div>
	</form>
	<script type="text/javascript">
	jQuery(document).trigger('stemcounter.action.renderVendorForm', {
		node: $('.add-vendor-form'),
		layout: 'add-vendor',
		allVendorTags: <?php echo json_encode(sc_get_vendor_tags()); ?>
	});
	</script>
	<?php
	exit;
}
add_action('wp_ajax_sc_add_user_vendor_form', 'sc_ajax_add_user_vendor_form');

function sc_ajax_add_user_vendor() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();


	$vendor_company_name = $request->input('vendor_company_name');
	try {
		if (empty($vendor_company_name)) {
			throw new Humanreadable_Exception('The Vendor field is required.');	
		} 
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	}

	$exists_vendor = Vendor::where(
		array(
			'company_name'	=> $vendor_company_name,
			'user_id'		=> get_current_user_id(),
		)
	)->exists();

	if ($exists_vendor) {
		$r->fail('Vendor already exists.');
	}

	try {
		
		$new_vendor = new Vendor();
		$new_vendor->user_id = get_current_user_id();
		$new_vendor->company_name = $vendor_company_name;
		$new_vendor->address = $request->input('vendor_address');
		$new_vendor->notes = $request->input('vendor_note');
		$new_vendor->phone = $request->input('vendor_phone');
		$new_vendor->email = is_null($vendor_email) ? '' : $vendor_email;
		$new_vendor->website = $request->input('vendor_website');
		$new_vendor->save();

		$vendor_tags = $request->input('vendor_tags');
		if (!empty($vendor_tags)) {
			foreach ($vendor_tags as $tag) {
				$new_tag = new Meta();
				$new_tag->type = 'vendor';
				$new_tag->type_id = $new_vendor->id; //vendor id
				$new_tag->meta_key = 'tag'; 
				$new_tag->meta_value = $tag; //slug
				$new_tag->save();
			}
		}
		$r->add_payload('vendor_data', $new_vendor);
	} catch (Exception $e) {
		$r->fail($e->getMessage());		
	}

	$r->respond('Success');
}
add_action('wp_ajax_sc_add_user_vendor', 'sc_ajax_add_user_vendor');



function sc_ajax_edit_user_vendor_form() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$vendor = Vendor::where(array(
			'id' => intval( $request->input('vendor_id') ),
			'user_id' => get_current_user_id()
		))->firstOrFail();

		$meta_tags = Meta::where(array(
			'type' => 'vendor',
			'type_id' => $vendor->id,
			'meta_key' => 'tag'
		))->get();

		$vendor_tags = array();
		foreach ($meta_tags as $tag) {
			array_push($vendor_tags, $tag->meta_value);
		}
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}

	?>
	<form class="form-horizontal style-form edit-vendor-form" method="post">
		<div class="form-shell"></div>
	</form>
	<script type="text/javascript">
	jQuery(document).trigger('stemcounter.action.renderVendorForm', {
		node: $('.edit-vendor-form'),
		layout: 'edit-vendor',
		vendorId: <?php echo json_encode($vendor->id); ?>,
		vendorCompanyName: <?php echo json_encode($vendor->company_name); ?>,
		vendorWebsite: <?php echo json_encode($vendor->website); ?>,
		vendorEmail: <?php echo json_encode($vendor->email); ?>,
		vendorPhone: <?php echo json_encode($vendor->phone); ?>,
		vendorAddress: <?php echo json_encode($vendor->address); ?>,
		vendorNote: <?php echo json_encode($vendor->notes); ?>,
		vendorTags: <?php echo json_encode( $vendor_tags ); ?>,
		allVendorTags: <?php echo json_encode(sc_get_vendor_tags()); ?>
	});
	</script>
	<?php
	exit;
}
add_action('wp_ajax_sc_edit_user_vendor_form', 'sc_ajax_edit_user_vendor_form');

function sc_ajax_edit_user_vendor() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$vendor_company_name = $request->input('vendor_company_name');
	try {
		if ( empty( $vendor_company_name ) ) {
			throw new Humanreadable_Exception('The Vendor field is required.');	
		}
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	}

	$vendor_email = $request->input('vendor_email');

	try {
		$vendor = Vendor::where(array(
			'id' => intval( $request->input('vendor_id') )
		))->withTrashed()->firstOrFail();
		$vendor->company_name = $vendor_company_name;
		$vendor->address = $request->input('vendor_address');
		$vendor->notes = $request->input('vendor_note');
		$vendor->phone = $request->input('vendor_phone');
		$vendor->email = $vendor_email;
		$vendor->website = $request->input('vendor_website');
		$vendor->save();

		Meta::where(array(
			'type' => 'vendor',
			'type_id' => intval( $request->input('vendor_id') ),
			'meta_key' => 'tag'
		))->delete();

		$vendor_tags = $request->input('vendor_tags');
		if ( !empty( $vendor_tags ) ) {
			foreach ($vendor_tags as $tag) {
				$new_tag = new Meta();
				$new_tag->type = 'vendor';
				$new_tag->type_id = abs($request->input('vendor_id')); //vendor id
				$new_tag->meta_key = 'tag'; 
				$new_tag->meta_value = $tag; //slug
				$new_tag->save();
			}
		}
		$r->add_payload('vendor_data', $vendor);
	} catch (Exception $e) {
		$r->fail($e->getMessage());		
	}

	$r->respond('Success');
}
add_action('wp_ajax_sc_edit_user_vendor', 'sc_ajax_edit_user_vendor');

function sc_ajax_delete_user_vendor() {
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$vendor = Vendor::where(array(
			'id' => $request->input('vendor_id'),
			'user_id' => get_current_user_id()
		))->delete();

		$vendor_meta = Meta::where(array(
			'type' => 'vendor',
			'type_id' => intval( $request->input('vendor_id') ),
		))->delete();
	} catch (Exception $e) {
		throw new Humanreadable_Exception('Could not find vendor.');
	}
	
	$r->respond('Success');
}
add_action('wp_ajax_sc_delete_user_vendor', 'sc_ajax_delete_user_vendor');

function sc_get_vendor_tags($slug = '') {
	// $tags = array(
	// 	'venue' => 'Venue',
	// 	'planner' => 'Planner',
	// 	'bridal-salon' => 'Bridal Salon',
	// 	'event-design' => 'Event Design',
	// 	'videographer' => 'Videographer',
	// 	'event-rental' => 'Event Rental',
	// 	'florist' => 'Florist',
	// 	'officiant' => 'Officiant',
	// 	'dj' => 'DJ',
	// 	'wedding-band' => 'Wedding Band',
	// 	'beauty' => 'Beauty',
	// 	'jeweler' => 'Jeweler',
	// 	'bridal-salon' => 'Bridal Salon',
	// 	'transportation' => 'Transportation'
	// );
	$tags = array(
		'venue' => 'Venue',
		'consultant' => 'Consultant',
		'photographer' => 'Photographer',
		'baker' => 'Baker',
		'caterer' => 'Caterer',
	);

	if (!empty($slug)) {
		return !empty($tags[$slug]) ? $tags[$slug] : false;
	}

	return $tags;
}

function sc_get_user_event_venues($event_id) {
	$event_vendors = sc_get_user_event_vendors($event_id);
	$_event_venues = $event_vendors['event_vendors']['venue'];
	$event_venues = array();

	foreach ($_event_venues as $venue) {
		$vendor = Vendor::where('id', '=', $venue['venue_id'])->withTrashed()->first();
		if ( ! $vendor ) {
			continue;
		}

		$event_venues[] = array(
			'id' => $vendor->id,
			'name' => $vendor->company_name,
			'address' => $vendor->address,
			'type' => $venue['venue_type']
		);
	}

	return $event_venues;
}

function sc_get_user_venues_list( $user_id = false ) {
	$user_id = $user_id ? $user_id : get_current_user_id();

	//getting all of the users vendors
	$vendor_list = array();
	$user_vendors = Vendor::where(array(
		'user_id' => $user_id,
	))->orderBy('company_name', 'asc')->get();
	//getting the venues for now
	
	foreach ($user_vendors as $vendor) {
		$vendor_metas = Meta::where(array(
			'type' => 'vendor',
			'type_id' => $vendor->id,
			'meta_key' => 'tag',
		))->get();

		foreach ($vendor_metas as $vendor_meta) {
			//extend this if you want the vendor of something else
			/*if ($vendor_meta->meta_value == 'venue') {*/
				array_push($vendor_list, array(
					'id'           => $vendor->id,
					'company_name' => $vendor->company_name,
					'address'      => $vendor->address,
				));
			/*}*/
		}
	}

	return $vendor_list;
}

function sc_get_user_event_vendors( $event_id, $user_id = false ) {
	//getting the event vendors
	$venues_list = array();
	$linked_event_vendors = Meta::where(array(
		'type' => 'event',
		'type_id' => $event_id,
		'meta_key' => 'linked_vendor' 
	))->get();

	$linked_event_venue_types = Meta::where(array(
		'type' => 'event',
		'type_id' => $event_id,
		'meta_key' => 'venue_types'
	))->first();

	$event_vendors = array(
		'venue' => array(),
	);
	foreach ( $linked_event_vendors as $i => $linked_event_vendor ) {
		switch ($linked_event_vendor->meta_value) {
			case 'no_venue':
				// $event_vendors['venue'][] = array(
				// 	'venue_id' => '-1',
				// 	'venue_type' => empty(json_decode($linked_event_venue_types)) || empty(json_decode($linked_event_venue_types->meta_value)[$i]) ? '-1': json_decode($linked_event_venue_types->meta_value)[$i]
				// ); //No Venue option
			default: 
				$event_vendors['venue'][] = array(
					'venue_id' => $linked_event_vendor->meta_value,
					'venue_type' => empty(json_decode($linked_event_venue_types)) || empty(json_decode($linked_event_venue_types->meta_value)[$i]) ? '-1': json_decode($linked_event_venue_types->meta_value)[$i]
				);
			break;
		}
	}

	//searching for deleted venues.. adding the deleted venue to the list
	foreach ($event_vendors['venue'] as $i => $venue_vendor) {
		$vendor = Vendor::where(array(
			'id' => $venue_vendor['venue_id']
		))->withTrashed()->first();

		if ( ! $vendor ) {
			continue;
		}

		if (!is_null($vendor->deleted_at)) {
			array_push($venues_list, array(
				'id'           => $vendor->id,
				'company_name' => $vendor->company_name,
				'address'      => $vendor->address,
			));
		} else {
			$event_vendors['venue'][ $i ]['address'] = $vendor->address;
			$event_vendors['venue'][ $i ]['company_name'] = $vendor->company_name;
		}
	}

	$venues_list = array_merge( $venues_list, sc_get_user_venues_list( $user_id ) );

	$venue_ids = array();
	foreach ( $venues_list as $i => $venue ) {
		if ( ! empty( $venue['id'] ) ) {
			if ( ! isset( $venue_ids[ $venue['id'] ] ) ) {
				$venue_ids[ $venue['id'] ] = true;
			} else {
				unset( $venues_list[ $i ] );
			}
		}
	}

	return array(
		'event_vendors' => $event_vendors,
		'venues_list' => array_values( $venues_list ),
	);
}