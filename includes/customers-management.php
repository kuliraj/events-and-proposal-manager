<?php
use Illuminate\Http\Request;
use Stemcounter\Event;
use Stemcounter\Vendor;
use Stemcounter\Customer;
use Stemcounter\Ajax_Response;
use Stemcounter\Humanreadable_Exception;
use Stemcounter\Meta;
use Stemcounter\Customer_Property;

function sc_render_manage_customers($content) {
	$customers = Customer::where(array(
		'user_id' => get_current_user_id()
	))->orderBy('last_name', 'ASC')->get();

	$table_len = get_user_meta( get_current_user_id(), 'sc_customer_table_len', true );
	$customer_properties = apply_filters( 'sc/react/data/customer', false, false );
	$js_date_format = sc_get_user_js_date_format( get_current_user_id() );

	ob_start(); ?>
	<div class="row">
			<?php if ($content) : ?>
				<div class="col-lg-12 order-message">
					<?php echo $content; ?>
				</div>
			<?php endif; ?>
		<!--
		<div class="col-lg-12 clearfix">
			<button class=" btn btn-primary btn-lg pull-right add-customer-button" style="margin-top: 10px; margin-bottom:10px;">New Customer</button>
		</div>
		-->
	</div><!-- /row -->

	<div class="customers-data">
		<div class="row mt">
			<div class="col-lg-12">
				<div class="content-panel">
					<section id="unseen">
						<table id="customersTable" class="table table-striped table-advance table-hover no-footer" data-page-length="<?php echo ! empty( $table_len ) ? $table_len : 25; ?>">
							<thead>
								<tr>
									<th colspan="7">
										<a class="add-customer-button cta-link">
											+ New Customer
										</a>
									</th>
								</tr>
								<tr>
									<th class="customer-td-first-name">First Name</th>
									<th class="hidden-xs customer-td-last-name">Last Name</th>
									<th class="hidden-xs customer-td-phone">Phone</th>
									<th class="hidden-xs customer-td-email">Email</th>
									<th class="hidden-xs customer-td-address">Address</th>
									<th class="hidden-xs customer-td-edit" style="width: 50px"></th>
									<th class="hidden-xs customer-td-delete" style="width: 50px"></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($customers as $customer): ?>
									<tr>
										<td class="customer-td-first-name"><span class="hidden-xs"><?php echo $customer->first_name; ?></span>
											<span class="visible-xs">
												<strong><?php echo $customer->first_name; ?> <?php echo $customer->last_name; ?><br /></strong>
												Phone: <?php echo $customer->phone; ?><br />
												Email: <?php echo $customer->email; ?> <br />
												Address: <?php echo $customer->address; ?><br />
												<a href="#" class="edit-customer-button" data-customer-id="<?php echo $customer->id; ?>">
													<i class="fa fa-pencil"></i>
												</a>
												<a href="#" class="delete-customer-button" data-customer-id="<?php echo $customer->id; ?>">
													<i class="fa fa-trash"></i>
												</a>
											</span>
										</td>
										<td class="hidden-xs customer-td-last-name"><?php echo $customer->last_name; ?></td>
										<td class="hidden-xs customer-td-phone"><?php echo $customer->phone; ?></td>
										<td class="hidden-xs customer-td-email"><?php echo $customer->email; ?></td>
										<td class="hidden-xs customer-td-address"><?php echo $customer->address; ?></td>
										<td class="hidden-xs order-td-edit" style="text-align: right;">
											<!--
											<button type="button" class="btn btn-primary btn-xs edit-customer-button" data-customer-id="<?php echo $customer->id; ?>"><i class="fa fa-pencil"></i></button> -->
											<a href="#" class="edit-customer-button" data-customer-id="<?php echo $customer->id; ?>">
												<i class="fa fa-pencil"></i>
											</a>
										</td>
										<td class="order-td-delete" style="text-align: right;">
											<!-- <button type="button" class="btn btn-danger btn-xs delete-customer-button" data-customer-id="<?php echo $customer->id; ?>"><i class="fa fa-trash"></i></button> -->
											<a href="#" class="delete-customer-button" data-customer-id="<?php echo $customer->id; ?>">
												<i class="fa fa-trash"></i>
											</a>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<!-- slide out properties panel -->
						<div class="customer-properties-sidebar container slide-out-sidebar">
							<a href="#" class="close-btn btn btn-primary button-submit">Close</a>

							<div  class="builder-form">
								<form id="edit-customer-form" class="form-horizontal style-form customer-form" method="post">
								</form>
							</div>

						</div>
					</section>
				</div><!-- /content-panel -->
			</div><!-- /col-lg-4 -->
		</div><!-- /row -->
	</div>
	<br/>
	<br/>
	<br/>
	<script type="text/javascript">
		(function($){
			$(document).ready(function(){
				$('#customersTable').dataTable({
					order: [[1, 'asc']],
					lengthMenu: [[25, 50, -1], [25, 50, 'All']],
					//"orderCellsTop": true
				});
			});

			$('.customer-properties-sidebar').on('click', '.close-btn', function(event) {
				event.preventDefault();
				$(this).closest('.customer-properties-sidebar').removeClass('visible');
				$('#customersTable tr.active').removeClass('active');
				ReactDOM.unmountComponentAtNode(document.getElementById('edit-customer-form'));
			});

			$('.add-customer-button').click(function(e) {
				e.preventDefault();
				$('.customer-properties-sidebar').addClass('visible');

				ReactDOM.render(
					React.createElement(CustomerModal, {
						layout: 'add-customer',
						properties: <?php echo json_encode( $customer_properties ); ?>,
						dateFormat: <?php echo json_encode( $js_date_format ); ?>		
					}),
					document.getElementById('edit-customer-form')
				);
			});

			$('.edit-customer-button').on('click', function(e) {
				e.preventDefault();
				var customerId = $(this).data('customerId');
				$('.customer-properties-sidebar').addClass('visible');
				$(this).closest('tr').addClass('active');

				$.ajax({
					type:"post",
					url: window.stemcounter.ajax_url,
					data: {
						action: 'sc_edit_user_customer_form',
						customer_id: customerId
					},
					success: function(response){
						stemcounter.JSONResponse(response, function(r) {
							var settings = r.payload.settings;
							ReactDOM.render(
								React.createElement(CustomerModal, {
									layout: settings.layout,
									eventId: settings.eventId,
									customer_id: settings.customerId,
									customer_first_name: settings.customerFirstName,
									customer_last_name: settings.customerLastName,
									customer_phone: settings.customerPhone,
									customer_email: settings.customerEmail,
									customer_source: settings.customerSource,
									customer_address: settings.customerAddress,
									customer_notes: settings.customerNotes,
									customerLinkedEvents: settings.customerLinkedEvents,
									createNewCustomerCaller: stemcounter.page.createNewCustomerCaller,
									editCustomerCaller: stemcounter.page.editCustomerCaller,
									noCustomerList: settings.noCustomerList,
									properties: settings.properties ? settings.properties : null,
									dateFormat: settings.dateFormat ? settings.dateFormat : null
								}),
								document.getElementById('edit-customer-form')
							);
						});
					}
				});
			});

			$('.delete-customer-button').click(function(e){
				e.preventDefault();
				var customerId = $(this).data('customerId');

				alertify.confirm('Are you sure you wish to delete this customer?', function () {
				    jQuery.ajax({
						type:"post",
						url: window.stemcounter.ajax_url,
						data: {
							action: 'sc_delete_user_customer',
							customer_id: customerId
						},
						success: function(response){
							stemcounter.JSONResponse(response, function(r) {
								if (r.success) document.location.reload();
							});
						}
					});
				});
			});

			$('#customersTable').on( 'length.dt', function ( e, settings, len ) {

				var url = window.stemcounter.aurl({ action: 'sc_datatables_len' }),
					data = {
						'meta_key': 'sc_customer_table_len',
						'meta_value': len
					};

				$.post(url, data, function (response) {
					stemcounter.JSONResponse(response, function (r) {
						if ( ! r.success ) {
							console.log( r );
						}
					});
				});
			});

		})(jQuery);
	</script>
	<?php
	return ob_get_clean();
}
add_filter( 'sc/app/content/customers', 'sc_render_manage_customers' );

function sc_ajax_add_user_customer() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$customer_first_name = $request->input('customer_first_name');
	$customer_last_name = $request->input('customer_last_name');
	$customer_email = $request->input('customer_email');
	$customer_phone = $request->input('customer_phone');
	$customer_address = $request->input('customer_address');
	$customer_notes = $request->input('customer_notes');
	$email_check = ! filter_var( $request->input( 'no_email_check' ), FILTER_VALIDATE_BOOLEAN) ;

	try {

		if (empty($customer_first_name)) {
			throw new Humanreadable_Exception('The First Name field is required.');
		}
		if (empty($customer_last_name)) {
			throw new Humanreadable_Exception('The Last Name field is required.');
		}

		if ( empty( $customer_email ) ) {
			$customer = new Customer;
		} else {
			$customer = Customer::firstOrNew(array(
				'user_id' 		=> get_current_user_id(),
				'email'			=> $customer_email
			));

			if ( ! is_null( $customer ) && $customer->exists ) {
				if ( $email_check ) {
					$r->add_payload( 'email_used', true );
					$r->fail( '<strong>' . $customer->name . '</strong> is already using the email <strong>' . esc_html( $customer_email ) . '</strong>. Do you want to update their record with the current data?' );
				} else {
					$customer_notes .= "\n\n--------\nData from before overriding customer information:";
					if ( trim( $customer->first_name ) ) {
						$customer_notes .= "\nFirst Name: {$customer->first_name}";
					}
					if ( trim( $customer->last_name ) ) {
						$customer_notes .= "\nLast Name: {$customer->last_name}";
					}
					if ( trim( $customer->phone ) ) {
						$customer_notes .= "\nPhone: {$customer->phone}";
					}
					if ( trim( $customer->address ) ) {
						$customer_notes .= "\nAddress: {$customer->address}";
					}
					if ( trim( $customer->notes ) ) {
						$customer_notes .= "\nNotes: \n{$customer->notes}";
					}
				}
			}
		}

		/*if (!is_null($customer) && $customer->exists()) {
			throw new Humanreadable_Exception('A Customer with this name already exists.');
		}*/
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	}

	try {
		$customer->user_id = get_current_user_id();
		$customer->first_name = $customer_first_name;
		$customer->last_name = $customer_last_name;
		$customer->email = $customer_email;
		$customer->phone = $customer_phone;
		$customer->address = is_null($customer_address) ? '' : $customer_address;
		$customer->notes = $customer_notes;
		$customer->save();

		do_action( 'sc/customer/save', $customer, $request );

		$r->add_payload('customer_id', $customer->id);
		$r->add_payload('customers_list', sc_get_user_customers_list());
	} catch (Exception $e) {
		$r->fail($e->getMessage());
	}

	try {
		$properties = $request->input( 'properties' );
		foreach ( $properties as $prop_key => $property ) {
			if ( ! empty( $property['values'] ) ) {
				foreach ( $property['values'] as $val_key => $value ) {
					if ( 0 < intval( $value['id'] ) ) {
						try {
							$customer_property = Customer_Property::where( array(
								'id' => intval( $value['id'] ),
							) )->first();
						} catch ( Exception $e ){
							$r->fail( $e->getMessage() );
						}
					} else {
						$customer_property = new Customer_Property();
					}

					$customer_property->customer_id = intval( $customer->id );
					$customer_property->property_id = intval( $property['id'] );
					$customer_property->value = $value['value'];
					$customer_property->save();

					$properties[ $prop_key ]['values'][ $val_key ]['id'] = $customer_property->id;
				}
				if ( 'multi-checkbox' == $property['type'] ) {
					try {
						$customer_properties = Customer_Property::where( array(
							'customer_id' => intval( $customer->id ),
							'property_id' => intval( $property['id'] ),
						) )->get();
						foreach ( $customer_properties as $_property ) {
							$customer_property_exists = wp_list_filter( $properties[ $prop_key ]['values'], array( 'id' => $_property->id ) );
							// Delete the question if it was not sent in the request
							if ( empty( $customer_property_exists ) ) {
								$_property->delete();
							}
						}
					} catch (Exception $e) {
						$r->fail( $e->getMessage() );
					}
				}
			}
		}
	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}

	$r->respond('Success');
}
add_action('wp_ajax_sc_add_user_customer', 'sc_ajax_add_user_customer');

function sc_ajax_edit_user_customer_form() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$customer = Customer::where(array(
			'id' => abs($request->input('customer_id')),
			'user_id' => get_current_user_id()
		))->withTrashed()->firstOrFail();

		$linked_customer_events = Meta::where(array(
			'type' => 'event',
			'meta_key' => 'linked_customer',
			'meta_value' => $customer->id
		))->get();

		$linked_events = array();
		foreach ($linked_customer_events as $event) {
			$event_info = Event::where(array(
				'id' => $event->type_id, //the event id
				'user_id' => get_current_user_id()
			))->first();

			$linked_events[] = array(
				'id'   => $event_info->id,
				'name' => $event_info->name,
				'link' => add_query_arg( 'eid', $event_info->id, sc_get_proposal_2_0_page_url() )
			);
		}
	} catch (Exception $e) {
		$r->fail('Something went wrong. Please try again.');
	}
	$js_date_format = sc_get_user_js_date_format( get_current_user_id() );

	$settings = array(
		'layout' =>  'edit-customer',
		'eventId' => $request->input('event_id'),
		'customerId' => $customer->id,
		'customerFirstName' => $customer->first_name,
		'customerLastName' => $customer->last_name,
		'customerPhone' => $customer->phone,
		'customerEmail' => $customer->email,
		'customerSource' => $customer->customerSource,
		'customerAddress' => $customer->address,
		'customerNotes' => $customer->notes,
		'customerLinkedEvents' => $linked_events,
		'noCustomerList' => $request->input( 'no_customer_list', false ) ? 1 : 0,
		'dateFormat'	=> $js_date_format,
	);
	$settings = apply_filters( 'sc/customer/edit/settings', $settings, $customer );
	$settings = apply_filters( 'sc/react/data/customer', $settings, $customer );

	$r->add_payload( 'settings', $settings );

	$r->respond();
}
add_action('wp_ajax_sc_edit_user_customer_form', 'sc_ajax_edit_user_customer_form');

function sc_ajax_edit_user_customer() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$customer_first_name = $request->input('customer_first_name');
	$customer_last_name = $request->input('customer_last_name');
	$customer_email = $request->input('customer_email');
	$customer_phone = $request->input('customer_phone');
	$customer_address = $request->input('customer_address');
	$customer_notes = $request->input('customer_notes');

	try {
		$customer = Customer::where('id', '!=', $request->input('customer_id'))
			->where('user_id', '=', get_current_user_id())
			->where('email', '!=', '')
			->where('email', '=', $customer_email)
		    ->first();

		if (empty($customer_first_name)) {
			throw new Humanreadable_Exception('The First Name field is required.');
		}
		if (empty($customer_last_name)) {
			throw new Humanreadable_Exception('The Last Name field is required.');
		}
		if ( ! empty( $customer_email ) && ! filter_var( $customer_email, FILTER_VALIDATE_EMAIL ) ) {
			throw new Humanreadable_Exception('Invalid Customer Email format.');
		}
		if (!is_null($customer) && $customer->exists()) {
			throw new Humanreadable_Exception('A Customer with this email already exists.');
		}
	} catch (Humanreadable_Exception $e) {
		$r->fail($e->getMessage());
	}

	try {
		$old_customer = Customer::where(array(
			'id' => abs($request->input('customer_id'))
		))->withTrashed()->firstOrFail();

		$old_customer->user_id = get_current_user_id();
		$old_customer->first_name = $customer_first_name;
		$old_customer->last_name = $customer_last_name;
		$old_customer->email = $customer_email;
		$old_customer->phone = $customer_phone;
		$old_customer->address = is_null($customer_address) ? '' : $customer_address;
		$old_customer->notes = $customer_notes;
		$old_customer->save();

		do_action( 'sc/customer/save', $old_customer, $request );

		if (!is_null($request->input('eventId'))) {
			$r->add_payload('customer_id', $old_customer->id);
			if ( $request->input( 'noCustomerList', false ) ) {
				$r->add_payload( 'customer', $old_customer->format_for_event() );
			} else {
				$r->add_payload('customers_list', sc_get_user_event_customers( $request->input( 'eventId' ) ) );
			}
		}
	} catch (Exception $e) {
		$r->fail($e->getMessage());
	}

	try {
		$properties = $request->input( 'properties' );
		foreach ( $properties as $prop_key => $property ) {
			if ( ! empty( $property['values'] ) ) {
				foreach ( $property['values'] as $val_key => $value ) {
					if ( 0 < intval( $value['id'] ) ) {
						try {
							$customer_property = Customer_Property::where( array(
								'id' => intval( $value['id'] ),
							) )->first();
						} catch ( Exception $e ){
							$r->fail( $e->getMessage() );
						}
					} else {
						$customer_property = new Customer_Property();
					}

					$customer_property->customer_id = intval( $old_customer->id );
					$customer_property->property_id = intval( $property['id'] );
					$customer_property->value = $value['value'];
					$customer_property->save();

					$properties[ $prop_key ]['values'][ $val_key ]['id'] = $customer_property->id;
				}
				if ( 'multi-checkbox' == $property['type'] ) {
					try {
						$customer_properties = Customer_Property::where( array(
							'customer_id' => intval( $old_customer->id ),
							'property_id' => intval( $property['id'] ),
						) )->get();
						foreach ( $customer_properties as $_property ) {
							$customer_property_exists = wp_list_filter( $properties[ $prop_key ]['values'], array( 'id' => $_property->id ) );
							// Delete the question if it was not sent in the request
							if ( empty( $customer_property_exists ) ) {
								$_property->delete();
							}
						}
					} catch (Exception $e) {
						$r->fail( $e->getMessage() );
					}
				}
			}
		}
	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}


	$r->respond('Success');
}
add_action('wp_ajax_sc_edit_user_customer', 'sc_ajax_edit_user_customer');


function sc_ajax_delete_user_customer() {
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$customer = Customer::where(array(
			'id' => $request->input('customer_id'),
			'user_id' => get_current_user_id()
		))->delete();
	} catch (Exception $e) {
		throw new Humanreadable_Exception('Could not find customer.');
	}

	$r->respond('Success');
}
add_action('wp_ajax_sc_delete_user_customer', 'sc_ajax_delete_user_customer');

function sc_get_event_customers( $event_id, $user_id = false ) {
	if ( ! $user_id ) {
		$event = Event::where( 'id', $event_id )->withTrashed()->first();
		if ( ! $event || ! $event->user_id ) {
			return array();
		}

		$user_id = $event->user_id;
	}

	$linked_customers = Meta::where( array(
		'type' => 'event',
		'type_id' => $event_id,
		'meta_key' => 'linked_customer',
	) )->with( array( 'customer' ) )->get();

	return $linked_customers->map(function($meta) {
		return $meta->customer ? $meta->customer->format_for_event() : array();
	})->toArray();
}

function sc_get_user_event_client($event_id, $include_id = false) {
	$customers_list = sc_get_user_event_customers($event_id);
	$linked_customer = Meta::where(array(
		'type' => 'event',
		'type_id' => $event_id,
		'meta_key' => 'linked_customer'
	))->first();

	try {
		$event = Event::where('id', $event_id)->first();

		$customer = Customer::where(array(
			'id' => abs($linked_customer->meta_value),
			'user_id' => $event->user_id
		))->withTrashed()->firstOrFail();

		$customer_id = $customer->id;

		$customer = array(
			'name'    => $customer->name,
			'address' => $customer->address,
			'email'   => $customer->email,
			'phone'   => $customer->phone
		);

		if ( $include_id ) {
			$customer['id'] = $customer_id;
		}
	} catch (Exception $e) {
		$customer = array();
	}

	return $customer;
}

function sc_get_user_event_customers($event_id) {
	//getting the event vendors
	$customers_list = array();

	$linked_event_customer = Meta::where( array(
		'type' => 'event',
		'type_id' => $event_id,
		'meta_key' => 'linked_customer'
	) )->first();

	if ( $linked_event_customer ) {
		$customer = Customer::where(array(
			'id' => $linked_event_customer->meta_value // customer id
		))->withTrashed()->first();

		if ( ! is_null( $customer->deleted_at ) ) {
			array_push( $customers_list, array(
				'id' => $customer->id,
				'first_name' => $customer->first_name,
				'last_name' => $customer->last_name,
				'address' => $customer->address,
				'email' => $customer->email,
				'phone' => $customer->phone
			) );
		}
	}

	$customer_list = array_merge($customers_list, sc_get_user_customers_list());

	return $customer_list;
}

function sc_get_user_customers_list() {
	//getting all of the users customers
	$customers_list = array();
	$has_template_customer = false;

	$user_customers = Customer::where(array(
		'user_id' => get_current_user_id()
	))->orderBy('first_name', 'asc')->get();
	//getting the venues for now

	foreach ( $user_customers as $customer ) {
		if ( 'Template' == $customer->first_name ) {
			$has_template_customer = true;
			break;
		}
	}
	// Create template customer if it doesn't exists
	if ( ! $has_template_customer ) {
		$new_customer = new Customer();
		$new_customer->user_id = get_current_user_id();
		$new_customer->first_name = 'Template';
		$new_customer->last_name = 'Customer';
		$new_customer->email = '';
		$new_customer->phone = '';
		$new_customer->address = '';
		$new_customer->notes = '';
		$new_customer->save();

		$customers_list[] = array(
			'id' => $new_customer->id,
			'first_name' => $new_customer->first_name,
			'last_name' => $new_customer->last_name,
			'address' => $new_customer->address,
			'email' => $new_customer->email,
			'phone' => $new_customer->phone
		);
	}

	foreach ( $user_customers as $customer ) {
		$customers_list[] = array(
			'id' => $customer->id,
			'first_name' => $customer->first_name,
			'last_name' => $customer->last_name,
			'address' => $customer->address,
			'email' => $customer->email,
			'phone' => $customer->phone
		);
	}

	return $customers_list;
}
