<?php
use Illuminate\Http\Request;
use Stemcounter\Ajax_Response;
use Stemcounter\Event;
use Stemcounter\Event_Version;
use Stemcounter\Tax_Rate;
use Stemcounter\Payment;

class SC_Quick_Books_Integration {
	public static function start() {
		static $started = false;
 
		if ( ! $started ) {
			self::add_filters();
 
			self::add_actions();
 
			$started = true;
		}
	}
 
	protected static function add_filters() {
		// Add all filters here

		add_filter( 'sc/react/data/proposal', array( __CLASS__, 'filter_react_proposal_data' ), 10, 3 );
	}

	public static function filter_react_proposal_data( $data, $event, $user_id ) {
		if ( self::can_access( 'iif_export', $user_id ) ) {
			$data['extensions']['iif'] = 1;
		}

		return $data;
	}
 
	protected static function add_actions() {
		// Add all actions here

		add_action( 'wp_ajax_sc/proposal/download_iif', array( __CLASS__, 'maybe_generate_proposal_iif' ), 10 );
		add_action( 'wp_ajax_sc/proposal/download_iif_precheck', array( __CLASS__, 'maybe_generate_proposal_iif_precheck' ), 10 );
	}

	public static function maybe_generate_proposal_iif_precheck() {
		$request = Request::capture();
		$r = new Ajax_Response();

		$event_id = $request->input( 'event_id' );
		try {
			$event = sc_user_can_edit_event( $event_id );
		} catch (Exception $e) {
			$r->fail( 'It appears that you are not allowed to do this. Are you still logged-in?' );
		}

		if ( ! self::can_access( 'iif_export', $event->user_id ) ) {
			$r->fail( 'You do not have access to this feature.' );
		}

		$version = Event_Version::where( array(
			'event_id' => $event_id,
		) )->orderBy( 'id', 'desc' )->first();

		if ( ! $version ) {
			$r->fail( 'You have to create a client version first.' );
		}

		$r->respond();
	}

	public static function fputiif( $handle, $data ) {
		fwrite( $handle, implode( "\t", $data ) . "\t\n" );
	}

	public static function maybe_generate_proposal_iif() {
		$request = Request::capture();
		$r = new Ajax_Response();

		$event_id = $request->input( 'event_id' );
		try {
			$event = sc_user_can_edit_event( $event_id );
		} catch (Exception $e) {
			$r->fail( 'It appears that you are not allowed to do this. Are you still logged-in?' );
		}

		if ( ! self::can_access( 'iif_export', $event->user_id ) ) {
			$r->fail( 'You do not have access to this feature.' );
		}

		$version = Event_Version::where( array(
			'event_id' => $event_id,
		) )->orderBy( 'id', 'desc' )->first();

		if ( ! $version ) {
			$r->fail( 'You have to create a client version first.' );
		}

		$event_data = json_decode( $version->event_data );
		$event_data->customer = ! empty( $event_data->customers ) ? $event_data->customers[0] : $event_data->customer;

		// Hacks around multi-tax rate support
		$tax_rate_value = 0;
		$tax_rate_total = 0;
		if ( ! empty( floatval( $event_data->eventCost->tax_rate_value ) ) ) {
			$tax_rate_value += floatval( $event_data->eventCost->tax_rate_value );
		}
		if ( ! empty( floatval( $event_data->eventCost->tax_rate_value2 ) ) ) {
			$tax_rate_value += floatval( $event_data->eventCost->tax_rate_value2 );
		}
		if ( ! empty( floatval( $event_data->eventCost->tax ) ) ) {
			$tax_rate_total += floatval( $event_data->eventCost->tax );
		}
		if ( ! empty( floatval( $event_data->eventCost->tax2 ) ) ) {
			$tax_rate_total += floatval( $event_data->eventCost->tax2 );
		}
		$event_data->eventCost->tax_rate_value = $tax_rate_value;
		$event_data->eventCost->tax = $tax_rate_total;

		$tax_name = 'Tax';
		if ( ! empty( $event_data->eventCost->tax_rate_id ) && ( $tax_rate = Tax_Rate::withTrashed()->where( 'id', $event_data->eventCost->tax_rate_id )->first() ) ) {
			$tax_name = array( $tax_rate->name );
		}
		if ( ! empty( $event_data->eventCost->tax_rate_id2 ) && ( $tax_rate = Tax_Rate::withTrashed()->where( 'id', $event_data->eventCost->tax_rate_id2 )->first() ) ) {
			if ( is_array( $tax_name ) ) {
				$tax_name[] = $tax_rate->name;
			} else {
				$tax_name = array( $tax_rate->name );
			}
		}
		if ( is_array( $tax_name ) ) {
			$tax_name = implode( ' / ', $tax_name );
		}

		$filename = 'proposal-' . sanitize_file_name( $event_data->event->name ) . '-quickbooks-export.iif';

		$cc_value = false;
		if ( ! empty( $event_data->event->separate_cc_fee ) ) {
			$cc_value = sc_format_price( $event_data->eventCost->total * ( floatval( $event_data->event->card ) / 100 ) );
		}

		header( 'Content-Type: text/iif' );
	    header( 'Content-Disposition: attachment; filename="' . $filename . '";' );
		$handle = fopen( 'php://output', 'w' );

		// Define the accounts
		self::fputiif( $handle, array( '!ACCNT', 'NAME', 'ACCNTTYPE', 'EXTRA' ) );
		self::fputiif( $handle, array( 'ACCNT', 'Accounts Receivable', 'AR', '' ) );
		self::fputiif( $handle, array( 'ACCNT', 'Income', 'INC', '' ) );
		self::fputiif( $handle, array( 'ACCNT', 'Sales Tax Payable', 'OCLIAB', 'SALESTAX' ) );

		// Define the customer
		self::fputiif( $handle, array( '!CUST', 'REFNUM', 'NAME', 'PHONE1', 'EMAIL' ) );
		self::fputiif( $handle, array( 'CUST', $event_data->customer->id, $event_data->customer->name, $event_data->customer->phone, $event_data->customer->email ) );

		// Define the invoice items
		self::fputiif( $handle, array( '!INVITEM', 'NAME', 'INVITEMTYPE', 'ACCNT', 'PRICE', 'COST', 'QNTY', 'TAXABLE', 'TAXVEND' ) );

		if ( ! empty( $event_data->event->arrangements ) ) {
			foreach ( $event_data->event->arrangements as $arrangement ) {
				if ( empty( $arrangement->include ) || ! empty( $arrangement->addon ) ) {
					continue;
				}

				$items_cost = 0;
				foreach ( $arrangement->items as $item ) {
					$items_cost += sc_format_price( $item->quantity * $item->cost );
				}
				$items_cost = $arrangement->quantity * $items_cost;

				self::fputiif(
					$handle,
					array(
						'INVITEM', // INVITEM
						$arrangement->name, // NAME
						'SERV', // INVITEMTYPE
						'Income', // ACCNT
						sc_format_price( $arrangement->subTotal ), // PRICE
						sc_format_price( $items_cost ), // COST
						$arrangement->quantity, // QNTY
						! empty( $arrangement->tax ) ? 'Y' : 'N', // TAXABLE
						'TaxAgencyVendor', // TAXVEND
					)
				);
			}
		}

		// Define the card fee as a separate line item
		if ( $cc_value ) {
			self::fputiif(
				$handle,
				array(
					'INVITEM', // INVITEM
					'Card Fee', // NAME
					'OTHC', // INVITEMTYPE
					'Income', // ACCNT
					$event_data->event->card . '%', // PRICE
					$cc_value, // COST
					1, // QNTY
					'N', // TAXABLE
					'TaxAgencyVendor', // TAXVEND
				)
			);
		}

		// Define the tax as a line item if there is one
		if ( ! empty( floatval( $event_data->eventCost->tax_rate_value ) ) ) {
			self::fputiif(
				$handle,
				array(
					'INVITEM', // INVITEM
					$tax_name, // NAME
					'COMPTAX', // INVITEMTYPE
					'Sales Tax Payable', // ACCNT
					$event_data->eventCost->tax_rate_value . '%', // PRICE
					0, // COST
					1, // QNTY
					'N', // TAXABLE
					'TaxAgencyVendor', // TAXVEND
				)
			);
		}

		self::fputiif(
			$handle,
			array(
				'INVITEM', // INVITEM
				'NOTAX', // NAME
				'COMPTAX', // INVITEMTYPE
				'Sales Tax Payable', // ACCNT
				'0%', // PRICE
				'0', // COST
				1, // QNTY
				'N', // TAXABLE
				'TaxAgencyVendor', // TAXVEND
			)
		);

		self::fputiif( $handle, array( '!VTYPE', 'NAME', 'REFNUM' ) );
		self::fputiif( $handle, array( 'VTYPE', 'Tax agency', $event->user_id ) );
		self::fputiif( $handle, array( '!VEND', 'NAME', 'REFNUM' ) );
		self::fputiif( $handle, array( 'VEND', 'TaxAgencyVendor', $event->user_id ) );

		// Define the Transactions items
		self::fputiif( $handle, array( '!TRNS', 'TRNSTYPE', 'DATE', 'ACCNT', 'NAME', 'ADDR1', 'AMOUNT', 'DOCNUM', 'MEMO', 'CLEAR', 'NAMEISTAXABLE' ) );
		self::fputiif( $handle, array( '!SPL', 'TRNSTYPE', 'DATE', 'ACCNT', 'INVITEM', 'NAME', 'AMOUNT', 'PRICE', 'DOCNUM', 'MEMO', 'CLEAR', 'EXTRA', 'QNTY', 'TAXABLE' ) );
		self::fputiif( $handle, array( '!ENDTRNS' ) );

		$complete_payments = wp_list_filter( $event_data->payments, array( 'status' => 'complete' ) );
		$all_paid = count( $event_data->payments ) == count( $complete_payments );

		$total_amount = $event_data->eventCost->total;
		if ( $cc_value ) {
			$total_amount += $cc_value;
		}

		self::fputiif(
			$handle,
			array(
				'TRNS', // TRNS
				'INVOICE', // TRNSTYPE
				date( 'n/d/Y', strtotime( $event_data->eventDate ) ), // DATE
				'Accounts Receivable', // ACCNT
				$event_data->customer->name, // NAME
				$event_data->customer->name, // ADDR1
				sc_format_price( $total_amount ), // AMOUNT
				'SC' . $event_data->event->id, // DOCNUM
				'', // MEMO
				$all_paid ? 'Y' : 'N', // CLEAR
				! empty( $event_data->eventCost->tax ) ? 'Y' : 'N', // NAMEISTAXABLE
			)
		);

		if ( ! empty( $event_data->event->arrangements ) ) {
			foreach ( $event_data->event->arrangements as $arrangement ) {
				if ( empty( $arrangement->include ) || ! empty( $arrangement->addon ) ) {
					continue;
				}

				self::fputiif(
					$handle,
					array(
						'SPL', // SPL
						'INVOICE', // TRNSTYPE
						date( 'n/d/Y', strtotime( $event_data->eventDate ) ), // DATE
						'Income', // ACCNT
						'SC Item', // INVITEM
						'', // NAME
						sc_format_price( -1 * $arrangement->subTotal ), // AMOUNT
						sc_format_price( $arrangement->subTotal / $arrangement->quantity ), // PRICE
						'', // DOCNUM
						$arrangement->name, // MEMO
						$all_paid ? 'Y' : 'N', // CLEAR
						'', // EXTRA
						-1 * $arrangement->quantity, // QNTY
						! empty( $arrangement->tax ) ? 'Y' : 'N', // TAXABLE
					)
				);
			}
		}

		if ( $cc_value ) {
			self::fputiif(
				$handle,
				array(
					'SPL', // SPL
					'INVOICE', // TRNSTYPE
					date( 'n/d/Y', strtotime( $event_data->eventDate ) ), // DATE
					'Income', // ACCNT
					'SC Item', // INVITEM
					'', // NAME
					sc_format_price( -1 * $cc_value ), // AMOUNT
					$cc_value, // PRICE
					'', // DOCNUM
					'Card Fee', // MEMO
					$all_paid ? 'Y' : 'N', // CLEAR
					'', // EXTRA
					-1, // QNTY
					'N', // TAXABLE
				)
			);
		}

		if ( ! empty( $event_data->eventCost->tax ) ) {
			self::fputiif(
				$handle,
				array(
					'SPL', // SPL
					'INVOICE', // TRNSTYPE
					date( 'n/d/Y', strtotime( $event_data->eventDate ) ), // DATE
					'Sales Tax Payable', // ACCNT
					$tax_name, // INVITEM
					'TaxAgencyVendor', // NAME
					sc_format_price( -1 * $event_data->eventCost->tax ), // AMOUNT
					'', // PRICE
					'', // DOCNUM
					'', // MEMO
					'N', // CLEAR
					'AUTOSTAX', // EXTRA
					'', // QNTY
					'N', // TAXABLE
				)
			);
		} else {
			// Set tax to 0 when there's no tax
			self::fputiif(
				$handle,
				array(
					'SPL', // SPL
					'INVOICE', // TRNSTYPE
					date( 'n/d/Y', strtotime( $event_data->eventDate ) ), // DATE
					'Sales Tax Payable', // ACCNT
					$tax_name, // INVITEM
					'TaxAgencyVendor', // NAME
					'0', // AMOUNT
					0, // PRICE
					'', // DOCNUM
					'', // MEMO
					'N', // CLEAR
					'AUTOSTAX', // EXTRA
					'', // QNTY
					'N', // TAXABLE
				)
			);
		}

		self::fputiif( $handle, array( 'ENDTRNS' ) );

		exit;
	}

	public static function can_access( $feature = 'iif_export', $user_id = false ) {
		return true;
	}
}
 
SC_Quick_Books_Integration::start();
