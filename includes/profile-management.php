<?php
use Illuminate\Http\Request;
use Stemcounter\Ajax_Response;
use Stemcounter\Tax_Rate;
use Stemcounter\Invoicing_Category;
use Stemcounter\Discount;
use Stemcounter\Delivery;
use Stemcounter\Email_Template;
use Stemcounter\Measuring_Unit;

/*Ajax Function for Edit Company Detail*/
function edit_company_detail(){
	global $wpdb;
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$user_id = get_current_user_id();
	$companyName = $request->input('companyName');
	$companyAddress = $request->input('companyAddress');
	$companyWebsite = $request->input('companyWebsite');
	$companyMobile = $request->input('companyMobile');
	// $color = $request->input('proposalColor');

	update_user_meta($user_id, 'company_name', $companyName);
	update_user_meta($user_id, 'company_address', $companyAddress);
	update_user_meta($user_id, 'company_website', $companyWebsite);
	// update_user_meta($user_id, 'proposal_color', $color);

	$additionalInfo['companyMobile'] = $companyMobile;
	$serializeAdditionalInfo = serialize($additionalInfo);
	$getAdditionalInfo = get_user_meta($user_id, 'additionalInfo', true);
	
	if (empty($getAdditionalInfo)) {
		add_user_meta($user_id, 'additionalInfo', $serializeAdditionalInfo);
	} else {
		update_user_meta($user_id, 'additionalInfo', $serializeAdditionalInfo);
	}

	$r->respond();
}
add_action('wp_ajax_edit_company_detail', 'edit_company_detail');

/*Ajax Function for Edit Invoice Detail*/
function edit_invoice(){
	global $wpdb;

	sc_clean_input_slashes();
	$request = Request::capture();

	$user_id = get_current_user_id();
	$hardgoodMultiple = abs(floatval($request->input('hardgoodMultiple')));
	$freshFlowerMultiple = abs(floatval($request->input('freshFlowerMultiple')));
	$chargeCardRate = abs(floatval($request->input('chargeCardRate')));
	$salesTax = abs(floatval($request->input('salesTax')));
	$dateFormat = $request->input('dateFormat');
	$prefCurrency = $request->input('prefCurrency');
	$pdf_paper_size = $request->input('pdf_paper_size');
	$prices_separator = $request->input('prices_separator');
	$labor = abs(floatval($request->input('labor')));
	
	update_user_meta($user_id, 'hardgood_multiple', $hardgoodMultiple);
	update_user_meta($user_id, 'fresh_flower_multiple', $freshFlowerMultiple);
	update_user_meta($user_id, '__charge_card_rate', $chargeCardRate);
	update_user_meta($user_id, '__labor', $labor);
	update_user_meta($user_id, '__date_format', $dateFormat);
	update_user_meta($user_id, '__pref_currency', $prefCurrency);
	update_user_meta($user_id, '_sc_pdf_settings', $pdf_paper_size);
	update_user_meta($user_id, '_sc_add_labor_costs', isset($_REQUEST['add_labor_costs']) ? 'y' : 'n');
	update_user_meta($user_id, '_sc_prices_separator', $prices_separator);

	$apply_labor_to = (empty($_REQUEST['apply_labor_to']) || !is_array($_REQUEST['apply_labor_to'])) ? array() : $_REQUEST['apply_labor_to'];
	$apply_labor_to = array_filter($apply_labor_to, function($item_type){
		$item_types = sc_get_mixed_arrangement_item_types();
		if (isset($item_types[$item_type])) {
			return true;
		}
		return false;
	});

	delete_user_meta($user_id, '_sc_apply_labor_to');
	foreach ($apply_labor_to as $item_type) {
		add_user_meta($user_id, '_sc_apply_labor_to', $item_type);
	}

	exit;
}
add_action( 'wp_ajax_edit_invoice', 'edit_invoice' );

function edit_invoicing_categories() {
	global $wpdb;

	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$category_name = $request->input('category_name');
		if (empty($category_name)) {
			$r->fail('Markup Profile Name field is required');
		}

		if ($request->input('category_id') == -1) {
			$invoice_category = Invoicing_Category::firstOrNew(array(
				'user_id' => get_current_user_id(),
				'name' => $category_name
			));
			if ($invoice_category->exists) {
				$r->fail('A category with this name already exists');
			}

			$message = 'Markup Profile Created';
		} else {
			$invoice_category = Invoicing_Category::where(array(
				'id' => $request->input('category_id'),
				'user_id' => get_current_user_id(),
			))->firstOrFail();

			$message = 'Markup Profile Edited';
		}

		$invoice_category->user_id = get_current_user_id();
		$invoice_category->name = $category_name;
		$invoice_category->hardgood_multiple = $request->input('hardgood_multiple');
		$invoice_category->fresh_flower_multiple = $request->input('fresh_flower_multiple');

		$labor_values = $request->input('labor');

		$invoice_category->global_labor_value = empty( $labor_values['globalLaborValue'] ) || is_null( $labor_values['globalLaborValue'] ) ? $labor_values['global_labor_value'] : $labor_values['globalLaborValue'];
		$invoice_category->flower_labor = $labor_values['flower'];
		$invoice_category->hardgood_labor = $labor_values['hardgood'];
		$invoice_category->base_price_labor = $labor_values['base_price'];
		$invoice_category->fee_labor = $labor_values['fee'];
		$invoice_category->rental_labor = $labor_values['rental'];

		$invoice_category->save();

		if ($request->input('category_id') == -1) {
			$r->add_payload('invoice_category_id', $invoice_category->id );
		}

		if ($request->input('default_category') == 'true') {
			update_user_meta(
				get_current_user_id(),
				'default_invoice_category_id',
				$invoice_category->id
			);
		}
		
	} catch (Exception $e) {
		$r->fail($e->getMessage());
	}

	$r->respond($message);
}
add_action('wp_ajax_edit_invoicing_categories', 'edit_invoicing_categories');

function delete_invoicing_categories() {
	global $wpdb;

	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	try {
		$category_id = $request->input('category_id');
		$invoice_category = Invoicing_Category::where(array(
			'id' => $category_id
		));

		$default_invoice_category_id = get_user_meta(
			get_current_user_id(),
			'default_invoice_category_id',
			true
		);

		if ($category_id == $default_invoice_category_id) {
			delete_user_meta(get_current_user_id(), 'default_invoice_category_id');
		}

		$invoice_category->delete();
	} catch (Exception $e) {
		$r->fail($e->getMessage());
	}

	$r->respond('Markup Profile Deleted');
}
add_action('wp_ajax_delete_invoicing_categories', 'delete_invoicing_categories');
/**
 * Gets the user's saved preferred currency
 * @param  int $user_id    The user ID
 * @return string          Returns the user's preferred currency
 */
function sc_get_user_saved_pref_currency($user_id) {
	$user_meta = get_user_meta($user_id, '__pref_currency', true);
	$pref_currency = empty($user_meta) ? 'USD' : $user_meta;

	return $pref_currency;
}

/**
 * Gets the user's saved date time format
 * @param  int $user_id    The user ID
 * @return string          Returns the user's preferred date time format
 */
function sc_get_user_saved_date_time_format($user_id) {
	$user_meta = get_user_meta($user_id, '__date_format', true);
	$date_format = empty($user_meta) ? 'm/d/Y' : $user_meta;

	return $date_format;
}

/**
 * Gets the country name for the user's saved date format
 * @param  int $user_id         The user ID
 * @return string|bool          Returns the country name US|EU or false if the date format does not matche
 */
function sc_get_user_date_time_country_type($user_id) {
	$date_format = sc_get_user_saved_date_time_format(get_current_user_id());
	$country_type = false;

	switch ($date_format) {
		case 'm/d/Y':
		case 'm.d.Y':
			$country_type = 'US';
			break;
		case 'd/m/Y':
		case 'd.m.Y':
			$country_type = 'EU';
			break;
	}

	return $country_type;
}

/**
 * Gets a date using the users preferred date time format
 * @param  int $user_id       The user ID 
 * @param  string|int $time   Date Time string or Timestamp
 * @param  boolean $timestamp Set this as true when passing a timestamp
 * @return string             Returns the formatted date
 */
function sc_get_user_date_time( $user_id, $time, $timestamp = false, $date_format = false ) {
	$date_format = $date_format ? $date_format : sc_get_user_saved_date_time_format( $user_id );
	$formatted_date = '';

	if ( $timestamp == true ) {
		$formatted_date = date( $date_format, $time );
	} else {
		$formatted_date = date( $date_format, strtotime( $time ) );
	}

	return $formatted_date;
}

/**
 * Gets the user membership level
 * @param  int $user_id    The user ID
 * @return string          Returns the current user membership level
 */
function sc_get_user_membership_level($user_id = null) {

	if (!empty($user_id)) {
		$role = get_user_field('s2member_access_label', $user_id);
	} else {
		$role = get_user_field('s2member_access_label');
	}

	return $role;
}

function sc_ajax_include_logo() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();
	update_user_meta(get_current_user_id(), '_sc_logo_on_proposal', ($request->input('include') == '1' ? '1' : '0') );
	$r->respond();
}
add_action('wp_ajax_sc_include_logo', 'sc_ajax_include_logo');

function sc_get_profile_logo_id($user_id) {
	$attachment_id = get_user_meta($user_id, '_sc_logo', true);
	return $attachment_id;
}

function sc_get_profile_photo_id($user_id) {
	$attachment_id = get_user_meta($user_id, '_sc_profile_photo', true);
	return $attachment_id;
}

function sc_get_profile_logo_on_proposal($user_id) {
	$include = get_user_meta($user_id, '_sc_logo_on_proposal', true);
	$include = ($include === '1' || $include === '') ? true : false;
	return $include;
}

function sc_edit_tax_rates(){
	global $wpdb;

	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	if ( 'proposal' == $request->input( 'layout' ) ) {
		$name = $request->input( 'name' );
		$value = floatval( $request->input( 'value' ) );

		$new_rate = new Tax_Rate();
		$new_rate->user_id = get_current_user_id();
		$new_rate->name = $name;
		$new_rate->value = $value;
		$new_rate->save();

		$r->add_payload( 'rate_id', $new_rate->id );
	} else {
		// Create a new collection
		$tax_rates = collect( Tax_Rate::where(array(
			'user_id' => get_current_user_id(),
		))->get() );

		$rates_to_delete = $tax_rates;

		$rates = (array) $request->input( 'rates', array() );

		$response = array();

		foreach ( $rates as $i => $rate ) {
			// If there is no $id, or a rate with that ID doesn't exist
			if ( empty( $rate['id'] ) || ! $tax_rates->contains( 'id', $rate['id'] ) ) {
				// Create the new Tax Rate
				$name = $rate['name'];
				$value = abs( floatval( $rate['value'] ) );

				$new_rate = new Tax_Rate();
				$new_rate->user_id = get_current_user_id();
				$new_rate->name = $rate['name'];
				$new_rate->value = abs( floatval( $rate['value'] ) );
				$new_rate->default = ! empty( $rate['default'] ) ? 1 : 0;
				$new_rate->default2 = ! empty( $rate['default2'] ) ? 1 : 0;
				$new_rate->save();

				$new_rate = $new_rate->toArray();
				$new_rate['key'] = intval( $rate['key'] );
				$response[] = $new_rate;
			} else {
				$tax_rate = $tax_rates->first( function ( $_rate ) use ( $rate ) {
					return $_rate->id == $rate['id'];
				} );
				$tax_rate->name = $rate['name'];
				$tax_rate->value = abs( floatval( $rate['value'] ) );
				$tax_rate->default = ! empty( $rate['default'] ) ? 1 : 0;
				$tax_rate->default2 = ! empty( $rate['default2'] ) ? 1 : 0;
				$tax_rate->save();

				$tax_rate = $tax_rate->toArray();
				$tax_rate['key'] = intval( $rate['key'] );
				$response[] = $tax_rate;

				$rates_to_delete = $rates_to_delete->reject( function ( $_rate ) use ( $rate ) {
					return $_rate->id == $rate['id'];
				} );
			}
		}

		foreach ( $rates_to_delete as $tax_rate ) {
			$tax_rate->delete();
		}

		$r->add_payload(
			'tax_rates',
			$response
		);
		
	}
	$r->respond( 'Tax rates saved.' );
}
add_action('wp_ajax_sc_edit_tax_rates', 'sc_edit_tax_rates');

/**
 * Checks and validates if two passwords match
 * @param  string $password         The initial password
 * @param  string $confirm_password The secondary password used to compare it to the 
 * original one
 * @return array                   Returns the errors
 */
function sc_validate_user_password($password = null, $confirm_password = null) {

	if ( !empty($password) ) {
		$errors = array();

		/* Removes spaces from the password */
		$password = preg_replace('/\s+/', '', $password);
		$confirm_password = preg_replace('/\s+/', '', $confirm_password);

		if (strlen($password) < 8) {
			$errors[] = 'Your password must consist of at least 8 characters.';
		}

		if ($password != $confirm_password) {
			$errors[] = 'Your passwords do not match.';
		}

		if (! preg_match('~[A-Z]~', $password)) {
			$errors[] = 'Your password must contain at least one uppercase character.';
		}

		if (! preg_match('~[a-z]~', $password)) {
			$errors[] = 'Your password must contain at least one lowercase character.';
		}

		if (! preg_match('~\d~', $password)) {
			$errors[] = 'Your password must contain at least one number.';
		}

		return $errors;
	}

	return false;
}

function sc_edit_user_password(){
	global $wpdb;

	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$password = $request->input('new_password', '');
	$confirm = $request->input('confirm_password', '');
	$errors = sc_validate_user_password($password, $confirm);

	if (!empty($errors)) {
		$r->fail(implode('<br>', $errors));
	}

	wp_update_user(array(
		'ID' => get_current_user_id(),
		'user_pass' => $password,
	));

	$r->respond('Password changed successfully.');
}
add_action('wp_ajax_sc_edit_user_password', 'sc_edit_user_password');

function sc_edit_discounts() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	// Create a new collection
	$user_discounts = collect( Discount::where(array(
		'user_id'=>get_current_user_id(),
	))->get() );

	$r_discounts = $request->input( 'discounts', array() );

	foreach ( $r_discounts as $disc_id => $discount ) {
		// If there is no $disc_id, or a Discount with that ID doesn't exist
		if ( ! $disc_id || ! $user_discounts->contains( 'id', $disc_id ) ) {
			// Create the new Discount
			$new_discount = new Discount();
			$new_discount->user_id = get_current_user_id();
			$new_discount->name = $discount['name'];
			$new_discount->value = abs( floatval( $discount['value'] ) );
			$new_discount->type = $discount['type'];
			$new_discount->save();

			if ( 'proposal' == $request->input( 'layout' ) ) {
				$r->add_payload( 'discount_id', $new_discount->id );
			}
		}
	}

	foreach ( $user_discounts as $user_discount ) {
		if ( ! isset( $r_discounts[ $user_discount->id ] ) ) {
			$user_discount->delete();
		} else {
			$user_discount->name = $r_discounts[ $user_discount->id ]['name'];
			$user_discount->value = abs( floatval( $r_discounts[ $user_discount->id ]['value'] ) );
			$user_discount->type = $r_discounts[ $user_discount->id ]['type'];
			$user_discount->save();
		}
	}

	
	if ( 'proposal' != $request->input( 'layout' ) ) {
		$r->add_payload( 'discounts', Discount::where(array(
				'user_id'=>get_current_user_id(),
			))->get()
		);
	}

	$r->respond( 'Discounts were saved.' );
}
add_action( 'wp_ajax_sc_edit_discounts', 'sc_edit_discounts', 10 );

function sc_edit_deliveries() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	// Create a new collection
	$user_deliveries = collect( Delivery::where(array(
		'user_id'=>get_current_user_id(),
	))->get() );

	$r_deliveries = $request->input( 'deliveries', array() );

	foreach ( $r_deliveries as $d_id => $delivery ) {
		// If there is no $d_id, or a Delivery with that ID doesn't exist
		if ( ! $d_id || ! $user_deliveries->contains( 'id', $d_id ) ) {
			// Create the new Tax Rate
			$new_delivery = new Delivery();
			$new_delivery->user_id = get_current_user_id();
			$new_delivery->name = $delivery['name'];
			$new_delivery->value = abs( floatval( $delivery['value'] ) );
			$new_delivery->type = $delivery['type'];
			$new_delivery->tax = (bool) $delivery['tax'];
			$new_delivery->default = (bool) $delivery['default_delivery'];
			$new_delivery->save();

			if ( 'proposal' == $request->input( 'layout' ) ) {
				$r->add_payload( 'delivery_id', $new_delivery->id );
			}
		}
	}

	foreach ( $user_deliveries as $user_delivery ) {
		if ( ! isset( $r_deliveries[ $user_delivery->id ] ) ) {
			$user_delivery->delete();
		} else {
			$user_delivery->name = $r_deliveries[ $user_delivery->id ]['name'];
			$user_delivery->value = abs( floatval( $r_deliveries[ $user_delivery->id ]['value'] ) );
			$user_delivery->type = $r_deliveries[ $user_delivery->id ]['type'];
			$user_delivery->tax = (bool) $r_deliveries[ $user_delivery->id ]['tax'];
			$user_delivery->default = (bool) $r_deliveries[ $user_delivery->id ]['default_delivery'];
			$user_delivery->save();
		}
	}

	if ( 'proposal' != $request->input( 'layout' ) ) {
		$r->add_payload( 'deliveries', Delivery::where(array(
				'user_id'=>get_current_user_id(),
			))->get()
		);
	}
	$r->respond( 'Deliveries were saved.' );
}
add_action( 'wp_ajax_sc_edit_deliveries', 'sc_edit_deliveries', 10 );

function sc_filter_user_avatar( $result, $id_or_email, $args ){
	$photo_id = false;

	if ( is_string( $id_or_email ) ) {
		$user = get_user_by( 'email', $id_or_email );
		$photo_id = $user ? get_user_meta( $user->ID, '_sc_profile_photo', true ) : false;
	} else if ( is_numeric( $id_or_email ) ) {
		$photo_id = get_user_meta( $id_or_email, '_sc_profile_photo', true );
	}

	if ( ! $photo_id ) {
		return $result;
	}
	
	// $image = array( 'http://....', width, height )
	$image = wp_get_attachment_image_src( $photo_id, array( $args['size'], $args['size'] ) );
	$class = array( 'avatar', 'avatar-' . (int) $args['size'], 'photo' );

	if ( ! $args['found_avatar'] || $args['force_default'] ) {
		$class[] = 'avatar-default';
	}

	if ( $args['class'] ) {
		if ( is_array( $args['class'] ) ) {
			$class = array_merge( $class, $args['class'] );
		} else {
			$class[] = $args['class'];
		}
	}

	$avatar = sprintf(
		"<img alt='%s' src='%s' class='%s' height='%d' width='%d' %s/>",
		esc_attr( $args['alt'] ),
		esc_url( $image[0] ),
		esc_attr( join( ' ', $class ) ),
		(int) $args['height'],
		(int) $args['width'],
		$args['extra_attr']
	);

	/**
	 * Filter the avatar to retrieve.
	 *
	 * @since 2.5.0
	 * @since 4.2.0 The `$args` parameter was added.
	 *
	 * @param string $avatar      &lt;img&gt; tag for the user's avatar.
	 * @param mixed  $id_or_email The Gravatar to retrieve. Accepts a user_id, gravatar md5 hash,
	 *                            user email, WP_User object, WP_Post object, or WP_Comment object.
	 * @param int    $size        Square avatar width and height in pixels to retrieve.
	 * @param string $alt         Alternative text to use in the avatar image tag.
	 *                                       Default empty.
	 * @param array  $args        Arguments passed to get_avatar_data(), after processing.
	 */
	return apply_filters( 'get_avatar', $avatar, $id_or_email, $args['size'], $args['default'], $args['alt'], $args );
}
add_filter( 'pre_get_avatar', 'sc_filter_user_avatar', 10, 3 );

function sc_edit_user_libraries() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$user_libraries = get_user_meta( get_current_user_id(), 'sc_user_libraries', true );
	$target_library = intval( $request->input( 'library_id' ) );
	$index = false;

	if ( ! intval( $request->input( 'include' ) ) ) {
		if ( ! empty( $user_libraries ) ) {
			$index = array_search( $target_library, $user_libraries );
			if ( false !== $index ) {
				unset( $user_libraries[ $index ] );
			}
		}
	} elseif ( intval( $request->input( 'include' ) ) ) {
		$user_libraries[] = $target_library;
	}

	update_user_meta( get_current_user_id(), 'sc_user_libraries', $user_libraries );

	$r->respond( 'Libraries were updated.' );
}
add_action( 'wp_ajax_sc_edit_user_libraries', 'sc_edit_user_libraries', 10 );

function sc_share_by_email(){
    $request = Request::capture();
	$r = new Ajax_Response();
	$email = $request->input( 'email' );
	$body = wpautop( strip_tags( stripslashes( $request->input( 'body' ) ) ) );
	$subject = strip_tags( stripslashes( $request->input( 'subject' ) ) );
	$current_user = wp_get_current_user();

	$body .= '<p><a target="_blank" href="http://info.stemcounter.com/wedding-florist-software-walk-through-0.2?utm_campaign=In-App%20user%20sharing&utm_source=' . 
	urlencode( $current_user->user_email ) . 
	'">Here is a video of how the software works.</a></p>';

	if ( filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
		$r->fail( 'No valid email address' );
	}

	$headers = array();
	$headers[] = 'From: "' . $current_user->display_name . '" <noreply@stemcounter.com>';
	$headers[] = 'Reply-To: "' . $current_user->display_name . '" <' . $current_user->user_email . '>';
	
	$to = sc_prepare_email_recipients_for_mandrill( $email );

	add_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );

	if ( wp_mail( $to, $subject, $body, $headers ) ) {
		$r->add_payload( 'message', 'Receipt was sent.' );
		$r->respond('Success');
	} else {
		$r->fail('There was error sending email');
	}
	remove_filter( 'wp_mail_content_type', 'sc_set_html_mail_content_type' );
	die();
}

add_action('wp_ajax_sc_share_by_email', 'sc_share_by_email');

function sc_get_visible_taxes_count( $user_id = FALSE ) {
	$user_id = $user_id ? $user_id : get_current_user_id();

	$visible_taxes = get_user_meta( $user_id, '_sc_visible_taxes', true );

	return $visible_taxes && 2 <= absint( $visible_taxes ) ? absint( $visible_taxes ) : 1;
}

function sc_update_profile_settings() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	if ( ! wp_verify_nonce( $request->input( 'nonce' ), 'sc/profile/settings/save' ) ) {
		$r->fail( 'Something went wrong. Please try refreshing the page.' );
	}

	$user_id = get_current_user_id();

	$user_libraries = array_map( 'absint', $request->input( 'user_libraries', array() ) );
	$user_units = $request->input( 'user_units', array() );
	if ( $user_units ) {
		$measuring_units = Measuring_Unit::orderBy( 'id', 'ASC' )->get();
		foreach ( $user_units as $i => $unit ) {
			if ( ! $measuring_units->where( 'id', $unit )->count() ) {
				unset( $user_units[ $i ] );
			}
		}
		$user_units = array_values( $user_units );
		update_user_meta( $user_id, 'sc_user_measuring_units', $user_units );
	}

	$additional_info = unserialize( get_user_meta( $user_id, 'additionalInfo', true ) );
	$additional_info = is_array( $additional_info ) ? $additional_info : array();
	$additional_info['companyMobile'] = $request->input( 'phone' );
	$additional_info = serialize( $additional_info );
	$visible_taxes = absint( $request->input( 'visible_taxes' ) );
	$visible_taxes = $visible_taxes && 2 == $visible_taxes ? 2 : 1;

	update_user_meta( $user_id, '_sc_logo', $request->input( 'logo_id' ) );
	update_user_meta( $user_id, '_sc_logo_on_proposal', intval( $request->input( 'logo_on_proposal', 0 ) ) );
	update_user_meta( $user_id, 'sc_user_libraries', $user_libraries );
	update_user_meta( $user_id, 'company_name', $request->input( 'company' ) );
	update_user_meta( $user_id, 'company_address', $request->input( 'address' ) );
	update_user_meta( $user_id, 'company_website', $request->input( 'website' ) );
	update_user_meta( $user_id, 'additionalInfo', $additional_info );
	update_user_meta( $user_id, '__charge_card_rate', abs( floatval( $request->input( 'card' ) ) ) );
	update_user_meta( $user_id, '__date_format', $request->input( 'date_format' ) );
	update_user_meta( $user_id, '__pref_currency', $request->input( 'pref_currency' ) );
	update_user_meta( $user_id, '_sc_pdf_settings', $request->input( 'pdf_sheet_size' ) );
	update_user_meta( $user_id, '_sc_visible_taxes', $visible_taxes );

	$r->respond();
}
add_action( 'wp_ajax_sc/profile/settings/save', 'sc_update_profile_settings', 10 );

function sc_edit_user_email_template() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	$template = $request->input( 'template' );
	
	try {
		$_template = Email_Template::firstOrNew( array( 'id' => intval( $template['id'] ), 'user_id' => get_current_user_id() ) );
		$_template->user_id = get_current_user_id();
		$_template->label = $template['label'];
		$_template->subject = $template['subject'];
		$_template->content = $template['content'];

		if ( SCAC::can_access( SCAC::STUDIO ) && intval( $template['default'] ) ) {
			try {
				$templates = Email_Template::where( array(
					'user_id' => get_current_user_id()
				) )->get();
				
				foreach ( $templates as $temp ) {
					$temp->default = 0;
					$temp->save();
				}

			} catch (Exception $e) {
				$r->fail( $e->getMessage() );
			}
		}

		$_template->default = intval( $template['default'] );
		$_template->save();


		if ( -1 == intval( $template['id'] ) ) {
			$r->add_payload( 'template_id', $_template->id );
		}
	} catch (Exception $e) {
		$r->fail( $e->getMessage() );
	}

	$r->respond();
}
add_action( 'wp_ajax_sc_edit_user_email_template', 'sc_edit_user_email_template', 10 );