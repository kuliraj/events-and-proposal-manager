<?php get_header('blog'); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row mt post-loop">
      <?php if (!is_home()) : ?>
        <?php the_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
      <?php endif; ?>

      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
          <?php sc_block('post-short'); ?>
        <?php endwhile; ?>

        <div class="pagination-nav">
          <?php posts_nav_link('&nbsp;&mdash;&nbsp;','&larr; Newer Posts','Older Posts &rarr;'); ?>
        </div>
      <?php else : ?>
        <article style="margin-top: 75px;">
          <header class="post-header">
            <h1 class="post-title">Oops</h1>
          </header><!-- .entry-header -->
          <div class="entry-content">
            <p style="text-align: center;">No posts found.</p>
          </div><!-- .entry-content -->
        </article>
      <?php endif; ?>
    </div>
  </section>
</section>

<?php get_footer( 'blog' ); ?>