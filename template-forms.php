<?php
/*
Template Name: User - Forms
*/
get_header(); ?>
	<section class="wrapper">
		<?php the_content(); ?>
	</section>
<?php get_footer(); ?>