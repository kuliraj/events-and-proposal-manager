<?php 
/*
Template Name: User - Order
*/

get_header(); ?>

<section class="wrapper">
	<?php while( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>
	<div id="feedback"></div>
</section><!--/wrapper -->

<?php
get_footer();