<?php
/*
Template Name: User - Recipes
*/

use Illuminate\Http\Request;
use Stemcounter\Flower;
use Stemcounter\Service;
use Stemcounter\Variation;
use Stemcounter\Recipe;
use Stemcounter\Recipe_Item;

get_header(); ?>

	<section class="wrapper">
		<?php the_content(); ?>
	</section><!--/wrapper -->

<?php get_footer(); ?>