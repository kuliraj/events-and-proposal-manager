<?php
$log_out_redirect = home_url('logon/');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php get_template_part('head'); ?>
</head>
<body <?php body_class(); ?> >
  <section id="container" >
  	<!--
  	*******************************
  	TOP BAR CONTENT & NOTIFICATIONS
  	*******************************
    -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation">&nbsp;</div>
      </div>
      <!--logo start-->
      <a href="<?php bloginfo('url'); ?>" class="logo"><b>StemCounter.com</b></a>
      <!--logo end-->
      <div class="top-menu">
        <div class="header-menus">
          <ul class="nav top-menu">
            <?php if(is_user_logged_in()) : ?>
              <li><a class="logout" href="<?php echo wp_logout_url($log_out_redirect); ?>">Logout</a></li>
            <?php else : ?>
              <li><a class="logout" href="<?php echo home_url('logon/'); ?>">Login</a></li>
              <li><a class="logout" href="<?php echo home_url('register/'); ?>">Register</a></li>
            <?php endif; ?>	
          </ul>
        </div>
      </div>
    </header>
    <!--header end-->

    <?php if ( is_page_template( 'template-details.php' ) || is_page_template( 'template-stemcounter.php' ) || is_page_template( 'template-shopping.php' ) || is_page_template( 'template-proposal.php' ) || is_page_template( 'template-proposal-2.0.php' ) ) : ?>
      <?php get_sidebar('event'); ?>
    <?php else : ?>
      <?php get_sidebar(); ?>
    <?php endif; ?>

    <!--main content start-->
    <section id="main-content" class="main-content">