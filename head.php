<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="StemCounter.com">
<meta name="keyword" content="StemCounter, Stem Count, Flowers, Wedding Flowers">
<meta name="keywords" content="Floral Software, Floral Design Software, Wedding Florist" />

<?php if (is_front_page()) : ?>
    <title>Floral Design Software for the Wedding Florist</title>
<?php else : ?>
    <title><?php wp_title(' ', true, 'right'); ?></title>
<?php endif; ?>

<link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/img/favicon.png">
<?php
$prices_separator = get_user_meta( get_current_user_id(), '_sc_prices_separator', true );

if ( empty( $prices_separator ) ) {
    $prices_separator = 'comma_dot';
}
?>
<script type="text/javascript">
window.stemcounter = {};
window.stemcounter.ajax_url = <?php echo json_encode(admin_url('admin-ajax.php')); ?>;
window.stemcounter.home_url = <?php echo json_encode(home_url('/')); ?>;
window.stemcounter.theme_url = <?php echo json_encode(get_bloginfo('template_directory')); ?>;
window.stemcounter.profile_url = <?php echo json_encode(sc_get_profile_page_url()); ?>;
window.stemcounter.login_url = <?php echo json_encode(home_url('logon/')); ?>;
window.stemcounter.page = {};
window.stemcounter.mixedArrangementItemTypes = <?php echo json_encode(sc_get_mixed_arrangement_item_types()); ?>;
window.stemcounter.invoiceSettings = <?php echo json_encode(sc_get_profile_settings()); ?>;
window.stemcounter.prices_separator = <?php echo json_encode($prices_separator); ?>;
window.stemcounter.currencySymbol = '<?php echo is_user_logged_in() ? sc_get_user_saved_pref_currency( get_current_user_id() ) : 'USD'; ?>';
window.stemcounter.user = <?php $current_user = wp_get_current_user();
    echo is_user_logged_in() ? json_encode( array(
    'user_email'  => $current_user->user_email,
    'display_name'=> $current_user->display_name,
    'id'=> $current_user->ID,
    ) ) : 'false' ?>;
</script>

<?php if (is_front_page() ||
          is_page_template('template-pricing.php') ||
          is_page_template('template-membership.php') ||
          get_post_type() == 'memberpressproduct' ) : ?>

    <link href="//fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/css/home-normalize.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/home-foundation.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/home-styles.css?ver=1.0.8" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/home-hover.css" rel="stylesheet" media="all">

    <!--[if lt IE 9]>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie8-grid-foundation-4.css">
    <![endif]-->
    <!--[if lt IE 8]>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/general_foundicons_ie7.css">
    <![endif]-->

<?php endif; ?>

<?php wp_head(); ?>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->