<?php 
if (is_user_logged_in()) {
	wp_redirect(sc_get_events_page_url());
	exit;
}

$redirect_to = sc_get_events_page_url();
if (isset($_GET['redirect_to'])) {
	$redirect_to = esc_js($_GET['redirect_to']);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php get_template_part('head'); ?>
  </head>
  <body>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login" onsubmit="return custom_login_function();">
		        <h2 class="form-login-heading"><!-- sign in now --></h2>
		        <h3 class="message-redirect"><?php

		        	if ( isset( $_GET['already_registered'] ) ) {
						echo 'The account ' . esc_html( $_GET['already_registered'] ) . ' is already registered.';
						$wpUser = esc_attr( $_GET['already_registered'] );
					}

					if ( isset( $_GET['new_registration'] ) ) {
						echo 'The account registered succesfuly. Please check your email ' .  esc_html( $_GET['new_registration'] ) . ' for password details';
						$wpUser = esc_attr( $_GET['new_registration'] );
					}

					if ( isset( $_GET['autoregister_error'] ) ) {
						echo 'There was error in registration process: ' .  esc_html( $_GET['autoregister_error'] );
					}
					

		        ?></h3>
		        <div class="login-wrap">
		            <input type="text" id="wpUser" class="form-control" placeholder="User ID" autofocus<?php
		            	echo ( isset( $wpUser ) ) ? " value=\"{$wpUser}\"" : ''; ?>>
		            <br>
		            <input type="password" id="wpPass" class="form-control" placeholder="Password">
		            <label class="checkbox">
		                <span class="pull-right">
		                <a data-toggle="modal" href=" <?php echo wp_lostpassword_url(get_permalink()); ?> "> Forgot Password?</a>
		                </span>
		            </label>
		            <?php wp_nonce_field('ajax-login-nonce', 'security' ); ?>
		            <br /><br />
		            <button class="btn btn-theme btn-block" href="index.html" type="submit"><!-- <i class="fa fa-lock"></i>-->SIGN IN</button>
		            <div class="log_ajax_resp"></div>
		            <!-- <hr> -->
		            <!-- <div class="login-social-link centered">
		            <p>or you can sign in via your social network</p>
		                <button class="btn btn-facebook" type="submit"><i class="fa fa-facebook"></i> Facebook</button>
		                <button class="btn btn-twitter" type="submit"><i class="fa fa-twitter"></i> Twitter</button>
		            </div> -->
		            <div class="registration">Don't have an account yet? 
		            <a class="" href="<?php bloginfo('url'); ?>/register/">Create an account</a>
		            </div>
		
		        </div>
		      </form>	  	
	  	</div>
	  </div>
	  
	  <!-- Modal -->
		       <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Forgot Password?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Enter your e-mail address below to reset your password.</p>
		                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
		                      </div>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                          <button class="btn btn-theme" type="button">Submit</button>
		                      </div>
		                  </div>
		              </div>
		       </div>
	<!-- modal -->
		          
    <!-- js placed at the end of the document so the pages load faster -->
    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.backstretch.min.js"></script>
    <script>
        jQuery.backstretch("<?php bloginfo('template_url'); ?>/img/login-bg.jpg", {speed: 500});
    
    function custom_login_function(){
    	var user = $("#wpUser").val();
    	var pass = $("#wpPass").val();
    	var security = $("#security").val();
    	if(user == "" || pass == ""){
    		alert("Please Enter your user name and password");
    	}else{
			jQuery.ajax({
			type:"post",
			url: window.stemcounter.ajax_url,
			data: {action: 'check_wp_login', user:user, pass:pass, security:security},
			success:function(edata){
				if(edata == 0){
					jQuery(".log_ajax_resp").html("<span class='red'>Please enter correct user name and password.</span>");
				}else{
					jQuery(".log_ajax_resp").html("<span class='green'>Login Sucessfully....</span>");
					window.setTimeout(function(){
						window.location.href = <?php echo json_encode($redirect_to); ?>;
					}, 1500);
				}
			}
			});
    	}
    	return false;
    }    
    </script>
    
  </body>
</html>
