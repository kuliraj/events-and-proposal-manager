<?php get_header(); ?>

  <!-- **********************************************************************************************************************************************************
  MAIN CONTENT
  *********************************************************************************************************************************************************** -->

	<div>
		<h1>OOPS</h1>
		<h2>... this page doesn't exist.</h2>

		<?php if ( is_user_logged_in() ) : ?>
			<section class="wrapper">
				<?php
				wp_nav_menu( array(
					'theme_location'	=> 'main-menu',
					'container' 		=> false,
					'menu_class'		=> 'sidebar-menu',
					'menu_id'			=> 'nav-accordion',
					'depth'     		=> 1,
					'link_before'  		=> '<span>',
					'link_after'   		=> '</span>'
					) );
				?>
			</section><!-- /wrapper -->
		<?php endif; ?>
	</div>

<?php get_footer(); ?>