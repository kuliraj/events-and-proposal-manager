<?php
/*
Template Name: User - Membership
*/
get_header( 'secondary-home' );
the_post(); ?>
<section id="main-content" class="membership-content">
	<section class="membership-container container">
		<?php the_content(); ?>
	</section>
</section>

<?php get_footer( 'blog' ); ?>

