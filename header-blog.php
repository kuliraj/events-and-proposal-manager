<?php
$body_classes = array();
if (is_search() || is_archive() || is_singular('post')) {
  $body_classes[] = 'blog';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php get_template_part('head'); ?>
</head>
<body <?php body_class(implode(' ', $body_classes)); ?> >
  <section id="container" >
  	<!--
  	*******************************
  	TOP BAR CONTENT & NOTIFICATIONS
  	*******************************
    -->
    <!--header start-->
    <header class="header">
      <div class="header-wrap">
        <a href="<?php echo home_url('/'); ?>" class="logo">
          <img src="<?php bloginfo('template_directory'); ?>/img/Logo_StemCounter.com_.png" class="logo hvr-shrink" style="width: 200px;" />
        </a>
        <div class="header-menus">
          <div class="blog-header clearfix">
            <?php wp_nav_menu('theme_location=blog-header&container=&fallback_cb=&depth=2'); ?>
            <ul>
              <li>
                <form action="<?php echo home_url('/'); ?>" method="get">
                  <div class="dashicons dashicons-search search-icon"></div>
                  <input type="text" name="s" value="<?php echo esc_attr(get_query_var('s')); ?>" placeholder="Search blog" class="search-input" />
                </form>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
    <!--header end-->