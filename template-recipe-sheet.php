<?php 
/*
Template Name: Event - Recipe Sheet
*/

use Stemcounter\Event;
use Stemcounter\Vendor;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Item;
use Stemcounter\Arrangement_Photo;

if ( ! isset( $_GET['eid'] ) ) {
	echo 'Event was not found.';
	die;
}

$event = Event::where(array(
	'id' => $_GET['eid'],
))->with(array(
	'arrangements' => function($query) {
		$query->where('include', '=', 1)->where('addon', '=', 0)->orderBy( 'order', 'asc' );
	},
), 'arrangements.items', 'arrangements.items.item_variation' )->firstOrFail();

$event->arrangements->sortBy( function($arrangement){
	return $arrangement->order;
} );

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php wp_title(' ', true, 'right'); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
<section id="container">
	<section id="main-content" class="main-content">
		<section class="wrapper">
			<div class="proposal-container new-proposal-container container">
				<div id="event-proposal">
					<div class="proposal-wrapper">
						<div class="proposal-toolbar">
							<div class="row">
								<div class="col-sm-12">
									<div class="pull-left event-breadcrumbs">
										<a href="<?php echo esc_url( sc_get_permalink_by_template( 'template-events.php' ) ); ?>" class="events-link">Events</a> > <span class="event-name"><?php echo $event->name; ?></span>
									</div>
									<div class="toolbar-action pull-right"><a href="<?php echo esc_url( add_query_arg( 'eid', $event->id, sc_get_proposal_2_0_page_url() ) ); ?>">Back to the proposal</a></div>
								</div>
							</div>
						</div>
						<div class="container recipe-sheet-container">
							<div class="recipe-sheet-settings">
								<label disabled><input id="arr-item-photo" type="checkbox" name="large-photos">Item Photos</label>
								<label disabled><input id="arr-recipe-photo" type="checkbox" name="large-recipe-photos">Recipe Photos</label>
								<label disabled><input id="arr-prices" type="checkbox" name="prices">Costs</label>
								<label disabled><input id="arr-florals" type="checkbox" name="florals-only">Items with Florals</label>
								<label disabled><input id="arr-notes" type="checkbox" name="notes">Notes</label>
							</div>
							<?php foreach ( $event->arrangements as $arr_key => $arrangement ) :
								if ( ! $arrangement->include ) {
									continue;
								}
								$ii = 1; ?>
								<div class="recipe-sheet-arrangements has-price clearfix">
									<div class="recipe-sheet-table">
									<?php foreach ( $arrangement->recipes as $key => $recipe ) : ?>
											<div class="recipe-sheet-row">
												<span class="arr-price">
													<?php echo sc_format_price( $arrangement->get_cost( $recipe->id ), true ); ?>
												</span>
												<span class="arr-quantity">
													<?php echo $arrangement->quantity; ?>
												</span>
												<span class="arr-caption">
													<?php echo $arrangement->name ? esc_html( $arrangement->name) : 'Untitled'; ?>
													-
													<?php echo $recipe->name ? esc_html( $recipe->name) : 'Untitled recipe'; ?>
												</span>
											</div>
												<?php foreach ( $arrangement->items as $arrangement_item ) {
													if ( $recipe->id == $arrangement_item->arrangement_recipe_id ) { ?>
														<div class="recipe-sheet-row arr-item arr-item-<?php echo sc_get_item_type( $arrangement_item->item, false, $arrangement_item ); ?>">
															<span class="arr-item-cost"><?php echo sc_format_price( $arrangement_item->cost, true ); ?></span>
															<span class="arr-item-quantity"><?php echo $arrangement_item->quantity; ?></span>
															<span class="arr-item-name"><?php echo $arrangement_item->get_full_name(); ?></span>
														</div>												
													<?php }
												}
												$ii++;
											endforeach; ?>
										</div>

										<div class="recipe-sheet-photos clearfix photos">
											<?php foreach ( $arrangement->photos as $photo_index => $photo ) {
												if ( $photo->photo_id ) {
													$photo_src = wp_get_attachment_image_src( $photo->photo_id, 'large' );
													echo '<div class="arr-photo arr-recipe-photo arr-photo-' . esc_attr( $photo_index + 1 ) . '"><div class="img" style="background-image: url(\'' . esc_url( $photo_src[0] ) . '\');"></div></div>';
												}
											}
											foreach ( $arrangement->items as $key => $arrangement_item ) {
												$attachment_id = null;
												foreach ( $arrangement_item->item->variations as $variation ) {
													if ( $arrangement_item->item_variation_id == $variation->id ) {
														if ( null != $variation->attachment_id ) {
															$attachment_id = $variation->attachment_id;
															break;
														} else {
															$attachment_id = $arrangement_item->item->variations[0]->attachment_id;
															break;
														}
													}
												}

												if ( $attachment_id ) {
														$photo_src = wp_get_attachment_image_src( $attachment_id, 'large' );
														echo '<div class="arr-photo arr-item-photo arr-item-' . esc_attr( $key + 1 ) . '"><div class="img" style="background-image: url(\'' . esc_url( $photo_src[0] ) . '\');"></div></div>';

														} ?>
											<?php } ?>
										</div>
										<div class="recipe-sheet-description clearfix">
											<?php echo $arrangement->note; ?>
										</div>
									</div>
								<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</section>
</section>



<?php wp_footer(); ?>
<script type="text/javascript">
(function($){
	$( document ).ready(function() {
		$("#arr-item-photo").click( function(){
			if( $(this).is(':checked') ) {
				if ( ! $('.recipe-sheet-arrangements').hasClass('has-photos') ) {
					$('.recipe-sheet-arrangements').addClass('has-photos');
					$('.recipe-sheet-arrangements').each(function(){
						// $(this).height($(this).innerHeight() - 20);
					});
				}
			} else {
				if ( $('.recipe-sheet-arrangements').hasClass('has-photos') ) {
					$('.recipe-sheet-arrangements').removeClass('has-photos');
					$('.recipe-sheet-arrangements').each(function(){
						// $(this).css('height', 'auto');
					});
				}
			}
		});

		$("#arr-recipe-photo").click( function(){
			if( $(this).is(':checked') ) {
				if ( ! $('.recipe-sheet-arrangements').hasClass('has-recipe-photos') ) {
					$('.recipe-sheet-arrangements').addClass('has-recipe-photos');
					$('.recipe-sheet-arrangements').each(function(){
					});
				}
			} else {
				if ( $('.recipe-sheet-arrangements').hasClass('has-recipe-photos') ) {
					$('.recipe-sheet-arrangements').removeClass('has-recipe-photos');
					$('.recipe-sheet-arrangements').each(function(){
					});
				}
			}
		});

		$("#arr-notes").click( function(){
			if( $(this).is(':checked') ) {
				if ( ! $('.recipe-sheet-arrangements').hasClass('has-note') ) {
					$('.recipe-sheet-arrangements').addClass('has-note');
				}
			} else {
				if ( $('.recipe-sheet-arrangements').hasClass('has-note') ) {
					$('.recipe-sheet-arrangements').removeClass('has-note');
				}
			}
		});

		$("#arr-prices").click( function(){
			if( $(this).is(':checked') ) {
				if ( ! $('.recipe-sheet-arrangements').hasClass('with-costs') ) {
					$('.recipe-sheet-arrangements').addClass('with-costs');
				}
			} else {
				if ( $('.recipe-sheet-arrangements').hasClass('with-costs') ) {
					$('.recipe-sheet-arrangements').removeClass('with-costs');
				}
			}
		});

		$('#arr-florals').on('change', function(){
			var $arrangements = $('.recipe-sheet-arrangements');

			if( $(this).is(':checked') ) {
				$arrangements.each(function(index, el) {
					if ( ! $(this).find('.arr-item-fresh_flower').length ) {
						$(this).addClass('without-florals')
					}
				});
			} else {
				if ( $arrangements.hasClass('without-florals') ) {
					$arrangements.removeClass('without-florals');
				}
			}
		});
	});
})(jQuery);	
</script>

</body>

</html>

