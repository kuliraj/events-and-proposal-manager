<?php get_header('blog'); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row mt post-loop">
      <?php while (have_posts()) : the_post(); ?>

        <?php sc_block('post-detailed'); ?>

        <?php sc_block('related-posts'); ?>

        <div class="post-comments-wrap">
			<?php comments_template(); ?>
		</div>

      <?php endwhile; ?>
    </div>
  </section>
</section>

<?php get_footer('blog'); ?>