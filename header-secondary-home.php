<!DOCTYPE html PUBLIC>
<!--[if gt IE 8]>
<!-->
<html class="no-js" lang="en" >
<!--<![endif]-->
<head>
  <?php get_template_part( 'head' ); ?>
</head>
<body <?php body_class( 'secondary-home' ); ?>>
  <!-- Begin Menu Bar -->
  <div class="full-row clearfix secondary-header">
    <div class="header-wrap">
      <a href="<?php echo home_url('/'); ?>">
        <img src="<?php bloginfo('template_directory'); ?>/img/Logo_StemCounter.com_.png" class="logo hvr-shrink" style="width: 200px;" />
      </a>
    </div>
    <div class="home-header-cta">
      <ul class="menu">
        <li>
          <a class="button" href="<?php echo home_url('register/'); ?>">Register</a>
        </li>
      </ul>
      <?php wp_nav_menu('theme_location=home-header&container=&fallback_cb=&depth=1') ?>
    </div>
  </div>
  <!-- End Top Navigation Bar -->