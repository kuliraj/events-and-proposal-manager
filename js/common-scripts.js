if(/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream){
    document.querySelector('meta[name=viewport]')
    .setAttribute(
        'content',
        'initial-scale=1.0001, minimum-scale=1.0001, maximum-scale=1.0001, user-scalable=no'
    );
}

(function($){
    $(document).ready(function(){
        if ( window.alertify ) {
            window.alertify.defaults.notifier.position = 'top-right';
        }

        $('.sc-openvoyce-trigger').on('click', function(e){
            e.preventDefault();
            $('#openvoyce-trigger').trigger('click');
        })

        /*---LEFT BAR ACCORDION----*/
        if ( $.fn.dcAccordion ) {
            $('#nav-accordion').dcAccordion({
                eventType: 'click',
                autoClose: true,
                saveState: true,
                disableLink: true,
                speed: 'slow',
                showCount: false,
                autoExpand: true,
                // cookie: 'dcjq-accordion-1',
                classExpand: 'dcjq-current-parent'
            });
        }

        /**
         * Fixes the Select2 And Bootstrap Modal Focus bug for Firefox
         */
         if ( $.fn.modal ) {
            $.fn.modal.Constructor.prototype.enforceFocus = function() {};
        }

       /* $('.fa-bars').click(function () {
            if ($('#sidebar > ul').is(":visible") === true) {
                $('#main-content').css({
                    'margin-left': '0px'
                });
                $('#sidebar').css({
                    'margin-left': '-210px'
                });
                $('#sidebar > ul').hide();
                $("#container").addClass("sidebar-closed");
            } else {
                $('#main-content').css({
                    'margin-left': '210px'
                });
                $('#sidebar > ul').show();
                $('#sidebar').css({
                    'margin-left': '0'
                });
                $("#container").removeClass("sidebar-closed");
            }
        });*/

        if ( $.fn.dataTable ) {
            $.extend(true, $.fn.dataTable.defaults, {
                "sDom":
                    "<'row'<'col-xs-12'<'col-xs-4'l><'col-xs-8'f>r>>"+
                    "t"+
                    "<'row'<'col-xs-12'<'col-md-6'i><'col-md-6'p>>>",
                "oLanguage": {
                    "sLengthMenu": "Show: _MENU_"
                }
            });
        }

        if ( $('.events-select-group').length ) {
            $('.events-select-group').select2();

            $('#eventsTable_wrapper').on('change', '.events-select-group', function(event) {
                event.preventDefault();
            
                var state = $(this).val(),
                    event_id = $(this).closest('.events-select-group').data('eventId');

                $.ajax({
                    url: stemcounter.ajax_url,
                    dataType: 'json',
                    data: {
                        'action':'sc_event_state',
                        'state' : state,
                        'event_id' : event_id
                    },
                    success:function(data) {
                        if ( data.success ) {
                            if ( data.message ) {
                                alertify.success( data.message );
                            }

                            window.location.reload(); 
                        } else {
                            if ( data.message ) {
                                alertify.error( data.message );
                            }
                        }
                    },
                    error: function(errorThrown){
                        console.log(errorThrown);
                    }
                });  
        	});
        }

        /* One page scroll */
        var lastId,
            topMenu = $('#menu-front-page, #menu-footer-home'),
            topMenuHeight = topMenu.outerHeight()+25,
            // All list items
            menuItems = topMenu.find("a"),
            // Anchors corresponding to menu items
            scrollItems = menuItems.map(function(){
              var item = $(this).attr("href");
              if ( 0 === item.indexOf('#') ) {
                item = $(item);
                if (item.length) {
                    return item;
                }
              }
            });
            
        // Bind click handler to menu items
        // so we can get a fancy scroll animation
        menuItems.click(function(e){
            var href = $(this).attr("href"),
              offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
            $('html, body').stop().animate({ 
              scrollTop: offsetTop
            }, 300);
            e.preventDefault();
        });
        
        // Bind to scroll
        $(window).scroll(function () {
            // Get container scroll position
            var fromTop = $(this).scrollTop()+topMenuHeight;

            // Get id of current scroll item
            var cur = scrollItems.map(function(){
                if ( $(this).offset().top < fromTop ) {
                    return this;
                }
            });

            // Get the id of the current element
            cur = cur[cur.length-1];
            var id = cur && cur.length ? cur[0].id : "";
           
            if (lastId !== id) {
                lastId = id;
                // Set/remove current class
                menuItems
                    .parent().removeClass("current")
                    .end().filter('[href="#' +id+ '"]').parent().addClass("current");
            }

            /* Login / Get Started menu item */
            var target = 0;
            var timeout = null;
            if ( $(".intro-section").length ) {
                target = $(".intro-section").offset().top;
                if (!timeout) {
                    timeout = setTimeout(function () {
                    clearTimeout(timeout);
                    timeout = null;
                        if ($(window).scrollTop() >= target) {
                            $('.home-header-cta .js-login').css('display', 'none');
                            $('.home-header-cta .inpage-login').removeClass('hidden');
                            $('.home-header-cta .inpage-login a').addClass('button').css({
                                height: '34px',
                                fontSize: '0.9em',
                                'padding-top': '9px'

                            });
                        } else {
                            $('.home-header-cta .js-login').css('display', 'block');
                            $('.home-header-cta .inpage-login').addClass('hidden');
                        }
                    }, 250);
                }
            }
        });

        if ( $('.public-proposal').length ) {
            $('.public-proposal #view-proposal').on('click', function(event) {
                event.preventDefault();
                $(this).select();
                clip($('#view-proposal').val() );
            });

            function clip(text) {
                var copyElement = document.createElement('input');
                copyElement.setAttribute('type', 'text');
                copyElement.setAttribute('value', text);
                copyElement = document.body.appendChild(copyElement);
                
                try {
                    if(!document.execCommand('copy')) {
                        throw 'Not allowed.';
                    }
                    $('.public-proposal .show-msg').fadeIn('slow').css('display','block');
                } catch(e) {
                    copyElement.remove();
                    console.log("document.execCommand('copy'); is not supported");
                    $('.public-proposal .show-msg').text('Selected');
                    $('.public-proposal .show-msg').fadeIn('slow').css('display','block');
                } finally {
                    if (typeof e == 'undefined') {
                        copyElement.remove();
                    }
                }
                setTimeout( function(){
                    $('.show-msg').fadeOut('slow');
                }, 2500);
            }
        }

        //Profile tabs
        $('#myTabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        })

        var $selector = $('table#eventsTable, table#listItems, table#customersTable, table#vendorsTable, table#ordersTable');
        //console.log($selector.width());
        $selector.css('width', '');
        $('.table th').css('width', '');
    });
        

    window.Script = function () {
        //    sidebar dropdown menu auto scrolling
        jQuery('#sidebar .sub-menu > a').click(function () {
            var o = ($(this).offset());
            diff = 250 - o.top;
            if(diff>0)
                $("#sidebar").scrollTo("-="+Math.abs(diff),500);
            else
                $("#sidebar").scrollTo("+="+Math.abs(diff),500);
        });

        //    sidebar toggle
        $(function() {
            function responsiveView() {
                var wSize = $(window).width();
                if (wSize <= 768) {
                    $('#container').addClass('sidebar-close');
                    //$('#sidebar > ul').hide();
                }

                if (wSize > 768) {
                    $('#container').removeClass('sidebar-close');
                    //$('#sidebar > ul').show();
                }
            }
            $(window).on('load', responsiveView);
            $(window).on('resize', responsiveView);
        });

        // widget tools
        jQuery('.panel .tools .fa-chevron-down').click(function () {
            var el = jQuery(this).parents(".panel").children(".panel-body");
            if (jQuery(this).hasClass("fa-chevron-down")) {
                jQuery(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
                el.slideUp(200);
            } else {
                jQuery(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
                el.slideDown(200);
            }
        });

        jQuery('.panel .tools .fa-times').click(function () {
            jQuery(this).parents(".panel").parent().remove();
        });


        //    tool tips
        if ( $.fn.tooltip ) {
            $('.tooltips').tooltip();
        }

        //    popovers
        if ( $.fn.popover ) {
            $('.popovers').popover();
        }

        // custom bar chart
        if ($(".custom-bar-chart")) {
            $(".bar").each(function () {
                var i = $(this).find(".value").html();
                $(this).find(".value").html("");
                $(this).find(".value").animate({
                    height: i
                }, 2000)
            })
        }
    }();


    $(window).scroll(function () {
    /* One page scroll */
    var lastId,
        topMenu = $('#menu-front-page, #menu-footer-home'),
        topMenuHeight = topMenu.outerHeight()+25,
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $(this).attr("href");
          if ( 0 === item.indexOf('#') ) {
            item = $(item);
            if (item.length) {
                return item;
            }
          }
        });

        // Bind click handler to menu items
        // so we can get a fancy scroll animation
        menuItems.click(function(e){
            var href = $(this).attr("href"),
              offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
            $('html, body').stop().animate({ 
              scrollTop: offsetTop
            }, 300);
            e.preventDefault();
        });

        // Bind to scroll

       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
       
       if (lastId !== id && id.length) {
           lastId = id;
           // Set/remove current class
           menuItems
             .parent().removeClass("current")
             .end().filter('a[href="#' + id + '"]').parent().addClass("current");
       }                   


    /* Login / Get Started menu item */
    var target = 0;
    var timeout = null;
        if ( $(".intro-section").length ) {
            target = $(".intro-section").offset().top;
            if (!timeout) {
                timeout = setTimeout(function () {
                clearTimeout(timeout);
                timeout = null;
                    if ($(window).scrollTop() >= target) {
                        $('.home-header-cta .js-login').css('display', 'none');
                        $('.home-header-cta .inpage-login').removeClass('hidden');
                        $('.home-header-cta .inpage-login a').addClass('button').css({
                            height: '34px',
                            fontSize: '0.9em',
                            'padding-top': '9px'

                        });
                    } else {
                        $('.home-header-cta .js-login').css('display', 'block');
                        $('.home-header-cta .inpage-login').addClass('hidden');
                    }
                }, 250);
            }
        }
    });


    window.initPhotoSwipeFromDOM = function(gallerySelector) {
        // parse slide data (url, title, size ...) from DOM elements 
        // (children of gallerySelector)
        var parseThumbnailElements = function(el) {
            var thumbElements = el.childNodes,
                numNodes = thumbElements.length,
                items = [],
                figureEl,
                linkEl,
                size,
                item;

            for(var i = 0; i < numNodes; i++) {

                figureEl = thumbElements[i]; // <figure> element

                // include only element nodes 
                if(figureEl.nodeType !== 1) {
                    continue;
                }

                // create slide object
                item = {
                    src: figureEl.getAttribute('data-full-image-url'),
                    w: parseInt(figureEl.getAttribute('data-full-image-w'), 10),
                    h: parseInt(figureEl.getAttribute('data-full-image-h'), 10)
                };

                if(figureEl.children.length > 1) {
                    // <figcaption> content
                    item.title = figureEl.children[1].innerHTML; 
                }

                item.el = figureEl; // save link to element for getThumbBoundsFn
                items.push(item);
            }

            return items;
        };

        // find nearest parent element
        var closest = function closest(el, fn) {
            return el && ( fn(el) ? el : closest(el.parentNode, fn) );
        };

        // triggers when user clicks on thumbnail
        var onThumbnailsClick = function(e) {

            e = e || window.event;
            e.preventDefault ? e.preventDefault() : e.returnValue = false;

            var eTarget = e.target || e.srcElement;

            // find root element of slide
            var clickedListItem = closest(eTarget, function(el) {
                return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
            });
            
            clickedListItem = clickedListItem || eTarget;

            if(!clickedListItem) {
                return;
            }

            // find index of clicked item by looping through all child nodes
            // alternatively, you may define index via data- attribute
            var clickedGallery = clickedListItem.parentNode,
                childNodes = clickedListItem.parentNode.childNodes,
                numChildNodes = childNodes.length,
                nodeIndex = 0,
                index;

            for (var i = 0; i < numChildNodes; i++) {
                if(childNodes[i].nodeType !== 1) { 
                    continue; 
                }

                if(childNodes[i] === clickedListItem) {
                    index = nodeIndex;
                    break;
                }
                nodeIndex++;
            }

            if(index >= 0) {
                // open PhotoSwipe if valid index found
                openPhotoSwipe( index, clickedGallery );
            }
            return false;
        };

        // parse picture index and gallery index from URL (#&pid=1&gid=2)
        var photoswipeParseHash = function() {
            var hash = window.location.hash.substring(1),
            params = {};

            if(hash.length < 5) {
                return params;
            }

            var vars = hash.split('&');
            for (var i = 0; i < vars.length; i++) {
                if(!vars[i]) {
                    continue;
                }
                var pair = vars[i].split('=');  
                if(pair.length < 2) {
                    continue;
                }           
                params[pair[0]] = pair[1];
            }

            if(params.gid) {
                params.gid = parseInt(params.gid, 10);
            }

            return params;
        };

        var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
            var pswpElement = document.querySelectorAll('.pswp')[0],
                gallery,
                options,
                items;

            items = parseThumbnailElements(galleryElement);

            // define options (if needed)
            options = {

                // define gallery index (for URL)
                galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                getThumbBoundsFn: function(index) {
                    // See Options -> getThumbBoundsFn section of documentation for more info
                    var thumbnail = items[index].el.getElementsByTagName('img')[0] || items[index].el, // find thumbnail
                        pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                        rect = thumbnail.getBoundingClientRect(); 

                    return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                }

            };

            // PhotoSwipe opened from URL
            if(fromURL) {
                if(options.galleryPIDs) {
                    // parse real index when custom PIDs are used 
                    // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                    for(var j = 0; j < items.length; j++) {
                        if(items[j].pid == index) {
                            options.index = j;
                            break;
                        }
                    }
                } else {
                    // in URL indexes start from 1
                    options.index = parseInt(index, 10) - 1;
                }
            } else {
                options.index = parseInt(index, 10);
            }

            // exit if index not found
            if( isNaN(options.index) ) {
                return;
            }

            if(disableAnimation) {
                options.showAnimationDuration = 0;
            }

            options.mainClass = 'pswp--minimal--dark';
            options.barsSize = {top:0,bottom:0};
            options.captionEl = false;
            options.fullscreenEl = false;
            options.shareEl = false;
            options.bgOpacity = 0.85;
            options.tapToClose = true;
            options.tapToToggleControls = false;

            // Pass data to PhotoSwipe and initialize it
            gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
            gallery.init();
        };

        // loop through all gallery elements and bind events
        var galleryElements = document.querySelectorAll( gallerySelector );

        for(var i = 0, l = galleryElements.length; i < l; i++) {
            galleryElements[i].setAttribute('data-pswp-uid', i+1);
            galleryElements[i].onclick = onThumbnailsClick;
        }

        // Parse URL and open gallery if it contains #&pid=3&gid=1
        var hashData = photoswipeParseHash();
        if(hashData.pid && hashData.gid) {
            openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
        }
    };

    stemcounter.resortUserItems = function( user_id ) {
        if ( ! stemcounter.page.userItemsSorted ) {
            stemcounter.page.userItems.sort( (function( a, b ){
                if ( a.user_id != b.user_id ) {
                    if ( a.user_id == user_id ) {
                        return -1;
                    } else if ( b.user_id == user_id ) {
                        return 1;
                    }
                }

                if ( a.pricing_category != b.pricing_category ) {
                    if ( a.pricing_category == 'fresh_flower' ) {
                        return -1;
                    } else if ( b.pricing_category == 'fresh_flower' ) {
                        return 1;
                    } else {
                        return a.pricing_category.toLowerCase() > b.pricing_category.toLowerCase() ? 1 : -1;
                    }
                } else {
                    if ( a.name.toLowerCase() == b.name.toLowerCase() ) {
                        return 0;
                    } else {
                        return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;
                    }
                }
            }) );

            stemcounter.page.userItemsIndex = {};
            stemcounter.page.userItemsAsOptions = [];
            var i = 0, ii = 0, total = stemcounter.page.userItems.length, item, lastOptgroup;
            for ( i = 0; i < total; i++ ) {
                item = stemcounter.page.userItems[i];
                stemcounter.page.userItemsIndex[ item.id.toString() ] = i;

                var optgroup = item.pricing_category;
                if ( 'fresh_flower' == optgroup ) {
                    optgroup = 'Flower';
                } else {
                    optgroup = optgroup.replace( '_', ' ' );
                    optgroup = optgroup[0].toUpperCase() + optgroup.slice(1);
                }

                if ( optgroup != lastOptgroup ) {
                    lastOptgroup = optgroup;
                    stemcounter.page.userItemsAsOptions.push( {
                        label: optgroup,
                        type: 'header'
                    } );
                }

                if ( item.variations.length > 1 ) {
                    for ( ii = 0; ii < item.variations.length; ii++ ) {
                        if ( 'Default' == item.variations[ii].name ) {
                            continue;
                        }
                        stemcounter.page.userItemsAsOptions.push( {
                            label: item.name + ' - ' + item.variations[ii].name,
                            value: item.id + '_' + item.variations[ii].id,
                            className: 'lib-logo user-lib-' + item.user_id,
                            user_id: item.user_id
                        } );
                    };
                } else {
                    stemcounter.page.userItemsAsOptions.push( {
                        label: item.name,
                        value: item.id + '_' + item.default_variation.id,
                        className: 'lib-logo user-lib-' + item.user_id,
                        user_id: item.user_id
                    } );
                }
            }
        }
        stemcounter.page.userItemsSorted = true;
    }

    
    $('.mobile-menu .menu-link').click( function() {
        $('.mobile-menu').toggleClass( 'mobile-menu-active' );
    } );

})(jQuery);