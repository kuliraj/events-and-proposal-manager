(function($){

window.stemcounter = window.stemcounter || {};

stemcounter.JSONResponse = function(response, callback, service) {
    callback = callback ? callback : $.noop;
    service = service ? service : 'alertify';

    try {
        var r = 'object' == $.type( response ) ? response : JSON.parse(response);
        if (r.success) {
            callback(r);
        } else {
            var alertMessage;

            if (typeof r.message == 'string') {
                alertMessage = r.message;
            } else {
                alertMessage = r.message.join("\n");
            }
            if (service == 'alertify') {
                alertify.error(alertMessage);

                if (r.code === 0 &&
                    typeof r.values !== 'undefined' &&
                    typeof r.type !== 'undefined') {

                    var url = window.stemcounter.aurl({
                        action: 'sc_edit_item_form',
                        type: r.type,
                        id: r.values.id
                    });
                    stemcounter.page.editExistingItem = true;
                    jQuery('.edit-item-form').closest('.modal').removeClass('fade').modal('hide');
                    //stemcounter.openAjaxModal('We noticed you already had this item... here it is to edit.', url,  'new-modal edit-item-modal');
                    stemcounter.openAjaxModal('Duplicated item... please edit.', url,  'new-modal edit-item-modal');
                }

            } else {
                alert(alertMessage);
            }
        }
    } catch (e) {
        if (typeof console != 'undefined') {
            console.log(e);
            console.trace();
        }

        var alertMessage = 'An unknown error occurred. Please try again later.';
        if (service == 'alertify') {
            alertify.error(alertMessage);
        } else {
            alert(alertMessage);
        }
    }
}

stemcounter.ajaxHandler = function(promise, $blockNode, callback) {
    $blockNode.block();
    promise.always(function(response){
        $blockNode.unblock();
        stemcounter.JSONResponse(response, callback);
    });
};

var modalIndex = 0;
stemcounter.openAjaxModal = function(label, url, modal_class) {
    /* don't close the modal when clicked outside the popup */
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';

    var $template = $('#ajax-modal-template').clone();
    $template.removeAttr('id'); // remove id to avoid duplication

    var newId = 'ajax-modal-' + modalIndex.toString();

    $template.find('.modal-title:first').attr('id', newId).text(label);

    if ( modal_class ) {
        $template.addClass(modal_class);
    }
    $template.attr('aria-labelledby', newId);
    $template.on('hidden.bs.modal', function() {
        // dispose of template as it is no longer used
        $template.remove();
    });

    $template.appendTo($('body'));
    $template.modal('show');

    $template.find('.modal-body:first').load(url);

    modalIndex ++;
}

stemcounter.url = function(base_url, query_obj) {
    var url = base_url;
    if (query_obj) {
        if (url.indexOf('?') === -1) {
            url += '?';
        }
        url += $.param(query_obj);
    }
    return url;
}

stemcounter.aurl = function(query_obj) {
    return stemcounter.url(stemcounter.ajax_url, query_obj);
}

/* Ajax Forms */
$(document).on('submit', 'form.edit-arrangement-form', function(e){
    e.preventDefault();
    var $this = $(this);

    var url = stemcounter.ajax_url + '?action=sc_edit_arrangement';
    $.post(url, $this.serialize(), function(response){
        stemcounter.JSONResponse(response, function(r){
            var id = parseInt($this.find('input[name="id"]').val());
            if (isNaN(id) || id == 0) {
                stemcounter.gaEvent('arrangement', 'created');
            }

            if (r.success) {
                if ( undefined !== stemcounter.editArrangementFormCaller ) {
                    $this.closest('.modal').modal('hide');
                    stemcounter.editArrangementFormCaller.onArrangementEdit(r, $this);
                } else {
                    window.location.reload();
                }
            }
        });
    });
});

$(document).on('submit', 'form.edit-recipe-form', function(e){
    e.preventDefault();
    var $this = $(this);

    var url = stemcounter.ajax_url + '?action=sc_edit_recipe';
    $.post(url, $this.serialize(), function(response){
        stemcounter.JSONResponse(response, function(r){
            if (r.success) {
                window.location.reload();
            }
        });
    });
});

$(document).on('stemcounter.action.deleteItem', function(e, data){
    if (!confirm('Are you sure you wish to delete this item?')) {
        return;
    }

    var url = window.stemcounter.aurl({
        action: 'sc_delete_item',
        id: data.itemId
    });

    $.get(url, {}, function(response){
        stemcounter.JSONResponse(response, function(r){
            stemcounter.gaEvent('item', 'deleted');
            window.location.reload();
        });
    });
});

})(jQuery)
