(function($){

window.stemcounter = window.stemcounter || {};

stemcounter.floatInput = function($input) {
	$input = $($input);
	
	$input.change(function(){
		var defaultValue = $(this).attr('placeholder') ? $(this).attr('placeholder') : 0;
		var val = $(this).val();
		var filteredVal = '';
		if (val != '') {
			filteredVal = parseFloat(val.replace(/[^0-9\.]*/gi, ''));
			filteredVal = isNaN(filteredVal) ? defaultValue : filteredVal;
		}

		if (val != filteredVal.toString()) {
			$(this).val(filteredVal.toString()).trigger('change');
		}
	});
};

stemcounter.parseFloatValue = function(value) {
	if (value != '') {
	  value = value.replace(/[^\d\.]/, '').split('.').slice(0, 2).join('.');
	  value = isNaN(parseFloat(value)) ? 0 : value;
	}
	return value;
};

stemcounter.formatToDecimals = function(value, decimals){
	var val = String(value).split('.');
	decimals = decimals || 2;

	if ( val.length > 1 ) {
		value = val[0] + '.' + val[1].substring(0, decimals);
	}

	return value;
};

stemcounter.maybeStripDecimals = function(value){
	var val = String(value);

	return val.replace( /\.0+$/, '' );
};

stemcounter.gaEvent = function(category, action) {
	if (typeof ga != 'undefined') {
		ga('send', 'event', category, action);
	} else if (typeof _gaq != 'undefined') {
		_gaq.push(['_trackEvent', category, action]);
	} else if (typeof console != 'undefined' && typeof console.error != 'undefined') {
		console.error('Google Analytics is required but not loaded.');
	}
};

stemcounter.formatPrice = function(value, addCurrency, currencySymbol, is_percentage, negative_parenthesis) {
	addCurrency = (typeof addCurrency == 'undefined') ? false : !!addCurrency;

	value = parseFloat(value);
	if (isNaN(value)) {
		value = 0;
	}

	var is_negative = 0 > value;
	value = value.toFixed10(2).toString();

	value = stemcounter.separate_price_sequences( value );

	if (addCurrency) {
		currencySymbol = stemcounter.formatCurrency(currencySymbol);

		if ( 1 == is_percentage ) {
			value = value + '%';
		} else {
			value = currencySymbol + value;
		}
	}

	if ( negative_parenthesis && is_negative ) {
		value = '(' + value.replace('-', '') + ')';
	}

	return value;
};

stemcounter.separate_price_sequences = function( num ) {
	var str = num.toString().split('.');
	var prices_separator = window.stemcounter.prices_separator;
	var separator = [];
	if ('space_dot' == prices_separator) {
		separator[0] = ' ';
		separator[1] = '.';
	} else if('space_comma' == prices_separator) {
		separator[0] = ' ';
		separator[1] = ',';
	} else if ('comma_dot' == prices_separator) {
		separator[0] = ',';
		separator[1] = '.';
	} else {
		separator[0] = '.';
		separator[1] = ',';
	}

	if (str[0].length >= 4) {
		str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1' + separator[0]);
	}
	if (str[1] && str[1].length >= 4) {
		str[1] = str[1].replace(/(\d{3})/g, '$1 ');
	}
	return str.join( separator[1] );
};

stemcounter.formatCurrency = function(currencySymbol) {

	if (typeof currencySymbol === 'undefined') {
		currencySymbol = String.fromCharCode(36); //dollar sign
	} else {
		currencies = {
			'AED': '\u062F.\u0625',
			'AUD': String.fromCharCode(36),
			'CAD': String.fromCharCode(36),
			'EUR': String.fromCharCode(8364),
			'GBP': String.fromCharCode(163), 
			'USD': String.fromCharCode(36),
			'ZAR': String.fromCharCode(82)
		};

		currencySymbol = currencies[currencySymbol];
	}
	return currencySymbol;
};

stemcounter.clone = function(object) {
	return JSON.parse(JSON.stringify(object));
};

$.fn.select2.amd.define('select2/dropdown/add-item', [], function(){
	function AddItem () { }

	AddItem.prototype.render = function (decorated) {
		var _this = this;

		this.$node = $('<div class="select2-add-item-button-wrap"><a href="#" class="btn btn-primary arrangement-create-new-item">Add New</a></div>');
		this.$node.find('a:first').click(function(e) {
			_this.$element.trigger('stemcounter.create_new_item').select2("close");
			// var temp = _this.$element.trigger('stemcounter.create_new_item'); //Добавено от Ганчо
		});

		var $rendered = decorated.call(this);
		$rendered.find('.select2-dropdown:first').append(this.$node);

		return $rendered;
	};

	return AddItem;
});

stemcounter.AddItemOptionSelect = function(node, options) {
	$.fn.select2.amd.require([
		'select2/utils',
		'select2/defaults',
		'select2/dropdown/add-item'
	], function(Utils, Defaults, AddItem){
		options.dropdownAdapter = Utils.Decorate(Defaults.apply(options).dropdownAdapter, AddItem);
		$(node).select2(options);
	});
}

$.fn.select2.amd.define('select2/dropdown/add-venue', [], function(){
	function addVenue () { }

	addVenue.prototype.render = function (decorated) {
		var _this = this;

		this.$node = $('<div class="select2-add-item-button-wrap"><a href="#" class="btn btn-primary arrangement-create-new-item">Add New Vendor</a></div>');
		this.$node.find('a:first').click(function(e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			
			var venueFormIndex = _this.$element.closest('.venue-form').data('formIndex');
			$('body').attr('data-add-new-event-to', venueFormIndex);
			
			_this.$element.trigger('stemcounter.create_new_venue').select2("close");
		});

		var $rendered = decorated.call(this);
		$rendered.find('.select2-dropdown:first').append(this.$node);

		return $rendered;
	};

	return addVenue;
});

stemcounter.AddVenueOptionSelect = function(node, options) {
	$.fn.select2.amd.require([
		'select2/utils',
		'select2/defaults',
		'select2/dropdown/add-venue'
	], function(Utils, Defaults, addVenue){
		options.dropdownAdapter = Utils.Decorate(Defaults.apply(options).dropdownAdapter, addVenue);
		$(node).select2(options);
	});
}

$.fn.select2.amd.define('select2/dropdown/add-customer', [], function(){
	function addCustomer () { }

	addCustomer.prototype.render = function (decorated) {
		var _this = this;

		this.$node = $('<div class="select2-add-item-button-wrap"><a href="#" class="btn btn-primary arrangement-create-new-item">Add New Customer</a></div>');
		this.$node.find('a:first').click(function(e) {
			e.preventDefault();
			
			_this.$element.trigger('stemcounter.create_new_customer').select2("close");
		});

		var $rendered = decorated.call(this);
		$rendered.find('.select2-dropdown:first').append(this.$node);

		return $rendered;
	};

	return addCustomer;
});

stemcounter.AddCustomerOptionSelect = function(node, options) {
	$.fn.select2.amd.require([
		'select2/utils',
		'select2/defaults',
		'select2/dropdown/add-customer'
	], function(Utils, Defaults, addCustomer){
		options.dropdownAdapter = Utils.Decorate(Defaults.apply(options).dropdownAdapter, addCustomer);
		$(node).select2(options);
	});
}


$.fn.select2.amd.define('select2/dropdown/scroll-fix-item', [], function(){
	function AddItem () { }

	AddItem.prototype.render = function (decorated) {
		var _this = this;

		this.$node = $('<div class="select2-add-item-button-wrap"></div>');

		var $rendered = decorated.call(this);
		$rendered.find('.select2-dropdown:first').append(this.$node);

		return $rendered;
	};

	return AddItem;
});
stemcounter.ScrollFix = function(node, options) {
	$.fn.select2.amd.require([
		'select2/utils',
		'select2/defaults',
		'select2/dropdown/scroll-fix-item'
	], function(Utils, Defaults, AddItem){
		options.dropdownAdapter = Utils.Decorate(Defaults.apply(options).dropdownAdapter, AddItem);
		$(node).select2(options);
	});
}

stemcounter.getItemByKeypair = function(keypair, currentItem){
	keypair = keypair.split('_');

	var itemId = keypair[0];
	var variationId = keypair[1];

	if ( undefined === stemcounter.page.userItems ) {
		return null;
	}

	var item = _.find(stemcounter.page.userItems, function(user_item){
	  return (user_item.id == itemId);
	});

	if (item === undefined || item.variations.length === 0 ) {
		return {
			deleted: true,
			itemId: currentItem.item_id,
			itemName: currentItem.name,
			itemVariationName: currentItem.item_variation_name,
			itemVariationId: variationId,
			itemCost: currentItem.cost
		};
	}

	return item;
}

stemcounter.sc_generate_rndstring = function() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for( var i=0; i < 5; i++ )
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

/**
 * Decimal adjustment of a number.
 *
 * @param   {String}    type    The type of adjustment.
 * @param   {Number}    value   The number.
 * @param   {Integer}   exp     The exponent (the 10 logarithm of the adjustment base).
 * @returns {Number}            The adjusted value.
 */
function decimalAdjust( type, value, exp ) {
	// If the exp is undefined or zero...
	if ( typeof exp === 'undefined' || +exp === 0 ) {
		return Math[type](value);
	}

	value = +value;
	exp = +exp;
	
	// If the value is not a number or the exp is not an integer...
	if ( isNaN(value) || ! ( typeof exp === 'number' && exp % 1 === 0 ) ) {
		return NaN;
	}
	
	// Shift
	value = value.toString().split('e');
	value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
	// Shift back
	value = value.toString().split('e');

	return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
}

// Decimal round
if ( ! Math.round10 ) {
	Math.round10 = function( value, exp ) {
		return decimalAdjust( 'round', value, exp );
	};
}
// Decimal floor
if ( ! Math.floor10 ) {
	Math.floor10 = function( value, exp ) {
		return decimalAdjust( 'floor', value, exp );
	};
}

// Decimal ceil
if ( ! Math.ceil10 ) {
	Math.ceil10 = function( value, exp ) {
		return decimalAdjust( 'ceil', value, exp );
	};
}

Number.prototype.toFixed10 = function(precision) {
	return Math.round10(this, -precision).toFixed(precision);
}

// Avoid errors when we call .toFixed10() on a string
String.prototype.toFixed10 = function(precision) {
	return parseFloat(this).toFixed10(precision);
}

$(document).ready(function(){
	$.fn.qtip.zindex = 1002; // override qtip zindex so it does not appear over modals
	$.fn.qtip.defaults.position.my = 'bottom center';
	$.fn.qtip.defaults.position.at = 'top center';
	$.fn.qtip.defaults.style.classes += ' stemcounter-qtip';
	
	$.blockUI.defaults.message = '<i class="fa fa-spinner fa-pulse fa-3x fa-fw sc-primary-color"></i>';
	$.blockUI.defaults.css.border = '0';
	$.blockUI.defaults.css.background = 'none';
	$.blockUI.defaults.css.width = 60;
	$.blockUI.defaults.css.height = 60;
	$.blockUI.defaults.css.textAlign = 'center';
	$.blockUI.defaults.css.top = '50%';
	$.blockUI.defaults.css.left = '50%';
	$.blockUI.defaults.overlayCSS.backgroundColor = '#FFFFFF';
	$.blockUI.defaults.overlayCSS.opacity = 0.75;

	$(document).on('hidden.bs.modal', '.modal', function(e){
		// fetch only the first visible one to improve performance as we only care about having *at least one* visible modal
		if ($('.modal:visible:first').length > 0) {
			$('body').addClass('modal-open');
		}
	});

	$(document).on('hide.bs.modal', '.modal', function(e){
		// datepicker sometimes opens up when a modal with a datepicker field inside closes
		// this is a work-around
		$('.hasDatepicker').datepicker('hide');
	});
});

stemcounter.confirmModal = function(opts){
	var body = $('body');
	var defaultOptions    = {
		confirmTitle     : 'Please confirm',
		confirmMessage   : 'Are you sure you want to do this?',
		confirmOk        : 'Yes',
		confirmCancel    : 'Cancel',
		confirmDirection : 'rtl',
		confirmStyle     : 'primary',
		confirmCallback  : $.noop
	};
	var options = $.extend(defaultOptions, opts);
	var time    = Date.now();

	var headModalTemplate =
		'<div class="modal new-modal hide fade" id="#modalId#" tabindex="-1" role="dialog" aria-labelledby="#AriaLabel#" aria-hidden="true">\
			<div class="modal-dialog">\
				<div class="modal-content">\
					<div class="modal-header">\
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>\
						<h4 class="modal-title">#Heading#</h4>\
					</div>\
					<div class="modal-body">\
						<p class="message">#Body#</p>\
					</div>\
					<div class="modal-footer">\
					#buttonTemplate#\
					</div>\
				</div>\
			</div>\
		</div>';

	var confirmLink = $(this);
	var targetData  = confirmLink.data();

	var currentOptions = $.extend(options, targetData);

	var modalId = "confirmModal" + parseInt(time);
	var modalTemplate = headModalTemplate;
	var buttonTemplate =
		'<button class="btn" data-dismiss="modal" aria-hidden="true">#Cancel#</button>' +
		'<button class="btn btn-#Style#" data-dismiss="ok" data-href="' + confirmLink.attr('href') + '">#Ok#</button>'
	;

	if(options.confirmDirection == 'ltr') {
		buttonTemplate =
			'<button class="btn btn-#Style#" data-dismiss="ok" data-href="' + confirmLink.attr('href') + '">#Ok#</button>' +
			'<button class="btn" data-dismiss="modal" aria-hidden="true">#Cancel#</button>'
		;
	}

	modalTemplate = modalTemplate.
		replace('#buttonTemplate#', buttonTemplate).
		replace('#modalId#', modalId).
		replace('#AriaLabel#', options.confirmTitle).
		replace('#Heading#', options.confirmTitle).
		replace('#Body#', options.confirmMessage).
		replace('#Ok#', options.confirmOk).
		replace('#Cancel#', options.confirmCancel).
		replace('#Style#', options.confirmStyle)
	;

	body.append(modalTemplate);

	var confirmModal = $('#' + modalId);
	var modalAlreadyOpened = body.hasClass('modal-open');

	confirmModal.on('hidden.bs.modal', function(){
		confirmModal.remove();
	}).on('show.bs.modal', function(){
	}).modal('show').removeClass('hide');

	if ( modalAlreadyOpened ) {
		confirmModal.find('.modal-content').css('transform', 'translateX(-8px)');
	}

	$('button[data-dismiss="ok"]', confirmModal).on('click', function(event) {
		confirmModal.modal('hide');
		options.confirmCallback(confirmLink);
	});
};

})(jQuery);

/**
 * Add dataset support to elements
 * No globals, no overriding prototype with non-standard methods, 
 *   handles CamelCase properly, attempts to use standard 
 *   Object.defineProperty() (and Function bind()) methods, 
 *   falls back to native implementation when existing
 * Inspired by http://code.eligrey.com/html5/dataset/ 
 *   (via https://github.com/adalgiso/html5-dataset/blob/master/html5-dataset.js )
 * Depends on Function.bind and Object.defineProperty/Object.getOwnPropertyDescriptor (polyfills below)
 * All code below is Licensed under the X11/MIT License
*/

// Inspired by https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function/bind#Compatibility
if (!Function.prototype.bind) {
	Function.prototype.bind = function (oThis) {
		'use strict';
		if (typeof this !== "function") {
			// closest thing possible to the ECMAScript 5 internal IsCallable function
			throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
		}

		var aArgs = Array.prototype.slice.call(arguments, 1),
			fToBind = this,
			FNOP = function () {},
			fBound = function () {
				return fToBind.apply(
					this instanceof FNOP && oThis ? this : oThis,
				   aArgs.concat(Array.prototype.slice.call(arguments))
			   );
			};

		FNOP.prototype = this.prototype;
		fBound.prototype = new FNOP();

		return fBound;
	};
}

/*
 * Xccessors Standard: Cross-browser ECMAScript 5 accessors
 * http://purl.eligrey.com/github/Xccessors
 * 
 * 2010-06-21
 * 
 * By Eli Grey, http://eligrey.com
 * 
 * A shim that partially implements Object.defineProperty,
 * Object.getOwnPropertyDescriptor, and Object.defineProperties in browsers that have
 * legacy __(define|lookup)[GS]etter__ support.
 * 
 * Licensed under the X11/MIT License
 *   See LICENSE.md
*/

// Removed a few JSLint options as Notepad++ JSLint validator complaining and 
//   made comply with JSLint; also moved 'use strict' inside function
/*jslint white: true, undef: true, plusplus: true,
  bitwise: true, regexp: true, newcap: true, maxlen: 90 */

/*! @source http://purl.eligrey.com/github/Xccessors/blob/master/xccessors-standard.js*/

(function () {
	'use strict';
	var ObjectProto = Object.prototype,
	defineGetter = ObjectProto.__defineGetter__,
	defineSetter = ObjectProto.__defineSetter__,
	lookupGetter = ObjectProto.__lookupGetter__,
	lookupSetter = ObjectProto.__lookupSetter__,
	hasOwnProp = ObjectProto.hasOwnProperty;
	
	if (defineGetter && defineSetter && lookupGetter && lookupSetter) {

		if (!Object.defineProperty) {
			Object.defineProperty = function (obj, prop, descriptor) {
				if (arguments.length < 3) { // all arguments required
					throw new TypeError("Arguments not optional");
				}
				
				prop += ""; // convert prop to string

				if (hasOwnProp.call(descriptor, "value")) {
					if (!lookupGetter.call(obj, prop) && !lookupSetter.call(obj, prop)) {
						// data property defined and no pre-existing accessors
						obj[prop] = descriptor.value;
					}

					if ((hasOwnProp.call(descriptor, "get") ||
						 hasOwnProp.call(descriptor, "set"))) 
					{
						// descriptor has a value prop but accessor already exists
						throw new TypeError("Cannot specify an accessor and a value");
					}
				}

				// can't switch off these features in ECMAScript 3
				// so throw a TypeError if any are false
				if (!(descriptor.writable && descriptor.enumerable && 
					descriptor.configurable))
				{
					throw new TypeError(
						"This implementation of Object.defineProperty does not support" +
						" false for configurable, enumerable, or writable."
					);
				}
				
				if (descriptor.get) {
					defineGetter.call(obj, prop, descriptor.get);
				}
				if (descriptor.set) {
					defineSetter.call(obj, prop, descriptor.set);
				}
			
				return obj;
			};
		}

		if (!Object.getOwnPropertyDescriptor) {
			Object.getOwnPropertyDescriptor = function (obj, prop) {
				if (arguments.length < 2) { // all arguments required
					throw new TypeError("Arguments not optional.");
				}
				
				prop += ""; // convert prop to string

				var descriptor = {
					configurable: true,
					enumerable  : true,
					writable    : true
				},
				getter = lookupGetter.call(obj, prop),
				setter = lookupSetter.call(obj, prop);

				if (!hasOwnProp.call(obj, prop)) {
					// property doesn't exist or is inherited
					return descriptor;
				}
				if (!getter && !setter) { // not an accessor so return prop
					descriptor.value = obj[prop];
					return descriptor;
				}

				// there is an accessor, remove descriptor.writable;
				// populate descriptor.get and descriptor.set (IE's behavior)
				delete descriptor.writable;
				descriptor.get = descriptor.set = undefined;
				
				if (getter) {
					descriptor.get = getter;
				}
				if (setter) {
					descriptor.set = setter;
				}
				
				return descriptor;
			};
		}

		if (!Object.defineProperties) {
			Object.defineProperties = function (obj, props) {
				var prop;
				for (prop in props) {
					if (hasOwnProp.call(props, prop)) {
						Object.defineProperty(obj, prop, props[prop]);
					}
				}
			};
		}
	}
}());

// Begin dataset code

if (!document.documentElement.dataset && 
		 // FF is empty while IE gives empty object
		(!Object.getOwnPropertyDescriptor(Element.prototype, 'dataset')  ||
		!Object.getOwnPropertyDescriptor(Element.prototype, 'dataset').get)
	) {
	var propDescriptor = {
		enumerable: true,
		get: function () {
			'use strict';
			var i, 
				that = this,
				HTML5_DOMStringMap, 
				attrVal, attrName, propName,
				attribute,
				attributes = this.attributes,
				attsLength = attributes.length,
				toUpperCase = function (n0) {
					return n0.charAt(1).toUpperCase();
				},
				getter = function () {
					return this;
				},
				setter = function (attrName, value) {
					return (typeof value !== 'undefined') ? 
						this.setAttribute(attrName, value) : 
						this.removeAttribute(attrName);
				};
			try { // Simulate DOMStringMap w/accessor support
				// Test setting accessor on normal object
				({}).__defineGetter__('test', function () {});
				HTML5_DOMStringMap = {};
			}
			catch (e1) { // Use a DOM object for IE8
				HTML5_DOMStringMap = document.createElement('div');
			}
			for (i = 0; i < attsLength; i++) {
				attribute = attributes[i];
				// Fix: This test really should allow any XML Name without 
				//         colons (and non-uppercase for XHTML)
				if (attribute && attribute.name && 
					(/^data-\w[\w\-]*$/).test(attribute.name)) {
					attrVal = attribute.value;
					attrName = attribute.name;
					// Change to CamelCase
					propName = attrName.substr(5).replace(/-./g, toUpperCase);
					try {
						Object.defineProperty(HTML5_DOMStringMap, propName, {
							enumerable: this.enumerable,
							get: getter.bind(attrVal || ''),
							set: setter.bind(that, attrName)
						});
					}
					catch (e2) { // if accessors are not working
						HTML5_DOMStringMap[propName] = attrVal;
					}
				}
			}
			return HTML5_DOMStringMap;
		}
	};
	try {
		// FF enumerates over element's dataset, but not 
		//   Element.prototype.dataset; IE9 iterates over both
		Object.defineProperty(Element.prototype, 'dataset', propDescriptor);
	} catch (e) {
		propDescriptor.enumerable = false; // IE8 does not allow setting to true
		Object.defineProperty(Element.prototype, 'dataset', propDescriptor);
	}
}

stemcounter.unmountComponentOnModalClose = function( component ) {
	jQuery( ReactDOM.findDOMNode(component) ).closest('.modal').on( 'hidden.bs.modal', function(){
		ReactDOM.unmountComponentAtNode( ReactDOM.findDOMNode(component).parentElement );
	} );
}

/* Example usage: stemcounter.AC.can_access( stemcounter.AC.TAGGING ) */
stemcounter.AC = new (function( config ){
	$.each(config.features, function( feature, feature_index ){
		Object.defineProperty( this, feature, {
			enumerable: true,
			get: function(){
				return config.features[ feature ];
			}
		} );
	}.bind(this));

	this.can_access = function( feature ) {
		return undefined === config.access[ feature ] ? null : config.access[ feature ];
	}.bind(this);

	return this;
})( SCAC_CONF );

stemcounter.rgb2hex = function( red, green, blue ) {
	var rgb = blue | (green << 8) | (red << 16);
	return '#' + (0x1000000 + rgb).toString(16).slice(1);
}

stemcounter.hex2rgb = function( hex ) {
	// Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
	var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	hex = hex.replace(shorthandRegex, function(m, r, g, b) {
		return r + r + g + g + b + b;
	});

	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	return result ? [
		parseInt( result[1], 16 ), // red
		parseInt( result[2], 16 ), // green
		parseInt( result[3], 16 )  // blue
	] : null;
}

stemcounter.get_hex_luma = function( hex ) {
	var rgb = stemcounter.hex2rgb( hex );

	return 0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2]; // per ITU-R BT.709
}

// Binds a bunch of methods to a class instance
// Good for when using a JS class and not React.createClass(), since our methods
// are not automatically bound to the created class instance
// hence using `this` in callback methods will not work properly
stemcounter.bindMethodsToComponent = function( instance ) {
	var properties = Object.getOwnPropertyNames( instance.__proto__ ),
		ignoredMethods = [ 'constructor', 'componentDidMount', 'render', 'componentWillMount', 'componentWillReceiveProps', 'shouldComponentUpdate', 'componentWillUpdate', 'componentDidUpdate', 'componentWillUnmount' ];

	for (var i = 0; i < properties.length; i++) {
		// Ignore React methods as they are bound already
		// Also ignore any non-function properties
		if ( -1 !== ignoredMethods.indexOf( properties[i] ) || 'function' !== typeof instance[ properties[i] ] ) {
			continue;
		}

		instance[ properties[i] ] = instance[ properties[i] ].bind( instance );
	}
}

stemcounter.arr_can_be_applied_on = function( curr_arr_id, tba_on_arr_id, arrangements ) {
	var arrangements_by_id = {},
		i;

	for ( i = arrangements.length - 1; i >= 0; i-- ) {
		arrangements_by_id[ arrangements[i].id ] = arrangements[i];
	}
	if ("undefined" == typeof( arrangements_by_id[ tba_on_arr_id ] ) ) {
		return false;
	} //?
	if ( 1 != parseInt( arrangements_by_id[ tba_on_arr_id ].is_percentage, 10 ) || 0 == arrangements_by_id[ tba_on_arr_id ].applies_on.length ) {
		return true;
	}

	function get_applies_on( arr_id, applies_on ) {
		var i;

		// Break if we already know that it ain't happening
		if ( -1 !== applies_on.indexOf( tba_on_arr_id ) ) {
			return applies_on;
		}

		for ( i = 0; i < arrangements_by_id[arr_id].applies_on.length; i++ ) {
			applies_on.push( arrangements_by_id[arr_id].applies_on[i] );
			if ( undefined !== arrangements_by_id[ arrangements_by_id[arr_id].applies_on[i] ] && 0 < arrangements_by_id[ arrangements_by_id[arr_id].applies_on[i] ].applies_on.length ) {
				get_applies_on( arrangements_by_id[arr_id].applies_on[i], applies_on );
			}
		}

		return applies_on;
	}

	return -1 == get_applies_on( tba_on_arr_id, [] ).indexOf( curr_arr_id );
};

/**
 * Returns a ${count} amount of new lines as a string
 * 
 * Useful when you need to insert newline characters in a string that's parsed as JSX
 * Babel strips all newline characters - for instance if you have 'This is\n\nsome good stuff',
 * you'll end up with 'This is some good stuff' - not fun :( So just change the string to
 * 'This is ' + stemcounter.new_lines( 2 ) + 'some good stuff'
 */
stemcounter.new_lines = function( count ) {
	return Array( count ).join( '\n' );
}

stemcounter.getMediaModal = function( args ){
	console.trace( 'Tried to get media modal without wp.media being present' );
	return false;
}

if ( undefined !== wp && undefined !== wp.media ) {
	wp.media.view.ScAttachmentRotate = wp.media.view.Attachment.extend({
		tagName:   'div',
		className: 'attachment-details',
		template:  wp.template('rotate-photos'),

		attributes: function() {
			return {
				'tabIndex':     0,
				'data-id':      this.model.get( 'id' )
			};
		},

		events: {
			'click .image-rotate-left':       'rotateAttachmentLeft',
			'click .image-rotate-right':      'rotateAttachmentRight',
			'click .rotate-save':             'saveChanges'
		},

		initialize: function( options ) {
			this.options = _.defaults( this.options, {
				rerenderOnModelChange: true
			});

			if ( undefined == this.model.get( 'rotation' ) || false == this.model.get( 'sc_saving' ) ) {
				this.model.set( {
					sc_saving: false,
					rotation: 0
				});
			}

			this.sidebar = options.sidebar;

			// Call 'initialize' directly on the parent class.
			wp.media.view.Attachment.prototype.initialize.apply( this, arguments );
		},

		/**
		 * @param {Object} event
		 */
		saveChanges: function( event ) {
			this.model.save().done( function( ) {
				this.model.fetch({
					success: function(){
						this.sidebar.get('details').render();
						this.model.set( {
							sc_saving: false,
							rotation: 0
						});
						//this.render();
					}.bind(this)
				});
			}.bind( this ) );
			this.model.set({
				sc_saving: true
			});
			//this.render();
		},

		/**
		 * @param {Object} event
		 */
		rotateAttachmentRight: function( event ) {
			event.preventDefault();
			var rotation = this.model.get( 'rotation' ) + 90;
			rotation = 360 <= rotation ? 360 - rotation : rotation;
			this.model.set( {
				sc_saving: false,
				rotation: rotation
			});
			// this.render();
		},

		/**
		 * @param {Object} event
		 */
		rotateAttachmentLeft: function( event ) {
			event.preventDefault();
			var rotation = this.model.get( 'rotation' ) - 90;
			rotation = 0 > rotation ? 360 + rotation : rotation;
			this.model.set( {
				sc_saving: false,
				rotation: rotation
			});
			// this.render();
		},
	});

	wp.media.view.ScAttachmentsBrowser = wp.media.view.AttachmentsBrowser.extend({
		createSingle: function() {
			var sidebar = this.sidebar,
				single = this.options.selection.single();

			sidebar.set( 'details', new wp.media.view.Attachment.Details({
				controller: this.controller,
				model:      single,
				priority:   80
			}) );

			sidebar.set( 'scrotate', new wp.media.view.ScAttachmentRotate({
				controller: this.controller,
				model:      single,
				priority:   80,
				sidebar:    sidebar
			}) );

			sidebar.set( 'compat', new wp.media.view.AttachmentCompat({
				controller: this.controller,
				model:      single,
				priority:   120
			}) );

			if ( this.options.display ) {
				sidebar.set( 'display', new wp.media.view.Settings.AttachmentDisplay({
					controller:   this.controller,
					model:        this.model.display( single ),
					attachment:   single,
					priority:     160,
					userSettings: this.model.get('displayUserSettings')
				}) );
			}

			// Show the sidebar on mobile
			if ( this.model.id === 'insert' ) {
				sidebar.$el.addClass( 'visible' );
			}
		},

		disposeSingle: function() {
			var sidebar = this.sidebar;
			sidebar.unset('details');
			sidebar.unset('compat');
			sidebar.unset('display');
			sidebar.unset('scrotate');
			// Hide the sidebar on mobile
			sidebar.$el.removeClass( 'visible' );
		}
	});

	wp.media.view.MediaFrame.ScSelect = wp.media.view.MediaFrame.Select.extend({
		browseContent: function( contentRegion ) {
			var state = this.state();

			this.$el.removeClass('hide-toolbar');

			// Browse our library of attachments.
			contentRegion.view = new wp.media.view.ScAttachmentsBrowser({
				controller: this,
				collection: state.get('library'),
				selection:  state.get('selection'),
				model:      state,
				sortable:   state.get('sortable'),
				search:     state.get('searchable'),
				filters:    state.get('filterable'),
				date:       state.get('date'),
				display:    state.has('display') ? state.get('display') : state.get('displaySettings'),
				dragInfo:   state.get('dragInfo'),

				idealColumnWidth: state.get('idealColumnWidth'),
				suggestedWidth:   state.get('suggestedWidth'),
				suggestedHeight:  state.get('suggestedHeight'),

				AttachmentView: state.get('AttachmentView')
			});
		}
	});

	stemcounter.getMediaModal = function( args ){
		return new wp.media.view.MediaFrame.ScSelect( args );
	}
}
