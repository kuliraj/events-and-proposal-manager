<?php
/*
Template Name: User - Event Proposal 2.0
*/

use Illuminate\Http\Request;
use Stemcounter\Event;

$request = Request::capture();

$user_id = intval(get_current_user_id());
$event_id = intval($request->input('eid'));

// Defined in event-management.php, sc_change_event_uid()
if ( empty( $skip_ownership_check ) ) {
	sc_event_ownership_checkpoint($event_id, $user_id, true, sc_get_proposal_page_url());
}

get_header( 'new-design' ); ?>

	<section class="wrapper">
		<?php if ( ! empty( $content_override ) ) {
			// Defined in event-management.php, sc_change_event_uid()
			echo $content_override;
		} else {
			the_content();
		} ?>
	</section>

<?php get_footer();