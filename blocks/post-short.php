<article id="post-<?php the_ID(); ?>" <?php post_class('post-loop-item'); ?> >
	<header class="post-header">
		<h1 class="post-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
		</h1>
		<?php sc_block('post-meta'); ?>
	</header><!-- .entry-header -->

	<?php if (has_post_thumbnail()) : ?>
		<div class="blog-thumb">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail('blog_thumb'); ?>
			</a>
		</div>
	<?php endif; ?>
	

	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->

	<div class="entry-meta entry-meta-footer">
		<span class="cat-links">
			Filed under: 
			<?php the_category(', '); ?>
		</span>
		<?php if (has_tag()) : ?>
			<span class="tags-links">
				Tags:
				<?php the_tags('', ', ', ''); ?>
			</span>
		<?php endif; ?>
	</div><!-- .entry-footer -->

	<p><a class="moretag" href="<?php the_permalink(); ?>">Read more &rarr;</a></p>
	
</article>