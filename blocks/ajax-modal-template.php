<div class="modal fade" id="ajax-modal-template" role="dialog" aria-labelledby="" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<div class="ajax-modal-loader" >
					<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
					<span class="sr-only">Loading...</span>
				</div>
				<!-- <img src="<?php bloginfo('template_directory'); ?>/img/spiffygif_40x40.gif" alt="" class="ajax-modal-loader" /> -->
			</div>
		</div>
	</div>     
</div>