<div class="blog-header clearfix">
	<?php wp_nav_menu('theme_location=blog-header&container=&fallback_cb=&depth=2'); ?>
	<ul>
		<li>
			<form action="<?php echo home_url('/'); ?>" method="get">
				<div class="dashicons dashicons-search search-icon"></div>
				<input type="text" name="s" value="<?php echo esc_attr(get_query_var('s')); ?>" placeholder="Search blog" class="search-input" />
			</form>
		</li>
	</ul>
</div>