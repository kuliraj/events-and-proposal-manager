<?php
$post_id = get_the_ID();
$post_categories = wp_get_object_terms($post_id, 'category');
$query = new WP_Query(array(
	'post_type'=>'post',
	'posts_per_page'=>3,
	'orderby'=>'rand',
	'post__not_in'=>array($post_id),
	'tax_query'=>array(
		array(
			'taxonomy'=>'category',
			'field'=>'term_id',
			'terms'=>wp_list_pluck($post_categories, 'term_id'),
		),
	),
));
if (!$query->have_posts()) {
	return;
}
?>
<div class="related-posts">
	<h3>Related stories</h3>

	<?php while ($query->have_posts()) : $query->the_post(); ?>
		<?php sc_block('post-glimpse'); ?>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</div>