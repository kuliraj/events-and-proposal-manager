<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
<a href="<?php the_permalink(); ?>" class="post-glimpse">
	<?php if ($image) : ?>
		<span class="image-thumb" style="background-image: url(<?php echo $image[0]; ?>);"></span>
	<?php endif; ?>
	<?php the_title(); ?>
</a>