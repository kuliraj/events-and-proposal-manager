<div class="entry-meta clearfix">
	<div class="details">
		<span class="post-author"><?php the_author(); ?></span>
		<span>|</span>
		<span class="post-date"><?php the_time('F j, Y'); ?></span>
		<span>|</span>
		<span class="comments-link">
			<a class="post-comments icon-comments" href="<?php comments_link(); ?>">
				<span class="dashicons dashicons-admin-comments"></span>
				<?php comments_number('0 comments', '1 comment', '% comments'); ?>
			</a>
		</span>
	</div>
	<div class="social">
		
	</div>
</div><!-- .entry-meta -->