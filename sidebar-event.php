<?php if ( has_nav_menu( 'event-menu' ) ) : ?>

<!-- to be moved to separate files -->

<style>
.sidebar-menu a.fa:before { margin-right: 6px; font-size: 15px; line-height: 15px; } 
</style>

<script	type="text/javascript">
(function($){
	
$(document).ready(function(){
	$('.sidebar-menu li').each(function(){
		var classes = $(this).attr('class').split(/\s+/);
		for (var i = 0; i < classes.length; i ++) {
			var _class = classes[i];
			if (_class == 'fa' || _class.substr(0, 3) == 'fa-') {
				$(this).removeClass(_class);
				$(this).find('a:first').addClass(_class);
			}
		}
	});
});
	
})(jQuery);
</script>
<!-- END: to be moved to separate files -->

<!--sidebar start-->
<aside class="___Sidebar_Event">
	<div id="sidebar"  class="nav-collapse ">
		<!-- sidebar menu start-->
		<?php
            // Social links navigation menu.
            wp_nav_menu( array(
                'theme_location'	=> 'event-menu',
                'container' 			=> false,
                'menu_class'			=> 'sidebar-menu',
                'menu_id'			=> 'nav-accordion',
                'depth'     			=> 1,
                'link_before'  		=> '<span>',
                'link_after'    		=> '</span>'
            ) );
        ?>
	</div><!-- #sidebar -->
        
</aside><!-- .___Sidebar_Event -->

<?php endif; ?>