(function($) {
'use strict';

var EventItem = React.createClass({
	getInitialState: function() {
		var state = { 
			selectedValue: '-1',
			canAddMore: true
		};

		if (this.props.eventValues) {
			state.selectedValue = this.props.eventValues;
			if (state.selectedValue !== '-1') {
				state.canAddMore = false;
			}
		}

		return state;
	},

	onEventChange: function(event) {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		updatedState.selectedValue = event.target.value;

		if (updatedState.selectedValue === '-1') {
			updatedState.canAddMore = true;
		}

		if (this.state.canAddMore) {
			this.props.autoAddMore();
			updatedState.canAddMore = false;
		}

		this.setState(updatedState);
	},

	removeEventItem: function() {
		event.preventDefault();
    	this.props.onDelete(this.props.reactKey);
	},

	render: function() {
		var itemHtml = false;
		if (this.props.layout === 'add-order') {
			itemHtml = (
				<div className="eventItem">
					<div className="col-xs-10 col-sm-11">
						<ReactSelect2 
							className="form-control eventItemValue" 
							name="order_events[]"
							children={this.props.eventsList} 
							selectedValue={this.state.selectedValue}
							onChange={this.onEventChange}
							replaceSelect2={this.replaceSelect2} />
					</div>
					<div className="col-xs-1">
						<a href="#" className="remove-order-item remove-arrangement-item" onClick={this.removeEventItem}>
							<i className="fa fa-trash-o"></i>
						</a>
					</div>
				</div>
			);
		} else if (this.props.layout === 'edit-order') {
			var eventName = 'Deleted Event';
			var eventDate = '';
			var _this = this;

			this.props.rawEventsList.forEach(function(event, index, arr) {
				if (event.eventId == _this.state.selectedValue) {
					eventName = event.eventName;
					eventDate = ' - ' + event.eventDate;
				}
			});

			itemHtml = (
				<div className="eventItem">
					<div className="form-control col-xs-12">{eventName}{eventDate}</div>
				</div>
			);
		}

		return itemHtml;
	}
});

var OrderModal = React.createClass({
	getInitialState: function() {
		var state = {
			orderName: '',
			note: '',
			estimatedCost: 0,
			orderFulfilmentDate: '',
			eventItems: ['-1']
		};

		if (this.props.eventItems) state.eventItems = this.props.eventItems;
		if (this.props.orderName) state.orderName = this.props.orderName
		if (this.props.orderNote) state.note = this.props.orderNote;
		if ( this.props.orderFulfilmentDate ) {
			state.orderFulfilmentDate = this.props.orderFulfilmentDate;
		}

		return state;
	},

	replaceSelect2: function(node) {
	    window.stemcounter.ScrollFix($(node), {});
	},

	handleChange: function(field, event) {
		var updatedState = {};
		
		if (field === 'name') {
			updatedState.orderName = event.target.value;
		} else if (field === 'note') {
			updatedState.note = event.target.value;
		}

		this.setState(updatedState);
	},

	componentDidUpdate: function() {
		if (this.props.layout == 'add-order') {
			var ids = [];
	
			$.each($('.eventItemValue'), function(index, value) {
				ids.push($(value).val());
			});

			if (ids.indexOf('-1') === -1) this.autoAddMore(); 
		}
	},

	componentDidMount: function(){
		if (this.refs.fulfilmentDate) {
			var _this = this;
			$(this.refs.fulfilmentDate).datepicker({ 
				dateFormat: this.props.dateFormat,
				onClose: function(val, opts) {
					var updatedState = $.extend( true, {}, _this.state );
					updatedState.orderFulfilmentDate = val;
					_this.setState( updatedState );
				}
			});
		}
		stemcounter.unmountComponentOnModalClose( this );
	},

	autoAddMore: function() {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		updatedState.eventItems.push('-1');
		this.setState(updatedState);
	},

	deleteEventItem: function(index) {
		var index = index;
		var newEventItems = this.state.eventItems.slice();
		//fill with null to not let react get confused and remove the wrong components
		newEventItems.splice(index, 1, null);
		
		if (newEventItems.every(function (el, i, arr) {
			return el === null;
		})) {
			//Add event item if there are no more items visible
			newEventItems.push('-1');
		}

		this.setState({ eventItems: newEventItems });
	},

	submit: function(event) {
		event.preventDefault();
		if (this.props.layout === 'add-order') {
			var url = window.stemcounter.aurl({ action: 'sc_add_user_order' });
		    var data = $('form.add-order-form:first').serialize();

		    $.post(url, data, function (response) {
		    	stemcounter.JSONResponse(response, function (r) {
		    		if (r.success) document.location.reload();
		        });
		    });
		} else if (this.props.layout === 'edit-order') {
			var url = window.stemcounter.aurl({ action: 'sc_edit_user_order' });
		    var data = $('form.edit-order-form:first').serialize();

		    $.post(url, data, function (response) {
		    	stemcounter.JSONResponse(response, function (r) {
		    		if (r.success) document.location.reload();
		        });
		    });
		}
	},

	render: function() {
		var formControl = (
			<div>
				<button className="btn btn-default btn-cancel" type="button" data-dismiss="modal">Cancel</button>
				<button className="btn btn-primary button-submit" type="submit" onClick={this.submit}>Save</button>
			</div>
		);

		var rawEventsList = [];
		var eventsList = this.props.eventsList.map(function(event, index, array) {
			rawEventsList.push({eventId: event.id, eventName: event.name, eventDate: event.date});
			return <option value={event.id} key={'event-' + event.id + '-' + index}>{event.name} - {event.date}</option>;
		});
		eventsList.unshift(<option value="-1" key={'event-na'}>Select Event</option>);

		return (
			<div>
				<div>
					<div className="modalpane-row clearfix pane-keep">
						<label className="col-xs-8 control-label modalpane">
							<div className="lbl">
								Name<span className="mandatory"></span>
							</div>
							<input placeholder="Enter name here" className="form-control" type="text" name="order_name" value={this.state.orderName} onChange={this.handleChange.bind(this, 'name')}/>
							<input type="hidden" name="order_id" value={this.props.orderId} />
						</label>
						<label className="col-xs-4 modalpane">
							<div className="lbl">
								Created At:
							</div>
							<div className="order-date">{this.props.orderDate}</div>
						</label>
					</div>

					<div className="modalpane-row clearfix pane-keep">
						<label className="col-xs-12 modalpane">
							<div className="lbl">
								Fulfilment Date:
							</div>
							<input className="order-fulfilment-date form-control" ref="fulfilmentDate" name="fulfilment_date" defaultValue={this.state.orderFulfilmentDate} />
						</label>
					</div>

					<div className="modalpane-row clearfix">
						<label className="col-sm-12 modalpane clearfix">
							<div className="lbl">
								Booked Event
							</div>
							<div className="eventItems">
								{this.state.eventItems.map(function(value, index, arr) {
									if (value === null) return;

									return <EventItem eventsList={eventsList} rawEventsList={rawEventsList} layout={this.props.layout} onDelete={this.deleteEventItem} key={index} reactKey={index} eventItems={this.state.eventItems} eventValues={value} autoAddMore={this.autoAddMore} ref="eventItemRef" />;

								}, this)}
							</div>
						</label>
					</div>

					<div className="modalpane-row clearfix">
						<label className="col-sm-12 control-label modalpane">
							<div className="lbl">
								Description
							</div>
							<textarea className="form-control" placeholder="Add notes" name="order_note" value={this.state.note} onChange={this.handleChange.bind(this, 'note')}></textarea>
						</label>
					</div>
				</div>
				<div className="modal-footer">
					<div className="pull-left hide">*required</div>
					{formControl}
				</div>
			</div>
		);
	}
});

$(document).on('stemcounter.action.renderOrderForm', function(e, settings) {
	ReactDOM.render(
		React.createElement(
			OrderModal, { 
			layout: settings.layout,
			eventsList: settings.eventsList,
			orderId: settings.orderId,
			orderName: settings.orderName,
			orderNote: settings.orderNote,
			orderDate: settings.orderDate,
			eventItems: settings.eventItems,
			orderFulfilmentDate: settings.orderFulfilmentDate
		}),
		$(settings.node).get(0)
	);
});

})(jQuery);