(function($){
	class ProfileSettings extends SC_Component {
		constructor( props ) {
			super( props );

			this.state = {
				logo: props.settings.logo,
				logo_id: props.settings.logo_id,
				logo_on_proposal: props.settings.logo_on_proposal ? 1 : 0,
				user_libraries: props.settings.user_libraries,
				user_units: props.settings.user_units,
				company: props.settings.company,
				address: props.settings.address,
				website: props.settings.website,
				phone: props.settings.phone,
				card: props.settings.card,
				date_format: props.settings.date_format,
				pref_currency: props.settings.pref_currency,
				pdf_sheet_size: props.settings.pdf_sheet_size,
				visible_taxes: props.settings.visible_taxes,
				prices_separator: props.settings.prices_separator
			};

			this.save = _.debounce( this.save, 1000 );
		}

		componentDidMount() {
			
		}

		onLibrarySelect( e ) {
			var user_libraries = $.extend(true, [], this.state.user_libraries),
				library = parseInt( e.target.value, 10 );

			if ( e.target.checked ) {
				if ( -1 === user_libraries.indexOf( library ) ) {
					alertify.confirm(
						'I understand that items imported into my library are only suggested initial prices to make my StemCounter setup easier. Prices ordered from Mayesh or other wholesalers will vary depending on the time of the year or seasonality of the product.',
						function(){
							// Yes
							user_libraries.push( library );
							this.setState({
								user_libraries: user_libraries
							}, this.save);
						}.bind(this)
					);
				}
			} else if ( -1 !== user_libraries.indexOf( library ) ) {
				user_libraries.splice( user_libraries.indexOf( library ), 1 );
				this.setState({
					user_libraries: user_libraries
				}, this.save);
			}
		}

		renderLibrary( library ) {
			return (
				<li key={'library-' + library.id}>
					<label>
						<input
							name="library"
							type="checkbox"
							value={library.id}
							checked={-1 !== this.state.user_libraries.indexOf( library.id )}
							onChange={this.onLibrarySelect}
						/> {library.name}
					</label>
				</li>
			);
		}

		onLogoUploaded( image_id, image ) {
			this.setState({
				logo_id: image_id,
				logo: image
			}, this.save);
		}

		save() {
			var data = $.extend(true, {}, this.state);
			data.action = 'sc/profile/settings/save';
			data.nonce = this.props.settings.nonce;

			$.post(stemcounter.ajax_url, data, this.onSaved);
		}

		onSaved( r ) {
			if ( r.success ) {
				alertify.success( 'Settings saved' );
			} else {
				alertify.error( 'Could not save settings' );
			}
		}

		logoOnProposalChange( e ) {
			this.setState({
				logo_on_proposal: e.target.checked ? 1 : 0
			}, this.save);
		}

		onFieldChanged( e ) {
			var updatedState = {},
				name = e.target.dataset.name;
			
			console.log( name, e.target.value );

			if ( name ) {
				updatedState[ name ] = e.target.value;
				this.setState( updatedState, this.save );
			}
		}

		onDateFormatChanged( e ) {
			this.setState({
				date_format: e.target.value
			}, this.save);
		}

		onCurrencyChanged( e ) {
			this.setState({
				pref_currency: e.target.value
			}, this.save);
		}

		onPaperSizeChanged( e ) {
			this.setState({
				pdf_sheet_size: e.target.value
			}, this.save);
		}

		onPricesSeparatorChanged( e ) {
			this.setState({
				prices_separator: e.target.value
			}, this.save);
		}

		onVisibleTaxesChanged( e ) {
			this.setState({
				visible_taxes: e.target.value
			}, this.save);
		}

		onUnitSelect( e ) {
			if ( e.target.checked ) {
				var units = $.extend(true, [], this.state.user_units);
				if ( -1 == units.indexOf( e.target.value ) ) {
					units.push( e.target.value );

					this.setState({
						user_units: units
					}, this.save);
				}
			}

		}

		render() {
			var dateFormats = [];
			var Currencies = [];

			Object.keys(this.props.settings.date_format_options).map((function (key) {
				dateFormats.push(
					<option value={key} key={'date-fomat-' + key}>{this.props.settings.date_format_options[key]}</option>
				);
			}).bind(this));


			Object.keys(this.props.settings.pref_currencies).map((function (key) {
				Currencies.push(
					<option value={key} key={'currency-' + key}>{this.props.settings.pref_currencies[key]}</option>
				);
			}).bind(this));

			var pref_prices_separators = [
				<option key="dot_comma" value="dot_comma">1.234.567,89</option>,
				<option key="space_comma" value="space_comma">1 234 567,89</option>,
				<option key="comma_dot" value="comma_dot">1,234,567.89</option>,
				<option key="space_dot" value="space_dot">1 234 567.89</option>,
			];

			var pref_paper_size = [
				<option key="US-Letter" value="US-Letter">US-Letter</option>,
				<option key="A4" value="A4">A4</option>
			];

			return (
				<div className="row">
					<div className="pane-row company-logo-row">
						<div className="pane company-logo">
							<label className="control-label">
								Company Logo
								{/*<ul className="nav nav-tabs" role="tablist">
									<li><a href="#company-logo-tab" data-toggle="tab" id="company-logo-link">Company Logo</a></li>
									<li><a href="#profile-photo-tab" data-toggle="tab" id="profile-photo-link">Profile photo</a></li>
								</ul>*/}
							</label>
							
							<div className="tab-content value">
								<div className="row tab-pane active" id="company-logo-tab">
									<div className="col-lg-12">
										<ImageUploader
											image={this.state.logo}
											imageId={this.state.logo_id}
											imageSize="logo_medium"
											uploadNonce={this.props.settings.logo_upload_nonce}
											uploadAction={this.props.settings.logo_upload_action}
											onImageUploaded={this.onLogoUploaded}
										/>
										<br />
										<p className="logo-on-proposal">
											<label><input type="checkbox" checked={this.state.logo_on_proposal} onChange={this.logoOnProposalChange} /> Include logo on proposal</label>
										</p>
									</div>
								</div>
								{/*<div className="row tab-pane" id="profile-photo-tab">
									<div className="col-lg-12">
										<ImageUploader
											image={this.state.photo}
											imageId={this.state.photo_id}
											imageSize="logo_medium"
											uploadNonce={this.props.settings.photo_upload_nonce}
											uploadAction={this.props.settings.photo_upload_action}
											onImageUploaded={this.onPhotoUploaded}
										/>
									</div>
								</div>*/}
							</div>
						</div>
					</div>
					<div className="profile-company-details">
						<div className="edit-company-address">
							<div className="form-layout pane-row company-address">
								<div className="pane">
									<label className="control-label">Company</label>
									<div className="value">
										<input type="text" className="form-control" id="company" value={this.state.company} data-name="company" onChange={this.onFieldChanged} />
									</div>
								</div>
								<div className="pane">
									<label className="control-label">Address</label>
									<div className="value">
										<input type="text" className="form-control" id="address" value={this.state.address} data-name="address" onChange={this.onFieldChanged} />
									</div>
								</div>
							</div>
							<div className="form-layout pane-row phone-website">
								<div className="pane">
									<label className="control-label">Phone</label>
									<div className="value">
										<input type="text" className="form-control" id="phone" value={this.state.phone} data-name="phone" onChange={this.onFieldChanged} />
									</div>
								</div>
								<div className="pane">
									<label className="control-label">Website</label>
									<div className="value">
										<input type="text" className="form-control" id="website" value={this.state.website} data-name="website" onChange={this.onFieldChanged} />
									</div>
								</div>
							</div>
						</div>
					</div>
					{/*<div className="pane-row profile-password">
						<div className="edit-profile-password"></div>
					</div>*/}
					<div className="pane-row invoicing-settings-pane">
						<div className="invoicing-settings-table">
							<div className="pane ccr">
								<label className="control-label">Charge Card Rate</label>
								<div className="value">
									<FloatInput
										type="text"
										className="form-control"
										value={this.state.card}
										onChange={this.onFieldChanged}
										data-name="card"
									/>
								</div>
							</div>

							<div className="pane date-format">
								<label className="control-label">Date Format</label>
								<div className="value">
									<ReactSelect2 ref="date_format" className="date-format-select" children={dateFormats} selectedValue={this.state.date_format} onChange={this.onDateFormatChanged} />
								</div>
							</div>
							<div className="pane currencies">
								<label className="control-label">Currency</label>
								<div className="value">
									<ReactSelect2 ref="currency" className="currency-select" children={Currencies} selectedValue={this.state.pref_currency} onChange={this.onCurrencyChanged} disabled={true} />
								</div>
							</div>
							<div className="pane paper-size" >
								<label className="control-label">PDF sheet size</label>
								<div className="value">
									<ReactSelect2 ref="paper_size" className="paper-size-select" children={pref_paper_size} selectedValue={this.state.pdf_sheet_size} onChange={this.onPaperSizeChanged} />
								</div>
							</div>
							<div className="pane price-separator" >
								<label className="control-label">Decimal Structure</label>
								<div className="value">
									<ReactSelect2 ref="prices_separator" className="prices-separator" children={pref_prices_separators} selectedValue={this.state.prices_separator} onChange={this.onPricesSeparatorChanged} />
								</div>
							</div>
						</div>
					</div>
					<div className="pane-row">
						<div className="pane libraries">
							<label className="control-label">Flower Libraries</label>
							<div className="value">
								<ul className="form-control libraries">
									{this.props.settings.libraries.map(this.renderLibrary)}
								</ul>
							</div>
						</div>
						<div className="pane">
							<label className="control-label">Number of Taxes</label>
							<div className="value">
								<ReactSelect2 selectedValue={this.state.visible_taxes} onChange={this.onVisibleTaxesChanged}>
									<option value="1">1</option>
									<option value="2">2</option>
								</ReactSelect2>
							</div>
						</div>
						<div className="pane libraries">
							<label className="control-label">Measuring Units</label>
							<div className="value">
								<ul className="form-control">
									{this.props.settings.measuring_units.map(function(unit){
										return (
											<li key={'unit-' + unit.id}>
												<label>
													<input
														name="unit"
														type="checkbox"
														value={unit.id}
														checked={-1 !== this.state.user_units.indexOf( unit.id )}
														disabled={-1 !== this.state.user_units.indexOf( unit.id )}
														onChange={this.onUnitSelect}
													/> {unit.singular}
												</label>
											</li>
										);
									}.bind(this))}
								</ul>
							</div>
						</div>
					</div>
				</div>
			);
		}
	}

	$(document).on('stemcounter.action.renderProfileSettingsForm', function( e, settings ){
		ReactDOM.render(
			<ProfileSettings settings={settings} />,
			settings.node
		);
	});
})(jQuery);