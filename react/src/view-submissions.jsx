(function($){
	"use strict";

	class ViewSubmission extends SC_Component {
		constructor( props ) {
			super( props );

			var state = {};
			state.loading = true;

			this.state = state;
		}

		componentDidMount() {
			$.get(
				stemcounter.ajax_url,
				{
					action: 'sc/form/submission/get',
					submission_id: this.props.submission
				},
				this.onSubmissionFetched
			);
		}

		onSubmissionFetched( r ) {
			if ( ! r.success ) {
				alertify.error( r.message );
				this.closeModal();

				return;
			}

			var submission = r.payload.submission;
			var answers;
			var submission_answers = [];
			// This consolidates all the answers to multi-option questions into a single answer object
			for (var i = 0; i < submission.answers.length; i++) {
				if ( undefined === _.findWhere( submission_answers, { question_id: submission.answers[i].question_id } ) ) {
					answers = _.pluck( _.where( submission.answers, { question_id: submission.answers[i].question_id } ), 'answer' );
					submission.answers[i].answer = answers;
					submission_answers.push( $.extend( true, {}, submission.answers[i] ) );
				}
			}

			submission.answers = submission_answers;

			$('.form-submissions-table tr[data-submission="' + this.props.submission + '"]').removeClass('not-seen');

			this.setState({
				loading: false,
				submission: submission
			});
		}

		closeModal() {

		}

		render() {
			if ( this.state.loading ) {
				return (
					<div className="ajax-modal-loader">
						<i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
						<span className="sr-only">Loading...</span>
					</div>
				);
			}

			return (
				<div className="form-layout">
					{this.state.submission.answers.map(function(answer){
						return (
							<div key={'answer-' + answer.id} className="modalpane-row clearfix">
								<label htmlFor="" className="col-xs-12 modalpane control-label">
									<div className="lbl">{answer.label}</div>
									{answer.answer.map(function(answer, i){
										return (
											<div key={'answer-' + i} className="value">{answer}</div>
										);
									})}
								</label>
							</div>
						);
					})}
					{/*<div className="modal-footer">
						<button type="submit" className="btn btn-primary">Save</button>
					</div>*/}
				</div>
			);
		}
	}

	$(document).ready(function(){
		$('.form-submissions-table').on('click', 'tbody tr .archive-submission', function(e){
			e.preventDefault();
			e.stopImmediatePropagation();

			var submission = $(this).closest('tr').attr('data-submission');

			alertify.confirm( 'Are you sure you want to archive this submission?', function() {
				$.post(
					stemcounter.ajax_url,
					{
						action: 'sc/form/submission/archive',
						submission: submission
					},
					function( r ) {
						if ( r.success ) {
							$('.form-submissions-table tr[data-submission="' + submission + '"]').remove();
							alertify.success( 'The submission was archived successfully.' );
						} else {
							alertify.error( r.message );
						}
					}
				);
			} );
		}).on('click', ' tbody tr', function(){
			var $modalWrapper = GenericModal.prototype.createWrapper('new-modal view-form-submission-modal'),
				modalContent = <ViewSubmission submission={$(this).attr('data-submission')} />;

			ReactDOM.render(
				<GenericModal
					title="Form Submission"
					content={modalContent}
					noFooter={true}
				/>,
				$modalWrapper[0]
			);
		});

		// Unarchive submissions
		$('.form-submissions-table').on('click', 'tbody tr .unarchive-submission', function (e) {
			e.preventDefault();
			e.stopImmediatePropagation();

			var submission = $(this).closest('tr').attr('data-submission');

			alertify.confirm('Are you sure you want to unarchive this submission?', function () {
				$.post(stemcounter.ajax_url, {
					action: 'sc/form/submission/unarchive',
					submission: submission
				}, function (r) {
					if (r.success) {
						$('.form-submissions-table tr[data-submission="' + submission + '"]').remove();
						alertify.success('The submission was unarchived successfully.');
					} else {
						alertify.error(r.message);
					}
				});
			});
		})
	});
})(jQuery);
