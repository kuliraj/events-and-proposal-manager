(function($){
'use strict';

class EmailTemplates extends SC_Component {
	constructor(props) {
		super(props);
		var state = {};
		state.templates = this.props.templates ? this.props.templates : [];
		state.selected_template = this.props.templates[0].id;
		for (var index = 0; index < state.templates.length; index++) {
			if (parseInt(state.templates[index].default, 10)) {
				state.selected_template = parseInt(state.templates[index].id, 10);
				break;
			}
		}
		this.sendRequestDebounced = _.debounce(
			this.sendRequestDebounced,
			1000,
			{
				leading: false
			}
		);
		this.state = state;
	}
	onTemplateChange(e) {
		this.setState({
			selected_template: parseInt( e.value, 10 )
		});

	}
	handleContent(content, delta, source, editor) {
		var updatedState = $.extend(true, {}, this.state);
		var template = null;
		for (var index = 0; index < updatedState.templates.length; index++) {
			if (parseInt(updatedState.templates[index].id, 10) == parseInt(updatedState.selected_template, 10)) {
				template = updatedState.templates[index];
				updatedState.templates[index].content = content;
				break;
			}
		}
		this.setState(updatedState, function(){
			this.sendRequestDebounced(template);
		});
	}
	handleChange(e) {
		var updatedState = $.extend(true, {}, this.state);
		var template = null;
		for (var index = 0; index < updatedState.templates.length; index++) {
			template = updatedState.templates[index];
			if (parseInt(updatedState.templates[index].id, 10) == parseInt(updatedState.selected_template, 10)) {
				switch (e.target.dataset.type) {
					case 'default':
						updatedState.templates[index].default = e.target.checked ? 1 : 0;
						break;
					case 'label':
						updatedState.templates[index].label = e.target.value;
						break;
					case 'subject':
						updatedState.templates[index].subject = e.target.value;
						break;
					default:
						break;
				}
				break;
			}
		}
		this.setState( updatedState );
	}
	sendRequestDebounced( template ) {
		this.sendRequest( template );
	}
	sendRequest(template) {
		if (null === template) {
			return;
		}
		var _this = this,
			updatedState = $.extend(true, {}, this.state),
			url = window.stemcounter.aurl({ action: 'sc_edit_user_email_template' }),
			data = {
				template: template
			};

		$.post(url, data, function (response) {
			if (response.success && response.payload.template_id) {
				for (var i = 0; i < updatedState.templates.length; i++) {
					if ( 1 == parseInt(template.default, 10) ) {
						updatedState.templates[i].default = 0;
					}
					if (parseInt(updatedState.templates[i].id, 10) == parseInt(template.id, 10)) {
						updatedState.templates[i].id = parseInt(response.payload.template_id, 10);
						updatedState.templates[i].default = template.default;
						updatedState.selected_template = parseInt(response.payload.template_id, 10);
					}
				}
				_this.setState(updatedState);
			}
		});
	}
	addTemplate(e) {
		e.preventDefault();

		if ( ! stemcounter.AC.can_access( stemcounter.AC.STUDIO )) {
			alertify.error('You must upgrade to a Studio membership in order to create more templates!');
			return;
		}

		var updatedState = $.extend(true, {}, this.state);
		var template = {
			id: -1,
			label: 'New Template',
			subject: 'Example Subject',
			content:'<p>Here is your proposal. You can reply to this email with any questions that we can answer for you.</p>',
			default: 0
		}
		updatedState.templates.push( template );
		updatedState.selected_template = -1;
		this.setState(updatedState, function(){
			this.sendRequest(template);
		});
	}
	render() {
		var _this = this;
		var template = this.state.templates[0];
		var templateOptions = [];
		var addNewTemplate = null;

		if (this.state.templates.length) {
			this.state.templates.map((function(_template, index, arr) {
				if (_this.state.selected_template == parseInt(_template.id, 10)) {
					template = _template;
				}
				templateOptions.push({
					value: _template.id,
					label: _template.label + ( 1 == parseInt( _template.default, 10 ) ? ' (default)' : '' )
				});
				return false;
			}));
		}

		if ( ! stemcounter.AC.can_access( stemcounter.AC.STUDIO )) {
			addNewTemplate = (
				<div className="value">
					<div className="alert alert-warning">
						<p>Want more templates? <a href="/upgrade/">Upgrade to Studio</a></p>
					</div>
				</div>
			);
		} else {
			addNewTemplate = (
				<a href="#" onClick={this.addTemplate} className="cta-link">Add Template</a>
			);
		}

		return (
			<div>
				<div className="form-layout pane-row">
					<div className="row">
						<div className="col-sm-6 col-md-4">
							<div className="value">
								<VirtualizedSelect
									clearable={false}
									searchable={false}
									scrollMenuIntoView={false}
									className="template-select"
									value={this.state.selected_template}
									onChange={this.onTemplateChange}
									options={templateOptions}
									optionHeight={32}
								/>
							</div>
						</div>
						<div className="col-sm-6 col-md-push-2">
							{addNewTemplate}
						</div>
					</div>
				</div>
				<div className="row">
					<EmailTemplate
						template={template}
						handleChange={this.handleChange}
						handleContent={this.handleContent}
						sendRequest={this.sendRequest}
					/>
				</div>
			</div>
		);
	}
}

class EmailTemplate extends SC_Component {
	constructor(props) {
		super(props);
		var state = {};
		this.state = state;
	}
	saveTemplate() {
		this.props.sendRequest( this.props.template );
	}
	render() {
		return (
			<div>
				<div className="row">
					<div className="col-xs-12 col-md-6">
						<label className="control-label">Template Name</label>
						<div className="value">
							<input type="text" className="form-control" data-type="label" value={this.props.template.label} onChange={this.props.handleChange} onBlur={this.saveTemplate} placeholder="Template Name(only visible to you)" />
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-12 col-md-6">
						<label className="control-label">Default Template? <input type="checkbox" className="" data-type="default" value="yes" checked={(parseInt(this.props.template.default, 10) ? true : false )} onChange={this.props.handleChange} onBlur={this.saveTemplate} /></label>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-12 col-md-6">
						<label className="control-label">Email Subject</label>
						<div className="value">
							<input type="text" data-type="subject" className="form-control" value={this.props.template.subject} onChange={this.props.handleChange} onBlur={this.saveTemplate} placeholder="Email Subject" />
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-12 col-lg-6">
						<label className="control-label">Email Content</label>
						<div className="value">
							<ReactQuill
								theme="snow"
								value={this.props.template.content}
								onChange={this.props.handleContent}
								modules={{
									toolbar: [
										[{ 'header': [1, 2, 3, false] }],
										['bold', 'italic', 'underline', 'link', { 'align': [] }],
										[{'list': 'ordered'}, {'list': 'bullet'}, 'clean']
									],
									clipboard: {
										matchVisual: false
									}
								}}
							/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

$(document).on('stemcounter.action.renderEmailTemplates', function(e, settings) {
	ReactDOM.render(
		<EmailTemplates
			templates={settings.templates}
		/>,
		$(settings.node).get(0)
	);
});

})(jQuery);