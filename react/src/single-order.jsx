(function($){
	"use strict";

	var SCOrders = React.createClass({
		getInitialState: function(){
			this.getEventName = _.memoize( this.getEventName );
			this.bumpSu = _.debounce(this.bumpSu, 2000 );

			return {
				items: this.props.items,
				processing: false,
				comparing_latest: false,
				last_toggled_included: null,
				last_toggled_excluded: null,
				new_split_order_name: null,
				su_changes: {
					order_id: null,
					items: {}
				},
				breakdown: false,
				checked: {
					included: {},
					discarded: {},
					extra: {}
				},
				refresh_checked: {}
			};
		},

		componentDidMount: function(){
			if (this.refs.fulfilment_date) {
				var _this = this;
				$(this.refs.fulfilment_date).datepicker({ 
					dateFormat: this.props.dateFormat,
					onClose: function(val, opts) {
						_this.updateFulfilmentDate( val );
					}
				});
			}
			this.original_order_state =  this.state;
		},

		onItemRefreshCheckUncheck: function(e) { console.log( e );
			var updatedState = {},
				type = e.target.dataset.type,
				refresh_checked = $.extend(true, {}, this.state.refresh_checked);
			if ( e.target.checked ) {
				refresh_checked[ e.target.value ] = true;
			} else {
				delete refresh_checked[ e.target.value ];
			}

			updatedState.refresh_checked = refresh_checked;
			this.setState( updatedState );
		},

		onItemCheckUncheck: function(e){
			var updatedState = {},
				type = e.target.dataset.type,
				checked = $.extend(true, {}, this.state.checked),
				skip_single_check = false;

			if ( e.nativeEvent.shiftKey && null !== this.state[ 'last_toggled_' + type ] ) {
				var selection = Object.keys( this.state.items[ type ] ),
					prev_cb = this.state[ 'last_toggled_' + type ],
					prev_index = selection.indexOf( prev_cb ),
					new_index = selection.indexOf( e.target.dataset.key ),
					all_checked = true;

				if ( -1 != prev_index && -1 != new_index && prev_index != new_index ) {
					skip_single_check = true;

					if ( prev_index < new_index ) {
						selection = selection.slice( prev_index, new_index + 1 );
					} else {
						selection = selection.slice( new_index, prev_index + 1 );
					}

					for ( var i = 0; i < selection.length; i++ ) {
						if ( undefined === checked[ type ][ selection[i] ] ) {
							all_checked = false;
							break;
						}
					}

					for ( var i = 0; i < selection.length; i++ ) {
						if ( all_checked ) {
							delete checked[ type ][ selection[i] ];
						} else {
							checked[ type ][ selection[i] ] = true;
						}
					}
				}
			}

			if ( ! skip_single_check ) {
				if ( ! e.target.checked ) {
					delete checked[ e.target.dataset.type ][ e.target.dataset.key ];
				} else {
					checked[ e.target.dataset.type ][ e.target.dataset.key ] = true;
				}
			}

			updatedState.checked = checked;
			updatedState[ 'last_toggled_' + type ] = e.target.dataset.key;

			this.setState( updatedState );
		},

		toggleCheckboxes: function(e){
			var checked = $.extend(true, {}, this.state.checked),
				type = e.target.dataset.type;

			checked[ type ] = {};

			if ( e.target.checked ) {
				Object.keys(this.state.items[type]).map(function(key){
					checked[ type ][key] = true;
				});
			}

			this.setState({
				checked: checked
			});
		},

		transferItems: function(e){
			e.preventDefault();

			var type = e.target.dataset.type,
				th = this;

			this.setState({
				processing: true
			});

			$.post(stemcounter.ajax_url, {
				action: 'sc/order/transfer_items',
				from: type,
				oid: this.props.order.id,
				item_keys: Object.keys(this.state.checked[ type ])
			}, function(response){
				stemcounter.JSONResponse(response, function (r) {
					if (r.success) {
						var checked = $.extend(true, {}, th.state.checked);
						checked[ type ] = {};

						th.setState({
							checked: checked,
							processing: false,
							items: r.payload.items
						});

						alertify.success( r.payload.message );
					} else {
						th.setState({
							processing: false
						});

						alertify.error( r.message );
					}
				});
			});
		},

		onSplitNameChange: function( new_split_order_name ) {
			this.setState({
				new_split_order_name: new_split_order_name
			});
		},

		splitOrderClicked: function(e) {
			e.preventDefault();
			var th = this,
				$modalWrapper = GenericModal.prototype.createWrapper('split-order-modal new-modal'),
				modalContent = <SplittedOrderModalContents oldOrderName={this.props.order.name} onNameChange={this.onSplitNameChange} onSave={this.splitOrder1} />;

			ReactDOM.render(
				<GenericModal
					title='Split order'
					content={modalContent}
					onSave={this.splitOrder}
					saveLabel="Split Order"
				/>,
				$modalWrapper[0]
			);
			return false;
		},

		splitOrder: function(e){
			//e.preventDefault();

			var type = 'included',
				th = this;

			this.setState({
				processing: true
			});

			$.post(stemcounter.ajax_url, {
				action: 'sc/order/split',
				oid: this.props.order.id,
				item_keys: Object.keys(this.state.checked[ type ]),
				new_split_order_name: this.state.new_split_order_name
			}, function(response){
				stemcounter.JSONResponse(response, function (r) {
					if (r.success) {
						var checked = $.extend(true, {}, th.state.checked);
						checked[ type ] = {};

						th.setState({
							checked: checked,
							items: r.payload.items
						});

						alertify.success( r.payload.message );
					} else {
						alertify.error( r.message );
					}
				});
				th.setState({
					processing: false
				});
			});
		},

		onBreakdownCheckUncheck: function(e) {
			this.setState({
				'breakdown': !this.state.breakdown
			});
		},

		onEmailOrder: function( $modal ){
			var th = this;

			this.setState({
				processing: true
			});

			var data = $modal.find('form').serialize() + '&action=sc/order/email&oid=' + this.props.order.id;

			$.post(stemcounter.ajax_url, data, function(response){
				stemcounter.JSONResponse(response, function (r) {
					if ( r.success ) {
						alertify.success( r.payload.message );
					} else {
						alertify.error( r.message );
					}
				});
				th.setState({
					processing: false
				});
			});
		},

		updateOrderNote: function(e){
			e.preventDefault();

			var notes = e.target.value,
				oid = this.props.order.id;

			$.post(stemcounter.ajax_url, {
				action: 'sc/order/update_notes',
				'oid': oid,
				'notes': notes
			});
		},

		updateFulfilmentDate: function( date ){
			$.post(stemcounter.ajax_url, {
				action: 'sc/order/update_fulfilment_date',
				'oid': this.props.order.id,
				'date': date
			});
		},

		deleteItems: function(e){
			e.preventDefault();

			var type = 'BUTTON' == e.target.tagName ? e.target.dataset.type : e.target.parentElement.dataset.type,
				th = this;

			alertify.confirm('Are you sure you want to delete the selected items? They will be completely removed from this order.', function(){
				th.setState({
					processing: true
				});

				$.post(stemcounter.ajax_url, {
					action: 'sc/order/delete_items',
					'oid': th.props.order.id,
					'items_to_delete': Object.keys( th.state.checked[ type ] )
				}, function(response){
					stemcounter.JSONResponse(response, function (r) {
						if ( r.success ) {
							alertify.success( r.payload.message );
							th.setState({
								items: r.payload.items
							});
						} else {
							alertify.error( r.message );
						}
					});
					th.setState({
						processing: false
					});
				});
			});
		},

		openMailPopup: function(e){
			e.preventDefault();
			//var fulfilment_date = $('.fulfilment-date' ).val();
			var fulfilment_date = '' != $.trim( $('.fulfilment-date').val() ) ? $('.fulfilment-date').datepicker('getDate').valueOf() : '';

			var th = this,
				$modalWrapper = GenericModal.prototype.createWrapper('email-order-modal new-modal'),
				recipient = e.target.dataset.recipient,
				modalContent = <OrderEmailModalContents email_addr={recipient} fulfilment_date={fulfilment_date} onSave={this.onEmailOrder} />;

			ReactDOM.render(
				<GenericModal
					title={( 'sanders@mayesh.com' == recipient ? 'Order From Mayesh' : 'Email Order' )}
					content={modalContent}
					onSave={this.onEmailOrder}
					saveLabel="Email Order"
				/>,
				$modalWrapper[0]
			);
		},

		getLatestOrderData: function(e) {
			e.preventDefault();
			var th = this;
			th.setState({
				processing: true
			});
			$.post(stemcounter.ajax_url, {
				action: 'sc_refresh_user_order',
				'order_id': this.props.order.id,
				'save_order': 0
			}, function(response){
				stemcounter.JSONResponse(response, function (r) {
					if ( r.success ) {
						alertify.success( r.message );

						//Make all checkboxes for renewal to be checked, 
						//so we fill our refresh_checked array with the items from the response
						var refresh_checked = {};
						for ( var k in r.payload.items ) {
							for ( var key in r.payload.items[k] ) {
								if ( r.payload.items[k][key].quantity != r.payload.items[k][key].quantity_new || 'extra' == k ) {
									refresh_checked[ r.payload.items[k][key].item_id ] = 1;
								}
							}
						}

						th.setState({
							'items': r.payload.items,
							'comparing_latest': true,
							'refresh_checked': refresh_checked,
							'checked': {
								'included': {},
								'discarded': {},
								'extra': {},
							},
							processing: false
						});
					} else {
						alertify.error( r.message );
					}
				});
				th.setState({
					processing: false
				});
			});
		},

		bumpClicked: function(e) {
			e.preventDefault();

			var type = e.target.dataset.type;
			var key = e.target.dataset.key;
			var item = $.extend( true, {}, this.state.items[type][key] );
			var step = parseInt( e.target.dataset.step );
			
			if ( undefined == item.bkdwn ) {
				item.bkdwn = [];
			}
			if (undefined == item.bkdwn['0']) {
				item.bkdwn['0'] = 0;
			}

			if (0 > step && 0 >= item.bkdwn['0'] ) {
				alertify.error( "No more manually added items" );
				return false;
			}

			var state = {};
			state.items = jQuery.extend( true, {}, this.state.items );

			var item_id = this.state.items[type][key].item_id;
			var quantity = parseFloat( this.state.items[type][key].quantity.toFixed10(2) );
			var items_su = parseFloat( this.state.items[type][key].items_su.toFixed10(2) );
			var su_needed = parseFloat( this.state.items[type][key].su_needed.toFixed10(2) );
			var cost = parseFloat( this.state.items[type][key].cost.toFixed10(2) );
			var su_changes = $.extend( true, {}, this.state.su_changes );
			su_changes.order_id = this.props.order.id;
			
			if ( undefined == su_changes.items[item_id] ) {
				su_changes.items[item_id] =  {
					step: step
				}
			} else {
				su_changes.items[item_id].step += step;
			}
			
			var price = cost / su_needed;
			su_needed += step;
			quantity += step * items_su;
			cost = su_needed * price;

			item.bkdwn['0'] += step * items_su;
			item.su_needed = su_needed;
			item.quantity = quantity;
			item.cost = cost;

			state.items[type][key] = item;
			state.su_changes = su_changes;

			this.setState( state );
			this.bumpSu();

		},

		bumpSu: function( ) {
			var th = this;

			this.ajaxReq =	$.post(stemcounter.ajax_url, {
					'action': 'sc_bump_su',
					'su_changes': this.state.su_changes
				}, function( response ) {
					stemcounter.JSONResponse(response, function (r) {
						if ( r.success ) {
							//document.location.reload();
						} else {
							alertify.error( r.message );
						}
					});
				}).always(function(){
					var latest_data = th.ajaxReq.latest_data;
					th.ajaxReq = null;
					if ( undefined !== latest_data ) {
						th.bumpSu( latest_data );
					}
				});

			th.setState({
				su_changes: {
					order_id: null,
					items: {}
				},
			});
		},

		saveRefreshedOrder: function(e) {
			e.preventDefault();
			var th = this;
			th.setState({
				processing: true
			});
			$.post(stemcounter.ajax_url, {
				action: 'sc_refresh_user_order',
				'order_id': this.props.order.id,
				'refresh_checked': this.state.refresh_checked,
				'save_order': 1
			}, function(response){
				stemcounter.JSONResponse(response, function (r) {
					if ( r.success ) {
						alertify.success( r.message );
						th.setState({
							'items': r.payload.items,
							'comparing_latest': false,
						});
					} else {
						alertify.error( r.message );
					}
				});
				th.setState({
					processing: false
				});
			});
		},

		cancelRefreshedOrder: function(e) {
			e.preventDefault();
			var th = this;
			this.setState( this.original_order_state );
		},

		getEventName: function(event_id) {
			var event = _.findWhere( this.props.order.events, {
				id: parseInt( event_id, 10 )
			} );

			return event ? event.name : 'Manually added';
		},

		onShowReplaceFlower: function( e ) {
			var el = 'span' === e.target.nodeName ? e.target : $(e.target).closest('span')[0];
			e.preventDefault();

			var state = $.extend( true, {}, this.state );
			state.items[el.dataset.type][el.dataset.key].show_replace = true;
			this.setState(state);
		},

		onShowReplaceFlowerCancel: function( k, key ) {
			var state = $.extend( true, {}, this.state );
			state.items[k][key].show_replace = false;
			this.setState(state);
		},

		onReplaceFlowerResult: function( items ) {
			this.setState({
				'items': items,
				'comparing_latest': false,
			});
		},

		renderItems: function( k ) {
			if ( 'extra' == k ) {
				return null;
			}
			var total = 0,
				label,
				button,
				delete_button,
				split_order = null,
				email_button = null,
				email_mayesh_button = null,
				overlay = null;

			if ( this.state.comparing_latest ) {
				var ComparingQuantityColumnHeader =	<th className="shopping-td-number-new diff-new"># new</th>
				var ComparingQuantityEmptyColumn = <td className="diff-new"></td>;
			} else {
				var ComparingQuantityColumnHeader = null;
				var ComparingQuantityEmptyColumn = null;
			}

			if ( 'discarded' == k ) {
				label = 'Discarded';
				button = 'Restore';
			} else if ( 'extra' == k ) {
				label = 'Extra';
				button = null;
			} else {
				label = 'Items';
				button = 'Discard';
			}

			if ( Object.keys(this.state.checked[k]).length ) {
				button = <button type="button" onClick={this.transferItems} data-type={k} className="btn btn-xs btn-primary transfer-items">{button}</button>;
				delete_button = <button type="button" onClick={this.deleteItems} data-type={k} className="btn btn-xs btn-primary btn-danger delete-items"><i className="fa fa-trash" /> Delete Selected Items</button>;
				if ( 'included' == k ) {
					split_order = <button type="button" onClick={this.splitOrderClicked} className="btn btn-xs btn-primary split-order">Split Order</button>;
				}
			} else {
				button = null;
			}

			if ( this.state.processing ) {
				overlay = (<div className="processing-overlay"></div>);
			}

			if ( 'included' == k ) {
				email_mayesh_button = <button type="button" onClick={this.openMailPopup} data-type={k} data-recipient="sanders@mayesh.com" className="btn alignright btn-primary mayesh-email mail-order">Order From Mayesh</button>;
				email_button = <button type="button" onClick={this.openMailPopup} data-type={k} data-recipient="" className="btn alignright btn-primary mail-order"><i className="fa fa-envelope" /></button>;
			}

			return (
				<div className="row mt">
					<div className="col-lg-12">
						<h4 className="clearfix">
							{label} {split_order} {button} {delete_button} {email_button} {email_mayesh_button}
						</h4>
						<section id="unseen">
							<table className="table table-bordered table-striped table-condensed">
								<thead>
									<tr>
										<th className="shopping-td-cb"></th>
										<th className="shopping-td-flower">Type</th>
										<th className="shopping-td-flower">Item</th>
										<th className="shopping-td-number numeric" style={{'width': '15%'}}>#</th>
										{ComparingQuantityColumnHeader}
										<th className="shopping-td-items-su">Items / PU</th>
										<th className="shopping-td-su-needed">Purchase Units Needed</th>
										<th className="shopping-td-cost">Expected Cost</th>
										<th className="shopping-td-leftovers">Leftovers</th>
									</tr>
								</thead>
								<tbody>
									{Object.keys(this.state.items[k]).map((function (key) {
										total += parseFloat(this.state.items[k][key].cost);
										var qty_measuring_unit = '';
										var needed_measuring_unit = '';
										var leftovers_measuring_unit = '';

										if (this.state.items[k][key].hasOwnProperty('purchase_unit') && stemcounter.page.measuring_units && stemcounter.page.measuring_units.length ) {
											for (var i = 0; i < stemcounter.page.measuring_units.length; i++) {
												var unit = stemcounter.page.measuring_units[i];
												if (this.state.items[k][key].purchase_unit == unit.id) {
													if ( 1 == parseFloat(this.state.items[k][key].items_su) ) {
														qty_measuring_unit = unit.short;
													} else {
														qty_measuring_unit = unit.short_plural;
													}
													if ( 1 == parseFloat(this.state.items[k][key].su_needed) ) {
														needed_measuring_unit = unit.short;
													} else {
														needed_measuring_unit = unit.short_plural;
													}
													if ( 1 == parseFloat(this.state.items[k][key].leftovers) ) {
														leftovers_measuring_unit = unit.short;
													} else {
														leftovers_measuring_unit = unit.short_plural;
													}
													break;
												}
											}
										}
										if ( this.state.comparing_latest ) {
											if ( this.state.items[k][key].quantity != this.state.items[k][key].quantity_new ) {
												var checkboxes = <label><input type="checkbox" value={this.state.items[k][key].item_id} checked={undefined !== this.state.refresh_checked[ this.state.items[k][key].item_id ]} onChange={this.onItemRefreshCheckUncheck} /></label>
											}
										} else {
											var checkboxes = <label><input type="checkbox" value={key} checked={undefined !== this.state.checked[ k ][ key ]} onChange={this.onItemCheckUncheck} data-type={k} data-key={key} /></label>;
										}

										var differenceClass = ( this.state.items[k][key].quantity < this.state.items[k][key].quantity_new ) ?
										'diff-positive' : null;
										differenceClass = ( this.state.items[k][key].quantity > this.state.items[k][key].quantity_new ) ?
										'diff-negative' : differenceClass;

										var ComparingQuantityColumn = ( this.state.comparing_latest ) ?
											<td className={`shopping-td-number-new diff-new ${differenceClass}`}>{this.state.items[k][key].quantity_new}</td> :
											null,
											breakdown = null;

										if ( this.state.breakdown && !this.state.comparing_latest ) {
											breakdown = <ItemBreakDown getEventName={this.getEventName} items={this.state.items[k][key].bkdwn} events={this.props.order.events} />
										}

										var replace_flower_select = null;
										if ( true == this.state.items[k][key].show_replace ) {
											replace_flower_select = (
												<ReplaceFlowerSelect
													itemType={k}
													itemKey={key}
													type="order"
													typeId={this.props.order.id}
													value={this.state.items[k][key].item_id}
													onCancel={this.onShowReplaceFlowerCancel}
													onRefresh={this.onReplaceFlowerResult}
												/>
											);
										}

										var su_needed_controls = ( this.state.comparing_latest ) ? null :
											<span className="change-numeric">
												<span
													data-step="1"
													data-type={k}
													data-key={key}
													onClick={this.bumpClicked} className="control">+
												</span>
												<span
													data-step="-1"
													data-type={k}
													data-key={key}
													onClick={this.bumpClicked} className="control">-
												</span>
											</span>

										return (
											<tr key={k + '-item-' + key}>
												<td className="shopping-td-cb">
													{checkboxes}
												</td>
												<td>{this.state.items[k][key].type}</td>
												<td className="shopping-td-flower">
													{this.state.items[k][key].name}
													{breakdown}
													{ ( k== 'discarded' ) ? null : 
														<span className="btn replace-flower hidden-print" data-type={k} data-key={key} onClick={this.onShowReplaceFlower}>
															<i className="fa fa-pencil" aria-hidden="true"></i>
														</span>
													}
														<span>{replace_flower_select}</span>
												</td>
												<td className="shopping-td-number numeric">{stemcounter.maybeStripDecimals(this.state.items[k][key].quantity)}</td>
												{ComparingQuantityColumn}
												<td>{stemcounter.maybeStripDecimals(this.state.items[k][key].items_su) + ' ' + qty_measuring_unit}</td>
												<td>
													<div className="su-needed-container">
													{stemcounter.maybeStripDecimals(this.state.items[k][key].su_needed) + ' ' + needed_measuring_unit}
													{su_needed_controls}
													</div>
												</td>
												<td>{stemcounter.formatPrice(this.state.items[k][key].cost, true, stemcounter.currencySymbol)}</td>
												<td>{stemcounter.maybeStripDecimals(this.state.items[k][key].leftovers) + ' ' + leftovers_measuring_unit}</td>
											</tr>
										);
									}).bind(this))}

									{Object.keys(this.state.items['extra']).map((function (key) {
										//Add extra items at the end of the included items
										if ( !this.state.comparing_latest ) return null;
										if ( 'included' != k ) return null;
										total += parseFloat(this.state.items['extra'][key].cost);
										var qty_measuring_unit = '';
										var needed_measuring_unit = '';
										var leftovers_measuring_unit = '';

										if (this.state.items['extra'][key].hasOwnProperty('purchase_unit') && stemcounter.page.measuring_units && stemcounter.page.measuring_units.length ) {
											for (var i = 0; i < stemcounter.page.measuring_units.length; i++) {
												var unit = stemcounter.page.measuring_units[i];
												if (this.state.items['extra'][key].purchase_unit == unit.id) {
													if ( 1 == parseFloat(this.state.items['extra'][key].items_su) ) {
														qty_measuring_unit = unit.short;
													} else {
														qty_measuring_unit = unit.short_plural;
													}
													if ( 1 == parseFloat(this.state.items['extra'][key].su_needed) ) {
														needed_measuring_unit = unit.short;
													} else {
														needed_measuring_unit = unit.short_plural;
													}
													if ( 1 == parseFloat(this.state.items['extra'][key].leftovers) ) {
														leftovers_measuring_unit = unit.short;
													} else {
														leftovers_measuring_unit = unit.short_plural;
													}
												}
											}
										}

										return (
											<tr key={'extra' + '-item-' + key} className="diff-new">
												<td className="shopping-td-cb">
													<label><input type="checkbox" value={this.state.items['extra'][key].item_id} checked={undefined !== this.state.refresh_checked[ this.state.items['extra'][key].item_id ]} onChange={this.onItemRefreshCheckUncheck} /></label>
												</td>
												<td>{this.state.items['extra'][key].type}</td>
												<td className="shopping-td-flower">{this.state.items['extra'][key].name}</td>
												<td className="shopping-td-number numeric">0</td>
												<td className="shopping-td-number-new diff-new">{this.state.items['extra'][key].quantity_new}</td>
												<td>{stemcounter.maybeStripDecimals(this.state.items['extra'][key].items_su) + ' ' + qty_measuring_unit}</td>
												<td>{stemcounter.maybeStripDecimals(this.state.items['extra'][key].su_needed) + ' ' + needed_measuring_unit}</td>
												<td>{stemcounter.formatPrice(this.state.items['extra'][key].cost, true, stemcounter.currencySymbol)}</td>
												<td>{stemcounter.maybeStripDecimals(this.state.items['extra'][key].leftovers) + ' ' + leftovers_measuring_unit}</td>
											</tr>
										);
									}).bind(this))}

									<tr>
										<td className="shopping-td-flower"></td>
										<td className="shopping-td-number numeric"></td>
										<td></td>
										<td></td>
										{ComparingQuantityEmptyColumn}
										<td></td>
										<td></td>
										<td>Total: {stemcounter.formatPrice(total, true, stemcounter.currencySymbol)}</td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</section>
					</div>
					{overlay}
				</div>
			);	
		},

		render: function() {
			var latest_data_button = <a href="#" title="Get latest event items" className="refresh-buttons latest-data" onClick={this.getLatestOrderData}></a>;
			var refresh_cancel_order_buttons = <span className="refresh-order-buttons">
					<a href="#" title="Save renewed order" className="refresh-buttons save-refreshed-order" onClick={this.saveRefreshedOrder}>
						<i className="fa fa-floppy-o"></i>
					</a>
					<a href="#" title="Return to original order" className="refresh-buttons cancel-refreshed-order" onClick={this.cancelRefreshedOrder}><i className="fa fa-times" aria-hidden="true"></i></a>
				</span>;
			var breakdown_checkbox = null;
			if ( !this.state.comparing_latest  ) {
				breakdown_checkbox = <label htmlFor="cta-breakdown" className="breakdown-lbl">
						<input type="checkbox" value="breakdown" id="cta-breakdown" checked={this.state.breakdown} onChange={this.onBreakdownCheckUncheck} /> Show Breakdown
					</label>
			}

			return (
				<div>
					<div className="row">
						<div className="col-lg-12">
							<h3 className="mb">
								{this.props.order.name}
								{( this.state.comparing_latest ) ? refresh_cancel_order_buttons : latest_data_button}
							</h3>
							
						</div>
					</div>

					{this.renderItems('included')}
					{this.renderItems('discarded')}
					{this.renderItems('extra')}

					<div className="row mt">
						<div className="col-lg-12">
							<h4>Events:</h4>
							{this.props.order.events.map(function(event, i){
								return (
									<div key={'event-' + i}>
										<h5><a href={event.shopping_list_url}>{event.name}</a></h5>
									</div>
								);
							})}
						{breakdown_checkbox}
						</div>
					</div>
					<div className="row">
						<div className="col-lg-12">
							<h4>Note: </h4>
							<textarea rows="6" className="form-control" defaultValue={this.props.order.notes} onBlur={this.updateOrderNote} />
						</div>
					</div>
					<div className="row">
						<div className="col-lg-12"><h4>Created On:</h4> <h5>{this.props.order.created_on}</h5></div>
					</div>
					<div className="row">
						<div className="col-lg-12">
							<h4>Fulfilment Date:</h4>
							<input type="text" className="form-control fulfilment-date" ref="fulfilment_date" defaultValue={this.props.order.fulfilment_date} />
						</div>
					</div>
				</div>
			);
		}
	});

	var ItemBreakDown = React.createClass({
		renderEventBreakdown: function( event_id ) {
			if ( 0 >= this.props.items[ event_id ] ) { //Hide, if there are no items
				return null;
			}

			return (
				<div className="breakdowned-row" key={'bkdwn-event-' + event_id}>
					<span className="bkdwn-item">{this.props.getEventName(event_id)}</span>:
					<span className="bkdwn-quantity"> {this.props.items[ event_id ]}</span>
				</div>
			);
		},

		render: function() {
			return (
				<div className="breakdowned">
					{Object.keys(this.props.items).map( this.renderEventBreakdown )}
				</div>
			);
		}
	});

	var SplittedOrderModalContents = React.createClass({
		getInitialState: function(){
			return {
				oldOrderName: 'Split from order "' + this.props.oldOrderName + '"',
			};
		},
		onNameChange: function(e) {
			this.setState({
				oldOrderName: e.target.value
			});
			this.props.onNameChange( e.target.value );
		},

		render: function(e) {
			return(
				<form action="" className="form-horizontal style-form split-order-form">
				<div className="split-order-form-wrap">
						
				<div className="modalpane-row clearfix">
					<label className="col-xs-12 modalpane control-label">
						<div className="lbl">
							<span>New order name</span>
							<span className="mandatory"></span>
						</div>
						<input type="text" placeholder="Enter new order name here" name="old_order_name" className="form-control" onChange={this.onNameChange} value={this.state.oldOrderName} />
					</label>
				</div>

				</div>
				</form>
			)
		}
	});

	var OrderEmailModalContents = React.createClass({
		onNotesChange: function(e){
			var iframe = this.refs.emailPreview;

			if ( iframe.contentWindow.updateNotes ) {
				iframe.contentWindow.updateNotes( e.target.value );
				iframe.height = iframe.contentWindow.document.body.scrollHeight;
			}

		},

		getInitialState: function() {
			var mayesh_classes = '';
			var fulfilment_classes = '';

			if ('sanders@mayesh.com' === this.props.email_addr) {
				fulfilment_classes = ( this.props.fulfilment_date < ( (new Date()).valueOf() + ( 5 * 1000 * 24 * 60 * 60 ) ) ) ?
				'fulfilment-warning' : 'hidden';
				mayesh_classes = 'mayesh-register';

			} else {
				fulfilment_classes = 'hidden';
				mayesh_classes = 'mayesh-register hidden';
			}

			return {
				mayesh_classes: mayesh_classes,
				fulfilment_classes: fulfilment_classes,
				attachments: null
			}
		},

		onAttachment: function(e) {

			var file = this.refs.attachment;
			var formdata = false;

			if ( window.FileReader ) {
				var reader = new FileReader();
				reader.onloadend = function (e) { 
					jQuery('#attachment_content').val( e.target.result );
					jQuery('#attachment_name').val( jQuery('#attachment').val() )
				};
				reader.readAsDataURL(file.files[0]);
			}
		},

		onShowphotosCheckUncheck: function(e) {
			var iframe = this.refs.emailPreview;

			if ( iframe.contentWindow.togglePhotosColumn ) {
				iframe.contentWindow.togglePhotosColumn( e.target.checked );
			}
		},

		render: function() {
			var defaultValue = '',
				hiddenInput = null,
				required = true,
				placeholder = 'Enter email here',
				attachment_control = null;

			if ( 'sanders@mayesh.com' == this.props.email_addr ) {
				hiddenInput = <input type="hidden" defaultValue="mayesh" name="wholesaler" />;
				required = false;
				placeholder = 'Enter your salesperson\'s email or leave blank.';
			}

			if ( false && window.FileReader ) {
				attachment_control = <div class="attachment_control">
					Add attachment:
					<input type="file" ref="attachment" id="attachment" onChange={this.onAttachment} />
					<input type="hidden" name="attachment_content" id="attachment_content" />
					<input type="hidden" name="attachment_name" id="attachment_name" />
				</div>
			}

			return (
				<form action="">
					<div className="email-order-form-wrap">
						<div className="modalpane-row clearfix pane-keep">
							<div className={this.state.mayesh_classes}>
								Not already a US Mayesh customer? <br /> Get started here: <a href="http://www.mayesh.com/register/" target="_blank">www.mayesh.com/register</a>
							</div>
							<label className="col-xs-12 control-label modalpane">
								<div className="lbl">Recipient's Email Address<span className="mandatory" />
								</div>
								<input placeholder={placeholder} className="form-control recipient-email" name="recipient" type="text" required={required} />
								{hiddenInput}
							</label>
						</div>
						<div className="modalpane-row clearfix pane-keep">
							<label className="col-xs-12 control-label modalpane">
								<div className="lbl">Notes</div>
								<textarea className="form-control order-email-notes" type="text" name="notes" placeholder="Enter any notes that you want to send with your order." onChange={this.onNotesChange} />
							</label>
						</div>
						<div className="modalpane-row clearfix pane-keep">
							<label className="col-xs-12 control-label modalpane">
								<div className="lbl">Email Preview</div>

								<iframe ref="emailPreview" src={window.location + '&email=1' + '&show_photos='+ this.state.show_photos} className="email-preview" frameBorder="0"></iframe>
							</label>
						</div>
						{attachment_control}
						<label htmlFor="cta-showphotos" className="showphotos-lbl">
							<input type="checkbox" ref="showphotos" name="with_photos" value="1" id="cta-showphotos" onChange={this.onShowphotosCheckUncheck} /> Show Photos
						</label>
						<div className={this.state.fulfilment_classes}>
							Please be aware all items may be not in stock, Sales rep will contact you if there is an issue for possible substitution.
						</div>
					</div>
				</form>
			);
		}
	});

	$(document).on('stemcounter.action.renderOrder', function( e, $el, order_data, items, date_format ){

		ReactDOM.render(<SCOrders
			order={order_data}
			items={items}
			dateFormat={date_format}
		/>, $el.get(0));
	});
})(jQuery);