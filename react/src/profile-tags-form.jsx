// use a closure so we do not pollute the global scope
(function($){
'use strict';

var ProfileTagsForm = React.createClass({
	getInitialState: function() {
		this.submit = _.debounce( this.submit, 400 );

		return {
			'tags': this.props.tags,
			'edit': false,
		};
	},
	removeTag: function( tagIndex, e ) {
		e.preventDefault();
		if ( this.state.edit ) {
			return;
		}

		var updatedState = {
			tags: $.extend( true, [], this.state.tags ),
			edit: false
		};

		updatedState.tags.splice( tagIndex, 1 );
		this.setState( updatedState, this.submit );		
	},

	addMore: function( e ) {
		e.preventDefault();
		if ( this.state.edit ) {
			return;
		}

		var updatedState = {
			tags: $.extend( true, [], this.state.tags ),
			edit: true
		};
		
		updatedState.tags.push( {
			color: '',
			name: ''
		} );	
		this.setState(updatedState);
	},

	onTagnameChange: function( e ) {
		var updatedState = {
			tags: $.extend( true, [], this.state.tags )
		};

		var tagIndex = parseInt( e.target.dataset.index, 10 );

		updatedState.tags[tagIndex].name = e.target.value;
		this.setState(updatedState);	
	},

	onFormSubmit: function( e ){
		e.preventDefault();

		this.submit();
	},

	submit: function() {
		var $form = $(this.refs.form);
		var th = this;
		var url = stemcounter.aurl({
			'action': 'sc_edit_tags'
		});

		$.post(url, { tags: this.state.tags })
			.always(function(response){
				stemcounter.JSONResponse(response, (function(r){
					var updatedState = {
						edit: false
					};

					this.setState( updatedState );
				}).bind(th));
			});
	},

	onColorTagsChange: function( tagIndex, color ) {
		var updatedState = {
			tags: $.extend( true, [], this.state.tags )
		};

		updatedState.tags[tagIndex].color = color;

		this.setState( updatedState, ! this.state.edit || ( this.state.tags.length - 1 ) != tagIndex ? this.submit : $.noop );
	},

	render: function() {
		return (
			<form action="" method="post" onSubmit={this.submit} ref="form">
				<div className="pane-row">
					<a className="add-tag-button cta-link" onClick={this.addMore}>
						+ Add New Tag
					</a>
				</div>
				{this.state.tags.map((function( tag, i ){
					var edit = ( this.state.edit && this.state.tags.length - 1 == i  );
					var delete_save_link;
					if ( edit ) {
						delete_save_link = (
							<a href="#" className="btn-remove" onClick={this.onFormSubmit}>
								<i className="fa fa-floppy-o"></i>
							</a>
						);
					} else {
						delete_save_link = (
							<a href="#" className="btn-remove" onClick={this.removeTag.bind(this, i)}>
								<i className="fa fa-trash-o"></i>
							</a>
						);
					}
					var tagStyle = {};

					if ( tag.color ) {
						tagStyle.backgroundColor = tag.color;
						tagStyle.borderColor = tag.color;
						tagStyle.color = 120 > stemcounter.get_hex_luma( tagStyle.backgroundColor ) ? 'white' : 'black';
					}

					return (
						<div className="row tax-rate-row pane-row" key={'event_tag_' + i}>
							<div className="pane">
								<label className="tag-name-label">Tag Name</label>
								<div className="value">
									<input type="text" readOnly={!edit} className="form-control" name={'tags[' + i + '][name]'} data-index={i} value={tag.name} onChange={this.onTagnameChange} placeholder="Tag Name" />
								</div>
							</div>
							<div className="pane">
								<label className="control-label">Color</label>
								<ColorPickerContainer
									color={tag.color}
									name={'tags[' + i + '][color]'}
									data-index={i}
									onChange={this.onColorTagsChange.bind(null, i)}
									className="value"
									showPredefinedColors={true}
								/>
							</div>
							<div className="pane">
								<label className="control-label">Preview</label>
								<div className="value">
									<a href="javascript:void(0)" className="btn btn-xs btn-default tag event-tag" style={tagStyle}>{tag.name}</a>
								</div>
							</div>
							<div className="pane pane-tag-delete">
								<label className="control-label"></label>
								<div className="value">
									{delete_save_link}
								</div>
							</div>
						</div>
					);
				}).bind(this))}
			</form>
		)
	}


});

$(document).on( 'stemcounter.action.renderEditTagsForm', function(e, settings){
	ReactDOM.render(
		<ProfileTagsForm 
		tags={settings.tags}
		color={settings.color}
		/>,
		settings.node.get(0)
	);
} );

})(jQuery);