(function($) {
'use strict';

var VendorModal = React.createClass({
	getInitialState: function() {
		var state = {
			layout: '',
			vendorId: -1,
			vendorCompanyName: '',
			vendorWebsite: '',
			vendorEmail: '',
			vendorPhone: '',
			vendorAddress: '',
			vendorNote: '',
			vendorTags: ["venue"],
			allVendorTags: {}
		};

		//fill state object with props
		for (var prop in this.props) {
			if (this.props.hasOwnProperty(prop) && this.props[prop] !== undefined) {
				state[prop] = this.props[prop];
			}
		}

		return state;
	},

	componentDidMount: function(e) {
		stemcounter.unmountComponentOnModalClose( this );
	},

	handleChange: function(field, event) {
		var updatedState = {};

		switch (field) {
			case 'companyName':
				updatedState.vendorCompanyName = event.target.value;
				break;
			case 'website':
				updatedState.vendorWebsite = event.target.value;
				break;
			case 'email':
				updatedState.vendorEmail = event.target.value;
				break;
			case 'phone':
				updatedState.vendorPhone = event.target.value;
				break;
			case 'address':
				updatedState.vendorAddress = event.target.value;
				break;
			case 'note':
				updatedState.vendorNote = event.target.value;
				break;
			default:
				break;
		}

		this.setState(updatedState);
	},

	submit: function(event) {
		//submit function
		event.preventDefault();
		var _this = this;
		if (this.props.layout === 'add-vendor') {
			var url = window.stemcounter.aurl({ action: 'sc_add_user_vendor' });
		    var data = $('form.add-vendor-form:first').serialize();

		    $.post(url, data, function (response) {
		    	stemcounter.JSONResponse(response, function (r) {
		    		if (r.success) {
		    			if (_this.props.createNewVendorCaller !== undefined) {
		    				$('form.add-vendor-form:first').closest('.modal').modal('hide');
		    				_this.props.createNewVendorCaller.addVenueToList(r);
		    			} else {
		    				document.location.reload();
		    			}
		    		}
		        });
		    });
		} else if (this.props.layout === 'edit-vendor') {
			var url = window.stemcounter.aurl({ action: 'sc_edit_user_vendor' });
		    var data = $('form.edit-vendor-form:first').serialize();

		    $.post(url, data, function (response) {
		    	stemcounter.JSONResponse(response, function (r) {
		    		if (_this.props.editVendorCaller !== undefined) {
		    				$('form.edit-vendor-form:first').closest('.modal').modal('hide');
		    				_this.props.editVendorCaller.editVenueList(r);
		    		} else {
		    			if (r.success) {
		    				$('form.edit-vendor-form:first').closest('.modal').modal('hide');
		    				document.location.reload(); //Added by Gancho
		    			}
		    			// if (r.success) document.location.reload();
		    		}
		        });
		    });
		}
	},

	render: function() {
		var _this = this;
		var vendorTagsInputs = [];
		var formControl = (
			<div>
				<button className="btn btn-default btn-cancel" type="button" data-dismiss="modal">Cancel</button>
				<button className="btn btn-primary button-submit" type="submit" onClick={this.submit}>Save</button>
			</div>
		);

		Object.keys(this.state.allVendorTags).map(function (key) {
			var checked = false;
			for (var i = 0; i < _this.state.vendorTags.length; i++) {
				if ( _this.state.vendorTags[i] == key ) {
					checked = true;
				}
			}
			vendorTagsInputs.push(
				<label key={'vendor-tag-' + key}>
					<input type="checkbox" name="vendor_tags[]" value={key} defaultChecked={checked}/>{_this.state.allVendorTags[key]}
				</label>
			);
		});

		return (
			<div>
				<div>
					<div className="modalpane-row clearfix">
						<label className="col-xs-12 col-sm-6 control-label modalpane">
							<div className="lbl">
								Vendor<span className="mandatory"></span>
							</div>
								<input className="form-control" placeholder="Add vendor here" type="text" name="vendor_company_name" value={this.state.vendorCompanyName} onChange={this.handleChange.bind(this, 'companyName')}/>
								<input type="hidden" name="vendor_id" value={this.state.vendorId} />
						</label>
						<label className="col-xs-12 col-sm-6 control-label modalpane">
							<div className="lbl">
								Website
							</div>
								<input type="text" placeholder="www.example.com" className="form-control" name="vendor_website" value={this.state.vendorWebsite} onChange={this.handleChange.bind(this, 'website')} />
						</label>
					</div>

					<div className="modalpane-row clearfix">
						<label className="col-xs-12 col-sm-6 control-label modalpane">
							<div className="lbl">
								Email
							</div>
							<input type="email" placeholder="Add email address here" className="form-control" name="vendor_email" value={this.state.vendorEmail} onChange={this.handleChange.bind(this, 'email')} />
						</label>
						<label className="col-xs-12 col-sm-6 control-label modalpane">
							<div className="lbl">
								Phone Number
							</div>
							<input type="text" placeholder="555-555-5555" className="form-control" name="vendor_phone" value={this.state.vendorPhone} onChange={this.handleChange.bind(this, 'phone')} />
						</label>
					</div>

					<div className="modalpane-row clearfix">
						<label className="col-sm-12 control-label modalpane">
							<div className="lbl">
								Address
							</div>
							<input type="text" className="form-control" placeholder="Add address here" name="vendor_address" value={this.state.vendorAddress} onChange={this.handleChange.bind(this, 'address')} />
						</label>
					</div>

					<div className="modalpane-row clearfix">
						<label className="col-sm-12 control-label modalpane">
							<div className="lbl">
								Tags
							</div>
							<div>
								{vendorTagsInputs}
							</div>
						</label>
					</div>

					<div className="modalpane-row clearfix">
						<label className="col-sm-12 control-label modalpane">
							<div className="lbl">
								Description
							</div>
							<textarea className="form-control" placeholder="Add description here" name="vendor_note" value={this.state.vendorNote} onChange={this.handleChange.bind(this, 'note')}></textarea>
						</label>
					</div>
				</div>
				<div className="modal-footer">
					<div className="pull-left hide">*required</div>
					{formControl}
				</div>
			</div>
		);
	}
});


$(document).on('stemcounter.action.renderVendorForm', function(e, settings) {
	ReactDOM.render(
		React.createElement(VendorModal, {
			layout: settings.layout,
			vendorId: settings.vendorId,
			vendorCompanyName: settings.vendorCompanyName,
			vendorWebsite: settings.vendorWebsite,
			vendorEmail: settings.vendorEmail,
			vendorPhone: settings.vendorPhone,
			vendorAddress: settings.vendorAddress,
			vendorNote: settings.vendorNote,
			vendorTags: settings.vendorTags,
			allVendorTags: settings.allVendorTags,
			createNewVendorCaller: stemcounter.page.createNewVendorCaller,
			editVendorCaller: stemcounter.page.editVendorCaller
		}),
		$(settings.node).get(0)
	);
});

})(jQuery);
