// use a closure so we do not pollute the global scope
(function($){
'use strict';

var TaxRatesForm = React.createClass({
	getInitialState: function() {
		var rawRates = this.props.value;

		var rates = [];
		for (var i = 0; i < rawRates.length; i++) {
			var rate = rawRates[i];
			rate.msg = '';
			rates.push(this.createNewRate(rate.name, rate.value, rate.id, parseInt(rate.default, 10), parseInt(rate.default2, 10)));
		};
		
		return {
			rates: rates
		};
	},

	rateAutoIncrement: 0,
	createNewRate: function(name, value, id, is_default, is_default2) {
		this.rateAutoIncrement ++;

		var newRate = {
			id: id,
			key: this.rateAutoIncrement,
			name: name,
			value: value,
			msg: '',
			default: is_default,
			default2: is_default2
		}

		return newRate;
	},

	addMore: function(e) {
		e.preventDefault();

		var updatedState = {
			rates: this.state.rates
		};

		updatedState.rates.push(this.createNewRate('', '0.00', null, this.state.rates.length ? 0 : 1, 0));

		this.setState(updatedState);
	},

	removeRate: function(rateIndex, e) {
		e.preventDefault();

		var updatedState = {
			rates: this.state.rates
		};

		updatedState.rates.splice(rateIndex, 1);

		this.setState(updatedState, this.submit);
	},

	onNameChange: function(rateIndex, e) {
		var updatedState = {
			rates: this.state.rates
		};

		var name = e.target.value;
		updatedState.rates[rateIndex].name = name;

		this.setState(updatedState);
	},

	onRateChange: function(rateIndex, e) {
		var updatedState = {
			rates: this.state.rates
		};

		var rate = e.target.value;

		if ( rate < 1 ) {
			updatedState.rates[rateIndex].msg =  'Mark your amount in % format. ( 0.086 should be written as 8.6 )';
		} else {
			updatedState.rates[rateIndex].msg = '';
		}

		rate = stemcounter.parseFloatValue(rate);
		updatedState.rates[rateIndex].value = rate;

		this.setState(updatedState);
	},

	onDefaultRateChange: function( rateIndex, e ) {
		var updatedState = {
			rates: $.extend(true, [], this.state.rates)
		};

		if ( undefined !== updatedState.rates[rateIndex] ) {
			for (var i = 0; i < updatedState.rates.length; i++) {
				var element = updatedState.rates[i];
				if ( rateIndex == i ) {
					updatedState.rates[i].default = 1;
				} else {
					updatedState.rates[i].default = 0;
				}
			}

			this.setState(updatedState, this.submit);
		}
	},

	onDefaultRate2Change: function( rateIndex, e ) {
		var updatedState = {
			rates: $.extend(true, [], this.state.rates)
		};

		if ( undefined !== updatedState.rates[rateIndex] ) {
			for (var i = 0; i < updatedState.rates.length; i++) {
				if ( rateIndex == i ) {
					updatedState.rates[i].default2 = e.target.checked ? 1 : 0;
				} else {
					updatedState.rates[i].default2 = 0;
				}
			}

			this.setState(updatedState, this.submit);
		}
	},

	submit: function() {

		var form = $(this.refs.form);
		var th = this;
		$.post(form.attr('action'), this.state)
			.always(function(r){
				var updatedState = {
					rates: $.extend(true, [], th.state.rates)
				};

				for (var i = 0; i < updatedState.rates.length; i++) {
					if ( ! updatedState.rates[i].id ) {
						var new_rate = _.findWhere( r.payload.tax_rates, { key: updatedState.rates[i].key } );
						if ( new_rate ) {
							updatedState.rates[i].id = new_rate.id;
						}
					}
				};

				th.setState( updatedState );
			});
	},

	render: function() {
		var labels = null;
		var taxRates = [];
		var showBothRates = this.props.ratesCount == 2;

		for (var i = 0; i < this.state.rates.length; i++) {
			var rate = this.state.rates[i];

			taxRates.push((
				<div className="row tax-rate-row pane-row" key={rate.key}>
					<div className="pane">
							<input type="text" className="form-control" value={rate.name} onChange={this.onNameChange.bind(this, i)} onBlur={this.submit} placeholder="Tax name" />
					</div>
					<div className="pane pane-rate">
						<input type="text" className="form-control" value={rate.value} onChange={this.onRateChange.bind(this, i)} onBlur={this.submit} />
						<span className="percentage-msg">{rate.msg}</span>
					</div>
					<div className="pane default-rate">
						<div>
							<input className="styled-radio" checked={rate.default} type="radio" value={1} id={'default_tax_' + rate.id } onChange={this.onDefaultRateChange.bind(this, i)} /><label htmlFor={'default_tax_' + rate.id }></label>
						</div>
						{showBothRates ? null : (<a href="#" className="btn-remove" onClick={this.removeRate.bind(this, i)}>
							<i className="fa fa-trash-o"></i>
						</a>)}
					</div>

					{
						showBothRates ? (
							<div className="pane default-rate">
								<div>
									<input className="styled-radio" checked={rate.default2} type="checkbox" value={1} id={'default_tax2_' + rate.id } onChange={this.onDefaultRate2Change.bind(this, i)} /><label htmlFor={'default_tax2_' + rate.id }></label>
								</div>

								<a href="#" className="btn-remove" onClick={this.removeRate.bind(this, i)}>
									<i className="fa fa-trash-o"></i>
								</a>
							</div>
						) : null
					}
						
				</div>
			));
		};
		if (taxRates.length == 0) {
			labels = null;
			taxRates = <p>{"You don't have any tax rates, yet."}</p>;
		} else {
			labels = (
				<div className="row tax-rate-row pane-row">
	 				<div className="pane">
							<label className="control-label">Name</label>
					</div>
					<div className="pane pane-rate">
						<label className="control-label">Tax %</label>
					</div>
					<div className="pane default-rate">
						<label className="control-label">Default</label>
					</div>

					{
						showBothRates ? (
							<div className="pane default-rate">
								<label className="control-label">Default Second Rate</label>
							</div>
						) : null
					}
						
				</div>
			);
		}

		var url = stemcounter.aurl({
			'action': 'sc_edit_tax_rates'
		});

		return (
			<form action={url} method="post" onSubmit={this.submit} ref="form">
				<div className="pane-row">
					<a className="add-tax-button cta-link" onClick={this.addMore}>
						+ New Tax Rate
					</a>
				</div>
				<div className="row no-gutter">
					{labels}
					{taxRates}
				</div>
			</form>
		);
	}
});

$(document).ready(function(){
	var formWrapper = $('.edit-tax-rates-form-wrapper:first');
	if (formWrapper.length == 1) {
		var value = formWrapper.data('value');
		ReactDOM.render(
			<TaxRatesForm value={value} ratesCount={formWrapper.data('rateCount')} />,
			formWrapper.get(0)
		);
	}
});

})(jQuery);