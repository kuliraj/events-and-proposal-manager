;(function($) {
'use strict';

var MAX_ARR_PHOTOS = 8;

var StandaloneRecipes = React.createClass({
	ajaxReq: null,
	DataTable: false,

	getInitialState: function(){
		var state = {
			event: {},
			arrangementsToSave: [],
			eventHasChanged: false,
			photoKeyIndex: 0,
			categoryId: -1,
			categoriesList: [],
			is_drop_target: false
		};

		//fill state object with props
		for (var prop in this.props) {
			if (this.props.hasOwnProperty(prop) && this.props[prop] !== undefined) {
				state[prop] = this.props[prop];
			}
		}

		state = this.setupArrangementPhotos( state );

		this.componentDidUpdate = _.throttle( this._componentDidUpdate, 4000 );
		this.eventNotesUpdated = _.throttle( this._eventNotesUpdated, 1000 );

		return state;
	},

	componentDidMount: function(){
		this.DataTable = $('.recipes-table').DataTable();

		$('.recipes-table').on('click', '.edit-item-button,.recipe-name', this.onArrangementClick).on('click', '.create-new-recipe-btn', this.onAddRecipeClick);
	},

	componentWillUnmount: function(){
		$('.recipes-table').off('click', '.edit-item-button,.recipe-name', this.onArrangementClick).off('click', '.create-new-recipe-btn', this.onAddRecipeClick);
	},

	setupArrangementPhotos: function( state ) {
		state.event.arrangements = state.event.arrangements.map(function(arrangement, i, arr) {
			while (arrangement.photos.length < MAX_ARR_PHOTOS) { 
				arrangement.photos.push({
					image_id: null,
					image_url: ''
				});
			}

			if (arrangement.photos.length) {
				for (var i = 0; i < arrangement.photos.length; i++) {
					arrangement.photos[i]['key'] = ++state.photoKeyIndex;
				}
			}

			return arrangement;
		});

		return state;
	},

	onArrangementClick: function(e){
		e.preventDefault();
		var $btn = $(e.target),
			arrangement_id,
			_this = this;

		$btn = $btn.is('a') ? $btn : $btn.closest('a');
		arrangement_id = $btn.data('itemId');

		// Hide the editing UI if we click on the same recipe
		if ( arrangement_id == this.state.selectedArrangement ) {
			this.skipAutoSave = true;
			this.setState({
				selectedArrangement: false
			});

			return;
		}

		this.skipAutoSave = true;
		this.setState({
			loading: true,
			selectedArrangement: false
		});

		var $recipe_row = $('tr#recipe-row-' + arrangement_id);
		if ( $recipe_row.length ) {
			$('#standalone-recipes-wrap').css({
				top: ($recipe_row.offset().top + $recipe_row.height()) + 'px',
				left: ( $recipe_row.offset().left - $('#main-content').offset().left ) + 'px',
				right: ( $(window).width() - $recipe_row.offset().left - $recipe_row.width() ) + 'px',
			});
		}

		$.post(stemcounter.ajax_url, {
			action: 'sc/standalone_recipe/load',
			arrangement_id: arrangement_id
		}, function(result){
			if ( result.success ) {
				var state = {
					event: $.extend(true, {}, _this.state.event),
					selectedArrangement: arrangement_id,
					photoKeyIndex: _this.state.photoKeyIndex
				};
			
				state.event.arrangements = [ result.payload.arrangement ];
				state = _this.setupArrangementPhotos( state );

				_this.skipAutoSave = true;
				_this.setState( state );

				var $recipe_row = $('tr#recipe-row-' + arrangement_id);
				if ( $recipe_row.length ) {
					$('#standalone-recipes-wrap').css({
						top: ($recipe_row.offset().top + $recipe_row.height()) + 'px',
						left: 0,
						right: 0
					});
				}
			} else {
				alertify.error( result.message );
			}
		}).always(function(){
			_this.skipAutoSave = true;
			_this.setState({
				loading: false
			});
		});
	},

	onAddRecipeClick: function(e){
		e.preventDefault();

		var event = $.extend(true, {}, this.state.event);
		event.arrangements = [];

		this.skipAutoSave = true;
		this.setState({
			event: event,
			selectedArrangement: false,
			loading: true
		}, (function(){
			this.createArrangementAt( 0, 0, true );
		}).bind(this));
	},

	onDragStart: function(event){
		event.preventDefault();
		event.stopPropagation();

		this.skipAutoSave = true;
		this.setState({
			is_drop_target: true
		});
	},

	onDragOver: function(event){
		this.skipAutoSave = true;
		this.setState({
			is_drop_target: true
		});
	},

	onDragEnter: function(event){
		this.skipAutoSave = true;
		this.setState({
			is_drop_target: true
		});
	},

	onDragLeave: function(event){
		event.preventDefault();
		event.stopPropagation();
		if ( undefined !== event.target.dataset.dragTarget ) {
			this.skipAutoSave = true;
			this.setState({
				is_drop_target: false
			});
		}

	},

	onDragEnd: function(event){
		if ( this.state.is_drop_target ) {
			this.skipAutoSave = true;
			this.setState({
				is_drop_target: false
			});
		}
	},

	getArrangementItemCalculatedPrice: function( item, arrangement, include_quantity ){
		include_quantity = undefined === include_quantity ? true : include_quantity;

		var item_price = parseFloat( parseFloat( item.cost ).toFixed10(2) );
		if ( include_quantity ) {
			item_price = parseFloat( (item_price * parseFloat( item.quantity )).toFixed10(2) );
		}

		item_price = parseFloat( item_price.toFixed10(2) ) * parseFloat( parseFloat( this.getArrangementItemInvoicingMultiplier( item, arrangement ) ).toFixed10(2) );
		item_price = parseFloat( item_price.toFixed10(2) ) * parseFloat( parseFloat( 1 + parseFloat( (this.getArrangementItemLaborPercent( item, arrangement ) / 100).toFixed10(2) ) ).toFixed10(2) );


		return parseFloat( item_price.toFixed10(2) );
	},

	getArrangementItemLaborPercent: function(item, arrangement){
		var percent = 1;

		if ( 'flower' == item.item_pricing_category || 'fresh_flower' == item.item_pricing_category ) {
			if ( null !== arrangement.flower_labor ) {
				percent = arrangement.flower_labor;  
			} else {
				percent = this.state.eventInvoiceValues.labor.flower;  
			}
		} else if ( 'hardgood' == item.item_pricing_category ) {
			if ( null !== arrangement.hardgood_labor ) {
				percent = arrangement.hardgood_labor;  
			} else {
				percent = this.state.eventInvoiceValues.labor.hardgood;  
			}
		} else if ( 'base_price' == item.item_pricing_category ) {
			if ( null !== arrangement.base_price_labor ) {
				percent = arrangement.base_price_labor;  
			} else {
				percent = this.state.eventInvoiceValues.labor.base_price;  
			}
		} else if ( 'fee' == item.item_pricing_category ) {
			if ( null !== arrangement.fee_labor ) {
				percent = arrangement.fee_labor;  
			} else {
				percent = this.state.eventInvoiceValues.labor.fee;  
			}
		} else if ( 'rental' == item.item_pricing_category ) {
			if ( null !== arrangement.rental_labor ) {
				percent = arrangement.rental_labor;  
			} else {
				percent = this.state.eventInvoiceValues.labor.rental;  
			}
		}

		return parseFloat( parseFloat( percent ).toFixed10(2) );
	},

	getArrangementItemInvoicingMultiplier: function(item, arrangement){
		var multiplier = 1;

		if ( 'flower' == item.type_raw ) {
			if ( null !== arrangement.fresh_flower_multiple ) {
				multiplier = arrangement.fresh_flower_multiple;  
			} else {
				multiplier = this.state.eventInvoiceValues.fresh_flower_multiple;  
			}
		} else if ( 'hardgood' == item.type_raw ) {
			if ( null !== arrangement.hardgood_multiple ) {
				multiplier = arrangement.hardgood_multiple;  
			} else {
				multiplier = this.state.eventInvoiceValues.hardgood_multiple;  
			}
		}

		return multiplier;
	},

	componentDidUpdate: $.noop,

	_componentDidUpdate: function( prevProps, prevState ) {
		// If there is another request running, abort it
		// if ( null !== this.ajaxReq ) {
		// 	this.ajaxReq.abort();
		// 	this.ajaxReq = null;
		// }

		if ( ! this.skipAutoSave || this.state.eventHasChanged ) {
			this.skipAutoSave = false;
			// Trigger the auto-save
			this.doSave();
		} else {
			this.skipAutoSave = false;
		}
	},

	doSave: function( updatedState ) {
		updatedState = updatedState || $.extend(true, {}, this.state);

		if (updatedState.eventHasChanged) {
			var data = {
				event_id: this.state.event.id,
				arrangements: []
			};
			var _this = this;

			if (updatedState.arrangementsToSave.length !== 0) {
				var updatedArrangements = [];

				for (var i = 0; i < updatedState.arrangementsToSave.length; i++) {
					for (var ii = 0; ii < updatedState.event.arrangements.length; ii++) {
						if ( updatedState.arrangementsToSave[i] == updatedState.event.arrangements[ii].id ) {
							var _arr = $.extend(true, {}, updatedState.event.arrangements[ii]);
							// Removing items for the request because they are not being saved. 
							delete _arr.items;
							
							updatedArrangements.push(_arr);

							break;
						}
					}
				}

				data.arrangements = updatedArrangements;
				updatedState.arrangementsToSave = [];
			}

			updatedState.eventHasChanged = false;
			if ( ! updatedState.version ) {
				if ( null === this.ajaxReq ) {
					this.sendSaveRequest( data );
				} else {
					this.ajaxReq.latest_data = data;
				}
			}
		}

		this.skipAutoSave = true;

		this.setState(updatedState, (function(){
			if ( ! this.state.event.arrangements.length ) {
				this.createArrangementAt( 0, 0 );
			}
		}).bind(this));
	},

	createArrangementAt: function(index, is_addon, reposition_component) {
		var arrangementsToSave = $.extend(true, [], this.state.arrangementsToSave);
		var event = $.extend(true, {}, this.state.event);

		var arrangement = {
			id: -1,
			items: [],
			name: '',
			note: '',
			photos: [],
			price: 0,
			quantity: 1,
			subTotal: 0,
			total: 0,
			tax: 1,
			addon: parseInt(is_addon, 10),
			include: 1,
			duplicate_unique_key: stemcounter.sc_generate_rndstring(),
			recipes: [],
			is_percentage: 0
		};

		for (var i = 1; i <= MAX_ARR_PHOTOS; i++) {
			arrangement.photos.push( {
				image_id: null,
				image_url: '',
				key: i
			} );
		}

		event.arrangements.splice(index + parseInt(is_addon, 10), 0, arrangement);

		for (var i = 0; i < event.arrangements.length; i++) {
			if ( -1 === arrangementsToSave.indexOf( event.arrangements[i].id ) ) {
				arrangementsToSave.push( event.arrangements[i].id );
			}

			event.arrangements[i].order = i;
		}

		this.DataTable.row.add( {
			0: '<a href="#" class="recipe-name" data-item-id="' + arrangement.id + '">' + 
				( ( ! arrangement.name || 0 === arrangement.name.length ) ? '(no name)' : arrangement.name  ) +
			 	'</a>',
			1: '<a href="#" class="edit-item-button" data-item-id="' + arrangement.id + '"><i class="fa fa-pencil"></i></a>',
			DT_RowId: 'recipe-row-' + arrangement.id,
			DT_RowClass: 'recipe'
		} ).draw();

		if ( reposition_component ) {
			var $recipe_row = $('tr#recipe-row-' + arrangement.id);
			if ( $recipe_row.length ) {
				$('#standalone-recipes-wrap').css({
					top: ($recipe_row.offset().top + $recipe_row.height()) + 'px',
					left: ( $recipe_row.offset().left - $('#main-content').offset().left ) + 'px',
					right: ( $(window).width() - $recipe_row.offset().left - $recipe_row.width() ) + 'px',
				});
			}
		}
		
		this.setState({
			eventHasChanged: true,
			arrangementsToSave: arrangementsToSave,
			event: event
		});
	},

	sendSaveRequest: function( data ){
		var url = window.stemcounter.aurl({ action: 'sc/standalone_recipe/edit' });
		var _this = this;

		this.ajaxReq = $.post(url, data, function (r) {
			if (r.success) {
				var updatedState = $.extend(true, {}, _this.state);

				// Return the IDs of new and duplicated arrangements
				if ( r.payload.inc_arrangements !== undefined && r.payload.inc_arrangements.length !== 0 ) {
					for ( var i = 0; i < updatedState.event.arrangements.length; i++ ) {
						if ( updatedState.event.arrangements[i].duplicate_unique_key !== undefined && updatedState.event.arrangements[i].duplicate_unique_key.length !== 0 ) {
							for (var ii = 0; ii < r.payload.inc_arrangements.length; ii++) {
								if ( updatedState.event.arrangements[i].duplicate_unique_key == r.payload.inc_arrangements[ii].key ) {
									updatedState.event.arrangements[i].id = r.payload.inc_arrangements[ii].arr_id;
									updatedState.event.arrangements[i].recipes = r.payload.inc_arrangements[ii].inc_recipes;
									updatedState.event.arrangements[i].is_percentage = r.payload.inc_arrangements[ii].is_percentage;

									if ( r.payload.inc_arrangements[ii].inc_items.length ) {
										updatedState.event.arrangements[i].items = r.payload.inc_arrangements[ii].inc_items;
									}

									if ( updatedState.loading ) {
										updatedState.loading = false;
										updatedState.selectedArrangement = updatedState.event.arrangements[i].id;
									}
									console.log( updatedState.event.arrangements[i].name );
									_this.DataTable.row( '#recipe-row-' + _this.state.event.arrangements[i].id ).data( {
										0: '<a href="#" class="recipe-name" data-item-id="' + updatedState.event.arrangements[i].id + '">' + 
										( ( ! updatedState.event.arrangements[i].name || 0 === updatedState.event.arrangements[i].name.length ) ? '(no name)' : updatedState.event.arrangements[i].name ) + '</a>',
										1: '<a href="#" class="edit-item-button" data-item-id="' + updatedState.event.arrangements[i].id + '"><i class="fa fa-pencil"></i></a>',
										DT_RowId: 'recipe-row-' + updatedState.event.arrangements[i].id,
										DT_RowClass: 'recipe'
									} ).draw();

									delete updatedState.event.arrangements[i].duplicate_unique_key;
									delete updatedState.event.arrangements[i].duplicated_from;

									break;
								}
							}
						} else {
							continue;
						}
					}

					if ( null !== _this.ajaxReq && undefined !== _this.ajaxReq.latest_data && undefined !== _this.ajaxReq.latest_data.arrangements ) {
						for ( var i = 0; i < _this.ajaxReq.latest_data.arrangements.length; i++ ) {
							if ( _this.ajaxReq.latest_data.arrangements[i].duplicate_unique_key !== undefined && _this.ajaxReq.latest_data.arrangements[i].duplicate_unique_key.length !== 0 ) {
								for (var ii = 0; ii < r.payload.inc_arrangements.length; ii++) {
									if ( _this.ajaxReq.latest_data.arrangements[i].duplicate_unique_key == r.payload.inc_arrangements[ii].key ) {
										_this.ajaxReq.latest_data.arrangements[i].id = r.payload.inc_arrangements[ii].arr_id;
										_this.ajaxReq.latest_data.arrangements[i].recipes = r.payload.inc_arrangements[ii].inc_recipes;
										_this.ajaxReq.latest_data.arrangements[i].is_percentage = r.payload.inc_arrangements[ii].is_percentage;

										if ( r.payload.inc_arrangements[ii].inc_items.length ) {
											_this.ajaxReq.latest_data.arrangements[i].items = r.payload.inc_arrangements[ii].inc_items;
										}
										delete _this.ajaxReq.latest_data.arrangements[i].duplicate_unique_key;
										delete _this.ajaxReq.latest_data.arrangements[i].duplicated_from;

										break;
									}
								}
							} else {
								continue;
							}
						}
					}
				}

				_this.skipAutoSave = true;
				_this.setState(updatedState);
			}
		}).always((function(){
			var latest_data = this.ajaxReq.latest_data;
			this.ajaxReq = null;
			if ( undefined !== latest_data ) {
				this.sendSaveRequest( latest_data );
			}
		}).bind(this));
	},

	arrangementDeleted: function(arrangement_id) {
		var event = $.extend(true, {}, this.state.event);

		event.arrangements = [];

		this.skipAutoSave = true;
		this.setState({
			arrangementsToSave: [],
			event: event,
			selectedArrangement: false
		});

		// Remove the row from the table
		this.DataTable.row( '#recipe-row-' + arrangement_id ).remove().draw();
	},

	arrangementItemUpdated: function(arrangement_id, updatedArrangement) {
		var arrangementsToSave = $.extend(true, [], this.state.arrangementsToSave);
		var event = $.extend(true, {}, this.state.event);
		var photoKeyIndex = this.state.photoKeyIndex;

		if (arrangementsToSave.indexOf(arrangement_id) === -1) {
			arrangementsToSave.push(arrangement_id);
		}
		
		updatedArrangement.photos = updatedArrangement.photos.map(function(photo, i, arr) {
			photo['key'] = ++photoKeyIndex;
			return photo;
		});

		for (var i = 0; i < event.arrangements.length; i++) {
			if ( arrangement_id == event.arrangements[i].id ) {
				if ( event.arrangements[i].name != updatedArrangement.name ) {
					// Update the name of the recipe in the table
					this.DataTable.cell( '#recipe-row-' + arrangement_id + ' > td:first' ).data( '<a href="#" class="recipe-name" data-item-id="' + arrangement_id + '">' + 
						( ( ! updatedArrangement.name || 0 === updatedArrangement.name.length ) ? '(no name)' : updatedArrangement.name )						+ '</a>' );
					this.DataTable.draw();
				}
				event.arrangements[i] = updatedArrangement;
				break;
			}
		}

		this.setState({
			photoKeyIndex: photoKeyIndex,
			eventHasChanged: true,
			arrangementsToSave: arrangementsToSave,
			event: event
		});
	},

	onArrangementSettingsSave: function(arrangement_id, data) {
		var event = $.extend(true, {}, this.state.event);
		var arrangementsToSave = $.extend(true, [], this.state.arrangementsToSave);

		if (arrangementsToSave.indexOf(arrangement_id) === -1) {
			arrangementsToSave.push(arrangement_id);
		}

		for (var i = 0; i < event.arrangements.length; i++) {
			if ( arrangement_id == event.arrangements[i].id ) {
				event.arrangements[i].include               = parseInt( data.include, 10 );
				event.arrangements[i].tax                   = data.tax;
				event.arrangements[i].addon                 = parseInt( data.addon, 10 );
				event.arrangements[i].invoicing_category_id = data.invoicing_category_id;
				event.arrangements[i].hardgood_multiple     = data.hardgood_multiple;
				event.arrangements[i].fresh_flower_multiple = data.fresh_flower_multiple;
				event.arrangements[i].global_labor_value    = data.global_labor_value;
				event.arrangements[i].hardgood_labor        = data.hardgood_labor;
				event.arrangements[i].flower_labor          = data.flower_labor;
				event.arrangements[i].base_price_labor      = data.base_price_labor;
				event.arrangements[i].fee_labor             = data.fee_labor;
				event.arrangements[i].rental_labor          = data.rental_labor;
				if ( parseInt( 1 == event.arrangements[i].is_percentage ) && 0 == parseInt( data.is_percentage ) ) {
					event.arrangements[i].override_cost     = event.arrangements[i].subTotal;
					event.arrangements[i].quantity 			= 1;
					if ( undefined !== this.refs.event_arrangements.myrefs['arr_' + event.arrangements[i].id] ) {
						$(this.refs.event_arrangements.myrefs['arr_' + event.arrangements[i].id].refs.price_override).val( event.arrangements[i].override_cost );
					}
				}
				event.arrangements[i].is_percentage	        = data.is_percentage;

				break;
			}
		}

		this.setState({
			eventHasChanged: true,
			arrangementsToSave: arrangementsToSave,
			event: event
		});
	},

	onArrangementEdit: function(arrangement_id, data) {
		var event = $.extend(true, {}, this.state.event);
		var arrangementsToSave = $.extend(true, [], this.state.arrangementsToSave);
		
		if (arrangementsToSave.indexOf(arrangement_id) === -1) {
			arrangementsToSave.push(arrangement_id);
		}

		for (var i = 0; i < event.arrangements.length; i++) {
			if ( arrangement_id == event.arrangements[i].id ) {
				event.arrangements[i].items = data.items;
				event.arrangements[i].recipes = data.recipes;
				event.arrangements[i].total = data.totals.total;

				event.arrangements[i].invoicing_category_id = data.invoicing_category_id;
				event.arrangements[i].hardgood_multiple     = data.hardgood_multiple;
				event.arrangements[i].fresh_flower_multiple = data.fresh_flower_multiple;
				event.arrangements[i].global_labor_value    = data.global_labor_value;
				event.arrangements[i].hardgood_labor        = data.hardgood_labor;
				event.arrangements[i].flower_labor          = data.flower_labor;
				event.arrangements[i].base_price_labor      = data.base_price_labor;
				event.arrangements[i].fee_labor             = data.fee_labor;
				event.arrangements[i].rental_labor          = data.rental_labor;
				event.arrangements[i].is_percentage        	= data.is_percentage;


				if ( null === data.override_cost || '' === data.override_cost ) {
					event.arrangements[i].override_cost = null;
				} else {
					event.arrangements[i].override_cost = event.arrangements[i].override_cost && 0 < event.arrangements[i].override_cost && event.arrangements[i].override_cost == data.override_cost ? event.arrangements[i].override_cost : data.override_cost;
				}

				if ( undefined !== this.refs.event_arrangements.myrefs['arr_' + arrangement_id] ) {
					if ( null === event.arrangements[i].override_cost ) {
						$(this.refs.event_arrangements.myrefs['arr_' + arrangement_id].refs.price_override).val( '' );
					} else {
						$(this.refs.event_arrangements.myrefs['arr_' + arrangement_id].refs.price_override).val( event.arrangements[i].override_cost );
					}
				}


				if ( event.arrangements[i].override_cost && null !== event.arrangements[i].override_cost ) {
					event.arrangements[i].subTotal = event.arrangements[i].override_cost * data.quantity;
				} else {
					event.arrangements[i].subTotal = event.arrangements[i].total * parseFloat( data.quantity );
				}

				break;
			}
		}

		this.setState({
			eventHasChanged: true,
			arrangementsToSave: arrangementsToSave,
			event: event
		});
	},

	close: function(e){
		e.preventDefault();

		this.skipAutoSave = true;
		this.setState({
			selectedArrangement: false
		});
	},

	render: function() {
		if ( ! this.state.selectedArrangement ) {
			if ( this.state.loading ) {
				return <div className="loading-bar"></div>
			}

			return null;
		}

		var categoriesList = $.extend(true, [], this.state.categoriesList);
		
		if ( 0 < categoriesList.length ) {
			categoriesList.unshift({
				'base_price_labor'     : this.state.eventInvoiceValues.labor.base_price,
				'fee_labor'            : this.state.eventInvoiceValues.labor.fee,
				'flower_labor'         : this.state.eventInvoiceValues.labor.flower,
				'fresh_flower_multiple': this.state.eventInvoiceValues.fresh_flower_multiple,
				'global_labor_value'   : this.state.eventInvoiceValues.labor.global_labor_value,
				'hardgood_labor'       : this.state.eventInvoiceValues.labor.hardgood,
				'hardgood_multiple'    : this.state.eventInvoiceValues.hardgood_multiple,
				'rental_labor'         : this.state.eventInvoiceValues.labor.rental,
				'name'                 : 'Default Event Markup',
				'id'                   : - this.state.categoryId
			});
		}

		return (
			<div className="proposal-container">
				<div className="proposal-state-edit">
					<div className={'container content-panel row proposal-section' + ( this.state.is_drop_target ? ' dragging' : '' )}>
						<button type="button" className="btn btn-primary btn-close" onClick={this.close}>×</button>
						<div className="row">
							<div className="col-sm-12" onDragStart={this.onDragStart} onDragOver={this.onDragOver} onDragEnter={this.onDragEnter} onDragLeave={this.onDragLeave} data-drag-target="1">
								<h4 className="section-heading">Editing Recipe</h4>
								<div className="section-divider section-2"></div>
								<EventArrangements
									arrangements={this.state.event.arrangements}
									addons={false}
									proposalState="edit"
									arrangementItemUpdated={this.arrangementItemUpdated}
									currencySymbol={this.state.currencySymbol}
									eventId={this.state.event.id}
									createArrangementAt={this.createArrangementAt}
									duplicateArrangement={this.duplicateArrangement}
									moveArrangement={this.moveArrangement}
									itemTypes={this.props.itemTypes}
									categoriesList={categoriesList}
									categoryId={this.state.categoryId}
									arrangementDeleted={this.arrangementDeleted}
									onArrangementEdit={this.onArrangementEdit}
									onArrangementSettingsSave={this.onArrangementSettingsSave}
									getArrangementItemPrice={this.getArrangementItemCalculatedPrice}
									getArrangementItemLaborPercent={this.getArrangementItemLaborPercent}
									getArrangementItemInvoicingMultiplier={this.getArrangementItemInvoicingMultiplier}
									onDrop={this.onDragEnd}
									onDragEnd={this.onDragEnd}
									standaloneRecipes={true}
									ref="event_arrangements"
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

$(document).on('stemcounter.action.renderStandaloneRecipes', function( e, settings ){
	ReactDOM.render(
		<StandaloneRecipes
			event={settings.event}
			currencySymbol={settings.currencySymbol}
			itemTypes={settings.itemTypes}
			categoryId={settings.categoryId}
			eventInvoiceValues={settings.eventInvoiceValues}
			categoriesList={settings.categoriesList}
			dateFormat={settings.dateFormat}
			selectedArrangement={settings.selectedArrangement}
		/>,
		$(settings.node).get(0)
	);
});

})(jQuery);