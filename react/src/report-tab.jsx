(function($) {
'use strict';

var Report = React.createClass({
	dataTable: false, 

	getInitialState: function() {
		return {
			dateStart: '',
			dateEnd: '',
			reportType: 'events',
			loading: false,
			data: [],
			data_labels: []
		}
	},

	componentDidMount: function(){
		$( ReactDOM.findDOMNode( this ) ).data( 'ReactComponent', this );
	},

	handleChange: function(e) {
		if ( 'date-end' == e.target.name ) {
			this.setState({
				dateEnd: e.target.value
			});
		}

		if ( '' != this.state.dateStart && '' != this.state.dateEnd ) {
			var dFrom = Date.parse( this.state.dateStart );
			var dTo = Date.parse( this.state.dateEnd );
			if ( dFrom > dTo ) {
				alertify.error( 'Your dates are messed up!' );
			}
		}
	},
	
	handleDateStartChange: function( value ){
		this.setState({
			dateStart: value
		});
	},
	
	handleDateEndChange: function( value ){
		this.setState({
			dateEnd: value
		});
	},

	handleReportType: function(e) {
		e.preventDefault();

		this.setState({
			reportType: e.target.value
		});
	},

	onReport: function(e) {
		e.preventDefault();

		if ( undefined == this.state.reportType ) {
			alertify.error( 'Report type must be selected' );
			return;
		}

		if ( '' == this.state.dateStart || '' == this.state.dateEnd ) {
			alertify.error( 'Select START and END dates for your report' );
			return;
		}

		this.setState({
			loading: true
		});

		var _this = this;
		var updatedState = $.extend(true, {}, this.state);
		var url = stemcounter.aurl({
			'action': 'sc_user_report'
		});
		var data = {
			date_start: this.state.dateStart,
			date_end: this.state.dateEnd,
			report_type: this.state.reportType
		};

		$.post(url, data, function (r) {
			if (r.success) {
				if ( r.payload.data && r.payload.data_labels ) {
					updatedState.data = r.payload.data;
					updatedState.data_labels = r.payload.data_labels;
					_this.setState(updatedState);
				}
			} else {
				alertify.error( r.message );
			}
		}).always(function(){
			_this.setState({
				loading: false
			});
		});
	},

	setDataTableRef: function( Ref ) {
		this.dataTable = Ref;
	},

	render: function() {
		var loading = this.state.loading ? (<div className="loading-bar"></div>) : null,
			reportTypeOptions = [
				<option key="report-type-0" value="events">Events</option>,
				<option key="report-type-1" value="payments">Payments</option>
			];

		return(
			<div className="row report-component">
			<div className="pane">
				<div className="">
					<div className="row report-heading">
						<div className="col-xs-12">
							<div className="report-type">
								<label className="clearfix">Type</label>
								<div className="">
									<ReactSelect2
										selectedValue={this.state.reportType}
										onChange={this.handleReportType}>
										{reportTypeOptions}
									</ReactSelect2>
								</div>
							</div>
							<SC_DatePicker type="text" classList="report-date" nameList="date-start" defaultValue={this.state.dateStart} onDateChange={this.handleDateStartChange} dateFormat={this.props.dateFormat} />
							<span> - </span>
							<SC_DatePicker type="text" classList="report-date" nameList="date-end" defaultValue={this.state.dateEnd} onDateChange={this.handleDateEndChange} dateFormat={this.props.dateFormat} />
							<a href="#" className="btn btn-primary report-btn" onClick={this.onReport}>Report</a>
						</div>
					</div>
					<div className="col-xs-12">
						{loading}
					</div>
					<div className="col-xs-12">
						<ReportOutput dataTableRef={this.setDataTableRef} reportType={this.state.reportType} dataRows={this.state.data} dataLabels={this.state.data_labels} />
					</div>
				</div>
			</div>
			</div>
		);
	}
});

var ReportOutput = React.createClass({
	getInitialState: function() {
		var state = {};
		state.defaultSortBy = '';
		state.dataLabels = this.props.data_labels ? this.props.dataLabels : [];
		state.dataRows = this.props.dataRows ? this.props.dataRows : [];
		state.taxSum = 0;
		state.amountSum = 0;

		if ( 'payments' == this.props.reportType && state.dataRows.length ) {
			state.defaultSortBy = 'due_date';
			Object.keys(state.dataRows).map((function (key) {
				state.dataRows[key]['amount'] = stemcounter.formatPrice( state.dataRows[key]['amount_raw'],true, stemcounter.currencySymbol );
				return false;
			}));
		} else if ( 'events' == this.props.reportType && state.dataRows.length ) {
			state.defaultSortBy = 'event_name';
			Object.keys(state.dataRows).map((function (key) {
				state.dataRows[key]['total'] = stemcounter.formatPrice( state.dataRows[key]['total_raw'],true, stemcounter.currencySymbol );
				state.dataRows[key]['tax'] = stemcounter.formatPrice( state.dataRows[key]['tax_raw'],true, stemcounter.currencySymbol );
				return false;
			}));
		}

		return state;
	},

	componentWillReceiveProps: function(nextProps) {
		var updatedState = $.extend(true, {}, this.state);
		updatedState.dataRows = nextProps.dataRows;
		updatedState.dataLabels = nextProps.dataLabels;
		updatedState.taxSum = 0;
		updatedState.amountSum = 0;

		if ( 'payments' == nextProps.reportType && updatedState.dataRows.length ) {
			updatedState.defaultSortBy = 'due_date';
			Object.keys(updatedState.dataRows).map((function (key) {
				updatedState.dataRows[key]['amount'] = stemcounter.formatPrice( updatedState.dataRows[key]['amount_raw'],true, stemcounter.currencySymbol );
				return false;
			}));
		} else if ( 'events' == nextProps.reportType && updatedState.dataRows.length ) {
			updatedState.defaultSortBy = 'event_name';
			Object.keys(updatedState.dataRows).map((function (key) {
				updatedState.dataRows[key]['total'] = stemcounter.formatPrice( updatedState.dataRows[key]['total_raw'],true, stemcounter.currencySymbol );
				updatedState.dataRows[key]['tax'] = stemcounter.formatPrice( updatedState.dataRows[key]['tax_raw'],true, stemcounter.currencySymbol );
				return false;
			}));
		}

		this.setState(updatedState);
	},

	shouldComponentUpdate: function(nextProps, nextState) {
		return this.props.dataRows !== nextProps.dataRows
				|| this.props.dataLabels !== nextProps.dataLabels
				|| this.state.dataRows !== nextState.dataRows
	},

	deleteRow: function(e) {
		e.preventDefault();

		var updatedState = $.extend(true, {}, this.state),
			index = $(e.target).closest('tr').data('index');

		updatedState.dataRows.splice(index, 1);

		this.setState(updatedState);
	},

	render: function() {
		var _this = this;
		var table = null;
		var amountSum = null;
		var taxSum = null;
		if ( this.state.dataRows ) {
			table = (
				<DataTable
					ref={this.props.dataTableRef}
					defaultSortBy={this.state.defaultSortBy}
					defaultSortDir="ASC"
					rows={this.state.dataRows}
					labels={this.state.dataLabels}
					deleteRow={this.deleteRow}
				/>
			);
			if ( this.state.dataRows.length ) {
				var taxTotal = 0;
				var amountTotal = 0;

				{Object.keys(this.state.dataRows).map((function (key) {
					if ( 'events' == _this.props.reportType ){
						taxTotal += parseFloat(_this.state.dataRows[key]['tax_raw']);	
						amountTotal += parseFloat(_this.state.dataRows[key]['total_raw']);
					} else if ( 'payments' == _this.props.reportType ) {
						amountTotal += parseFloat(_this.state.dataRows[key]['amount_raw']);
					}
					return false;
				}))}

				amountSum = (
					<div className="row">
						<span className="pull-right">Total: <p className="pull-right amountsum">{stemcounter.formatPrice( amountTotal,true, stemcounter.currencySymbol )}</p></span>
					</div>
				);
				if ( 'events' == this.props.reportType ) {
					taxSum = (
						<div className="row">
							<span className="pull-right">Tax: <p className="pull-right amountsum">{stemcounter.formatPrice( taxTotal,true, stemcounter.currencySymbol )}</p></span>
						</div>
					);
				}
			}
		} else {
			table = 'No records found for the given time period';
		}
		return (
			<div id="report-table">
				{table}
				{amountSum}
				{taxSum}
			</div>
		);
	}
});

$(document).on('stemcounter.action.renderReport', function(e, settings) {
	ReactDOM.render(
		<Report dateFormat={settings.dateFormat} />,
		settings.node.get(0)
	);
});

})(jQuery);