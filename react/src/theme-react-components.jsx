// use a closure so we do not pollute the global scope
(function(window, $){
'use strict';

var MAX_ARR_PHOTOS = 8;

var VirtualizedSelectOrig = window['react-virtualized-select'].default;
class VirtualizedSelect extends VirtualizedSelectOrig {
  componentDidMount () {
	this.onBottomButtonClick = this.onBottomButtonClick.bind( this );
  }

  onBottomButtonClick ( e ) {
	this._selectRef.closeMenu();
	this._selectRef.blurInput();
	if ( this.props.onBottomButtonClick ) {
	  this.props.onBottomButtonClick( e );
	  /*this._selectRef.input.blur();
	  setTimeout((function(){
		this._selectRef.setState({
		  isOpen: false
		});
	  }).bind(this), 100);*/
	}
  }

  _renderMenu ( props ) {
	if ( this.props.bottomButton ) {
	  return (
		<div>
		  {VirtualizedSelectOrig.prototype._renderMenu.call(this,props)}
		  <a className="btn btn-primary select-bottom-button" onClick={this.onBottomButtonClick}>{this.props.bottomButton}</a>
		</div>
	  );
	} else {
	  return VirtualizedSelectOrig.prototype._renderMenu.call(this,props);
	}
  }
}
window.VirtualizedSelect = VirtualizedSelect;

var ReactSelect2 = React.createClass({
	propTypes: {
		// Array of <option /> components to be placed into the select
		children: React.PropTypes.array,
		className: React.PropTypes.string,
		name: React.PropTypes.string,
		multiple: React.PropTypes.bool,
		readOnly: React.PropTypes.bool,
		disabled: React.PropTypes.bool,
		// The initial selected value; one of the option children should have a
		// matching value="..."
//		defaultValue: React.PropTypes.string,
		defaultValue: React.PropTypes.oneOfType( [
			React.PropTypes.string,
			React.PropTypes.array,
			React.PropTypes.number
		] ),

//		selectedValue: React.PropTypes.string,
		selectedValue: React.PropTypes.oneOfType( [
			React.PropTypes.string,
			React.PropTypes.array,
			React.PropTypes.number
		] ),

		// Callback executed when the selected value changes; receives a single
		// jQuery event object `e` from select2; `e.target` refers to the real
		// <select> element and `e.val` refers to the new selected value
		onChange: React.PropTypes.func,

		replaceSelect2: React.PropTypes.func,

		templateResult: React.PropTypes.func
	},

	firstCall: true,

	activateSelect2: function() {
		var rootNode = ReactDOM.findDOMNode(this);
		var settings = {};

		if ( this.props.templateResult ) {
			settings.templateResult = this.props.templateResult;
		}

		if (this.props.replaceSelect2) {
			this.props.replaceSelect2(rootNode, settings);
		} else {
			$(rootNode).select2( settings );
		}
	},

	render: function() {
		return <select
			className={this.props.className}
			defaultValue={this.props.defaultValue}
			value={this.props.selectedValue}
			name={this.props.name}
			onChange={this._handleChange}
			multiple={this.props.multiple}
			readOnly={this.props.readOnly}
			disabled={this.props.disabled}>
			{this.props.children}
		</select>;
	},

	componentDidUpdate: function( prevProps ) {
		// Commenting this out fixed an error with Select2 - ¯\_(ツ)_/¯
		// this.activateSelect2();

		// This triggers an update to the UI when the selectedValue changes in props
		// and not by the user selecting an item in the list
		if ( prevProps ) {
			var trigger_change = false;
			if ( ! this.props.multiple ) {
				trigger_change = prevProps.selectedValue !== this.props.selectedValue;
			} else {
				for (var i = prevProps.selectedValue.length - 1; i >= 0; i--) {
					if ( -1 === this.props.selectedValue.indexOf( prevProps.selectedValue[ i ] ) ) {
						trigger_change = true;
						break;
					}
				}
			}

			if ( trigger_change ) {
				var change = this._handleChange;
				this._handleChange = $.noop;
				$( ReactDOM.findDOMNode( this ) ).trigger('change');
				this._handleChange = change;
			}
		}
	},

	componentDidMount: function() {
		var rootNode = ReactDOM.findDOMNode(this);

		this.activateSelect2();

		if (this.props.defaultValue != null) {
			$(rootNode).select2("val", this.props.defaultValue);
		}

		$(rootNode).on("change", this._handleChange);
		if (this.firstCall) {
			this.componentDidUpdate();
			this.firstCall = false;
		}
	},

	componentWillUnmount: function() {
		var rootNode = ReactDOM.findDOMNode(this);

		if ( $(rootNode).data('select2') ) {
			$(rootNode).select2("destroy");
		}
	},

	_handleChange: function(e) {
		this.props.onChange && this.props.onChange(e);
	}
});

window.ReactSelect2 = ReactSelect2;

var RenderSelectCategory = React.createClass({
	getInitialState: function() {
		var state = {
			categoryId: this.props.categoryId,
			showCategoryValues: true
		};

		if (!this.props.categoriesList) state.categoriesList = [];
		if (this.props.layout === 'arrangement') state.showCategoryValues = false;

		/* Labor values */
		for(var type in this.props.itemTypes) {
			if(this.props.itemTypes.hasOwnProperty(type)) {
				state[type] = 0;
			}
		}

		return state;
	},

	componentWillReceiveProps: function(nextProps) {
	},

	onCategoryChange: function(event) {
		var updatedState = {};

		if (typeof event.target == 'undefined') {
			updatedState.categoryId = event;
		} else {
			updatedState.categoryId = event.target.value
		}

		//callback in order to update the parent component
		this.props.onCategoryChange(updatedState.categoryId);

		this.setState(updatedState);
	},

	onShowHideCatValues: function(event) {
		event.preventDefault();
		var updatedState = {};
		updatedState.showCategoryValues = !this.state.showCategoryValues;
		this.props.showHideCategoryValues(updatedState.showCategoryValues);
		this.setState(updatedState)
	},

	refreshCategoryList: function(event) {
		event.preventDefault();
		this.props.refreshCategoryList();
	},

	replaceSelect2: function(node) {
		window.stemcounter.ScrollFix($(node), {});
	},

	render: function() {
		var categoriesList = [];
		if (this.props.layout === 'profile') {
			categoriesList = [<option value="-1">New Category</option>];
		}

		for (var i = 0; i < this.props.categoriesList.length; i++) {
			categoriesList.push(
				<option key={'cat-' + this.props.categoriesList[i].id + '_' + i} value={this.props.categoriesList[i].id}>{this.props.categoriesList[i].name}</option>
			);
		};
		var showValues;
		if (this.props.layout === 'arrangement') {
			var showHideButton = (
				<a href="#" onClick={this.onShowHideCatValues}>Show Values &nbsp;<i className="fa fa-arrow-down"></i></a>
			);

			if (this.state.showCategoryValues == true) {
				showHideButton = (
					<a href="#" onClick={this.onShowHideCatValues}>Hide Values &nbsp;<i className="fa fa-arrow-up"></i></a>
				);
			}

			showValues = (
				<div className="modalpane-row clearfix">
					<div className="modalpane col-sm-12">
						<label className="control-label">
							<div className="lbl">Markup Profile</div>

							<div className="row">
								<div className="col-xs-12 col-sm-9">
									<ReactSelect2 className="categoryList" name="category_id" children={categoriesList} onChange={this.onCategoryChange} selectedValue={this.state.categoryId} replaceSelect2={this.replaceSelect2} />
								</div>
								<div className="col-xs-12 col-sm-3 showhidecontainer">
									{showHideButton}
								</div>
							</div>


						</label>
					</div>
				</div>
			);
		} else if (this.props.layout === 'event-details') {
			showValues = (
				<div>
					<label className="col-sm-4 control-label">Markup Profiles</label>
					<div className="col-sm-5">
						<ReactSelect2 className="categoryList" name="category_id" children={categoriesList} onChange={this.onCategoryChange} selectedValue={this.state.categoryId} replaceSelect2={this.replaceSelect2} />
					</div>
					<div className="control-label col-sm-3"><a href="#" onClick={this.refreshCategoryList}><i className="fa fa-refresh"></i> Refresh</a></div>
				</div>
			);
		} else if ( 'proposal' === this.props.layout ) {
			showValues = (
				<div>
					<label className="control-label">Markup Profile:</label>
					<div className="profile-name">
						<ReactSelect2 className="categoryList" name="category_id" children={categoriesList} onChange={this.onCategoryChange} selectedValue={this.state.categoryId} replaceSelect2={this.replaceSelect2} />
					</div>
					<a href="#" className="category-refresh" onClick={this.refreshCategoryList}><i className="fa fa-refresh"></i></a>
				</div>
			);
		} else {
			showValues = (
				<div>
					<label className="col-sm-4 control-label">Markup Profiles</label>
					<div className="col-sm-8">
						<ReactSelect2 className="categoryList" name="category_id" children={categoriesList} onChange={this.onCategoryChange} selectedValue={this.state.categoryId} replaceSelect2={this.replaceSelect2} />
					</div>
				</div>
			);
		}

		return (
			<div className="clearfix style-form form-group modalpane-row" id="selectInvoice">
				<div className="form-group">
					{showValues}
				</div>
			</div>
		);
	}
});

var RenderCategoryName = React.createClass({
	getInitialState: function() {
		var state = {
			categoryName: this.props.categoryName
		};

		if (!this.props.categoryName) state.categoryName = '';

		return state;
	},

	componentWillReceiveProps: function(nextProps) {
		var updatedState = {
			categoryName: nextProps.categoryName
		};

		this.setState(updatedState);
	},

	onNameChange: function(event) {
		var updatedState = {
			categoryName: event.target.value
		};

		this.setState(updatedState);
		this.props.onNameChange(event.target.value);
	},

	render: function() {
		if ( 'proposal' == this.props.layout ) {
			return (
				<div>
					<label className="control-label">Markup Profile:</label>
					<div className="profile-name">{this.state.categoryName}</div>
				</div>
			);
		} else if ( 'new_profile' == this.props.layout ) {
			return (
				<div className="pane">
					<label className="control-label">Name</label>
					<div className="">
						<input type="text" className="form-control" onBlur={this.onNameChange} defaultValue={this.state.categoryName} />
					</div>
				</div>
			);
		} else {
			return (
				<div className="clearfix form-group">
					<div className="form-group">
						<label className="col-sm-4 control-label">Profile Name *</label>
						<div className="col-sm-8">
							<input type="text" className="form-control" onBlur={this.onNameChange} value={this.state.categoryName} />
						</div>
					</div>
					<br/>
				</div>
			);
		}
	}
});

var HardgoodMultiple = React.createClass({
	getInitialState: function() {
		var state = {
			hardgoodMultiple: this.props.categoryHardgoodMult,
			checked: 1!=this.props.categoryHardgoodMult,
			inputVisible: 1!=this.props.categoryHardgoodMult
		};

		// if (!this.props.categoryHardgoodMult) state.hardgoodMultiple = 1;

		return state;
	},

	componentWillReceiveProps: function(nextProps) {
		var updatedState = {
			hardgoodMultiple: nextProps.categoryHardgoodMult
		};

		if (updatedState.hardgoodMultiple != 1) {
			updatedState.checked = 'checked';
			updatedState.inputVisible = true;
		} else {
			updatedState.checked = '';
			updatedState.inputVisible = false;
		}

		this.setState(updatedState);
	},

	onHardgoodChange: function(event) {
		var updatedState = {
			hardgoodMultiple: event.target.value
		};

		this.setState(updatedState);
	},

	onCheck: function(event) {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		if (event.target.checked) {
			updatedState.checked = 'checked';
			updatedState.inputVisible = true;
		} else {
			updatedState.checked = '';
			updatedState.inputVisible = false;
			updatedState.hardgoodMultiple = 1;
		}
		this.setState(updatedState);
	},

	componentDidUpdate: function( prevProps, prevState ) {
		if ( prevState.hardgoodMultiple != this.state.hardgoodMultiple ) {
			this.props.changeValue(this.state.hardgoodMultiple);
		}
	},

	render: function() {
		var input;
		var checkbox;
		if (this.props.layout == 'event-details' && this.props.formState == 'view')
		{
			input = (
				<div>
					<p>{this.state.hardgoodMultiple}</p>-1
					<input type="text" name="hardgood_multiple" className="hide-category-input form-control" value={this.state.hardgoodMultiple} onChange={this.onHardgoodChange} />
				</div>
			);
		}	else if ( 'arrangement' == this.props.layout) {
			input = (
				<div>
					<p>{this.state.hardgoodMultiple}</p>
					<input type="text" name="hardgood_multiple" className="hide-category-input form-control" value={this.state.hardgoodMultiple} onChange={this.onHardgoodChange} />
				</div>
			);
		}	else if ( 'proposal' == this.props.layout ) {
			if ( 'edit' == this.props.formState ) {
				checkbox = (
					<input type="checkbox" checked={this.state.checked} onChange={this.onCheck} />
				);
				input = (
					<input type="text" name="hardgood_multiple" className="form-control" value={this.state.hardgoodMultiple} onChange={this.onHardgoodChange} />
				);
			} else {
				checkbox = (
					<input type="checkbox" checked={this.state.checked} disabled="disabled" />
				);
				input = (
					<input type="text" name="hardgood_multiple" className="form-control" value={this.state.hardgoodMultiple} disabled="disabled" />
				);
			}
		} else if ( 'new_profile' == this.props.layout ) {
				checkbox = null;
				return (
					<input type="text" name="hardgood_multiple" className="form-control" value={this.state.hardgoodMultiple} onChange={this.onHardgoodChange} />
				);
		} else {
			checkbox = (
				<input type="checkbox" checked={this.state.checked} onChange={this.onCheck} />
			);
			if (this.state.inputVisible) {
				input = (
					<input type="text" name="hardgood_multiple" className="form-control" value={this.state.hardgoodMultiple} onChange={this.onHardgoodChange} />
				);
			} else {
				input = (
					<input type="text" name="hardgood_multiple" className="hide-category-input form-control" value={this.state.hardgoodMultiple} onChange={this.onHardgoodChange} />
				);
			}

		}

		if ( 'proposal' == this.props.layout ) {
			return (
				<div className="hgood-wrap">
					<label className="">{checkbox}Hardgood Multiple</label>
					{input}
				</div>
			);
		} else {
			return (
				<div className="modalpane col-xs-12 col-sm-6">
					<label>
						<div className="lbl">{checkbox}Hardgood Multiple</div>
						{input}
					</label>
				</div>
			);
		}
	}
});

var FreshFlowerMultiple = React.createClass({
	getInitialState: function() {
		var state = {
			freshFlowerMultiple: this.props.categoryFreshFlowerMult,
			checked: 1!=this.props.categoryFreshFlowerMult,
			inputVisible: 1!=this.props.categoryFreshFlowerMult
		};

		// if (!this.props.categoryFreshFlowerMult) state.freshFlowerMultiple = 1;

		return state;
	},

	componentWillReceiveProps: function(nextProps) {
		var updatedState = {
			freshFlowerMultiple: nextProps.categoryFreshFlowerMult
		};

		if (updatedState.freshFlowerMultiple != 1) {
			updatedState.checked = 'checked';
			updatedState.inputVisible = true;
		} else {
			updatedState.checked = '';
			updatedState.inputVisible = false;
		}

		this.setState(updatedState);
	},

	onFreshFlowerChange: function(event) {
		var updatedState = {
			freshFlowerMultiple: event.target.value
		};

		this.setState(updatedState);
	},

	onCheck: function(event) {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		if (event.target.checked) {
			updatedState.checked = 'checked';
			updatedState.inputVisible = true;
		} else {
			updatedState.checked = '';
			updatedState.inputVisible = false;
			updatedState.freshFlowerMultiple = 1;
		}
		this.setState(updatedState);
	},

	componentDidUpdate: function( prevProps, prevState ) {
		if ( prevState.freshFlowerMultiple != this.state.freshFlowerMultiple ) {
			this.props.changeValue(this.state.freshFlowerMultiple);
		}
	},

	render: function() {
		var input;
		var checkbox;
		if (this.props.layout == 'event-details' && this.props.formState == 'view' ||
			this.props.layout == 'arrangement') {
			input = (
				<div>
					<p>{this.state.freshFlowerMultiple}</p>
					<input type="text" name="fresh_flower_multiple" className="hide-category-input form-control" value={this.state.freshFlowerMultiple} onChange={this.onFreshFlowerChange} />
				</div>
			);
		} else if ( 'proposal' == this.props.layout ) {
			if ( 'edit' == this.props.formState ) {
				checkbox = (
					<input type="checkbox" checked={this.state.checked} onChange={this.onCheck} />
				);
				input = (
					<input type="text" name="fresh_flower_multiple" className="form-control" value={this.state.freshFlowerMultiple} onChange={this.onFreshFlowerChange} />
				);
			} else {
				checkbox = (
					<input type="checkbox" checked={this.state.checked} disabled="disabled" />
				);
				input = (
					<input type="text" name="fresh_flower_multiple" className="form-control" value={this.state.freshFlowerMultiple} disabled="disabled" />
				);
			}

		} else if ( 'new_profile' == this.props.layout ) {
			checkbox = null;
			return (
				<input type="text" name="fresh_flower_multiple" className="form-control" value={this.state.freshFlowerMultiple} onChange={this.onFreshFlowerChange} />
			);
		} else {
			checkbox = (
				<input type="checkbox" checked={this.state.checked} onChange={this.onCheck} />
			);
			if (this.state.inputVisible) {
				input = (
					<input type="text" name="fresh_flower_multiple" className="form-control" value={this.state.freshFlowerMultiple} onChange={this.onFreshFlowerChange} />
				);
			} else {
				input = (
					<input type="text" name="fresh_flower_multiple" className="hide-category-input form-control" value={this.state.freshFlowerMultiple} onChange={this.onFreshFlowerChange} />
				);
			}


		}

		if ( 'proposal' == this.props.layout ) {
			return (
				<div className="flowerm-wrap">
					<label>{checkbox}Floral Multiple</label>
					{input}
				</div>
			);
		} else {
			return (
				<div className="modalpane col-sm-6 ">
					<label>
					<div className="lbl">{checkbox}Fresh Flower Multiple</div>
					{input}
					</label>
				</div>
			);
		}
	}
});

var ApplyLaborTo = React.createClass({
	getInitialState: function() {
		var state = {
			checkedLaborCosts: ''
		};

		var categoryLabor = this.props.categoryLabor;

		if (typeof this.props.categoryLabor === 'undefined' ||
			!this.props.categoryLabor.global_labor_value) {
			state.globalLaborValue = 0;
		} else {
			state.globalLaborValue = this.props.categoryLabor.global_labor_value;
		}

		if (state.globalLaborValue != 0 && state.globalLaborValue != '0.00') state.checkedLaborCosts = 'checked';

		/* Labor values */
		for(var type in this.props.itemTypes) {
			if(this.props.itemTypes.hasOwnProperty(type)) {
				if (typeof categoryLabor !== 'undefined' && categoryLabor[type] != 0 && categoryLabor[type] != '0.00') {
					state[type] = categoryLabor[type];
				} else {
					state[type] = 0;
				}
			}
		}

		state.showAdvancedMode = false;
		for(var type in this.props.itemTypes) {
			if(this.props.itemTypes.hasOwnProperty(type)) {
				if (typeof categoryLabor !== 'undefined' &&
					(categoryLabor[type] != 0 || categoryLabor[type] != '0.00') &&
					categoryLabor[type] != state.globalLaborValue) {
					state.showAdvancedMode = true;
					break;
				}
			}
		}

		return state;
	},

	componentWillReceiveProps: function(nextProps) {
		var updatedState = $.extend(true, {}, this.state);
		updatedState.globalLaborValue = nextProps.categoryLabor.global_labor_value;

		for(var type in this.props.itemTypes) {
			if(this.props.itemTypes.hasOwnProperty(type)) {
				updatedState[type] = nextProps.categoryLabor[type];
				document.getElementById("hiddenLaborCategory-" + type).value = nextProps.categoryLabor[type];
			}
		}

		for(var type in this.props.itemTypes) {
			if(this.props.itemTypes.hasOwnProperty(type)) {
				if (typeof categoryLabor !== 'undefined'
					&& (categoryLabor[type] != 0 || categoryLabor[type] != '0.00')
					&& categoryLabor[type] != state.globalLaborValue) {
					updatedState.showAdvancedMode = true;
					break;
				}
			}
		}

		if (updatedState.globalLaborValue != 0 && updatedState.globalLaborValue != '0.00') {
			updatedState.checkedLaborCosts = 'checked';
		} else {
			updatedState.checkedLaborCosts = ''
		}

		this.setState(updatedState);
		this.props.changeValue( updatedState );
	},

	onGlobalLaborChange: function(event) {
		var updatedState = $.extend(true, {}, this.state);
		updatedState.globalLaborValue = event.target.value;

		for(var type in this.props.itemTypes) {
			if(this.props.itemTypes.hasOwnProperty(type)) {
				var hiddenLaborInput = document.getElementById("hiddenLaborCategory-" + type),
					checkboxLaborInput = document.getElementById("checkboxLaborCategory-" + type);

				if (checkboxLaborInput.checked) {
					hiddenLaborInput.value = event.target.value;
					updatedState[type] = event.target.value;
				} else {
					hiddenLaborInput.value = 0;
					updatedState[type] = 0;
				}
			}
		}

		this.setState(updatedState);
		this.props.changeValue( updatedState );
	},

	onLaborValueChange: function(type, event) {
		var updatedState = $.extend(true, {}, this.state);

		updatedState[type] = event.target.value;

		this.setState(updatedState);
		this.props.changeValue( updatedState );
	},

	onCheck: function(event) {
		var updatedState = $.extend(true, {}, this.state),
			checkbox = event.target;

		var type = checkbox.id.split("checkboxLaborCategory-")[1];
		if (checkbox.checked) {
			updatedState[type] = document.getElementById('globalLaborValue').value;
			document.getElementById("hiddenLaborCategory-" + type).readOnly = false;
		} else {
			updatedState[type] = 0;
			document.getElementById("hiddenLaborCategory-" + type).readOnly = true;
		}
		document.getElementById("hiddenLaborCategory-" + type).value = updatedState[type];

		this.setState(updatedState);
		this.props.changeValue( updatedState );
	},

	onAddLaborCheck: function(event) {
		var updatedState = $.extend(true, {}, this.state);

		if (event.target.checked) {
			updatedState.checkedLaborCosts = 'checked';
		} else {
			updatedState.checkedLaborCosts = '';
			updatedState.globalLaborValue = 0;
		}

		for(var type in this.props.itemTypes) {
			if(this.props.itemTypes.hasOwnProperty(type)) {
				var hiddenLaborInput = document.getElementById("hiddenLaborCategory-" + type),
					checkboxLaborInput = document.getElementById("checkboxLaborCategory-" + type);

				checkboxLaborInput.checked = '';
				hiddenLaborInput.value = updatedState.globalLaborValue;
				updatedState[type] = updatedState.globalLaborValue;
			}
		}

		this.setState(updatedState);
		this.props.changeValue( updatedState );
	},

	onAdvancedMode: function(event) {
		var updatedState = {};

		updatedState.showAdvancedMode = event.target.checked;

		this.setState(updatedState);
	},

	render: function() {
		var checkboxes = [];

		for(var type in this.props.itemTypes) {
			if(this.props.itemTypes.hasOwnProperty(type)) {
				var checkboxName = "apply_labor_to[" + type + "]",
					hiddenInputName = type + "_labor",
					checkboxInputID = "checkboxLaborCategory-" + type,
					hiddenInputID = "hiddenLaborCategory-" + type;

				var individualLaborInputs;
				if (this.state[type] != 0 && this.state[type] != '0.00') {
					var hiddenInputClasses = "hiddenCategoryValues col-xs-12 form-control";
					hiddenInputClasses += this.state.showAdvancedMode ? '' : ' hide-category-input';

					individualLaborInputs = (
						<label key={'input-' + type}>
							<input id={checkboxInputID} type="checkbox" onChange={this.onCheck} checked />{this.props.itemTypes[type]}
							<input id={hiddenInputID} className={hiddenInputClasses} type="text" name={hiddenInputName} defaultValue={this.state[type]} onChange={this.onLaborValueChange.bind(null, type)} />
						</label>
					);

					if ( ( this.props.layout == 'event-details' || 'proposal' == this.props.layout ) && this.props.formState == 'view') {
						individualLaborInputs = (
							<label key={'input-' + type}>
								<input id={checkboxInputID} type="checkbox" onChange={this.onCheck} checked disabled/>{this.props.itemTypes[type]}
								<input id={hiddenInputID} className={hiddenInputClasses} type="text" name={hiddenInputName} defaultValue={this.state[type]} onChange={this.onLaborValueChange.bind(null, type)} readOnly />
							</label>
						);
					}

					if (this.props.layout == 'arrangement') {
						hiddenInputClasses += ' hide-category-input';
						individualLaborInputs = (
							<div key={'input-' + type}>
								<label>
									<input id={checkboxInputID} type="checkbox" onChange={this.onCheck} checked disabled/>{this.props.itemTypes[type] + ' Labor'}
								</label>
								<input id={hiddenInputID} className={hiddenInputClasses} type="text" name={hiddenInputName} defaultValue={this.state[type]} onChange={this.onLaborValueChange.bind(null, type)} readOnly />
							</div>
						);
					}

				} else {
					var hiddenInputClasses = "hiddenCategoryValues col-xs-12 form-control";
					hiddenInputClasses += this.state.showAdvancedMode ? '' : ' hide-category-input';

					individualLaborInputs = (
						<label key={'input-' + type}>
							<input id={checkboxInputID} type="checkbox" name={checkboxName} onChange={this.onCheck} />{this.props.itemTypes[type]}
							<input id={hiddenInputID} className={hiddenInputClasses} type="text" name={hiddenInputName} defaultValue={this.state[type]} onChange={this.onLaborValueChange.bind(null, type)} readOnly />
						</label>
					);

					if ( ( this.props.layout == 'event-details' || 'proposal' == this.props.layout ) && this.props.formState == 'view') {
						individualLaborInputs = (
							<label key={'input-' + type}>
								<input id={checkboxInputID} type="checkbox" onChange={this.onCheck} disabled/>{this.props.itemTypes[type]}
								<input id={hiddenInputID} className={hiddenInputClasses} type="text" name={hiddenInputName} defaultValue={this.state[type]} onChange={this.onLaborValueChange.bind(null, type)} readOnly />
							</label>
						);
					}

					if (this.props.layout == 'arrangement') {
						hiddenInputClasses += ' hide-category-input';
						individualLaborInputs = (
							<div key={'input-' + type}>
								<label>
									<input id={checkboxInputID} type="checkbox" onChange={this.onCheck} disabled/>{this.props.itemTypes[type] + ' Labor'}
								</label>
								<div>
									<input id={hiddenInputID} className={hiddenInputClasses} type="text" name={hiddenInputName} defaultValue={this.state[type]} onChange={this.onLaborValueChange.bind(null, type)} readOnly />
								</div>
							</div>
						);
					}
				}

				if ( 'proposal' == this.props.layout ) {
					checkboxes.push(individualLaborInputs);
				} else {
					checkboxes.push(
						<div className="col-xs-4" key={'input-' + type}>
							{individualLaborInputs}
						</div>
					);
				}

			}
		}

		var showAdvancedMode;
		if (this.state.showAdvancedMode === true) {
			showAdvancedMode = (
				<label className="col-xs-12 control-label">
					<input type="checkbox" onChange={this.onAdvancedMode} checked="checked"/>Show Advanced Labor Settings
				</label>
			);
		} else {
			showAdvancedMode = (
				<label className="col-xs-12 control-label">
					<input type="checkbox" onChange={this.onAdvancedMode} />Show Advanced Labor Settings
				</label>
			);
		}

		var addLaborCost = (
			<div className="form-group clearfix">
				<label className="col-xs-12 control-label"><input type="checkbox" checked={this.state.checkedLaborCosts} onChange={this.onAddLaborCheck}/>Add Labor Costs</label>
				<em className="col-xs-12">Use a percentage of the total for labor cost</em>
			</div>
		);
		if (this.props.layout == 'event-details' && this.props.formState == 'view' ||
			this.props.layout == 'arrangement') {
			addLaborCost = false;
		}

		var globalLaborContainer = (
			<div className="col-xs-6">
				<input id="globalLaborValue" type="text" name="global_labor_value" value={this.state.globalLaborValue} className="form-control" onChange={this.onGlobalLaborChange} ref="globalLaborInput"/>
			</div>
		);
		if (this.props.layout == 'event-details' && this.props.formState == 'view' || this.props.layout == 'arrangement') {
			globalLaborContainer = (
				<div>
					<span className="control-label" id="globalLaborValue">{this.state.globalLaborValue}</span>
					<input id="globalLaborValue" type="hidden" name="global_labor_value" value={this.state.globalLaborValue} className="form-control" onChange={this.onGlobalLaborChange} ref="globalLaborInput"/>
				</div>
			);
		}

		var laborOptions,
			applyLaborTo;
		if (this.state.checkedLaborCosts == 'checked') {
			laborOptions = (
				<div className="form-group clearfix labor-option">
					<label className="col-xs-4 control-label">Labor</label>
					{globalLaborContainer}
					<label className="col-xs-2 field-suffix-label nopadding">%</label>
				</div>
			);
			applyLaborTo = (
				<div className="form-group clearfix labor-option">
					<label className="col-xs-12 control-label">Apply Labor To</label>
					<div className="col-xs-12">
						<div className="row">
							{checkboxes}
						</div>
					</div>
				</div>
			);
		} else {
			laborOptions = (
				<div className="hide-category-input form-group clearfix labor-option">
					<label className="col-xs-4 control-label">Labor</label>
					{globalLaborContainer}
					<label className="col-xs-2 field-suffix-label nopadding">%</label>
				</div>
			);
			applyLaborTo = (
				<div className="hide-category-input form-group clearfix labor-option">
					<label className="col-xs-12 control-label">Apply Labor To</label>
					<div className="col-xs-12">
						<div className="row">
							{checkboxes}
						</div>
					</div>
				</div>
			);
		}

		if (this.props.layout == 'arrangement') {
			laborOptions = (
				<div className="form-group clearfix labor-option">
					<label className="control-label">
						<div className="lbl">Labor</div>
							<div className="col-xs-1">
								{globalLaborContainer}
							</div>
							<label className="col-xs-10 field-suffix-label nopadding">%</label>
					</label>
				</div>
			);
			applyLaborTo = (
				<div className="form-group clearfix laborcheckboxes">
					<div className="col-xs-12">
						<div className="row">
							{checkboxes}
						</div>
					</div>
				</div>
			);
		} else if ( 'proposal' == this.props.layout ) {
			laborOptions = (<div></div>);
			if ( 'edit' == this.props.formState ) {
				addLaborCost = (
					<div className="form-group clearfix">
						<label className="col-xs-12 control-label"><input type="checkbox" checked={this.state.checkedLaborCosts} onChange={this.onAddLaborCheck}/> Labor %</label>
						<input id="globalLaborValue" type="text" name="global_labor_value" value={this.state.globalLaborValue} className="form-control" ref="globalLaborInput" onChange={this.onGlobalLaborChange} />
					</div>
				);
			} else {
				addLaborCost = (
					<div className="form-group clearfix">
						<label className="col-xs-12 control-label"><input type="checkbox" disabled="disabled" checked={this.state.checkedLaborCosts} /> Labor %</label>
						<input id="globalLaborValue" type="text" name="global_labor_value" value={this.state.globalLaborValue} className="form-control" disabled="disabled" ref="globalLaborInput"/>
					</div>
				);
			}

			applyLaborTo = (
				<div className="col-xs-12 item-types-wrap">
					<div className="row">
						{checkboxes}
					</div>
				</div>
			);
		}

		return (
			<div className="clearfix form-group labor-costs-wrap">
				{addLaborCost}
				{laborOptions}
				{applyLaborTo}
			</div>
		);
	}
});

var InvoicingCategories = React.createClass({
	getInitialState: function() {
		var itemTypes = this.props.itemTypes,
			layout = 'profile',
			defaultCategory = false,
			defaultCategoryCheck = '',
			categoryId = -1,
			categoryName = '',
			categoryHardgood = 1,
			categoryFreshFlower = 1,
			categoryLabor = {
				global_labor_value: 0,
				flower: 0,
				hardgood: 0,
				base_price: 0,
				fee: 0,
				rental: 0
			},
			showCategoryValues = true;

		if (this.props.categoryId) categoryId = this.props.categoryId;
		if (this.props.layout) layout = this.props.layout;
		if (this.props.layout === 'arrangement') showCategoryValues = false;

		defaultCategoryCheck = this.props.defaultCategoryId == categoryId ? 'checked' : '';

		if (this.props.arrangementValues) {
			categoryHardgood = this.props.arrangementValues.hardgood_multiple;
			categoryFreshFlower = this.props.arrangementValues.fresh_flower_multiple;
			categoryLabor = this.props.arrangementValues.labor;
		}

		if (this.props.eventValues) {
			categoryHardgood = this.props.eventValues.hardgood_multiple;
			categoryFreshFlower = this.props.eventValues.fresh_flower_multiple;
			categoryLabor = this.props.eventValues.labor;
		}

		if (categoryId > 0) {
			for (var i = 0; i < this.props.categoriesList.length; i++) {
				if (this.props.categoriesList[i].id == categoryId) {
					categoryName = this.props.categoriesList[i].name;
					categoryHardgood = this.props.categoriesList[i].hardgood_multiple;
					categoryFreshFlower = this.props.categoriesList[i].fresh_flower_multiple;

					categoryLabor = {
						global_labor_value: this.props.categoriesList[i].global_labor_value,
						flower: this.props.categoriesList[i].flower_labor,
						hardgood: this.props.categoriesList[i].hardgood_labor,
						base_price: this.props.categoriesList[i].base_price_labor,
						fee: this.props.categoriesList[i].fee_labor,
						rental: this.props.categoriesList[i].rental_labor
					};
					break;
				}
			};
		}

		return {
			itemTypes: itemTypes,
			layout: layout,
			categoryId: categoryId,
			defaultCategory: defaultCategory,
			defaultCategoryCheck: defaultCategoryCheck,
			categoryName: categoryName,
			categoryHardgood: categoryHardgood,
			categoryFreshFlower: categoryFreshFlower,
			categoryLabor: categoryLabor,
			showCategoryValues: showCategoryValues
		};
	},

	componentDidMount: function(){

	},

	componentWillReceiveProps: function(nextProps) {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		if (nextProps.eventValues) {
			updatedState.categoryHardgood = nextProps.eventValues.hardgood_multiple;
			updatedState.categoryFreshFlower = nextProps.eventValues.fresh_flower_multiple;
			updatedState.categoryLabor = nextProps.eventValues.labor;
		}
		if (nextProps.categoryId) updatedState.categoryId = nextProps.categoryId;
		if (nextProps.defaultCategoryId) updatedState.defaultCategoryId = nextProps.defaultCategoryId;
		if (nextProps.categoriesList) updatedState.categoriesList = nextProps.categoriesList;

		if (typeof this.refs.categorySelection !== 'undefined' &&
			this.props.layout !== 'event-details' && this.props.layout !== 'arrangement') {
			// this.refs.categorySelection.onCategoryChange(updatedState.categoryId);
		}
		this.setState(updatedState);
	},

	componentChanged: function( type, values ){
		if (typeof this.props.onChange != 'undefined') {
			this.props.onChange( type, values );
		}
	},

	onCategoryHardgoodChange: function(value) {
		if ( this.state.categoryHardgood == value ) {
			return;
		}

		this.componentChanged( 'category_hardgood', value );
	},

	onCategoryFreshFlowerChange: function(value) {
		if ( this.state.categoryFreshFlower == value ) {
			return;
		}

		this.componentChanged( 'category_flower', value );
	},

	onLaborChange: function(values) {
		if ( this.state.categoryLabor.base_price == values.base_price &&
			this.state.categoryLabor.fee == values.fee &&
			this.state.categoryLabor.flower == values.flower &&
			this.state.categoryLabor.global_labor_value == values.globalLaborValue &&
			this.state.categoryLabor.hardgood == values.hardgood &&
			this.state.categoryLabor.rental == values.rental ) {
			return;
		}

		this.componentChanged( 'labor', values );
	},

	onCategoryChange: function(categoryId) {
		if ( this.state.categoryId == categoryId ) {
			return;
		}

		//This is a callback function
		//categoryId is coming from the ReactSelect2 component
		var updatedState = {};

		updatedState.categoryId = categoryId;
		for (var i = 0; i < this.props.categoriesList.length; i++) {
			if (this.props.categoriesList[i].id == categoryId) {
				updatedState.categoryName = this.props.categoriesList[i].name;
				updatedState.categoryHardgood = this.props.categoriesList[i].hardgood_multiple;
				updatedState.categoryFreshFlower = this.props.categoriesList[i].fresh_flower_multiple;
				updatedState.categoryLabor = {
					global_labor_value: this.props.categoriesList[i].global_labor_value,
					flower: this.props.categoriesList[i].flower_labor,
					hardgood: this.props.categoriesList[i].hardgood_labor,
					base_price: this.props.categoriesList[i].base_price_labor,
					fee: this.props.categoriesList[i].fee_labor,
					rental: this.props.categoriesList[i].rental_labor
				};
			} else if (this.props.layout == 'profile' && categoryId == -1) {
				//If we have selected New Category from the dropdown
				updatedState.categoryName = '';
				updatedState.categoryHardgood = 1;
				updatedState.categoryFreshFlower = 1;
				updatedState.categoryLabor = {
					global_labor_value: 0,
					flower: 0,
					hardgood: 0,
					base_price: 0,
					fee: 0,
					rental: 0
				};
			}
		};

		updatedState.defaultCategoryCheck = this.props.defaultCategoryId == updatedState.categoryId ? 'checked' : '';

		if (this.props.onCategoryChange) {
			this.props.onCategoryChange(updatedState);
		}

		if (this.props.layout != 'event-details') {
			this.setState(updatedState);
		}

		this.componentChanged( 'invoicing_category', updatedState );
	},

	showHideCategoryValues: function(showValues) {
		var updatedState = {};
		updatedState.showCategoryValues = showValues;
		this.setState(updatedState);
	},

	refreshCategoryList: function() {
		var _this = this;

		var postdata = {
			action: 'sc_refresh_event_details_invoice',
			category_id: this.state.categoryId
		};

		$.ajax({
			type: 'POST',
			url: window.stemcounter.ajax_url,
			data: postdata,
			success: function(resp){
				resp = JSON.parse(JSON.stringify(resp));
				if (resp.success) {
					var updatedState = JSON.parse(JSON.stringify(_this.state));

					updatedState.categoryHardgood = resp.payload.invoiceCategory.hardgood_multiple;
					updatedState.categoryFreshFlower = resp.payload.invoiceCategory.fresh_flower_multiple;
					updatedState.categoryLabor = resp.payload.invoiceCategory.labor;

					_this.setState(updatedState);

					_this.componentChanged( 'invoicing_category', updatedState );
				}
			}
		});
	},

	onDefaultCatClick: function(event) {
		var updatedState = {};
		updatedState.defaultCategoryCheck = event.target.checked ? 'checked' : '';
		if ( undefined !== this.refs.categoryName ) {
			updatedState.categoryName = this.refs.categoryName.state.categoryName;
		}
		updatedState.categoryHardgood = this.refs.hardgoodMultiple.state.hardgoodMultiple;
		updatedState.categoryFreshFlower = this.refs.freshFlowerMultiple.state.freshFlowerMultiple;
		updatedState.categoryLabor = {
			global_labor_value: this.refs.applyLaborTo.state.globalLaborValue,
			flower: this.refs.applyLaborTo.state.flower,
			hardgood: this.refs.applyLaborTo.state.hardgood,
			base_price: this.refs.applyLaborTo.state.base_price,
			fee: this.refs.applyLaborTo.state.fee,
			rental: this.refs.applyLaborTo.state.rental
		};

		this.setState(updatedState);
	},

	removeInvoiceCategory: function(event) {
		var postdata = {
			action: 'delete_invoicing_categories',
			default_category: this.state.defaultCategory,
			category_id: this.state.categoryId
		};

		$.ajax({
			type: 'POST',
			url: window.stemcounter.ajax_url,
			data: postdata,
			success: function(resp){
				resp = JSON.parse(resp);
				if (resp.success) {
					alertify.success(resp.message);

					setTimeout(function() {
						location.reload();
					}, 1000);
				} else {
					alertify.error(resp.message);
				}
			}
		});
	},

	submit: function(e) {
		e.preventDefault();
		var postdata = {
			action: 'edit_invoicing_categories',
			default_category: document.getElementById('defaultCategory').checked,
			category_id: this.state.categoryId,
			category_name: this.refs.categoryName.state.categoryName,
			hardgood_multiple: this.refs.hardgoodMultiple.state.hardgoodMultiple,
			fresh_flower_multiple: this.refs.freshFlowerMultiple.state.freshFlowerMultiple,
			labor: this.refs.applyLaborTo.state
		};

		$.ajax({
			type: 'POST',
			url: window.stemcounter.ajax_url,
			data: postdata,
			success: function(resp){
				if (resp.success) {
					alertify.success(resp.message);

					setTimeout(function() {
						location.reload();
					}, 1000);
				} else {
					alertify.error(resp.message);
				}
			}
		});
	},

	render: function() {
		var deleteButton;
		if (this.state.categoryId != -1 &&
			this.state.categoryId != this.props.defaultCategoryId) {
			deleteButton = (
				<button type="button" className="btn btn-danger" onClick={this.removeInvoiceCategory}>Delete</button>
			);
		}

		/* Profile Layout */
		var categoryName;
		var modalFooter;
		var defaultCategoryCheckbox;

		if (this.props.layout === 'profile') {
			categoryName = (
				<RenderCategoryName categoryName={this.state.categoryName} ref="categoryName" layout={this.props.layout} />
			);

			defaultCategoryCheckbox = (
				<div className="clearfix form-group">
					<div className="form-group clearfix">
						<label className="col-xs-12 control-label">
							Make this Markup Profile the default one? &nbsp;
							<input id="defaultCategory" checked={this.state.defaultCategoryCheck} type="checkbox" onClick={this.onDefaultCatClick} />Yes
						</label>
					</div>
				</div>
			);

			modalFooter = (
				<div className="modal-footer">
					<div className="pull-left">*required</div>
					{deleteButton}
					<button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" name="action" className="btn btn-primary" onClick={this.submit}>Save</button>
				</div>
			);
		} else if ( 'proposal' == this.props.layout && 'view' == this.props.formState ) {
			categoryName = (
				<RenderCategoryName categoryName={this.state.categoryName} ref="categoryName" layout={this.props.layout} />
			);
		}

		var invoiceCategoryBody;

		if (this.state.categoryId == 'default' || this.state.showCategoryValues == false) {
			invoiceCategoryBody = (
				<div className="hide-category-input">
					<div className="modalpane-row clearfix">
						<HardgoodMultiple categoryHardgoodMult={this.state.categoryHardgood} changeValue={this.onCategoryHardgoodChange} ref="hardgoodMultiple" />
						<FreshFlowerMultiple categoryFreshFlowerMult={this.state.categoryFreshFlower} changeValue={this.onCategoryFreshFlowerChange} ref="freshFlowerMultiple" />
					</div>
					<ApplyLaborTo layout={this.props.layout} itemTypes={this.props.itemTypes} categoryLabor={this.state.categoryLabor} changeValue={this.onLaborChange} ref="applyLaborTo" />
				</div>
			);
		} else {
			invoiceCategoryBody = (
				<div>
					<div className="modalpane-row clearfix">
						<HardgoodMultiple categoryHardgoodMult={this.state.categoryHardgood} changeValue={this.onCategoryHardgoodChange} layout={this.props.layout} formState={this.props.formState} ref="hardgoodMultiple" />
						<FreshFlowerMultiple categoryFreshFlowerMult={this.state.categoryFreshFlower} changeValue={this.onCategoryFreshFlowerChange} layout={this.props.layout} formState={this.props.formState} ref="freshFlowerMultiple" />
					</div>
					<ApplyLaborTo layout={this.props.layout} itemTypes={this.props.itemTypes} categoryLabor={this.state.categoryLabor} formState={this.props.formState} changeValue={this.onLaborChange} ref="applyLaborTo" />
				</div>
			);
		}

		var renderSelectCategory = (
			<RenderSelectCategory categoryId={this.state.categoryId} categoriesList={this.props.categoriesList} onCategoryChange={this.onCategoryChange} refreshCategoryList={this.refreshCategoryList} layout={this.props.layout} ref="categorySelection" showHideCategoryValues={this.showHideCategoryValues} />
		);
		if ( (this.props.layout == 'event-details' || 'proposal' == this.props.layout) && this.props.formState == 'view') {
			renderSelectCategory = (<div></div>);
		}

		return (
			<div>
				{renderSelectCategory}
				{categoryName}
				{invoiceCategoryBody}
				{defaultCategoryCheckbox}
				{modalFooter}
			</div>
		);
	}
});
window.InvoicingCategories = InvoicingCategories;

var Payments = React.createClass({

	getInitialState: function() {
		var state = {
			paymentModuleState: 'scheduled',
			formState: this.props.formState,
			eventCost: this.props.eventCost,
			scheduledPayments: this.props.scheduledPayments,
			madePayments: this.props.madePayments,
			addToProposal: this.props.addScheduledPaymentsToProposal
		};

		state.scheduledPayments[state.scheduledPayments.length - 1].amount = this.calculateFinalPrice(state);

		return state;
	},

	componentWillReceiveProps: function(nextProps) {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		updatedState.formState = nextProps.formState;
		if (updatedState.formState != 'edit') {
			updatedState.scheduledPayments = nextProps.scheduledPayments;
			updatedState.madePayments = nextProps.madePayments;
		}
		updatedState.eventCost = nextProps.eventCost;
		updatedState.scheduledPayments[updatedState.scheduledPayments.length - 1].amount = this.calculateFinalPrice(updatedState);
		this.setState(updatedState);
	},

	componentDidUpdate: function() {
		var _this = this;
		$('.payment-date').datepicker({
			dateFormat: this.props.dateFormat,
			onClose: function(val, opts) {
				//We are dispatching an event because we need to trigger
				//the onChange for the datepicker
				var event = new Event('input', {
					'bubbles': true
				});
				var node = $(opts.input[0]);
				node[0].dispatchEvent(event);
			}
		});
	},

	componentWillUnmount: function() {
		$('.payment-date').datepicker('destroy');
	},

	handleChange: function(index, field, payment, event) {
		var updatedState = JSON.parse(JSON.stringify(this.state));

		if (payment === 'scheduled') {
			if (field === 'amount') {
				updatedState.scheduledPayments[index].amount = event.target.value;
			} else if (field === 'date') {
				updatedState.scheduledPayments[index].date = event.target.value;
			} else if (field === 'amountType' && index < updatedState.scheduledPayments.length - 1) {
				if (event.target.value === 'currency') {
					updatedState.scheduledPayments[index].amountType = 'percentage';
				} else if (event.target.value === 'percentage') {
					updatedState.scheduledPayments[index].amountType = 'currency';
				}
			}
			updatedState.scheduledPayments[updatedState.scheduledPayments.length - 1].amount = this.calculateFinalPrice(updatedState);
		} else if (payment === 'made') {
			if (field === 'amount') {
				updatedState.madePayments[index].paymentAmount = event.target.value;
			} else if (field === 'date') {
				updatedState.madePayments[index].paymentDate = event.target.value;
			} else if (field === 'note') {
				updatedState.madePayments[index].paymentNote = event.target.value;
			} else if (field === 'paymentType') {
				if (event.target.value === 'card') {
					updatedState.scheduledPayments[index].paymentType = 'card';
				} else if (event.target.value === 'check') {
					updatedState.scheduledPayments[index].paymentType = 'check';
				} else if (event.target.value === 'cash') {
					updatedState.scheduledPayments[index].paymentType = 'cash';
				}
			}
		}

		this.setState(updatedState);
	},

	onGeneralPaymentChange: function(event) {
		event.preventDefault();
		var updatedState = {};
		$('.payments-controls a').removeClass('btn-primary');
		$(event.target).addClass('btn-primary');

		if ($(event.target).hasClass('payments-scheduled')) {
			updatedState.paymentModuleState = 'scheduled';
		} else if ($(event.target).hasClass('payments-made')) {
			updatedState.paymentModuleState = 'made';
		}
		this.setState(updatedState);
	},

	onPaymentTypeChange: function(index, event) {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		updatedState.madePayments[index].paymentType = event.target.value;
		this.setState(updatedState);
	},

	replaceSelect2: function(node) {
		window.stemcounter.ScrollFix($(node), {});
	},

	calculateFinalPrice: function(state) {
		var scheduledPrice = 0;
		for (var i = 0; i < state.scheduledPayments.length; i++) {
			if (i < state.scheduledPayments.length - 1 && !isNaN(parseFloat(state.scheduledPayments[i].amount))) {
				if (state.scheduledPayments[i].amountType === 'percentage') {
					scheduledPrice += parseFloat(state.eventCost * (state.scheduledPayments[i].amount / 100));
				} else {
					scheduledPrice += parseFloat(state.scheduledPayments[i].amount);
				}
			}
		};
		return (state.eventCost - scheduledPrice).toFixed10(2);
	},

	onCheck: function(event) {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		if (event.target.checked) {
			updatedState.addToProposal = 1;
		} else {
			updatedState.addToProposal = 0;
		}
		this.setState(updatedState);
	},

	addPayment: function() {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		if (this.state.paymentModuleState === 'scheduled') {
			updatedState.scheduledPayments.splice(this.state.scheduledPayments.length - 1, 0, {
				paymentId: '-1',
				paymentName: 'Payment ' + this.state.scheduledPayments.length.toString(),
				amount: '0.00',
				amountType: 'currency',
				date: ''
			});
		} else if (this.state.paymentModuleState === 'made') {
			updatedState.madePayments.push({
				paymentId: '-1',
				paymentAmount: '0.00',
				paymentDate: '',
				paymentType: 'card',
				paymentNote: ''
			});
		}

		this.setState(updatedState);
	},

	removePayment: function(index, event) {
		event.preventDefault();
		var updatedState = JSON.parse(JSON.stringify(this.state));

		if (this.state.paymentModuleState === 'scheduled') {
			updatedState.scheduledPayments.splice(index, 1);
			updatedState.scheduledPayments.map(function(payment, index, arr) {
				if (index < arr.length - 1) {
					payment.paymentName = 'Payment ' + (index + 1).toString();
				}
			});

			updatedState.scheduledPayments[updatedState.scheduledPayments.length -1].amount = this.calculateFinalPrice(updatedState);
		} else if (this.state.paymentModuleState === 'made') {
			updatedState.madePayments.splice(index, 1);
		}

		this.setState(updatedState);
	},

	switchPaymentType: function(index, event) {
		event.preventDefault();
		var updatedState = JSON.parse(JSON.stringify(this.state));
		var calculatedValue = 0;

		if (updatedState.scheduledPayments[index].amountType === 'currency') {
			updatedState.scheduledPayments[index].amountType = 'percentage';
			if (this.state.eventCost > 0) {
				calculatedValue = ((updatedState.scheduledPayments[index].amount / this.state.eventCost) * 100).toFixed10(2);
			} else {
				updatedState.eventCost = 0;
				updatedState.scheduledPayments[updatedState.scheduledPayments.length - 1].amount = 0;
			}
			updatedState.scheduledPayments[index].amount = calculatedValue;
		} else if (updatedState.scheduledPayments[index].amountType === 'percentage') {
			updatedState.scheduledPayments[index].amountType = 'currency';
			if (this.state.eventCost > 0) {
				calculatedValue = (this.state.eventCost * (updatedState.scheduledPayments[index].amount / 100)).toFixed10(2);
			} else {
				updatedState.eventCost = 0;
				updatedState.scheduledPayments[updatedState.scheduledPayments.length - 1].amount = 0;
			}
			updatedState.scheduledPayments[index].amount = calculatedValue;
		}
		this.setState(updatedState);
	},

	getScheduledPayments: function() {
		var _this = this,
			paymentsHtml = false,
			paymentBody = false;
		var regExp = new RegExp('(&)?(#)?(;)?', 'g');

		if (_this.state.formState === 'edit') {
			paymentBody = _this.state.scheduledPayments.map(function(payment, index, arr) {
				var amountCurrency;
				var amountPercentage;
				var amountTypeSwitch

				if (payment.amountType === 'currency') {
					//currency
					var amountClass = payment.amount < 0 ? 'text-danger form-control' : 'form-control';
					var paymentAmountName = 'payments['+index+'][payment_amount]';
					var paymentAmountTypeName = 'payments['+index+'][payment_amount_type]';
					amountCurrency = (
						<div>
							<input type="text" className={amountClass} name={paymentAmountName} value={payment.amount} onChange={_this.handleChange.bind(_this, index, 'amount', 'scheduled')} />
							<input type="hidden" name={paymentAmountTypeName} value="currency" />
						</div>
					);

					//percentage
					amountClass = payment.amount < 0 ? 'text-danger form-control' : 'form-control';
					paymentAmountName = 'payments['+index+'][payment_amount]';
					paymentAmountTypeName = 'payments['+index+'][payment_amount_type]';
					var percentageValue = 0;
					if (_this.state.eventCost > 0) {
						percentageValue = ((payment.amount / _this.state.eventCost) * 100).toFixed10(2);
					}
					amountPercentage = (
						<div>
							<input type="text" className={amountClass} name={paymentAmountName} value={percentageValue} disabled="disabled"/>
							<span className="date-required">*</span>
						</div>
					);

					amountTypeSwitch = (
						<button type="button" className="amount-type-switcher btn btn-default form-control" onClick={_this.switchPaymentType.bind(_this, index)}>
						{String.fromCharCode(_this.props.prefCurrency.replace(regExp, ''))}<i className="fa fa-arrow-left"></i>%
						</button>
					);

				} else if (payment.amountType === 'percentage') {
					//percentage
					var amountClass = payment.amount < 0 ? 'text-danger form-control' : 'form-control';
					var paymentAmountName = 'payments[' + index + '][payment_amount]';
					var paymentAmountTypeName = 'payments['+index+'][payment_amount_type]';
					amountPercentage = (
						<div>
							<input type="text" className={amountClass} name={paymentAmountName} value={payment.amount} onChange={_this.handleChange.bind(_this, index, 'amount', 'scheduled')}/>
							<input type="hidden" name={paymentAmountTypeName} value="percentage" />
							<span className="date-required">*</span>
						</div>
					);

					//currency
					amountClass = payment.amount < 0 ? 'text-danger form-control' : 'form-control';
					paymentAmountName = 'payments['+index+'][payment_amount]';
					paymentAmountTypeName = 'payments['+index+'][payment_amount_type]';
					var currencyValue = _this.state.eventCost * (payment.amount / 100);
					amountCurrency = (
						<div>
							<input type="text" className={amountClass} name={paymentAmountName} value={currencyValue.toFixed10(2)} disabled="disabled" />
						</div>
					);

					amountTypeSwitch = (
						<button type="button" className="amount-type-switcher btn btn-default form-control" onClick={_this.switchPaymentType.bind(_this, index)}>
						{String.fromCharCode(_this.props.prefCurrency.replace(regExp, ''))}<i className="fa fa-arrow-right"></i>%
						</button>
					);
				}

				var removePayment;
				if (arr.length !== index + 1) {
					var removePayment = (
						<a href="#" className="remove-payment" onClick={_this.removePayment.bind(_this, index)}>x</a>
					);
				} else {
					amountClass = payment.amount < 0 ? 'text-danger form-control' : 'form-control';
					paymentAmountName = 'payments['+index+'][payment_amount]';
					paymentAmountTypeName = 'payments['+index+'][payment_amount_type]';
					amountCurrency = (
						<div>
							<input type="text" className={amountClass} name={paymentAmountName} value={payment.amount} />
							<input type="hidden" name={paymentAmountTypeName} value="currency" />
						</div>
					);
					amountTypeSwitch = false;
				}

				var paymentIdName = 'payments['+index+'][payment_id]';
				var paymentNameName = 'payments['+index+'][payment_name]';
				var paymentDateName = 'payments['+index+'][payment_date]';

				return (
					<tr>
						<td>
							<input type="hidden" name={paymentIdName} value={payment.paymentId} />
							{payment.paymentName}
							<input type="hidden" name={paymentNameName} value={payment.paymentName}/>
						</td>
						<td>{amountCurrency}<span className="date-required">*</span></td>
						<td>{amountTypeSwitch}</td>
						<td>{amountPercentage}</td>
						<td>
							<input type="text"
								className="payment-date form-control"
								placeholder={_this.props.dateFormat.toUpperCase()}
								name={paymentDateName}
								value={payment.date}
								onChange={_this.handleChange.bind(_this, index, 'date', 'scheduled')}
								ref="paymentDate"/><span className="date-required">*</span>
						</td>
						<td>{removePayment}</td>
					</tr>
				);
			});

			paymentsHtml = (
				<tbody>
					{paymentBody}
				</tbody>
			);
		} else if (_this.state.formState === 'view') {
			paymentBody = _this.state.scheduledPayments.map(function(payment, index, arr) {
				var amountCurrency;
				var amountPercentage;
				if (payment.amountType === 'currency') {
					var amountClass = payment.amount < 0 ? 'text-danger' : '';
					amountCurrency = (
						<div className={amountClass}>
							{String.fromCharCode(_this.props.prefCurrency.replace(regExp, ''))}{parseFloat(payment.amount).toFixed10(2)}
						</div>
					);

					//percentage
					if (index !== arr.length - 1) {
						amountClass = payment.amount < 0 ? 'text-danger form-control' : '';

						var percentageValue = 0;
						if (_this.state.eventCost > 0) {
							percentageValue = ((payment.amount / _this.state.eventCost) * 100).toFixed10(2);
						}
						amountPercentage = (
							<div className={amountClass}>{percentageValue}%</div>
						);
					} else {
						var percentageValue = 0;
						if (_this.state.eventCost > 0) {
							percentageValue = ((payment.amount / _this.state.eventCost) * 100).toFixed10(2);
						}
						amountPercentage = (
							<div className={amountClass}>{percentageValue}%</div>
						);
					}
				} else if (payment.amountType === 'percentage') {
					var amountClass = payment.amount < 0 ? 'text-danger' : '';
					amountPercentage = (<div className={amountClass}>{payment.amount}%</div>);

					//currency
					if (index !== arr.length - 1) {
						amountClass = payment.amount < 0 ? 'text-danger' : '';
						var currencyValue = (_this.state.eventCost * (payment.amount / 100)).toFixed10(2);
						amountCurrency = (
							<div className={amountClass}>{String.fromCharCode(_this.props.prefCurrency.replace(regExp, ''))}{currencyValue}</div>
						);
					}
				}
				return (
					<tr>
						<td>{payment.paymentName}</td>
						<td>{amountCurrency}</td>
						<td></td>
						<td>{amountPercentage}</td>
						<td>{payment.date}</td>
						<td></td>
					</tr>
				);
			});

			paymentsHtml = (
				<tbody>
					{paymentBody}
				</tbody>
			);
		}

		return paymentsHtml;
	},

	getMadePayments: function() {
		var _this = this,
			paymentsHtml = false,
			paymentBody = false;
		var regExp = new RegExp('(&)?(#)?(;)?', 'g');

		var paymentTypesList = [
			<option value="card">Card</option>,
			<option value="check">Check</option>,
			<option value="cash">Cash</option>
		];

		if (_this.state.formState === 'edit') {
			paymentBody = _this.state.madePayments.map(function(payment, index, arr) {
				var removePayment,
					amount;

				var removePayment = (
					<a href="#" className="remove-payment" onClick={_this.removePayment.bind(_this, index)}>x</a>
				);
				var amountClass = payment.paymentAmount < 0 ? 'text-danger form-control' : 'form-control';
				var paymentAmountName = 'payments['+index+'][payment_amount]';

				amount = (
					<div>
						<input type="text" className={amountClass} name={paymentAmountName} value={payment.paymentAmount} onChange={_this.handleChange.bind(_this, index, 'amount', 'made')} />
					</div>
				);

				var paymentIdName = 'payments['+index+'][payment_id]';
				var paymentDateName = 'payments['+index+'][payment_date]';
				var paymentTypeName = 'payments['+index+'][payment_type]';
				var paymentNoteName = 'payments['+index+'][payment_note]';

				return (
					<tr>
						<td colSpan="7">
							<table className="edit-made-payment-table">
								<thead>
									<tr>
										<th>Amount</th>
										<th>Date</th>
										<th>Type</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>{amount}<span className="date-required">*</span></td>
										<td>
											<input type="text"
												className="payment-date form-control"
												placeholder={_this.props.dateFormat.toUpperCase()}
												name={paymentDateName}
												value={payment.paymentDate}
												onChange={_this.handleChange.bind(_this, index, 'date', 'made')}
												ref="paymentDate"/><span className="date-required">*</span>
										</td>
										<td>
											<ReactSelect2 className="paymentType" name={paymentTypeName} children={paymentTypesList} onChange={_this.onPaymentTypeChange.bind(_this, index)} selectedValue={payment.paymentType} replaceSelect2={_this.replaceSelect2} />
										</td>
										<td>{removePayment}</td>
									</tr>
									<tr>
										<td><b>Note:</b></td>
										<td colSpan="6">
											<textarea className="made-payment-note form-control" name={paymentNoteName} onChange={_this.handleChange.bind(_this, index, 'note', 'made')}>{payment.paymentNote}</textarea>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				);
			});

			paymentsHtml = (
				<tbody>
					{paymentBody}
				</tbody>
			);
		} else if (_this.state.formState === 'view') {
			paymentBody = _this.state.madePayments.map(function(payment, index, arr) {
				var amount;
				var amountClass = payment.paymentAmount < 0 ? 'text-danger' : '';
				amount = (
					<div className={amountClass}>
						{String.fromCharCode(_this.props.prefCurrency.replace(regExp, ''))}{parseFloat(payment.paymentAmount).toFixed10(2)}
					</div>
				);

				return (
					<tr>
						<td>{amount}</td>
						<td>{payment.paymentDate}</td>
						<td>{payment.paymentType.charAt(0).toUpperCase() + payment.paymentType.substr(1)}</td>
						<td className="payment-note">{payment.paymentNote}</td>
						<td></td>
					</tr>
				);
			});

			paymentsHtml = (
				<tbody>
					{paymentBody}
				</tbody>
			);
		}

		return paymentsHtml;
	},

	render: function() {
		var paymentModuleHeader,
			paymentModuleBody,
			tableBody,
			addMoreButton;

		if (this.state.formState === 'edit') {
			if (this.state.paymentModuleState === 'scheduled') {
				var checked = this.state.addToProposal == 1 ? 'checked' : '';
				addMoreButton = (
					<div className="modal-footer">
						<label className="pull-left">
							Add payment schedule to proposal
							<input className="add-schedule-to-proposal" type="checkbox" name="add_to_proposal" onChange={this.onCheck} checked={checked}/>
						</label>
						<button className="btn btn-default" type="button" onClick={this.addPayment}>Add More</button>
					</div>
				);
			} else {
				addMoreButton = (
					<div className="modal-footer">
						<button className="btn btn-default" type="button" onClick={this.addPayment}>Add More</button>
					</div>
				);
			}
		} else if (this.state.formState === 'view') {
			if (this.state.paymentModuleState === 'scheduled') {
				var checked = this.state.addToProposal == 1 ? 'checked' : '';
				addMoreButton = (
					<div className="modal-footer">
						<label className="pull-left">
							Add payment schedule to proposal
							<input className="add-schedule-to-proposal" type="checkbox" name="add_to_proposal" onChange={this.onCheck} checked={checked} disabled />
						</label>
					</div>
				);
			}
		}

		if (this.state.paymentModuleState === 'scheduled') {
			paymentModuleHeader = (
				<div className="payments-controls">
					<button href="#"
						className="btn-primary payments-scheduled"
						onClick={this.onGeneralPaymentChange}>Scheduled</button>
					<input type="hidden" name="payments_scheduled" value="1" />
					<button href="#"
						className="payments-made"
						onClick={this.onGeneralPaymentChange}>Made</button>
					<input type="hidden" name="payments_made" value="0" />
				</div>
			);
			tableBody = this.getScheduledPayments();
			paymentModuleBody = (
				<table className="table table-striped table-advance table-hover table-payments-scheduled">
					<thead>
						<tr>
							<th>Name</th>
							<th>Amount</th>
							<th></th>
							<th>%</th>
							<th>Date</th>
							<th></th>
						</tr>
					</thead>
					{tableBody}
				</table>
			);
		} else if (this.state.paymentModuleState === 'made') {
			paymentModuleHeader = (
				<div className="payments-controls">
					<button href="#"
						className="payments-scheduled"
						onClick={this.onGeneralPaymentChange}>Scheduled</button>
					<input type="hidden" name="payments_scheduled" value="0" />
					<button href="#"
						className="btn-primary payments-made"
						onClick={this.onGeneralPaymentChange}>Made</button>
					<input type="hidden" name="payments_made" value="1" />
				</div>
			);
			tableBody = this.getMadePayments();
			if (this.state.formState === 'edit') {
				paymentModuleBody = (
					<table className="table table-striped table-advance table-hover table-payments-made">
						<thead>
							<tr>
							</tr>
						</thead>
						{tableBody}
					</table>
				);
			} else {
				paymentModuleBody = (
					<table className="table table-striped table-advance table-hover table-payments-made">
						<thead>
							<tr>
								<th>Amount</th>
								<th>Date</th>
								<th>Type</th>
								<th>Note</th>
								<th></th>
							</tr>
						</thead>
						{tableBody}
					</table>
				);
			}
		}

		return (
			<div>
				{paymentModuleHeader}
				{paymentModuleBody}
				{addMoreButton}
			</div>

		);
	}
});
window.Payments = Payments;

var IncludeOnProposal = React.createClass({displayName: "IncludeOnProposal",
	name: 'include',

	render: function() {
		return (
			React.createElement("div", {className: "col-sm-4 nosidepadding"},
				React.createElement("label", {className: "col-sm-12 checkbox-label"},
					React.createElement("input", {type: "checkbox", name: this.name, value: "1", defaultChecked: this.props.defaultValue == 0 ? true : false }),
					" ", this.props.label ? this.props.label : 'Include on proposal'
				)
			)
		);
	}
});
window.IncludeOnProposal = IncludeOnProposal;

var NoTax = React.createClass({displayName: "NoTax",
	name: 'tax',

	render: function() {
		return (
			<div>
				<label className="checkbox-label no-tax-label">
					<div className="lbl">Taxable</div>
					<label><input type="checkbox" name={this.name} value="1" defaultChecked={this.props.defaultValue == 1} /> {this.props.label ? this.props.label : 'Taxable'}</label>
				</label>
			</div>
		);
	}
});
window.NoTax = NoTax;

var InvoicingCategoriesProfile = React.createClass({
	getInitialState: function() {
		return {
			categoriesList: this.props.categoriesList,
			defaultCategoryId: this.props.defaultCategoryId
		}
	},
	createCategory:function(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);

		var category = {
			id: -1,
			name: '',
			base_price_labor: 0,
			fee_labor: 0,
			flower_labor: 0,
			fresh_flower_multiple: 0,
			global_labor_value: 0,
			hardgood_labor: 0,
			hardgood_multiple: 0,
			rental_labor: 0
		};

		updatedState.categoriesList.splice(0, 0, category);
		this.setState(updatedState);
	},
	categoryRemoved: function(id) {
		var updatedState = $.extend(true, {}, this.state);

		for (var i = 0; i < updatedState.categoriesList.length; i++) {
			if ( id == updatedState.categoriesList[i].id ) {
				delete updatedState.categoriesList[i];
				break;
			}
		}
		this.setState(updatedState);
	},
	onDefaultCat: function( id ) {
		var updatedState = $.extend(true, {}, this.state);

		updatedState.defaultCategoryId = id;
		this.setState(updatedState);
	},
	onChange: function( id, type, values ){
		var updatedState = $.extend(true, {}, this.state);

		for (var i = 0; i < updatedState.categoriesList.length; i++) {
			if ( id == updatedState.categoriesList[i].id ) {
				updatedState.categoriesList[i][type] = values;
				break;
			}
		}

		this.setState(updatedState);
	},
	onNewCat: function( id ) {
		var updatedState = $.extend(true, {}, this.state);

		for (var i = 0; i < updatedState.categoriesList.length; i++) {
			if ( -1 == updatedState.categoriesList[i].id ) {
				updatedState.categoriesList[i].id = id;
				break;
			}
		}

		this.setState(updatedState);
	},
	render: function() {
		return (
			<div className="markup-profiles-wrapper">
				<div className="pane-row">
					<a href="#" className="cta-link" onClick={this.createCategory}>+ Add New Category</a>
				</div>
				{this.state.categoriesList.map(function(category, index){
					return (
						<CategoriesRow
							key={'category-' + category.id}
							ref={'category_' + category.id}
							category={category}
							index={index}
							defaultCategoryId={this.state.defaultCategoryId}
							layout={this.props.layout}
							onChange={this.onChange}
							onDefaultCat={this.onDefaultCat}
							categoryRemoved={this.categoryRemoved}
							onNewCat={this.onNewCat}
						/>
					);
				}, this)}
			</div>
		);
	}
});

var CategoriesRow = React.createClass({
	getInitialState: function() {
		var itemTypes = this.props.itemTypes,
			layout = 'new_profile',
			defaultCategory = false,
			defaultCategoryCheck = '',
			categoryId = parseInt(this.props.category.id),
			categoryName = '',
			categoryHardgood = 1,
			categoryFreshFlower = 1,
			categoryLabor = {
				global_labor_value: 0,
				flower: 0,
				hardgood: 0,
				base_price: 0,
				fee: 0,
				rental: 0
			},
			showCategoryValues = true;

		defaultCategoryCheck = parseInt(this.props.defaultCategoryId) == categoryId ? true : false;

		if (categoryId > 0) {
			categoryName = this.props.category.name;
			categoryHardgood = this.props.category.hardgood_multiple;
			categoryFreshFlower = this.props.category.fresh_flower_multiple;

			categoryLabor = {
				global_labor_value: this.props.category.global_labor_value,
				flower: this.props.category.flower_labor,
				hardgood: this.props.category.hardgood_labor,
				base_price: this.props.category.base_price_labor,
				fee: this.props.category.fee_labor,
				rental: this.props.category.rental_labor
			};
		}

		return {
			itemTypes: itemTypes,
			layout: layout,
			categoryId: categoryId,
			defaultCategory: defaultCategory,
			defaultCategoryCheck: defaultCategoryCheck,
			categoryName: categoryName,
			categoryHardgood: categoryHardgood,
			categoryFreshFlower: categoryFreshFlower,
			categoryLabor: categoryLabor,
			showCategoryValues: showCategoryValues,
			onLabor: parseFloat( categoryLabor.global_labor_value ) > 0 ? true : false
		};
	},
	componentWillReceiveProps: function(nextProps) {
			var updatedState = $.extend(true, {}, this.state);
			updatedState.defaultCategoryCheck = parseInt(nextProps.defaultCategoryId) == parseInt(this.state.categoryId) ? true : false;
			this.setState(updatedState);

	},
	componentChanged: function( type, values ){
		if (typeof this.props.onChange != 'undefined') {
			this.props.onChange( this.state.categoryId, type, values );
		}
	},

	onCategoryHardgoodChange: function(value) {
		if ( this.state.categoryHardgood == value ) {
			return;
		} else {
			this.setState({
				categoryHardgood: value
			}, this.submit);
		}
		/*this.componentChanged( 'category_hardgood', value );*/
	},

	onCategoryFreshFlowerChange: function(value) {
		if ( this.state.categoryFreshFlower == value ) {
			return;
		} else {
			this.setState({
				categoryFreshFlower: value
			}, this.submit);
		}

		/*this.componentChanged( 'category_flower', value );*/
	},

	onNameChange: function( value ) {
		if ( this.state.categoryName == value ) {
			return;
		} else {
			this.setState({
				categoryName: value
			}, this.submit);
		}

		/*this.componentChanged( 'name', value );*/
	},
	onLaborChange: function(values) {
		if ( this.state.categoryLabor.base_price == values.base_price &&
			this.state.categoryLabor.fee == values.fee &&
			this.state.categoryLabor.flower == values.flower &&
			this.state.categoryLabor.global_labor_value == values.globalLaborValue &&
			this.state.categoryLabor.hardgood == values.hardgood &&
			this.state.categoryLabor.rental == values.rental ) {
			return;
		}

		this.componentChanged( 'labor', values );
	},

	showHideCategoryValues: function(showValues) {
		var updatedState = $.extend(true, {}, this.state);
		updatedState.showCategoryValues = showValues;
		this.setState(updatedState);
	},

	onDefaultCatClick: function(event) {
		var updatedState = $.extend(true, {}, this.state);
		updatedState.defaultCategoryCheck = event.target.checked ? true : false;
		if ( event.target.checked ) {
			this.props.onDefaultCat( this.state.categoryId );
		}
		this.setState(updatedState, this.submit);
	},

	removeInvoiceCategory: function(event) {
		var _this = this;
		var postdata = {
			action: 'delete_invoicing_categories',
			default_category: this.state.defaultCategory,
			category_id: this.state.categoryId
		};

		$.ajax({
			type: 'POST',
			url: window.stemcounter.ajax_url,
			data: postdata,
			success: function(resp){
				if (resp.success) {
					_this.props.categoryRemoved(_this.state.categoryId)
				} else {
					alertify.error(resp.message);
				}
			}
		});
	},

	onAddLabor: function(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);
		var active_label = $(this.refs.labor_row).find('.active');

		if ( ! e.target.parentElement.classList.contains('active') ) {
			active_label.removeClass('active');
			e.target.parentElement.classList.add('active');

			updatedState.onLabor = e.target.value == 'yes' ? true : false;
			for (var key in updatedState.categoryLabor) {
				if (updatedState.categoryLabor.hasOwnProperty(key)) {
					if ( ! updatedState.onLabor ) {
						updatedState.categoryLabor[key] = 0;
					}
				}
			}
			this.setState(updatedState, this.submit);
		}
	},

	globalLaborValue: function(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);

		updatedState.categoryLabor.global_labor_value = null == e.target.value ? 0 : parseFloat(parseFloat(e.target.value).toFixed10(2));

		for (var key in updatedState.categoryLabor) {
			if (updatedState.categoryLabor.hasOwnProperty(key)) {
				if ( 0 < updatedState.categoryLabor[key] ) {
					updatedState.categoryLabor[key] = updatedState.categoryLabor.global_labor_value;
				}
			}
		}

		this.setState(updatedState, this.submit);
	},

	onApplyLabor: function(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);

		if ( e.target.classList.contains('active') ) {
			e.target.classList.remove('active');
			updatedState.categoryLabor[e.target.dataset.type] = 0;

		} else {
			e.target.classList.add('active');
			updatedState.categoryLabor[e.target.dataset.type] = this.state.categoryLabor.global_labor_value;
		}
		this.setState(updatedState, this.submit);
	},

	submit: function() {
		var _this = this;
		var updatedState = $.extend(true, {}, this.state);
		var postdata = {
			action: 'edit_invoicing_categories',
			default_category: 'checked' == this.state.defaultCategoryCheck ? true : false,
			category_id: this.state.categoryId,
			category_name: this.refs.categoryName.state.categoryName,
			hardgood_multiple: this.refs.hardgoodMultiple.state.hardgoodMultiple,
			fresh_flower_multiple: this.refs.freshFlowerMultiple.state.freshFlowerMultiple,
			labor: this.state.categoryLabor
		};

		$.ajax({
			type: 'POST',
			url: window.stemcounter.ajax_url,
			data: postdata,
			success: function(resp){
				if (resp.success) {
					if ( undefined !== resp.payload.invoice_category_id ) {
						updatedState.categoryId = resp.payload.invoice_category_id;
						_this.props.onNewCat(resp.payload.invoice_category_id);
						_this.setState(updatedState);
					}
				} else {
					alertify.error(resp.message);
				}
			}
		});
	},

	render: function() {
		var _this = this,
			deleteButton = null,
			laborValue = null,
			laborApply = null;

		if (this.state.categoryId != -1 &&
			this.state.categoryId != this.props.defaultCategoryId) {
			deleteButton = (
				<a href="#" className="btn-remove remove-category" onClick={function(e){
					var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0];
					e.preventDefault();
					stemcounter.confirmModal({
						confirmCallback: function(){
							_this.removeInvoiceCategory();
						}
					});
				}}><i className="fa fa-trash-o"></i></a>
			);
		}

		if ( this.state.onLabor ) {
			laborValue = (
				<div className="pane labor-settings">
					<label className="control-label">Labor %</label>
					<div>
						<input type="text" className="form-control" defaultValue={this.props.category.global_labor_value} onChange={this.globalLaborValue} />
					</div>
				</div>
			);

			laborApply = (
				<div className="pane labor-settings">
					<label className="control-label">Apply Labor To</label>
					<div className="labor-applyto">
						<span className={'labor-type ' + (0 < this.state.categoryLabor.flower ? 'active' : '') } onClick={this.onApplyLabor} data-type="flower">FLOWER</span>
						<span className={'labor-type ' + (0 < this.state.categoryLabor.fee ? 'active' : '') } onClick={this.onApplyLabor} data-type="fee">FEE</span>
						<span className={'labor-type ' + (0 < this.state.categoryLabor.rental ? 'active' : '') } onClick={this.onApplyLabor} data-type="rental">RENTAL</span>
						<span className={'labor-type ' + (0 < this.state.categoryLabor.base_price ? 'active' : '') } onClick={this.onApplyLabor} data-type="base_price">BASEPRICE</span>
						<span className={'labor-type ' + (0 < this.state.categoryLabor.hardgood ? 'active' : '') } onClick={this.onApplyLabor} data-type="hardgood">HARDGOOD</span>
					</div>
				</div>
			);
		} else {
			laborValue = (
				<div className="pane no-content"></div>
			);

			laborApply = (
				<div className="pane no-content"></div>
			);
		}

		return (
			<div className="row pane-row markup-profile-row" data-id={this.props.category.id} data-index={this.props.index}>


						<RenderCategoryName categoryName={this.state.categoryName} ref="categoryName" layout={this.props.layout} onNameChange={this.onNameChange}/>
						<div className="pane">
							<label className="control-label">Hardgood</label>
							<div>
								<HardgoodMultiple categoryHardgoodMult={this.state.categoryHardgood} changeValue={this.onCategoryHardgoodChange} ref="hardgoodMultiple" layout={this.props.layout} />
							</div>
						</div>
						<div className="pane">
							<label className="control-label">Flower</label>
							<div>
								<FreshFlowerMultiple categoryFreshFlowerMult={this.state.categoryFreshFlower} changeValue={this.onCategoryFreshFlowerChange} ref="freshFlowerMultiple" layout={this.props.layout} />
							</div>
						</div>
						<div className="pane">
							<label className="control-label">Add Labor %?</label>
							<div ref="labor_row" className="clearfix style-form form-group labor_settings">
								<label className={'control-label ' + (parseFloat(this.state.categoryLabor.global_labor_value) > 0 ? 'active' : '')}><input type="checkbox" value="yes" onChange={this.onAddLabor} /> <strong>YES</strong> </label>
								<label className={'control-label ' + (parseFloat(this.state.categoryLabor.global_labor_value) <= 0 ? 'active' : '')}><input type="checkbox" value="no" onChange={this.onAddLabor} /> <strong>NO</strong> </label>
							</div>
						</div>

						{laborValue}
						{laborApply}
						<div className="pane default-category">
							<label className="control-label">Default</label>
							<div className="">
								<input className="styled-radio" id={'defaultCategory_' + this.props.category.id} defaultChecked={this.state.defaultCategoryCheck} type="radio" name="default-invoicing-category" onChange={this.onDefaultCatClick} /><label htmlFor={'defaultCategory_' + this.props.category.id }></label>
							</div>
							{deleteButton}
						</div>


			</div>
		);
	}
});

var GenericModal = React.createClass({
	seq_id: 0,

	createWrapper: function(classNames){
		GenericModal.prototype.seq_id ++;
		if ( undefined === classNames ) {
			classNames = '';
		} else {
			classNames = ' '+ classNames;
		}
		var id = 'generic-modal-' + GenericModal.prototype.seq_id;

		return $('<div class="modal fade' + classNames + '" id="' + id + '" role="dialog" aria-labelledby="' + id + '" aria-hidden="true"></div>').appendTo( $('body') );
	},

	getInitialState: function(){
		// GenericModal.prototype.seq_id ++;

		return {};
	},

	onSave: function() {
		var $modal = $('#' + this.id);

		if ( false !== this.props.onSave( $modal, ReactDOM.findComponent ) ) {
			$modal.modal('hide');
		};
	},

	onModalHidden: function(){
		ReactDOM.unmountComponentAtNode( ReactDOM.findDOMNode(this).parentElement );

		$('#' + this.id).remove();
		// $('body').scrollTop( this.state.scrollPosition );
	},

	componentDidMount: function(){
		/* don't close the modal when clicked outside the popup */
		$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';

		this.id = ReactDOM.findDOMNode(this).parentElement.id;

		var $modal = $('#' + this.id);

		if ( this.props.className ) {
			$modal.addClass(this.props.className);
		}

		$modal.modal('show').on('hidden.bs.modal', this.onModalHidden);
	},

	render: function() {
		var footer = null,
			close = null;

		if ( ! this.props.noFooter ) {
			footer = (
				<div className="modal-footer">
					<div>
						<button className="btn btn-primary btn-submit" type="button" onClick={this.onSave}>{this.props.saveLabel || 'Save'}</button>
					</div>
				</div>
			);
		}

		if ( ! this.props.noClose ) {
			close = (
				<button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			);
		}

		return (
			<div className="modal-dialog">
				<div className="modal-content">
					<div className="modal-header">
						{close}
						<h4 className="modal-title">{this.props.title}</h4>
					</div>
					<div className="modal-body">
						{this.props.content}
					</div>
					{footer}
				</div>
			</div>
		);
	}

});

window.GenericModal = GenericModal;

window.EventArrangements = React.createClass({
	myrefs: {},

	componentDidMount: function() {
		if ( 'edit' !== this.props.proposalState ) {
			initPhotoSwipeFromDOM( '.arrangement-photos' );
		} else {
			this.maybeInitMediaFrame();
		}
	},

	componentDidUpdate: function() {
		if ( 'edit' !== this.props.proposalState ) {
			initPhotoSwipeFromDOM( '.arrangement-photos' );
		} else {
			this.maybeInitMediaFrame();
		}
	},

	maybeInitMediaFrame: function(){
		if (undefined === stemcounter.arrangement_photo_frame) {
			if (!wp.media.model.Query.prototype.is_custom) {
				wp.media.model.Query.prototype.is_custom = true;
				wp.media.model.Query.prototype.initialize = function(models, options) {
					var allowed;

					options = options || {};
					wp.media.model.Attachments.prototype.initialize.apply(this, arguments);

					this.args = options.args;
					this._hasMore = true;
					this.created = new Date();

					this.filters.order = function(attachment) {
						var orderby = this.props.get('orderby'),
							order = this.props.get('order');

						if (!this.comparator) {
							return true;
						}

						// We want any items that can be placed before the last
						// item in the set. If we add any items after the last
						// item, then we can't guarantee the set is complete.
						if (this.length) {
							return 1 !== this.comparator(attachment, this.last(), {
								ties: true
							});

							// Handle the case where there are no items yet and
							// we're sorting for recent items. In that case, we want
							// changes that occurred after we created the query.
						} else if ('DESC' === order && ('date' === orderby || 'modified' === orderby)) {
							return attachment.get(orderby) >= this.created;

							// If we're sorting by menu order and we have no items,
							// accept any items that have the default menu order (0).
						} else if ('ASC' === order && 'menuOrder' === orderby) {
							return attachment.get(orderby) === 0;
						}

						// Otherwise, we don't want any items yet.
						return false;
					};

					// Observe the central `wp.Uploader.queue` collection to watch for
					// new matches for the query.
					//
					// Only observe when a limited number of query args are set. There
					// are no filters for other properties, so observing will result in
					// false positives in those queries.
					allowed = ['s', 'order', 'orderby', 'posts_per_page', 'post_mime_type', 'post_parent', 'author'];
					if (wp.Uploader && _(this.args).chain().keys().difference(allowed).isEmpty().value()) {
						this.observe(wp.Uploader.queue);
					}
				};
			}

			stemcounter.arrangement_photo_frame = stemcounter.getMediaModal( {
				frame: false,
				title: 'Upload Arrangement Photo',
				library: {
					type: 'image',
					author: userSettings.uid
				},
				button: {
					text: 'Add photo'
				},
				multiple: true // Set to true to allow multiple files to be selected
			} );

			// This will initialize an uploader that we need access to for drop to upload
			stemcounter.arrangement_photo_frame.open();
			stemcounter.arrangement_photo_frame.content.mode('browse');
			stemcounter.arrangement_photo_frame.close();
			// var region = new wp.media.controller.Region();
			// stemcounter.arrangement_photo_frame.browseContent( region );
			// region.view.collection.more();
		}
	},

	refCallback: function(id, ref){
		this.myrefs = this.myrefs;

		if ( ref != null ) {
			this.myrefs[ 'arr_' + id ] = ref;
		} else {
			delete this.myrefs[ 'arr_' + id ];
		}
	},

	render: function() {
		var totalArrangements = 0,
			filteredIndex = -1;
		for (var i = 0; i < this.props.arrangements.length; i++) {
			if ( ( ! this.props.addons && parseInt(this.props.arrangements[i].addon, 10) ) || ( this.props.addons && ! parseInt(this.props.arrangements[i].addon, 10) ) ) {
				continue;
			}

			totalArrangements ++;
		}

		return (
			<table>
				<thead>
					<tr>
						<th className="col-heading"></th>
						<th className="col-qty">QTY</th>
						{
							'view' == this.props.proposalState && parseInt( this.props.hideItemPrices, 10 ) ? null : (
								<th className="col-price">Price</th>
							)
						}
						{
							'view' == this.props.proposalState && parseInt( this.props.hideItemPrices, 10 ) ? null : (
								<th className="col-subtotal">Subtotal</th>
							)
						}
					</tr>
				</thead>
				{this.props.arrangements.map(function(arrangement, index){
					if ( 'view' == this.props.proposalState && ! parseInt( arrangement.include, 10 ) ) {
						return;
					}

					if ( ( ! this.props.addons && parseInt(arrangement.addon, 10) ) || ( this.props.addons && ! parseInt(arrangement.addon, 10) ) ) {
						return;
					}

					filteredIndex ++;

					return (
						<EventArrangementRow
							key={'arrangement-row-' + arrangement.id + '-' + index}
							ref={this.refCallback.bind( null, arrangement.id )}
							arrangement={arrangement}
							addons={this.props.addons}
							index={index}
							filteredIndex={filteredIndex}
							totalArrangements={totalArrangements}
							proposalState={this.props.proposalState}
							currencySymbol={this.props.currencySymbol}
							eventId={this.props.eventId}
							arrangementItemUpdated={this.props.arrangementItemUpdated}
							createArrangementAt={this.props.createArrangementAt}
							duplicateArrangement={this.props.duplicateArrangement}
							moveArrangement={this.props.moveArrangement}
							itemTypes={this.props.itemTypes}
							categoriesList={this.props.categoriesList}
							categoryId={this.props.categoryId}
							arrangementDeleted={this.props.arrangementDeleted}
							onArrangementEdit={this.props.onArrangementEdit}
							onArrangementSettingsSave={this.props.onArrangementSettingsSave}
							getArrangementItemPrice={this.props.getArrangementItemPrice}
							getArrangementItemLaborPercent={this.props.getArrangementItemLaborPercent}
							getArrangementItemInvoicingMultiplier={this.props.getArrangementItemInvoicingMultiplier}
							onDrop={this.props.onDrop}
							onApplyOnAll={this.props.onApplyOnAll}
							userRecipes={this.props.userRecipes}
							addUserRecipe={this.props.addUserRecipe}
							standaloneRecipes={this.props.standaloneRecipes || false}
							hideItemPrices={this.props.hideItemPrices}
							arrangements = {this.props.arrangements}
						/>
					);
				}, this)}
			</table>
		);
	}
});

window.EventArrangementRow = React.createClass({
	modalRef: false,

	dropzone: false,

	fileQueue: [],

	getInitialState: function(){
		return {
			is_drop_target: false,
		};
	},

	shouldComponentUpdate: function(nextProps, nextState) {
		return !_.isEqual(this.props, nextProps);
	},

	initDropZone: function(){
		if ( mOxie ) {
			if ( this.dropzone ) {
				this.dropzone.destroy();
			}

			this.dropzone = new mOxie.FileDrop({
				drop_zone: $(ReactDOM.findDOMNode(this)).find('.arrangement-item-wrapper').get(0)
			});

			this.dropzone.ondrop = this.onDropZoneDrop;

			this.dropzone.init();
		}
	},

	onDropZoneDrop: function(event) {
		var attachment_idx = this.props.arrangement.photos.length;
		var i;

		for ( i = 0; i < this.props.arrangement.photos.length; i++ ) {
			if ( null == this.props.arrangement.photos[i].image_id ) {
				attachment_idx = i;
				break;
			}
		}

		if ( attachment_idx == ( this.props.arrangement.photos.length ) ) {
			alertify.error( 'You have reached the maximum number of photos( ' + MAX_ARR_PHOTOS + ' ) for this arrangement.' );
			return;
		}

		stemcounter.arrangement_photo_frame.uploader.uploader.uploader.unbind('FileUploaded', this.onFileUploaded);
		stemcounter.arrangement_photo_frame.uploader.uploader.uploader.bind('FileUploaded', this.onFileUploaded);

		stemcounter.arrangement_photo_frame.uploader.uploader.uploader.bind('FilesAdded', this.onFilesAdded);

		// Add the file/s to the uploader
		for ( i = 0; i < this.dropzone.files.length; i++ ) {
			// Can't add any more attachments
			if ( ( MAX_ARR_PHOTOS - 1 ) < attachment_idx ) {
				alertify.error( 'You have reached the maximum number of photos( ' + MAX_ARR_PHOTOS + ' ) for this arrangement.' );
				break;
			}

			attachment_idx ++;
			stemcounter.arrangement_photo_frame.uploader.uploader.uploader.addFile( this.dropzone.files[ i ] );
		}

		setTimeout((function(){
			stemcounter.arrangement_photo_frame.uploader.uploader.uploader.unbind('FilesAdded', this.onFilesAdded);
		}).bind(this), 300);
	},

	onFilesAdded: function( up, files ) {
		var attachment_idx = this.props.arrangement.photos.length;
		var i;
		var arrangement = $.extend(true, {}, this.props.arrangement);

		for ( i = 0; i < this.props.arrangement.photos.length; i++ ) {
			if ( null == this.props.arrangement.photos[i].image_id ) {
				attachment_idx = i;
				break;
			}
		}

		if ( attachment_idx == ( this.props.arrangement.photos.length ) ) {
			return;
		}

		// Add the file/s to the uploader
		for ( i = 0; i < files.length; i++ ) {
			if ( plupload.FAILED === files[ i ].status ) {
				continue;
			}

			// Can't add any more attachments
			if ( ( MAX_ARR_PHOTOS - 1 ) < attachment_idx ) {
				break;
			}

			arrangement.photos[ attachment_idx ] = {
				image_id: files[i].id,
				image_url: stemcounter.theme_url + '/img/loading.gif',
				full_image_src: stemcounter.theme_url + '/img/loading.gif',
				full_image_w: 75,
				full_image_h: 75
			};

			attachment_idx ++;
		}

		this.props.arrangementItemUpdated( arrangement.id, arrangement );
	},

	onFileUploaded: function( up, file, response ) {
		var arrangement = $.extend(true, {}, this.props.arrangement),
			id = file.id,
			found_attachment = false,
			i;

		for ( i = 0; i < arrangement.photos.length; i++ ) {
			if ( id == arrangement.photos[ i ].image_id ) {
				found_attachment = true;
				arrangement.photos[ i ] = {
					image_id: file.attachment.get('id'),
					image_url: file.attachment.get('sizes').medium ? file.attachment.get('sizes').medium.url : file.attachment.get('url'),
					full_image_src: file.attachment.get('url'),
					full_image_w: file.attachment.get('width'),
					full_image_h: file.attachment.get('height')
				}

				// Remove the file from our queue
				/*this.fileQueue = _.reject( this.fileQueue, function( f ){
					return f.uid.replace( 'uid_', '' ) == id;
				} );*/

				break;
			}
		}

		if ( found_attachment ) {
			this.props.arrangementItemUpdated( arrangement.id, arrangement );
		}
	},

	componentDidMount: function() {
		if ( this.refs.arrangementNote ) {
			var note = $(this.refs.arrangementNote);

			if ( note.length ) {
				var height = note.prop('scrollHeight');
				note.css('height', height);
			}
		}

		if ( 'edit' !== this.props.proposalState ) {
			initPhotoSwipeFromDOM( '.arrangement-photos' );
		} else {
			this.initDropZone();
		}
	},

	componentDidUpdate: function() {
		if ( this.refs.arrangementNote ) {
			var note = $(this.refs.arrangementNote);

			if ( note.length ) {
				var height = note.prop('scrollHeight');
				note.css('height', height);
			}
		}

		if ( 'edit' !== this.props.proposalState ) {
			initPhotoSwipeFromDOM( '.arrangement-photos' );
		} else {
			this.initDropZone();
		}
	},

	onDragOver: function(event){
		event.preventDefault();
		event.stopPropagation();
	},

	onDrop: function(event){
		event.stopPropagation();
		event.preventDefault();

		this.props.onDrop( event );

		if ( event.dataTransfer.files.length ) {
			return;
		}

		var url = '',
			html = event.dataTransfer.getData('text/html'),
			_this = this;

		html = $( $.parseHTML( html ) );

		var files = [];

		if( html.children().length > 0 ){
			url = html.find('img').attr('src');
		} else {
			url = html.attr('src');
		}

		if ( ! url ) {
			alertify.error( 'Could not upload dropped image.' );
			return;
		}

		var attachment_idx = this.props.arrangement.photos.length;
		var i;

		for ( i = 0; i < this.props.arrangement.photos.length; i++ ) {
			if ( null == this.props.arrangement.photos[i].image_id ) {
				attachment_idx = i;
				break;
			}
		}

		if ( attachment_idx == ( this.props.arrangement.photos.length ) ) {
			alertify.error( 'You have reached the maximum number of photos( ' + MAX_ARR_PHOTOS + ' ) for this arrangement.' );
			return;
		}

		var arrangement = $.extend(true, {}, _this.props.arrangement);
		arrangement.photos[ attachment_idx ].image_id = -1;
		arrangement.photos[ attachment_idx ].image_id = -1;
		arrangement.photos[ attachment_idx ] = {
			image_id: -1,
			image_url: stemcounter.theme_url + '/img/loading.gif',
			full_image_src: stemcounter.theme_url + '/img/loading.gif',
			full_image_w: 75,
			full_image_h: 75
		};

		this.props.arrangementItemUpdated( arrangement.id, arrangement );

		var	ajax_url = window.stemcounter.aurl({ action: 'sc_upload_dragged_media' }),
			data = {
				event_id: _this.props.eventId,
				arrangement_id: _this.props.arrangement.id,
				url: url
			};

		_this.uploading = true;
		// $('.proposal-section').removeClass('dragging');
		$('body').css( 'cursor', 'wait' );
		$.post(ajax_url, data, function (response) {
			if (response.success) {
				var arrangement = $.extend(true, {}, _this.props.arrangement);

				for (var i = 0; i < arrangement.photos.length; i++) {
					if ( -1 == arrangement.photos[i].image_id ) {
						_this.addPhotoToArrProposal(i, response.payload.photo);
						break;
					}
				}
			} else {
				console.log( response );
			}
		}).always(function(){
			_this.uploading = false;
			$('body').css( 'cursor', '' );
		});
	},

	arrangementEdit: function(e) {
		var field = e.target.dataset.field;
		var arrangement = $.extend(true, {}, this.props.arrangement);

		switch (field) {
			case 'name':
				arrangement.name = e.target.value;
				break;
			case 'quantity':
				arrangement.quantity = e.target.value;
				arrangement.subTotal = arrangement.quantity * arrangement.total;
				break;
			case 'price':
				if ( '' === e.target.value ) {
					arrangement.override_cost = null;
					arrangement.subTotal = arrangement.quantity * arrangement.total;
				} else {
					arrangement.override_cost = e.target.value;
					arrangement.subTotal = arrangement.quantity * parseFloat( arrangement.override_cost );

				}
				break;
			case 'note':
				arrangement.note = e.target.value;
				break;
			default:
				break;
		}

		this.props.arrangementItemUpdated(arrangement.id, arrangement);
	},

	addPhotoToArrProposal: function(photoIndex, imageState, arrangementIndex) {
		var arrangement = $.extend(true, {}, this.props.arrangement);

		arrangement.photos[photoIndex] = imageState;

		this.props.arrangementItemUpdated(arrangement.id, arrangement);
	},

	removePhotoFromArrProposal: function(photoIndex, imageState, arrangementIndex) {
		var arrangement = $.extend(true, {}, this.props.arrangement);

		arrangement.photos[photoIndex] = imageState;

		this.props.arrangementItemUpdated(arrangement.id, arrangement);
	},

	addPhoto: function(e){
		e.preventDefault();

		//For multiple photos:
		var _this = this;
		var slots_exceeded = true;

		stemcounter.arrangement_photo_frame.open();
		stemcounter.arrangement_photo_frame.off('select').on('select', function() {
			stemcounter.arrangement_photo_frame.state().get('selection').each( function( value ) {
				slots_exceeded = true;
				for (var i = 0; i < _this.props.arrangement.photos.length; i++) {
					if ( null === _this.props.arrangement.photos[i].image_id && i < MAX_ARR_PHOTOS ) {
						_this.refs['photo_' + i].setPhoto( value.toJSON() );
						slots_exceeded = false;
						break;
					}
				}

			});

			if ( slots_exceeded ) {
				alertify.error( 'You have reached the maximum number of photos( ' + MAX_ARR_PHOTOS + ' ) for this arrangement.' );
			}
		});

		//For single photos:
/*
		for (var i = 0; i < this.props.arrangement.photos.length; i++) {
			if ( null === this.props.arrangement.photos[i].image_id ) {
				this.refs['photo_' + i].addPhoto();
				break;
			}
		} */
	},

	addProposalItem: function(e){
		e.preventDefault();

		this.props.createArrangementAt( this.props.index + 1, parseInt(this.props.arrangement.addon, 10) );
	},

	duplicateProposalItem: function(e) {
		e.preventDefault();

		this.props.duplicateArrangement(this.props.index + 1, this.props.arrangement.id);
	},

	openAddRecipeModal: function(e){
		e.preventDefault();

		var $modalWrapper = GenericModal.prototype.createWrapper('new-modal add-user-recipe-modal'),
			modalContent = <AddRecipeModalContent recipes={this.props.userRecipes} />;

		ReactDOM.render(
			<GenericModal
				title="Add Proposal Item From Recipe"
				content={modalContent}
				saveLabel="Add From Recipe"
				onSave={this.onAddRecipeModalSave}
			/>,
			$modalWrapper[0]
		);
	},

	onAddRecipeModalSave: function( $modal ){
		var recipe_id = parseInt( $modal.find('select.recipe-sel').val(), 10 );

		this.props.duplicateArrangement(this.props.index + 1, recipe_id, _.findWhere( this.props.userRecipes, { id: recipe_id } ));
	},

	moveArrangement: function(e){
		e.preventDefault();

		var dir = 'A' === e.target.nodeName ? e.target.dataset.dir : $(e.target).closest('a')[0].dataset.dir;

		this.props.moveArrangement( this.props.filteredIndex, dir );
	},

	onArrangementEdit: function(response, $form){
		if ( response.success ) {
			// Detach our listener
			delete stemcounter.editArrangementFormCaller;

			this.props.onArrangementEdit( response.payload.id, response.payload );
		}
	},

	onArrangementDelete: function(){
		if ( ! confirm( 'Are you sure you wish to delete this arrangement?' ) ) {
			return;
		}

		var url = window.stemcounter.aurl({
			action: 'sc_delete_arrangement',
			id: this.props.arrangement.id
		});

		$.get(url, {}, (function(response){
			if ( response.success ) {
				stemcounter.gaEvent('arrangement', 'deleted');

				this.props.arrangementDeleted( this.props.arrangement.id );
				this.closeModal();
			} else {
				alertify.error('Arrangement was not deleted. Please contact support.');
			}
		}).bind(this));
	},

	onClickArrangementSettings: function(e) {
		e.preventDefault();

		var modal = this.getModal('Arrangement settings');

		this.modalRef = ReactDOM.render(
			<ArrangementSettingsModal
				arrangement={this.props.arrangement}
				itemTypes={this.props.itemTypes}
				categoriesList={this.props.categoriesList}
				categoryId={parseInt(this.props.categoryId)}
				closeModal={this.closeModal}
				onSave={this.props.onArrangementSettingsSave}
				onDelete={this.onArrangementDelete}
				standaloneRecipes={this.props.standaloneRecipes}
				addUserRecipe={this.props.addUserRecipe}
			/>,
			modal
		);
	},

	onModalHidden: function(){
		if ( this.modalRef ) {
			this.modalRef.onModalHidden();
		}
	},

	getModal: function( label ) {
		/* don't close the modal when clicked outside the popup */
		$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';

		var newId = 'modal-arrangement-settings';

		if ( $('#'+ newId).length ) {
			var $template = $('#'+ newId);

			$template.find('.modal-title:first').text(label);
		} else {
			var $template = $('#ajax-modal-template').clone();
			$template.attr('id', newId);
			$template.addClass("new-modal");

			$template.find('.modal-title:first').text(label);
			$template.attr('aria-labelledby', newId);
			$template.on('hidden.bs.modal', function() {
				// dispose of template as it is no longer used
				$template.remove();
			});

			$template.appendTo($('body'));
		}

		$template.modal('show').on('hidden.bs.modal', this.onModalHidden);

		return $template.find('.modal-body:first')[0];
	},

	closeModal: function(){
		$('#modal-arrangement-settings').modal('hide');
	},

	onClickEditArrangement: function(e){
		e.preventDefault();

		if ( -1 == this.props.arrangement.id ) {
			alertify.error('Something unexpected happened. Please try again.');
			return;
		}

		stemcounter.page.deleteRecipe = this.deleteRecipe;

		var url = stemcounter.aurl({
			action: 'sc_edit_user_arrangement_form',
			items_only: 1,
			id: this.props.arrangement.id,
			recipe_id: 'edit-recipe-modal' != e.target.className ? e.target.parentElement.dataset.recipeId : e.target.dataset.recipeId,
			event_id: this.props.eventId,
			standaloneRecipes: this.props.standaloneRecipes ? '1' : ''
		});

		// Declare our component as the caller for the Edit Arrangement form
		// Once the form is submitted this.onArrangementEdit will be called
		stemcounter.editArrangementFormCaller = this;
		stemcounter.openAjaxModal('Edit Recipe', url, 'new-modal edit-recipe-modal');
		//stemcounter.openAjaxModal('Edit Recipe', url);
	},

	itemPriceCalculation: function(item){
		var calculation = parseFloat( item.cost ).toFixed10(2) + ' cost',
			markup = this.props.getArrangementItemInvoicingMultiplier( item, this.props.arrangement ),
			labor = this.props.getArrangementItemLaborPercent( item, this.props.arrangement );

		if ( markup && 1 < markup ) {
			calculation += ' * ' + markup + ' ' + ( 'flower' == item.type_raw ? 'floral' : item.type_raw ) + ' markup';
		}

		if ( labor && 0 < labor ) {
			calculation += ' * ' + ( 1 + ( labor / 100 ) ) + ' (Labor ' + labor + '%)';
		}

		return calculation;
	},

	addItemPhoto: function(e) {
		e.preventDefault();

		var arrangement = $.extend(true, {}, this.props.arrangement);
		var item_id = null;
		var attachment = {};

		if ( 'arr-item-attachment' != e.target.className ) {
			item_id = e.target.parentElement.dataset.itemId;
		} else {
			item_id = e.target.dataset.itemId;
		}

		for (var i = 0; i < arrangement.items.length; i++) {
			if ( item_id == arrangement.items[i].id ) {
				attachment = {
					full_image_h: arrangement.items[i].attachment.imageH,
					full_image_src: arrangement.items[i].attachment.imageURL,
					full_image_w: arrangement.items[i].attachment.imageW,
					image_id: arrangement.items[i].attachment.imageId,
					image_url: arrangement.items[i].attachment.imageURL
				}
			}
		}

		for (var i = 0; i < arrangement.photos.length; i++) {
			if ( null == arrangement.photos[i].image_id ) {
				this.addPhotoToArrProposal(i, attachment);
				break;
			}
		}
	},

	renderArrangementItems: function() {
		var _this = this;
		var output = [];
		if ( 'edit' === _this.props.proposalState ) {
			if ( _this.props.arrangement.recipes.length ) {
				for (var i = 0; i < _this.props.arrangement.recipes.length; i++) {

					output.push(
						<tr key={'arrangement-' + _this.props.arrangement.id + '-' + _this.props.arrangement.recipes[i].id + '-recipe'} className="arrangement-items">
							<td className="arrangement-edit-recipe">
								<h5 className="arrangement-items-edit">
									<a href="#" className="edit-recipe-modal" data-arrangement-id={_this.props.arrangement.id} data-recipe-id={_this.props.arrangement.recipes[i].id} onClick={_this.onClickEditArrangement} title={_this.props.arrangement.recipes[i].name}>
										<i className="fa fa-pencil"></i> {_this.props.arrangement.recipes[i].name}
									</a>
								</h5>
							</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					);

					_this.props.arrangement.items.map(function(item){
						var itemName = null;
						if (item.attachment && item.attachment.imageId) {
							itemName = (
								<a href="#" title="Add item photo to arrangement" className="arr-item-attachment"  data-item-id={item.id} onClick={_this.addItemPhoto}><i className="fa fa-picture-o" aria-hidden="true"></i></a>
							);
						}

						if ( _this.props.arrangement.recipes[i].id == item.recipe_id ) {
							output.push(
								<tr key={'item-' + item.id + '-' + _this.props.arrangement.recipes[i].id + '-recipe'} className="arrangement-item-tr">
									<td>{itemName}{item.name}</td>
									<td>{item.quantity}</td>
									<td>
										<div className="item-price-wrap">
											{stemcounter.formatPrice(_this.props.getArrangementItemPrice( item, _this.props.arrangement, false ), true, _this.props.currencySymbol)}
											<div className="item-calculations-info">{_this.itemPriceCalculation(item)}</div>
										</div>
									</td>
									<td></td>
								</tr>
							);
						}
					});
				}
			}

			return output;
		} else {
			return null;
		}
	},

	getSimpleItemsList: function(){
		var itemsList = [];

		for (var i = 0; i < this.props.arrangement.items.length; i++) {
			var arrangementItem = this.props.arrangement.items[i];
			var item = stemcounter.getItemByKeypair(arrangementItem.itemKeypair, arrangementItem);
			if (!item) {
				continue;
			}

			if (arrangementItem.itemQuantity == 0) {
				continue;
			}

			var name = '';

			if ( item.deleted ) {
				name = item.itemName;
				// It seems like the variation name is already included in the item name ¯\_(ツ)_/¯
				/*if ( '' != item.itemVariationName && 'Default' != item.itemVariationName ) {
					name += ' - ' + item.itemVariationName;
				}*/
			} else {
				name = item.name;
				var variationId = arrangementItem.itemKeypair.split('_')[1];
				if ( variationId && '0' != variationId ) {
					for (var j = 0; j < item.variations.length; j++) {
						if ( item.variations[j].id == variationId ) {
							if ( 'Default' != item.variations[j].name ) {
								name += ' - ' + item.variations[j].name;
							}
							break;
						}
					};
				}
			}

			itemsList.push(name);
		};

		return itemsList;
	},

	addDescription: function(e){
		e.preventDefault();
		var arrangement = $.extend(true, {}, this.props.arrangement);
		var description = arrangement.note;
		var items = this.getSimpleItemsList();
		var newItemsDescription = '---\n' + arrangement.name + (items.length > 0 ? ' with ' + items.join(', ') : '') + '\n---';
		var descriptionRegex = /^---$((.|[\n\r]+)*?)^---$/m;

		if (descriptionRegex.test(description)) {
		  description = description.replace(descriptionRegex, newItemsDescription);
		} else {
		  description += '\n\n' + newItemsDescription;
		}

		arrangement.note = description;
		$(this.refs.arrangementNote).val( arrangement.note );
		this.props.arrangementItemUpdated(arrangement.id, arrangement);
	},

	addRecipe: function(e) {
		e.preventDefault();

		var _this = this,
			arrangement = $.extend(true, {}, this.props.arrangement),
			url = window.stemcounter.aurl({ action: 'sc_arrangement_recipe' }),
			data = {
				'arrangement_id': arrangement['id']
			};

		$.post(url, data, function (response) {
			if (response.success) {
				arrangement.recipes.push({id:response.payload.recipe_id, name:response.payload.name});
				_this.props.arrangementItemUpdated( arrangement['id'], arrangement );
			} else {
				console.log( response );
			}
		});
	},

	deleteRecipe: function( recipe_id ) {

		var _this = this,
			url = window.stemcounter.aurl({ action: 'sc_arrangement_recipe_delete' }),
			arrangement = $.extend(true, {}, this.props.arrangement),
			data = {
				'recipe_id': recipe_id,
				'arrangement_id': arrangement['id']
			};

		$.post(url, data, function (r) {
			if (r.success) {
				arrangement = $.extend(true, {}, _this.props.arrangement);
				// var items = $.extend(true, [], arrangement.items);

				for (var i = arrangement.recipes.length - 1; i >= 0; i--) {
					if ( recipe_id == arrangement.recipes[i].id ) {
						for (var ii = arrangement.items.length - 1; ii >= 0; ii--) {
							if ( arrangement.items[ii].recipe_id == recipe_id ) {
								arrangement.items.splice(ii, 1);
							}
						}
						arrangement.recipes.splice(i, 1);
						_this.props.arrangementItemUpdated( arrangement['id'], arrangement );
						break;
					}
				}
			} else {
				console.log( r );
			}
		});
	},

	renderArrangementAddDropdown: function(){
		var total_photos = 0;
		for (var i = 0; i < this.props.arrangement.photos.length; i++) {
			if ( null !== this.props.arrangement.photos[i].image_id ) {
				total_photos ++;
			}
		}

		var addPhotoClass = total_photos == 8 ? 'disabled' : '';

		return (
			<div className="add-dropdown">
				<a className={addPhotoClass} href="#" onClick={this.addPhoto}>Photo</a>
				<a href="#" onClick={this.addDescription}>Description</a>
				<a href="#" onClick={this.addRecipe}>Recipe</a>
				{! this.props.standaloneRecipes ? <div className="separator"></div> : null}
				{! this.props.standaloneRecipes ? <a href="#" onClick={this.addProposalItem}>Blank Item</a> : null}
				{! this.props.standaloneRecipes ? <a href="#" onClick={this.duplicateProposalItem}>Duplicate Item</a> : null}
				{! this.props.standaloneRecipes && this.props.userRecipes ? <a href="#" onClick={this.openAddRecipeModal}>Recipe Item</a> : null}
			</div>
		);
	},

	toggleArrangementAddDropdown: function(e){
		e.preventDefault();

		var $anchor = 'I' == e.target.tagName ? $(e.target.parentElement) : $(e.target);
		var $dd = 'I' == e.target.tagName ? $anchor.next() : $anchor.next();

		if ( $dd.is(':visible') ) {
			$dd.hide();
			$anchor.removeClass('active');
			$(document).off('click', this.maybeHideArrangementAddDropdown);
		} else {
			$dd.show();
			$anchor.addClass('active');
			$(document).on('click', this.maybeHideArrangementAddDropdown);
		}
	},

	maybeHideArrangementAddDropdown: function(e){
		var $el = $(e.target);

		if ( ! $el.hasClass('add-dropdown') ) {
			$('.edit-arrangement-content').removeClass('active');
			$('.arrangement-action-wrap .add-dropdown').hide();
			$(document).off('click', this.maybeHideArrangementAddDropdown);
		}
	},

	renderViewArrangement: function() {
		var price = parseInt( this.props.arrangement.is_percentage ) ? this.props.arrangement.override_cost : this.props.arrangement.total;
		return (
			<tr key={'arrangement-tr-' + this.props.arrangement.id + '-' + this.props.index} className="page-breaker">
				<td >
					<div className="arrangement-name">{this.props.arrangement.name}</div>
					<div className="arrangement-details">{this.props.arrangement.note.split("\n").map(function(line, i){
						return (
							<span key={'split-' + i}>
								{line}
								<br />
							</span>
						);
					})}</div>

					<div className="arrangement-photos clearfix" >

						{this.props.arrangement.photos.map(function(photo, index, array) {
							if (photo.image_id === null) return;
							return (
								<ArrangementPhoto
									key={'photo-' + index + '-' + photo.image_id}
									proposalState={this.props.proposalState}
									defaultValue={photo}
									inModal={false}
								/>
							);
						}, this)}
					</div>
					{this.renderArrangementItems()}
				</td>
				<td className="arrangement-quantity">{0 < parseInt( this.props.arrangement.is_percentage ) ? null : this.props.arrangement.quantity}</td>
				{parseInt( this.props.hideItemPrices, 10 ) ? null : <td className="arrangement-total">{stemcounter.formatPrice( price, true, this.props.currencySymbol, this.props.arrangement.is_percentage, true)}</td>}
				{parseInt( this.props.hideItemPrices, 10 ) ? null : <td className="arrangement-subtotal">{stemcounter.formatPrice(this.props.arrangement.subTotal, true, this.props.currencySymbol, false, true)}</td>}
			</tr>
		);
	},

	autoResize: function(e) {

		e.target.style.height = "5px";
		e.target.style.height = (e.target.scrollHeight)+"px";
	},

	//Add or remove a checkbox id to applies_on array
	toggleCheckboxInArray: function( the_array, checkbox_value, checked ) {
		var value_index = the_array.indexOf( checkbox_value );

		if ( undefined == checked ) {
			checked = ( -1 == value_index );
		}

		if ( -1 != value_index ) {
			the_array.splice( value_index, 1);
		}

		if ( checked ) {
			the_array.push( checkbox_value );
		}
	},

	//Find position of an arangment by it's id in all arrangements
	findPosition: function( id, arrangements ) {
		for (var i = 0; i < arrangements.length; i++) {
			if ( id == arrangements[i].id ) return i;
		}
	},

	appliesOnChanged: function(e) {
		var updatedState = {},
			arrangement = $.extend( true, {}, this.props.arrangement ),
			value = parseInt( e.target.value );

		if ( e.nativeEvent.shiftKey && null !== this.state.lastToggled && undefined !== this.state.lastToggled ) {
			var old_index = this.findPosition( this.state.lastToggled, this.props.arrangements ),
			new_index = this.findPosition( value, this.props.arrangements ),
			all_checked = (-1 != arrangement.applies_on.indexOf( this.state.lastToggled ) );

			if ( new_index < old_index ) {
				new_index = [old_index, old_index = new_index][0]; //Swap variables - one liner :D
			}

			for (var i = 0; i < this.props.arrangements.length; i++) {
				if ( ! parseInt( this.props.arrangements[i].include ) ||
					this.props.arrangements[i].addon !=  arrangement.addon ||
					this.props.arrangements[i].id == arrangement.id ||
					i < old_index || i > new_index
					) {
					continue;
				}
				if ( stemcounter.arr_can_be_applied_on (arrangement.id, this.props.arrangements[i].id , this.props.arrangements) ) {
					this.toggleCheckboxInArray( arrangement.applies_on, this.props.arrangements[i].id, all_checked );
				}
			}
		} else {
			this.toggleCheckboxInArray( arrangement.applies_on, value );
		}

		updatedState.lastToggled = value;
		this.setState( updatedState );
		this.props.arrangementItemUpdated(arrangement.id, arrangement);
	},

	//When user clicks on Check all checkbox in a pecent item
	onApplyOnAllChanged: function( e ) {
		var arrangement = $.extend( true, {}, this.props.arrangement );
		arrangement.is_applied_all = ( ( 1 != arrangement.is_applied_all ) | 0 ); // this thing "| 0" makes false to be 0 and true to be 1

		if ( arrangement.is_applied_all ) {
			this.props.onApplyOnAll( arrangement, this.props.arrangements );
		} 		console.log( arrangement.is_applied_all );

		this.props.arrangementItemUpdated( arrangement.id, arrangement );
	},

	appliesTitleClicked: function(e) {
		jQuery( e.target).closest('.applies').toggleClass('collapsed');
	},

	renderAppliesOn: function( ) {
		if ( ! parseInt( this.props.arrangement.is_percentage ) ) {
			return '';
		}

		var applies_on = [],
			not_applyable,
			sc_tooltip;

		for (var i = 0; i < this.props.arrangements.length; i++) {
			if ( ! parseInt( this.props.arrangements[i].include ) || this.props.arrangements[i].id == this.props.arrangement.id ) {
				continue;
			}

			not_applyable = ! stemcounter.arr_can_be_applied_on( this.props.arrangement.id, this.props.arrangements[i].id, this.props.arrangements );

			sc_tooltip = not_applyable ? <SC_Tooltip tooltip="This is a % item that depends on itself so cannot be checked." /> : '';

			if ( parseInt( this.props.arrangements[i].addon, 10 ) != parseInt( this.props.arrangement.addon ) )   {
				continue
			}

			var checked = ( -1 == this.props.arrangement.applies_on.indexOf( this.props.arrangements[i].id ) ) ? false : true;

			applies_on.push(
				<div key={'applies-on-' + i} className="applies-on">
				{sc_tooltip}
				<label>
					<input className={'applies-on-selects-'+this.props.arrangement.id} disabled={not_applyable || this.props.arrangement.is_applied_all} type="checkbox" checked={checked} value={this.props.arrangements[i].id} onChange={this.appliesOnChanged} />
					{this.props.arrangements[i].name}
				</label>
				</div> );
		}

		return(<div className={'applies ' + ( ( 0 !== this.props.arrangement.collapsed ) ? 'collapsed' : '' ) }>
				<span className="collapse-status"></span>
				<span className="applies-title" onClick={this.appliesTitleClicked}>Applies on</span>
				<div key={'applies-on-all'} className="applies-on check-all">
					<label>
						<input ref="checkAll" type="checkbox" checked={this.props.arrangement.is_applied_all} value={1} onChange={this.onApplyOnAllChanged} />
						Apply to all
					</label>
				</div>
				{applies_on}
			</div>)
	},

	renderEditArrangement: function() {
		var override_cost,
			isDraft = null,
			arrangement_quantity = ' ',
			output = [],
			className = 'arrangement-item-wrapper';

		if ( undefined != this.props.arrangement.override_cost && null !== this.props.arrangement.override_cost ) {
			override_cost = stemcounter.formatToDecimals( this.props.arrangement.override_cost ).toFixed10(2);
		} else {
			override_cost = '';
		}

		if ( ! parseInt(this.props.arrangement.include) ) {
			isDraft = (
				<a className="draft-arrangement disabled" ><i className="fa fa-eye-slash" aria-hidden="true"></i></a>
			);
		}

		if ( this.state.is_drop_target ) {
			className += ' dragging';
		}

		if ( 0 < parseInt( this.props.arrangement.is_percentage ) ) {
			arrangement_quantity = ' ';
		} else {
			arrangement_quantity =
			(<input type="text" defaultValue={parseInt(this.props.arrangement.quantity, 10)} onBlur={this.arrangementEdit} data-field="quantity" />)
		}

		output.push(
			<tr className={'arrangement-item-wrapper' + ( 0 < parseInt( this.props.arrangement.is_percentage, 10 ) ? ' arrangement-item-percentage' : '' )} id={'arrangement_' + this.props.arrangement.id} key={'arrangement-' + this.props.arrangement.id + '-' + this.props.index} onDrop={this.onDrop} onDragOver={this.onDragOver}>
				<td>
					<div className="arrangement-actions">
						<a href="#" className="edit-arrangement-invoicing" onClick={this.onClickArrangementSettings}><i className="fa fa-wrench"></i></a>
						<div className="arrangement-action-wrap">
							<a href="#" className="edit-arrangement-content" onClick={this.toggleArrangementAddDropdown}>
								<i className="fa fa-plus-square-o"></i>
							</a>
							{this.renderArrangementAddDropdown()}
						</div>
						{isDraft}
						<div className="arrangement-reordering">
							<a href="#" data-dir="up" onClick={this.moveArrangement} style={{'visibility': 0 == this.props.filteredIndex ? 'hidden' : 'visible'}}><i className="fa fa-arrow-up"></i></a>
							<a href="#" data-dir="down" onClick={this.moveArrangement} style={{'visibility': ( this.props.totalArrangements - 1 ) == this.props.filteredIndex ? 'hidden' : 'visible'}}><i className="fa fa-arrow-down"></i></a>
						</div>
					</div>
					<div className="arrangement-details">
						<div className="arrangement-name">
							<input type="text" defaultValue={this.props.arrangement.name} onBlur={this.arrangementEdit} data-field="name" placeholder="Arrangement name" disabled={(0 < parseInt(this.props.arrangement.id) ? false: true)}/>
						</div>
						<div className="arrangement-desc">
							<textarea id={'arrangement-note-' + this.props.arrangement.id} ref="arrangementNote" onBlur={this.arrangementEdit} data-field="note" onChange={this.autoResize} defaultValue={this.props.arrangement.note}
							 placeholder="Arrangement description" />
						</div>
						{this.renderAppliesOn()}
						<div className="arrangement-photos clearfix">
							{this.props.arrangement.photos.map(function(photo, index, array) {
								// if (photo.image_id === null) return;
								return (
									<ArrangementPhoto
										key={'photo-' + index + '-' + photo.image_id}
										proposalState={this.props.proposalState}
										ref={'photo_' + index}
										defaultValue={photo}
										photoIndex={photo.key}
										hidden={null == photo.image_id}
										arrangementIndex={this.props.index}
										addPhotoToArrProposal={this.addPhotoToArrProposal.bind(this, index)}
										removePhotoFromArrProposal={this.removePhotoFromArrProposal.bind(this, index)}
										inModal={false}
									/>
								);
							}, this)}
						</div>
					</div>
				</td>
				<td>{arrangement_quantity}</td>
				<td><span className="override-cost-wrap"><input ref="price_override" type="number" step="any" defaultValue={override_cost} placeholder={this.props.arrangement.total.toFixed10(2)} onBlur={this.arrangementEdit} data-field="price" /></span></td>
				<td>{stemcounter.formatPrice(this.props.arrangement.subTotal, true, this.props.currencySymbol)}</td>
			</tr>
		);

		output.push(
			this.renderArrangementItems()
		);

		return output;
	},


	render: function() {
		if ( 'edit' === this.props.proposalState ) {
			return (
				<tbody key={'arrangement-' + this.props.arrangement.id + '-' + this.props.index + '-tbody'}>
					{this.renderEditArrangement()}
				</tbody>
			);
		} else {
			return (
				<tbody key={'arrangement-' + this.props.arrangement.id + '-' + this.props.index + '-tbody'}>
					{this.renderViewArrangement()}
				</tbody>
			);

		}
	}
});

window.ArrangementSettingsModal = React.createClass({
	getInitialState: function() {
		var arr_state = 'live';
		if ( ! parseInt( this.props.arrangement.include, 10 ) && ! parseInt( this.props.arrangement.addon, 10 ) ) {
			arr_state = 'draft';
		} else if ( parseInt( this.props.arrangement.addon, 10 ) ) {
			arr_state = 'addon'
		}

		var arrValues = {};
		if ( null == this.props.arrangement.invoicing_category_id ) {
			for (var i = 0; i < this.props.categoriesList.length; i++) {
				if ( this.props.categoriesList[i].id == -Math.abs(this.props.categoryId) ) {
					arrValues = {
						hardgood_multiple: this.props.categoriesList[i].hardgood_multiple,
						fresh_flower_multiple: this.props.categoriesList[i].fresh_flower_multiple,
						labor: {
							global_labor_value: this.props.categoriesList[i].global_labor_value,
							flower: this.props.categoriesList[i].flower_labor,
							hardgood: this.props.categoriesList[i].hardgood_labor,
							base_price: this.props.categoriesList[i].base_price_labor,
							fee: this.props.categoriesList[i].fee_labor,
							rental: this.props.categoriesList[i].rental_labor
						}
					}
					break;
				}
			}
		} else {
			arrValues = {
				hardgood_multiple: this.props.arrangement.hardgood_multiple,
				fresh_flower_multiple: this.props.arrangement.fresh_flower_multiple,
				labor: {
					global_labor_value: this.props.arrangement.global_labor_value,
					flower: this.props.arrangement.flower_labor,
					hardgood: this.props.arrangement.hardgood_labor,
					base_price: this.props.arrangement.base_price_labor,
					fee: this.props.arrangement.fee_labor,
					rental: this.props.arrangement.rental_labor
				}
			}
		}

		return {
			arr_state: arr_state,
			categoryId: null !== this.props.arrangement.invoicing_category_id ? this.props.arrangement.invoicing_category_id : - this.props.categoryId,
			arrangementValues: arrValues,
			savingToRecipes: false
		};
	},
	componentDidMount: function() {
		var _this = this;
		$('.modal-payment-date').datepicker({
			dateFormat: _this.props.dateFormat
		});
	},

	onModalHidden: function(){
		ReactDOM.unmountComponentAtNode( ReactDOM.findDOMNode(this).parentElement );
	},

	onDelete: function( e ){
		e.preventDefault();

		this.props.onDelete( this.props.index );
		this.props.closeModal();
	},

	onSave: function( e ){
		e.preventDefault();

		var data = {
			include: 'draft' == ReactDOM.findDOMNode(this.refs.arr_state).value ? 0 : 1,
			addon: 'addon' == ReactDOM.findDOMNode(this.refs.arr_state).value ? 1 : 0,
			is_percentage: 1 == ReactDOM.findDOMNode(this.refs.is_percentage).value ? 1 : 0,
			tax: $(ReactDOM.findDOMNode(this.refs.tax)).find('input').is(':checked') ? 1 : 0,
			invoicing_category_id: this.refs.InvoicingCategories.state.categoryId,
			hardgood_multiple: this.refs.InvoicingCategories.refs.hardgoodMultiple.state.hardgoodMultiple,
			fresh_flower_multiple: this.refs.InvoicingCategories.refs.freshFlowerMultiple.state.freshFlowerMultiple,
			global_labor_value: this.refs.InvoicingCategories.refs.applyLaborTo.state.globalLaborValue,
			hardgood_labor: this.refs.InvoicingCategories.refs.applyLaborTo.state.hardgood,
			flower_labor: this.refs.InvoicingCategories.refs.applyLaborTo.state.flower,
			base_price_labor: this.refs.InvoicingCategories.refs.applyLaborTo.state.base_price,
			fee_labor: this.refs.InvoicingCategories.refs.applyLaborTo.state.fee,
			rental_labor: this.refs.InvoicingCategories.refs.applyLaborTo.state.rental
		};

		this.props.onSave( this.props.arrangement.id, data );
		this.props.closeModal();
	},

	onModalError: function(data) {
		if ( data !== null && typeof data === 'object' ) {
			var has_error = false;

			Object.keys(data).map((function (key) {
				var _refs;
				switch(key) {
					case 'paymentDate':
						_refs = 'date';
						break;
					case 'paymentAmount':
						_refs = 'amount';
						break;
					default:
				}

				if ( '' == data[key] && ( 'paymentDate' == key || 'paymentAmount' == key ) ) {
					$(this.refs[_refs]).closest('.form-group').addClass('has-error');
					has_error = true;
				}
			}).bind(this));

			return has_error;
		} else {
			return false;
		}
	},

	saveArrangementToRecipes: function(e){
		e.preventDefault();

		this.setState({
			savingToRecipes: true
		});

		$.post(stemcounter.ajax_url, {
			action: 'sc/standalone_recipe/import',
			arrangement_id: this.props.arrangement.id
		}, (function(r){
			if ( this.setState ) {
				this.setState({
					savingToRecipes: false
				});
			}
			if ( r.success ) {
				alertify.success( 'This arrangement was saved as a recipe!' );
				if ( r.payload.recipe ) {
					this.props.addUserRecipe( r.payload.recipe );
				}
			} else {
				alertify.error( r.message );
			}
		}).bind(this), 'json');
	},

	render: function() {

		var arrStateOptions = [ <option value="draft" key="draft">Draft</option> ];
		if ( ! this.props.standaloneRecipes ) {
			arrStateOptions.push( <option value="addon" key="addon">Possibilities</option> );
		}
		arrStateOptions.push( <option value="live" key="live">Live</option> );

		var deleteBtn = null;
		var saveIcon = null;

		if ( this.state.savingToRecipes ) {
			saveIcon = <span><i className="fa fa-spinner fa-spin" aria-hidden="true"></i> </span>;
		} else {
			saveIcon = <span><i className="fa fa-floppy-o" aria-hidden="true"></i> </span>;
		}

		var stateClass = ! this.props.standaloneRecipes ? 'modalpane col-sm-4' : 'modalpane col-sm-7';
		var saveAsRecipe = null;
		if ( ! this.props.standaloneRecipes ) {
			saveAsRecipe = (
				<div className="modalpane col-sm-3">
					<label className="control-label">
						<div className="lbl">Save To Recipe</div>
						<button type="button" className="btn save-user-recipe" onClick={this.saveArrangementToRecipes}>{saveIcon}</button>
					</label>
				</div>
			);
		}

		if ( -1 != this.props.arrangement.id ) {
			deleteBtn = (
				<a href="#" onClick={this.onDelete} className="delete-arrangement"><i className="fa fa-trash-o"></i></a>
			);
		}

		return (
			<div className="arrangement-settings-modal form-horizontal style-form">
				<div className="modalpane-fixed-height-labels modalpane-row clearfix">
					<div className={stateClass}>
						<label className="control-label">
							<div className="lbl">State</div>
							<ReactSelect2 ref="arr_state" className="arr-state-select" children={arrStateOptions} selectedValue={this.state.arr_state} />
						</label>
					</div>
					{saveAsRecipe}
					<div className="modalpane col-sm-3">
						<NoTax ref="tax" defaultValue={this.props.arrangement.tax} />
					</div>
					<div className="modalpane col-sm-2 delete-arrangement-col">
						<label>
							<div className="lbl">&nbsp;</div>
							{deleteBtn}
						</label>
					</div>
				</div>
				<div className="show-price modalpane-row">
					<label className="control-label">
						<div className="lbl">Type</div>
						<ReactSelect2 ref="is_percentage" selectedValue={this.props.arrangement.is_percentage}>
							<option value={0}>Currency</option>
							<option value={1}>Percentage</option>
						</ReactSelect2>
						{/*<input type="checkbox" ref="is_percentage" defaultChecked={(0 == parseInt( this.props.arrangement.is_percentage ) ? false : true )} /> Percentage (%)*/}
					</label>
				</div>
				<div className="invoicing-categories clearfix">
					<InvoicingCategories
						layout="arrangement"
						itemTypes={this.props.itemTypes}
						categoriesList={this.props.categoriesList}
						arrangementValues={this.state.arrangementValues}
						categoryId={this.state.categoryId}
						onChange={this.onChange}
						ref="InvoicingCategories"
					/>
				</div>
				<div className="modal-footer">
					<div>
						<button type="button" key="cancel" className="btn btn-default button-cancel" data-dismiss="modal">Cancel</button>
						<button className="btn btn-primary btn-submit" type="button" onClick={this.onSave}>Save</button>
					</div>
				</div>
			</div>
		);
	}
});

var AddRecipeModalContent = React.createClass({
	getInitialState: function(){
		return {
			recipe_id: this.props.recipes[0].id
		};
	},

	updateSelectedRecipe: function(e){
		this.setState({
			recipe_id: e.target.value
		});
	},

	render: function() {
		var note = null;

		if ( this.state.recipe_id ) {
			for (var i = 0; i < this.props.recipes.length; i++) {
				if ( this.props.recipes[i].id == this.state.recipe_id && '' != $.trim( this.props.recipes[i].note ) ) {
					note = (
						<div className="modalpane-row clearfix pane-keep">
							<label className="col-xs-12 control-label modalpane">
								<div className="lbl">Recipe Note</div>
								<div className="arrangement-details">{this.props.recipes[i].note.split("\n").map(function(line, i){
									return (
										<span key={'split-' + i}>
											{line}
											<br />
										</span>
									);
								})}</div>
							</label>
						</div>
					);

					break;
				}
			}
		}

		return (
			<div>
				<div className="modalpane-row clearfix pane-keep">
					<label className="col-xs-12 control-label modalpane">
						<div className="lbl">Recipe<span className="mandatory" /></div>
						<ReactSelect2 placeholder="Select Recipe" className="form-control recipe-sel" onChange={this.updateSelectedRecipe}>
							{this.props.recipes.map(function(recipe){
								return <option key={'recipe-' + recipe.id} value={recipe.id}>{( ! recipe.name || 0 === recipe.name.length ) ? '(no name)' : recipe.name }</option>;
							})}
						</ReactSelect2>
					</label>
				</div>
				{note}
			</div>
		);
	}
});

jQuery(document).on('stemcounter.action.renderInvoicingCategories', function(e, settings){
	ReactDOM.render(
		React.createElement(InvoicingCategories, { layout: settings.layout, itemTypes: settings.itemTypes, categoryId: settings.categoryId, categoriesList: settings.categoriesList, defaultCategoryId: settings.defaultCategoryId }),
		$(settings.node).get(0)
	);
});

jQuery(document).on('stemcounter.action.renderInvoicingCategoriesProfile', function(e, settings){
	ReactDOM.render(
		React.createElement(InvoicingCategoriesProfile, { layout: settings.layout, itemTypes: settings.itemTypes, categoryId: settings.categoryId, categoriesList: settings.categoriesList, defaultCategoryId: settings.defaultCategoryId }),
		$(settings.node).get(0)
	);
});


var ShareByEmailForm = React.createClass({
	getInitialState: function(){
		return {
		};
	},

	sendEmail: function ( $modal ) {
		var email = $modal.find('.recipient-email').val();
		var subject = $modal.find('.email-subject').val();
		var body = $modal.find('.email-body').val();

		if ( -1 == email.indexOf( '@' ) ) {
			alertify.error( 'You must supply valid email address' );
			return false;
		}

		var postdata = {
			'action': 'sc_share_by_email',
			'email':   email,
			'subject': subject,
			'body':    body
		};

		$.ajax({
			'type': 'POST',
			'url': window.stemcounter.ajax_url,
			'data': postdata,
			success: function(r){
				if (r.success) {
					alertify.success( 'Your email was sent' );
				} else {
					alertify.error( 'There was error sending email' );
				}
			}
		});
		return true;
	},

	render: function() {
		var body = "Here's the software I was telling you about. Stemcounter has saved us a headache on our recipes, stem counts, wholesaler lists, and accurate proposals!" + ( this.props.coupon ? '\nYou can use coupon code ' + this.props.coupon + ' to get your second month for free!' : '' ) + "\n\n" + this.props.user_display_name;

		return (
			<div>
				<div className="modalpane-row clearfix">
					<label className="col-xs-12 control-label modalpane">
						<div className="lbl">Email address</div>
						<input placeholder="Enter recipient's email here" className="form-control recipient-email" id="share-email-address" type="email" name="email" />
					</label>
				</div>
				<div className="modalpane-row clearfix">
					<label className="col-xs-12 control-label modalpane">
						<div className="lbl">Subject</div>
						<input placeholder="Enter subject here" defaultValue="Wedding Florist Software" className="form-control email-subject" id="share-email-subject" type="text" name="subject" />
					</label>
				</div>
				<div className="modalpane-row clearfix">
					<label className="col-xs-12 modalpane">
						<div className="lbl">Message</div>
						<textarea className="form-control email-body" placeholder="Your message" name="message" id="share-email-message" defaultValue={body}></textarea>
						<a target="_blank" href="http://info.stemcounter.com/wedding-florist-software-walk-through-0.2" className="form-control">Here is a video of how the software works.</a>
					</label>
				</div>
			</div>
		);
	}
});

window.ColorPickerContainer = React.createClass({
	drag: false,
	getInitialState: function() {
		var predefinedColors = [];

		if ( this.props.showPredefinedColors ) {
			predefinedColors = [
				'#b60205',
				'#d93f0b',
				'#fbca04',
				'#0e8a16',
				'#006b75',
				'#1d76db',
				'#0052cc',
				'#5319e7',
				'#e99695',
				'#f9d0c4',
				'#fef2c0',
				'#c2e0c6',
				'#bfdadc',
				'#c5def5',
				'#bfd4f2',
				'#d4c5f9'
			];

			if ( this.props.predefinedColors ) {
				if ( this.props.overridePredefinedColors ) {
					predefinedColors = [];
				}

				predefinedColors = predefinedColors.join( this.props.predefinedColors );
			}
		}

		return {
			isPickerVisible: false,
			color: this.props.color,
			predefinedColors: predefinedColors
		};
	},
	innerContainerRef: function(ref){
		this.$innerContainer = $(ref);
	},
	maybeClosePicker: function(e){
		if ( ! $(e.target).closest( this.$innerContainer ).length ) {
			this.setState({
				isPickerVisible: false
			});
		}
	},
	componentWillReceiveProps: function(nextProps){
		if ( nextProps.color != this.state.color ) {
			this.setState({
				color: nextProps.color
			}, this.gradientBlock);
		}
	},
	componentDidUpdate: function( prevProps, prevState ){
		if ( prevState.isPickerVisible != this.state.isPickerVisible ) {
			$(document).off('click', this.maybeClosePicker);
			// If the picker is now visible, listen to click events to maybe hide it
			if ( this.state.isPickerVisible ) {
				$(document).on('click', this.maybeClosePicker);
			}
		}
	},
	togglePicker: function(id) {
		this.setState({
			isPickerVisible: !this.state.isPickerVisible
		});
	},
	blockFill: function() {
		this.ctxB.rect(0, 0, 150, 150);
		this.gradientBlock();
	},
	stripFill: function() {
		this.ctxS.rect(0, 0, 20, 150);
		var grd1 = this.ctxS.createLinearGradient(0, 0, 0, 150);
		grd1.addColorStop(0, 'rgb(255, 0, 0)'); // red
		grd1.addColorStop(0.17, 'rgb(255, 255, 0)'); // yellow
		grd1.addColorStop(0.34, 'rgb(0, 255, 0)'); // green
		grd1.addColorStop(0.51, 'rgb(0, 255, 255)'); // aqua
		grd1.addColorStop(0.68, 'rgb(0, 0, 255)'); // blue
		grd1.addColorStop(0.85, 'rgb(255, 0, 255)'); // magenta
		grd1.addColorStop(1, 'rgb(255, 0, 0)'); // red
		this.ctxS.fillStyle = grd1;
		this.ctxS.fill();
	},
	gradientBlock: function() {
		this.ctxB.fillStyle = this.state.color;
		this.ctxB.fillRect(0, 0, 150, 150);
		var grdWhite = this.ctxB.createLinearGradient(0, 0, 150, 0);
		grdWhite.addColorStop(0, 'rgb(255,255,255)');
		grdWhite.addColorStop(1, 'transparent');
		this.ctxB.fillStyle = grdWhite;
		this.ctxB.fillRect(0, 0, 150, 150);
		var grdBlack = this.ctxB.createLinearGradient(0, 0, 0, 150);
		grdBlack.addColorStop(0, 'transparent');
		grdBlack.addColorStop(1, 'rgb(0,0,0)');
		this.ctxB.fillStyle = grdBlack;
		this.ctxB.fillRect(0, 0, 150, 150);
	},
	selectColor: function(ctx, e, self, update_gradient_block) {
		var x = e.nativeEvent.offsetX;
		var y = e.nativeEvent.offsetY;
		var imageData = ctx.getImageData(x, y, 1, 1).data;
		var hexColor = stemcounter.rgb2hex(imageData[0], imageData[1], imageData[2]);

		self.setState({
			color: hexColor
		}, function(){
			self.props.onChange( hexColor );
			if ( update_gradient_block ) {
				this.gradientBlock();
			}
		});
	},
	clickStrip: function(e) {
		this.selectColor(this.ctxS, e, this, true);
	},
	mouseDownBlock: function(e) {
		this.drag = true;
		this.selectColor(this.ctxB, e, this);
	},
	mouseMoveBlock: function(e) {
		if (this.drag) {
			this.selectColor(this.ctxB, e, this);
		}
	},
	mouseUpBlock: function() {
		this.drag = false;
	},
	setContexts: function(ctxB, ctxS) {
		this.ctxB = ctxB;
		this.ctxS = ctxS;
	},
	hexToRgb: function(hex) {
		var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
		hex = hex.replace(shorthandRegex, function(m, r, g, b) {
			return r + r + g + g + b + b;
		});

		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null;
	},
	onInputChange: function(e) {
		var _this = this,
			color = e.target.value;

		this.setState({
			color: color
		}, function(){
			this.gradientBlock();

			if ( _this.props.onChange ) {
				_this.props.onChange( color );
			}
		});

	},
	setPredefinedColor: function(color) {
		var _this = this;

		this.setState({
			color: color
		}, function(){
			this.gradientBlock();

			if ( _this.props.onChange ) {
				_this.props.onChange( color );
			}
		});
	},
	onInputClick: function(e) {
		if ( ! this.state.isPickerVisible ) {
			this.setState({
				isPickerVisible: true
			});
		}
	},
	render: function() {
		var className = 'color-picker-container value';
		if ( this.state.isPickerVisible ) {
			className += ' picker-visible';
		}

		return (
			<div className={className}>
				<div className="colorpicker-inner-container" ref={this.innerContainerRef}>
					<ColorLabel
						color={this.state.color}
						handleClick={this.togglePicker}
					/>
					<input type='text' className='color-input' value={this.state.color} onChange={this.onInputChange} onClick={this.onInputClick} name={this.props.name} />
					<ColorPicker
						isVisible={this.state.isPickerVisible}
						color={this.state.color}
						setContexts={this.setContexts}
						mouseDownBlock={this.mouseDownBlock}
						mouseMoveBlock={this.mouseMoveBlock}
						mouseUpBlock={this.mouseUpBlock}
						clickStrip={this.clickStrip}
						blockFill={this.blockFill}
						stripFill={this.stripFill}
						showPredefinedColors={this.props.showPredefinedColors}
						predefinedColors={this.state.predefinedColors}
						setPredefinedColor={this.setPredefinedColor}
					/>
				</div>
			</div>
		);
	}
});

window.ColorLabel = React.createClass({
	handleClick: function() {
		this.props.handleClick();
	},
	render: function() {
		var styles = {
			backgroundColor: this.props.color
		};

		return (
			<button type='button' className='color-label' style={styles} onClick={this.props.handleClick}  />
		);
	}
});

window.ColorPicker = React.createClass({
	contexts: [],

	componentDidMount: function() {
		var self = this;
		var canvasB = this.refs.canvasBlock;
		var canvasS = this.refs.canvasStrip;
		// console.log( this.refs );
		// var canvasB = document.getElementById("color-block");
		// var canvasS = document.getElementById("color-strip");
		var ctxB = canvasB.getContext('2d');
		var ctxS = canvasS.getContext('2d');
		this.contexts.push(ctxB, ctxS);
		self.props.setContexts(ctxB, ctxS);
		this.contexts.forEach(function(item) {
			self.props.blockFill(item);
			self.props.stripFill(item);
		});
	},

	selectPredefinedColor: function( e ) {
		this.props.setPredefinedColor( e.target.dataset.color )
	},

	render: function(e) {
		var predefinedColors = null;

		if ( this.props.predefinedColors ) {
			predefinedColors = (
				<div className="predefined-colors">
					{this.props.predefinedColors.map((function(color){
						var className = 'color-block' + ( this.props.color == color ? ' selected' : '' );
						return (
							<span onClick={this.selectPredefinedColor} data-color={color} key={'color-' + color} className="color-block" style={{backgroundColor: color + '!important'}}></span>
						);
					}).bind(this))}
				</div>
			);
		}

		return(
			<div className="color-picker">
				<canvas
					className="color-block"
					height="150"
					width="150"
					onMouseDown={this.props.mouseDownBlock}
					onMouseMove={this.props.mouseMoveBlock}
					onMouseUp={this.props.mouseUpBlock}
					ref="canvasBlock"
				></canvas>
				<canvas
					className="color-strip"
					height="150"
					width="20"
					onClick={this.props.clickStrip}
					ref="canvasStrip"
				></canvas>
				{predefinedColors}
			</div>
		);
	}
});

window.DataTable = React.createClass({
	getInitialState: function() {
		var state = {};

		state.filteredDataList = this.props.rows ? this.props.rows : [];
		if ( this.props.defaultSortBy ) {
			state.sortBy = this.props.defaultSortBy;
		} else {
			state.sortBy = this.props.labels ? Object.keys(this.props.labels)[0] : '';
		}
		if ( this.props.defaultSortDir ) {
			state.sortDir = this.props.defaultSortDir;
		} else {
			state.sortDir = null;
		}

		if ( state.sortBy && state.sortDir ) {
			var _sortBy = undefined !== state.filteredDataList[0][ this.props.defaultSortBy + '_raw' ] ? this.props.defaultSortBy + '_raw' : this.props.defaultSortBy;

			state.filteredDataList.sort( function ( a, b ) {
				var sortVal = 0;

				if (a[_sortBy] > b[_sortBy]) {
					sortVal = 1;
				}
				if (a[_sortBy] < b[_sortBy]) {
					sortVal = -1;
				}

				if (state.sortDir === 'DESC') {
					sortVal = sortVal * -1;
				}

				return sortVal;
			});
		}

		return state;
	},

	componentWillReceiveProps: function(nextProps) {
		var updatedState = $.extend(true, {}, this.state);

		updatedState.filteredDataList = nextProps.rows ? nextProps.rows : [];
		if ( nextProps.defaultSortBy ) {
			updatedState.sortBy = nextProps.defaultSortBy;
		} else {
			updatedState.sortBy = this.props.labels ? Object.keys(this.props.labels)[0] : '';
		}
		if ( nextProps.defaultSortDir ) {
			updatedState.sortDir = nextProps.defaultSortDir;
		} else {
			updatedState.sortDir = null;
		}

		if ( updatedState.sortBy && updatedState.sortDir ) {
			var _sortBy = undefined !== updatedState.filteredDataList[0][ nextProps.defaultSortBy + '_raw' ] ? nextProps.defaultSortBy + '_raw' : nextProps.defaultSortBy;

			updatedState.filteredDataList.sort( function ( a, b ) {
				var sortVal = 0;

				if (a[_sortBy] > b[_sortBy]) {
					sortVal = 1;
				}
				if (a[_sortBy] < b[_sortBy]) {
					sortVal = -1;
				}

				if (updatedState.sortDir === 'DESC') {
					sortVal = sortVal * -1;
				}

				return sortVal;
			});
		}

		this.setState(updatedState);
	},

	onFilterChange: function(event) {
		event.preventDefault();
		var cellDataKey = event.target.dataset.filter,
			sortDir = this.state.sortDir,
			sortBy = cellDataKey,
			_sortBy = undefined !== this.state.filteredDataList[0][ sortBy + '_raw' ] ? sortBy + '_raw' : sortBy;

		if (sortBy === this.state.sortBy) {
			sortDir = this.state.sortDir === 'ASC' ? 'DESC' : 'ASC';
		} else {
			sortDir = 'DESC';
		}
		var rows = this.state.filteredDataList.slice();
		rows.sort((a, b) => {
		var sortVal = 0;
			if (a[_sortBy] > b[_sortBy]) {
				sortVal = 1;
			}
			if (a[_sortBy] < b[_sortBy]) {
				sortVal = -1;
			}

			if (sortDir === 'DESC') {
				sortVal = sortVal * -1;
			}
			return sortVal;
		});

  		this.setState({sortBy, sortDir, filteredDataList : rows});
	},

	render: function() {
		var rows = $.extend(true, {}, this.state.filteredDataList),
			labels = $.extend(true, {}, this.props.labels),
			thead = [],
			tbody = [],
			sortDirArrow = '';

		sortDirArrow = this.state.sortDir === 'DESC' ? ' ↓' : ' ↑';

		Object.keys(labels).map((function (key) {
			thead.push(
				<th key={'table-th-'+labels[key].key}><a href="#" data-filter={labels[key].key} onClick={this.onFilterChange}>{labels[key].label}<span style={{visibility: ( this.state.sortBy === labels[key].key ? 'visible' : 'hidden' )}}>{sortDirArrow}</span></a></th>
			);
			return false;
		}).bind(this));

		Object.keys(rows).map((function (key) {
			var row = rows[key], atts = {};

			if ( row._data_atts ) {
				Object.keys( row._data_atts ).map( function( key ) {
					atts['data-' + key] = row._data_atts[key];
				} );
			}

			tbody.push(
				<tr key={'table-row-' + key} data-index={key}  {...atts}>
					{Object.keys(labels).map((function (key) {
						var deleteBtn = null;
						if ( this.props.deleteRow && labels[Object.keys(labels)[Object.keys(labels).length - 1]].key == labels[key].key ) {
							deleteBtn = (
								<a className="delete-table-row sc-primary-color" href="#" onClick={this.props.deleteRow}> <i className="fa fa-times"></i></a>
							);
						}

						if ( 'url' == labels[key].type ) {
							return <td className={'cell-' + labels[key].key} key={'table-td-' + labels[key].key}><a className="sc-primary-color" href={row[labels[key].key + '_url']} target="_blank">{row[labels[key].key]}{deleteBtn}</a></td>
						} else {
							return <td className={'cell-' + labels[key].key} key={'table-td-' + labels[key].key}>{row[labels[key].key]}{deleteBtn}</td>
						}
					}).bind(this))}
				</tr>
			);

			return false;
		}).bind(this));

		return(
			<table className="react-datatable">
				<thead>
					<tr>
						{thead}
					</tr>
				</thead>
				<tbody>
					{tbody}
				</tbody>
			</table>
		);
	}
});

// A base React component - currently only used to auto-bind
// component methods to the component instance
class SC_Component extends React.Component {
	constructor( props ) {
		super( props );

		stemcounter.bindMethodsToComponent( this );
	}
}

window.SC_Component = SC_Component;

class LoadingOverlay extends SC_Component {
	render() {
		return (
			<div className="sc-loader-wrap">
				<div className="spinner-wrap">
					<i className="fa fa-spinner fa-pulse fa-3x fa-fw sc-primary-color"></i>
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
	}
}
LoadingOverlay.containerClassName = 'sc-loader-container';

window.LoadingOverlay = LoadingOverlay;

class SC_Tooltip extends SC_Component {
	render() {
		return (
			<span className="sc-tooltip-wrap">
				<span className="tooltip-msg">{this.props.message}</span>
				<span className="tooltip-content">{this.props.tooltip}</span>
			</span>
		);
	}
}
SC_Tooltip.propTypes = {
	tooltip: PropTypes.string.isRequired,
	image: PropTypes.oneOfType( [
		PropTypes.string,
		PropTypes.node
	] )
};
SC_Tooltip.defaultProps = {
	message: <i className="fa fa-question-circle" />
};
window.SC_Tooltip = SC_Tooltip;

class ImageUploader extends SC_Component {
	constructor( props ) {
		super( props );

		this.state = {
			progress: false
		}
	}

	componentDidMount() {
		this.uploader = new plupload.Uploader({
			runtimes: 'html5,flash,silverlight,html4',

			browse_button: this.uploadButton,
			// container: document.getElementById('container'),

			url: stemcounter.aurl( {
				action: 'sc_upload_image',
				upload_nonce: this.props.uploadNonce,
				upload_action: this.props.uploadAction,
				image_size: this.props.imageSize
			} ),

			filters: {
				max_file_size: '10mb',
				mime_types: [
					{title: "Image files", extensions: "jpg,jpeg,png"}
				]
			},

			// Flash settings
			flash_swf_url: stemcounter.theme_url + '/js/plupload/Moxie.swf',

			// Silverlight settings
			silverlight_xap_url: stemcounter.theme_url + '/js/plupload/Moxie.xap',

			init: {
				FilesAdded: this.onFilesAdded,
				UploadProgress: this.onUploadProgress,
				Error: this.onUploadError,
				FileUploaded: this.onFileUploaded,
				UploadFile: this.onUploadFile
			}
		});

		this.uploader.init();
	}

	onFilesAdded( up, files ) {
		this.uploader.start();
	}

	onUploadProgress( up, file ) {
		this.setState({
			progress: file.percent
		});
	}

	onUploadError( up, err ) {
		this.setState({
			progress: false
		});
		alert('There was an error with your file. Please try a different or a smaller one.');
	}

	onFileUploaded( up, file, r ) {
		r.response = 'object' == $.type( r.response ) ? r.response : JSON.parse( r.response );

		this.setState({
			progress: false
		});

		if ( r.response.success ) {

			if ( this.props.onImageUploaded ) {
				this.props.onImageUploaded( r.response.payload.image_id, r.response.payload.image );
			}
		} else {
			alertify.error( r.response.message );
		}
	}

	onUploadFile( up, file ) {
		this.setState({
			progress: 1
		});
	}

	uploadButtonRef( ref ) {
		this.uploadButton = ref;
	}

	render() {
		var image = null;

		if ( this.props.image && this.props.imageId ) {
			image = <img src={this.props.image} alt="image" />
		} else {
			image = this.props.noImageText;
		}

		var progressStyle = {};
		if ( this.state.progress ) {
			progressStyle.width = this.state.progress + '%';
		}

		return (
			<div className="sc-image-upload clearfix">
				<span className="image-preview">
					<a className="upload-image-link" ref={this.uploadButtonRef}>
						{image}
					</a>
				</span>
				<div className={'upload-progress' + ( this.state.progress ? ' upload-in-progress' : '' )}>
					<div className="upload-progress-bar" style={progressStyle}></div>
				</div>
			</div>
		);
	}
}

ImageUploader.propTypes = {
	noImageText: PropTypes.string,
	imageSize: PropTypes.string,
	image: PropTypes.oneOfType( [
		PropTypes.string,
		PropTypes.bool
	] ).isRequired,
	imageId: PropTypes.oneOfType( [
		PropTypes.string,
		PropTypes.number
	] ).isRequired,
	onImageUploaded: PropTypes.func
};

ImageUploader.defaultProps = {
	noImageText: 'Upload Image',
	image: false,
	imageId: 0,
	imageSize: 'medium'
};

window.ImageUploader = ImageUploader;

class FloatInput extends SC_Component {
	constructor( props ) {
		super( props );

		this.state = {
			value: undefined !== props.value ? props.value : props.defaultValue
		};
		if ( undefined === this.state.value ) {
			this.state.value = '';
		}
	}

	onChange( e ) {
		e.persist();
		var defaultValue = this.props.placeholder ? this.props.placeholder : 0;
		var val = e.target.value;
		var filteredVal = '';
		if ( val != '' ) {
			filteredVal = parseFloat( val.replace( /[^0-9\.]*/gi, '' ) );
			filteredVal = isNaN( filteredVal ) ? defaultValue : filteredVal;
		}

		if ( val != filteredVal.toString() ) {
			val = filteredVal;
		}

		this.setState({
			value: val
		}, function(){
			if ( this.props.onChange ) {
				this.props.onChange( e );
			}
		}.bind(this));
	}

	componentWillReceiveProps( nextProps ) {
		if ( nextProps.value != this.state.value ) {
			this.setState({
				value: nextProps.value
			});
		}
	}

	render() {
		var props = $.extend( true, {}, this.props );
		props.onChange = this.onChange;
		props.value = this.state.value;
		delete props.defaultValue;

		return (
			<input
				{...props}
			/>
		);
	}
}

window.FloatInput = FloatInput;

class SC_DatePicker extends SC_Component {
	constructor(props) {
		super( props );
		this.state = {
			value: this.props.value ? this.props.value : ''
		};
	}
	initPropertyDatePicker(){
		var _this = this;
		var idList = this.props.idList ? this.props.idList : '';
		$(this.inputRef).datepicker({
			dateFormat: _this.props.dateFormat,
			onClose: function(val, opts) {
				_this.setState({
					value: val
				}, function(){
					if (typeof _this.props.onDateChange !== undefined) {
						_this.props.onDateChange(val);
					}
				});
			}
		});
	}
	componentDidMount() {
		this.initPropertyDatePicker();
	}
	componentWillReceiveProps( nextProps ) {
		if ( nextProps.value != this.props.value && nextProps.value != this.state.value ) {
			this.setState({
				value: nextProps.value
			});
		}
	}
	componentDidUpdate(prevProps, prevState) {
		this.initPropertyDatePicker();
	}
	setInputRef( ref ) {
		this.inputRef = ref;
	}
	onChange( e ) {
		this.setState({
			value: e.target.value
		});
	}
	onBlur( e ) {
		if ( this.props.onDateChange ) {
			this.props.onDateChange(e.target.value);
		}
	}
	render() {
		var idList = this.props.idList ? this.props.idList : '';
		var classList = this.props.classList ? this.props.classList : '';
		var nameList = this.props.nameList ? this.props.nameList : '';
		var placeholderList = this.props.placeholderList ? this.props.placeholderList : (this.props.dateFormat ? this.props.dateFormat.toUpperCase() : '');
		var readOnly = this.props.readOnly ? this.props.readOnly : false;
		var disabled = this.props.disabled ? this.props.disabled : false;

		return (
			<input
				ref={this.setInputRef}
				type="text"
				id={idList}
				className={'datepicker-field'+ this.props.idList + ' ' + classList}
				name={nameList}
				placeholder={placeholderList}
				readOnly={readOnly}
				disabled={disabled}
				value={this.state.value}
				onChange={this.onChange}
				onBlur={this.onBlur}
			/>
		);
	}
}

window.SC_DatePicker = SC_DatePicker;

stemcounter.formatItems = function( { focusedOption, focusOption, key, labelKey, option, selectValue, style, valueArray } ) {
	var className = [ 'VirtualizedSelectOption' ]

	if ( 'header' === option.type ) {
		className.push( 'item-header' );

		return (
			<div
			className={className.join(' ')}
			key={key}
			style={style}>
			{option[labelKey]}
			</div>
		);
	}

	if ( option === focusedOption ) {
	className.push('VirtualizedSelectFocusedOption');
	}

	if (option.disabled) {
		className.push('VirtualizedSelectDisabledOption');
	}

	if (valueArray && valueArray.indexOf(option) >= 0) {
		className.push('VirtualizedSelectSelectedOption');
	}
	
	if ( option.className ) {
		className.push(option.className);
	}

	var events = option.disabled
		? {}
		: {
		onClick: () => selectValue(option),
		onMouseOver: () => focusOption(option)
		}

	return (
		<div
		className={className.join(' ')}
		key={key}
		style={style}
		title={option.title || option[labelKey]}
		{...events}
		>
		{option[labelKey]}
		</div>
	);
}

$(document).ready(function(){

	$('.share-fb').on('click', function(e) {
		var winHeight = 500,
			winWidth=550,
			url='http://www.stemcounter.com';
		var winTop = (screen.height / 2) - (winHeight / 2);
		var winLeft = (screen.width / 2) - (winWidth / 2);
		window.open('https://www.facebook.com/sharer/sharer.php?u='+ url  + '&display=popup', 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=600,height=400');
	});


	$('.share-email').on('click', function(e) {
		e.preventDefault();

		var modalInstance = null;

		var $modalWrapper = GenericModal.prototype.createWrapper('new-modal share-by-email-modal'),
			modalContent = <ShareByEmailForm user_display_name={stemcounter.user.display_name} coupon={$('.sidebar-sharing').data('coupon')} />;

		ReactDOM.render(
			<GenericModal
				title='Share StemCounter'
				content={modalContent}
				onSave={ShareByEmailForm.prototype.sendEmail}
				saveLabel="Send"
			/>,
			$modalWrapper[0]
		);
	});
})

})(window, jQuery);



var ReplaceFlowerSelect = React.createClass({
	getInitialState: function() {

		if ( undefined == this.props.value || '' == this.props.value ) {
			return {
				value: this.props.value,
				saveDisabled: true
			};
		}
		var value = this.props.value.toString();
		if ( -1 == value.indexOf( '_' ) ) {
			for ( var i = 0; i < stemcounter.page.userItemsAsOptions.length; i++) {
				if ( undefined == stemcounter.page.userItemsAsOptions[i].value ) {
					continue;
				}
				if (0 <= stemcounter.page.userItemsAsOptions[i].value.indexOf( '_' +  this.props.value ) ) {
					value = stemcounter.page.userItemsAsOptions[i].value;
					break;
				}
			}
		}

		return {
			value: value,
			saveDisabled: false
		}
	},

	onChange: function( _item ){
		if ( null == _item ) {
			return;
		}
		var updatedState = $.extend(true, {}, this.state);
		updatedState.value = _item.value;
		this.setState(updatedState);
	},

	onSave: function() {
		$('body').css( 'cursor', 'wait' );
		var _this = this;
		var data = {
			action: 'sc/event_management/replace_flower',
			type_id: _this.props.typeId,
			type: _this.props.type,
			old_iid: _this.props.value,
			new_iid: _this.state.value,
		}

		$.post( window.stemcounter.ajax_url, data, function (response) {
			if (response.success) {
				if ( 'event'  == _this.props.type ) {
					window.location.reload();
				} else if ('order'  == _this.props.type) {
					_this.props.onRefresh( response.payload.items );
				}
			} else {
				alertify.error( response.message );
			}
		}).always(function(){
			$('body').css( 'cursor', '' );
		});
	},

	onCancel: function() {

		if ( undefined != this.props.onCancel ) {
			this.props.onCancel( this.props.itemType, this.props.itemKey );
		}
		ReactDOM.unmountComponentAtNode( ReactDOM.findDOMNode(this).parentElement );
	},

	render: function() {

		var save_btn = ( this.state.saveDisabled ) ?
			<a className="btn btn-warning"  title="Please, create new proposal for this to work">Save</a> :
			<a className="btn btn-primary" onClick={this.onSave}>Save</a>;

		return (
			<div className="replace-flower-wrapper">
				<VirtualizedSelect
					clearable={false}
					searchable={true}
					scrollMenuIntoView={false}
					name="replaceFlower"
					className={"select-style-items"}
					value={this.state.value}
					onChange={this.onChange}
					options={stemcounter.page.userItemsAsOptions}
					optionRenderer={stemcounter.formatItems}
					optionHeight={32}
				/>
				{save_btn}
				<a className="btn btn-primary" onClick={this.onCancel}>Cancel</a>
			</div>
		);
	}
});

$(document).on( 'stemcounter.action.shoppingList', function( e, settings ) {

	$('.replace-flower').click( function() {
		var $select_container = $( this ).closest('.shopping-td-flower').find( '.select-container' );
		var value = $( this ).closest('tr').data( 'variation-id' );
		ReactDOM.render( <ReplaceFlowerSelect value={value} type="event" typeId={settings.eid} /> , $select_container.get ( 0 ) );
	} );

});
