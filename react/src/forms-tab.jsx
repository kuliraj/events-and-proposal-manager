(function($) {
'use strict';

class ICFController extends SC_Component {
	constructor(props) {
		super(props);
		var state = { edit_mode: false };
		state.userForms = this.props.userForms ? this.props.userForms : {}
		state.currentSchema = {};

		this.state = state;
	}
	getProperty(fieldType) {
		if (undefined === fieldType) {
			return;
		}
		var property;

		if ('date' == fieldType) {
			property = {
				default: false,
				description: '',
				format: 'date',
				required: 0,
				title: 'Question ',
				type: 'input',
				property_type: ''
			};
		} else if ('single-line' == fieldType) {
			property = {
				default: false,
				description: '',
				format: 'string',
				required: 0,
				title: 'Question ',
				type: 'input',
				property_type: ''
			};
		} else if ('multi-line' == fieldType) {
			property = {
				default: false,
				description: '',
				format: 'string',
				required: 0,
				title: 'Question ',
				type: 'textarea',
				property_type: ''
			};
		} else if ('checkbox' == fieldType) {
			property = {
				default: false,
				description: '',
				format: 'boolean',
				required: 0,
				title: 'Question ',
				type: 'checkbox',
				property_type: ''
			};
		} else if ('multi-checkbox' == fieldType) {
			property = {
				aopt: ['Option 1', 'Option 2', 'Option 3'],
				default: false,
				description: '',
				format: 'boolean',
				required: 0,
				title: 'Question ',
				type: 'multi-checkbox',
				property_type: ''
			};
		} else if ('number' == fieldType) {
			property = {
				default: false,
				description: '',
				format: 'number',
				required: 0,
				title: 'Question ',
				type: 'input',
				property_type: ''
			};
		} else if ('radio' == fieldType) {
			property = {
				aopt: ['Option 1', 'Option 2', 'Option 3'],
				default: false,
				description: '',
				format: 'boolean',
				required: 0,
				title: 'Question ',
				type: 'radio',
				property_type: ''
			};
		}

		property.internal_field = null;

		return property;
	}
	createNewForm(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);
		updatedState.edit_mode = true;
		updatedState.currentSchema = {
			id: -1,
			title: 'Untitled form',
			description: 'Enter some description',
			properties: []
		};
		this.setState(updatedState);
	}
	onPropToForm(property) {
		if (undefined == property) {
			console.log('Can\'t add property to form');
		} else {
			this.form.onPropFromForm(property);
		}
	}
	browseFormsProperties(property) {
		var formsTitiles = [];
		if (undefined !== property) {
			
			for (var i = 0; i < this.state.userForms.length; i++) {
				var form = this.state.userForms[i];
				for (var j = 0; j < form.properties.length; j++) {
					var _property = form.properties[j];
					if (parseInt(_property.property_id, 10) == parseInt(property.id, 10)) {
						formsTitiles.push({
							form: form.title,
							property: _property.title
						});
					}
				}
			}
		}
		return formsTitiles;
	}
	formDeletedProperty(property){
		this.childProp.formDeletedProperty(property);
	}
	formReset(){
		this.childProp.formReset();
	}
	formSaved(schema) {
		var updatedState = $.extend(true, {}, this.state);
		var qwe = false;
		for (var i = 0; i < updatedState.userForms.length; i++) {
			if (parseInt(updatedState.userForms[i].id, 10) == parseInt(schema.id, 10)) {
				updatedState.userForms[i] = schema;
				qwe = true;
				break;
			}
		}

		if ( false === qwe ) {
			updatedState.userForms.push(schema);
		}
		updatedState.edit_mode = false;
		this.setState(updatedState, this.childProp.formReset());
	}
	onFormEdit(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state),
			id = 'A' == e.target.tagName ? parseInt($(e.target).closest('li').data('id'),10) : parseInt(e.target.dataset.id, 10);

		for (var i = 0; i < Object.keys(this.state.userForms).length; i++) {
			if (id == parseInt(this.state.userForms[i].id, 10) ) {
				updatedState.currentSchema = this.state.userForms[i];
				updatedState.edit_mode = true;
				Object.keys(updatedState.currentSchema.properties).map((function (key) {
					updatedState.currentSchema.properties[key].editing = false;
					return false;
				}));
				break;
			}
		}

		this.setState(updatedState, this.childProp.formLoaded(updatedState.currentSchema.properties));
	}
	onBackClick(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);
		updatedState.edit_mode = false;
		this.childProp.formReset();
		this.setState(updatedState);
	}
	formDeleted(id) {
		var updatedState = $.extend(true, {}, this.state);
		if (undefined !== id) {
			for (var i = 0; i < updatedState.userForms.length; i++) {
				if (parseInt(id, 10) == parseInt(updatedState.userForms[i].id, 10)) {
					updatedState.userForms.splice(i, 1);
				}
			}
			updatedState.edit_mode = false;
			this.setState(updatedState, this.childProp.formReset());
		}
	}
	propToggle(e) {
		e.preventDefault();
		if ($(e.target).closest('.properties-wrapper').hasClass('on')) {
			$(e.target).closest('.properties-wrapper').removeClass('on');
		} else {
			$(e.target).closest('.properties-wrapper').addClass('on');
		}
	}

	onFormURLClick(e) {
		e.stopPropagation();
	}

	render() {
		var formPanel = null;

		if ( this.state.edit_mode ) {
			formPanel = (
				<FormBuilder
					onRef={ref => (this.form = ref)}
					dateFormat={this.props.dateFormat}
					getProperty={this.getProperty}
					formDeletedProperty={this.formDeletedProperty}
					formReset={this.formReset}
					onBackClick={this.onBackClick}
					formSaved={this.formSaved}
					schema={this.state.currentSchema}
					formDeleted={this.formDeleted}
				/>
			);
		} else {
			formPanel = (
				<div className="studio-forms">
					<a className="add-form-button cta-link" onClick={this.createNewForm}>
						+ Add New Form
					</a>
					<ul id="user-forms">
						{this.state.userForms.map((function (form, key) {
							return (
								<li key={'form-' + key} className="user-form" data-id={form.id} onClick={this.onFormEdit} title={form.title}>
									<a href="#" className="sc-primary-color">{form.title}</a>
									<a href={form.url} className="sc-primary-color" target="_blank" onClick={this.onFormURLClick}><i className="fa-fw fa fa-link"></i></a>
								</li>
							);
						}).bind(this))}
					</ul>
				</div>
			);
		}

		return(
			<div className="ifc-container">
				<div className="row">
					<div className="col-xs-8">
						{formPanel}
					</div>
					<div id="sliding-panel" className="properties-wrapper col-xs-4 on" >
						{/* <div className="panel-toggler" onClick={this.propToggle}> */}
						<div className="panel-toggler">
							<div className="vertical-text">Properties</div>
							<div className="arrow"></div>
						</div>
						<PropBuilder
							onRef={ref => (this.childProp = ref)}
							dateFormat={this.props.dateFormat}
							edit_mode={this.state.edit_mode}
							getProperty={this.getProperty}
							onPropToForm={this.onPropToForm}
							properties={this.props.userProperties}
							browseFormsProperties={this.browseFormsProperties}
						/>
					</div>
				</div>
			</div>
		);
	}
}

class FormBuilder extends SC_Component {
	constructor(props) {
		super(props);
		var state = {};
		if (this.props.schema && Object.keys(this.props.schema).length !== 0) {
			state.schema = this.props.schema;
		} else {
			state.schema = {
				id: -1,
				title: 'Untitled form',
				description: 'Enter some description',
				properties: []
			};
		}

		this.state = state;
	}
	componentWillReceiveProps(nextProps){
		var updatedState = $.extend(true, {}, this.state);
		if (nextProps.schema) {
			updatedState.schema = nextProps.schema;
			this.setState(updatedState);
		}
	}
	componentDidMount() {
		this.props.onRef(this);
	}
	componentWillUnmount() {
		this.props.onRef(undefined)
	}
	onResetForm(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);
		updatedState.schema = {
			'id': updatedState.schema.id,
			title: updatedState.schema.title,
			description: updatedState.schema.description,
			properties: []
		};
		this.setState(updatedState,this.props.formReset());
	}
	onDeleteForm(e) {
		var _this = this,
			url = window.stemcounter.aurl({ action: 'sc_form_delete' }),
			data = {
				id: parseInt(this.state.schema.id, 10)
			};

		$.post(url, data, function (response) {
			stemcounter.JSONResponse(response, function (r) {
				if ( ! r.success ) {
					alertify.error( r.message );
				} else {
					_this.props.formDeleted(parseInt(_this.state.schema.id, 10));
					alertify.success( r.payload.message );
				}
			});
		});
	}
	onSaveForm(e) {
		e.preventDefault();
		if ('' == this.state.schema.title) {
			alertify.error('Please enter a title for your form');
			return;
		}
		if ( this.state.schema.properties.length === 0 ) {
			alertify.error('Your form doesn\'t have property fields');
			return;
		}
		var _this = this,
			url = window.stemcounter.aurl({ action: 'sc/form/edit' }),
			data = {
				schema: this.state.schema
			};

		$.post(url, data, function (response) {
			stemcounter.JSONResponse(response, function (r) {
				if ( ! r.success ) {
					alertify.error( r.message );
				} else {
					if ( r.payload.schema ) {
						var updatedState = $.extend(true, {}, _this.state);
						updatedState.schema.id = r.payload.schema.id;
						updatedState.schema.url = r.payload.schema.url;
						Object.keys(updatedState.schema.properties).map((function (key) {
							if (-1 == parseInt(updatedState.schema.properties[key].id, 10)) {
								updatedState.schema.properties[key].id = parseInt(r.payload.schema.properties[key].id, 10);
							}
							return false;
						}));

						_this.props.formSaved( updatedState.schema );
						alertify.success( r.payload.message );
					}
				}
			});
		});
	}
	onPropFromForm(property) {
		var updatedState = $.extend(true, {}, this.state);
		var index = updatedState.schema.properties ? Object.keys(updatedState.schema.properties).length : 0;
		index = index + 1;
		property.property_id = property.id;
		property.id = -1;
		property.required = 0;
		property.editing = false;
		property.order = updatedState.schema.properties.length;
		updatedState.schema.properties.push( property );
		this.setState(updatedState);
	}
	onFieldAdd(e){
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);
		var fieldType = e.target.dataset.type;
		var index = updatedState.schema.properties ? updatedState.schema.properties.length : 0;
		index = index + 1;
		var property = this.props.getProperty(fieldType);
		property.title = property.title + index;
		updatedState.schema.properties.push( property );
		this.setState(updatedState);
	}
	submitProperty(key, property) {
		var updatedState = $.extend(true, {}, this.state);
		if (undefined !== key && undefined !== property) {
			updatedState.schema.properties[key] = property;
		}
		this.setState(updatedState);
	}
	deleteProperty(key) {
		var updatedState = $.extend(true, {}, this.state);
		if (undefined !== key) {
			this.props.formDeletedProperty(updatedState.schema.properties[key]);
			updatedState.schema.properties.splice( key, 1 );
		}

		this.setState(updatedState);
	}
	// changeProperty(key, type, property) {
	// 	var updatedState = $.extend(true, {}, this.state);
	// 	if (undefined !== key && undefined !== type && undefined !== property) {
	// 		updatedState.schema.properties[key] = this.props.getProperty(type);
	// 		updatedState.schema.properties[key].title = property.title;
	// 		updatedState.schema.properties[key].description = property.description;
	// 	}
	// 	this.setState(updatedState);
	// }
	onChangeField(type, value) {
		var updatedState = $.extend(true, {}, this.state);
		if ('title' == type) {
			updatedState.schema.title = value;
		} else if ('description' == type) {
			updatedState.schema.description = value;
		} else if ('confirmation_msg' == type) {
			updatedState.schema.confirmation_msg = value;
		}

		this.setState(updatedState);
	}

	moveField( index, direction ) {
		var updatedState = $.extend( true, {}, this.state );

		if ( undefined === updatedState.schema.properties[ index ] ) {
			return;
		}

		var filteredIndex = -1,
			order = 0;

		if ( 'up' == direction ) {
			if ( index == 0 ) {
				return;
			}

			for (var i = 0; i < updatedState.schema.properties.length; i++) {
				filteredIndex ++;

				// If this is the previous arrangement, move it down
				if ( ( index - 1 ) == filteredIndex ) {
					updatedState.schema.properties[i].order = order + 1;
				} else if ( filteredIndex == index ) { // If it's the current arrangement, move it up
					updatedState.schema.properties[i].order = order - 1;
				} else { // Otherwise just set the order
					updatedState.schema.properties[i].order = order;
				}

				order ++;

			}
		} else {
			if ( index == ( updatedState.schema.properties.length - 1 ) ) {
				return;
			}

			for (var i = 0; i < updatedState.schema.properties.length; i++) {
				filteredIndex ++;

				// If this is the next arrangement, move it up
				if ( ( index + 1 ) == filteredIndex ) {
					updatedState.schema.properties[i].order = order - 1;
				} else if ( filteredIndex == index ) { // If it's the current arrangement, move it down
					updatedState.schema.properties[i].order = order + 1;
				} else { // Otherwise just set the order
					updatedState.schema.properties[i].order = order;
				}

				order ++;
			}
		}

		updatedState.schema.properties.sort(function(a, b){
			if ( a.order == b.order ) {
				return 0;
			}

			return a.order < b.order ? -1 : 1;
		});

		this.setState( updatedState );
	}

	render(){
		var _this = this;
		return(
			<div className="builder-form col-sm-12 center">
				<SchemaFields
					schema={this.state.schema}
					onChangeField={this.onChangeField}
					dateFormat={this.props.dateFormat}
					deleteProperty={this.deleteProperty}
					submitProperty={this.submitProperty}
					changeProperty={this.changeProperty}
					onBackClick={this.props.onBackClick}
					viewing={false}
					moveField={this.moveField}
				/>
				<div className="row form-actions">
					<button type="button" className="btn btn-primary btn-delete-form pull-left" onClick={function(e){
						var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0];
						e.preventDefault();
						stemcounter.confirmModal({
							confirmCallback(){
								_this.onDeleteForm();
							}
						});
					}}><i className="fa-fw fa fa-trash-o" aria-hidden="true"></i>Delete Form</button>
					<button type="button" onClick={this.onResetForm} className="btn btn-primary btn-reset-form pull-left"><i className="fa-fw fa fa-times" aria-hidden="true"></i>Reset Form</button>
					<button type="button" onClick={this.onSaveForm} className="btn btn-primary btn-save-form pull-right"><i className="fa-fw fa fa-pencil-square-o" aria-hidden="true"></i>Save your form</button>
				</div>
			</div>
		);
	}
}

class SchemaFields extends SC_Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	onConfirmationChanged( content ) {
		this.props.onChangeField( 'confirmation_msg', content );
	}

	renderHeader() {
		if ( this.props.viewing ) {
			return <h2>{this.props.schema.title}</h2>;
		} else {
			return (
				<div>
					<SInpit type={'title'} value={this.props.schema.title} onChangeField={this.props.onChangeField}/>
					<a href="#" onClick={this.props.onBackClick} className="sc-primary-color back-btn">Back<i className="fa-fw fa fa-arrow-right" aria-hidden="true"></i></a>
					{/* <SInpit classList="description" type={'description'} value={this.props.schema.description} onChangeField={this.props.onChangeField}/> */}
					<div className="form-thank-you form-group row">
						<label>Confirmation Message(shown after form is submitted) <span className="mandatory"></span></label>
						<ReactQuill
							theme="snow"
							value={this.props.schema.confirmation_msg}
							onChange={this.onConfirmationChanged}
							modules={{
								toolbar: [
									[{ 'header': [1, 2, 3, false] }],
									['bold', 'italic', 'underline', 'link', { 'align': [] }],
									[{'list': 'ordered'}, {'list': 'bullet'}, 'clean']
								]
							}}
						/>
					</div>
				</div>
			);
		}
	}

	render() {
		var total_fields = this.props.schema.properties.length - 1;

		return (
			<div className="form-group">
				{this.renderHeader()}
				{this.props.schema.properties.map((function (property, key) {
					return (
						<ObjectField
							index={key}
							key={'field-object-' + key + '-' + property.id + '-' + property.title}
							layout='form'
							dateFormat={this.props.dateFormat}
							property={property}
							onChangeField={this.props.onChangeField}
							deleteProperty={this.props.deleteProperty}
							submitProperty={this.props.submitProperty}
							changeProperty={this.props.changeProperty}
							isFirst={0 == key}
							isLast={total_fields == key}
							viewing={this.props.viewing}
							move={this.props.moveField}
						/>
					);
				}).bind(this))}
			</div>
		);
	}
}

class ObjectField extends SC_Component {
	constructor(props) {
		super(props);
		this.state = {
			editing: this.props.property.hasOwnProperty('editing') ? this.props.property.editing : true,
			property: this.props.property ? this.props.property : {},
			saving: false
		}

		this.throttledSubmitProperty = _.throttle( this.throttledSubmitProperty, 300 );
	}
	componentWillReceiveProps(nextProps) {
		if ( nextProps.property && nextProps.property.type != this.state.property.type && this.state.editing ) {
			nextProps.property.property_type = this.state.property_type;
			this.setState({
				editing: nextProps.property.hasOwnProperty('editing') ? nextProps.property.editing : true,
				property: nextProps.property ? nextProps.property : {}
			});
		}
	}
	componentDidMount() {}
	throttledSubmitProperty() {
		this.props.submitProperty( this.props.index, this.state.property );
	}
	componentDidUpdate(prevProps, prevState) {
		// Auto-save any changes to properties - otherwise if you didn't get out of property
		// edit UI and directly click on Save your form, your changes will be lost :(
		if ( 'modal' != this.props.layout && prevState.editing && this.state.editing ) {
			var needs_submit = false,
				_this = this,
				keys = Object.keys( this.state.property ),
				total_keys = keys.length,
				key,
				i;

			for ( i = 0; i < total_keys; i++) {
				key = keys[ i ];
				if ( this.state.property[ key ] != prevProps.property[ key ] ) {
					needs_submit = true;
					break;
				}
			}

			if ( needs_submit ) {
				this.throttledSubmitProperty();
			}
		}
	}
	onEdit(e) {
		e.preventDefault();
		this.setState({
			editing: true
		});
	}
	onClose(e) {
		e.preventDefault();

		// field validarion here!
		if ('modal' == this.props.layout) {
			this.setState({
				editing: false
			}, this.initPropertyDatePicker());
		} else {
			this.setState({
				editing: false
			}, this.props.submitProperty(this.props.index, this.state.property), this.initPropertyDatePicker());
		}
	}
	onPropSubmit(e) {
		e.preventDefault();
		// field validarion here!
		if ('modal' == this.props.layout) {
			this.setState({
				saving: true
			});
			this.props.submitProperty(this.props.index, this.state.property);
		} else {
			this.setState({
				editing: false
			}, this.props.submitProperty(this.props.index, this.state.property), this.initPropertyDatePicker());
		}
	}
	onPropChange(e) {
		e.preventDefault();
		var type = e.target.dataset.type;
		if (undefined !== type) {
			this.props.changeProperty(this.props.index, type, this.state.property);
		}
	}
	onTitleChange(e) {
		var updatedState = $.extend(true, {}, this.state);
		updatedState.property.title = e.target.value;

		// Questions don't get placeholders
		if ( ! this.state.property.property_id ) {
			if ( ! updatedState.property.id ) {
				updatedState.property.placeholder_tag = this.props.createPlaceholder( updatedState.property.id, updatedState.property.title );
			}
		}

		this.setState( updatedState );
	}
	onDescriptionChange(e) {
		var updatedState = $.extend(true, {}, this.state);
		updatedState.property.description = e.target.value;
		this.setState(updatedState);
	}
	onRequired(e) {
		var updatedState = $.extend(true, {}, this.state);
		updatedState.property.required = false == e.target.checked ? 0 : 1;
		this.setState(updatedState)
	}
	onOptionChange(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);
		var optIndex = $(e.target).closest('.field-option').data('index');

		if(undefined !== optIndex) {
			updatedState.property.aopt[optIndex] = e.target.value;
		}
		this.setState(updatedState);
	}
	delOption(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);
		var optIndex = $(e.target).closest('.field-option').data('index');

		if(undefined !== optIndex) {
			updatedState.property.aopt.splice(optIndex, 1);
		}
		this.setState(updatedState);
	}
	addOption(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);

		updatedState.property.aopt.push('Option ' + (updatedState.property.aopt.length + 1) );
		this.setState(updatedState);
	}
	handleDate(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);

		updatedState.property.date = e.target.value;
		this.setState(updatedState);
	}
	onTypeChange(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);

		updatedState.property.property_type = e.target.value;
		this.setState(updatedState);
	}
	onDeleteButtonClick(e) {
		var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0],
			_this = this;

		e.preventDefault();
		e.stopPropagation();

		stemcounter.confirmModal({
			confirmCallback(){
				_this.props.deleteProperty(_this.props.index);
			}
		});
	}
	onMoveUp( e ) {
		e.preventDefault();
		e.stopPropagation();

		this.props.move( this.props.index, 'up' );
	}
	onMoveDown( e ) {
		e.preventDefault();
		e.stopPropagation();

		this.props.move( this.props.index, 'down' );
	}
	editedProperty(e) {
		if ((! this.props.version && 'proposal' == this.props.layout) || 'customer' == this.props.layout) {
			this.props.editProperty(e, this.state.property);
		} else {
			return;
		}
	}
	render() {
		var _this = this,
			field = null,
			property = $.extend(true, {}, this.state.property),
			fieldOptions = null,
			fieldDescription = null,
			fieldProperty = null,
			requiredLabel = null,
			required = false,
			panelHeading = null,
			panelFooter = null,
			fieldPropertyType = null,
			fieldRequired = null,
			modalFooter = null,
			placeholderTag = null,
			internalProp = null,
			fieldContainerClass = this.props.viewing ? 'col-sm-12' : 'col-sm-9',
			readOnly = this.props.viewing ? false : true,
			moveUp = null,
			moveDown = null,
			moveButtons = null,
			values = null,
			disabled = false;

		if ( 'form' == this.props.layout || 'proposal' == this.props.layout || 'customer' == this.props.layout ) {
			if ( ! this.props.isFirst ) {
				moveUp = (
					<button className="btn btn-link btn-up" onClick={this.onMoveUp}><i className="fa-fw fa fa-arrow-up" aria-hidden="true"></i></button>
				);
			}

			if ( ! this.props.isLast ) {
				moveDown = (
					<button className="btn btn-link btn-down" onClick={this.onMoveDown}><i className="fa-fw fa fa-arrow-down" aria-hidden="true"></i></button>
				);
			}

			moveButtons = (
				<div className="pull-right move-buttons">
					{moveUp}
					{moveDown}
				</div>
			);
		}

		if (this.state.editing) {

			if ( 'form' == this.props.layout || 'proposal' == this.props.layout || 'customer' == this.props.layout ) {
				panelHeading = (
					<div className="panel-heading clearfix" onClick={this.onClose}>
						<strong className="panel-title">Edit Question</strong>
						<div className="pull-right btn-toolbar">
							<button className="btn btn-link" onClick={this.onDeleteButtonClick}>delete <i className="fa-fw fa fa-trash-o" aria-hidden="true"></i></button>
							<button className="btn btn-link" onClick={this.onClose}>close <i className="fa-fw fa fa-times-circle" aria-hidden="true"></i></button>
							{moveButtons}
						</div>
					</div>
				);

				fieldRequired = (
					<div className="form-group">
						<div className="checkbox">
							<label className="control-label">
								<input type="checkbox" value="on" defaultChecked={parseInt(property.required, 10)} onChange={this.onRequired}/>
								<strong>required</strong>
							</label>
						</div>
					</div>
				);
			} else if ( 'modal' == this.props.layout ) {
				var deleteProp = null;
				if (-1 == this.props.index) {
					panelHeading = (
						<div className="panel-heading clearfix">
							<div className="pull-right btn-toolbar">
								<div className="dropdown pull-right">
									<button type="button" className="btn btn-link" data-toggle="dropdown">change field <i className="fa-fw fa fa-cog" aria-hidden="true"></i></button>
									<ul className="dropdown-menu">
										<li><a href="#" data-type="date" onClick={this.onPropChange}>Date</a></li>
										<li><a href="#" data-type="single-line" onClick={this.onPropChange}>Single Line Text</a></li>
										<li><a href="#" data-type="multi-line" onClick={this.onPropChange}>Multiple Line</a></li>
										<li><a href="#" data-type="checkbox" onClick={this.onPropChange}>Single Checkbox</a></li>
										<li><a href="#" data-type="multi-checkbox" onClick={this.onPropChange}>Multiple Checkboxes</a></li>
										<li><a href="#" data-type="number" onClick={this.onPropChange}>Number</a></li>
										<li><a href="#" data-type="radio" onClick={this.onPropChange}>Radio Select</a></li>
									</ul>
								</div>
							</div>
						</div>
					);
				} else {
					panelHeading = (
						<div className="panel-heading clearfix">
							<div className="pull-right btn-toolbar">
								<p className="pull-right">{property.type}</p>
							</div>
						</div>
					);
				}

				if (null === property.internal_field) {
					var deleteProp = (
						<button className="btn btn-delete" onClick={function(e){
							var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0];
							e.preventDefault();
							stemcounter.confirmModal({
								confirmCallback(){
									_this.props.deleteProperty(_this.props.index);
								}
							});
						}}>Delete</button>
					);
				}

				modalFooter = (
					<div className="modal-footer">
						<div>
							{deleteProp}
							<button className="btn btn-primary btn-submit" type="button" onClick={this.onPropSubmit} disabled={this.state.saving}>Save</button>
						</div>
					</div>
				);

				fieldPropertyType = (
					<div className="form-group">
						<label>Property Type<span className="mandatory"></span></label> <SC_Tooltip tooltip={'A property is a key information piece about a customer or a job. You already have some customer properties such as "First name" or "last name", however, you might want to have a place for more information.' + stemcounter.new_lines( 2 ) + 'There are two types of properties: a customer property or a job property. For example, you can add a JOB property called "Number of bridesmaids" that will let you know how many bridesmaids that a wedding should have. You can choose to add this property on a form for weddings because your corporate event clients wouldn\'t need this question.'} />
						<ReactSelect2
							className=""
							children={[
								<option key="event" value="event">Job</option>,
								<option key="customer" value="customer">Customer</option>
							]}
							disabled={null !== property.internal_field}
							selectedValue={property.property_type}
							onChange={this.onTypeChange}
						/>
					</div>
				);

				placeholderTag = (
					<div className="form-group">
						<label>Placeholder Tag</label>
						<input type="text" className="form-control" value={property.placeholder_tag} onChange={$.noop} readOnly={true}/>
					</div>
				);
			}

			if ( 'modal' == this.props.layout && property.aopt && property.aopt.length ) {
				fieldOptions = (
					<div className="form-group">
						<div className="panel-header">Options</div>
						<div className="row field-options">
							{property.aopt.map((function (option, key, arr) {
								var delOptBtn = <div className="col-xs-3"><button className="form-control" onClick={this.delOption}>delete <i className="fa-fw fa fa-trash-o" aria-hidden="true"></i></button></div>;

								if(2 >= property.aopt.length) {
									delOptBtn = null;
								}

								return (
									<div className="field-option" data-index={key} key={property.type + '-option-' + key}>
										<div className="col-xs-9">
											<input className="form-control" type="text" value={option} onChange={this.onOptionChange}/>
										</div>
										{delOptBtn}
									</div>
								);
							}).bind(this))}
						</div>
						<div className="row">
							<p className="col-xs-offset-10">
								<button className="btn btn-link" onClick={this.addOption}>Add<i className="fa-fw fa fa-plus" aria-hidden="true"></i></button>
							</p>
						</div>
					</div>
				);
			}

			field = (
				<div className="field-object panel cleafix" data-index={this.props.index}>
					{panelHeading}
					<div className="panel-body">
						{fieldPropertyType}
						<div className="form-group">
							<label>Label<span className="mandatory"></span></label>{ 'modal' == this.props.layout ? <SC_Tooltip tooltip={'This is what you call this property internally. You can adjust how this looks on the actual form. For example, the "Date" property for jobs could be for weddings or corporate. However, on your actual form, your weddings form could say, "Wedding date" and your corporate event could say, "Event date."'} /> : null }
							<input type="text" className="form-control" defaultValue={property.title} onBlur={this.onTitleChange}/>
						</div>
						{placeholderTag}
						{fieldDescription}
						{fieldRequired}
						{fieldOptions}
						{panelFooter}
					</div>
					{modalFooter}
				</div>
			);
		} else {

			if (parseInt(property.required, 10)) {
				required = true;
				requiredLabel = <span className="mandatory"></span>;
			}
			if (property.description && 'boolean' != property.format) {
				fieldDescription = <p>{property.description}</p>
			}

			if (this.props.version) {
				disabled=true;
			}

			if ('input' == property.type) {
				var format = 'number' == property.format ? 'number' : 'text';
				if('date' == property.format) {
					fieldProperty = (
						<div className={fieldContainerClass}>
							<label htmlFor={'question_' + property.id}>{property.title}{requiredLabel}</label>
							{fieldDescription}
							<SC_DatePicker
								dateFormat={this.props.dateFormat}
								idList={'question_' + property.id}
								classList="form-control"
								nameList={'questions[' + property.id + ']'}
								readOnly={readOnly}
								value={((property.values && property.values.length) ? property.values[0].value : '')}
								disabled={disabled}
								onDateChange={this.editedProperty}
							/>
						</div>
					);
				} else {
					fieldProperty = (
						<div className={fieldContainerClass}>
							<label htmlFor={'question_' + property.id}>{property.title}{requiredLabel}</label>
							{fieldDescription}
							<input type={format} className="form-control" name={'questions[' + property.id + ']'} required={required} readOnly={readOnly} id={'question_' + property.id} defaultValue={((property.values && property.values.length) ? property.values[0].value : '')} disabled={disabled} onBlur={this.editedProperty} />
						</div>
					);
				}
			} else if ('textarea' == property.type) {
				fieldProperty = (
					<div className={fieldContainerClass}>
						<label htmlFor={'question_' + property.id}>{property.title}{requiredLabel}</label>
						{fieldDescription}
						<textarea className="form-control" rows="2" name={'questions[' + property.id + ']'} required={required} readOnly={readOnly} id={'question_' + property.id} defaultValue={((property.values && property.values.length) ? property.values[0].value : '')} disabled={disabled} onBlur={this.editedProperty}></textarea>
					</div>
				);
			} else if ('checkbox' == property.type) {
				fieldProperty = (
					<div className={fieldContainerClass}>
						<label className="control-label chk-question">
							<input type="checkbox" value="on" name={'questions[' + property.id + ']'} defaultChecked={((property.values && property.values.length && property.values[0].value == 'Yes' ) ? true : false)} required={required} readOnly={readOnly} id={'question_' + property.id} disabled={disabled} onChange={this.editedProperty}/>
							<strong>{property.title}{requiredLabel}</strong>
						</label>
					</div>
				);
			} else if('multi-checkbox' == property.type) {
				fieldProperty = (
					<div className={fieldContainerClass}>
						<label>{property.title}{requiredLabel}</label>
						<div className="checkboxes">
							{property.aopt.map((function (option, key, arr) {
								var checked = false;

								if (property.values && property.values.length) {
									property.values.map((function (value, key, arr) {
										if (value.value == option) {
											checked = true;
										}
										return false;
									}));
								}

								return (
									<div className="checkbox" key={this.props.index + '_chk_group_' + key}>
										<label className="control-label">
											<input className="" type="checkbox" value={key} name={'questions[' + property.id + '][]'} defaultChecked={checked} required={required} readOnly={readOnly} disabled={disabled} onChange={this.editedProperty}/>
											<strong>{option}</strong>
										</label>
									</div>
								);
						}).bind(this))}
						</div>
					</div>
				);
			} else if ('radio' == property.type) {
				fieldProperty = (
					<div className={fieldContainerClass}>
						<label>{property.title}{requiredLabel}</label>
						<div className="checkboxes">
							{property.aopt.map((function (option, key, arr) {
								var checked = false;

								if (property.values && property.values.length) {
									property.values.map((function (value, key, arr) {
										if (value.value == option) {
											checked = true;
										}
										return false;
									}));
								}
							return (
								<div className="radio" key={this.props.index + '_radio_group_' + key}>
									<label className="control-label">
										<input className="" type="radio" value={key} name={'questions[' + property.id + ']'} defaultChecked={checked} required={required} readOnly={readOnly} disabled={disabled} onChange={this.editedProperty}/>
										<strong>{option}</strong>
									</label>
								</div>
							);
						}).bind(this))}
						</div>
					</div>
				);
			}

			field = (
				<div className="field-object panel-view" onClick={this.props.viewing ? null : this.onEdit}>
					<div className="form-group row">
						{fieldProperty}
						{this.props.viewing ? null :
							<div className="col-sm-3">
								<div className="pull-right btn-toolbar">
									<button className="btn btn-link" onClick={this.onDeleteButtonClick}>delete <i className="fa-fw fa fa-trash-o" aria-hidden="true"></i></button>
									<button className="btn btn-link" onClick={this.onEdit}>edit <i className="fa-fw fa fa-pencil-square-o" aria-hidden="true"></i></button>
									{moveButtons}
								</div>
							</div>
						}
					</div>
				</div>
			);
		}
		return (
			<div className="field-editor">
				{field}
			</div>
		);
	}
}

window.ObjectField = ObjectField;

class SInpit extends SC_Component {
	constructor(props) {
		super(props);
		this.state = {
			disabled: false,
			editing: false
		}
	}
	componentDidUpdate(prevProps, prevState) {
		if ( ! this.state.disabled && this.state.editing) {
			$('.sinput input').select();
		}
	}
	onFocus(e) {
		e.preventDefault();

		this.setState({
			disabled: false,
			editing: true
		});

	}
	onBlur(e) {
		e.preventDefault();
		if ('' == e.target.value.replace(/\s/g, '')) {
			this.props.onChangeField(this.props.type, 'Empty string');
			this.setState({
				disabled: false,
				editing: false
			});
		} else {
			this.props.onChangeField(this.props.type, e.target.value);
			this.setState({
				disabled: false,
				editing: false
			});
		}
	}
	render() {
		var classList = this.props.classList ? this.props.classList : '';
		var field = <span className="edit-in-place">{this.props.value}</span>;
		if ( ! this.state.disabled && this.state.editing) {
			field = <input type="text" className="edit-in-place" defaultValue={this.props.value} onBlur={this.onBlur}/>;
		} else if (! this.state.disabled) {
			field = <span className="edit-in-place" onClick={this.onFocus}>{this.props.value}</span>;
		}

		return (
			<div className={'sinput ' + classList }>
				{field}
			</div>
		);
	}
}

class PropBuilder extends SC_Component {
	constructor(props) {
		super(props);

		this.state = {
			properties: this.props.properties ? this.props.properties : {},
			filter: '',
			current_tab: 'customer'
		};
	}
	componentDidMount() {
		this.props.onRef(this);
	}
	componentWillUnmount() {
		this.props.onRef(undefined)
	}
	getModal( label ) {
		/* don't close the modal when clicked outside the popup */
	$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';

	var newId = 'modal-property';

	if ( $('#'+ newId).length ) {
		var $template = $('#'+ newId);

		$template.find('.modal-title:first').text(label);
	} else {
		var $template = $('#ajax-modal-template').clone();
		$template.attr('id', newId);
		switch(label) {
			case "Deliveries": $template.addClass("new-modal"); break;
			case "Discounts": $template.addClass("new-modal"); break;
			case "Tax Rates": $template.addClass("new-modal"); break;
		}
		$template.find('.modal-title:first').text(label);
		$template.attr('aria-labelledby', newId);
		$template.on('hidden.bs.modal', function() {
			// dispose of template as it is no longer used
			$template.remove();
		});

		$template.appendTo($('body'));
		}

		$template.modal('show').on('hidden.bs.modal', this.onModalHidden);

		return $template.find('.modal-body:first')[0];
	}
	closeModal(){
		$('#modal-property').modal('hide');
	}
	setProperty(id, property) {
		var properties = $.extend(true, [], this.state.properties);
		if (-1 == id) {
			properties.push(property);
		} else {
			for (var i = 0; i < properties.length; i++) {
				if (id == properties[i].id) {
					properties[i] = property;
					break;
				}
			}
		}

		properties = $.extend(true, {}, properties);
		this.setState({properties: properties})
	}
	unsetProperty(id) {
		var properties = $.extend(true, [], this.state.properties);

		for (var i = 0; i < properties.length; i++) {
			if (parseInt(id, 10) == parseInt(properties[i].id, 10)) {
				properties.splice(i, 1);
				break;
			}
		}
		this.setState({properties: properties});
	}
	formReset() {
		var properties = $.extend(true, {}, this.state.properties);

		for (var i = 0; i < Object.keys(properties).length; i++) {
			properties[i].default = false;
		}
		this.setState({properties: properties})
	}
	formLoaded(_properties) {
		var properties = $.extend(true, [], this.state.properties);

		for ( var i = 0; i < properties.length; i++ ) {
			if ( _.findWhere( _properties, { property_id: properties[ i ].id } ) ) {
				properties[i].default = true;
			}
		}

		this.setState({properties: properties});
	}
	formDeletedProperty(property) {
		var properties = $.extend(true, {}, this.state.properties);

		for (var i = 0; i < Object.keys(properties).length; i++) {
			if (property.property_id == parseInt(properties[i].id, 10) ) {
				properties[i].default = false;
				break;
			}
		}
		this.setState({properties: properties});
	}
	editProp(e) {
		e.preventDefault();
		e.stopPropagation();
		var id = parseInt($(e.target).closest('li').data('id'),10);
		var property = {};
		if ( -1 == id ) {
			var fieldType = e.target.dataset.type;
			property = this.props.getProperty(fieldType);
			property.property_type = this.state.current_tab;
			property.placeholder_tag = this.createPlaceholderFromTitle( false, property.title );
		} else {
			for (var i = 0; i < Object.keys(this.state.properties).length; i++) {
				if (id == this.state.properties[i].id ) {
					property = this.state.properties[i];
					break;
				}
			}
		}

		var modal = this.getModal('Property');
		this.modalRef = ReactDOM.render(
			<PropertyModal
				id={id}
				property={property}
				setProperty={this.setProperty}
				unsetProperty={this.unsetProperty}
				getProperty={this.props.getProperty}
				closeModal={this.closeModal}
				createPlaceholder={this.createPlaceholderFromTitle}
				browseFormsProperties={this.props.browseFormsProperties}
			/>,
			modal
		);
	}
	onPropSearch(e) {
		e.preventDefault();

		this.setState({
			filter: e.target.value.toLowerCase()
		});
	}
	onPropToForm(e) {
		e.preventDefault();
		var properties = $.extend(true, [], this.state.properties);
		if ( ! this.props.edit_mode) {
			alertify.error('You must open or create a from in order to add properties.');
			return;
		}
		var property,
			id = 'A' == e.target.tagName ? parseInt($(e.target).closest('li').data('id'),10) : parseInt(e.target.dataset.id, 10);

		for (var i = 0; i < Object.keys(properties).length; i++) {
			if (id == parseInt(properties[i].id, 10) ) {
				if (true == properties[i].default) {
					return;
				}
				property = properties[i];
				properties[i].default = true;
				break;
			}
		}
		properties = $.extend(true, {}, properties);
		this.props.onPropToForm(property);
		this.setState({properties: properties})
	}

	createPlaceholderFromTitle( prop_id, title ) {
		var existing_tags = {};

		for (var i = 0; i < this.state.properties.length; i++) {
			var element = this.state.properties[i];
			if ( prop_id && this.state.properties[i].id == prop_id ) {
				continue;
			}

			existing_tags[ this.state.properties[i].placeholder_tag ] = true;
		}

		var placeholder_tag = $.trim(title).toLowerCase().replace( /[^a-zA-Z0-9\s]/gi, '' ).replace( /[\s-_]{1,}/g, '_' );
		var underscore = '';
		var suffix = '';

		while ( undefined !== existing_tags[ placeholder_tag + underscore + suffix ] ) {
			suffix = '' == suffix ? 1 : suffix + 1;
			underscore = '_';
		}

		return placeholder_tag + underscore + suffix;
	}

	renderPropertiesList( type ) {
		return (
			<ul className="property-list">
				{Object.keys(this.state.properties).map((function (key) {
					if ( type != this.state.properties[key].property_type ) {
						return null;
					}

					var classList = true == this.state.properties[key].default ? 'disabled' : '';
					var title = this.state.properties[key].title;
					if ( null !== this.state.properties[key].internal_field ) {
						title += ' *';
					}

					if ( '' != this.state.filter && -1 == title.toLowerCase().indexOf( this.state.filter ) ) {
						return null;
					}

					return (
						<li key={'property-' + key} onClick={this.onPropToForm} data-id={this.state.properties[key].id} title={this.state.properties[key].title} className={classList}>
							<a href="#" className="sc-primary-color">{title}</a>
							<a href="#" className="sc-primary-color prop-edit" onClick={this.editProp}><i className="fa-fw fa fa-cog" aria-hidden="true"></i></a>
						</li>
					);
				}).bind(this))}
			</ul>
		)
	}

	setCurrentTab( e ) {
		e.preventDefault();

		this.setState({
			current_tab: e.target.dataset.type
		});
	}

	render() {
		var addToForm = null;

		return (
			<div className="builder-property col-sm-12 center">
				<input className="form-control" type="text" id="propFilter" onChange={this.onPropSearch} value={this.state.filter} placeholder="Search properties..." />
				<ul className="horizontal-tab-nav nav" role="tablist">
					<li role="presentation" className={ 'customer' == this.state.current_tab ? 'active' : '' }>
						<a href="#customer-properties" aria-controls="customer-properties" role="tab" onClick={this.setCurrentTab} data-type="customer">Customer</a>
					</li>
					<li role="presentation" className={ 'event' == this.state.current_tab ? 'active' : '' }>
						<a href="#job-properties" aria-controls="job-properties" role="tab" onClick={this.setCurrentTab} data-type="event">Job</a>
					</li>
				</ul>
				<div className="tab-content">
					<div id="customer-properties" className={'form-panel tab-pane' + ( 'customer' == this.state.current_tab ? ' active' : '' )} role="tabpanel">
						{ this.renderPropertiesList( 'customer' ) }
					</div>
					<div id="job-properties" className={'form-panel tab-pane' + ( 'event' == this.state.current_tab ? ' active' : '' )} role="tabpanel">
						{ this.renderPropertiesList( 'event' ) }
					</div>
					<p className="help">
						<small>Properties marked with * will be automatically mapped to the corresponding Customer or Job fields</small>
					</p>
				</div>
				<div className="row">
					<div className="dropdown pull-right">
						<button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i className="fa-fw fa fa-plus" aria-hidden="true"></i> Add a property <span className="caret"></span></button>
						<ul className="dropdown-menu">
							<li data-id={-1}><a href="#" data-type="date" onClick={this.editProp}>Date</a></li>
							<li data-id={-1}><a href="#" data-type="single-line" onClick={this.editProp}>Single Line Text</a></li>
							<li data-id={-1}><a href="#" data-type="multi-line" onClick={this.editProp}>Multiple Line</a></li>
							<li data-id={-1}><a href="#" data-type="checkbox" onClick={this.editProp}>Single Checkbox</a></li>
							<li data-id={-1}><a href="#" data-type="multi-checkbox" onClick={this.editProp}>Multiple Checkboxes</a></li>
							<li data-id={-1}><a href="#" data-type="number" onClick={this.editProp}>Number</a></li>
							<li data-id={-1}><a href="#" data-type="radio" onClick={this.editProp}>Radio Select</a></li>
						</ul>
					</div>
				</div>
			</div>
		);
	}
}

class PropertyModal extends SC_Component {
	constructor(props) {
		super(props);
		this.state = {
			property: this.props.property
		}
	}
	componentDidMount(e) {
		stemcounter.unmountComponentOnModalClose(this);
	}
	submitProperty(key, property) {
		if ('' == property.property_type || '' == property.title) {
			alertify.error('Please fill up the required fields');
			return;
		}
		var _this = this;
		var updatedState = $.extend(true, {}, this.state);
		if (undefined !== property) {
			updatedState.property = property;
			var url = window.stemcounter.aurl({ action: 'sc_property_edit' }),
				data = {
					id: parseInt(this.props.id, 10),
					property: property
				};

			$.post(url, data, function (response) {
				stemcounter.JSONResponse(response, function (r) {
					if ( ! r.success ) {
						alertify.error( r.payload.message );
					} else {
						if (r.payload.property_id) {
							updatedState.property.id = parseInt(r.payload.property_id, 10);
							_this.props.setProperty(_this.props.id, updatedState.property);
							_this.props.closeModal();
							alertify.success( r.payload.message );
						}
					}
				});
			});
		}
	}
	changeProperty(key, type, property) {
		var updatedState = $.extend(true, {}, this.state);
		if (undefined !== key && undefined !== type && undefined !== property) {
			updatedState.property = this.props.getProperty(type);
			updatedState.property.title = property.title;
			updatedState.property.description = property.description;
		}
		this.setState(updatedState);
	}
	deleteProperty(id){
		if (undefined !== id) {
			var propertyInForms = this.props.browseFormsProperties(this.state.property);
			if (propertyInForms.length) {
				var message = 'To delete a property, you must remove it from all forms first. <br>';
				propertyInForms.map( function(prop, index, arr) {
					message = message + prop.form + ' -> ' + prop.property + ' <br>';
					return false;
				});
				alertify.alert('Delete Property', message);
				return;
			}
			var _this = this,
				url = window.stemcounter.aurl({ action: 'sc_property_delete' }),
				data = {
					id: parseInt(id, 10)
				};

			$.post(url, data, function (response) {
				stemcounter.JSONResponse(response, function (r) {
					if ( ! r.success ) {
						alertify.error( r.payload.message );
						this.fieldRef.setState({
							saving: false
						});
					} else {
						_this.props.unsetProperty(id);
						_this.props.closeModal();
						alertify.success( r.payload.message );
					}
				});
			});
		}
	}
	setFieldRef( ref ) {
		this.fieldRef = ref;
	}
	render() {
		return (
			<div className="tax-rates-modal form-horizontal style-form">
				<div className="modalpane-row clearfix noborderbottom">
					<ObjectField
						index={this.props.id}
						property={this.state.property}
						layout="modal"
						changeProperty={this.changeProperty}
						submitProperty={this.submitProperty}
						deleteProperty={this.deleteProperty}
						createPlaceholder={this.props.createPlaceholder}
						ref={this.setFieldRef}
					/>
				</div>
			</div>
		);
	}
}

class ICForm extends SC_Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,
			confirmationMessage: false
		};
	}

	onSubmit( e ) {
		e.preventDefault();

		if ( this.state.loading ) {
			return;
		}

		var data = $.param( {
				action: 'sc/form/submission/new',
				form_id: this.props.schema.id
			} ),
			_this = this;

		data += '&' + $(e.target).serialize();

		this.setState({
			loading: true
		});

		$.post(
			stemcounter.ajax_url,
			data,
			function( r ) {
				if ( ! r.success ) {
					alertify.error( r.message );
				} else {
					_this.showConfirmationMessage( r.message );
				}

				_this.setState({
					loading: false
				});
			}
		);
	}

	clearForm() {
		ReactDOM.findDOMNode( this ).reset();
	}

	showConfirmationMessage( message ) {
		this.setState({
			confirmationMessage: message
		});
	}

	render() {
		if ( this.state.confirmationMessage ) {
			return <div className="form-confirmation-message form-success" dangerouslySetInnerHTML={{__html: this.state.confirmationMessage}} />;
		}

		return (
			<form onSubmit={this.onSubmit}>
				{ this.state.loading ? <LoadingOverlay /> : null }
				<SchemaFields
					schema={this.props.schema}
					onChangeField={$.noop}
					dateFormat={this.props.dateFormat}
					viewing={true}
				/>

				<div className="form-group row">
					<div className="col-sm-2 col-sm-push-10 col-xs-12">
						<button type="submit" className="btn btn-primary btn-lg btn-block">Send</button>
					</div>
				</div>
			</form>
		);
	}
}

$(document).on('stemcounter.action.renderICFController', function(e, settings) {
	ReactDOM.render(
		<ICFController
			dateFormat={settings.dateFormat}
			userForms={settings.userForms}
			userProperties={settings.userProperties}
		/>,
		$(settings.node).get(0)
	);
});

$(document).on('stemcounter.action.renderICForm', function(e, settings) {
	ReactDOM.render(
		<ICForm
			schema={settings.schema}
			dateFormat={settings.dateFormat}
		/>,
		$(settings.node).get(0)
	);
});

})(jQuery);
