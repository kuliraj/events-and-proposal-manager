// use a closure so we do not pollute the global scope
(function($){
'use strict';

var DiscountsForm = React.createClass({
	getInitialState: function() {
		var rawDiscounts = [];

		try {
			rawDiscounts = JSON.parse(this.props.value);
		} catch (e) {
			// do nothing
		}

		var discounts = [];
		for (var i = 0; i < rawDiscounts.length; i++) {
			var discount = rawDiscounts[i];
			discount.msg = '';
			discounts.push(this.createNewDiscount(discount.name, discount.value, discount.type, discount.id));
		};
		
		return {
			discounts: discounts
		};
	},

	discAutoIncrement: 0,
	createNewDiscount: function(name, value, type, id) {
		this.discAutoIncrement ++;

		var newDiscount = {
			id: id,
			key: this.discAutoIncrement,
			name: name,
			value: value,
			type: type,
			msg: ''
		}

		return newDiscount;
	},

	addMore: function(e) {
		e.preventDefault();

		var updatedState = {
			discounts: this.state.discounts
		};

		updatedState.discounts.push(this.createNewDiscount('', '0.00', 'amount'));

		this.setState(updatedState);
	},

	removeDiscount: function(discIndex) {
		
		var updatedState = {
			discounts: this.state.discounts
		};

		updatedState.discounts.splice(discIndex, 1);

		this.setState(updatedState, this.submit);
	},

	onNameChange: function(discIndex, e) {
		var updatedState = {
			discounts: this.state.discounts
		};

		var name = e.target.value;
		updatedState.discounts[discIndex].name = name;

		this.setState(updatedState);
	},

	onDiscountChange: function(e) {
		var updatedState = {
			discounts: this.state.discounts
		};

		var discount = e.target.value;

		if ( discount < 1 && 'percentage' == updatedState.discounts[e.target.dataset.index].type ) {
			updatedState.discounts[e.target.dataset.index].msg =  'Mark your amount in % format. ( 0.086 should be written as 8.6 )';
		} else {
			updatedState.discounts[e.target.dataset.index].msg = '';
		}

		discount = stemcounter.parseFloatValue(discount);
		updatedState.discounts[e.target.dataset.index].value = discount;

		this.setState(updatedState);
	},

	onCurrencyClick:function(index, e) {

		var updatedState = {
			discounts: this.state.discounts
		};

		var active_label = $(this.refs.discount_form).find('.discount-row:nth-child(' + index + ')');

		if ( ! e.target.parentElement.classList.contains('active') ) {
			active_label.removeClass('active');
			e.target.parentElement.classList.add('active');
		}

		if ( e.target.value != updatedState.discounts[e.target.dataset.index].type ) {
			updatedState.discounts[e.target.dataset.index].type = e.target.value;
			this.setState(updatedState, this.submit);
		}
	},

	submit: function() {
		var th = this;
		var form = $(this.refs.discount_form);
		var url = form.attr('action');
		var state = {
			discounts: this.state.discounts,
			layout: 'profile'
		};
		$.post(url, state, function (response) {
			stemcounter.JSONResponse(response, (function (r) {
				if (r.success) {
					var updatedState = {
						discounts: this.state.discounts
					};

					var discounts = [];
					for (var i = 0; i < r.payload.discounts.length; i++) {
						var discount = r.payload.discounts[i];
						discounts.push(th.createNewDiscount(discount.name, discount.value, discount.type, discount.id));
					};

					updatedState.discounts = discounts;

					th.setState( updatedState );
				} else {
					console.log( r );
				}
			}).bind(th));
		});
	},

	closeModal: function(){
		$('.modal.fade.in').modal('hide');
	},

	render: function() {
		var _this = this;
		var labels = null;
		var discounts = [];
		var discRow;
		var removeDisc;


		for (var i = 0; i < this.state.discounts.length; i++) {
			var discount = this.state.discounts[i];
			var index = i;

			removeDisc = (
					<a href="#" className="btn-remove" data-index={index} onClick={function(e){
						var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0];
						e.preventDefault();
						console.log( el.dataset.index );
						stemcounter.confirmModal({
							confirmCallback: function(){
								_this.removeDiscount( el.dataset.index );
							}
						});
					}}><i className="fa fa-trash-o"></i></a>
			);
		
			discRow = (
				<div className="row discount-row pane-row" key={discount.key}>
					<div className="pane">
						<input type="text" name={discount.id ? 'discount_name[' + discount.id + ']' : 'discount_name[]'} className="form-control" value={discount.name} onChange={this.onNameChange.bind(null, i)} onBlur={this.submit} placeholder="Discount name"/>
					</div>

					<div className="pane">
						<div ref="discount_row" className="clearfix style-form form-group discount-setting">
							<label className={'control-label ' + (discount.type == 'percentage' ? 'active' : '')}><input type="checkbox" data-index={i} name={discount.id ? 'discount_percentage[' + discount.id + ']' : 'discount_percentage[]'} value="percentage" onChange={this.onCurrencyClick.bind(this, index)} /> <strong>%</strong> </label>
							<label className={'control-label ' + (discount.type == 'amount' ? 'active' : '')}><input type="checkbox" data-index={i} name={discount.id ? 'discount_percentage[' + discount.id + ']' : 'discount_percentage[]'} value="amount" onChange={this.onCurrencyClick.bind(this, index)} /> <strong>$</strong> </label>
						</div>
					</div>
					<div className="pane">
						<div className="clearfix style-form form-group discount-setting">
							<input type="number" defaultValue={discount.value} data-index={i} onChange={this.onDiscountChange} className="form-control" onBlur={this.submit}/>
							<span className="percentage-msg">{discount.msg}</span>
						</div>
					{removeDisc}
					</div>
				</div>
			);
	

			discounts.push(( discRow ));
		};
		if (discounts.length == 0) {
			labels = null;
			discounts = <p>{"You don't have any discounts, yet."}</p>;
		} else {
			labels = (
				<div className="row discount-row pane-row">
					<div className="pane">
					<label>Name</label>
					</div>

					<div className="pane">
						<label>Rate</label>
					</div>
					<div className="pane">
						<label>Amount</label>
					</div>
				</div>
			);
		}

		var url = stemcounter.aurl({
			'action': 'sc_edit_discounts'
		});

		return (
			<form action={url} method="post" onSubmit={this.submit} ref="discount_form">
				<div className="pane-row">
					<a className="add-tax-button cta-link" onClick={this.addMore}>
						+ Add New Discount
					</a>
				</div>
				<div className="row no-gutter">
					{labels}
					{discounts}
				</div>
			</form>
		);
	}
});

$(document).ready(function(){
	var formWrapper = $('.edit-discounts-form-wrapper:first');
	if (formWrapper.length == 1) {
		var value = formWrapper.attr('data-value');
		ReactDOM.render(
			<DiscountsForm value={value} />,
			formWrapper.get(0)
		);
	}
});

})(jQuery);