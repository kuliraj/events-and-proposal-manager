// use a closure so we do not pollute the global scope
(function($){
'use strict';

var Notifications = React.createClass({
	getInitialState: function() {
		return {
			allow_notifications: this.props.allow_notifications,
			notifications: this.props.notifications,
			user_email: this.props.user_email
		}
	},
	onAllowNotifications: function(e) {
		var updatedState = $.extend(true, {}, this.state);

		updatedState.allow_notifications = e.target.checked ? 'yes' : 'no';
		this.setState(updatedState, this.submit);
	},
	onEmailAddr: function(e) {
		e.preventDefault();
		var updatedState = $.extend(true, {}, this.state);

		updatedState.user_email = e.target.value;

		this.setState(updatedState, this.submit);
		return;

	},
	submit: function() {
		var url = window.stemcounter.aurl({
			action: 'sc_handle_notifications_form',
			email: this.state.user_email,
			allow: this.state.allow_notifications
		});

		$.get(url, {}, (function(response){
			stemcounter.JSONResponse(response, (function(r){
				if ( r.success ) {

				} else {

				}
			}).bind(this));
		}).bind(this));
	},
	render: function() {
		var _this = this,
			notifications = [];

		Object.keys(this.state.notifications).map(function (key, index) {
			notifications.push(
				<tr key={'key-notifications-row' + index }>
					<td>{_this.props.dateFormat, _this.state.notifications[key].date}</td>
					<td dangerouslySetInnerHTML={{__html: _this.state.notifications[key].notification}} />
				</tr>
			);
		});

		return (
			<div className="notifications-react-form">
				<div className="row">
					<div className="col-xs-12">
						<div className="col-xs-6">
							<label className="control-label">Email Address
								<input type="text" className="form-control" onBlur={this.onEmailAddr} placeholder="email@example.com" defaultValue={this.state.user_email} />
							</label>
						</div>
						<div className="col-xs-6">
							<label className="control-label" >
								<input type="checkbox" defaultChecked={('yes' == this.state.allow_notifications ? true : false)} onChange={this.onAllowNotifications} /> 
								Email notifications
							</label>
						</div>
					</div>
				</div>
				<div className="row">
				<table id="notifications">
					<thead>
						<tr>
							<th>Date</th>
							<th>Notification</th>
						</tr>
					</thead>
					<tbody>
						{notifications}
					</tbody>
				</table>
				</div>
			</div>
		);
	}
});

jQuery(document).on('stemcounter.action.renderNotifications', function(e, settings){
	ReactDOM.render(
		<Notifications notifications={settings.notifications} user_email={settings.user_email} allow_notifications={settings.allow_notifications} dateFormat={settings.dateFormat}/>,
		$(settings.node).get(0)
	);
});

})(jQuery);