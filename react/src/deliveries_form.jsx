// use a closure so we do not pollute the global scope
(function($){
'use strict';

var DeliveriesForm = React.createClass({
	getInitialState: function() {
		var rawDeliveries = [];

		try {
			rawDeliveries = JSON.parse(this.props.value);
		} catch (e) {
			// do nothing
		}

		var deliveries = [];
		for (var i = 0; i < rawDeliveries.length; i++) {
			var delivery = rawDeliveries[i];
			delivery.msg = '';
			deliveries.push(this.createNewDelivery(delivery.name, delivery.value, delivery.type, delivery.tax, delivery.default, delivery.id));
		};
		
		return {
			deliveries: deliveries
		};
	},

	deliveryAutoIncrement: 0,
	createNewDelivery: function(name, value, type, tax, default_delivery, id) {
		this.deliveryAutoIncrement ++;

		var newDelivery = {
			id: id,
			key: this.deliveryAutoIncrement,
			name: name,
			value: value,
			type: type,
			tax: tax,
			msg: '',
			default_delivery: parseInt(default_delivery)
		}

		return newDelivery;
	},

	addMore: function(e) {
		e.preventDefault();

		var updatedState = {
			deliveries: this.state.deliveries
		};

		updatedState.deliveries.push(this.createNewDelivery('', '0.00', 'amount', 1, 0));

		this.setState(updatedState);
	},

	removeDelivery: function(index) {
		var updatedState = {
			deliveries: this.state.deliveries
		};

		updatedState.deliveries.splice(index, 1);

		this.setState(updatedState, this.submit);
	},

	onNameChange: function(index, e) {
		var updatedState = {
			deliveries: this.state.deliveries
		};

		var name = e.target.value;
		updatedState.deliveries[index].name = name;

		this.setState(updatedState);
	},

	onDeliveryChange: function(e) {
		var updatedState = {
			deliveries: this.state.deliveries
		};

		var delivery = e.target.value;
		
		if ( delivery < 1 && 'percentage' == updatedState.deliveries[e.target.dataset.index].type ) {
			updatedState.deliveries[e.target.dataset.index].msg =  'Mark your amount in % format. ( 0.086 should be written as 8.6 )';
		} else {
			updatedState.deliveries[e.target.dataset.index].msg = '';
		}

		delivery = stemcounter.parseFloatValue(delivery);
		updatedState.deliveries[e.target.dataset.index].value = delivery;

		this.setState(updatedState);
	},

	onCurrencyClick:function(index, e) {

		var updatedState = {
			deliveries: this.state.deliveries
		};

		var active_label = $(this.refs.delivery_form).find('.delivery-row:nth-child(' + index + ')');

		if ( ! e.target.parentElement.classList.contains('active') ) {
			active_label.removeClass('active');
			e.target.parentElement.classList.add('active');
		}

		if ( e.target.value != updatedState.deliveries[e.target.dataset.index].type ) {
			updatedState.deliveries[e.target.dataset.index].type = e.target.value;
			this.setState(updatedState, this.submit);
		}
	},

	onTaxableCheck: function(e) {

		var updatedState = {
			deliveries: this.state.deliveries
		};

		
		updatedState.deliveries[e.target.dataset.index].tax = false == e.target.checked ? 0 : 1;
		this.setState(updatedState, this.submit);
	},

	submit: function() {
		var th = this;
		var form = $(this.refs.delivery_form);
		var url = form.attr('action');
		var state = {
			deliveries: this.state.deliveries
		};
		$.post(url, state, function (response) {
			stemcounter.JSONResponse(response, (function (r) {
				if (r.success) {

					var updatedState = {
						deliveries: this.state.deliveries
					};

					var deliveries = [];
					for (var i = 0; i < r.payload.deliveries.length; i++) {
						var delivery = r.payload.deliveries[i];
						deliveries.push(th.createNewDelivery(delivery.name, delivery.value, delivery.type, delivery.tax, delivery.default, delivery.id));
					};

					updatedState.deliveries = deliveries;

					th.setState( updatedState );
				} else {
					console.log( r );
				}
			}).bind(th));
		});
	},

	onDefaultClick: function(e) {
		var updatedState = {
			deliveries: this.state.deliveries
		};

		if ( e.target.checked ) {
			for (var i = 0; i < updatedState.deliveries.length; i++) {
				if ( i == e.target.dataset.index ) {
					updatedState.deliveries[i].default_delivery = 1;
				} else {
					updatedState.deliveries[i].default_delivery = 0;
				}
			}
		}

		this.setState(updatedState, this.submit);
	},

	render: function() {
		var _this = this;
		var labels = null;
		var deliveries = [];
		var deliveryRow;
		var removeDelivery;


		for (var i = 0; i < this.state.deliveries.length; i++) {
			var delivery = this.state.deliveries[i];
			var index = i;

			removeDelivery = (
				<a href="#" className="btn-remove" data-index={index} onClick={function(e){
					var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0];
					e.preventDefault();
					stemcounter.confirmModal({
						confirmCallback: function(){
							_this.removeDelivery( el.dataset.index );
						}
					});
				}}><i className="fa fa-trash-o"></i></a>
			);
		
			deliveryRow = (
				<div className="row delivery-row pane-row" key={delivery.key}>
					<div className="pane">
						<input type="text" name={delivery.id ? 'delivery_name[' + delivery.id + ']' : 'delivery_name[]'} className="form-control" value={delivery.name} onChange={this.onNameChange.bind(null, i)} onBlur={this.submit} placeholder="Delivery name"/>
					</div>

					<div className="pane">
						<div ref="delivery_row" className="clearfix style-form form-group delivery-setting">
							<label className={'control-label ' + (delivery.type == 'percentage' ? 'active' : '')}><input type="checkbox" data-index={i} name={delivery.id ? 'delivery_percentage[' + delivery.id + ']' : 'delivery_percentage[]'} value="percentage" onChange={this.onCurrencyClick.bind(this, index)} /> <strong>%</strong> </label>
							<label className={'control-label ' + (delivery.type == 'amount' ? 'active' : '')}><input type="checkbox" data-index={i} name={delivery.id ? 'delivery_percentage[' + delivery.id + ']' : 'delivery_percentage[]'} value="amount" onChange={this.onCurrencyClick.bind(this, index)} /> <strong>$</strong> </label>
						</div>
					</div>
					<div className="pane">
						<div className="clearfix style-form form-group delivery-setting">
							<input type="number" defaultValue={delivery.value} data-index={i} onChange={this.onDeliveryChange} onBlur={this.submit}  className="form-control" />
							<span className="percentage-msg">{delivery.msg}</span>
						</div>
					</div>
					<div className="pane taxable">
						<div className="checkbox">
							<input type="checkbox" data-index={i} defaultChecked={(0 == delivery.tax ? false : true)} onClick={this.onTaxableCheck} />
						</div>

					</div>
					<div className="pane default-delivery">
						<div className="">
							<input className="styled-radio" data-index={i} id={'default_delivery_' + delivery.id } defaultChecked={delivery.default_delivery} type="radio" name="default-delivery" onChange={this.onDefaultClick} /><label htmlFor={'default_delivery_' + delivery.id }></label>
						</div>
						{removeDelivery}
					</div>
				</div>
			);
	

			deliveries.push(( deliveryRow ));
		};
		if (deliveries.length == 0) {
			labels = null;
			deliveries = <p>{"You don't have any deliveries, yet."}</p>;
		} else {
			labels = (
				<div className="row delivery-row pane-row">
					<div className="pane">
						<label>Name</label>
					</div>

					<div className="pane">
						<label>Rate</label>
					</div>
					<div className="pane">
						<label>Amount</label>
					</div>
					<div className="pane taxable">
						<label>Tax</label>
					</div>
					<div className="pane">
						<label>Default</label>
					</div>
				</div>
			);
		}

		var url = stemcounter.aurl({
			'action': 'sc_edit_deliveries'
		});

		return (
			<form action={url} method="post" onSubmit={this.submit} ref="delivery_form">
				<div className="pane-row">
					<a className="add-tax-button cta-link" onClick={this.addMore}>
						+ Add New Delivery
					</a>
				</div>
				<div className="row no-gutter">
					{ labels}
					{deliveries}
				</div>
			</form>
		);
	}
});

$(document).ready(function(){
	var formWrapper = $('.edit-deliveries-form-wrapper:first');
	if (formWrapper.length == 1) {
		var value = formWrapper.attr('data-value');
		ReactDOM.render(
			<DeliveriesForm value={value} />,
			formWrapper.get(0)
		);
	}
});

})(jQuery);