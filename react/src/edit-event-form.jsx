// use a closure so we do not pollute the global scope
(function($){
'use strict';

var eventForms = {};

var EditEventForm = React.createClass({displayName: "EditEventForm",
	getInitialState: function() {
		var state = {
			values: {
				method: 'add',
				id: 0,
				name: '',
				clearName: false,
				date: '',
				dateFormat: '',
				tax_rate_id: 0,
				tax_rate_value: 0,
				tax_rate_id2: 0,
				tax_rate_value2: 0,
				event_state_position: '',
				state: 'inquiry',
				tags: [],
				private_note: '',
				note_styles: { overflowY: 'hidden' },
				contract_note: '',
				customerIds: [-1],
				updatedTextArea: false,
				delivery: 0,
				card: 0,
				fresh_flower_multiple: 1,
				hardgood_multiple: 1,
				duplicate: '',
				labor: {
					global_labor_value: 0,
					flower: 0,
					hardgood: 0,
					base_price: 0,
					fee: 0,
					rental: 0
				},
				payments: [],
				eventCost: 0,
				from_submission: ''
			},
			venuesList: [],
			customersList: [],
			taxRates: [],
			previousValues: {},
			formState: 'view'
		};
		if (this.props.eventData.event) {
			var event = this.props.eventData.event;
			state.values.id = event.id;
			state.values.name = event.name;
			state.values.date = event.date;

			if (event.tax_rate_id && this.props.eventData.taxRates) {
				var tax_rate = null;

				if (event.tax_rate_id !== 0) {
					for (var i = 0; i < this.props.eventData.taxRates.length; i++) {
						var rate = this.props.eventData.taxRates[i];
						/*if (rate.id < 0) {
							tax_rate = rate;
							break;
						}*/

						if (rate.id == event.tax_rate_id) {
							tax_rate = rate;
						}
					};
				}
				
				state.values.tax_rate_id = tax_rate === null ? 0 : tax_rate.id;
			} else {
				state.values.tax_rate_id = 0;
			}

			if (event.tax_rate_id2 && this.props.eventData.taxRates) {
				var tax_rate = null;

				if (event.tax_rate_id2 !== 0) {
					for (var i = 0; i < this.props.eventData.taxRates.length; i++) {
						var rate = this.props.eventData.taxRates[i];
						/*if (rate.id < 0) {
							tax_rate = rate;
							break;
						}*/

						if (rate.id == event.tax_rate_id2) {
							tax_rate = rate;
						}
					};
				}
				
				state.values.tax_rate_id2 = tax_rate === null ? 0 : tax_rate.id;
			} else {
				state.values.tax_rate_id2 = 0;
			}
			if ( this.props.eventData.tags ) {
				state.values.tags = this.props.eventData.tags;
			}
			state.values.tax_rate_value = event.tax_rate_value;
			state.values.tax_rate_value2 = event.tax_rate_value2;
			state.values.note = event.private_note;
			state.values.duplicate = event.duplicate ? '1' : '';
			state.values.proposal_note = event.contract_note;
			state.values.delivery = event.delivery ? event.delivery : 0;
			state.values.card = event.card ? event.card : 0;
			state.values.fresh_flower_multiple = event.fresh_flower_multiple ? event.fresh_flower_multiple : 1;
			state.values.hardgood_multiple = event.hardgood_multiple ? event.hardgood_multiple : 1;
			state.values.event_state_position = event.event_state_position ? event.event_state_position : '0';
			state.values.state = event.state ? event.state : 'inquiry';

			state.values.labor = {
				global_labor_value: event.global_labor_value,
				flower: event.flower_labor,
				hardgood: event.hardgood_labor,
				base_price: event.base_price_labor,
				fee: event.fee_labor,
				rental: event.rental_labor
			};
		}
		if (this.props.eventData.customerIds) {
			state.values.customerIds = this.props.eventData.customerIds;
		}
		if ( state.values.customerIds.length == 1 ) {
			state.values.customerIds.push( -1 );
		}

		if (this.props.eventData.customersList) {
			state.customersList = this.props.eventData.customersList;
		}

		if (this.props.eventData.dateFormat) {
			state.values.dateFormat = this.props.eventData.dateFormat;
		}

		if (this.props.eventData.taxRates) {
			state.taxRates = this.props.eventData.taxRates;
		}

		if (state.values.id == 0 && state.values.tax_rate_id == 0 && this.props.eventData.taxRates.length > 0) {
			var default_tr = _.findWhere( this.props.eventData.taxRates, { default: 1 } );
			if ( default_tr ) {
				state.values.tax_rate_id = default_tr.id;
				state.values.tax_rate_value = default_tr.value;
			} else {
				state.values.tax_rate_id = this.props.eventData.taxRates[0].id;
				state.values.tax_rate_value = this.props.eventData.taxRates[0].value;
			}
		}

		if (state.values.id == 0 && state.values.tax_rate_id2 == 0 && this.props.eventData.taxRates.length > 0) {
			state.values.tax_rate_id2 = 0;
			state.values.tax_rate_value2 = 0;
		}

		if (this.props.eventData.categoryId) {
			state.values.categoryId = this.props.eventData.categoryId;
		}

		if (this.props.eventData.eventInvoiceValues) {
			state.eventInvoiceValues = this.props.eventData.eventInvoiceValues;
		}

		if (this.props.eventData.scheduledPayments) {
			state.values.scheduledPayments = this.props.eventData.scheduledPayments;
		}

		if (this.props.eventData.madePayments) {
			state.values.madePayments = this.props.eventData.madePayments;
		}

		if (this.props.eventData.eventCost) {
			state.values.eventCost = this.props.eventData.eventCost;
		}

		/*if (this.props.eventData.tagsList) {
			state.values.tagsOptions = this.props.eventData.tagsList;
		}*/

		if (state.values.id > 0) {
			state.values.method = 'edit';
		}
		if (this.props.eventData.categoriesList) {
			state.categoriesList = this.props.eventData.categoriesList;
		}

		if (state.values.state === 'template' && state.values.duplicate ==  1) {
			state.values.name = '';
			state.values.date = '';
			state.values.dateFormat = state.values.dateFormat;
			state.values.tax_rate_id = 0;
			state.values.tax_rate_value = 0;
			state.values.tax_rate_id2 = 0;
			state.values.tax_rate_value2 = 0;
			state.values.state = 'template';
			state.values.vendor_name = '';
			state.values.customer_first_name = '';
			state.values.customer_last_name = '';
		}

		state.previousValues = state.values;

		return state;
	},

	submit: function(e) {
		e.preventDefault();
		var _this = this;
		
		var form = $(this.refs.form);
		var promise = $.post(form.attr('action'), form.serialize());
		stemcounter.ajaxHandler(promise, form, function(r){
			if ( r.success && r.message ) {
				alertify.success( r.message );
			}

			if (_this.state.values.method == 'add') {
				stemcounter.gaEvent('event', 'created');
				window.location.href = r.payload.event_url;
			} else {
				if (_this.props.layout == 'full') {
					window.location.reload();
				}
			}
		});
	},

	selectionChanged: function(field, e) {
		var a = e.target.value.toString();
		var selectedoptions = [];
		for ( var i = 0; i < e.target.options.length; i++ ) {
			var opt = e.target.options[i];
			if (opt.selected) {
				selectedoptions.push(opt.value || opt.text);
			}
		}
		var updatedState = {
			values: Object.assign( {}, this.state.values ) 
		};
		updatedState.values['tags'] = selectedoptions;
		this.setState( updatedState );
	},

	fieldChanged: function(field, e, e2) {
		var updatedState = {
			values: this.state.values
		};

		if (typeof this.refs.InvoicingCategories !== 'undefined') {
			updatedState.eventInvoiceValues = {
				hardgood_multiple: this.refs.InvoicingCategories.refs.hardgoodMultiple.state.hardgoodMultiple,
				fresh_flower_multiple: this.refs.InvoicingCategories.refs.freshFlowerMultiple.state.freshFlowerMultiple,
				labor: {
					global_labor_value: this.refs.InvoicingCategories.refs.applyLaborTo.state.globalLaborValue,
					hardgood: this.refs.InvoicingCategories.refs.applyLaborTo.state.hardgood,
					flower: this.refs.InvoicingCategories.refs.applyLaborTo.state.flower,
					base_price: this.refs.InvoicingCategories.refs.applyLaborTo.state.base_price,
					fee: this.refs.InvoicingCategories.refs.applyLaborTo.state.fee,
					rental: this.refs.InvoicingCategories.refs.applyLaborTo.state.rental
				}
			}
		}

		if (field === 'note' || field === 'proposal_note') {

			updatedState.values.note_styles.height = e.target.scrollHeight < 52 ? 52 : e.target.scrollHeight;

			if (field === 'note' && this.state.values.note.length > e.target.value.length) {
				updatedState.values.note_styles.height = 'auto';
				this.state.values.updatedTextArea = false;
			} else if (field === 'proposal_note' && this.state.values.proposal_note.length > e.target.value.length) {
				updatedState.values.note_styles.height = 'auto';
				this.state.values.updatedTextArea = false;
			}
		} else if (field === 'delivery_cb') {
			var delivery = document.getElementById('event_fields_delivery');

			if ( e.target.checked ) {
				delivery.style.display = 'block';
				updatedState.values['delivery'] = stemcounter.invoiceSettings.delivery;
			} else {
				updatedState.values['delivery'] = 0;
				delivery.style.display = 'none';
			}
		} else if (field === 'card_cb') {
			var card = document.getElementById('event_fields_card');

			if ( e.target.checked ) {
				card.style.display = 'block';
				updatedState.values['card'] = stemcounter.invoiceSettings.card;
			} else {
				updatedState.values['card'] = 0;
				card.style.display = 'none';
			}
		} else if (field === 'tax_rate_id') {
			if (e.target.value == 0) {
				updatedState.values['tax_rate_value'] = 0;
			} else {
				updatedState.values['tax_rate_value'] = this.getTaxRate(e.target.value).value;
			}
		} else if (field === 'tax_rate_id2') {
			if (e.target.value == 0) {
				updatedState.values['tax_rate_value2'] = 0;
			} else {
				updatedState.values['tax_rate_value2'] = this.getTaxRate(e.target.value).value;
			}
		} else if (field === 'state') {
			updatedState.values['state'] = e.target.value;

			if ( 'template' == e.target.value ) {
				var new_customer = _.findWhere( this.state.customersList, { first_name: 'Template' } );
				new_customer = new_customer ? new_customer.id : -1;
				if ( updatedState.values.customerIds[1] && -1 != updatedState.values.customerIds[1] ) {
					updatedState.values.customerIds[1] = new_customer;
				}
				updatedState.values.customerIds[0] = new_customer;
			}
		} else if (field === 'event_tags') {
			var a = e.target.value.toString();
			var selectedoptions = [];
			for ( var i = 0; i < e.target.options.length; i++ ) {
				var opt = e.target.options[i];
				if (opt.selected) {
					selectedoptions.push(opt.value || opt.text);
				}
			}
			
			updatedState.values['tags'] = selectedoptions;
		} else if (field === 'customer_id') {
			updatedState.values.customerIds[ parseInt( e, 10 ) ] = e2.target.value
			if (e2.target.value > 0) {
				// updatedState.values.name = this.getCustomerName(updatedState.values.customerIds[0])/* + ' Wedding'*/;
				// updatedState.values.clearName = true;
			} else {
				// updatedState.values.name = '';
				// updatedState.values.clearName = false;
			}

			// shim to not break the "updatedState.values[field] = e.target.value" line below
			e = e2;
		} else if ( 'from_submission' === field ) {
			if ( -1 != e.target.value ) {
				var submission = _.findWhere( this.props.eventData.submissions, { id: parseInt( e.target.value, 10 ) } );
				if ( submission ) {
					// Pre-select the customer from the form submission
					if ( -1 != submission.customer_id ) {
						updatedState.values.customerIds = [ submission.customer_id, -1 ];
					} else {
						updatedState.values.customerIds = [ -1, -1 ];
					}

					if ( submission.event_fields ) {
						if ( submission.event_fields.date ) {
							updatedState.values.date = submission.event_fields.date;
						} else {
							updatedState.values.date = '';
						}

						if ( submission.event_fields.name ) {
							updatedState.values.name = submission.event_fields.name;
						} else {
							updatedState.values.name = '';
						}
					}
				}
			}
		}

		updatedState.values[field] = e.target.value;

		if (field == 'name' && updatedState.values.clearName) {
			updatedState.values.name = this.getCustomerName(updatedState.values.customerIds[0]) + ' ';
			updatedState.values.clearName = false;
		}
		
		this.setState(updatedState);
	},

	enableFormEdit: function(e) {
		e.preventDefault();

		this.setState({
			previousValues: JSON.parse(JSON.stringify(this.state.values)), // clone object
			formState: 'edit'
		});
	},

	cancelFormEdit: function(e) {
		e.preventDefault();

		this.setState({
			values: this.state.previousValues,
			formState: 'view'
		});
	},

	getTaxRate: function(taxRateId) {
		var result = null;

		if (taxRateId) {
			for (var i = 0; i < this.state.taxRates.length; i++) {
				var rate = this.state.taxRates[i];
				if (rate.id == taxRateId) {
					result = rate;
					break;
				}
			};
		}

		return result;
	},

	getTaxRateName: function(taxRateId) {
		var taxRateName = 'No Tax';
		var rate = this.getTaxRate(taxRateId);
		if (rate) {
			taxRateName = rate.name + ' - ' + rate.value + '%';
		}
		return taxRateName;
	},

	getCustomerName: function(customerId, customersList) {
		if (customersList !== undefined) {
			customersList = customersList;
		} else {
			customersList = this.state.customersList;
		}

		for (var i = 0; i < customersList.length; i++) {
			if (customersList[i].id == customerId) {
				return customersList[i].first_name + ' ' + customersList[i].	last_name;
			}
		}

		return 'No Customer';
	},

	addCustomerToList: function(r) {
		var updatedState = JSON.parse(JSON.stringify(this.state));

		updatedState.values.customerIds[0] = r.payload.customer_id;
		updatedState.customersList = r.payload.customers_list;
		this.setState(updatedState);
		
		$('.customer-properties-sidebar').removeClass('visible');
		ReactDOM.unmountComponentAtNode(document.getElementById('edit-customer-form'));
		$('body').removeClass('customers-sidebar-on-top');
	},

	editCurrentCustomer: function(e) {
		e.preventDefault();
		if (this.state.values.customerIds[0] == -1) return;
		var title = (typeof title == 'undefined') ? 'Edit Customer' : title;
		var url = stemcounter.aurl({
			action: 'sc_edit_user_customer_form',
			customer_id: this.state.values.customerId,
			event_id: this.state.values.id
		});

		stemcounter.page.editCustomerCaller = this;

		stemcounter.openAjaxModal(title, url, 'new-modal');
	},

	editEventCustomer: function(r) {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		updatedState.values.customerId = r.payload.customer_id;
		updatedState.customersList = r.payload.customers_list;

		this.setState(updatedState);
	},

	componentDidMount: function() {
		if (this.refs.eventDateField) {
			var _this = this;
			$(this.refs.eventDateField).datepicker({ 
				dateFormat: this.state.values.dateFormat,
				onClose: function(val, opts) {
					var updatedState = JSON.parse(JSON.stringify(_this.state));
					updatedState.values.date = val;
					_this.setState(updatedState);
				}
			});
		}
		stemcounter.unmountComponentOnModalClose( this );
  	},
	componentDidUpdate: function() {
		if (this.state.values.updatedTextArea === false && typeof this.refs.form !== 'undefined') {
			var textarea = this.refs.form.getElementsByTagName('textarea');

			if (textarea.length) {
				var updatedState = { 
					values: this.state.values,
				};

				updatedState.values.note_styles.height = textarea[0].scrollHeight;
				updatedState.values.updatedTextArea = true;
				this.setState(updatedState);
			}
		}
	}
	,
	componentWillUnmount: function() {
		/*if ( this.refs.eventDateField && $(this.refs.eventDateField).length ) {
			$(this.refs.eventDateField).datepicker('destroy');
		}*/
	},

	onCategoryChange: function( invoicingCategoriesState ){
		var updatedState = {
			eventInvoiceValues: this.state.eventInvoiceValues,
			values: this.state.values
		};

		updatedState.values.categoryId = invoicingCategoriesState.categoryId;
		updatedState.eventInvoiceValues.hardgood_multiple = invoicingCategoriesState.categoryHardgood;
		updatedState.eventInvoiceValues.fresh_flower_multiple = invoicingCategoriesState.categoryFreshFlower;

		updatedState.eventInvoiceValues.labor = {
			global_labor_value: invoicingCategoriesState.categoryLabor.global_labor_value,
			flower: invoicingCategoriesState.categoryLabor.flower,
			hardgood: invoicingCategoriesState.categoryLabor.hardgood,
			base_price: invoicingCategoriesState.categoryLabor.base_price,
			fee: invoicingCategoriesState.categoryLabor.fee,
			rental: invoicingCategoriesState.categoryLabor.rental
		};

		this.setState(updatedState);
	},

	refreshFinalPayment: function(amount) {
		var updatedState = JSON.parse(JSON.stringify(this.state));
		updatedState.values.eventCost = amount.toFixed10(2);
		updatedState.values.scheduledPayments[updatedState.values.scheduledPayments.length - 1].amount = amount.toFixed10(2);
		updatedState.previousValues.scheduledPayments[updatedState.previousValues.scheduledPayments.length - 1].amount = amount.toFixed10(2);
		updatedState.previousValues.eventCost = amount.toFixed10(2);
		this.setState(updatedState);
	},

	onCustomerSidebarClose: function(event) {
		event.preventDefault();

		$(event.target).closest('.customer-properties-sidebar').removeClass('visible');
		ReactDOM.unmountComponentAtNode(document.getElementById('edit-customer-form'));
		$('body').removeClass('customers-sidebar-on-top');
	},

	customerSelectRef: function(ref) {
		if (ref !== null) {
			var _this = this;

			if (ref) {
				this.selectRef = ref;
				$(ReactDOM.findDOMNode(ref)).on('stemcounter.create_new_customer', function() {
					/* var title = (typeof title == 'undefined') ? 'Add New Customer' : title;
					var url = stemcounter.aurl({
						action: 'sc_add_user_customer_form'
					}); */
					// stemcounter.openAjaxModal(title, url, 'new-modal');

					$('.customer-properties-sidebar').on('click', '.close-btn', _this.onCustomerSidebarClose);

					$('.customer-properties-sidebar').addClass('visible');
					$('body').addClass('customers-sidebar-on-top');

					ReactDOM.render(
						React.createElement(CustomerModal, {
							layout: 'add-customer',
							properties: stemcounter.customer_properties,
							createNewCustomerCaller: _this
						}),
						document.getElementById('edit-customer-form')
					);
				});
			} else {
				$(ReactDOM.findDOMNode(_this.selectRef)).off('stemcounter.create_new_customer');
			}
		}
	},

	replaceCustomerSelect2: function(node) {
	    window.stemcounter.AddCustomerOptionSelect($(node), {});
	},

	renderCustomerSelect: function(customerId, i) {
		// We don't want to show multiple selects if
		// the user hasn't selected the first customer yet
		if ( 0 < i && -1 == this.state.values.customerIds[0] ) {
			return null;
		}

		return (
			<div className="modalpane-row clearfix" key={'customer-' + i}>
				<label className="col-sm-12 modalpane control-label">
					<div className="lbl">
						{ i == 0 ? null : <span>Second </span> }Customer{ i == 0 ? <span className="mandatory"></span> : null }
					</div>
					<ReactSelect2
						name="event_fields[customer_id][]"
						className="form-control"
						selectedValue={String( customerId )}
						onChange={this.fieldChanged.bind(this, 'customer_id', i)}
						replaceSelect2={this.replaceCustomerSelect2}
						ref={this.customerSelectRef}
					>
						<option value="-1">No Customer</option>
						{this.state.customersList.map(function(customer, i){
							var name = $.trim( customer.first_name + ' ' + customer.last_name );
							return <option key={'cust_' + i + '_' + customer.id} value={customer.id}>{name}</option>;
						})}
					</ReactSelect2>
				</label> 
			</div>
		);
	},

	render: function() {
		var url = stemcounter.aurl({
			action: 'sc_edit_user_event'
		});

		var taxRateOptions = [];
		for (var i = 0; i < this.state.taxRates.length; i++) {
			var rate = this.state.taxRates[i];
			var name = rate.name + ' - ' + rate.value + '%'; 
			taxRateOptions.push(React.createElement("option", {key: i, value: rate.id}, name));
		}

		var eventPositionOptions = [
			<option key="0" value="inquiry">Inquiry</option>,
			<option key="1" value="proposal_sent">Proposal Sent</option>,
			<option key="2" value="booked">Booked</option>,
			<option key="3" value="completed">Completed</option>,
			<option key="4" value="template">Template</option>
		];

		var formSubmissionSelect = null;

		if ( ( 0 == this.state.values.id || 1 == parseInt( this.state.values.duplicate, 0 ) ) && stemcounter.AC.can_access( stemcounter.AC.STUDIO ) && this.props.eventData.submissions && this.props.eventData.submissions.length ) {
			formSubmissionSelect = (
				<div className="modalpane-row clearfix">
					<label className="col-sm-12 modalpane control-label">
						<div className="lbl">
							Create From Submission
						</div>
						<ReactSelect2
							name="event_fields[from_submission]"
							className="form-control"
							selectedValue={this.state.values.from_submission}
							onChange={this.fieldChanged.bind(this, 'from_submission')}
						>
							<option value="">No</option>
							{this.props.eventData.submissions.map(function(submission, i){
								var name = submission.form;
								if ( '' != submission.customer ) {
									name += ' by ' + submission.customer;
								}

								name += ' on ' + submission.submitted_on;

								return <option key={'submission_' + i + '_' + submission.id} value={submission.id}>{name}</option>;
							})}
						</ReactSelect2>
					</label> 
				</div>
			);
		}

		return (
			<form action={url} method="post" onSubmit={this.submit} ref="form" className="form-horizontal style-form edit-event-form"> 
				<input type="hidden" name="method" value={this.state.values.method} />
				<input type="hidden" name="layout" value={this.props.layout} />
				<input type="hidden" name="id" value={this.state.values.id} />
				<input type="hidden" name="duplicate" value={this.state.values.duplicate} />
				<div className="form-layout">
					{formSubmissionSelect}

					{this.state.values.customerIds.map(this.renderCustomerSelect)}

					<div className="modalpane-row clearfix"> 
						<label className="col-xs-12 col-sm-7 modalpane control-label">
							<div className="lbl">
								Event Name<span className="mandatory"></span>
							</div>
							<input type="text" placeholder="Enter event name here" name="event_fields[name]" className="form-control" value={this.state.values.name} onChange={this.fieldChanged.bind(this, 'name')} />
						</label> 

						<label className="col-xs-12 col-sm-5 modalpane control-label">
							<div className="lbl">
								Date
							</div>
							<input type="text" name="event_fields[date]" className="form-control" placeholder={this.state.values.dateFormat.toUpperCase()} value={this.state.values.date} onChange={this.fieldChanged.bind(this, 'date')} ref="eventDateField" />
						</label>
					</div> 

					<div className="modalpane-row clearfix">
						<label className="col-xs-12 modalpane control-label">
							<div className="lbl">
								Event Stage
							</div>
							<ReactSelect2
								name="event_fields[state]"
								className="form-control"
								selectedValue={String( this.state.values.state )}
								onChange={this.fieldChanged.bind(this, 'state')}
							>
								{eventPositionOptions}
							</ReactSelect2>
						</label>
					</div>

					{ stemcounter.AC.can_access( stemcounter.AC.TAGGING ) ? <div className="modalpane-row clearfix">
						<label className="col-xs-12 col-sm-12 modalpane control-label">
							<div className="lbl">
								Tags
							</div>
							<ReactSelect2
								name="event_fields[tags][event_tags][]"
								className="form-control"
								multiple={true}
								selectedValue={this.state.values.tags}
								onChange={this.fieldChanged.bind(this, 'event_tags')}
							>
								{this.props.eventData.tagsList.map( function( val, index ) {
									return (
										<option key={'tagopt-' + index.toString() + '-' + val} value={val}>{val}</option>
									)
								}, this )}
							</ReactSelect2>
						</label>
					</div> : null }

					<div className="modal-footer"> 
						<div className="pull-left hide">*required</div> 
						<button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button> 
						<button type="submit" value="" className="btn btn-primary">Save</button>
					</div>
				</div>
			</form>
		);
	}
});

jQuery(document).on('stemcounter.action.renderEditEventForm', function(e, settings){
	eventForms[settings.layout] = ReactDOM.render(
		React.createElement(EditEventForm, {eventData: settings.data, layout: settings.layout,parentNode: settings.node}),
		$(settings.node).get(0)
	);
});

})(jQuery);