// use a closure so we do not pollute the global scope
(function($){
'use strict';

var SettingsForm = React.createClass({
	getInitialState: function() {
		this.submit = _.debounce( this.submit, 400 );
		this.getFontOptions();

		return {
			color: this.props.color,
			style: this.props.style,
			font: this.props.font,

		};
	},

	onFormSubmit: function( e ) {
		e.preventDefault();
		this.submit();
	},

	submit: function() {
		var $form = $(this.refs.form);
		var th = this;
		
		$.post($form.attr('action'), $form.serialize())
		.always(function(response){
			stemcounter.JSONResponse(response, function(r){
				$('style#studio-whitelabel-css').remove();

				if ( r.payload.whitelabel_css ) {
					$('head').append( r.payload.whitelabel_css );
				}
			}.bind(th));
		});
	},

	onColorChange: function(color) {
		this.setState({
			color: color
		});

		this.submit();
	},

	onLogoColorPickerModalSave: function( $modal ){
		var color = $modal.find('.color-input').val();

		this.setState({
			color: color
		}, this.submit);
	},

	openLogoColorPickerModal: function(){
		var $modalWrapper = GenericModal.prototype.createWrapper('new-modal logo-color-picker-modal'),
			modalContent = (
				<LogoColorPickerModal
					logo={this.props.logo}
					color={this.state.color}
				/>
			);

		ReactDOM.render(
			<GenericModal
				title="Pick Color"
				content={modalContent}
				onSave={this.onLogoColorPickerModalSave}
				saveLabel="Set Color"
			/>,
			$modalWrapper[0]
		);
	},

	getStyleOptions: function() {
		var style = [];
		style.push( {
			label: 'Default',
			value: ''
		} );
		style.push( {
			label: 'Proposal Option 2',
			value: '2'
		} );

		return style;
	},

	getFontOptions: function() {
		var fonts = [];
		fonts.push( {
			label: 'Default' ,
			value: '' ,
			className: 'pt-sans-caption'
		} );
		fonts.push( {
			label: 'Arimo' ,
			value: 'arimo' ,
			className: 'arimo'
		} );
		fonts.push( {
			label: 'Cormorant Garamond' ,
			value: 'cormorant-garamond' ,
			className: 'cormorant-garamond'
		} );
		fonts.push( {
			label: 'Patua One' ,
			value: 'patua-one' ,
			className: 'patua-one'
		} );
		fonts.push( {
			label: 'Courgette' ,
			value: 'courgette' ,
			className: 'courgette'
		} );

		return fonts;

	},

	onFontChange: function( option ) {
		this.setState({
			font: option.value
		}, this.submit );
	},

	onStyleChange: function( option ) {
		this.setState({
			style: option.value
		}, this.submit );
	},

	render: function() {
		console.log(  );
		var url = stemcounter.aurl({
			'action': 'sc_studio_settings'
		});
		
		return (
			<form action={url} method="post" onSubmit={this.onFormSubmit} ref="form">
				<div className="pane-row">
					<div className="pane color-pane">
						<label className="control-label">Color</label>
						<ColorPickerContainer color={this.state.color} name="proposalColor" onChange={this.onColorChange} className="value" />
						{this.props.logo ? (
							<div className="value">
								<small>Not sure which color to pick? <a href="#" className="sc-primary-color" onClick={this.openLogoColorPickerModal}>Pick one from your logo</a>!</small>
							</div>
						) : null}
					</div>
					<div className="pane label-pane">
						<label className="control-label">White Label</label>
						<div className="value">
							<label>
								<input defaultValue='yes' defaultChecked={Boolean(this.props.white_label_acc)} name="choiseLogo" type="checkbox" onChange={this.submit} /> White label my account!
							</label>
						</div>
					</div>
					{/*<div className="pane style-pane">
						<label className="control-label" htmlFor="proposal_style">Proposal Style</label>
						<div className="value">
							<VirtualizedSelect
							clearable={false}
							searchable={false}
							scrollMenuIntoView={false}
							name={"proposalStyle"}
							className={"select-style-items"}
							value={this.state.style}
							onChange={this.onStyleChange}
							options={this.getStyleOptions()}
							optionHeight={32}
							/>
						</div>
					</div>
					<div className="pane fonts-pane">
						<label className="control-label">Fonts</label>
						<div className="value">
							<VirtualizedSelect
							clearable={false}
							searchable={false}
							scrollMenuIntoView={false}
							name={"proposalFont"}
							className={"select-font-items"}
							value={this.state.font}
							onChange={this.onFontChange}
							options={this.getFontOptions()}
							optionHeight={32}
							/>
						</div>
					</div>*/}
				</div>
			</form>
		)
	}
});

var LogoColorPickerModal = React.createClass({
	getInitialState: function(){
		return {
			color: this.props.color,
			newColor: false
		}
	},

	onModalShown: function(){
		var img = new Image(),
			maxWidth = $(this.canvas).parent().width(),
			maxHeight,
			rWidth,
			rHeight,
			nWidth,
			nHeight,
			ratio;

		img.crossOrigin = 'Anonymous';
		img.onload = (function(){
			this.canvasCtx.drawImage( img, 0, 0, this.props.logo.w, this.props.logo.h, 0, 0, this.canvas.width, this.canvas.height );
		}).bind(this);

		if ( maxWidth > screen.width && screen.width > 0 ) {
			maxWidth = screen.width;
		}

		maxHeight = Math.min( 550, maxWidth );
		rWidth = 1;
		rHeight = 1;

		if ( this.props.logo.w > maxWidth ) {
			rWidth = maxWidth/this.props.logo.w;
		} 
		if ( this.props.logo.h > maxHeight ) {
			rHeight = maxHeight/this.props.logo.h;
		}

		ratio = Math.min( rWidth, rHeight );
		nWidth = this.props.logo.w * ratio;
		nHeight = this.props.logo.h * ratio;
		this.canvas.width = nWidth;
		this.canvas.height = nHeight;
		this.canvas.style.width = nWidth + 'px';
		this.canvas.style.height = nHeight + 'px';

		img.src = this.props.logo.src + '?' + new Date().getTime();
	},

	componentDidMount: function(){
		$(ReactDOM.findDOMNode( this )).closest('.modal').one('shown.bs.modal', this.onModalShown);
	},

	canvasRef: function(ref) {
		this.canvas = ref;
		this.canvasCtx = this.canvas ? this.canvas.getContext( '2d' ) : false;
	},

	onCanvasMouseMove: function(e){
		var x = e.nativeEvent.offsetX,
			y = e.nativeEvent.offsetY;

		var cData = this.canvasCtx.getImageData(x,y,1,1).data;
		this.setState({
			newColor: stemcounter.rgb2hex( cData[0], cData[1], cData[2] )
		});
	},

	onCanvasClick: function(e){
		var x = e.nativeEvent.offsetX,
			y = e.nativeEvent.offsetY;

		var cData = this.canvasCtx.getImageData(x,y,1,1).data;
		this.setState({
			color: stemcounter.rgb2hex( cData[0], cData[1], cData[2] )
		});
	},

	render: function() {
		return (
			<div className="">
				<div className="modalpane-row clearfix pane-keep">
					<label className="col-xs-12 control-label modalpane">
						<div className="lbl">Click anywhere on your logo to select a color</div>
					</label>
					<div className="col-xs-12 modalpane">
						<canvas ref={this.canvasRef} onMouseMove={this.onCanvasMouseMove} onClick={this.onCanvasClick}></canvas>
					</div>
				</div>
				<div className="modalpane-row clearfix pane-keep">
					<label className="col-xs-6 control-label modalpane">
						<div className="lbl">Current Color</div>
						<div className="color-sample" style={{ backgroundColor: this.state.color ? this.state.color : 'transparent' }}></div>
					</label>
					<label className="col-xs-6 control-label modalpane">
						<div className="lbl">New Color</div>
						<div className="color-sample" style={{ backgroundColor: this.state.newColor ? this.state.newColor : 'transparent' }}></div>
					</label>
				</div>
				<input type="hidden" className="color-input" value={this.state.color} onChange={$.noop} />
			</div>
		);
	}
});

$(document).on( 'stemcounter.action.renderSettingsForm', function(e, settings){
	ReactDOM.render( 
		<SettingsForm
			color={settings.color}
			white_label_acc={settings.white_label_acc}
			logo={settings.logo}
			style={settings.style}
			font={settings.font}
		/>,
		settings.node.get(0)
	);
} );

})(jQuery);
