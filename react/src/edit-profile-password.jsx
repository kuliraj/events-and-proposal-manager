// use a closure so we do not pollute the global scope
(function($){
'use strict';

var EditProfilePasswordForm = React.createClass({
	isValid: function(){
		var valid = true;
		var password = this.refs.passwordField.state.value;
		var confirm = this.refs.confirmField.state.value;
		var hasUppercase = new RegExp('[A-Z]', 'g');
		var hasLowercase = new RegExp('[a-z]', 'g');
		var hasNumber = new RegExp('[0-9]', 'g');
		var errors = [];

		password = password.replace(/\s+/g, '');
		confirm = confirm.replace(/\s+/g, '');

		if (password.length < 8) {
			errors.push('Your password must consist of at least 8 characters.');
		}

		if (password != confirm) {
			errors.push('Your passwords do not match.');
		}

		if (! hasUppercase.test(password)) {
			errors.push('Your password must contain at least one uppercase character.');
		}

		if (! hasLowercase.test(password)) {
			errors.push('Your password must contain at least one lowercase character.');
		}

		if (! hasNumber.test(password)) {
			errors.push('Your password must contain at least one number.');
		}

		if (errors.length) {
			alertify.error(errors.join('<br>'));

			return false;
		}

		return valid;
	},

	submit: function(e) {
		e.preventDefault();
		var _this = this;
		
		if (!this.isValid()) {
			return;
		}

		var form = $(this.refs.form);
		$.post(form.attr('action'), form.serialize())
			.always(function(response){
				stemcounter.JSONResponse(response, function(r){
					alertify.success(r.message);
					_this.refs.passwordField.setState({value: ''});
					_this.refs.confirmField.setState({value: ''});
				});
			});
	},

	render: function() {
		var url = stemcounter.aurl({
			action: 'sc_edit_user_password'
		});
		
		return (
			<form action={url} method="post" onSubmit={this.submit} ref="form" className="form-horizontal style-form">
				<div className="form-layout">
					<PasswordField ref="passwordField" label="Change password" name="new_password" />
					<PasswordField ref="confirmField" label="Confirm Password" name="confirm_password" />
					<div className="pane">
						<button type="submit" value="" className="btn btn-primary" onClick={this.submit}>Save Password</button>
					</div>
				</div>
			</form>
		);
	}
});

var PasswordField = React.createClass({
	getInitialState: function(){
		return {
			value: ''
		};
	},

	updateValue: function(e){
		this.setState({
			value: e.target.value
		});
	},

	render: function(){
		return (
			<div className="pane">
				<label className="control-label">{this.props.label}</label>
				<div className="value">
					<input type="password" name={this.props.name} className="form-control" value={this.state.value} onChange={this.updateValue} />
				</div>
			</div>
		);
	}
});

jQuery(document).on('stemcounter.action.renderEditProfilePasswordForm', function(e, settings){
	ReactDOM.render(
		<EditProfilePasswordForm parentNode={settings.node}/>,
		$(settings.node).get(0)
	);
});

})(jQuery);