// use a closure so we do not pollute the global scope

var ArrangementPhoto = React.createClass({displayName: 'ArrangementPhoto',
  shouldComponentUpdate: function(nextProps, nextState) {
    return this.props.arrangementIndex !== nextProps.arrangementIndex ||
        this.props.photoIndex !== nextProps.photoIndex ||
        this.props.proposalState !== nextProps.proposalState ||
         !_.isEqual(this.state, nextState);
  },

  getInitialState: function() {
    var state = {
      imageId: null,
      imageURL: '',
      fullImageURL: '',
      fullImageW: '',
      fullImageH: ''
    };

    if (typeof this.props.defaultValue !== 'undefined') {
      state.imageId = this.props.defaultValue.image_id;
      state.imageURL = this.props.defaultValue.image_url;
      state.fullImageURL = this.props.defaultValue.full_image_src;
      state.fullImageW = this.props.defaultValue.full_image_w;
      state.fullImageH = this.props.defaultValue.full_image_h;
    }

    return state;
  },

  setPhoto: function( attachment ) {
    var _this = this;
    var updatedState = {};

    updatedState.imageURL = attachment.sizes.medium ? attachment.sizes.medium.url : attachment.url;
    updatedState.imageId = attachment.id;
    updatedState.fullImageURL = attachment.url;
    updatedState.fullImageW = attachment.width;
    updatedState.fullImageH = attachment.height;
    _this.setState(updatedState);

    if (typeof _this.props.addPhotoToArrProposal !== 'undefined') {
      
      _this.props.addPhotoToArrProposal({
        image_id: attachment.id,
        image_url: attachment.sizes.medium ? attachment.sizes.medium.url : attachment.url,
        full_image_src: attachment.url,
        full_image_w: attachment.width,
        full_image_h: attachment.height

      }, _this.props.arrangementIndex);
    }

  },

  addPhoto: function() {
    var _this = this;

    stemcounter.arrangement_photo_frame.open();

    stemcounter.arrangement_photo_frame.off('select').on('select', (function() {
      
      // Get media attachment details from the frame state
      var attachment = stemcounter.arrangement_photo_frame.state().get('selection').first().toJSON();
      var updatedState = {};

      updatedState.imageURL = attachment.sizes.medium ? attachment.sizes.medium.url : attachment.url;
      updatedState.imageId = attachment.id;
      updatedState.fullImageURL = attachment.url;
      updatedState.fullImageW = attachment.width;
      updatedState.fullImageH = attachment.height;
      _this.setState(updatedState);

      if (typeof _this.props.addPhotoToArrProposal !== 'undefined') {
        
        _this.props.addPhotoToArrProposal({
          image_id: attachment.id,
          image_url: attachment.sizes.medium ? attachment.sizes.medium.url : attachment.url,
          full_image_src: attachment.url,
          full_image_w: attachment.width,
          full_image_h: attachment.height

        }, _this.props.arrangementIndex);
      }

      if ( this.props.inModal ) {
        setTimeout(function() {
          $('body').addClass('modal-open');
        }, 10);
      }

    }).bind(this));

    /** 
     * Add modal open class in order for the bootstrap modal 
     * to be scrollable again
     * after closing the WP Media modal 
     **/
    stemcounter.arrangement_photo_frame.off('escape').on('escape', (function(){
      if ( this.props.inModal ) {
        setTimeout(function() {
          $('body').addClass('modal-open');
        }, 10);
      }
    }).bind(this));

  },

  removePhoto: function() {
    var updatedState = {}

    updatedState.imageId = null;
    updatedState.imageURL = '';

    if (typeof this.props.removePhotoFromArrProposal !== 'undefined') {
        this.props.removePhotoFromArrProposal({
          image_id: null,
          image_url: '',
          full_image_src: '',
          full_image_w: '',
          full_image_h: ''

        }, this.props.arrangementIndex);
    }

    this.setState(updatedState);
  },

  render: function() {
    var AddRemoveElement = null;
    var cursor = null;
    if ( 'view' !== this.props.proposalState ) {
      AddRemoveElement = (
        <button className="remove-photo" onClick={this.removePhoto}>
          <i className="fa fa-times" />
          <input type="hidden" name={'image_' + this.props.componentPosition + '_id'} value={this.state.imageId} />
        </button>
      );
    } else {
      cursor = 'pointer';
    }

    return (
      <div
        className="arrangement-photo col-xs-3"
        data-full-image-url={this.state.fullImageURL}
        data-full-image-w={this.state.fullImageW}
        data-full-image-h={this.state.fullImageH}
        style={{
          backgroundImage: 'url("' + this.state.imageURL + '")',
          display: this.props.hidden ? 'none' : 'block',
          cursor: cursor
        }}>
        {AddRemoveElement}
      </div>
    );
  }
});

(function(){
'use strict';

/* Run lodash in no conflict mode because we need _ underscore.js */
if (undefined === window.lodash) {
  window.lodash = _.noConflict();
}

function getKeypair(itemType, itemId, variationId) {
  var keypair = [];
  keypair.push( (itemType == 'flower' ? 'flower' : 'service') );
  keypair.push( itemId );

  if (typeof variationId != 'undefined') {
    keypair.push( variationId );
  }

  return keypair.join('|');
}

function getItemTypeFromKeypair(keypair) {
  var data = keypair.split('|');
  var itemType = (data[0] == 'flower') ? 'flower' : 'service';
  return itemType;
}

function getItemIdFromKeypair(keypair) {
  return keypair.split('_')[0];
}

function getItemVariationIdFromKeypair(keypair) {
  var data = keypair.split('|');
  var variationId = (typeof data[2] == 'undefined') ? 0 : parseInt(data[2]);
  return variationId;
}

var EditArrangementForm = React.createClass({displayName: "EditArrangementForm",
  getInitialState: function() {
    var state = {
      id: 0,
      event_id: this.props.eventId,
      userId: parseInt(this.props.userId),
      recipe_id: this.props.recipe_id,
      recipe_name: this.props.recipe_name,
      name: '',
      quantity: 1,
      include: 1,
      tax: 1,
      note: '',
      items: [],
      photos: [],
      invoice: [],
      userRecipes: this.props.userRecipes,
      UserRecipeIdSelected: this.props.UserRecipeIdSelected,
      totals: {
        cost: 0,
        total: 0,
        total_item_price: 0,
        override: ''
      }
    };

    if (this.props.values) {
      //fill state object with props
      for (var prop in this.props.values) {
        if (this.props.values.hasOwnProperty(prop) && this.props.values[prop] !== undefined) {
          state[prop] = this.props.values[prop];
        }
      }
    }

    return state;
  },

  componentDidMount: function(e) {
    stemcounter.unmountComponentOnModalClose( this );
  },

  deleteArrangement: function() {
    $(document).trigger('stemcounter.action.deleteArrangement', {
      arrangementId: this.state.id
    });
  },

  calculateNewTotalsThrottle: null,
  calculateNewTotals: function() {
    var _this = this;

    clearTimeout(_this.calculateNewTotalsThrottle);
    _this.calculateNewTotalsThrottle = setTimeout(function(){
      _this.totalsRef.calculateNewTotals();
    }, 500);
  },

  getDescription: function(){
    return {
      name: this.state.name,
      items: this.refs.ItemsListController.getSimpleItemsList()
    };
  },

  onNameChange: function(value){
    this.setState({
      name: value
    });
  },

  onInvoiceCatValueChange: function() {
    this.calculateNewTotals();
  },

  deleteThis: function() {
    this.props.deleteRecipe( this.props.recipe_id );
  },

  onRecipeSelect: function(e) {
    e.preventDefault();

    var updatedState = $.extend(true, {}, this.state);
    var user_recipe_id = parseInt(e.target.value);

    if ( 0 == user_recipe_id ) {
      updatedState.items = [];
      updatedState.recipe_name = this.props.recipe_name;
    } else {
      for (var i = 0; i < this.state.userRecipes.length; i++) {
        if ( user_recipe_id == this.state.userRecipes[i].id ) {
          updatedState.items = this.state.userRecipes[i].items;
          updatedState.recipe_name = this.state.userRecipes[i].name;
          break;
        }
      }
    }
    updatedState.UserRecipeIdSelected = user_recipe_id;

    this.setState( updatedState );
  },

  setTotalsRef: function( ref ) {
    this.totalsRef = ref;
  },

  render: function() {
    var _this = this;
    var buttons = [
     React.createElement("button", {type: "button", key: "delete", className: "btn btn-default", onClick: function(e){
          var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0];
          var component = $(e.target).closest('.edit-arrangement-form');
          var modal = $(e.target).closest('.modal.fade.in');
          e.preventDefault();
          stemcounter.confirmModal({
            confirmCallback: function(){
              _this.deleteThis();
              modal.modal('hide');
              ReactDOM.unmountComponentAtNode(component[0]);
            }
          });
        }}, "Delete"),
      React.createElement("button", {type: "button", key: "cancel", className: "btn btn-default button-cancel", "data-dismiss": "modal"}, "Cancel"),
      React.createElement("button", {type: "submit", key: "submit", className: "btn btn-primary button-submit"}, "Save")
    ];
    if (this.state.id > 0 && ! this.props.itemsOnly) {
      buttons.unshift(React.createElement("button", {type: "button", key: "delete", className: "btn btn-default", onClick: this.deleteArrangement}, "Delete"));
    }
    
    if ( this.props.itemsOnly ) {
      return (
        <div>
          <input type="hidden" name="id" value={this.state.id} />
          <input type="hidden" name="event_id" value={this.props.eventId} />
          <input type="hidden" name="items_only" value="1" />
          <input type="hidden" name="category_id" value={this.props.categoryId} />
          <input type="hidden" name="hardgood_multiple" value={this.state.invoice.hardgood_multiple} />
          <input type="hidden" name="fresh_flower_multiple" value={this.state.invoice.fresh_flower_multiple} />
          <input type="hidden" name="global_labor_value" value={this.state.invoice.labor.global_labor_value} />
          <input type="hidden" name="flower_labor" value={this.state.invoice.labor.flower} />
          <input type="hidden" name="hardgood_labor" value={this.state.invoice.labor.hardgood} />
          <input type="hidden" name="base_price_labor" value={this.state.invoice.labor.base_price} />
          <input type="hidden" name="fee_labor" value={this.state.invoice.labor.fee} />
          <input type="hidden" name="rental_labor" value={this.state.invoice.labor.rental} />
          <input type="hidden" name="quantity" value={this.state.quantity} />
          <input type="hidden" name="include" value={this.state.include} />
          <input type="hidden" name="tax" value={this.state.tax} />
          <input type="hidden" name="recipe_id" value={this.state.recipe_id} />
          <input type="hidden" name="user_recipe_id" value={this.state.UserRecipeIdSelected} />
          <div className="categories-group row">
            <div className="col-sm-12 input_fields_wrap">
              <ItemsListController
                userId={this.state.userId}
                defaultValue={this.state.items}
                eventId={this.props.eventId}
                recipe_name={this.state.recipe_name}
                onChange={this.calculateNewTotals}
                UserRecipeIdSelected={this.state.UserRecipeIdSelected}
                userRecipes={this.state.userRecipes}
                onRecipeSelect={this.onRecipeSelect}
                currencySymbol={this.state.totals.currencySymbol}
                standaloneRecipes={this.props.standaloneRecipes}
                ref="ItemsListController"
              />
            </div>
          </div>
          <ArrangementTotals
            defaultValue={this.state.totals.override}
            cost={this.state.totals.cost}
            total={this.state.totals.total}
            totalItemPrice={this.state.totals.total_item_price}
            currencySymbol={this.state.totals.currencySymbol}
            standaloneRecipes={this.props.standaloneRecipes}
            ref={this.setTotalsRef}
          />
          <div className="modal-footer"> 
            <div className="pull-left hidden">*required</div> 
            {buttons}
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <input type="hidden" name="id" value={this.state.id} />
          <input type="hidden" name="event_id" value={this.props.eventId} />
          <ArrangementName defaultValue={this.state.name} onChange={this.onNameChange} />
          <div className="clearfix form-group">
            <Quantity defaultValue={this.state.quantity} /> 
            <IncludeOnProposal defaultValue={this.state.include} />
            <NoTax defaultValue={this.state.tax} />
          </div>
          <div className="arrangement-photos-wrap">
            <ArrangementPhoto defaultValue={this.state.photos[0]} componentPosition={1} inModal={true} />
            <ArrangementPhoto defaultValue={this.state.photos[1]} componentPosition={2} inModal={true} />
            <ArrangementPhoto defaultValue={this.state.photos[2]} componentPosition={3} inModal={true} />
            <ArrangementPhoto defaultValue={this.state.photos[3]} componentPosition={4} inModal={true} />
          </div>
          <div className="categories-group">
            <div className="col-sm-12 input_fields_wrap">
              <ItemsListController defaultValue={this.state.items} eventId={this.props.eventId} onChange={this.calculateNewTotals} ref="ItemsListController" currencySymbol={this.state.totals.currencySymbol} userId={this.state.userId} />
            </div>
          </div>
          <ArrangementNote defaultValue={this.state.note} getDescription={this.getDescription} /><br />
          <div className="form-group">
            <div className="col-sm-12 invoicingCategories">
              <InvoicingCategories layout={this.props.layout} itemTypes={this.props.itemTypes} categoriesList={this.props.categoriesList} arrangementValues={this.state.invoice} categoryId={this.props.categoryId} onChange={this.onInvoiceCatValueChange} ref="InvoicingCategories" />
            </div> 
          </div>
          <ArrangementTotals defaultValue={this.state.totals.override} cost={this.state.totals.cost} total={this.state.totals.total} currencySymbol={this.state.totals.currencySymbol} ref="totals" /> 
          <div className="modal-footer">
              <div className="pull-left">*required</div>
              {buttons}
          </div>
        </div>
      );
    }
  }
});

var ArrangementName = React.createClass({displayName: "ArrangementName",
  label_text: 'Arrangement name*',
  name: 'name',

  getInitialState: function(){
    return {
      value: this.props.defaultValue
    }
  },

  onChange: function(event){
    var value = event.target.value;
    this.setState({
      value: value
    });
    if (this.props.onChange) {
      this.props.onChange(value);
    }
  },

  render: function() {
    return (
      <div className="form-group"> 
        <label className="col-sm-12 control-label">{this.label_text}</label>
        <div className="col-sm-12"> 
          <input type="text" name={this.name} value={this.state.value} onChange={this.onChange} className="form-control" />
        </div>
      </div>
    );
  }
});

var Quantity = React.createClass({displayName: "Quantity",
  label_text: 'No. of Arrangement',
  name: 'quantity',

  render: function() {
    return (
      <div className="col-sm-4 nosidepadding">
        <label className="col-sm-12 control-label">{this.label_text}</label> 
        <div className="col-sm-12">
          <input type="text" name={this.name} defaultValue={this.props.defaultValue} className="form-control"></input>
        </div>
      </div>
    );
  }
});

var ArrangementItem = React.createClass({displayName: "ArrangementItem",
  getInitialState: function() {
    var state = {
      // userItems: stemcounter.page.userItemsstemcounter.page.userItems,
      itemKeypair: '',
      itemCost: '0.00',
      itemId: '',
      itemVariationId: '',
      itemQuantity: 1,
      shy: this.props.shy,
      canAddMore: true
    };

    if (this.props.defaultValue) {
      var val = this.props.defaultValue;
      if (this.props.defaultValue.item) {
        state.itemId = this.props.defaultValue.item.id;
        state.itemName = this.props.defaultValue.item.name;
        if ( undefined !== this.props.defaultValue.item_variation ) {
          state.itemCost = null !== this.props.defaultValue.item_variation.cost ? this.props.defaultValue.item_variation.cost : this.props.defaultValue.item.default_variation.cost;
        }
        state.purchase_unit = this.props.defaultValue.item.purchase_unit;
      }
      if (this.props.defaultValue.item_variation_name) {
        state.itemVariationName = this.props.defaultValue.item_variation_name;
      } else {
        state.itemVariationName = '';
      }
      state.itemVariationId = val.item_variation_id;
      if ( ! this.props.standaloneRecipes ) {
        state.itemCost = val.cost;
      }
      state.itemKeypair = state.itemId + '_' + state.itemVariationId;
      state.itemQuantity = val.quantity;
      state.canAddMore = false;
    }

    if ( ! stemcounter.page.userItemsSorted ) {
      this.resortUserItems();
    }
    
    return state;
  },

  onUserItemAdded: function(event, data) {
    if ( ! stemcounter.page.userItemsSorted ) {
      this.resortUserItems();
    }

    var stateUpdate = {
      // userItems: stemcounter.page.userItems
      cachebust: Math.random()
    };

    if (typeof data != 'undefined' && data.caller == this) {
      stateUpdate.itemId = data.item.id;
      stateUpdate.itemName = data.item.name;
      stateUpdate.itemCost = data.item.default_variation.cost;
      stateUpdate.itemVariationId = data.itemVariationId;
      stateUpdate.itemVariationName = data.item.default_variation.name;
      stateUpdate.itemKeypair = data.item.id + '_' + data.itemVariationId;
      stateUpdate.purchase_unit = data.item.purchase_unit;
      if ( this.state.canAddMore ) {
        this.props.autoAddMore();
      }
    }

    this.componentChanged();
    this.setState(stateUpdate);
  },

  onUserItemModified: function(event, data) {
    var stateUpdate = {
      // userItems: stemcounter.page.userItems
      cachebust: Math.random()
    };

    if ( ! stemcounter.page.userItemsSorted ) {
      this.resortUserItems();
    }

    if ( this.state.itemId == data.item.id ) {
      var item = undefined !== stemcounter.page.userItemsIndex[ data.item.id.toString() ] ? stemcounter.page.userItems[ stemcounter.page.userItemsIndex[ data.item.id.toString() ] ] : null;
      if ( item ) {
        var variation = _.findWhere( item.variations, { id: this.state.itemVariationId } );
        if ( variation && variation.cost ) {
          stateUpdate.itemVariationId = data.itemVariationId;
          stateUpdate.itemVariationName = data.item.default_variation.name;
          stateUpdate.itemCost = variation.cost;
        } else {
          stateUpdate.itemVariationId = data.itemVariationId;
          stateUpdate.itemVariationName = data.item.default_variation.name;
          stateUpdate.itemCost = item.default_variation.cost;
        }
        stateUpdate.itemKeypair = data.item.id + '_' + data.itemVariationId;
        stateUpdate.purchase_unit = data.item.purchase_unit;
      }
    }

    this.setState(stateUpdate);
    this.componentChanged();
  },

  getUpdatedItemCost: function(itemId, itemType, newCost, oldCost) {
    var partialKeypair = getKeypair(itemType, itemId);
    if (!this.compareKeypairs(this.state.itemKeypair, partialKeypair, true) || this.state.itemCost != oldCost) {
      return this.state.itemCost;
    }
    return newCost;
  },

  onUserItemCostModified: function(event, data) {
    var newCost = this.getUpdatedItemCost(data.item.id, data.itemType, data.cost, data.oldCost);
    this.setState({
      itemCost: newCost
    });
    this.componentChanged();
  },

  resortUserItems: function(){
    stemcounter.resortUserItems( this.props.userId );
  },

  componentDidMount: function() {
    if ( ! stemcounter.page.userItemsSorted ) {
      this.resortUserItems();
    }
    jQuery(document).bind('stemcounter.userItemAdded', this.onUserItemAdded);
    jQuery(document).bind('stemcounter.userItemModified', this.onUserItemModified);
    jQuery(document).bind('stemcounter.userItemCostModified', this.onUserItemCostModified);
  },

  componentWillUnmount: function() {
    jQuery(document).unbind('stemcounter.userItemAdded', this.onUserItemAdded);
    jQuery(document).unbind('stemcounter.userItemModified', this.onUserItemModified);
    jQuery(document).unbind('stemcounter.userItemCostModified', this.onUserItemCostModified);
  },

  compareKeypairs: function(a, b, partial) {
    partial = (typeof partial == 'undefined') ? false : partial;
    a = a.split('|');
    b = b.split('|');

    if (partial) {
      if (a[0] == b[0] && a[1] == b[1]) {
        return true;
      }
    } else {
      if (a[0] == b[0] && a[1] == b[1] && a[2] == b[2]) {
        return true;
      }
    }

    return false;
  },

  getItemByIdPair: function(item_id, variation_id) {
    var item = false;

    if ( ! variation_id ) {
      item_id = item_id.split( '_' );
      variation_id = item_id[1];
      item_id = item_id[0];
    }

    if ( undefined !== stemcounter.page.userItemsIndex[ item_id.toString() ] ) {
      var idx = stemcounter.page.userItemsIndex[ item_id.toString() ];
      for (var j = 0; j < stemcounter.page.userItems[ idx ].variations.length; j++) {
        if ( variation_id == stemcounter.page.userItems[ idx ].variations[j].id && ( 1 == stemcounter.page.userItems[ idx ].variations.length || variation_id != stemcounter.page.userItems[ idx ].default_variation.id ) ) {
          item = {
            id: stemcounter.page.userItems[ idx ].id,
            deleted: false,
            name: stemcounter.page.userItems[ idx ].name,
            variationName: 'Default' != stemcounter.page.userItems[ idx ].variations[j].name ? stemcounter.page.userItems[ idx ].variations[j].name : '',
            itemVariationId: variation_id,
            cost: stemcounter.page.userItems[ idx ].variations[j].cost
          };

          if ( null == item.cost ) {
            item.cost = stemcounter.page.userItems[ idx ].default_variation.cost;
          }

          break;
        }
      }  
    }

    if ( ! item ) {
      return {
        id: this.state.itemId,
        deleted: true,
        name: this.state.itemName,
        itemVariationId: this.state.itemVariationId,
        variationName: this.state.itemVariationName,
        cost: this.state.itemCost
      };
    }

    return item;
  },

  getItemByKeypair: function(keypair) {
    var itemType = getItemTypeFromKeypair(keypair);
    var itemId = getItemIdFromKeypair(keypair);
    var item = stemcounter.page.userItems[ stemcounter.page.userItemsIndex[ itemId.toString() ] ];
    var variationDeleted = false;
    var variationId = getItemVariationIdFromKeypair( keypair );

    if ( undefined !== item &&
        undefined !== this.state.itemVariationName &&
        0 < this.state.itemVariationId &&
        item.variations &&
        item.variations.length ) {

      variationDeleted = true;
      _.each(item.variations, function(variation){
        if ( variation.id == variationId && ( 1 == item.variations.length || variationId != item.default_variation.id )) {
          variationDeleted = false;
          return false;
        }
      });
    }

    if (item === undefined || 
          (
            $.type( item.variations ) == 'object' &&
            this.state.itemVariationName !== undefined &&
            item.variations.length === 0 && 
            this.state.itemVariationName.length > 0
          ) ||
          variationDeleted
        ) {

      return item = {
        deleted: true,
        id: this.state.itemId,
        name: this.state.itemName,
        variationName: this.state.itemVariationName,
        itemCost: this.state.itemCost
      };
    } 
    return item;
  },

  onItemChange: function(_item){
    if ( null == _item ) {
      this.setState({
        itemKeypair: '',
        itemId: '0',
        itemCost: '0.00',
        canAddMore: false
      });

      return;
    }
    var newValue = _item.value;
    if ( newValue == this.state.itemKeypair ) {
      return;
    }

    if ( this.state.canAddMore ) {
      this.props.autoAddMore();
    }

    var item = this.getItemByIdPair( newValue );
    var lib_item_id = null;
    var userid = parseInt( _item.user_id );

    if ( userid ) {
      if ( this.props.userId != userid ) {
        if ( undefined !== stemcounter.page.userItemsIndex[ item.id.toString() ] ) {
          lib_item_id = parseInt(stemcounter.page.userItems[ stemcounter.page.userItemsIndex[ item.id.toString() ] ].id, 10);
        }

        jQuery(document).trigger('stemcounter.action.renderAddNewItemForm', [this, userid, lib_item_id, item.itemVariationId]);
      }
    }

    if ( item ) {
      this.setState({
        itemId: item.id,
        itemVariationId: item.itemVariationId,
        itemKeypair: item.id + '_' + item.itemVariationId,
        itemCost: item.cost,
        canAddMore: false
      });
    } else {
      this.setState({
        itemKeypair: '',
        itemId: '0',
        itemCost: '0.00',
        canAddMore: false
      });
    }

    if ( null === lib_item_id ) {
      this.componentChanged();
    }
  },

  onItemCostChange: function(event) {
    var value = stemcounter.parseFloatValue(event.target.value);

    this.setState({
      itemCost: value
    });

    this.componentChanged();
  },

  onItemQuantityChange: function(event) {
    var value = stemcounter.parseFloatValue(event.target.value);

    var stateUpdate = {
      itemQuantity: value
    }
    if (stateUpdate.itemQuantity > 0 && this.state.shy) {
      stateUpdate.shy = false;
    }

    this.setState(stateUpdate);

    this.componentChanged();
  },

  applyGlobalCost: function(event) {
    // removed for now
    event.preventDefault();
    var _this = this;

    var keypair = this.state.itemKeypair;
    if ( ! keypair ) {
      return;
    }

    // var itemType = getItemTypeFromKeypair( keypair );
    var item = this.getItemByIdPair( keypair );

    if ( ! item ) {
      return;
    }

    var oldCost = item.cost;
    item.cost = this.state.itemCost;

    jQuery(document).trigger( 'stemcounter.userItemCostModified', {
      // 'itemType': itemType,
      'item': item,
      'cost': item.cost,
      'oldCost': oldCost
    } );

    $.get( window.stemcounter.ajax_url, {
        action: 'sc_apply_item_cost',
        // item_type: itemType,
        item_id: item.id,
        variation_id: item.itemVariationId,
        cost: item.cost
    }, function(){
      _this.componentChanged();
    } );

    $(this.refs.itemCost)
      .stop(true, true)
      .css({
          'background-color': '#ACFFAC'
      })
      .animate({
          'background-color': '#FFFFFF'
      }, 1000);

    return;
  },

  editItem: function(event) {
    event.preventDefault();
    var itemId = getItemIdFromKeypair(this.state.itemKeypair);
    var variation_id = this.state.itemVariationId;

    jQuery(document).trigger('stemcounter.action.renderEditItemForm', [event, itemId, null, variation_id]);
  },

  removeItem: function(event) {
    if (event) {
      event.preventDefault();
    }
    
    if (this.props.onDelete) {
      this.props.onDelete(this);
    }
    
    this.componentChanged();
  },

  itemKeypairSelectRef: function(ref) {
    var _this = this;

    if (ref) {
      this.selectRef = ref;
      // $(ref.getDOMNode()).on('stemcounter.create_new_item', function(){
      //   jQuery(document).trigger('stemcounter.action.renderAddNewItemForm', _this);
      //   /*_this.setState({
      //     itemKeypair: '0'
      //   });*/
      // });
    } else {
      // $(this.selectRef.getDOMNode()).off('stemcounter.create_new_item');
    }
  },

  replaceSelect2: function(node, settings) {
    window.stemcounter.AddItemOptionSelect($(node), settings);
  },

  componentChanged: function(){
    if (typeof this.props.onChange != 'undefined') {
      this.props.onChange();
    }
  },

  formatItems: function( { focusedOption, focusOption, key, labelKey, option, selectValue, style, valueArray } ) {
    var className = [ 'VirtualizedSelectOption' ]

    if ( 'header' === option.type ) {
      className.push( 'item-header' );

      return (
        <div
          className={className.join(' ')}
          key={key}
          style={style}>
          {option[labelKey]}
        </div>
      );
    }

    if ( option === focusedOption ) {
      className.push('VirtualizedSelectFocusedOption');
    }

    if (option.disabled) {
      className.push('VirtualizedSelectDisabledOption');
    }

    if (valueArray && valueArray.indexOf(option) >= 0) {
      className.push('VirtualizedSelectSelectedOption');
    }

    if ( option.className ) {
      className.push(option.className);
    }

    var events = option.disabled
      ? {}
      : {
        onClick: () => selectValue(option),
        onMouseOver: () => focusOption(option)
      }

    return (
      <div
        className={className.join(' ')}
        key={key}
        style={style}
        title={option.title || option[labelKey]}
        {...events}
      >
        {option[labelKey]}
      </div>
    );
  },

  onAddItemButtonClick: function(e){
    e.preventDefault();
    $(document).trigger('stemcounter.action.renderAddNewItemForm', this);
  },

  render: function(){
    var measurement_unit = '';
    var removeButton = null;

    if (this.state.purchase_unit && stemcounter.page.measuring_units && stemcounter.page.measuring_units.length ) {
			for (var i = 0; i < stemcounter.page.measuring_units.length; i++) {
        var unit = stemcounter.page.measuring_units[i];
				if (this.state.purchase_unit == unit.id) {
          if ( 1 == parseFloat(this.state.itemQuantity) ) {
            measurement_unit = unit.short;
          } else {
            measurement_unit = unit.short_plural;
          }
          break;
				}
			}
		}

    if (this.props.deletable) {
      removeButton = (
          <a href="#" className="remove-arrangement-item" onClick={this.removeItem}> x</a>
      );
    }
    
    var editButton = null;
    if (this.state.itemKeypair != '') {
      //editButton = React.createElement("a", {href: "#", className: "apply-global-cost", onClick: this.editItem}, "Edit Master");
      editButton = (
        <a href="#" className="apply-global-cost" onClick={this.editItem}>Edit<br /> Master</a>
      );
    }

    var classes = ['row', 'row modalpane-row arrangement-item-row'];
    if (this.state.shy) {
      classes.push('shy-arrangement-item');
    }

    var _item = this.getItemByIdPair(this.state.itemId, this.state.itemVariationId);

    if ( _item.deleted && _item.id) {
      //If the item is deleted but still in the arrangement
      return (
        <div className={classes.join(' ')}>
          <input type="hidden" name={'items[' + this.props.index + '][shy]'} value={this.state.shy ? 'y' : 'n'} />
          <div className="col-xs-12 col-sm-5 item-select modalpane" >
            <div className="form-control">
              {this.state.itemName + this.state.itemVariationName}
            </div>
            <input type="hidden" name={'items[' + this.props.index + '][keypair]'} value={this.state.itemKeypair} />
          </div>
          <div className=" col-xs-7 col-sm-4 modalpane splittedpane">
            <div className="col-xs-6">
              <div className="itemCostDiv">
                <input type="text" name={'items[' + this.props.index + '][cost]'} ref="itemCost" readOnly={this.props.standaloneRecipes} value={this.state.itemCost} onChange={this.onItemCostChange} className="form-control flow_cost" />
              </div>
            </div> 
          </div>
          <div className="col-xs-5 col-sm-3 modalpane quantitypane">
             <input type="text" name={'items[' + this.props.index + '][quantity]'} value={this.state.itemQuantity} onChange={this.onItemQuantityChange} className="form-control flow_qty" />
              <span className="measurement_unit">{measurement_unit}</span>
             {removeButton}
          </div>
        </div>
      );
    }

    var button = <a href="#">Add New asfasfsa</a>;

    return (
      <div className={classes.join(' ')}>
        <input type="hidden" name={'items[' + this.props.index + '][shy]'} value={this.state.shy ? 'y' : 'n'} />
        <div className="col-xs-12 col-sm-5 item-select modalpane"> 
          <VirtualizedSelect
            clearable={false}
            scrollMenuIntoView={false}
            name={'items[' + this.props.index + '][keypair]'}
            className="form-control select-recipe-items"
            value={this.state.itemId + '_' + this.state.itemVariationId}
            onChange={this.onItemChange}
            ref={this.itemKeypairSelectRef}
            options={stemcounter.page.userItemsAsOptions}
            optionHeight={32}
            optionRenderer={stemcounter.formatItems}
            bottomButton="Add New"
            onBottomButtonClick={this.onAddItemButtonClick}
          />
        </div>
        <div className=" col-xs-7 col-sm-4 modalpane splittedpane">
          <div className="col-xs-6">
            <div className="itemCostDiv">
              <input type="text" name={'items[' + this.props.index + '][cost]'} ref="itemCost" readOnly={this.props.standaloneRecipes} value={this.state.itemCost} onChange={this.onItemCostChange} className="form-control flow_cost" />
            </div>
          </div>
          <div className="col-xs-6">
            {editButton}
          </div>
        </div> 
        <div className="col-xs-5 col-sm-3 modalpane quantitypane">
          <input type="text" name={'items[' + this.props.index + '][quantity]'} value={this.state.itemQuantity} onChange={this.onItemQuantityChange} className="form-control flow_qty" />
          <span className="measurement_unit">{measurement_unit}</span>
          {removeButton}
        </div>
        
      </div>
    );
  }
});

var ItemsListController = React.createClass({displayName: "ItemsListController",
  itemKeyIndex: 0,

  itemRefs: [],

  getInitialState: function() {
    var items = [];
    if (this.props.defaultValue.length > 0) {
      for (var i = 0; i < this.props.defaultValue.length; i++) {
        var item = this.props.defaultValue[i];
        items.push(this.generateNewItem(item, true));
      };
      items.push(this.generateNewItem(null, true));
    } else {
      items.push(this.generateNewItem(null, true));
    }

    return {
      items: items,
      recipe_name: this.props.recipe_name,
      UserRecipeIdSelected: this.props.UserRecipeIdSelected,
      allItemsButton: 'show'
    }
  },

  componentWillReceiveProps: function(nextProps){
    if (
      nextProps.defaultValue != this.props.defaultValue ||
      nextProps.recipe_name != this.state.recipe_name ||
      nextProps.UserRecipeIdSelected != this.state.UserRecipeIdSelected
    ) {
      var items = [];
      if ( nextProps.defaultValue.length > 0) {
        for (var i = 0; i < nextProps.defaultValue.length; i++) {
          var item = nextProps.defaultValue[i];
          items.push(this.generateNewItem(item, true));
        };
        items.push(this.generateNewItem(null, true));
      } else {
        items.push(this.generateNewItem(null, true));
      }

      this.setState({
        items: items,
        recipe_name: nextProps.recipe_name,
        UserRecipeIdSelected: nextProps.UserRecipeIdSelected
      });

      this.componentChanged();
    }
  },

  generateNewItem: function(value, deletable, shy) {
    deletable = (typeof deletable == 'undefined') ? true : deletable;
    shy = (typeof shy == 'undefined') ? false : shy;
    var item = (
      <ArrangementItem
        key={this.itemKeyIndex}
        defaultValue={value}
        deletable={deletable}
        index={this.itemKeyIndex}
        shy={shy}
        onChange={this.componentChanged}
        autoAddMore={this.autoAddMore}
        onDelete={this.itemOnDelete}
        currencySymbol={this.props.currencySymbol}
        userId={this.props.userId}
        standaloneRecipes={this.props.standaloneRecipes}
      />
    );

    this.itemKeyIndex ++;
    return item;
  },

  addMore: function() {
    var items = this.state.items.slice(); // clone
      items.push(this.generateNewItem());
      this.setState({
        items: items
      });

      this.componentChanged();
  },

  autoAddMore: function() {
    this.addMore();
  },

  getSimpleItemsList: function(){
    var itemsList = [];

    for (var i = 0; i < this.state.items.length; i++) {
      var arrangementItem = this.refs['item' + i.toString()];
      var item = arrangementItem.getItemByIdPair(arrangementItem.state.itemKeypair);
      if ( ! item ) {
        continue;
      }

      if ( arrangementItem.state.itemQuantity == 0 ) {
        continue;
      }

      var name = item.name;
      if ( item.variationName ) {
        name += ' - ' + item.variationName;
      }
      
      itemsList.push(name);
    };

    return itemsList;
  },

  componentChanged: function(){
    if (typeof this.props.onChange != 'undefined') {
      this.props.onChange();
    }
  },

  itemOnDelete: function(ref){
    var _this = this;

    var index = $(ReactDOM.findDOMNode(_this)).find('.list-root:first .arrangement-item-row').index(ReactDOM.findDOMNode(ref));
    var items = _this.state.items;
    items = items.slice(); // clone
    items.splice(index, 1);
    _this.setState({
      items: items
    });
  },

  showAllItems: function(event){
    event.preventDefault();

    var _this = this;

    _this.setState({
      allItemsButton: 'loading'
    });

    $.get(window.stemcounter.ajax_url, {
        action: 'sc_get_all_arrangement_items',
        event_id: _this.props.eventId
    }, function(response){
      stemcounter.JSONResponse(response, function(r) {
        var itemsAdded = _this.addShyItems(r.payload);

        if (itemsAdded > 0) {
          _this.setState({
            allItemsButton: 'hide'
          });
        } else {
          _this.setState({
            allItemsButton: 'no_items'
          });
        }    
      });
    });
  },

  hideAllItems: function(event){
    event.preventDefault();

    var _this = this;

    var items = [];
    for (var i = 0; i < this.state.items.length; i++) {
      var item = this.state.items[i];
      var arrangementItem = this.refs['item' + i.toString()];

      if (arrangementItem.state.shy && arrangementItem.state.itemQuantity == 0) {
        continue;
      }

      items.push(item);
    };

    _this.setState({
      allItemsButton: 'show',
      items: items
    });
  },

  addShyItems: function(shyItems){
    var items = this.state.items.slice(); // clone
    var signatures = [];

    for (var i = 0; i < items.length; i++) {
      var arrangementItem = this.refs['item' + i.toString()];
      var signature = arrangementItem.state.itemKeypair;
      
      if (signatures.indexOf(signature) != -1) {
        continue;
      }
      signatures.push(arrangementItem.state.itemKeypair);
    };

    var itemsAdded = 0;
    for (var i = 0; i < shyItems.length; i++) {
      var si = shyItems[i];

      var signature = si.item_id + '_' + si.item_variation_id;

      if ( signatures.indexOf( signature ) != -1) {
        continue;
      }

      items.push( this.generateNewItem( si, true, true ) );
      itemsAdded ++;
    };

    if (itemsAdded > 0) {
      this.setState({
        items: items
      });

      this.componentChanged();
    }

    return itemsAdded;
  },

  editUserRecipe: function(e) {
    e.preventDefault();

    var data = $('form.edit-arrangement-form:first').serialize();
    var ajax_url = window.stemcounter.aurl({ action: 'sc_edit_recipe' });

    $.post(ajax_url, data, function (response) {
      stemcounter.JSONResponse(response, function (r) {
        if (r.success) {
          alertify.success( 'Recipe Saved!' );
        } else {
          console.log( r );
        }
      });
    });
  },

  render: function(){
    var _this = this;
    var items = [];
    var user_recipes = null;    
    var recipes = $.extend(true, {}, this.props.userRecipes);

    for (var i = 0; i < this.state.items.length; i++) {
      var item = this.state.items[i];
      items.push(React.cloneElement(item, {ref: 'item' + i.toString()}));
    };

    var allItemsButton = '';
    if (this.state.allItemsButton == 'show' && ! this.props.standaloneRecipes) {
      allItemsButton = React.createElement("a", {href: "#", className: "show-all-arrangement-items-button", onClick: this.showAllItems}, "Show All");
    }
    if (this.state.allItemsButton == 'loading') {
      allItemsButton = React.createElement("img", {src: window.stemcounter.theme_url + '/img/ajax-loader.gif'});
    }
    if (this.state.allItemsButton == 'no_items') {
      allItemsButton = React.createElement("span", null, "No other items");
    }
    if (this.state.allItemsButton == 'hide') {
      allItemsButton = React.createElement("a", {href: "#", className: "show-all-arrangement-items-button", onClick: this.hideAllItems}, "Hide All");
    }

    return (
      <div className="clearfix">
        <div className="clearfix list-root">
          <div className="row clearfix modalpane-row col-sm-12">
            <label className="modalpane col-sm-6 control-label">
              <div className="lbl">Name</div>
              <input type="text" className="form-control" ref="recipename" placeholder="Add name here" name="recipe_name" defaultValue={this.state.recipe_name} />
            </label>
            {user_recipes}
          </div>
          <div className="row clearfix modalpane-row modalheaders hidden-xs">
            <div className="col-sm-5">
              <label>Items</label>
            </div> 
            <div className="col-sm-4">
              <label>{this.props.standaloneRecipes ? 'Current Cost' : 'Cost'}</label>
            </div>
            <div className="col-sm-3">
              <label>Quantity</label>
            </div>
          </div>
          {items}
        </div> 
        <p></p>
        <div className="row">
          <div className="container-fluid">
            <div className="">
              <div className="col-sm-12" style={{textAlign: "right"}}>
                {allItemsButton}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

var ArrangementNote = React.createClass({displayName: "ArrangementNote",
  label_text: 'Arrangement note',
  name: 'note',

  getInitialState: function(){
    return {
      value: this.props.defaultValue
    }
  },

  onChange: function(event){
    this.setState({
      value: event.target.value
    });
  },

  addItems: function(){
    var description = this.props.getDescription();
    var newItemsDescription = '---\n' + description.name + (description.items.length > 0 ? ' with ' + description.items.join(', ') : '') + '\n---';
    var descriptionRegex = /^---$((.|[\n\r]+)*?)^---$/m;

    var newValue = this.state.value;
    if (descriptionRegex.test(newValue)) {
      newValue = newValue.replace(descriptionRegex, newItemsDescription);
    } else {
      newValue += '\n\n' + newItemsDescription;
    }

    this.setState({
      value: newValue
    });
  },

  render: function() {
    return (
      <div className="form-group arrangement-note-group">
        <label className="col-sm-12 control-label">
          {this.label_text}
          <button type="button" className="mini-button" onClick={this.addItems}/> Add Description
        </label>
         <div className="col-sm-12">
          <textarea name={this.name} value={this.state.value} onChange={this.onChange} className="form-control"></textarea>
        </div>
      </div>
    );
  }
});

var ArrangementTotals = React.createClass({displayName: "ArrangementTotals",
  name: 'override_cost',

  getInitialState: function() {
    return {
      currencySymbol: this.props.currencySymbol,
      cost: this.props.cost,
      total: this.props.total,
      total_item_price: this.props.totalItemPrice,
      override: this.props.defaultValue
    };
  },

  formatPrice: function(value, addCurrency, currencySymbol) {
    value = stemcounter.formatPrice(value, addCurrency, currencySymbol);
    return value;
  },

  calculateNewTotals: function() {
    var _this = this;
    var url = window.stemcounter.aurl({action: 'sc_calculate_arrangement_totals'});
    var data = $(ReactDOM.findDOMNode(this)).closest('form.edit-arrangement-form').serialize();
    data += '&standaloneRecipes=' + ( this.props.standaloneRecipes ? '1' : '' );

    $.post(url, data, function(response) {
      stemcounter.JSONResponse(response, function(r) {
        _this.setState({
          cost: r.payload.cost,
          total: r.payload.total,
          total_item_price: r.payload.total_item_price
        });
      });
    });
  },

  render: function() {
    return (
      <div className= "form-group totals-wrap"> 
        <div className= "col-xs-9 input_fields_wrap totals-labels">
          <label className="control-label">Cost:</label>
          <label className="control-label">Total:</label>
          <label className="control-label">Total item price:</label>
          <label className="control-label">Item price override:</label>
        </div>
        <div className= "col-xs-3 input_fields_wrap totals-values">
          <label className="control-label">{this.formatPrice(this.state.cost, true, this.state.currencySymbol)}</label>
          <label className="control-label">{this.formatPrice(this.state.total, true, this.state.currencySymbol)}</label>
          <label className="control-label">{this.formatPrice(this.state.total_item_price, true, this.state.currencySymbol)}</label>
          <input type="text" name={this.name} defaultValue={this.state.override} placeholder={this.formatPrice(this.state.total_item_price, false)} className="form-control" />
        </div>
      </div>
    );
  }
});

jQuery(document).on('stemcounter.action.renderArrangementForm', function(e, data){
  ReactDOM.render(
    <EditArrangementForm
      values={data.values}
      eventId={data.eventId}
      userId={data.userId}
      layout="arrangement"
      itemTypes={data.itemTypes}
      categoriesList={data.categoriesList}
      categoryId={data.categoryId}
      itemsOnly={data.itemsOnly}
      recipe_id={data.recipe_id}
      recipe_name={data.recipe_name}
      userRecipes={data.user_recipes}
      UserRecipeIdSelected={data.user_recipe_id}
      deleteRecipe={stemcounter.page.deleteRecipe}
      standaloneRecipes={data.standaloneRecipes || false}
    />,
    jQuery('.edit-arrangement-form .form-shell:first').get(0)
  );
});

})();
