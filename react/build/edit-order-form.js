'use strict';

(function ($) {
	'use strict';

	var EventItem = React.createClass({
		displayName: 'EventItem',

		getInitialState: function getInitialState() {
			var state = {
				selectedValue: '-1',
				canAddMore: true
			};

			if (this.props.eventValues) {
				state.selectedValue = this.props.eventValues;
				if (state.selectedValue !== '-1') {
					state.canAddMore = false;
				}
			}

			return state;
		},

		onEventChange: function onEventChange(event) {
			var updatedState = JSON.parse(JSON.stringify(this.state));
			updatedState.selectedValue = event.target.value;

			if (updatedState.selectedValue === '-1') {
				updatedState.canAddMore = true;
			}

			if (this.state.canAddMore) {
				this.props.autoAddMore();
				updatedState.canAddMore = false;
			}

			this.setState(updatedState);
		},

		removeEventItem: function removeEventItem() {
			event.preventDefault();
			this.props.onDelete(this.props.reactKey);
		},

		render: function render() {
			var itemHtml = false;
			if (this.props.layout === 'add-order') {
				itemHtml = React.createElement(
					'div',
					{ className: 'eventItem' },
					React.createElement(
						'div',
						{ className: 'col-xs-10 col-sm-11' },
						React.createElement(ReactSelect2, {
							className: 'form-control eventItemValue',
							name: 'order_events[]',
							children: this.props.eventsList,
							selectedValue: this.state.selectedValue,
							onChange: this.onEventChange,
							replaceSelect2: this.replaceSelect2 })
					),
					React.createElement(
						'div',
						{ className: 'col-xs-1' },
						React.createElement(
							'a',
							{ href: '#', className: 'remove-order-item remove-arrangement-item', onClick: this.removeEventItem },
							React.createElement('i', { className: 'fa fa-trash-o' })
						)
					)
				);
			} else if (this.props.layout === 'edit-order') {
				var eventName = 'Deleted Event';
				var eventDate = '';
				var _this = this;

				this.props.rawEventsList.forEach(function (event, index, arr) {
					if (event.eventId == _this.state.selectedValue) {
						eventName = event.eventName;
						eventDate = ' - ' + event.eventDate;
					}
				});

				itemHtml = React.createElement(
					'div',
					{ className: 'eventItem' },
					React.createElement(
						'div',
						{ className: 'form-control col-xs-12' },
						eventName,
						eventDate
					)
				);
			}

			return itemHtml;
		}
	});

	var OrderModal = React.createClass({
		displayName: 'OrderModal',

		getInitialState: function getInitialState() {
			var state = {
				orderName: '',
				note: '',
				estimatedCost: 0,
				orderFulfilmentDate: '',
				eventItems: ['-1']
			};

			if (this.props.eventItems) state.eventItems = this.props.eventItems;
			if (this.props.orderName) state.orderName = this.props.orderName;
			if (this.props.orderNote) state.note = this.props.orderNote;
			if (this.props.orderFulfilmentDate) {
				state.orderFulfilmentDate = this.props.orderFulfilmentDate;
			}

			return state;
		},

		replaceSelect2: function replaceSelect2(node) {
			window.stemcounter.ScrollFix($(node), {});
		},

		handleChange: function handleChange(field, event) {
			var updatedState = {};

			if (field === 'name') {
				updatedState.orderName = event.target.value;
			} else if (field === 'note') {
				updatedState.note = event.target.value;
			}

			this.setState(updatedState);
		},

		componentDidUpdate: function componentDidUpdate() {
			if (this.props.layout == 'add-order') {
				var ids = [];

				$.each($('.eventItemValue'), function (index, value) {
					ids.push($(value).val());
				});

				if (ids.indexOf('-1') === -1) this.autoAddMore();
			}
		},

		componentDidMount: function componentDidMount() {
			if (this.refs.fulfilmentDate) {
				var _this = this;
				$(this.refs.fulfilmentDate).datepicker({
					dateFormat: this.props.dateFormat,
					onClose: function onClose(val, opts) {
						var updatedState = $.extend(true, {}, _this.state);
						updatedState.orderFulfilmentDate = val;
						_this.setState(updatedState);
					}
				});
			}
			stemcounter.unmountComponentOnModalClose(this);
		},

		autoAddMore: function autoAddMore() {
			var updatedState = JSON.parse(JSON.stringify(this.state));
			updatedState.eventItems.push('-1');
			this.setState(updatedState);
		},

		deleteEventItem: function deleteEventItem(index) {
			var index = index;
			var newEventItems = this.state.eventItems.slice();
			//fill with null to not let react get confused and remove the wrong components
			newEventItems.splice(index, 1, null);

			if (newEventItems.every(function (el, i, arr) {
				return el === null;
			})) {
				//Add event item if there are no more items visible
				newEventItems.push('-1');
			}

			this.setState({ eventItems: newEventItems });
		},

		submit: function submit(event) {
			event.preventDefault();
			if (this.props.layout === 'add-order') {
				var url = window.stemcounter.aurl({ action: 'sc_add_user_order' });
				var data = $('form.add-order-form:first').serialize();

				$.post(url, data, function (response) {
					stemcounter.JSONResponse(response, function (r) {
						if (r.success) document.location.reload();
					});
				});
			} else if (this.props.layout === 'edit-order') {
				var url = window.stemcounter.aurl({ action: 'sc_edit_user_order' });
				var data = $('form.edit-order-form:first').serialize();

				$.post(url, data, function (response) {
					stemcounter.JSONResponse(response, function (r) {
						if (r.success) document.location.reload();
					});
				});
			}
		},

		render: function render() {
			var formControl = React.createElement(
				'div',
				null,
				React.createElement(
					'button',
					{ className: 'btn btn-default btn-cancel', type: 'button', 'data-dismiss': 'modal' },
					'Cancel'
				),
				React.createElement(
					'button',
					{ className: 'btn btn-primary button-submit', type: 'submit', onClick: this.submit },
					'Save'
				)
			);

			var rawEventsList = [];
			var eventsList = this.props.eventsList.map(function (event, index, array) {
				rawEventsList.push({ eventId: event.id, eventName: event.name, eventDate: event.date });
				return React.createElement(
					'option',
					{ value: event.id, key: 'event-' + event.id + '-' + index },
					event.name,
					' - ',
					event.date
				);
			});
			eventsList.unshift(React.createElement(
				'option',
				{ value: '-1', key: 'event-na' },
				'Select Event'
			));

			return React.createElement(
				'div',
				null,
				React.createElement(
					'div',
					null,
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix pane-keep' },
						React.createElement(
							'label',
							{ className: 'col-xs-8 control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Name',
								React.createElement('span', { className: 'mandatory' })
							),
							React.createElement('input', { placeholder: 'Enter name here', className: 'form-control', type: 'text', name: 'order_name', value: this.state.orderName, onChange: this.handleChange.bind(this, 'name') }),
							React.createElement('input', { type: 'hidden', name: 'order_id', value: this.props.orderId })
						),
						React.createElement(
							'label',
							{ className: 'col-xs-4 modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Created At:'
							),
							React.createElement(
								'div',
								{ className: 'order-date' },
								this.props.orderDate
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix pane-keep' },
						React.createElement(
							'label',
							{ className: 'col-xs-12 modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Fulfilment Date:'
							),
							React.createElement('input', { className: 'order-fulfilment-date form-control', ref: 'fulfilmentDate', name: 'fulfilment_date', defaultValue: this.state.orderFulfilmentDate })
						)
					),
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix' },
						React.createElement(
							'label',
							{ className: 'col-sm-12 modalpane clearfix' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Booked Event'
							),
							React.createElement(
								'div',
								{ className: 'eventItems' },
								this.state.eventItems.map(function (value, index, arr) {
									if (value === null) return;

									return React.createElement(EventItem, { eventsList: eventsList, rawEventsList: rawEventsList, layout: this.props.layout, onDelete: this.deleteEventItem, key: index, reactKey: index, eventItems: this.state.eventItems, eventValues: value, autoAddMore: this.autoAddMore, ref: 'eventItemRef' });
								}, this)
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix' },
						React.createElement(
							'label',
							{ className: 'col-sm-12 control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Description'
							),
							React.createElement('textarea', { className: 'form-control', placeholder: 'Add notes', name: 'order_note', value: this.state.note, onChange: this.handleChange.bind(this, 'note') })
						)
					)
				),
				React.createElement(
					'div',
					{ className: 'modal-footer' },
					React.createElement(
						'div',
						{ className: 'pull-left hide' },
						'*required'
					),
					formControl
				)
			);
		}
	});

	$(document).on('stemcounter.action.renderOrderForm', function (e, settings) {
		ReactDOM.render(React.createElement(OrderModal, {
			layout: settings.layout,
			eventsList: settings.eventsList,
			orderId: settings.orderId,
			orderName: settings.orderName,
			orderNote: settings.orderNote,
			orderDate: settings.orderDate,
			eventItems: settings.eventItems,
			orderFulfilmentDate: settings.orderFulfilmentDate
		}), $(settings.node).get(0));
	});
})(jQuery);