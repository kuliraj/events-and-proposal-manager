'use strict';

(function ($) {
	'use strict';

	var SettingsForm = React.createClass({
		displayName: 'SettingsForm',

		getInitialState: function getInitialState() {
			this.submit = _.debounce(this.submit, 400);
			this.getFontOptions();

			return {
				color: this.props.color,
				style: this.props.style,
				font: this.props.font

			};
		},

		onFormSubmit: function onFormSubmit(e) {
			e.preventDefault();
			this.submit();
		},

		submit: function submit() {
			var $form = $(this.refs.form);
			var th = this;

			$.post($form.attr('action'), $form.serialize()).always(function (response) {
				stemcounter.JSONResponse(response, function (r) {
					$('style#studio-whitelabel-css').remove();

					if (r.payload.whitelabel_css) {
						$('head').append(r.payload.whitelabel_css);
					}
				}.bind(th));
			});
		},

		onColorChange: function onColorChange(color) {
			this.setState({
				color: color
			});

			this.submit();
		},

		onLogoColorPickerModalSave: function onLogoColorPickerModalSave($modal) {
			var color = $modal.find('.color-input').val();

			this.setState({
				color: color
			}, this.submit);
		},

		openLogoColorPickerModal: function openLogoColorPickerModal() {
			var $modalWrapper = GenericModal.prototype.createWrapper('new-modal logo-color-picker-modal'),
			    modalContent = React.createElement(LogoColorPickerModal, {
				logo: this.props.logo,
				color: this.state.color
			});

			ReactDOM.render(React.createElement(GenericModal, {
				title: 'Pick Color',
				content: modalContent,
				onSave: this.onLogoColorPickerModalSave,
				saveLabel: 'Set Color'
			}), $modalWrapper[0]);
		},

		getStyleOptions: function getStyleOptions() {
			var style = [];
			style.push({
				label: 'Default',
				value: ''
			});
			style.push({
				label: 'Proposal Option 2',
				value: '2'
			});

			return style;
		},

		getFontOptions: function getFontOptions() {
			var fonts = [];
			fonts.push({
				label: 'Default',
				value: '',
				className: 'pt-sans-caption'
			});
			fonts.push({
				label: 'Arimo',
				value: 'arimo',
				className: 'arimo'
			});
			fonts.push({
				label: 'Cormorant Garamond',
				value: 'cormorant-garamond',
				className: 'cormorant-garamond'
			});
			fonts.push({
				label: 'Patua One',
				value: 'patua-one',
				className: 'patua-one'
			});
			fonts.push({
				label: 'Courgette',
				value: 'courgette',
				className: 'courgette'
			});

			return fonts;
		},

		onFontChange: function onFontChange(option) {
			this.setState({
				font: option.value
			}, this.submit);
		},

		onStyleChange: function onStyleChange(option) {
			this.setState({
				style: option.value
			}, this.submit);
		},

		render: function render() {
			console.log();
			var url = stemcounter.aurl({
				'action': 'sc_studio_settings'
			});

			return React.createElement(
				'form',
				{ action: url, method: 'post', onSubmit: this.onFormSubmit, ref: 'form' },
				React.createElement(
					'div',
					{ className: 'pane-row' },
					React.createElement(
						'div',
						{ className: 'pane color-pane' },
						React.createElement(
							'label',
							{ className: 'control-label' },
							'Color'
						),
						React.createElement(ColorPickerContainer, { color: this.state.color, name: 'proposalColor', onChange: this.onColorChange, className: 'value' }),
						this.props.logo ? React.createElement(
							'div',
							{ className: 'value' },
							React.createElement(
								'small',
								null,
								'Not sure which color to pick? ',
								React.createElement(
									'a',
									{ href: '#', className: 'sc-primary-color', onClick: this.openLogoColorPickerModal },
									'Pick one from your logo'
								),
								'!'
							)
						) : null
					),
					React.createElement(
						'div',
						{ className: 'pane label-pane' },
						React.createElement(
							'label',
							{ className: 'control-label' },
							'White Label'
						),
						React.createElement(
							'div',
							{ className: 'value' },
							React.createElement(
								'label',
								null,
								React.createElement('input', { defaultValue: 'yes', defaultChecked: Boolean(this.props.white_label_acc), name: 'choiseLogo', type: 'checkbox', onChange: this.submit }),
								' White label my account!'
							)
						)
					)
				)
			);
		}
	});

	var LogoColorPickerModal = React.createClass({
		displayName: 'LogoColorPickerModal',

		getInitialState: function getInitialState() {
			return {
				color: this.props.color,
				newColor: false
			};
		},

		onModalShown: function onModalShown() {
			var img = new Image(),
			    maxWidth = $(this.canvas).parent().width(),
			    maxHeight,
			    rWidth,
			    rHeight,
			    nWidth,
			    nHeight,
			    ratio;

			img.crossOrigin = 'Anonymous';
			img.onload = function () {
				this.canvasCtx.drawImage(img, 0, 0, this.props.logo.w, this.props.logo.h, 0, 0, this.canvas.width, this.canvas.height);
			}.bind(this);

			if (maxWidth > screen.width && screen.width > 0) {
				maxWidth = screen.width;
			}

			maxHeight = Math.min(550, maxWidth);
			rWidth = 1;
			rHeight = 1;

			if (this.props.logo.w > maxWidth) {
				rWidth = maxWidth / this.props.logo.w;
			}
			if (this.props.logo.h > maxHeight) {
				rHeight = maxHeight / this.props.logo.h;
			}

			ratio = Math.min(rWidth, rHeight);
			nWidth = this.props.logo.w * ratio;
			nHeight = this.props.logo.h * ratio;
			this.canvas.width = nWidth;
			this.canvas.height = nHeight;
			this.canvas.style.width = nWidth + 'px';
			this.canvas.style.height = nHeight + 'px';

			img.src = this.props.logo.src + '?' + new Date().getTime();
		},

		componentDidMount: function componentDidMount() {
			$(ReactDOM.findDOMNode(this)).closest('.modal').one('shown.bs.modal', this.onModalShown);
		},

		canvasRef: function canvasRef(ref) {
			this.canvas = ref;
			this.canvasCtx = this.canvas ? this.canvas.getContext('2d') : false;
		},

		onCanvasMouseMove: function onCanvasMouseMove(e) {
			var x = e.nativeEvent.offsetX,
			    y = e.nativeEvent.offsetY;

			var cData = this.canvasCtx.getImageData(x, y, 1, 1).data;
			this.setState({
				newColor: stemcounter.rgb2hex(cData[0], cData[1], cData[2])
			});
		},

		onCanvasClick: function onCanvasClick(e) {
			var x = e.nativeEvent.offsetX,
			    y = e.nativeEvent.offsetY;

			var cData = this.canvasCtx.getImageData(x, y, 1, 1).data;
			this.setState({
				color: stemcounter.rgb2hex(cData[0], cData[1], cData[2])
			});
		},

		render: function render() {
			return React.createElement(
				'div',
				{ className: '' },
				React.createElement(
					'div',
					{ className: 'modalpane-row clearfix pane-keep' },
					React.createElement(
						'label',
						{ className: 'col-xs-12 control-label modalpane' },
						React.createElement(
							'div',
							{ className: 'lbl' },
							'Click anywhere on your logo to select a color'
						)
					),
					React.createElement(
						'div',
						{ className: 'col-xs-12 modalpane' },
						React.createElement('canvas', { ref: this.canvasRef, onMouseMove: this.onCanvasMouseMove, onClick: this.onCanvasClick })
					)
				),
				React.createElement(
					'div',
					{ className: 'modalpane-row clearfix pane-keep' },
					React.createElement(
						'label',
						{ className: 'col-xs-6 control-label modalpane' },
						React.createElement(
							'div',
							{ className: 'lbl' },
							'Current Color'
						),
						React.createElement('div', { className: 'color-sample', style: { backgroundColor: this.state.color ? this.state.color : 'transparent' } })
					),
					React.createElement(
						'label',
						{ className: 'col-xs-6 control-label modalpane' },
						React.createElement(
							'div',
							{ className: 'lbl' },
							'New Color'
						),
						React.createElement('div', { className: 'color-sample', style: { backgroundColor: this.state.newColor ? this.state.newColor : 'transparent' } })
					)
				),
				React.createElement('input', { type: 'hidden', className: 'color-input', value: this.state.color, onChange: $.noop })
			);
		}
	});

	$(document).on('stemcounter.action.renderSettingsForm', function (e, settings) {
		ReactDOM.render(React.createElement(SettingsForm, {
			color: settings.color,
			white_label_acc: settings.white_label_acc,
			logo: settings.logo,
			style: settings.style,
			font: settings.font
		}), settings.node.get(0));
	});
})(jQuery);