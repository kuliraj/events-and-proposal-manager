'use strict';

(function ($) {
	'use strict';

	var Onboarding = React.createClass({
		displayName: 'Onboarding',

		getInitialState: function getInitialState() {
			var state = {
				steps: [],
				currentStepNumber: 0,
				doingAjax: false,
				showLabor: false,
				values: {
					floralMarkup: 1,
					hardgoodMarkup: 1,
					labor: 0,
					tax: 0,
					currency: 'USD'
				}
			};

			//fill state object with props
			for (var prop in this.props) {
				if (this.props.hasOwnProperty(prop) && this.props[prop] !== undefined && this.props[prop] !== null) {
					state[prop] = this.props[prop];
				}
			}

			return state;
		},

		componentDidUpdate: function componentDidUpdate() {
			if ($('.tutorial-tooltip').length) {
				$('.tutorial-tooltip').tooltip();
			}
		},

		componentDidMount: function componentDidMount() {
			if ($('.tutorial-tooltip').length) {
				$('.tutorial-tooltip').tooltip();
			}

			var $progressbarprogress = $('.logo-progress-bar');
			var uploader = new plupload.Uploader({
				runtimes: 'html5,flash,silverlight,html4',

				browse_button: 'new-logo',

				url: stemcounter.ajax_url + '?action=sc_edit_logo',

				filters: {
					max_file_size: '10mb',
					mime_types: [{ title: "Image files", extensions: "jpg,jpeg,png" }]
				},
				init: {
					FilesAdded: function FilesAdded(up, files) {
						uploader.start();
						$('#new-logo').val(files[0].name);
					},
					UploadProgress: function UploadProgress(up, file) {
						if (file.percent < 70 && file.percent > 20) {
							$progressbarprogress.css('width', file.percent.toString() + '%');
						}
					},
					Error: function Error(up, err) {
						alert('There was an error with your file. Please try a different or a smaller one.');
					},
					FileUploaded: function FileUploaded(up, file, r) {
						$progressbarprogress.css('width', '100%');
					},
					UploadFile: function UploadFile(up, file) {
						$progressbarprogress.css('width', '20%');
					}
				}
			});

			uploader.init();
		},

		handleChange: function handleChange(field, event) {
			var updatedState = JSON.parse(JSON.stringify(this.state));

			switch (field) {
				case 'labor':
					updatedState.values.labor = stemcounter.parseFloatValue(event.target.value);
					break;
				case 'tax':
					updatedState.values.tax = stemcounter.parseFloatValue(event.target.value);
					break;
				case 'floralMarkup':
					updatedState.values.floralMarkup = stemcounter.parseFloatValue(event.target.value);
					break;
				case 'hardgoodMarkup':
					updatedState.values.hardgoodMarkup = stemcounter.parseFloatValue(event.target.value);
					break;
				case 'currency':
					updatedState.values.currency = event.target.value;
					break;
				case 'firstname':
					updatedState.values.firstname = event.target.value;
					break;
				case 'lastname':
					updatedState.values.lastname = event.target.value;
					break;
				case 'companyname':
					updatedState.values.companyname = event.target.value;
					break;
				case 'infoAboutUs':
					updatedState.values.infoAboutUs = event.target.value;
					break;
				case 'password':
					updatedState.values.password = event.target.value;
					break;
				case 'password-repeat':
					updatedState.values.password_repeat = event.target.value;
					break;
				case 'librarian':
					updatedState.values.sc_user_libraries = this.state.values.sc_user_libraries ? this.state.values.sc_user_libraries : [];
					var pos = updatedState.values.sc_user_libraries.indexOf(event.target.value);
					var _value = event.target.value;
					var _this = this;

					if (-1 == pos) {
						alertify.confirm('I understand that items imported into my library are only suggested initial prices to make my StemCounter setup easier. Prices ordered from Mayesh or other wholesalers will vary depending on the time of the year or seasonality of the product.', function () {
							// Yes
							updatedState.values.sc_user_libraries.push(_value);
							_this.setState(updatedState);
						}, function () {
							// No
						});
					} else {
							updatedState.values.sc_user_libraries.splice(pos, 1);
						}
					break;
				default:
					break;
			}
			this.setState(updatedState);
		},

		submitInformation: function submitInformation(callback) {
			var url = window.stemcounter.aurl({ action: 'sc_user_onboarding_step' });
			var data = $('form.onboarding-info:first').serialize();
			var _this = this;

			$.post(url, data, function (response) {
				//var parsedResponse = JSON.parse(response); ?????
				var parsedResponse = response;

				stemcounter.JSONResponse(response, function (r) {
					if (r.success) callback();
				});

				if (parsedResponse.success === false) {
					_this.setState({ doingAjax: false });
				}
			});
		},

		nextStep: function nextStep(type, event) {

			if (type !== 'video') event.preventDefault();
			if (type === 'event') {
				var url = window.stemcounter.aurl({
					action: 'sc_edit_event_form',
					id: 0
				});

				stemcounter.openAjaxModal('Add Event', url, 'new-modal');
				return;
			}

			var _this = this;
			var updatedState = JSON.parse(JSON.stringify(_this.state));

			if (_this.state.doingAjax === false) {
				updatedState.doingAjax = true;

				_this.setState(updatedState);

				_this.submitInformation(function () {
					$('.onboarding-content').fadeOut('400', function () {
						updatedState.doingAjax = false;
						updatedState.currentStepNumber += 1;

						_this.setState(updatedState);

						$(this).fadeIn('400');
					});
				});
			}
		},

		laborChange: function laborChange(type, e) {
			if (type === 'percent') {
				this.setState({ showLabor: true });
			} else {
				this.setState({ showLabor: false });
			}
		},

		renderStep: function renderStep() {
			var stepHtml,
			    currentStep = this.state.steps[this.state.currentStepNumber];

			switch (currentStep.type) {
				case 'account':
					stepHtml = React.createElement(
						'div',
						{ className: 'onboarding-content' },
						React.createElement(
							'div',
							{ className: 'step-count text-right' },
							'Step 1/3'
						),
						React.createElement(
							'h3',
							{ className: 'onbarding-title text-center' },
							currentStep.message
						),
						React.createElement(
							'form',
							{ className: 'onboarding-info' },
							React.createElement(
								'div',
								{ className: 'form-group' },
								React.createElement(
									'label',
									{ htmlFor: 'business-name', className: 'col-sm-2 control-label' },
									'Business Name*:'
								),
								React.createElement(
									'div',
									{ className: 'col-sm-4 col-lg-3' },
									React.createElement('input', { onChange: this.handleChange.bind(this, 'companyname'), id: 'business-name', name: 'onboarding[account][business_name]', value: this.state.values.companyname, className: 'form-control', type: 'text' })
								),
								React.createElement(
									'label',
									{ htmlFor: 'business-website', className: 'col-sm-2 control-label align-right' },
									'Business Website:'
								),
								React.createElement(
									'div',
									{ className: 'col-sm-4 col-lg-3' },
									React.createElement('input', { id: 'business-website', name: 'onboarding[account][business_website]', className: 'form-control', type: 'text' })
								)
							),
							React.createElement('br', null),
							React.createElement(
								'div',
								{ className: 'form-group' },
								React.createElement(
									'label',
									{ className: 'col-sm-2 control-label' },
									'Date Format:'
								),
								React.createElement(
									'div',
									{ className: 'col-sm-4 col-lg-3' },
									React.createElement(
										'select',
										{ className: 'form-control', name: 'onboarding[account][date_format]', id: 'dateFormat' },
										React.createElement(
											'option',
											{ value: 'm/d/Y', selected: 'selected' },
											'M/D/Y (e.g. 03/21/2015)'
										),
										React.createElement(
											'option',
											{ value: 'm.d.Y' },
											'M.D.Y (e.g. 07.31.2015)'
										),
										React.createElement(
											'option',
											{ value: 'd/m/Y' },
											'D/M/Y (e.g. 14/09/2015)'
										),
										React.createElement(
											'option',
											{ value: 'd.m.Y' },
											'D.M.Y (e.g. 16.05.2015)'
										)
									)
								),
								React.createElement(
									'label',
									{ className: 'col-sm-2 control-label align-right' },
									'Currency:'
								),
								React.createElement(
									'div',
									{ className: 'col-sm-4 col-lg-3' },
									React.createElement(
										'select',
										{ className: 'form-control', name: 'onboarding[account][pref_currency]', id: 'prefCurrency', onChange: this.handleChange.bind(this, 'currency') },
										React.createElement(
											'option',
											{ value: 'AUD' },
											'AUD - Australian Dollar'
										),
										React.createElement(
											'option',
											{ value: 'CAD' },
											'CAD - Canadian Dollar'
										),
										React.createElement(
											'option',
											{ value: 'EUR' },
											'EUR - Euro'
										),
										React.createElement(
											'option',
											{ value: 'GBP' },
											'GBP - British Pound'
										),
										React.createElement(
											'option',
											{ value: 'AED' },
											'AED - United Arab Emirates'
										),
										React.createElement(
											'option',
											{ value: 'ZAR' },
											'ZAR - South African Rand'
										),
										React.createElement(
											'option',
											{ value: 'USD', selected: 'selected' },
											'USD - United States Dollar'
										)
									)
								)
							),
							React.createElement('br', null),
							React.createElement(
								'div',
								{ className: 'form-group' },
								React.createElement(
									'label',
									{ htmlFor: 'new-logo', className: 'col-sm-2 control-label' },
									'Logo:'
								),
								React.createElement(
									'div',
									{ className: 'col-sm-4 col-lg-3' },
									React.createElement(
										'div',
										{ className: 'logo-container' },
										React.createElement('span', { className: 'logo-progress-bar' }),
										React.createElement('input', { id: 'new-logo', type: 'button', name: 'onboarding[account][logo]', className: 'form-control', value: 'Choose File' })
									)
								)
							),
							React.createElement('br', null),
							React.createElement(
								'div',
								{ className: 'row' },
								React.createElement('div', { className: 'hidden-xs col-sm-3 col-md-4' }),
								React.createElement(
									'div',
									{ className: 'col-sm-6 col-md-4' },
									React.createElement(
										'div',
										{ className: 'form-group' },
										React.createElement(
											'p',
											{ className: 'text-center' },
											'Where did you hear about us?*'
										),
										React.createElement(
											'div',
											{ className: 'text-center' },
											React.createElement('input', { name: 'onboarding[account][info]', type: 'text', className: 'form-control', onChange: this.handleChange.bind(this, 'infoAboutUs'), value: this.state.values.infoAboutUs })
										)
									)
								),
								React.createElement('div', { className: 'col-sm-3 col-md-4' })
							)
						),
						React.createElement(
							'div',
							{ className: 'form-group next-step clearfix' },
							React.createElement(
								'a',
								{ className: 'btn btn-primary', onClick: this.nextStep.bind(this, 'account') },
								'Next Step'
							)
						)
					);

					break;
				case 'invoice':
					var laborSettings = false;
					var librarians = $.extend(true, {}, this.state.values.librarians);

					if (this.state.showLabor) {
						laborSettings = React.createElement(
							'div',
							null,
							React.createElement(
								'label',
								{ htmlFor: 'new-labor-percent', className: 'col-sm-12 control-label' },
								'Labor %*: ',
								React.createElement(
									'span',
									{ className: 'tutorial-tooltip', title: 'If you calculate labor by using 15% of the subtotal, you can enter 15 here. If you do not use a percentage or you build it into the arrangements, just write 0 here.' },
									' What\'s this?'
								)
							),
							React.createElement(
								'div',
								{ className: 'col-sm-12 col-lg-12' },
								React.createElement('input', { id: 'new-labor-percent', name: 'onboarding[invoice][labor_percent]', className: 'form-control', type: 'text', onChange: this.handleChange.bind(this, 'labor'), value: this.state.values.labor })
							),
							React.createElement('br', null),
							React.createElement(
								'div',
								{ className: 'apply-labor-to col-sm-12 col-lg-12' },
								'Apply Labor To:',
								React.createElement('br', null),
								React.createElement(
									'label',
									null,
									React.createElement('input', { type: 'checkbox', name: 'onboarding[invoice][floral_selected]' }),
									' Flower'
								),
								React.createElement(
									'label',
									null,
									React.createElement('input', { type: 'checkbox', name: 'onboarding[invoice][hardgood_selected]' }),
									' Hardgood'
								)
							)
						);
					}

					stepHtml = React.createElement(
						'div',
						{ className: 'onboarding-content' },
						React.createElement(
							'div',
							{ className: 'step-count text-right' },
							'Step 2/3'
						),
						React.createElement(
							'h3',
							{ className: 'onbarding-title text-center' },
							currentStep.message
						),
						React.createElement(
							'form',
							{ className: 'onboarding-info' },
							React.createElement(
								'div',
								{ className: 'form-group' },
								React.createElement(
									'label',
									{ htmlFor: 'new-floral-markup', className: 'col-sm-2 control-label' },
									'Floral Markup*:'
								),
								React.createElement(
									'div',
									{ className: 'col-sm-3 col-lg-3' },
									React.createElement('input', { id: 'new-floral-markup', name: 'onboarding[invoice][floral_markup]', className: 'form-control', type: 'text', value: this.state.values.floralMarkup, onChange: this.handleChange.bind(this, 'floralMarkup') })
								),
								React.createElement(
									'span',
									{ className: 'tutorial-tooltip col-sm-1 control-label', title: 'This multiplies the cost of your floral items by the entered number. Many florists use 2.8 or 3 as their standard fresh multiple.' },
									' What\'s this?'
								),
								React.createElement('div', { className: 'col-sm-1' }),
								React.createElement(
									'div',
									{ className: 'col-sm-5' },
									React.createElement(
										'p',
										null,
										'A ',
										stemcounter.formatPrice(1, true, this.state.values.currency),
										' rose will end up being ',
										stemcounter.formatPrice(this.state.values.floralMarkup, true, this.state.values.currency),
										' before labor.'
									)
								)
							),
							React.createElement('br', null),
							React.createElement(
								'div',
								{ className: 'form-group' },
								React.createElement(
									'label',
									{ htmlFor: 'new-hardgood-markup', className: 'col-sm-2 control-label align-right' },
									'Hardgood Markup*:'
								),
								React.createElement(
									'div',
									{ className: 'col-sm-3 col-lg-3' },
									React.createElement('input', { id: 'new-hardgood-markup', name: 'onboarding[invoice][hardgood_markup]', className: 'form-control', type: 'text', value: this.state.values.hardgoodMarkup, onChange: this.handleChange.bind(this, 'hardgoodMarkup') })
								),
								React.createElement(
									'span',
									{ className: 'tutorial-tooltip col-sm-1 control-label', title: 'This multiplies the cost of your hardgoods by the entered number. Many florists use 2 as their standard hardgood multiple.' },
									'What\'s this?'
								),
								React.createElement('div', { className: 'col-sm-1' }),
								React.createElement(
									'div',
									{ className: 'col-sm-5' },
									React.createElement(
										'p',
										null,
										'A ',
										stemcounter.formatPrice(1, true, this.state.values.currency),
										' floral foam will end up being ',
										stemcounter.formatPrice(this.state.values.hardgoodMarkup, true, this.state.values.currency),
										' before labor.'
									)
								)
							),
							React.createElement('br', null),
							React.createElement(
								'div',
								{ className: 'form-group' },
								React.createElement(
									'label',
									{ htmlFor: 'new-tax-rate', className: 'col-sm-2 control-label align-right' },
									'Tax Rate %*:'
								),
								React.createElement(
									'div',
									{ className: 'col-sm-4 col-lg-3' },
									React.createElement('input', { id: 'new-tax-rate', name: 'onboarding[invoice][tax_rate]', className: 'form-control', placeholder: '8.60', type: 'text', onChange: this.handleChange.bind(this, 'tax'), value: this.state.values.tax }),
									React.createElement(
										'p',
										null,
										'Input this as a percentage. A tax rate of 0.087 should be written as 8.7.'
									)
								)
							),
							React.createElement('br', null),
							React.createElement(
								'div',
								{ className: 'form-group' },
								React.createElement(
									'div',
									{ className: 'col-sm-2' },
									'Flower libraries:'
								),
								React.createElement(
									'div',
									{ className: 'col-sm-4' },
									Object.keys(librarians).map(function (key) {
										var librarian = librarians[key].data;
										var checked = -1 == this.state.values.sc_user_libraries.indexOf(librarian.ID) ? false : true;
										return React.createElement(
											'div',
											{ key: 'lib-' + librarian.ID, className: 'librarian' },
											React.createElement(
												'label',
												null,
												React.createElement('input', { type: 'checkbox', name: 'onboarding[invoice][sc_user_libraries][]', checked: checked, value: librarian.ID, onChange: this.handleChange.bind(this, 'librarian') }),
												'  ',
												librarian.display_name
											)
										);
									}.bind(this))
								),
								React.createElement(
									'div',
									{ className: 'col-sm-6' },
									React.createElement(
										'p',
										{ className: 'text-center' },
										'When calculating labor, in most cases...'
									),
									React.createElement(
										'div',
										{ className: 'labor-wrapper' },
										React.createElement(
											'label',
											null,
											React.createElement('input', { name: 'onboarding[invoice][labor]', type: 'radio', onChange: this.laborChange.bind(this, 'percent') }),
											'I use a %.'
										),
										laborSettings,
										React.createElement('br', null),
										React.createElement(
											'label',
											null,
											React.createElement('input', { name: 'onboarding[invoice][labor]', type: 'radio', onChange: this.laborChange.bind(this, 'set') }),
											'I use a set fee.'
										)
									)
								)
							)
						),
						React.createElement(
							'div',
							{ className: 'form-group next-step clearfix' },
							React.createElement(
								'a',
								{ className: 'btn btn-primary', onClick: this.nextStep.bind(this, 'invoice') },
								'Next Step'
							)
						)
					);
					break;
				case 'event':
					stepHtml = React.createElement(
						'div',
						{ className: 'onboarding-content' },
						React.createElement(
							'div',
							{ className: 'step-count text-right' },
							'Step 3/3'
						),
						React.createElement(
							'h3',
							{ className: 'onbarding-title text-center' },
							currentStep.message
						),
						React.createElement(
							'div',
							{ className: 'finish-onboarding' },
							React.createElement(
								'a',
								{ className: 'btn btn-primary', onClick: this.nextStep.bind(this, 'event') },
								'Create my first event'
							)
						),
						React.createElement(
							'div',
							{ className: 'onboarding-tip' },
							'Pro Tip: New users get the best test when using an actual event.'
						)
					);
					break;

			}

			return stepHtml;
		},

		render: function render() {
			return React.createElement(
				'div',
				null,
				this.renderStep()
			);
		}
	});

	$(document).on('stemcounter.action.renderOnboarding', function (e, settings) {
		ReactDOM.render(React.createElement(Onboarding, {
			steps: settings.steps,
			currentStepNumber: parseInt(settings.currentStepNumber),
			values: settings.values
		}), $(settings.node).get(0));
	});
})(jQuery);