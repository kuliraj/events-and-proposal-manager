'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function ($) {
	"use strict";

	var ViewSubmission = function (_SC_Component) {
		_inherits(ViewSubmission, _SC_Component);

		function ViewSubmission(props) {
			_classCallCheck(this, ViewSubmission);

			var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ViewSubmission).call(this, props));

			var state = {};
			state.loading = true;

			_this.state = state;
			return _this;
		}

		_createClass(ViewSubmission, [{
			key: 'componentDidMount',
			value: function componentDidMount() {
				$.get(stemcounter.ajax_url, {
					action: 'sc/form/submission/get',
					submission_id: this.props.submission
				}, this.onSubmissionFetched);
			}
		}, {
			key: 'onSubmissionFetched',
			value: function onSubmissionFetched(r) {
				if (!r.success) {
					alertify.error(r.message);
					this.closeModal();

					return;
				}

				var submission = r.payload.submission;
				var answers;
				var submission_answers = [];
				// This consolidates all the answers to multi-option questions into a single answer object
				for (var i = 0; i < submission.answers.length; i++) {
					if (undefined === _.findWhere(submission_answers, { question_id: submission.answers[i].question_id })) {
						answers = _.pluck(_.where(submission.answers, { question_id: submission.answers[i].question_id }), 'answer');
						submission.answers[i].answer = answers;
						submission_answers.push($.extend(true, {}, submission.answers[i]));
					}
				}

				submission.answers = submission_answers;

				$('.form-submissions-table tr[data-submission="' + this.props.submission + '"]').removeClass('not-seen');

				this.setState({
					loading: false,
					submission: submission
				});
			}
		}, {
			key: 'closeModal',
			value: function closeModal() {}
		}, {
			key: 'render',
			value: function render() {
				if (this.state.loading) {
					return React.createElement(
						'div',
						{ className: 'ajax-modal-loader' },
						React.createElement('i', { className: 'fa fa-spinner fa-pulse fa-3x fa-fw' }),
						React.createElement(
							'span',
							{ className: 'sr-only' },
							'Loading...'
						)
					);
				}

				return React.createElement(
					'div',
					{ className: 'form-layout' },
					this.state.submission.answers.map(function (answer) {
						return React.createElement(
							'div',
							{ key: 'answer-' + answer.id, className: 'modalpane-row clearfix' },
							React.createElement(
								'label',
								{ htmlFor: '', className: 'col-xs-12 modalpane control-label' },
								React.createElement(
									'div',
									{ className: 'lbl' },
									answer.label
								),
								answer.answer.map(function (answer, i) {
									return React.createElement(
										'div',
										{ key: 'answer-' + i, className: 'value' },
										answer
									);
								})
							)
						);
					})
				);
			}
		}]);

		return ViewSubmission;
	}(SC_Component);

	$(document).ready(function () {
		$('.form-submissions-table').on('click', 'tbody tr .archive-submission', function (e) {
			e.preventDefault();
			e.stopImmediatePropagation();

			var submission = $(this).closest('tr').attr('data-submission');

			alertify.confirm('Are you sure you want to archive this submission?', function () {
				$.post(stemcounter.ajax_url, {
					action: 'sc/form/submission/archive',
					submission: submission
				}, function (r) {
					if (r.success) {
						$('.form-submissions-table tr[data-submission="' + submission + '"]').remove();
						alertify.success('The submission was archived successfully.');
					} else {
						alertify.error(r.message);
					}
				});
			});
		}).on('click', ' tbody tr', function () {
			var $modalWrapper = GenericModal.prototype.createWrapper('new-modal view-form-submission-modal'),
			    modalContent = React.createElement(ViewSubmission, { submission: $(this).attr('data-submission') });

			ReactDOM.render(React.createElement(GenericModal, {
				title: 'Form Submission',
				content: modalContent,
				noFooter: true
			}), $modalWrapper[0]);
		});

		// Unarchive submissions
		$('.form-submissions-table').on('click', 'tbody tr .unarchive-submission', function (e) {
			e.preventDefault();
			e.stopImmediatePropagation();

			var submission = $(this).closest('tr').attr('data-submission');

			alertify.confirm('Are you sure you want to unarchive this submission?', function () {
				$.post(stemcounter.ajax_url, {
					action: 'sc/form/submission/unarchive',
					submission: submission
				}, function (r) {
					if (r.success) {
						$('.form-submissions-table tr[data-submission="' + submission + '"]').remove();
						alertify.success('The submission was unarchived successfully.');
					} else {
						alertify.error(r.message);
					}
				});
			});
		});
	});
})(jQuery);