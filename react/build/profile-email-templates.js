'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function ($) {
	'use strict';

	var EmailTemplates = function (_SC_Component) {
		_inherits(EmailTemplates, _SC_Component);

		function EmailTemplates(props) {
			_classCallCheck(this, EmailTemplates);

			var _this2 = _possibleConstructorReturn(this, Object.getPrototypeOf(EmailTemplates).call(this, props));

			var state = {};
			state.templates = _this2.props.templates ? _this2.props.templates : [];
			state.selected_template = _this2.props.templates[0].id;
			for (var index = 0; index < state.templates.length; index++) {
				if (parseInt(state.templates[index].default, 10)) {
					state.selected_template = parseInt(state.templates[index].id, 10);
					break;
				}
			}
			_this2.sendRequestDebounced = _.debounce(_this2.sendRequestDebounced, 1000, {
				leading: false
			});
			_this2.state = state;
			return _this2;
		}

		_createClass(EmailTemplates, [{
			key: 'onTemplateChange',
			value: function onTemplateChange(e) {
				this.setState({
					selected_template: parseInt(e.value, 10)
				});
			}
		}, {
			key: 'handleContent',
			value: function handleContent(content, delta, source, editor) {
				var updatedState = $.extend(true, {}, this.state);
				var template = null;
				for (var index = 0; index < updatedState.templates.length; index++) {
					if (parseInt(updatedState.templates[index].id, 10) == parseInt(updatedState.selected_template, 10)) {
						template = updatedState.templates[index];
						updatedState.templates[index].content = content;
						break;
					}
				}
				this.setState(updatedState, function () {
					this.sendRequestDebounced(template);
				});
			}
		}, {
			key: 'handleChange',
			value: function handleChange(e) {
				var updatedState = $.extend(true, {}, this.state);
				var template = null;
				for (var index = 0; index < updatedState.templates.length; index++) {
					template = updatedState.templates[index];
					if (parseInt(updatedState.templates[index].id, 10) == parseInt(updatedState.selected_template, 10)) {
						switch (e.target.dataset.type) {
							case 'default':
								updatedState.templates[index].default = e.target.checked ? 1 : 0;
								break;
							case 'label':
								updatedState.templates[index].label = e.target.value;
								break;
							case 'subject':
								updatedState.templates[index].subject = e.target.value;
								break;
							default:
								break;
						}
						break;
					}
				}
				this.setState(updatedState);
			}
		}, {
			key: 'sendRequestDebounced',
			value: function sendRequestDebounced(template) {
				this.sendRequest(template);
			}
		}, {
			key: 'sendRequest',
			value: function sendRequest(template) {
				if (null === template) {
					return;
				}
				var _this = this,
				    updatedState = $.extend(true, {}, this.state),
				    url = window.stemcounter.aurl({ action: 'sc_edit_user_email_template' }),
				    data = {
					template: template
				};

				$.post(url, data, function (response) {
					if (response.success && response.payload.template_id) {
						for (var i = 0; i < updatedState.templates.length; i++) {
							if (1 == parseInt(template.default, 10)) {
								updatedState.templates[i].default = 0;
							}
							if (parseInt(updatedState.templates[i].id, 10) == parseInt(template.id, 10)) {
								updatedState.templates[i].id = parseInt(response.payload.template_id, 10);
								updatedState.templates[i].default = template.default;
								updatedState.selected_template = parseInt(response.payload.template_id, 10);
							}
						}
						_this.setState(updatedState);
					}
				});
			}
		}, {
			key: 'addTemplate',
			value: function addTemplate(e) {
				e.preventDefault();

				if (!stemcounter.AC.can_access(stemcounter.AC.STUDIO)) {
					alertify.error('You must upgrade to a Studio membership in order to create more templates!');
					return;
				}

				var updatedState = $.extend(true, {}, this.state);
				var template = {
					id: -1,
					label: 'New Template',
					subject: 'Example Subject',
					content: '<p>Here is your proposal. You can reply to this email with any questions that we can answer for you.</p>',
					default: 0
				};
				updatedState.templates.push(template);
				updatedState.selected_template = -1;
				this.setState(updatedState, function () {
					this.sendRequest(template);
				});
			}
		}, {
			key: 'render',
			value: function render() {
				var _this = this;
				var template = this.state.templates[0];
				var templateOptions = [];
				var addNewTemplate = null;

				if (this.state.templates.length) {
					this.state.templates.map(function (_template, index, arr) {
						if (_this.state.selected_template == parseInt(_template.id, 10)) {
							template = _template;
						}
						templateOptions.push({
							value: _template.id,
							label: _template.label + (1 == parseInt(_template.default, 10) ? ' (default)' : '')
						});
						return false;
					});
				}

				if (!stemcounter.AC.can_access(stemcounter.AC.STUDIO)) {
					addNewTemplate = React.createElement(
						'div',
						{ className: 'value' },
						React.createElement(
							'div',
							{ className: 'alert alert-warning' },
							React.createElement(
								'p',
								null,
								'Want more templates? ',
								React.createElement(
									'a',
									{ href: '/upgrade/' },
									'Upgrade to Studio'
								)
							)
						)
					);
				} else {
					addNewTemplate = React.createElement(
						'a',
						{ href: '#', onClick: this.addTemplate, className: 'cta-link' },
						'Add Template'
					);
				}

				return React.createElement(
					'div',
					null,
					React.createElement(
						'div',
						{ className: 'form-layout pane-row' },
						React.createElement(
							'div',
							{ className: 'row' },
							React.createElement(
								'div',
								{ className: 'col-sm-6 col-md-4' },
								React.createElement(
									'div',
									{ className: 'value' },
									React.createElement(VirtualizedSelect, {
										clearable: false,
										searchable: false,
										scrollMenuIntoView: false,
										className: 'template-select',
										value: this.state.selected_template,
										onChange: this.onTemplateChange,
										options: templateOptions,
										optionHeight: 32
									})
								)
							),
							React.createElement(
								'div',
								{ className: 'col-sm-6 col-md-push-2' },
								addNewTemplate
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'row' },
						React.createElement(EmailTemplate, {
							template: template,
							handleChange: this.handleChange,
							handleContent: this.handleContent,
							sendRequest: this.sendRequest
						})
					)
				);
			}
		}]);

		return EmailTemplates;
	}(SC_Component);

	var EmailTemplate = function (_SC_Component2) {
		_inherits(EmailTemplate, _SC_Component2);

		function EmailTemplate(props) {
			_classCallCheck(this, EmailTemplate);

			var _this3 = _possibleConstructorReturn(this, Object.getPrototypeOf(EmailTemplate).call(this, props));

			var state = {};
			_this3.state = state;
			return _this3;
		}

		_createClass(EmailTemplate, [{
			key: 'saveTemplate',
			value: function saveTemplate() {
				this.props.sendRequest(this.props.template);
			}
		}, {
			key: 'render',
			value: function render() {
				return React.createElement(
					'div',
					null,
					React.createElement(
						'div',
						{ className: 'row' },
						React.createElement(
							'div',
							{ className: 'col-xs-12 col-md-6' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								'Template Name'
							),
							React.createElement(
								'div',
								{ className: 'value' },
								React.createElement('input', { type: 'text', className: 'form-control', 'data-type': 'label', value: this.props.template.label, onChange: this.props.handleChange, onBlur: this.saveTemplate, placeholder: 'Template Name(only visible to you)' })
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'row' },
						React.createElement(
							'div',
							{ className: 'col-xs-12 col-md-6' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								'Default Template? ',
								React.createElement('input', { type: 'checkbox', className: '', 'data-type': 'default', value: 'yes', checked: parseInt(this.props.template.default, 10) ? true : false, onChange: this.props.handleChange, onBlur: this.saveTemplate })
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'row' },
						React.createElement(
							'div',
							{ className: 'col-xs-12 col-md-6' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								'Email Subject'
							),
							React.createElement(
								'div',
								{ className: 'value' },
								React.createElement('input', { type: 'text', 'data-type': 'subject', className: 'form-control', value: this.props.template.subject, onChange: this.props.handleChange, onBlur: this.saveTemplate, placeholder: 'Email Subject' })
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'row' },
						React.createElement(
							'div',
							{ className: 'col-xs-12 col-lg-6' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								'Email Content'
							),
							React.createElement(
								'div',
								{ className: 'value' },
								React.createElement(ReactQuill, {
									theme: 'snow',
									value: this.props.template.content,
									onChange: this.props.handleContent,
									modules: {
										toolbar: [[{ 'header': [1, 2, 3, false] }], ['bold', 'italic', 'underline', 'link', { 'align': [] }], [{ 'list': 'ordered' }, { 'list': 'bullet' }, 'clean']],
										clipboard: {
											matchVisual: false
										}
									}
								})
							)
						)
					)
				);
			}
		}]);

		return EmailTemplate;
	}(SC_Component);

	$(document).on('stemcounter.action.renderEmailTemplates', function (e, settings) {
		ReactDOM.render(React.createElement(EmailTemplates, {
			templates: settings.templates
		}), $(settings.node).get(0));
	});
})(jQuery);