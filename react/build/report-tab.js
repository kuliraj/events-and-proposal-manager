'use strict';

(function ($) {
	'use strict';

	var Report = React.createClass({
		displayName: 'Report',

		dataTable: false,

		getInitialState: function getInitialState() {
			return {
				dateStart: '',
				dateEnd: '',
				reportType: 'events',
				loading: false,
				data: [],
				data_labels: []
			};
		},

		componentDidMount: function componentDidMount() {
			$(ReactDOM.findDOMNode(this)).data('ReactComponent', this);
		},

		handleChange: function handleChange(e) {
			if ('date-end' == e.target.name) {
				this.setState({
					dateEnd: e.target.value
				});
			}

			if ('' != this.state.dateStart && '' != this.state.dateEnd) {
				var dFrom = Date.parse(this.state.dateStart);
				var dTo = Date.parse(this.state.dateEnd);
				if (dFrom > dTo) {
					alertify.error('Your dates are messed up!');
				}
			}
		},

		handleDateStartChange: function handleDateStartChange(value) {
			this.setState({
				dateStart: value
			});
		},

		handleDateEndChange: function handleDateEndChange(value) {
			this.setState({
				dateEnd: value
			});
		},

		handleReportType: function handleReportType(e) {
			e.preventDefault();

			this.setState({
				reportType: e.target.value
			});
		},

		onReport: function onReport(e) {
			e.preventDefault();

			if (undefined == this.state.reportType) {
				alertify.error('Report type must be selected');
				return;
			}

			if ('' == this.state.dateStart || '' == this.state.dateEnd) {
				alertify.error('Select START and END dates for your report');
				return;
			}

			this.setState({
				loading: true
			});

			var _this = this;
			var updatedState = $.extend(true, {}, this.state);
			var url = stemcounter.aurl({
				'action': 'sc_user_report'
			});
			var data = {
				date_start: this.state.dateStart,
				date_end: this.state.dateEnd,
				report_type: this.state.reportType
			};

			$.post(url, data, function (r) {
				if (r.success) {
					if (r.payload.data && r.payload.data_labels) {
						updatedState.data = r.payload.data;
						updatedState.data_labels = r.payload.data_labels;
						_this.setState(updatedState);
					}
				} else {
					alertify.error(r.message);
				}
			}).always(function () {
				_this.setState({
					loading: false
				});
			});
		},

		setDataTableRef: function setDataTableRef(Ref) {
			this.dataTable = Ref;
		},

		render: function render() {
			var loading = this.state.loading ? React.createElement('div', { className: 'loading-bar' }) : null,
			    reportTypeOptions = [React.createElement(
				'option',
				{ key: 'report-type-0', value: 'events' },
				'Events'
			), React.createElement(
				'option',
				{ key: 'report-type-1', value: 'payments' },
				'Payments'
			)];

			return React.createElement(
				'div',
				{ className: 'row report-component' },
				React.createElement(
					'div',
					{ className: 'pane' },
					React.createElement(
						'div',
						{ className: '' },
						React.createElement(
							'div',
							{ className: 'row report-heading' },
							React.createElement(
								'div',
								{ className: 'col-xs-12' },
								React.createElement(
									'div',
									{ className: 'report-type' },
									React.createElement(
										'label',
										{ className: 'clearfix' },
										'Type'
									),
									React.createElement(
										'div',
										{ className: '' },
										React.createElement(
											ReactSelect2,
											{
												selectedValue: this.state.reportType,
												onChange: this.handleReportType },
											reportTypeOptions
										)
									)
								),
								React.createElement(SC_DatePicker, { type: 'text', classList: 'report-date', nameList: 'date-start', defaultValue: this.state.dateStart, onDateChange: this.handleDateStartChange, dateFormat: this.props.dateFormat }),
								React.createElement(
									'span',
									null,
									' - '
								),
								React.createElement(SC_DatePicker, { type: 'text', classList: 'report-date', nameList: 'date-end', defaultValue: this.state.dateEnd, onDateChange: this.handleDateEndChange, dateFormat: this.props.dateFormat }),
								React.createElement(
									'a',
									{ href: '#', className: 'btn btn-primary report-btn', onClick: this.onReport },
									'Report'
								)
							)
						),
						React.createElement(
							'div',
							{ className: 'col-xs-12' },
							loading
						),
						React.createElement(
							'div',
							{ className: 'col-xs-12' },
							React.createElement(ReportOutput, { dataTableRef: this.setDataTableRef, reportType: this.state.reportType, dataRows: this.state.data, dataLabels: this.state.data_labels })
						)
					)
				)
			);
		}
	});

	var ReportOutput = React.createClass({
		displayName: 'ReportOutput',

		getInitialState: function getInitialState() {
			var state = {};
			state.defaultSortBy = '';
			state.dataLabels = this.props.data_labels ? this.props.dataLabels : [];
			state.dataRows = this.props.dataRows ? this.props.dataRows : [];
			state.taxSum = 0;
			state.amountSum = 0;

			if ('payments' == this.props.reportType && state.dataRows.length) {
				state.defaultSortBy = 'due_date';
				Object.keys(state.dataRows).map(function (key) {
					state.dataRows[key]['amount'] = stemcounter.formatPrice(state.dataRows[key]['amount_raw'], true, stemcounter.currencySymbol);
					return false;
				});
			} else if ('events' == this.props.reportType && state.dataRows.length) {
				state.defaultSortBy = 'event_name';
				Object.keys(state.dataRows).map(function (key) {
					state.dataRows[key]['total'] = stemcounter.formatPrice(state.dataRows[key]['total_raw'], true, stemcounter.currencySymbol);
					state.dataRows[key]['tax'] = stemcounter.formatPrice(state.dataRows[key]['tax_raw'], true, stemcounter.currencySymbol);
					return false;
				});
			}

			return state;
		},

		componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
			var updatedState = $.extend(true, {}, this.state);
			updatedState.dataRows = nextProps.dataRows;
			updatedState.dataLabels = nextProps.dataLabels;
			updatedState.taxSum = 0;
			updatedState.amountSum = 0;

			if ('payments' == nextProps.reportType && updatedState.dataRows.length) {
				updatedState.defaultSortBy = 'due_date';
				Object.keys(updatedState.dataRows).map(function (key) {
					updatedState.dataRows[key]['amount'] = stemcounter.formatPrice(updatedState.dataRows[key]['amount_raw'], true, stemcounter.currencySymbol);
					return false;
				});
			} else if ('events' == nextProps.reportType && updatedState.dataRows.length) {
				updatedState.defaultSortBy = 'event_name';
				Object.keys(updatedState.dataRows).map(function (key) {
					updatedState.dataRows[key]['total'] = stemcounter.formatPrice(updatedState.dataRows[key]['total_raw'], true, stemcounter.currencySymbol);
					updatedState.dataRows[key]['tax'] = stemcounter.formatPrice(updatedState.dataRows[key]['tax_raw'], true, stemcounter.currencySymbol);
					return false;
				});
			}

			this.setState(updatedState);
		},

		shouldComponentUpdate: function shouldComponentUpdate(nextProps, nextState) {
			return this.props.dataRows !== nextProps.dataRows || this.props.dataLabels !== nextProps.dataLabels || this.state.dataRows !== nextState.dataRows;
		},

		deleteRow: function deleteRow(e) {
			e.preventDefault();

			var updatedState = $.extend(true, {}, this.state),
			    index = $(e.target).closest('tr').data('index');

			updatedState.dataRows.splice(index, 1);

			this.setState(updatedState);
		},

		render: function render() {
			var _this = this;
			var table = null;
			var amountSum = null;
			var taxSum = null;
			if (this.state.dataRows) {
				table = React.createElement(DataTable, {
					ref: this.props.dataTableRef,
					defaultSortBy: this.state.defaultSortBy,
					defaultSortDir: 'ASC',
					rows: this.state.dataRows,
					labels: this.state.dataLabels,
					deleteRow: this.deleteRow
				});
				if (this.state.dataRows.length) {
					var taxTotal = 0;
					var amountTotal = 0;

					{
						Object.keys(this.state.dataRows).map(function (key) {
							if ('events' == _this.props.reportType) {
								taxTotal += parseFloat(_this.state.dataRows[key]['tax_raw']);
								amountTotal += parseFloat(_this.state.dataRows[key]['total_raw']);
							} else if ('payments' == _this.props.reportType) {
								amountTotal += parseFloat(_this.state.dataRows[key]['amount_raw']);
							}
							return false;
						});
					}

					amountSum = React.createElement(
						'div',
						{ className: 'row' },
						React.createElement(
							'span',
							{ className: 'pull-right' },
							'Total: ',
							React.createElement(
								'p',
								{ className: 'pull-right amountsum' },
								stemcounter.formatPrice(amountTotal, true, stemcounter.currencySymbol)
							)
						)
					);
					if ('events' == this.props.reportType) {
						taxSum = React.createElement(
							'div',
							{ className: 'row' },
							React.createElement(
								'span',
								{ className: 'pull-right' },
								'Tax: ',
								React.createElement(
									'p',
									{ className: 'pull-right amountsum' },
									stemcounter.formatPrice(taxTotal, true, stemcounter.currencySymbol)
								)
							)
						);
					}
				}
			} else {
				table = 'No records found for the given time period';
			}
			return React.createElement(
				'div',
				{ id: 'report-table' },
				table,
				amountSum,
				taxSum
			);
		}
	});

	$(document).on('stemcounter.action.renderReport', function (e, settings) {
		ReactDOM.render(React.createElement(Report, { dateFormat: settings.dateFormat }), settings.node.get(0));
	});
})(jQuery);