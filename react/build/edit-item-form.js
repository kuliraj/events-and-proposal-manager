'use strict';

(function ($) {
	'use strict';

	/* Run lodash in no conflict mode because we need _ underscore.js */

	if (undefined === window.lodash) {
		window.lodash = _.noConflict();
	}

	var CreateNewItemForm = React.createClass({
		displayName: 'CreateNewItemForm',

		getInitialState: function getInitialState() {
			var values = this.props.values;
			if (!values) {
				values = {
					id: -1,
					name: '',
					description: '',
					purchase_qty: 1,
					purchase_unit: 'item',
					variations: [{
						attachment: {},
						id: -1,
						cost: null,
						cost_pu: null,
						inventory: null,
						name: 'Default'
					}]
				};
			}
			return {
				itemType: this.props.defaultType,
				itemCategory: this.props.defaultCategory,
				values: values,
				saving: false
			};
		},

		componentDidMount: function componentDidMount() {
			initPhotoSwipeFromDOM('.item-photos');

			if (undefined === stemcounter.item_photo_frame) {
				if (!wp.media.model.Query.prototype.is_custom) {
					wp.media.model.Query.prototype.is_custom = true;
					wp.media.model.Query.prototype.initialize = function (models, options) {
						var allowed;

						options = options || {};
						wp.media.model.Attachments.prototype.initialize.apply(this, arguments);

						this.args = options.args;
						this._hasMore = true;
						this.created = new Date();

						this.filters.order = function (attachment) {
							var orderby = this.props.get('orderby'),
							    order = this.props.get('order');

							if (!this.comparator) {
								return true;
							}

							// We want any items that can be placed before the last
							// item in the set. If we add any items after the last
							// item, then we can't guarantee the set is complete.
							if (this.length) {
								return 1 !== this.comparator(attachment, this.last(), { ties: true });

								// Handle the case where there are no items yet and
								// we're sorting for recent items. In that case, we want
								// changes that occurred after we created the query.
							} else if ('DESC' === order && ('date' === orderby || 'modified' === orderby)) {
									return attachment.get(orderby) >= this.created;

									// If we're sorting by menu order and we have no items,
									// accept any items that have the default menu order (0).
								} else if ('ASC' === order && 'menuOrder' === orderby) {
										return attachment.get(orderby) === 0;
									}

							// Otherwise, we don't want any items yet.
							return false;
						};

						// Observe the central `wp.Uploader.queue` collection to watch for
						// new matches for the query.
						//
						// Only observe when a limited number of query args are set. There
						// are no filters for other properties, so observing will result in
						// false positives in those queries.
						allowed = ['s', 'order', 'orderby', 'posts_per_page', 'post_mime_type', 'post_parent', 'author'];
						if (wp.Uploader && _(this.args).chain().keys().difference(allowed).isEmpty().value()) {
							this.observe(wp.Uploader.queue);
						}
					};
				}

				stemcounter.item_photo_frame = stemcounter.getMediaModal({
					title: 'Upload Variation Photo',
					library: {
						type: 'image',
						author: userSettings.uid
					},
					button: {
						text: 'Add photo'
					},
					multiple: false // Set to true to allow multiple files to be selected
				});
			}

			$(ReactDOM.findDOMNode(this)).closest('.modal').on('hidden.bs.modal', this.onModalClosed);

			stemcounter.unmountComponentOnModalClose(this);
		},

		onPhotoUpload: function onPhotoUpload(e) {
			e.preventDefault();

			var _this = this;
			var updatedState = $.extend(true, {}, this.state);
			var index = $(e.target).closest('.item-variation').data('index');

			stemcounter.item_photo_frame.open();

			stemcounter.item_photo_frame.off('select').on('select', function () {

				// Get media attachment details from the frame state
				var attachment = stemcounter.item_photo_frame.state().get('selection').first().toJSON();

				for (var i = 0; i < updatedState.values.variations.length; i++) {
					if (index == i) {
						updatedState.values.variations[index].attachment = {
							imageId: attachment.id,
							imageURL: attachment.sizes.full ? attachment.sizes.full.url : attachment.url,
							imageThumbURL: attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url,
							imageW: attachment.width,
							imageH: attachment.height
						};
						_this.setState(updatedState);
						break;
					}
				}

				initPhotoSwipeFromDOM('.item-photos');

				setTimeout(function () {
					$('body').addClass('modal-open');
				}, 1000);
			}.bind(this));

			/** 
   * Add modal open class in order for the bootstrap modal 
   * to be scrollable again
   * after closing the WP Media modal 
   **/
			stemcounter.item_photo_frame.off('escape').on('escape', function () {
				setTimeout(function () {
					$('body').addClass('modal-open');
				}, 1000);
			}.bind(this));
		},

		onPhotoRemove: function onPhotoRemove(e) {
			e.preventDefault();
			var updatedState = $.extend(true, {}, this.state);
			var index = $(e.target).closest('.item-variation').data('index');

			for (var i = 0; i < updatedState.values.variations.length; i++) {
				if (index == i) {
					updatedState.values.variations[index].attachment = {};
					this.setState(updatedState);
					break;
				}
			}
		},

		onModalClosed: function onModalClosed() {
			if (!this.state.saving && (undefined !== stemcounter.page.createNewItemCaller || null !== stemcounter.page.createNewItemCaller) && parseInt(this.props.added_form_lib)) {
				stemcounter.page.createNewItemCaller.removeItem();
			}
			stemcounter.page.createNewItemCaller = null;
		},

		saveItem: function saveItem(e) {
			e.preventDefault();

			if (null === this.state.values.purchase_qty || 0 >= this.state.values.purchase_qty) {
				alertify.error('Purchased Quantity field is required!');
				return;
			}

			if (null === this.state.values.variations[0].cost_pu || 0 > this.state.values.variations[0].cost_pu) {
				alertify.error('Default variation cost is required!');
				return;
			}

			var _this = this;
			var $modal = $(e.target).closest('.modal');
			var url = window.stemcounter.aurl({ action: 'sc_edit_item' });
			var data = {
				item_category: this.state.itemCategory,
				item_type: this.state.itemType,
				values: this.state.values
			};
			this.setState({
				saving: true
			});

			$.post(url, data, function (r) {
				if (r.success) {
					var id = parseInt(_this.state.values.id);
					var eventName = '';
					var eventArgs = {
						'item': r.payload.item,
						'pricingCategory': r.payload.pricing_category,
						'itemVariationId': r.payload.item_variation_id,
						'oldItemCost': r.payload.old_item_cost,
						'attachment': r.payload.attachment,
						'variation_index': r.payload.variation_index
					};

					if (typeof stemcounter.page.createNewItemCaller == 'undefined') {
						window.location.reload();
						return;
					}

					// This will force re-sort the items in the Edit Recipe form
					stemcounter.page.userItemsSorted = false;
					if (isNaN(id) || id == -1 || stemcounter.page.editExistingItem) {
						stemcounter.gaEvent('item', 'created');
						eventName = 'stemcounter.userItemAdded';
						eventArgs['caller'] = typeof stemcounter.page.createNewItemCaller == 'undefined' ? null : stemcounter.page.createNewItemCaller;
						stemcounter.page.editExistingItem = false;
					} else {
						eventName = 'stemcounter.userItemModified';
					}

					stemcounter.page.userItems.push(r.payload.item);

					$(document).trigger(eventName, eventArgs);
					$modal.modal('hide');
				} else {
					console.log(r);
				}
			});
		},

		deleteItem: function deleteItem() {
			$(document).trigger('stemcounter.action.deleteItem', {
				itemId: this.state.values.id
			});
		},

		onChange: function onChange(e) {
			e.preventDefault();
			var updatedState = $.extend(true, {}, this.state);

			if ('pricing_catecogy' == e.target.name) {
				updatedState.itemCategory = e.target.value;
				if ('rental' == updatedState.itemCategory) {
					updatedState.itemType = 'rental';
					updatedState.values.cost = null;
					updatedState.values.purchase_qty = 1;
					updatedState.values.purchase_unit = 'item';
				} else if ('hardgood' == updatedState.itemCategory || 'flower' == updatedState.itemCategory) {
					updatedState.itemType = 'consumable';
					// recalculate cost/each to avoid issues
					if (!isNaN(updatedState.values.purchase_qty)) {
						for (var i = 0; i < updatedState.values.variations.length; i++) {
							if (!isNaN(updatedState.values.variations[i].cost_pu) && null !== updatedState.values.variations[i].cost_pu) {
								updatedState.values.variations[i].cost = parseFloat(parseFloat(updatedState.values.variations[i].cost_pu / updatedState.values.purchase_qty).toFixed10(2));
							} else {
								updatedState.values.variations[i].cost = null;
							}
						}
					}
				} else {
					updatedState.itemType = 'intangible';
					updatedState.values.cost = null;
					updatedState.values.purchase_qty = 1;
					updatedState.values.purchase_unit = 'item';
				}
			} else if ('item_name' == e.target.name) {
				updatedState.values.name = e.target.value;
			} else if ('description' == e.target.name) {
				updatedState.values.description = e.target.value;
			} else if ('purchase_qty' == e.target.name) {
				updatedState.values.purchase_qty = parseFloat(parseFloat(e.target.value).toFixed10(2));
				if (!isNaN(updatedState.values.purchase_qty) && 'consumable' == this.state.itemType) {
					for (var i = 0; i < updatedState.values.variations.length; i++) {
						if (!isNaN(updatedState.values.variations[i].cost_pu) && null !== updatedState.values.variations[i].cost_pu) {
							updatedState.values.variations[i].cost = parseFloat(parseFloat(updatedState.values.variations[i].cost_pu / updatedState.values.purchase_qty).toFixed10(2));
							this.refs['item_variation_' + i].refs.cost_unit.value = updatedState.values.variations[i].cost;
						} else {
							updatedState.values.variations[i].cost = null;
							this.refs['item_variation_' + i].refs.cost_unit.value = null;
						}
					}
				}
			} else if ('purchased_cost' == e.target.name) {
				updatedState.values.purchased_cost = parseFloat(parseFloat(e.target.value).toFixed10(2));
			} else if ('purchase_unit' == e.target.name) {
				updatedState.values.purchase_unit = e.target.value;
			}

			this.setState(updatedState);
		},

		onChangeVariation: function onChangeVariation(e) {
			e.preventDefault();
			var updatedState = $.extend(true, {}, this.state);
			var index = $(e.target).closest('.item-variation').data('index');

			if ('variation_name' == e.target.name) {
				updatedState.values.variations[index].name = e.target.value;
				if ('default' == e.target.value.toLowerCase()) {
					alertify.error('You can have ONLY one default variation!');
				}
			} else if ('variation_cost' == e.target.name) {
				updatedState.values.variations[index].cost_pu = parseFloat(parseFloat(e.target.value).toFixed10(2));
				if (isNaN(updatedState.values.variations[index].cost_pu)) {
					updatedState.values.variations[index].cost_pu = null;
					if ('consumable' == this.state.itemType) {
						updatedState.values.variations[index].cost = null;
						this.refs['item_variation_' + index].refs.cost_unit.value = null;
					}
				} else {
					if ('consumable' == this.state.itemType) {
						updatedState.values.variations[index].cost = parseFloat(parseFloat(parseFloat(e.target.value) / this.state.values.purchase_qty).toFixed10(2));
						this.refs['item_variation_' + index].refs.cost_unit.value = updatedState.values.variations[index].cost;
					}
				}
			} else if ('inventory' == e.target.name) {
				updatedState.values.variations[index].inventory = parseFloat(parseFloat(e.target.value).toFixed10(2));
				if (isNaN(updatedState.values.variations[index].inventory)) {
					updatedState.values.variations[index].inventory = null;
				}
			} else if ('cost_unit' == e.target.name) {
				updatedState.values.variations[index].cost = parseFloat(parseFloat(e.target.value).toFixed10(2));
				if (!isNaN(updatedState.values.variations[index].cost)) {
					updatedState.values.variations[index].cost_pu = parseFloat(parseFloat(parseFloat(e.target.value) * this.state.values.purchase_qty).toFixed10(2));
					this.refs['item_variation_' + index].refs.variation_cost.value = updatedState.values.variations[index].cost_pu;
				} else {
					updatedState.values.variations[index].cost = null;
					updatedState.values.variations[index].cost_pu = null;
				}
			}

			this.setState(updatedState);
		},

		onVariationRemove: function onVariationRemove(e) {
			e.preventDefault();

			var updatedState = $.extend(true, {}, this.state);
			var index = parseInt($(e.target).closest('.item-variation').data('index'), 10);

			updatedState.values.variations.splice(index, 1);

			if (parseInt(updatedState.values.variation_index, 10) == index && 1 < updatedState.values.variations.length) {
				updatedState.values.variation_index = 1;
			} else if (parseInt(updatedState.values.variation_index, 10) > index) {
				updatedState.values.variation_index = parseInt(updatedState.values.variation_index, 10) - 1;
			}

			this.setState(updatedState);
		},

		onAddVariation: function onAddVariation(e) {
			e.preventDefault();

			var updatedState = $.extend(true, {}, this.state);

			updatedState.values.variations[updatedState.values.variations.length] = {
				attachment: null,
				id: -1,
				name: '',
				cost: null,
				inventory: null
			};

			if (2 == updatedState.values.variations.length) {
				updatedState.values.variation_index = 1;
			}

			this.setState(updatedState);
		},

		onDrop: function onDrop(index, attachment) {
			var updatedState = $.extend(true, {}, this.state);
			updatedState.values.variations[index].attachment = attachment;

			this.setState(updatedState);
			initPhotoSwipeFromDOM('.item-photos');
		},

		render: function render() {
			var fields = [];
			var labels = null;
			var categoryOptions = [];
			var purchaseUnitOptions = [];
			var buttons = [React.createElement(
				'button',
				{ type: 'button', key: 'cancel', className: 'btn btn-default button-cancel', 'data-dismiss': 'modal', onClick: this.closeModal },
				'Cancel'
			), React.createElement(
				'button',
				{ type: 'submit', key: 'submit', className: 'btn btn-primary button-submit', onClick: this.saveItem },
				'Save'
			)];
			var measurement_unit = 'Each';

			for (var key in stemcounter.mixedArrangementItemTypes) {
				var value = stemcounter.mixedArrangementItemTypes[key];
				categoryOptions.push(React.createElement(
					'option',
					{ value: key, key: key },
					value
				));
			}

			var typeOptions = [React.createElement(
				'option',
				{ value: 'consumable', key: 'consumable' },
				'Consumable'
			), React.createElement(
				'option',
				{ value: 'rental', key: 'rental' },
				'Rental'
			), React.createElement(
				'option',
				{ value: 'intangible', key: 'intangible' },
				'Intangible'
			)];

			if (stemcounter.page.measuring_units && stemcounter.page.measuring_units.length) {
				for (var i = 0; i < stemcounter.page.measuring_units.length; i++) {
					var unit = stemcounter.page.measuring_units[i];
					if (1 == parseFloat(this.state.values.purchase_qty)) {
						purchaseUnitOptions.push(React.createElement(
							'option',
							{ value: unit.id, key: unit.short },
							unit.short
						));
					} else {
						purchaseUnitOptions.push(React.createElement(
							'option',
							{ value: unit.id, key: unit.short_plural },
							unit.short_plural
						));
					}
					if (this.state.values.purchase_unit == unit.id) {
						measurement_unit = unit.short;
					}
				}
			} else {
				if (1 == parseFloat(this.state.values.purchase_qty)) {
					purchaseUnitOptions.push(React.createElement(
						'option',
						{ value: 'item', key: 'item' },
						'item'
					));
				} else {
					purchaseUnitOptions.push(React.createElement(
						'option',
						{ value: 'item', key: 'items' },
						'items'
					));
				}
			}

			if (this.state.values.id > 0) {
				buttons.unshift(React.createElement(
					'button',
					{ type: 'button', key: 'delete', className: 'btn btn-default', onClick: this.deleteItem },
					'Delete'
				));
			}

			if (this.state.itemType == 'consumable') {

				fields.push(React.createElement(
					'label',
					{ className: 'col-sm-6 control-label modalpane', key: 'pq-field' },
					React.createElement(
						'div',
						{ className: 'lbl' },
						'Purchase Unit (pu) Quantity',
						React.createElement('span', { className: 'mandatory' })
					),
					React.createElement('input', { type: 'text', name: 'purchase_qty', defaultValue: this.state.values.purchase_qty, onChange: this.onChange, className: 'purchase-qty form-control' }),
					React.createElement(
						'div',
						{ className: 'purchase-unit' },
						React.createElement(ReactSelect2, {
							className: 'form-control eventItemValue',
							name: 'purchase_unit',
							children: purchaseUnitOptions,
							selectedValue: this.state.values.purchase_unit,
							onChange: this.onChange
						})
					)
				));
			} else if (this.state.itemType == 'rental') {
				// Rental Purchased Cost - can come later

				/*fields.push(
    	<div className="modalpane-row clearfix">
    		<label className="col-sm-12 control-label modalpane">
    			<div className="lbl">
    				Purchased Cost<span className="mandatory"></span>
    			</div>
    			<input type="text" name="purchased_cost" defaultValue={'?'} onChange={this.onChange} className="form-control" />
    		</label>
    	</div>
    );*/
			}

			if (this.state.itemType == 'consumable') {
				labels = React.createElement(
					'div',
					{ className: 'clearfix' },
					React.createElement(
						'div',
						{ className: 'lbl col-sm-4' },
						'Variations',
						React.createElement('span', { className: '' })
					),
					React.createElement(
						'div',
						{ className: 'lbl col-sm-3' },
						'Cost/' + measurement_unit,
						React.createElement('span', { className: '' })
					),
					React.createElement(
						'div',
						{ className: 'lbl col-sm-3' },
						'Cost/PU',
						React.createElement('span', { className: '' })
					),
					React.createElement(
						'div',
						{ className: 'lbl col-sm-2' },
						'Photo',
						React.createElement('span', { className: '' })
					)
				);
			} else if (this.state.itemType == 'intangible') {
				labels = React.createElement(
					'div',
					{ className: 'clearfix' },
					React.createElement(
						'div',
						{ className: 'lbl col-sm-5' },
						'Variations',
						React.createElement('span', { className: '' })
					),
					React.createElement(
						'div',
						{ className: 'lbl col-sm-4' },
						'Price/Each',
						React.createElement('span', { className: '' })
					),
					React.createElement(
						'div',
						{ className: 'lbl col-sm-3' },
						'Photo',
						React.createElement('span', { className: '' })
					)
				);
			} else {
				labels = React.createElement(
					'div',
					{ className: 'clearfix' },
					React.createElement(
						'div',
						{ className: 'lbl col-sm-4' },
						'Variations',
						React.createElement('span', { className: '' })
					),
					React.createElement(
						'div',
						{ className: 'lbl col-sm-3' },
						'Price/Each',
						React.createElement('span', { className: '' })
					),
					React.createElement(
						'div',
						{ className: 'lbl col-sm-3' },
						'Inventory',
						React.createElement('span', { className: '' })
					),
					React.createElement(
						'div',
						{ className: 'lbl col-sm-2' },
						'Photo',
						React.createElement('span', { className: '' })
					)
				);
			}
			return React.createElement(
				'div',
				null,
				React.createElement(
					'div',
					{ className: 'modalpane-row col-sm-12 clearfix' },
					React.createElement(
						'label',
						{ className: 'control-label modalpane col-sm-6' },
						React.createElement(
							'div',
							{ className: 'lbl' },
							'Pricing Category',
							React.createElement('span', { className: 'mandatory' })
						),
						React.createElement(ReactSelect2, {
							className: 'form-control eventItemValue',
							name: 'pricing_catecogy',
							children: categoryOptions,
							selectedValue: this.state.itemCategory,
							disabled: this.state.values.id > 0 ? true : false,
							onChange: this.onChange
						})
					)
				),
				React.createElement(
					'div',
					{ className: 'modalpane-row clearfix' },
					React.createElement(
						'div',
						{ className: 'col-xs-12 modalpane' },
						React.createElement(
							'label',
							{ className: 'control-label col-sm-6' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Name',
								React.createElement('span', { className: 'mandatory' })
							),
							React.createElement('input', { type: 'text', placeholder: 'Add name here', name: 'item_name', defaultValue: this.state.values.name, onChange: this.onChange, className: 'form-control' })
						),
						fields
					)
				),
				React.createElement(
					'div',
					{ className: 'modalpane-row clearfix' },
					React.createElement(
						'label',
						{ className: 'col-sm-12 control-label modalpane' },
						labels,
						Object.keys(this.state.values.variations).map(function (key) {
							return React.createElement(ItemVariation, {
								key: 'item-variation-' + this.state.values.variations[key].id + '-' + key,
								ref: 'item_variation_' + key,
								index: key,
								'default': 0 == key ? true : false,
								itemType: this.state.itemType,
								values: this.state.values,
								variation: this.state.values.variations[key],
								defaultVariation: this.state.values.variations[0],
								onChangeVariation: this.onChangeVariation,
								onAddVariation: this.onAddVariation,
								onVariationRemove: this.onVariationRemove,
								onPhotoUpload: this.onPhotoUpload,
								onPhotoRemove: this.onPhotoRemove,
								onDrop: this.onDrop
							});
						}.bind(this))
					),
					React.createElement('a', { href: '', className: 'add-item-variation', onClick: this.onAddVariation })
				),
				React.createElement(
					'div',
					{ className: 'modalpane-row clearfix' },
					React.createElement(
						'div',
						{ className: 'col-sm-12 modalpane' },
						React.createElement(
							'label',
							{ className: ' control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Description',
								React.createElement('span', { className: '' })
							),
							React.createElement('textarea', { placeholder: 'Add description here', name: 'description', defaultValue: this.state.values.description, onChange: this.onChange, className: 'form-control' })
						)
					)
				),
				React.createElement(
					'div',
					{ className: 'modal-footer' },
					React.createElement(
						'div',
						{ className: 'pull-left hide' },
						'*required'
					),
					buttons
				)
			);
		}
	});

	var ItemVariation = React.createClass({
		displayName: 'ItemVariation',

		getInitialState: function getInitialState() {
			var variation = this.props.variation;
			if (!variation) {
				variation = {
					attachment: {},
					id: -1,
					cost: null,
					cost_pu: null,
					inventory: null,
					name: 'Default'
				};
			}
			return {
				variation: variation
			};
		},
		onDragOver: function onDragOver(event) {
			event.preventDefault();
			event.stopPropagation();
		},
		onDrop: function onDrop(event) {
			event.stopPropagation();
			event.preventDefault();

			var _this = this,
			    variation_index = parseInt(this.props.index, 10);

			if (event.dataTransfer.files.length) {
				return;
			}

			var url = '',
			    html = event.dataTransfer.getData('text/html'),
			    _this = this;

			html = $($.parseHTML(html));

			var files = [];

			if (html.children().length > 0) {
				url = html.find('img').attr('src');
			} else {
				url = html.attr('src');
			}

			if (!url) {
				alertify.error('Could not upload dropped image.');
				return;
			}

			var variation = $.extend(true, {}, this.state.variation);
			variation.attachment.imageId = -1;
			variation.attachment = {
				imageId: -1,
				imageThumbURL: stemcounter.theme_url + '/img/loading.gif',
				imageURL: stemcounter.theme_url + '/img/loading.gif',
				imageH: 75,
				imageW: 75
			};

			this.setState({
				variation: variation
			}, this.props.onDrop(variation_index, variation.attachment));

			var ajax_url = window.stemcounter.aurl({ action: 'sc_upload_dragged_variation_media' }),
			    data = {
				variation_id: variation.id,
				url: url
			};

			$.post(ajax_url, data, function (response) {
				stemcounter.JSONResponse(response, function (r) {
					if (r.success) {
						var variation = $.extend(true, {}, _this.state.variation);
						variation.attachment = r.payload.photo;
						_this.setState({
							variation: variation
						}, _this.props.onDrop(variation_index, variation.attachment));
					} else {
						console.log(r);
					}
				});
			});
		},

		render: function render() {
			var itemPhoto = null,
			    removeBtn = null,
			    pointer = null,
			    variation_index = parseInt(this.props.values.variation_index);

			// use defaultVariation values if the current are NULL's
			var varCost = null == this.props.variation.cost ? this.props.defaultVariation.cost : this.props.variation.cost,
			    varCostPu = null == this.props.variation.cost_pu ? this.props.defaultVariation.cost_pu : this.props.variation.cost_pu,
			    varAttachment = null == this.props.variation.attachment ? this.props.defaultVariation.attachment : this.props.variation.attachment,
			    varInventory = null == this.props.variation.inventory ? this.props.defaultVariation.inventory : this.props.variation.inventory;

			if (-1 == this.props.defaultVariation.id) {
				if (null == this.props.defaultVariation.cost_pu) {
					varCost = 'default';
					varCostPu = 'default';
				}
				if (null == this.props.defaultVariation.inventory) {
					varInventory = 'default';
				}
			}

			if (varAttachment && varAttachment.imageId) {
				if (null != this.props.variation.attachment) {
					itemPhoto = React.createElement(
						'div',
						{ className: '' },
						React.createElement(
							'div',
							{ className: 'item-photos' },
							React.createElement('div', { className: 'item-photo', style: {
									backgroundImage: 'url("' + varAttachment.imageThumbURL + '")'
								},
								'data-full-image-url': varAttachment.imageURL,
								'data-full-image-w': varAttachment.imageW,
								'data-full-image-h': varAttachment.imageH
							})
						),
						React.createElement(
							'a',
							{ href: '#', onClick: this.props.onPhotoRemove },
							'Remove'
						)
					);
				} else {
					itemPhoto = React.createElement(
						'a',
						{ href: '#', className: 'upload-photo', onClick: this.props.onPhotoUpload },
						'default'
					);
				}
			} else {
				itemPhoto = React.createElement(
					'a',
					{ href: '#', className: 'upload-photo', onClick: this.props.onPhotoUpload },
					'Upload'
				);
			}

			if (!this.props.default) {
				removeBtn = React.createElement('a', { href: '', className: 'remove-variation', onClick: this.props.onVariationRemove });
			}

			if (variation_index && parseInt(this.props.index) == variation_index || !variation_index && 1 < this.props.values.variations.length && 1 == parseInt(this.props.index) || 0 == parseInt(this.props.index) && 1 == this.props.values.variations.length) {
				pointer = React.createElement(
					'span',
					{ className: 'var-pointer', title: 'Selected Variation' },
					'>'
				);
			}

			if ('consumable' == this.props.itemType) {
				return React.createElement(
					'div',
					{ className: 'modalpane-row clearfix item-variation', 'data-index': this.props.index, onDrop: this.onDrop, onDragOver: this.onDragOver },
					pointer,
					React.createElement(
						'div',
						{ className: 'col-sm-4' },
						React.createElement('input', { type: 'text', placeholder: 'Name', name: 'variation_name', defaultValue: this.props.variation.name, onBlur: this.props.onChangeVariation, className: 'form-control', disabled: this.props.default ? true : false })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-3' },
						React.createElement('input', { type: 'text', ref: 'cost_unit', placeholder: varCost, name: 'cost_unit', defaultValue: this.props.variation.cost, onChange: this.props.onChangeVariation, className: 'form-control' })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-3' },
						React.createElement('input', { type: 'text', ref: 'variation_cost', placeholder: varCostPu, name: 'variation_cost', defaultValue: this.props.variation.cost_pu, onChange: this.props.onChangeVariation, className: 'form-control' })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-2' },
						itemPhoto
					),
					removeBtn
				);
			} else if ('intangible' == this.props.itemType) {
				return React.createElement(
					'div',
					{ className: 'modalpane-row clearfix item-variation', 'data-index': this.props.index, onDrop: this.onDrop, onDragOver: this.onDragOver },
					pointer,
					React.createElement(
						'div',
						{ className: 'col-sm-5' },
						React.createElement('input', { type: 'text', placeholder: 'Name', name: 'variation_name', defaultValue: this.props.variation.name, onChange: this.props.onChangeVariation, className: 'form-control', disabled: this.props.default ? true : false })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-4' },
						React.createElement('input', { type: 'text', placeholder: varCostPu, name: 'variation_cost', defaultValue: this.props.variation.cost_pu, onChange: this.props.onChangeVariation, className: 'form-control' })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-3' },
						itemPhoto
					),
					removeBtn
				);
			} else {
				return React.createElement(
					'div',
					{ className: 'modalpane-row clearfix item-variation', 'data-index': this.props.index, onDrop: this.onDrop, onDragOver: this.onDragOver },
					pointer,
					React.createElement(
						'div',
						{ className: 'col-sm-4' },
						React.createElement('input', { type: 'text', placeholder: 'Name', name: 'variation_name', defaultValue: this.props.variation.name, onChange: this.props.onChangeVariation, className: 'form-control', disabled: this.props.default ? true : false })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-3' },
						React.createElement('input', { type: 'text', placeholder: varCostPu, name: 'variation_cost', defaultValue: this.props.variation.cost_pu, onChange: this.props.onChangeVariation, className: 'form-control' })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-3' },
						React.createElement('input', { type: 'text', placeholder: varInventory, name: 'inventory', defaultValue: this.props.variation.inventory, onChange: this.props.onChangeVariation, className: 'form-control' })
					),
					React.createElement(
						'div',
						{ className: 'col-sm-2' },
						itemPhoto
					),
					removeBtn
				);
			}
		}
	});

	jQuery(document).on('stemcounter.action.renderCreateNewItemForm', function (e, data) {
		ReactDOM.render(React.createElement(CreateNewItemForm, { defaultType: data.defaultType, defaultCategory: data.defaultCategory, values: data.values, attachment: data.attachment, added_form_lib: data.added_form_lib }), $('.edit-item-form .form-shell:first').get(0));
	});
})(jQuery);