'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function ($) {
	var ProfileSettings = function (_SC_Component) {
		_inherits(ProfileSettings, _SC_Component);

		function ProfileSettings(props) {
			_classCallCheck(this, ProfileSettings);

			var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ProfileSettings).call(this, props));

			_this.state = {
				logo: props.settings.logo,
				logo_id: props.settings.logo_id,
				logo_on_proposal: props.settings.logo_on_proposal ? 1 : 0,
				user_libraries: props.settings.user_libraries,
				user_units: props.settings.user_units,
				company: props.settings.company,
				address: props.settings.address,
				website: props.settings.website,
				phone: props.settings.phone,
				card: props.settings.card,
				date_format: props.settings.date_format,
				pref_currency: props.settings.pref_currency,
				pdf_sheet_size: props.settings.pdf_sheet_size,
				visible_taxes: props.settings.visible_taxes,
				prices_separator: props.settings.prices_separator
			};

			_this.save = _.debounce(_this.save, 1000);
			return _this;
		}

		_createClass(ProfileSettings, [{
			key: 'componentDidMount',
			value: function componentDidMount() {}
		}, {
			key: 'onLibrarySelect',
			value: function onLibrarySelect(e) {
				var user_libraries = $.extend(true, [], this.state.user_libraries),
				    library = parseInt(e.target.value, 10);

				if (e.target.checked) {
					if (-1 === user_libraries.indexOf(library)) {
						alertify.confirm('I understand that items imported into my library are only suggested initial prices to make my StemCounter setup easier. Prices ordered from Mayesh or other wholesalers will vary depending on the time of the year or seasonality of the product.', function () {
							// Yes
							user_libraries.push(library);
							this.setState({
								user_libraries: user_libraries
							}, this.save);
						}.bind(this));
					}
				} else if (-1 !== user_libraries.indexOf(library)) {
					user_libraries.splice(user_libraries.indexOf(library), 1);
					this.setState({
						user_libraries: user_libraries
					}, this.save);
				}
			}
		}, {
			key: 'renderLibrary',
			value: function renderLibrary(library) {
				return React.createElement(
					'li',
					{ key: 'library-' + library.id },
					React.createElement(
						'label',
						null,
						React.createElement('input', {
							name: 'library',
							type: 'checkbox',
							value: library.id,
							checked: -1 !== this.state.user_libraries.indexOf(library.id),
							onChange: this.onLibrarySelect
						}),
						' ',
						library.name
					)
				);
			}
		}, {
			key: 'onLogoUploaded',
			value: function onLogoUploaded(image_id, image) {
				this.setState({
					logo_id: image_id,
					logo: image
				}, this.save);
			}
		}, {
			key: 'save',
			value: function save() {
				var data = $.extend(true, {}, this.state);
				data.action = 'sc/profile/settings/save';
				data.nonce = this.props.settings.nonce;

				$.post(stemcounter.ajax_url, data, this.onSaved);
			}
		}, {
			key: 'onSaved',
			value: function onSaved(r) {
				if (r.success) {
					alertify.success('Settings saved');
				} else {
					alertify.error('Could not save settings');
				}
			}
		}, {
			key: 'logoOnProposalChange',
			value: function logoOnProposalChange(e) {
				this.setState({
					logo_on_proposal: e.target.checked ? 1 : 0
				}, this.save);
			}
		}, {
			key: 'onFieldChanged',
			value: function onFieldChanged(e) {
				var updatedState = {},
				    name = e.target.dataset.name;

				console.log(name, e.target.value);

				if (name) {
					updatedState[name] = e.target.value;
					this.setState(updatedState, this.save);
				}
			}
		}, {
			key: 'onDateFormatChanged',
			value: function onDateFormatChanged(e) {
				this.setState({
					date_format: e.target.value
				}, this.save);
			}
		}, {
			key: 'onCurrencyChanged',
			value: function onCurrencyChanged(e) {
				this.setState({
					pref_currency: e.target.value
				}, this.save);
			}
		}, {
			key: 'onPaperSizeChanged',
			value: function onPaperSizeChanged(e) {
				this.setState({
					pdf_sheet_size: e.target.value
				}, this.save);
			}
		}, {
			key: 'onPricesSeparatorChanged',
			value: function onPricesSeparatorChanged(e) {
				this.setState({
					prices_separator: e.target.value
				}, this.save);
			}
		}, {
			key: 'onVisibleTaxesChanged',
			value: function onVisibleTaxesChanged(e) {
				this.setState({
					visible_taxes: e.target.value
				}, this.save);
			}
		}, {
			key: 'onUnitSelect',
			value: function onUnitSelect(e) {
				if (e.target.checked) {
					var units = $.extend(true, [], this.state.user_units);
					if (-1 == units.indexOf(e.target.value)) {
						units.push(e.target.value);

						this.setState({
							user_units: units
						}, this.save);
					}
				}
			}
		}, {
			key: 'render',
			value: function render() {
				var dateFormats = [];
				var Currencies = [];

				Object.keys(this.props.settings.date_format_options).map(function (key) {
					dateFormats.push(React.createElement(
						'option',
						{ value: key, key: 'date-fomat-' + key },
						this.props.settings.date_format_options[key]
					));
				}.bind(this));

				Object.keys(this.props.settings.pref_currencies).map(function (key) {
					Currencies.push(React.createElement(
						'option',
						{ value: key, key: 'currency-' + key },
						this.props.settings.pref_currencies[key]
					));
				}.bind(this));

				var pref_prices_separators = [React.createElement(
					'option',
					{ key: 'dot_comma', value: 'dot_comma' },
					'1.234.567,89'
				), React.createElement(
					'option',
					{ key: 'space_comma', value: 'space_comma' },
					'1 234 567,89'
				), React.createElement(
					'option',
					{ key: 'comma_dot', value: 'comma_dot' },
					'1,234,567.89'
				), React.createElement(
					'option',
					{ key: 'space_dot', value: 'space_dot' },
					'1 234 567.89'
				)];

				var pref_paper_size = [React.createElement(
					'option',
					{ key: 'US-Letter', value: 'US-Letter' },
					'US-Letter'
				), React.createElement(
					'option',
					{ key: 'A4', value: 'A4' },
					'A4'
				)];

				return React.createElement(
					'div',
					{ className: 'row' },
					React.createElement(
						'div',
						{ className: 'pane-row company-logo-row' },
						React.createElement(
							'div',
							{ className: 'pane company-logo' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								'Company Logo'
							),
							React.createElement(
								'div',
								{ className: 'tab-content value' },
								React.createElement(
									'div',
									{ className: 'row tab-pane active', id: 'company-logo-tab' },
									React.createElement(
										'div',
										{ className: 'col-lg-12' },
										React.createElement(ImageUploader, {
											image: this.state.logo,
											imageId: this.state.logo_id,
											imageSize: 'logo_medium',
											uploadNonce: this.props.settings.logo_upload_nonce,
											uploadAction: this.props.settings.logo_upload_action,
											onImageUploaded: this.onLogoUploaded
										}),
										React.createElement('br', null),
										React.createElement(
											'p',
											{ className: 'logo-on-proposal' },
											React.createElement(
												'label',
												null,
												React.createElement('input', { type: 'checkbox', checked: this.state.logo_on_proposal, onChange: this.logoOnProposalChange }),
												' Include logo on proposal'
											)
										)
									)
								)
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'profile-company-details' },
						React.createElement(
							'div',
							{ className: 'edit-company-address' },
							React.createElement(
								'div',
								{ className: 'form-layout pane-row company-address' },
								React.createElement(
									'div',
									{ className: 'pane' },
									React.createElement(
										'label',
										{ className: 'control-label' },
										'Company'
									),
									React.createElement(
										'div',
										{ className: 'value' },
										React.createElement('input', { type: 'text', className: 'form-control', id: 'company', value: this.state.company, 'data-name': 'company', onChange: this.onFieldChanged })
									)
								),
								React.createElement(
									'div',
									{ className: 'pane' },
									React.createElement(
										'label',
										{ className: 'control-label' },
										'Address'
									),
									React.createElement(
										'div',
										{ className: 'value' },
										React.createElement('input', { type: 'text', className: 'form-control', id: 'address', value: this.state.address, 'data-name': 'address', onChange: this.onFieldChanged })
									)
								)
							),
							React.createElement(
								'div',
								{ className: 'form-layout pane-row phone-website' },
								React.createElement(
									'div',
									{ className: 'pane' },
									React.createElement(
										'label',
										{ className: 'control-label' },
										'Phone'
									),
									React.createElement(
										'div',
										{ className: 'value' },
										React.createElement('input', { type: 'text', className: 'form-control', id: 'phone', value: this.state.phone, 'data-name': 'phone', onChange: this.onFieldChanged })
									)
								),
								React.createElement(
									'div',
									{ className: 'pane' },
									React.createElement(
										'label',
										{ className: 'control-label' },
										'Website'
									),
									React.createElement(
										'div',
										{ className: 'value' },
										React.createElement('input', { type: 'text', className: 'form-control', id: 'website', value: this.state.website, 'data-name': 'website', onChange: this.onFieldChanged })
									)
								)
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'pane-row invoicing-settings-pane' },
						React.createElement(
							'div',
							{ className: 'invoicing-settings-table' },
							React.createElement(
								'div',
								{ className: 'pane ccr' },
								React.createElement(
									'label',
									{ className: 'control-label' },
									'Charge Card Rate'
								),
								React.createElement(
									'div',
									{ className: 'value' },
									React.createElement(FloatInput, {
										type: 'text',
										className: 'form-control',
										value: this.state.card,
										onChange: this.onFieldChanged,
										'data-name': 'card'
									})
								)
							),
							React.createElement(
								'div',
								{ className: 'pane date-format' },
								React.createElement(
									'label',
									{ className: 'control-label' },
									'Date Format'
								),
								React.createElement(
									'div',
									{ className: 'value' },
									React.createElement(ReactSelect2, { ref: 'date_format', className: 'date-format-select', children: dateFormats, selectedValue: this.state.date_format, onChange: this.onDateFormatChanged })
								)
							),
							React.createElement(
								'div',
								{ className: 'pane currencies' },
								React.createElement(
									'label',
									{ className: 'control-label' },
									'Currency'
								),
								React.createElement(
									'div',
									{ className: 'value' },
									React.createElement(ReactSelect2, { ref: 'currency', className: 'currency-select', children: Currencies, selectedValue: this.state.pref_currency, onChange: this.onCurrencyChanged, disabled: true })
								)
							),
							React.createElement(
								'div',
								{ className: 'pane paper-size' },
								React.createElement(
									'label',
									{ className: 'control-label' },
									'PDF sheet size'
								),
								React.createElement(
									'div',
									{ className: 'value' },
									React.createElement(ReactSelect2, { ref: 'paper_size', className: 'paper-size-select', children: pref_paper_size, selectedValue: this.state.pdf_sheet_size, onChange: this.onPaperSizeChanged })
								)
							),
							React.createElement(
								'div',
								{ className: 'pane price-separator' },
								React.createElement(
									'label',
									{ className: 'control-label' },
									'Decimal Structure'
								),
								React.createElement(
									'div',
									{ className: 'value' },
									React.createElement(ReactSelect2, { ref: 'prices_separator', className: 'prices-separator', children: pref_prices_separators, selectedValue: this.state.prices_separator, onChange: this.onPricesSeparatorChanged })
								)
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'pane-row' },
						React.createElement(
							'div',
							{ className: 'pane libraries' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								'Flower Libraries'
							),
							React.createElement(
								'div',
								{ className: 'value' },
								React.createElement(
									'ul',
									{ className: 'form-control libraries' },
									this.props.settings.libraries.map(this.renderLibrary)
								)
							)
						),
						React.createElement(
							'div',
							{ className: 'pane' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								'Number of Taxes'
							),
							React.createElement(
								'div',
								{ className: 'value' },
								React.createElement(
									ReactSelect2,
									{ selectedValue: this.state.visible_taxes, onChange: this.onVisibleTaxesChanged },
									React.createElement(
										'option',
										{ value: '1' },
										'1'
									),
									React.createElement(
										'option',
										{ value: '2' },
										'2'
									)
								)
							)
						),
						React.createElement(
							'div',
							{ className: 'pane libraries' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								'Measuring Units'
							),
							React.createElement(
								'div',
								{ className: 'value' },
								React.createElement(
									'ul',
									{ className: 'form-control' },
									this.props.settings.measuring_units.map(function (unit) {
										return React.createElement(
											'li',
											{ key: 'unit-' + unit.id },
											React.createElement(
												'label',
												null,
												React.createElement('input', {
													name: 'unit',
													type: 'checkbox',
													value: unit.id,
													checked: -1 !== this.state.user_units.indexOf(unit.id),
													disabled: -1 !== this.state.user_units.indexOf(unit.id),
													onChange: this.onUnitSelect
												}),
												' ',
												unit.singular
											)
										);
									}.bind(this))
								)
							)
						)
					)
				);
			}
		}]);

		return ProfileSettings;
	}(SC_Component);

	$(document).on('stemcounter.action.renderProfileSettingsForm', function (e, settings) {
		ReactDOM.render(React.createElement(ProfileSettings, { settings: settings }), settings.node);
	});
})(jQuery);