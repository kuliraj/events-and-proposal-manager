'use strict';

(function ($) {
	'use strict';

	var CustomerModal = React.createClass({
		displayName: 'CustomerModal',

		getInitialState: function getInitialState() {
			var state = {
				layout: '',
				customer_id: -1,
				customer_first_name: '',
				customer_last_name: '',
				customer_phone: '',
				customer_email: '',
				customer_source: '',
				customer_address: '',
				customer_notes: '',
				customerLinkedEvents: [],
				properties: []
			};

			//fill state object with props
			for (var prop in this.props) {
				if ('createNewCustomerCaller' != prop && this.props.hasOwnProperty(prop) && this.props[prop] !== undefined) {
					state[prop] = this.props[prop];
				}
			}

			return state;
		},

		componentDidMount: function componentDidMount(e) {
			stemcounter.unmountComponentOnModalClose(this);
		},

		handleChange: function handleChange(field, event) {
			var updatedState = {};

			switch (field) {
				case 'firstName':
					updatedState.customer_first_name = event.target.value;
					break;
				case 'lastName':
					updatedState.customer_last_name = event.target.value;
					break;
				case 'phone':
					updatedState.customer_phone = event.target.value;
					break;
				case 'email':
					updatedState.customer_email = event.target.value;
					break;
				case 'customer_source':
					updatedState.customer_source = event.target.value;
					break;
				case 'address':
					updatedState.customer_address = event.target.value;
					break;
				case 'note':
					updatedState.customer_notes = event.target.value;
					break;
				default:
					break;
			}

			this.setState(updatedState);
		},
		editProperty: function editProperty(e, property) {
			var updatedState = $.extend(true, {}, this.state);
			if ('multi-checkbox' == property.field_type) {
				if (e.target.checked) {
					property.values.push({
						id: -1,
						customer_id: this.state.customer_id,
						property_id: property.id,
						value: property.aopt[parseInt(e.target.value, 10)]
					});
				} else {
					property.values.map(function (value, key, arr) {
						if (value.value == property.aopt[parseInt(e.target.value, 10)]) {
							property.values.splice(key, 1);
						}
						return false;
					});
				}
			} else {
				if (!property.values.length) {
					property.values.push({
						id: -1,
						customer_id: this.state.customer_id,
						property_id: property.id
					});
				}

				if ('input' == property.field_type || 'textarea' == property.field_type) {
					if ('string' == property.format || 'number' == property.format) {
						property.values[0].value = e.target.value;
					} else if ('date' == property.format) {
						property.values[0].value = e;
					}
				} else if ('radio' == property.field_type) {
					property.values[0].value = property.aopt[parseInt(e.target.value, 10)];
				} else if ('checkbox' == property.field_type) {
					if (e.target.checked) {
						property.values[0].value = 'Yes';
					} else {
						property.values[0].value = 'No';
					}
				}
			}

			updatedState.properties.map(function (_property, index, arr) {
				if (_property.id == property.id) {
					updatedState.properties[index] = property;
				}
				return false;
			});
			this.setState(updatedState);
		},

		submit: function submit(event) {
			//submit function
			event.preventDefault();
			event.persist();
			var _this = this;
			if (this.props.layout === 'add-customer') {
				var url = window.stemcounter.aurl({ action: 'sc_add_user_customer' });
				var data = this.state;

				$.post(url, data, function (r) {

					if (r.success) {
						if (_this.props.createNewCustomerCaller !== undefined) {
							_this.props.createNewCustomerCaller.addCustomerToList(r);
						} else {
							document.location.reload();
						}
					} else {
						if (r.payload.email_used) {
							alertify.confirm(r.message,
							//Clicked OK
							function () {
								ReactDOM.findDOMNode(_this.refs.no_email_check).value = true;
								_this.submit(event);
							});
						}
					}
				});
			} else if (this.props.layout === 'edit-customer') {
				var url = window.stemcounter.aurl({ action: 'sc_edit_user_customer' });
				var data = this.state;
				$.post(url, data, function (response) {
					if (_this.props.eventId !== null) {
						_this.props.onCustomerEdit(response.payload.customer, _this.props.customerKey);
					} else {
						if (response.success) document.location.reload();
					}
				});
			}
		},

		render: function render() {
			var _this = this;
			var properties = null;

			var formControl = React.createElement(
				'div',
				{ className: 'from-btns' },
				React.createElement(
					'button',
					{ className: 'btn btn-primary button-submit', type: 'submit', onClick: this.submit },
					'Save'
				),
				React.createElement('input', { type: 'hidden', name: 'no_customer_list', value: this.props.noCustomerList })
			);

			var linkedEvents = false;
			if (this.state.customerLinkedEvents.length !== 0) {
				linkedEvents = this.state.customerLinkedEvents.map(function (event, index, arr) {
					return React.createElement(
						'div',
						{ key: event.id },
						React.createElement(
							'a',
							{ href: event.link, target: '_blank' },
							event.name
						)
					);
				});
			} else {
				linkedEvents = React.createElement(
					'div',
					null,
					'No Linked Events'
				);
			}

			var customer_source = null;
			if (stemcounter.AC.can_access(stemcounter.AC.STUDIO)) {
				customer_source = React.createElement(
					'div',
					{ className: 'modalpane-row clearfix' },
					React.createElement(
						'label',
						{ className: 'col-xs-12 col-sm-12 control-label modalpane' },
						React.createElement(
							'div',
							{ className: 'lbl' },
							'Customer Source'
						),
						React.createElement(
							'div',
							null,
							React.createElement('input', { className: 'form-control', name: 'customer_source', value: this.state.customer_source, onChange: this.handleChange.bind(null, 'customer_source') })
						)
					)
				);

				if (this.state.properties.length) {
					properties = React.createElement(
						'div',
						{ className: 'properties-list' },
						React.createElement(
							'h3',
							null,
							'Properties'
						),
						this.state.properties.map(function (prop, key, arr) {
							var values = _.pluck(prop.values, 'value').join('-');
							prop.editing = false;
							prop.title = prop.label;
							prop.type = prop.field_type;
							Object.keys(prop.settings).map(function (key) {
								prop[key] = prop.settings[key];
								return false;
							});

							return React.createElement(
								'div',
								{ className: 'property-wrap form-group row', key: 'property-' + prop.id + '-' + values },
								React.createElement(
									'div',
									{ className: 'col-sm-12' },
									React.createElement(ObjectField, {
										idList: 'customer_question_' + prop.id,
										index: prop.id,
										layout: 'customer',
										editProperty: _this.editProperty,
										version: false,
										dateFormat: _this.props.dateFormat,
										property: prop,
										viewing: true
									})
								)
							);
						})
					);
				}
			}

			return React.createElement(
				'div',
				null,
				React.createElement(
					'div',
					{ className: 'form-header' },
					React.createElement(
						'h3',
						null,
						'Customer'
					),
					formControl
				),
				React.createElement(
					'div',
					{ className: 'col-sm-12 customer-fields' },
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix' },
						React.createElement(
							'label',
							{ className: 'col-xs-12 control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'First Name',
								React.createElement('span', { className: 'mandatory' })
							),
							React.createElement(
								'div',
								null,
								React.createElement('input', { className: 'form-control', type: 'text', placeholder: 'First name', name: 'customer_first_name', value: this.state.customer_first_name, onChange: this.handleChange.bind(null, 'firstName') }),
								React.createElement('input', { type: 'hidden', name: 'customer_id', value: this.state.customer_id }),
								React.createElement('input', { type: 'hidden', name: 'event_id', value: this.state.eventId })
							)
						),
						React.createElement(
							'label',
							{ className: 'col-xs-12 control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Last Name',
								React.createElement('span', { className: 'mandatory' })
							),
							React.createElement(
								'div',
								null,
								React.createElement('input', { className: 'form-control', type: 'text', placeholder: 'Last name', name: 'customer_last_name', value: this.state.customer_last_name, onChange: this.handleChange.bind(null, 'lastName') })
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix' },
						React.createElement(
							'label',
							{ className: 'col-xs-12 control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Email'
							),
							React.createElement(
								'div',
								null,
								React.createElement('input', { type: 'email', className: 'form-control', placeholder: 'Add email here', name: 'customer_email', value: this.state.customer_email, onChange: this.handleChange.bind(null, 'email') })
							)
						),
						React.createElement(
							'label',
							{ className: 'col-xs-12 control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Phone Number'
							),
							React.createElement(
								'div',
								null,
								React.createElement('input', { type: 'text', className: 'form-control', placeholder: '555-555-5555', name: 'customer_phone', value: this.state.customer_phone, onChange: this.handleChange.bind(null, 'phone') })
							)
						)
					),
					customer_source,
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix' },
						React.createElement(
							'label',
							{ className: 'col-xs-12 col-sm-12 control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Address'
							),
							React.createElement(
								'div',
								null,
								React.createElement('input', { type: 'text', className: 'form-control', placeholder: 'Add address here', name: 'customer_address', value: this.state.customer_address, onChange: this.handleChange.bind(null, 'address') })
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix' },
						React.createElement(
							'label',
							{ className: 'col-xs-12 col-sm-12 control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Notes'
							),
							React.createElement(
								'div',
								null,
								React.createElement('textarea', { className: 'form-control', placeholder: 'Add notes here', name: 'customer_notes', value: this.state.customer_notes, onChange: this.handleChange.bind(null, 'note') })
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix' },
						React.createElement(
							'label',
							{ className: 'col-xs-12 col-sm-12 control-label modalpane' },
							React.createElement(
								'div',
								{ className: 'lbl' },
								'Linked Events'
							),
							React.createElement(
								'div',
								{ className: 'linked-events' },
								linkedEvents
							)
						)
					)
				),
				properties,
				React.createElement(
					'div',
					{ className: 'modal-footer' },
					React.createElement(
						'div',
						{ className: 'pull-left hide' },
						React.createElement('span', { className: 'mandatory' }),
						'required'
					),
					React.createElement('input', { type: 'hidden', name: 'no_email_check', ref: 'no_email_check', value: false })
				)
			);
		}
	});
	window.CustomerModal = CustomerModal;
})(jQuery);