'use strict';

// use a closure so we do not pollute the global scope
(function ($) {
	'use strict';

	var TaxRatesForm = React.createClass({
		displayName: 'TaxRatesForm',

		getInitialState: function getInitialState() {
			var rawRates = this.props.value;

			var rates = [];
			for (var i = 0; i < rawRates.length; i++) {
				var rate = rawRates[i];
				rate.msg = '';
				rates.push(this.createNewRate(rate.name, rate.value, rate.id, parseInt(rate.default, 10), parseInt(rate.default2, 10)));
			};

			return {
				rates: rates
			};
		},

		rateAutoIncrement: 0,
		createNewRate: function createNewRate(name, value, id, is_default, is_default2) {
			this.rateAutoIncrement++;

			var newRate = {
				id: id,
				key: this.rateAutoIncrement,
				name: name,
				value: value,
				msg: '',
				default: is_default,
				default2: is_default2
			};

			return newRate;
		},

		addMore: function addMore(e) {
			e.preventDefault();

			var updatedState = {
				rates: this.state.rates
			};

			updatedState.rates.push(this.createNewRate('', '0.00', null, this.state.rates.length ? 0 : 1, 0));

			this.setState(updatedState);
		},

		removeRate: function removeRate(rateIndex, e) {
			e.preventDefault();

			var updatedState = {
				rates: this.state.rates
			};

			updatedState.rates.splice(rateIndex, 1);

			this.setState(updatedState, this.submit);
		},

		onNameChange: function onNameChange(rateIndex, e) {
			var updatedState = {
				rates: this.state.rates
			};

			var name = e.target.value;
			updatedState.rates[rateIndex].name = name;

			this.setState(updatedState);
		},

		onRateChange: function onRateChange(rateIndex, e) {
			var updatedState = {
				rates: this.state.rates
			};

			var rate = e.target.value;

			if (rate < 1) {
				updatedState.rates[rateIndex].msg = 'Mark your amount in % format. ( 0.086 should be written as 8.6 )';
			} else {
				updatedState.rates[rateIndex].msg = '';
			}

			rate = stemcounter.parseFloatValue(rate);
			updatedState.rates[rateIndex].value = rate;

			this.setState(updatedState);
		},

		onDefaultRateChange: function onDefaultRateChange(rateIndex, e) {
			var updatedState = {
				rates: $.extend(true, [], this.state.rates)
			};

			if (undefined !== updatedState.rates[rateIndex]) {
				for (var i = 0; i < updatedState.rates.length; i++) {
					var element = updatedState.rates[i];
					if (rateIndex == i) {
						updatedState.rates[i].default = 1;
					} else {
						updatedState.rates[i].default = 0;
					}
				}

				this.setState(updatedState, this.submit);
			}
		},

		onDefaultRate2Change: function onDefaultRate2Change(rateIndex, e) {
			var updatedState = {
				rates: $.extend(true, [], this.state.rates)
			};

			if (undefined !== updatedState.rates[rateIndex]) {
				for (var i = 0; i < updatedState.rates.length; i++) {
					if (rateIndex == i) {
						updatedState.rates[i].default2 = e.target.checked ? 1 : 0;
					} else {
						updatedState.rates[i].default2 = 0;
					}
				}

				this.setState(updatedState, this.submit);
			}
		},

		submit: function submit() {

			var form = $(this.refs.form);
			var th = this;
			$.post(form.attr('action'), this.state).always(function (r) {
				var updatedState = {
					rates: $.extend(true, [], th.state.rates)
				};

				for (var i = 0; i < updatedState.rates.length; i++) {
					if (!updatedState.rates[i].id) {
						var new_rate = _.findWhere(r.payload.tax_rates, { key: updatedState.rates[i].key });
						if (new_rate) {
							updatedState.rates[i].id = new_rate.id;
						}
					}
				};

				th.setState(updatedState);
			});
		},

		render: function render() {
			var labels = null;
			var taxRates = [];
			var showBothRates = this.props.ratesCount == 2;

			for (var i = 0; i < this.state.rates.length; i++) {
				var rate = this.state.rates[i];

				taxRates.push(React.createElement(
					'div',
					{ className: 'row tax-rate-row pane-row', key: rate.key },
					React.createElement(
						'div',
						{ className: 'pane' },
						React.createElement('input', { type: 'text', className: 'form-control', value: rate.name, onChange: this.onNameChange.bind(this, i), onBlur: this.submit, placeholder: 'Tax name' })
					),
					React.createElement(
						'div',
						{ className: 'pane pane-rate' },
						React.createElement('input', { type: 'text', className: 'form-control', value: rate.value, onChange: this.onRateChange.bind(this, i), onBlur: this.submit }),
						React.createElement(
							'span',
							{ className: 'percentage-msg' },
							rate.msg
						)
					),
					React.createElement(
						'div',
						{ className: 'pane default-rate' },
						React.createElement(
							'div',
							null,
							React.createElement('input', { className: 'styled-radio', checked: rate.default, type: 'radio', value: 1, id: 'default_tax_' + rate.id, onChange: this.onDefaultRateChange.bind(this, i) }),
							React.createElement('label', { htmlFor: 'default_tax_' + rate.id })
						),
						showBothRates ? null : React.createElement(
							'a',
							{ href: '#', className: 'btn-remove', onClick: this.removeRate.bind(this, i) },
							React.createElement('i', { className: 'fa fa-trash-o' })
						)
					),
					showBothRates ? React.createElement(
						'div',
						{ className: 'pane default-rate' },
						React.createElement(
							'div',
							null,
							React.createElement('input', { className: 'styled-radio', checked: rate.default2, type: 'checkbox', value: 1, id: 'default_tax2_' + rate.id, onChange: this.onDefaultRate2Change.bind(this, i) }),
							React.createElement('label', { htmlFor: 'default_tax2_' + rate.id })
						),
						React.createElement(
							'a',
							{ href: '#', className: 'btn-remove', onClick: this.removeRate.bind(this, i) },
							React.createElement('i', { className: 'fa fa-trash-o' })
						)
					) : null
				));
			};
			if (taxRates.length == 0) {
				labels = null;
				taxRates = React.createElement(
					'p',
					null,
					"You don't have any tax rates, yet."
				);
			} else {
				labels = React.createElement(
					'div',
					{ className: 'row tax-rate-row pane-row' },
					React.createElement(
						'div',
						{ className: 'pane' },
						React.createElement(
							'label',
							{ className: 'control-label' },
							'Name'
						)
					),
					React.createElement(
						'div',
						{ className: 'pane pane-rate' },
						React.createElement(
							'label',
							{ className: 'control-label' },
							'Tax %'
						)
					),
					React.createElement(
						'div',
						{ className: 'pane default-rate' },
						React.createElement(
							'label',
							{ className: 'control-label' },
							'Default'
						)
					),
					showBothRates ? React.createElement(
						'div',
						{ className: 'pane default-rate' },
						React.createElement(
							'label',
							{ className: 'control-label' },
							'Default Second Rate'
						)
					) : null
				);
			}

			var url = stemcounter.aurl({
				'action': 'sc_edit_tax_rates'
			});

			return React.createElement(
				'form',
				{ action: url, method: 'post', onSubmit: this.submit, ref: 'form' },
				React.createElement(
					'div',
					{ className: 'pane-row' },
					React.createElement(
						'a',
						{ className: 'add-tax-button cta-link', onClick: this.addMore },
						'+ New Tax Rate'
					)
				),
				React.createElement(
					'div',
					{ className: 'row no-gutter' },
					labels,
					taxRates
				)
			);
		}
	});

	$(document).ready(function () {
		var formWrapper = $('.edit-tax-rates-form-wrapper:first');
		if (formWrapper.length == 1) {
			var value = formWrapper.data('value');
			ReactDOM.render(React.createElement(TaxRatesForm, { value: value, ratesCount: formWrapper.data('rateCount') }), formWrapper.get(0));
		}
	});
})(jQuery);