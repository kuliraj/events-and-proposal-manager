'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function ($) {
	'use strict';

	var ICFController = function (_SC_Component) {
		_inherits(ICFController, _SC_Component);

		function ICFController(props) {
			_classCallCheck(this, ICFController);

			var _this2 = _possibleConstructorReturn(this, Object.getPrototypeOf(ICFController).call(this, props));

			var state = { edit_mode: false };
			state.userForms = _this2.props.userForms ? _this2.props.userForms : {};
			state.currentSchema = {};

			_this2.state = state;
			return _this2;
		}

		_createClass(ICFController, [{
			key: 'getProperty',
			value: function getProperty(fieldType) {
				if (undefined === fieldType) {
					return;
				}
				var property;

				if ('date' == fieldType) {
					property = {
						default: false,
						description: '',
						format: 'date',
						required: 0,
						title: 'Question ',
						type: 'input',
						property_type: ''
					};
				} else if ('single-line' == fieldType) {
					property = {
						default: false,
						description: '',
						format: 'string',
						required: 0,
						title: 'Question ',
						type: 'input',
						property_type: ''
					};
				} else if ('multi-line' == fieldType) {
					property = {
						default: false,
						description: '',
						format: 'string',
						required: 0,
						title: 'Question ',
						type: 'textarea',
						property_type: ''
					};
				} else if ('checkbox' == fieldType) {
					property = {
						default: false,
						description: '',
						format: 'boolean',
						required: 0,
						title: 'Question ',
						type: 'checkbox',
						property_type: ''
					};
				} else if ('multi-checkbox' == fieldType) {
					property = {
						aopt: ['Option 1', 'Option 2', 'Option 3'],
						default: false,
						description: '',
						format: 'boolean',
						required: 0,
						title: 'Question ',
						type: 'multi-checkbox',
						property_type: ''
					};
				} else if ('number' == fieldType) {
					property = {
						default: false,
						description: '',
						format: 'number',
						required: 0,
						title: 'Question ',
						type: 'input',
						property_type: ''
					};
				} else if ('radio' == fieldType) {
					property = {
						aopt: ['Option 1', 'Option 2', 'Option 3'],
						default: false,
						description: '',
						format: 'boolean',
						required: 0,
						title: 'Question ',
						type: 'radio',
						property_type: ''
					};
				}

				property.internal_field = null;

				return property;
			}
		}, {
			key: 'createNewForm',
			value: function createNewForm(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state);
				updatedState.edit_mode = true;
				updatedState.currentSchema = {
					id: -1,
					title: 'Untitled form',
					description: 'Enter some description',
					properties: []
				};
				this.setState(updatedState);
			}
		}, {
			key: 'onPropToForm',
			value: function onPropToForm(property) {
				if (undefined == property) {
					console.log('Can\'t add property to form');
				} else {
					this.form.onPropFromForm(property);
				}
			}
		}, {
			key: 'browseFormsProperties',
			value: function browseFormsProperties(property) {
				var formsTitiles = [];
				if (undefined !== property) {

					for (var i = 0; i < this.state.userForms.length; i++) {
						var form = this.state.userForms[i];
						for (var j = 0; j < form.properties.length; j++) {
							var _property = form.properties[j];
							if (parseInt(_property.property_id, 10) == parseInt(property.id, 10)) {
								formsTitiles.push({
									form: form.title,
									property: _property.title
								});
							}
						}
					}
				}
				return formsTitiles;
			}
		}, {
			key: 'formDeletedProperty',
			value: function formDeletedProperty(property) {
				this.childProp.formDeletedProperty(property);
			}
		}, {
			key: 'formReset',
			value: function formReset() {
				this.childProp.formReset();
			}
		}, {
			key: 'formSaved',
			value: function formSaved(schema) {
				var updatedState = $.extend(true, {}, this.state);
				var qwe = false;
				for (var i = 0; i < updatedState.userForms.length; i++) {
					if (parseInt(updatedState.userForms[i].id, 10) == parseInt(schema.id, 10)) {
						updatedState.userForms[i] = schema;
						qwe = true;
						break;
					}
				}

				if (false === qwe) {
					updatedState.userForms.push(schema);
				}
				updatedState.edit_mode = false;
				this.setState(updatedState, this.childProp.formReset());
			}
		}, {
			key: 'onFormEdit',
			value: function onFormEdit(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state),
				    id = 'A' == e.target.tagName ? parseInt($(e.target).closest('li').data('id'), 10) : parseInt(e.target.dataset.id, 10);

				for (var i = 0; i < Object.keys(this.state.userForms).length; i++) {
					if (id == parseInt(this.state.userForms[i].id, 10)) {
						updatedState.currentSchema = this.state.userForms[i];
						updatedState.edit_mode = true;
						Object.keys(updatedState.currentSchema.properties).map(function (key) {
							updatedState.currentSchema.properties[key].editing = false;
							return false;
						});
						break;
					}
				}

				this.setState(updatedState, this.childProp.formLoaded(updatedState.currentSchema.properties));
			}
		}, {
			key: 'onBackClick',
			value: function onBackClick(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state);
				updatedState.edit_mode = false;
				this.childProp.formReset();
				this.setState(updatedState);
			}
		}, {
			key: 'formDeleted',
			value: function formDeleted(id) {
				var updatedState = $.extend(true, {}, this.state);
				if (undefined !== id) {
					for (var i = 0; i < updatedState.userForms.length; i++) {
						if (parseInt(id, 10) == parseInt(updatedState.userForms[i].id, 10)) {
							updatedState.userForms.splice(i, 1);
						}
					}
					updatedState.edit_mode = false;
					this.setState(updatedState, this.childProp.formReset());
				}
			}
		}, {
			key: 'propToggle',
			value: function propToggle(e) {
				e.preventDefault();
				if ($(e.target).closest('.properties-wrapper').hasClass('on')) {
					$(e.target).closest('.properties-wrapper').removeClass('on');
				} else {
					$(e.target).closest('.properties-wrapper').addClass('on');
				}
			}
		}, {
			key: 'onFormURLClick',
			value: function onFormURLClick(e) {
				e.stopPropagation();
			}
		}, {
			key: 'render',
			value: function render() {
				var _this3 = this;

				var formPanel = null;

				if (this.state.edit_mode) {
					formPanel = React.createElement(FormBuilder, {
						onRef: function onRef(ref) {
							return _this3.form = ref;
						},
						dateFormat: this.props.dateFormat,
						getProperty: this.getProperty,
						formDeletedProperty: this.formDeletedProperty,
						formReset: this.formReset,
						onBackClick: this.onBackClick,
						formSaved: this.formSaved,
						schema: this.state.currentSchema,
						formDeleted: this.formDeleted
					});
				} else {
					formPanel = React.createElement(
						'div',
						{ className: 'studio-forms' },
						React.createElement(
							'a',
							{ className: 'add-form-button cta-link', onClick: this.createNewForm },
							'+ Add New Form'
						),
						React.createElement(
							'ul',
							{ id: 'user-forms' },
							this.state.userForms.map(function (form, key) {
								return React.createElement(
									'li',
									{ key: 'form-' + key, className: 'user-form', 'data-id': form.id, onClick: this.onFormEdit, title: form.title },
									React.createElement(
										'a',
										{ href: '#', className: 'sc-primary-color' },
										form.title
									),
									React.createElement(
										'a',
										{ href: form.url, className: 'sc-primary-color', target: '_blank', onClick: this.onFormURLClick },
										React.createElement('i', { className: 'fa-fw fa fa-link' })
									)
								);
							}.bind(this))
						)
					);
				}

				return React.createElement(
					'div',
					{ className: 'ifc-container' },
					React.createElement(
						'div',
						{ className: 'row' },
						React.createElement(
							'div',
							{ className: 'col-xs-8' },
							formPanel
						),
						React.createElement(
							'div',
							{ id: 'sliding-panel', className: 'properties-wrapper col-xs-4 on' },
							React.createElement(
								'div',
								{ className: 'panel-toggler' },
								React.createElement(
									'div',
									{ className: 'vertical-text' },
									'Properties'
								),
								React.createElement('div', { className: 'arrow' })
							),
							React.createElement(PropBuilder, {
								onRef: function onRef(ref) {
									return _this3.childProp = ref;
								},
								dateFormat: this.props.dateFormat,
								edit_mode: this.state.edit_mode,
								getProperty: this.getProperty,
								onPropToForm: this.onPropToForm,
								properties: this.props.userProperties,
								browseFormsProperties: this.browseFormsProperties
							})
						)
					)
				);
			}
		}]);

		return ICFController;
	}(SC_Component);

	var FormBuilder = function (_SC_Component2) {
		_inherits(FormBuilder, _SC_Component2);

		function FormBuilder(props) {
			_classCallCheck(this, FormBuilder);

			var _this4 = _possibleConstructorReturn(this, Object.getPrototypeOf(FormBuilder).call(this, props));

			var state = {};
			if (_this4.props.schema && Object.keys(_this4.props.schema).length !== 0) {
				state.schema = _this4.props.schema;
			} else {
				state.schema = {
					id: -1,
					title: 'Untitled form',
					description: 'Enter some description',
					properties: []
				};
			}

			_this4.state = state;
			return _this4;
		}

		_createClass(FormBuilder, [{
			key: 'componentWillReceiveProps',
			value: function componentWillReceiveProps(nextProps) {
				var updatedState = $.extend(true, {}, this.state);
				if (nextProps.schema) {
					updatedState.schema = nextProps.schema;
					this.setState(updatedState);
				}
			}
		}, {
			key: 'componentDidMount',
			value: function componentDidMount() {
				this.props.onRef(this);
			}
		}, {
			key: 'componentWillUnmount',
			value: function componentWillUnmount() {
				this.props.onRef(undefined);
			}
		}, {
			key: 'onResetForm',
			value: function onResetForm(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state);
				updatedState.schema = {
					'id': updatedState.schema.id,
					title: updatedState.schema.title,
					description: updatedState.schema.description,
					properties: []
				};
				this.setState(updatedState, this.props.formReset());
			}
		}, {
			key: 'onDeleteForm',
			value: function onDeleteForm(e) {
				var _this = this,
				    url = window.stemcounter.aurl({ action: 'sc_form_delete' }),
				    data = {
					id: parseInt(this.state.schema.id, 10)
				};

				$.post(url, data, function (response) {
					stemcounter.JSONResponse(response, function (r) {
						if (!r.success) {
							alertify.error(r.message);
						} else {
							_this.props.formDeleted(parseInt(_this.state.schema.id, 10));
							alertify.success(r.payload.message);
						}
					});
				});
			}
		}, {
			key: 'onSaveForm',
			value: function onSaveForm(e) {
				e.preventDefault();
				if ('' == this.state.schema.title) {
					alertify.error('Please enter a title for your form');
					return;
				}
				if (this.state.schema.properties.length === 0) {
					alertify.error('Your form doesn\'t have property fields');
					return;
				}
				var _this = this,
				    url = window.stemcounter.aurl({ action: 'sc/form/edit' }),
				    data = {
					schema: this.state.schema
				};

				$.post(url, data, function (response) {
					stemcounter.JSONResponse(response, function (r) {
						if (!r.success) {
							alertify.error(r.message);
						} else {
							if (r.payload.schema) {
								var updatedState = $.extend(true, {}, _this.state);
								updatedState.schema.id = r.payload.schema.id;
								updatedState.schema.url = r.payload.schema.url;
								Object.keys(updatedState.schema.properties).map(function (key) {
									if (-1 == parseInt(updatedState.schema.properties[key].id, 10)) {
										updatedState.schema.properties[key].id = parseInt(r.payload.schema.properties[key].id, 10);
									}
									return false;
								});

								_this.props.formSaved(updatedState.schema);
								alertify.success(r.payload.message);
							}
						}
					});
				});
			}
		}, {
			key: 'onPropFromForm',
			value: function onPropFromForm(property) {
				var updatedState = $.extend(true, {}, this.state);
				var index = updatedState.schema.properties ? Object.keys(updatedState.schema.properties).length : 0;
				index = index + 1;
				property.property_id = property.id;
				property.id = -1;
				property.required = 0;
				property.editing = false;
				property.order = updatedState.schema.properties.length;
				updatedState.schema.properties.push(property);
				this.setState(updatedState);
			}
		}, {
			key: 'onFieldAdd',
			value: function onFieldAdd(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state);
				var fieldType = e.target.dataset.type;
				var index = updatedState.schema.properties ? updatedState.schema.properties.length : 0;
				index = index + 1;
				var property = this.props.getProperty(fieldType);
				property.title = property.title + index;
				updatedState.schema.properties.push(property);
				this.setState(updatedState);
			}
		}, {
			key: 'submitProperty',
			value: function submitProperty(key, property) {
				var updatedState = $.extend(true, {}, this.state);
				if (undefined !== key && undefined !== property) {
					updatedState.schema.properties[key] = property;
				}
				this.setState(updatedState);
			}
		}, {
			key: 'deleteProperty',
			value: function deleteProperty(key) {
				var updatedState = $.extend(true, {}, this.state);
				if (undefined !== key) {
					this.props.formDeletedProperty(updatedState.schema.properties[key]);
					updatedState.schema.properties.splice(key, 1);
				}

				this.setState(updatedState);
			}
			// changeProperty(key, type, property) {
			// 	var updatedState = $.extend(true, {}, this.state);
			// 	if (undefined !== key && undefined !== type && undefined !== property) {
			// 		updatedState.schema.properties[key] = this.props.getProperty(type);
			// 		updatedState.schema.properties[key].title = property.title;
			// 		updatedState.schema.properties[key].description = property.description;
			// 	}
			// 	this.setState(updatedState);
			// }

		}, {
			key: 'onChangeField',
			value: function onChangeField(type, value) {
				var updatedState = $.extend(true, {}, this.state);
				if ('title' == type) {
					updatedState.schema.title = value;
				} else if ('description' == type) {
					updatedState.schema.description = value;
				} else if ('confirmation_msg' == type) {
					updatedState.schema.confirmation_msg = value;
				}

				this.setState(updatedState);
			}
		}, {
			key: 'moveField',
			value: function moveField(index, direction) {
				var updatedState = $.extend(true, {}, this.state);

				if (undefined === updatedState.schema.properties[index]) {
					return;
				}

				var filteredIndex = -1,
				    order = 0;

				if ('up' == direction) {
					if (index == 0) {
						return;
					}

					for (var i = 0; i < updatedState.schema.properties.length; i++) {
						filteredIndex++;

						// If this is the previous arrangement, move it down
						if (index - 1 == filteredIndex) {
							updatedState.schema.properties[i].order = order + 1;
						} else if (filteredIndex == index) {
							// If it's the current arrangement, move it up
							updatedState.schema.properties[i].order = order - 1;
						} else {
							// Otherwise just set the order
							updatedState.schema.properties[i].order = order;
						}

						order++;
					}
				} else {
					if (index == updatedState.schema.properties.length - 1) {
						return;
					}

					for (var i = 0; i < updatedState.schema.properties.length; i++) {
						filteredIndex++;

						// If this is the next arrangement, move it up
						if (index + 1 == filteredIndex) {
							updatedState.schema.properties[i].order = order - 1;
						} else if (filteredIndex == index) {
							// If it's the current arrangement, move it down
							updatedState.schema.properties[i].order = order + 1;
						} else {
							// Otherwise just set the order
							updatedState.schema.properties[i].order = order;
						}

						order++;
					}
				}

				updatedState.schema.properties.sort(function (a, b) {
					if (a.order == b.order) {
						return 0;
					}

					return a.order < b.order ? -1 : 1;
				});

				this.setState(updatedState);
			}
		}, {
			key: 'render',
			value: function render() {
				var _this = this;
				return React.createElement(
					'div',
					{ className: 'builder-form col-sm-12 center' },
					React.createElement(SchemaFields, {
						schema: this.state.schema,
						onChangeField: this.onChangeField,
						dateFormat: this.props.dateFormat,
						deleteProperty: this.deleteProperty,
						submitProperty: this.submitProperty,
						changeProperty: this.changeProperty,
						onBackClick: this.props.onBackClick,
						viewing: false,
						moveField: this.moveField
					}),
					React.createElement(
						'div',
						{ className: 'row form-actions' },
						React.createElement(
							'button',
							{ type: 'button', className: 'btn btn-primary btn-delete-form pull-left', onClick: function onClick(e) {
									var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0];
									e.preventDefault();
									stemcounter.confirmModal({
										confirmCallback: function confirmCallback() {
											_this.onDeleteForm();
										}
									});
								} },
							React.createElement('i', { className: 'fa-fw fa fa-trash-o', 'aria-hidden': 'true' }),
							'Delete Form'
						),
						React.createElement(
							'button',
							{ type: 'button', onClick: this.onResetForm, className: 'btn btn-primary btn-reset-form pull-left' },
							React.createElement('i', { className: 'fa-fw fa fa-times', 'aria-hidden': 'true' }),
							'Reset Form'
						),
						React.createElement(
							'button',
							{ type: 'button', onClick: this.onSaveForm, className: 'btn btn-primary btn-save-form pull-right' },
							React.createElement('i', { className: 'fa-fw fa fa-pencil-square-o', 'aria-hidden': 'true' }),
							'Save your form'
						)
					)
				);
			}
		}]);

		return FormBuilder;
	}(SC_Component);

	var SchemaFields = function (_SC_Component3) {
		_inherits(SchemaFields, _SC_Component3);

		function SchemaFields(props) {
			_classCallCheck(this, SchemaFields);

			var _this5 = _possibleConstructorReturn(this, Object.getPrototypeOf(SchemaFields).call(this, props));

			_this5.state = {};
			return _this5;
		}

		_createClass(SchemaFields, [{
			key: 'onConfirmationChanged',
			value: function onConfirmationChanged(content) {
				this.props.onChangeField('confirmation_msg', content);
			}
		}, {
			key: 'renderHeader',
			value: function renderHeader() {
				if (this.props.viewing) {
					return React.createElement(
						'h2',
						null,
						this.props.schema.title
					);
				} else {
					return React.createElement(
						'div',
						null,
						React.createElement(SInpit, { type: 'title', value: this.props.schema.title, onChangeField: this.props.onChangeField }),
						React.createElement(
							'a',
							{ href: '#', onClick: this.props.onBackClick, className: 'sc-primary-color back-btn' },
							'Back',
							React.createElement('i', { className: 'fa-fw fa fa-arrow-right', 'aria-hidden': 'true' })
						),
						React.createElement(
							'div',
							{ className: 'form-thank-you form-group row' },
							React.createElement(
								'label',
								null,
								'Confirmation Message(shown after form is submitted) ',
								React.createElement('span', { className: 'mandatory' })
							),
							React.createElement(ReactQuill, {
								theme: 'snow',
								value: this.props.schema.confirmation_msg,
								onChange: this.onConfirmationChanged,
								modules: {
									toolbar: [[{ 'header': [1, 2, 3, false] }], ['bold', 'italic', 'underline', 'link', { 'align': [] }], [{ 'list': 'ordered' }, { 'list': 'bullet' }, 'clean']]
								}
							})
						)
					);
				}
			}
		}, {
			key: 'render',
			value: function render() {
				var total_fields = this.props.schema.properties.length - 1;

				return React.createElement(
					'div',
					{ className: 'form-group' },
					this.renderHeader(),
					this.props.schema.properties.map(function (property, key) {
						return React.createElement(ObjectField, {
							index: key,
							key: 'field-object-' + key + '-' + property.id + '-' + property.title,
							layout: 'form',
							dateFormat: this.props.dateFormat,
							property: property,
							onChangeField: this.props.onChangeField,
							deleteProperty: this.props.deleteProperty,
							submitProperty: this.props.submitProperty,
							changeProperty: this.props.changeProperty,
							isFirst: 0 == key,
							isLast: total_fields == key,
							viewing: this.props.viewing,
							move: this.props.moveField
						});
					}.bind(this))
				);
			}
		}]);

		return SchemaFields;
	}(SC_Component);

	var ObjectField = function (_SC_Component4) {
		_inherits(ObjectField, _SC_Component4);

		function ObjectField(props) {
			_classCallCheck(this, ObjectField);

			var _this6 = _possibleConstructorReturn(this, Object.getPrototypeOf(ObjectField).call(this, props));

			_this6.state = {
				editing: _this6.props.property.hasOwnProperty('editing') ? _this6.props.property.editing : true,
				property: _this6.props.property ? _this6.props.property : {},
				saving: false
			};

			_this6.throttledSubmitProperty = _.throttle(_this6.throttledSubmitProperty, 300);
			return _this6;
		}

		_createClass(ObjectField, [{
			key: 'componentWillReceiveProps',
			value: function componentWillReceiveProps(nextProps) {
				if (nextProps.property && nextProps.property.type != this.state.property.type && this.state.editing) {
					nextProps.property.property_type = this.state.property_type;
					this.setState({
						editing: nextProps.property.hasOwnProperty('editing') ? nextProps.property.editing : true,
						property: nextProps.property ? nextProps.property : {}
					});
				}
			}
		}, {
			key: 'componentDidMount',
			value: function componentDidMount() {}
		}, {
			key: 'throttledSubmitProperty',
			value: function throttledSubmitProperty() {
				this.props.submitProperty(this.props.index, this.state.property);
			}
		}, {
			key: 'componentDidUpdate',
			value: function componentDidUpdate(prevProps, prevState) {
				// Auto-save any changes to properties - otherwise if you didn't get out of property
				// edit UI and directly click on Save your form, your changes will be lost :(
				if ('modal' != this.props.layout && prevState.editing && this.state.editing) {
					var needs_submit = false,
					    _this = this,
					    keys = Object.keys(this.state.property),
					    total_keys = keys.length,
					    key,
					    i;

					for (i = 0; i < total_keys; i++) {
						key = keys[i];
						if (this.state.property[key] != prevProps.property[key]) {
							needs_submit = true;
							break;
						}
					}

					if (needs_submit) {
						this.throttledSubmitProperty();
					}
				}
			}
		}, {
			key: 'onEdit',
			value: function onEdit(e) {
				e.preventDefault();
				this.setState({
					editing: true
				});
			}
		}, {
			key: 'onClose',
			value: function onClose(e) {
				e.preventDefault();

				// field validarion here!
				if ('modal' == this.props.layout) {
					this.setState({
						editing: false
					}, this.initPropertyDatePicker());
				} else {
					this.setState({
						editing: false
					}, this.props.submitProperty(this.props.index, this.state.property), this.initPropertyDatePicker());
				}
			}
		}, {
			key: 'onPropSubmit',
			value: function onPropSubmit(e) {
				e.preventDefault();
				// field validarion here!
				if ('modal' == this.props.layout) {
					this.setState({
						saving: true
					});
					this.props.submitProperty(this.props.index, this.state.property);
				} else {
					this.setState({
						editing: false
					}, this.props.submitProperty(this.props.index, this.state.property), this.initPropertyDatePicker());
				}
			}
		}, {
			key: 'onPropChange',
			value: function onPropChange(e) {
				e.preventDefault();
				var type = e.target.dataset.type;
				if (undefined !== type) {
					this.props.changeProperty(this.props.index, type, this.state.property);
				}
			}
		}, {
			key: 'onTitleChange',
			value: function onTitleChange(e) {
				var updatedState = $.extend(true, {}, this.state);
				updatedState.property.title = e.target.value;

				// Questions don't get placeholders
				if (!this.state.property.property_id) {
					if (!updatedState.property.id) {
						updatedState.property.placeholder_tag = this.props.createPlaceholder(updatedState.property.id, updatedState.property.title);
					}
				}

				this.setState(updatedState);
			}
		}, {
			key: 'onDescriptionChange',
			value: function onDescriptionChange(e) {
				var updatedState = $.extend(true, {}, this.state);
				updatedState.property.description = e.target.value;
				this.setState(updatedState);
			}
		}, {
			key: 'onRequired',
			value: function onRequired(e) {
				var updatedState = $.extend(true, {}, this.state);
				updatedState.property.required = false == e.target.checked ? 0 : 1;
				this.setState(updatedState);
			}
		}, {
			key: 'onOptionChange',
			value: function onOptionChange(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state);
				var optIndex = $(e.target).closest('.field-option').data('index');

				if (undefined !== optIndex) {
					updatedState.property.aopt[optIndex] = e.target.value;
				}
				this.setState(updatedState);
			}
		}, {
			key: 'delOption',
			value: function delOption(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state);
				var optIndex = $(e.target).closest('.field-option').data('index');

				if (undefined !== optIndex) {
					updatedState.property.aopt.splice(optIndex, 1);
				}
				this.setState(updatedState);
			}
		}, {
			key: 'addOption',
			value: function addOption(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state);

				updatedState.property.aopt.push('Option ' + (updatedState.property.aopt.length + 1));
				this.setState(updatedState);
			}
		}, {
			key: 'handleDate',
			value: function handleDate(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state);

				updatedState.property.date = e.target.value;
				this.setState(updatedState);
			}
		}, {
			key: 'onTypeChange',
			value: function onTypeChange(e) {
				e.preventDefault();
				var updatedState = $.extend(true, {}, this.state);

				updatedState.property.property_type = e.target.value;
				this.setState(updatedState);
			}
		}, {
			key: 'onDeleteButtonClick',
			value: function onDeleteButtonClick(e) {
				var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0],
				    _this = this;

				e.preventDefault();
				e.stopPropagation();

				stemcounter.confirmModal({
					confirmCallback: function confirmCallback() {
						_this.props.deleteProperty(_this.props.index);
					}
				});
			}
		}, {
			key: 'onMoveUp',
			value: function onMoveUp(e) {
				e.preventDefault();
				e.stopPropagation();

				this.props.move(this.props.index, 'up');
			}
		}, {
			key: 'onMoveDown',
			value: function onMoveDown(e) {
				e.preventDefault();
				e.stopPropagation();

				this.props.move(this.props.index, 'down');
			}
		}, {
			key: 'editedProperty',
			value: function editedProperty(e) {
				if (!this.props.version && 'proposal' == this.props.layout || 'customer' == this.props.layout) {
					this.props.editProperty(e, this.state.property);
				} else {
					return;
				}
			}
		}, {
			key: 'render',
			value: function render() {
				var _this = this,
				    field = null,
				    property = $.extend(true, {}, this.state.property),
				    fieldOptions = null,
				    fieldDescription = null,
				    fieldProperty = null,
				    requiredLabel = null,
				    required = false,
				    panelHeading = null,
				    panelFooter = null,
				    fieldPropertyType = null,
				    fieldRequired = null,
				    modalFooter = null,
				    placeholderTag = null,
				    internalProp = null,
				    fieldContainerClass = this.props.viewing ? 'col-sm-12' : 'col-sm-9',
				    readOnly = this.props.viewing ? false : true,
				    moveUp = null,
				    moveDown = null,
				    moveButtons = null,
				    values = null,
				    disabled = false;

				if ('form' == this.props.layout || 'proposal' == this.props.layout || 'customer' == this.props.layout) {
					if (!this.props.isFirst) {
						moveUp = React.createElement(
							'button',
							{ className: 'btn btn-link btn-up', onClick: this.onMoveUp },
							React.createElement('i', { className: 'fa-fw fa fa-arrow-up', 'aria-hidden': 'true' })
						);
					}

					if (!this.props.isLast) {
						moveDown = React.createElement(
							'button',
							{ className: 'btn btn-link btn-down', onClick: this.onMoveDown },
							React.createElement('i', { className: 'fa-fw fa fa-arrow-down', 'aria-hidden': 'true' })
						);
					}

					moveButtons = React.createElement(
						'div',
						{ className: 'pull-right move-buttons' },
						moveUp,
						moveDown
					);
				}

				if (this.state.editing) {

					if ('form' == this.props.layout || 'proposal' == this.props.layout || 'customer' == this.props.layout) {
						panelHeading = React.createElement(
							'div',
							{ className: 'panel-heading clearfix', onClick: this.onClose },
							React.createElement(
								'strong',
								{ className: 'panel-title' },
								'Edit Question'
							),
							React.createElement(
								'div',
								{ className: 'pull-right btn-toolbar' },
								React.createElement(
									'button',
									{ className: 'btn btn-link', onClick: this.onDeleteButtonClick },
									'delete ',
									React.createElement('i', { className: 'fa-fw fa fa-trash-o', 'aria-hidden': 'true' })
								),
								React.createElement(
									'button',
									{ className: 'btn btn-link', onClick: this.onClose },
									'close ',
									React.createElement('i', { className: 'fa-fw fa fa-times-circle', 'aria-hidden': 'true' })
								),
								moveButtons
							)
						);

						fieldRequired = React.createElement(
							'div',
							{ className: 'form-group' },
							React.createElement(
								'div',
								{ className: 'checkbox' },
								React.createElement(
									'label',
									{ className: 'control-label' },
									React.createElement('input', { type: 'checkbox', value: 'on', defaultChecked: parseInt(property.required, 10), onChange: this.onRequired }),
									React.createElement(
										'strong',
										null,
										'required'
									)
								)
							)
						);
					} else if ('modal' == this.props.layout) {
						var deleteProp = null;
						if (-1 == this.props.index) {
							panelHeading = React.createElement(
								'div',
								{ className: 'panel-heading clearfix' },
								React.createElement(
									'div',
									{ className: 'pull-right btn-toolbar' },
									React.createElement(
										'div',
										{ className: 'dropdown pull-right' },
										React.createElement(
											'button',
											{ type: 'button', className: 'btn btn-link', 'data-toggle': 'dropdown' },
											'change field ',
											React.createElement('i', { className: 'fa-fw fa fa-cog', 'aria-hidden': 'true' })
										),
										React.createElement(
											'ul',
											{ className: 'dropdown-menu' },
											React.createElement(
												'li',
												null,
												React.createElement(
													'a',
													{ href: '#', 'data-type': 'date', onClick: this.onPropChange },
													'Date'
												)
											),
											React.createElement(
												'li',
												null,
												React.createElement(
													'a',
													{ href: '#', 'data-type': 'single-line', onClick: this.onPropChange },
													'Single Line Text'
												)
											),
											React.createElement(
												'li',
												null,
												React.createElement(
													'a',
													{ href: '#', 'data-type': 'multi-line', onClick: this.onPropChange },
													'Multiple Line'
												)
											),
											React.createElement(
												'li',
												null,
												React.createElement(
													'a',
													{ href: '#', 'data-type': 'checkbox', onClick: this.onPropChange },
													'Single Checkbox'
												)
											),
											React.createElement(
												'li',
												null,
												React.createElement(
													'a',
													{ href: '#', 'data-type': 'multi-checkbox', onClick: this.onPropChange },
													'Multiple Checkboxes'
												)
											),
											React.createElement(
												'li',
												null,
												React.createElement(
													'a',
													{ href: '#', 'data-type': 'number', onClick: this.onPropChange },
													'Number'
												)
											),
											React.createElement(
												'li',
												null,
												React.createElement(
													'a',
													{ href: '#', 'data-type': 'radio', onClick: this.onPropChange },
													'Radio Select'
												)
											)
										)
									)
								)
							);
						} else {
							panelHeading = React.createElement(
								'div',
								{ className: 'panel-heading clearfix' },
								React.createElement(
									'div',
									{ className: 'pull-right btn-toolbar' },
									React.createElement(
										'p',
										{ className: 'pull-right' },
										property.type
									)
								)
							);
						}

						if (null === property.internal_field) {
							var deleteProp = React.createElement(
								'button',
								{ className: 'btn btn-delete', onClick: function onClick(e) {
										var el = 'A' === e.target.nodeName ? e.target : $(e.target).closest('a')[0];
										e.preventDefault();
										stemcounter.confirmModal({
											confirmCallback: function confirmCallback() {
												_this.props.deleteProperty(_this.props.index);
											}
										});
									} },
								'Delete'
							);
						}

						modalFooter = React.createElement(
							'div',
							{ className: 'modal-footer' },
							React.createElement(
								'div',
								null,
								deleteProp,
								React.createElement(
									'button',
									{ className: 'btn btn-primary btn-submit', type: 'button', onClick: this.onPropSubmit, disabled: this.state.saving },
									'Save'
								)
							)
						);

						fieldPropertyType = React.createElement(
							'div',
							{ className: 'form-group' },
							React.createElement(
								'label',
								null,
								'Property Type',
								React.createElement('span', { className: 'mandatory' })
							),
							' ',
							React.createElement(SC_Tooltip, { tooltip: 'A property is a key information piece about a customer or a job. You already have some customer properties such as "First name" or "last name", however, you might want to have a place for more information.' + stemcounter.new_lines(2) + 'There are two types of properties: a customer property or a job property. For example, you can add a JOB property called "Number of bridesmaids" that will let you know how many bridesmaids that a wedding should have. You can choose to add this property on a form for weddings because your corporate event clients wouldn\'t need this question.' }),
							React.createElement(ReactSelect2, {
								className: '',
								children: [React.createElement(
									'option',
									{ key: 'event', value: 'event' },
									'Job'
								), React.createElement(
									'option',
									{ key: 'customer', value: 'customer' },
									'Customer'
								)],
								disabled: null !== property.internal_field,
								selectedValue: property.property_type,
								onChange: this.onTypeChange
							})
						);

						placeholderTag = React.createElement(
							'div',
							{ className: 'form-group' },
							React.createElement(
								'label',
								null,
								'Placeholder Tag'
							),
							React.createElement('input', { type: 'text', className: 'form-control', value: property.placeholder_tag, onChange: $.noop, readOnly: true })
						);
					}

					if ('modal' == this.props.layout && property.aopt && property.aopt.length) {
						fieldOptions = React.createElement(
							'div',
							{ className: 'form-group' },
							React.createElement(
								'div',
								{ className: 'panel-header' },
								'Options'
							),
							React.createElement(
								'div',
								{ className: 'row field-options' },
								property.aopt.map(function (option, key, arr) {
									var delOptBtn = React.createElement(
										'div',
										{ className: 'col-xs-3' },
										React.createElement(
											'button',
											{ className: 'form-control', onClick: this.delOption },
											'delete ',
											React.createElement('i', { className: 'fa-fw fa fa-trash-o', 'aria-hidden': 'true' })
										)
									);

									if (2 >= property.aopt.length) {
										delOptBtn = null;
									}

									return React.createElement(
										'div',
										{ className: 'field-option', 'data-index': key, key: property.type + '-option-' + key },
										React.createElement(
											'div',
											{ className: 'col-xs-9' },
											React.createElement('input', { className: 'form-control', type: 'text', value: option, onChange: this.onOptionChange })
										),
										delOptBtn
									);
								}.bind(this))
							),
							React.createElement(
								'div',
								{ className: 'row' },
								React.createElement(
									'p',
									{ className: 'col-xs-offset-10' },
									React.createElement(
										'button',
										{ className: 'btn btn-link', onClick: this.addOption },
										'Add',
										React.createElement('i', { className: 'fa-fw fa fa-plus', 'aria-hidden': 'true' })
									)
								)
							)
						);
					}

					field = React.createElement(
						'div',
						{ className: 'field-object panel cleafix', 'data-index': this.props.index },
						panelHeading,
						React.createElement(
							'div',
							{ className: 'panel-body' },
							fieldPropertyType,
							React.createElement(
								'div',
								{ className: 'form-group' },
								React.createElement(
									'label',
									null,
									'Label',
									React.createElement('span', { className: 'mandatory' })
								),
								'modal' == this.props.layout ? React.createElement(SC_Tooltip, { tooltip: 'This is what you call this property internally. You can adjust how this looks on the actual form. For example, the "Date" property for jobs could be for weddings or corporate. However, on your actual form, your weddings form could say, "Wedding date" and your corporate event could say, "Event date."' }) : null,
								React.createElement('input', { type: 'text', className: 'form-control', defaultValue: property.title, onBlur: this.onTitleChange })
							),
							placeholderTag,
							fieldDescription,
							fieldRequired,
							fieldOptions,
							panelFooter
						),
						modalFooter
					);
				} else {

					if (parseInt(property.required, 10)) {
						required = true;
						requiredLabel = React.createElement('span', { className: 'mandatory' });
					}
					if (property.description && 'boolean' != property.format) {
						fieldDescription = React.createElement(
							'p',
							null,
							property.description
						);
					}

					if (this.props.version) {
						disabled = true;
					}

					if ('input' == property.type) {
						var format = 'number' == property.format ? 'number' : 'text';
						if ('date' == property.format) {
							fieldProperty = React.createElement(
								'div',
								{ className: fieldContainerClass },
								React.createElement(
									'label',
									{ htmlFor: 'question_' + property.id },
									property.title,
									requiredLabel
								),
								fieldDescription,
								React.createElement(SC_DatePicker, {
									dateFormat: this.props.dateFormat,
									idList: 'question_' + property.id,
									classList: 'form-control',
									nameList: 'questions[' + property.id + ']',
									readOnly: readOnly,
									value: property.values && property.values.length ? property.values[0].value : '',
									disabled: disabled,
									onDateChange: this.editedProperty
								})
							);
						} else {
							fieldProperty = React.createElement(
								'div',
								{ className: fieldContainerClass },
								React.createElement(
									'label',
									{ htmlFor: 'question_' + property.id },
									property.title,
									requiredLabel
								),
								fieldDescription,
								React.createElement('input', { type: format, className: 'form-control', name: 'questions[' + property.id + ']', required: required, readOnly: readOnly, id: 'question_' + property.id, defaultValue: property.values && property.values.length ? property.values[0].value : '', disabled: disabled, onBlur: this.editedProperty })
							);
						}
					} else if ('textarea' == property.type) {
						fieldProperty = React.createElement(
							'div',
							{ className: fieldContainerClass },
							React.createElement(
								'label',
								{ htmlFor: 'question_' + property.id },
								property.title,
								requiredLabel
							),
							fieldDescription,
							React.createElement('textarea', { className: 'form-control', rows: '2', name: 'questions[' + property.id + ']', required: required, readOnly: readOnly, id: 'question_' + property.id, defaultValue: property.values && property.values.length ? property.values[0].value : '', disabled: disabled, onBlur: this.editedProperty })
						);
					} else if ('checkbox' == property.type) {
						fieldProperty = React.createElement(
							'div',
							{ className: fieldContainerClass },
							React.createElement(
								'label',
								{ className: 'control-label chk-question' },
								React.createElement('input', { type: 'checkbox', value: 'on', name: 'questions[' + property.id + ']', defaultChecked: property.values && property.values.length && property.values[0].value == 'Yes' ? true : false, required: required, readOnly: readOnly, id: 'question_' + property.id, disabled: disabled, onChange: this.editedProperty }),
								React.createElement(
									'strong',
									null,
									property.title,
									requiredLabel
								)
							)
						);
					} else if ('multi-checkbox' == property.type) {
						fieldProperty = React.createElement(
							'div',
							{ className: fieldContainerClass },
							React.createElement(
								'label',
								null,
								property.title,
								requiredLabel
							),
							React.createElement(
								'div',
								{ className: 'checkboxes' },
								property.aopt.map(function (option, key, arr) {
									var checked = false;

									if (property.values && property.values.length) {
										property.values.map(function (value, key, arr) {
											if (value.value == option) {
												checked = true;
											}
											return false;
										});
									}

									return React.createElement(
										'div',
										{ className: 'checkbox', key: this.props.index + '_chk_group_' + key },
										React.createElement(
											'label',
											{ className: 'control-label' },
											React.createElement('input', { className: '', type: 'checkbox', value: key, name: 'questions[' + property.id + '][]', defaultChecked: checked, required: required, readOnly: readOnly, disabled: disabled, onChange: this.editedProperty }),
											React.createElement(
												'strong',
												null,
												option
											)
										)
									);
								}.bind(this))
							)
						);
					} else if ('radio' == property.type) {
						fieldProperty = React.createElement(
							'div',
							{ className: fieldContainerClass },
							React.createElement(
								'label',
								null,
								property.title,
								requiredLabel
							),
							React.createElement(
								'div',
								{ className: 'checkboxes' },
								property.aopt.map(function (option, key, arr) {
									var checked = false;

									if (property.values && property.values.length) {
										property.values.map(function (value, key, arr) {
											if (value.value == option) {
												checked = true;
											}
											return false;
										});
									}
									return React.createElement(
										'div',
										{ className: 'radio', key: this.props.index + '_radio_group_' + key },
										React.createElement(
											'label',
											{ className: 'control-label' },
											React.createElement('input', { className: '', type: 'radio', value: key, name: 'questions[' + property.id + ']', defaultChecked: checked, required: required, readOnly: readOnly, disabled: disabled, onChange: this.editedProperty }),
											React.createElement(
												'strong',
												null,
												option
											)
										)
									);
								}.bind(this))
							)
						);
					}

					field = React.createElement(
						'div',
						{ className: 'field-object panel-view', onClick: this.props.viewing ? null : this.onEdit },
						React.createElement(
							'div',
							{ className: 'form-group row' },
							fieldProperty,
							this.props.viewing ? null : React.createElement(
								'div',
								{ className: 'col-sm-3' },
								React.createElement(
									'div',
									{ className: 'pull-right btn-toolbar' },
									React.createElement(
										'button',
										{ className: 'btn btn-link', onClick: this.onDeleteButtonClick },
										'delete ',
										React.createElement('i', { className: 'fa-fw fa fa-trash-o', 'aria-hidden': 'true' })
									),
									React.createElement(
										'button',
										{ className: 'btn btn-link', onClick: this.onEdit },
										'edit ',
										React.createElement('i', { className: 'fa-fw fa fa-pencil-square-o', 'aria-hidden': 'true' })
									),
									moveButtons
								)
							)
						)
					);
				}
				return React.createElement(
					'div',
					{ className: 'field-editor' },
					field
				);
			}
		}]);

		return ObjectField;
	}(SC_Component);

	window.ObjectField = ObjectField;

	var SInpit = function (_SC_Component5) {
		_inherits(SInpit, _SC_Component5);

		function SInpit(props) {
			_classCallCheck(this, SInpit);

			var _this7 = _possibleConstructorReturn(this, Object.getPrototypeOf(SInpit).call(this, props));

			_this7.state = {
				disabled: false,
				editing: false
			};
			return _this7;
		}

		_createClass(SInpit, [{
			key: 'componentDidUpdate',
			value: function componentDidUpdate(prevProps, prevState) {
				if (!this.state.disabled && this.state.editing) {
					$('.sinput input').select();
				}
			}
		}, {
			key: 'onFocus',
			value: function onFocus(e) {
				e.preventDefault();

				this.setState({
					disabled: false,
					editing: true
				});
			}
		}, {
			key: 'onBlur',
			value: function onBlur(e) {
				e.preventDefault();
				if ('' == e.target.value.replace(/\s/g, '')) {
					this.props.onChangeField(this.props.type, 'Empty string');
					this.setState({
						disabled: false,
						editing: false
					});
				} else {
					this.props.onChangeField(this.props.type, e.target.value);
					this.setState({
						disabled: false,
						editing: false
					});
				}
			}
		}, {
			key: 'render',
			value: function render() {
				var classList = this.props.classList ? this.props.classList : '';
				var field = React.createElement(
					'span',
					{ className: 'edit-in-place' },
					this.props.value
				);
				if (!this.state.disabled && this.state.editing) {
					field = React.createElement('input', { type: 'text', className: 'edit-in-place', defaultValue: this.props.value, onBlur: this.onBlur });
				} else if (!this.state.disabled) {
					field = React.createElement(
						'span',
						{ className: 'edit-in-place', onClick: this.onFocus },
						this.props.value
					);
				}

				return React.createElement(
					'div',
					{ className: 'sinput ' + classList },
					field
				);
			}
		}]);

		return SInpit;
	}(SC_Component);

	var PropBuilder = function (_SC_Component6) {
		_inherits(PropBuilder, _SC_Component6);

		function PropBuilder(props) {
			_classCallCheck(this, PropBuilder);

			var _this8 = _possibleConstructorReturn(this, Object.getPrototypeOf(PropBuilder).call(this, props));

			_this8.state = {
				properties: _this8.props.properties ? _this8.props.properties : {},
				filter: '',
				current_tab: 'customer'
			};
			return _this8;
		}

		_createClass(PropBuilder, [{
			key: 'componentDidMount',
			value: function componentDidMount() {
				this.props.onRef(this);
			}
		}, {
			key: 'componentWillUnmount',
			value: function componentWillUnmount() {
				this.props.onRef(undefined);
			}
		}, {
			key: 'getModal',
			value: function getModal(label) {
				/* don't close the modal when clicked outside the popup */
				$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';

				var newId = 'modal-property';

				if ($('#' + newId).length) {
					var $template = $('#' + newId);

					$template.find('.modal-title:first').text(label);
				} else {
					var $template = $('#ajax-modal-template').clone();
					$template.attr('id', newId);
					switch (label) {
						case "Deliveries":
							$template.addClass("new-modal");break;
						case "Discounts":
							$template.addClass("new-modal");break;
						case "Tax Rates":
							$template.addClass("new-modal");break;
					}
					$template.find('.modal-title:first').text(label);
					$template.attr('aria-labelledby', newId);
					$template.on('hidden.bs.modal', function () {
						// dispose of template as it is no longer used
						$template.remove();
					});

					$template.appendTo($('body'));
				}

				$template.modal('show').on('hidden.bs.modal', this.onModalHidden);

				return $template.find('.modal-body:first')[0];
			}
		}, {
			key: 'closeModal',
			value: function closeModal() {
				$('#modal-property').modal('hide');
			}
		}, {
			key: 'setProperty',
			value: function setProperty(id, property) {
				var properties = $.extend(true, [], this.state.properties);
				if (-1 == id) {
					properties.push(property);
				} else {
					for (var i = 0; i < properties.length; i++) {
						if (id == properties[i].id) {
							properties[i] = property;
							break;
						}
					}
				}

				properties = $.extend(true, {}, properties);
				this.setState({ properties: properties });
			}
		}, {
			key: 'unsetProperty',
			value: function unsetProperty(id) {
				var properties = $.extend(true, [], this.state.properties);

				for (var i = 0; i < properties.length; i++) {
					if (parseInt(id, 10) == parseInt(properties[i].id, 10)) {
						properties.splice(i, 1);
						break;
					}
				}
				this.setState({ properties: properties });
			}
		}, {
			key: 'formReset',
			value: function formReset() {
				var properties = $.extend(true, {}, this.state.properties);

				for (var i = 0; i < Object.keys(properties).length; i++) {
					properties[i].default = false;
				}
				this.setState({ properties: properties });
			}
		}, {
			key: 'formLoaded',
			value: function formLoaded(_properties) {
				var properties = $.extend(true, [], this.state.properties);

				for (var i = 0; i < properties.length; i++) {
					if (_.findWhere(_properties, { property_id: properties[i].id })) {
						properties[i].default = true;
					}
				}

				this.setState({ properties: properties });
			}
		}, {
			key: 'formDeletedProperty',
			value: function formDeletedProperty(property) {
				var properties = $.extend(true, {}, this.state.properties);

				for (var i = 0; i < Object.keys(properties).length; i++) {
					if (property.property_id == parseInt(properties[i].id, 10)) {
						properties[i].default = false;
						break;
					}
				}
				this.setState({ properties: properties });
			}
		}, {
			key: 'editProp',
			value: function editProp(e) {
				e.preventDefault();
				e.stopPropagation();
				var id = parseInt($(e.target).closest('li').data('id'), 10);
				var property = {};
				if (-1 == id) {
					var fieldType = e.target.dataset.type;
					property = this.props.getProperty(fieldType);
					property.property_type = this.state.current_tab;
					property.placeholder_tag = this.createPlaceholderFromTitle(false, property.title);
				} else {
					for (var i = 0; i < Object.keys(this.state.properties).length; i++) {
						if (id == this.state.properties[i].id) {
							property = this.state.properties[i];
							break;
						}
					}
				}

				var modal = this.getModal('Property');
				this.modalRef = ReactDOM.render(React.createElement(PropertyModal, {
					id: id,
					property: property,
					setProperty: this.setProperty,
					unsetProperty: this.unsetProperty,
					getProperty: this.props.getProperty,
					closeModal: this.closeModal,
					createPlaceholder: this.createPlaceholderFromTitle,
					browseFormsProperties: this.props.browseFormsProperties
				}), modal);
			}
		}, {
			key: 'onPropSearch',
			value: function onPropSearch(e) {
				e.preventDefault();

				this.setState({
					filter: e.target.value.toLowerCase()
				});
			}
		}, {
			key: 'onPropToForm',
			value: function onPropToForm(e) {
				e.preventDefault();
				var properties = $.extend(true, [], this.state.properties);
				if (!this.props.edit_mode) {
					alertify.error('You must open or create a from in order to add properties.');
					return;
				}
				var property,
				    id = 'A' == e.target.tagName ? parseInt($(e.target).closest('li').data('id'), 10) : parseInt(e.target.dataset.id, 10);

				for (var i = 0; i < Object.keys(properties).length; i++) {
					if (id == parseInt(properties[i].id, 10)) {
						if (true == properties[i].default) {
							return;
						}
						property = properties[i];
						properties[i].default = true;
						break;
					}
				}
				properties = $.extend(true, {}, properties);
				this.props.onPropToForm(property);
				this.setState({ properties: properties });
			}
		}, {
			key: 'createPlaceholderFromTitle',
			value: function createPlaceholderFromTitle(prop_id, title) {
				var existing_tags = {};

				for (var i = 0; i < this.state.properties.length; i++) {
					var element = this.state.properties[i];
					if (prop_id && this.state.properties[i].id == prop_id) {
						continue;
					}

					existing_tags[this.state.properties[i].placeholder_tag] = true;
				}

				var placeholder_tag = $.trim(title).toLowerCase().replace(/[^a-zA-Z0-9\s]/gi, '').replace(/[\s-_]{1,}/g, '_');
				var underscore = '';
				var suffix = '';

				while (undefined !== existing_tags[placeholder_tag + underscore + suffix]) {
					suffix = '' == suffix ? 1 : suffix + 1;
					underscore = '_';
				}

				return placeholder_tag + underscore + suffix;
			}
		}, {
			key: 'renderPropertiesList',
			value: function renderPropertiesList(type) {
				return React.createElement(
					'ul',
					{ className: 'property-list' },
					Object.keys(this.state.properties).map(function (key) {
						if (type != this.state.properties[key].property_type) {
							return null;
						}

						var classList = true == this.state.properties[key].default ? 'disabled' : '';
						var title = this.state.properties[key].title;
						if (null !== this.state.properties[key].internal_field) {
							title += ' *';
						}

						if ('' != this.state.filter && -1 == title.toLowerCase().indexOf(this.state.filter)) {
							return null;
						}

						return React.createElement(
							'li',
							{ key: 'property-' + key, onClick: this.onPropToForm, 'data-id': this.state.properties[key].id, title: this.state.properties[key].title, className: classList },
							React.createElement(
								'a',
								{ href: '#', className: 'sc-primary-color' },
								title
							),
							React.createElement(
								'a',
								{ href: '#', className: 'sc-primary-color prop-edit', onClick: this.editProp },
								React.createElement('i', { className: 'fa-fw fa fa-cog', 'aria-hidden': 'true' })
							)
						);
					}.bind(this))
				);
			}
		}, {
			key: 'setCurrentTab',
			value: function setCurrentTab(e) {
				e.preventDefault();

				this.setState({
					current_tab: e.target.dataset.type
				});
			}
		}, {
			key: 'render',
			value: function render() {
				var addToForm = null;

				return React.createElement(
					'div',
					{ className: 'builder-property col-sm-12 center' },
					React.createElement('input', { className: 'form-control', type: 'text', id: 'propFilter', onChange: this.onPropSearch, value: this.state.filter, placeholder: 'Search properties...' }),
					React.createElement(
						'ul',
						{ className: 'horizontal-tab-nav nav', role: 'tablist' },
						React.createElement(
							'li',
							{ role: 'presentation', className: 'customer' == this.state.current_tab ? 'active' : '' },
							React.createElement(
								'a',
								{ href: '#customer-properties', 'aria-controls': 'customer-properties', role: 'tab', onClick: this.setCurrentTab, 'data-type': 'customer' },
								'Customer'
							)
						),
						React.createElement(
							'li',
							{ role: 'presentation', className: 'event' == this.state.current_tab ? 'active' : '' },
							React.createElement(
								'a',
								{ href: '#job-properties', 'aria-controls': 'job-properties', role: 'tab', onClick: this.setCurrentTab, 'data-type': 'event' },
								'Job'
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'tab-content' },
						React.createElement(
							'div',
							{ id: 'customer-properties', className: 'form-panel tab-pane' + ('customer' == this.state.current_tab ? ' active' : ''), role: 'tabpanel' },
							this.renderPropertiesList('customer')
						),
						React.createElement(
							'div',
							{ id: 'job-properties', className: 'form-panel tab-pane' + ('event' == this.state.current_tab ? ' active' : ''), role: 'tabpanel' },
							this.renderPropertiesList('event')
						),
						React.createElement(
							'p',
							{ className: 'help' },
							React.createElement(
								'small',
								null,
								'Properties marked with * will be automatically mapped to the corresponding Customer or Job fields'
							)
						)
					),
					React.createElement(
						'div',
						{ className: 'row' },
						React.createElement(
							'div',
							{ className: 'dropdown pull-right' },
							React.createElement(
								'button',
								{ type: 'button', className: 'btn btn-primary dropdown-toggle', 'data-toggle': 'dropdown' },
								React.createElement('i', { className: 'fa-fw fa fa-plus', 'aria-hidden': 'true' }),
								' Add a property ',
								React.createElement('span', { className: 'caret' })
							),
							React.createElement(
								'ul',
								{ className: 'dropdown-menu' },
								React.createElement(
									'li',
									{ 'data-id': -1 },
									React.createElement(
										'a',
										{ href: '#', 'data-type': 'date', onClick: this.editProp },
										'Date'
									)
								),
								React.createElement(
									'li',
									{ 'data-id': -1 },
									React.createElement(
										'a',
										{ href: '#', 'data-type': 'single-line', onClick: this.editProp },
										'Single Line Text'
									)
								),
								React.createElement(
									'li',
									{ 'data-id': -1 },
									React.createElement(
										'a',
										{ href: '#', 'data-type': 'multi-line', onClick: this.editProp },
										'Multiple Line'
									)
								),
								React.createElement(
									'li',
									{ 'data-id': -1 },
									React.createElement(
										'a',
										{ href: '#', 'data-type': 'checkbox', onClick: this.editProp },
										'Single Checkbox'
									)
								),
								React.createElement(
									'li',
									{ 'data-id': -1 },
									React.createElement(
										'a',
										{ href: '#', 'data-type': 'multi-checkbox', onClick: this.editProp },
										'Multiple Checkboxes'
									)
								),
								React.createElement(
									'li',
									{ 'data-id': -1 },
									React.createElement(
										'a',
										{ href: '#', 'data-type': 'number', onClick: this.editProp },
										'Number'
									)
								),
								React.createElement(
									'li',
									{ 'data-id': -1 },
									React.createElement(
										'a',
										{ href: '#', 'data-type': 'radio', onClick: this.editProp },
										'Radio Select'
									)
								)
							)
						)
					)
				);
			}
		}]);

		return PropBuilder;
	}(SC_Component);

	var PropertyModal = function (_SC_Component7) {
		_inherits(PropertyModal, _SC_Component7);

		function PropertyModal(props) {
			_classCallCheck(this, PropertyModal);

			var _this9 = _possibleConstructorReturn(this, Object.getPrototypeOf(PropertyModal).call(this, props));

			_this9.state = {
				property: _this9.props.property
			};
			return _this9;
		}

		_createClass(PropertyModal, [{
			key: 'componentDidMount',
			value: function componentDidMount(e) {
				stemcounter.unmountComponentOnModalClose(this);
			}
		}, {
			key: 'submitProperty',
			value: function submitProperty(key, property) {
				if ('' == property.property_type || '' == property.title) {
					alertify.error('Please fill up the required fields');
					return;
				}
				var _this = this;
				var updatedState = $.extend(true, {}, this.state);
				if (undefined !== property) {
					updatedState.property = property;
					var url = window.stemcounter.aurl({ action: 'sc_property_edit' }),
					    data = {
						id: parseInt(this.props.id, 10),
						property: property
					};

					$.post(url, data, function (response) {
						stemcounter.JSONResponse(response, function (r) {
							if (!r.success) {
								alertify.error(r.payload.message);
							} else {
								if (r.payload.property_id) {
									updatedState.property.id = parseInt(r.payload.property_id, 10);
									_this.props.setProperty(_this.props.id, updatedState.property);
									_this.props.closeModal();
									alertify.success(r.payload.message);
								}
							}
						});
					});
				}
			}
		}, {
			key: 'changeProperty',
			value: function changeProperty(key, type, property) {
				var updatedState = $.extend(true, {}, this.state);
				if (undefined !== key && undefined !== type && undefined !== property) {
					updatedState.property = this.props.getProperty(type);
					updatedState.property.title = property.title;
					updatedState.property.description = property.description;
				}
				this.setState(updatedState);
			}
		}, {
			key: 'deleteProperty',
			value: function deleteProperty(id) {
				if (undefined !== id) {
					var propertyInForms = this.props.browseFormsProperties(this.state.property);
					if (propertyInForms.length) {
						var message = 'To delete a property, you must remove it from all forms first. <br>';
						propertyInForms.map(function (prop, index, arr) {
							message = message + prop.form + ' -> ' + prop.property + ' <br>';
							return false;
						});
						alertify.alert('Delete Property', message);
						return;
					}
					var _this = this,
					    url = window.stemcounter.aurl({ action: 'sc_property_delete' }),
					    data = {
						id: parseInt(id, 10)
					};

					$.post(url, data, function (response) {
						stemcounter.JSONResponse(response, function (r) {
							if (!r.success) {
								alertify.error(r.payload.message);
								this.fieldRef.setState({
									saving: false
								});
							} else {
								_this.props.unsetProperty(id);
								_this.props.closeModal();
								alertify.success(r.payload.message);
							}
						});
					});
				}
			}
		}, {
			key: 'setFieldRef',
			value: function setFieldRef(ref) {
				this.fieldRef = ref;
			}
		}, {
			key: 'render',
			value: function render() {
				return React.createElement(
					'div',
					{ className: 'tax-rates-modal form-horizontal style-form' },
					React.createElement(
						'div',
						{ className: 'modalpane-row clearfix noborderbottom' },
						React.createElement(ObjectField, {
							index: this.props.id,
							property: this.state.property,
							layout: 'modal',
							changeProperty: this.changeProperty,
							submitProperty: this.submitProperty,
							deleteProperty: this.deleteProperty,
							createPlaceholder: this.props.createPlaceholder,
							ref: this.setFieldRef
						})
					)
				);
			}
		}]);

		return PropertyModal;
	}(SC_Component);

	var ICForm = function (_SC_Component8) {
		_inherits(ICForm, _SC_Component8);

		function ICForm(props) {
			_classCallCheck(this, ICForm);

			var _this10 = _possibleConstructorReturn(this, Object.getPrototypeOf(ICForm).call(this, props));

			_this10.state = {
				loading: false,
				confirmationMessage: false
			};
			return _this10;
		}

		_createClass(ICForm, [{
			key: 'onSubmit',
			value: function onSubmit(e) {
				e.preventDefault();

				if (this.state.loading) {
					return;
				}

				var data = $.param({
					action: 'sc/form/submission/new',
					form_id: this.props.schema.id
				}),
				    _this = this;

				data += '&' + $(e.target).serialize();

				this.setState({
					loading: true
				});

				$.post(stemcounter.ajax_url, data, function (r) {
					if (!r.success) {
						alertify.error(r.message);
					} else {
						_this.showConfirmationMessage(r.message);
					}

					_this.setState({
						loading: false
					});
				});
			}
		}, {
			key: 'clearForm',
			value: function clearForm() {
				ReactDOM.findDOMNode(this).reset();
			}
		}, {
			key: 'showConfirmationMessage',
			value: function showConfirmationMessage(message) {
				this.setState({
					confirmationMessage: message
				});
			}
		}, {
			key: 'render',
			value: function render() {
				if (this.state.confirmationMessage) {
					return React.createElement('div', { className: 'form-confirmation-message form-success', dangerouslySetInnerHTML: { __html: this.state.confirmationMessage } });
				}

				return React.createElement(
					'form',
					{ onSubmit: this.onSubmit },
					this.state.loading ? React.createElement(LoadingOverlay, null) : null,
					React.createElement(SchemaFields, {
						schema: this.props.schema,
						onChangeField: $.noop,
						dateFormat: this.props.dateFormat,
						viewing: true
					}),
					React.createElement(
						'div',
						{ className: 'form-group row' },
						React.createElement(
							'div',
							{ className: 'col-sm-2 col-sm-push-10 col-xs-12' },
							React.createElement(
								'button',
								{ type: 'submit', className: 'btn btn-primary btn-lg btn-block' },
								'Send'
							)
						)
					)
				);
			}
		}]);

		return ICForm;
	}(SC_Component);

	$(document).on('stemcounter.action.renderICFController', function (e, settings) {
		ReactDOM.render(React.createElement(ICFController, {
			dateFormat: settings.dateFormat,
			userForms: settings.userForms,
			userProperties: settings.userProperties
		}), $(settings.node).get(0));
	});

	$(document).on('stemcounter.action.renderICForm', function (e, settings) {
		ReactDOM.render(React.createElement(ICForm, {
			schema: settings.schema,
			dateFormat: settings.dateFormat
		}), $(settings.node).get(0));
	});
})(jQuery);