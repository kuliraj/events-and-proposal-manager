'use strict';

// use a closure so we do not pollute the global scope
(function ($) {
	'use strict';

	var Notifications = React.createClass({
		displayName: 'Notifications',

		getInitialState: function getInitialState() {
			return {
				allow_notifications: this.props.allow_notifications,
				notifications: this.props.notifications,
				user_email: this.props.user_email
			};
		},
		onAllowNotifications: function onAllowNotifications(e) {
			var updatedState = $.extend(true, {}, this.state);

			updatedState.allow_notifications = e.target.checked ? 'yes' : 'no';
			this.setState(updatedState, this.submit);
		},
		onEmailAddr: function onEmailAddr(e) {
			e.preventDefault();
			var updatedState = $.extend(true, {}, this.state);

			updatedState.user_email = e.target.value;

			this.setState(updatedState, this.submit);
			return;
		},
		submit: function submit() {
			var url = window.stemcounter.aurl({
				action: 'sc_handle_notifications_form',
				email: this.state.user_email,
				allow: this.state.allow_notifications
			});

			$.get(url, {}, function (response) {
				stemcounter.JSONResponse(response, function (r) {
					if (r.success) {} else {}
				}.bind(this));
			}.bind(this));
		},
		render: function render() {
			var _this = this,
			    notifications = [];

			Object.keys(this.state.notifications).map(function (key, index) {
				notifications.push(React.createElement(
					'tr',
					{ key: 'key-notifications-row' + index },
					React.createElement(
						'td',
						null,
						(_this.props.dateFormat, _this.state.notifications[key].date)
					),
					React.createElement('td', { dangerouslySetInnerHTML: { __html: _this.state.notifications[key].notification } })
				));
			});

			return React.createElement(
				'div',
				{ className: 'notifications-react-form' },
				React.createElement(
					'div',
					{ className: 'row' },
					React.createElement(
						'div',
						{ className: 'col-xs-12' },
						React.createElement(
							'div',
							{ className: 'col-xs-6' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								'Email Address',
								React.createElement('input', { type: 'text', className: 'form-control', onBlur: this.onEmailAddr, placeholder: 'email@example.com', defaultValue: this.state.user_email })
							)
						),
						React.createElement(
							'div',
							{ className: 'col-xs-6' },
							React.createElement(
								'label',
								{ className: 'control-label' },
								React.createElement('input', { type: 'checkbox', defaultChecked: 'yes' == this.state.allow_notifications ? true : false, onChange: this.onAllowNotifications }),
								'Email notifications'
							)
						)
					)
				),
				React.createElement(
					'div',
					{ className: 'row' },
					React.createElement(
						'table',
						{ id: 'notifications' },
						React.createElement(
							'thead',
							null,
							React.createElement(
								'tr',
								null,
								React.createElement(
									'th',
									null,
									'Date'
								),
								React.createElement(
									'th',
									null,
									'Notification'
								)
							)
						),
						React.createElement(
							'tbody',
							null,
							notifications
						)
					)
				)
			);
		}
	});

	jQuery(document).on('stemcounter.action.renderNotifications', function (e, settings) {
		ReactDOM.render(React.createElement(Notifications, { notifications: settings.notifications, user_email: settings.user_email, allow_notifications: settings.allow_notifications, dateFormat: settings.dateFormat }), $(settings.node).get(0));
	});
})(jQuery);