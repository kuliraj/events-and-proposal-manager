<?php
/*
Template Name: User - Profile
*/

use Stemcounter\Tax_Rate;
use Stemcounter\Invoicing_Category;
use Stemcounter\Discount;
use Stemcounter\Delivery;
use Stemcounter\Notification;
use Stemcounter\Email_Template;
use Stemcounter\Measuring_Unit;

$current_user = wp_get_current_user();
$replace_zip = sc_get_user_meta( get_current_user_id() ); //Get User Details
$additionalInfo = unserialize( $current_user->additionalInfo ); //Get User Details

$company = isset($replace_zip['company_name']) ? $replace_zip['company_name']: ''; 
$address = isset($replace_zip['company_address']) ? $replace_zip['company_address']: '';
$website = isset($replace_zip['company_website'])  ? $replace_zip['company_website']: '';
$phone = isset($additionalInfo['companyMobile'])  ? $additionalInfo['companyMobile']: '';

$logo_id = sc_get_profile_logo_id( get_current_user_id() );
$logo_img = wp_get_attachment_image_src( $logo_id, 'logo_medium' );
if ( $logo_img ) {
	$logo_img = $logo_img[0];
}

$photo_id = sc_get_profile_photo_id( get_current_user_id() );
$photo_img = wp_get_attachment_image ($photo_id, 'logo_medium' );
if ( $photo_img ) {
	$photo_img = $photo_img ? $photo_img: '<img src="' . get_bloginfo('template_directory') . '/img/gray-pixel.png" alt="" />';
} else {
	$photo_img = 'Upload photo';
}

$tags = get_user_meta(
	get_current_user_id(),
	'tags',
	true
);

if ( !isset( $tags['event_tags']  ) ) {
	$tags['event_tags'] = array() ;
}

$profile_settings = sc_get_profile_settings();
$item_types = sc_get_mixed_arrangement_item_types();
$tax_rates = Tax_Rate::where( array(
	'user_id' => get_current_user_id(),
) )->get();
$invoice_categories_list = Invoicing_Category::where(array(
	'user_id' => get_current_user_id()
))->get()->toArray();

$default_invoice_category_id = get_user_meta(
	get_current_user_id(), 
	'default_invoice_category_id', 
	true
);
if ($default_invoice_category_id) {
	try {
		$default_invoice_category = Invoicing_Category::where(array(
			'id' => $default_invoice_category_id
		))->firstOrFail()->toArray();	
	} catch (Exception $e) {
		
	}
	
} else {
	$default_invoice_category = array();
}

$invoicing_settings_table = array (
	'Charge Card Rate'=>array ( '<i class="fa fa-edit"></i>', $profile_settings['card'] ),
	'Date Format'=>array ( '<i class="fa fa-calendar"></i>', strtoupper( $profile_settings['date_format'] ) ),
	'Currency'=>array ( '<i class="fa fa-money"></i>', strtoupper( $profile_settings['pref_currency'] ) ),
	'PDF sheet size'=>array ( '<i class="fa fa-print"></i>', $profile_settings['pdf_sheet_size'] ),
);

$date_format_options = array(
	'm/d/Y' => '03/21/2015', 
	'm.d.Y' => '07.31.2015', 
	'd/m/Y' => '14/09/2015', 
	'd.m.Y' => '16.05.2015',
);

$pref_currencies = array(
	'AED' => 'AED - United Arab Emirates Dirham', 
	'AUD' => 'AUD - Australian Dollar',
	'CAD' => 'CAD - Canadian Dollar',
	'EUR' => 'EUR - Euro',
	'GBP' => 'GBP - British Pound',
	'ZAR' => 'ZAR - South African rand',
	'USD' => 'USD - United States Dollar', 
);

$selected_paper_size = get_user_meta( get_current_user_id(), '_sc_pdf_settings', true );

if ( empty( $selected_paper_size ) ) {
	$selected_paper_size = 'US-Letter';
}

$prices_separator = get_user_meta( get_current_user_id(), '_sc_prices_separator', true );

if ( empty( $prices_separator ) ) {
	$prices_separator = 'comma_dot';
}

$notifications = Notification::where(array(
	'user_id' => get_current_user_id(),
))->orderBy('id', 'DESC')->limit(15)->get();

$user_notification_info = get_user_meta( get_current_user_id(), '_sc_user_notifications', true );

$date_format = get_user_meta( get_current_user_id(), '__date_format', true );
if ( $date_format == 'd/m/Y' ) {
	$date_format = 'd/m/Y g:ia';
} else if ( $date_format == 'm/d/Y' ) {
	$date_format = 'm/d/Y g:ia';
} else if ( $date_format == 'd.m.Y' ) {
	$date_format = 'd.m.Y g:ia';
} else if ( $date_format == 'd.m.Y' ) {
	$date_format = 'd.m.Y g:ia';
}

foreach ( $notifications as $notification ) {
	$notification->date = date( $date_format, strtotime( $notification->created_at ) );
	$notification->notification = wpautop( $notification->notification );
}
$user_libraries = get_user_meta( get_current_user_id(), 'sc_user_libraries', true );
$user_libraries = is_array( $user_libraries ) ? $user_libraries : array();
$librarians = get_users( array(
	'role' => 'librarian',
) );
$libraries = array_map( function( $librarian ){
	return array(
		'id' => $librarian->ID,
		'name' => $librarian->display_name,
	);
}, $librarians );

$dateFormat = sc_get_user_js_date_format();

$measuring_units = Measuring_Unit::orderBy( 'id', 'ASC' )->get()->toArray();
$user_units = get_user_meta( get_current_user_id(), 'sc_user_measuring_units', true );
$user_units = is_array( $user_units ) ? $user_units : array();

$profile_settings_args = array(
	'logo' => $logo_img,
	'logo_id' => $logo_id,
	'logo_on_proposal' => (bool) sc_get_profile_logo_on_proposal( get_current_user_id() ),
	'logo_upload_action' => 'logo',
	'logo_upload_nonce' => sc_ajax_get_image_upload_nonce( 'logo' ),
	'date_format_options' => $date_format_options,
	'pref_currencies' => $pref_currencies,
	'user_libraries' => $user_libraries,
	'user_units' => $user_units,
	'libraries' => $libraries,
	'company' => $company,
	'address' => $address,
	'website' => $website,
	'phone' => $phone,
	'card' => $profile_settings['card'],
	'date_format' => $profile_settings['date_format'],
	'pref_currency' => $profile_settings['pref_currency'],
	'pdf_sheet_size' => $profile_settings['pdf_sheet_size'],
	'visible_taxes' => sc_get_visible_taxes_count(),
	'prices_separator' => $prices_separator,
	'nonce' => wp_create_nonce( 'sc/profile/settings/save' ),
	'measuring_units' => $measuring_units,
);

$user_email_templates = array();
if ( SCAC::can_access( SCAC::STUDIO ) ) {
	$user_email_templates = Email_Template::where( array(
		'user_id'	=> get_current_user_id(),
	) )->get();
} else {
	$user_email_templates[] = Email_Template::where( array(
		'user_id'	=> get_current_user_id(),
		'default'	=> 1,
	) )->first();
}

get_header();

// Add the id's of any new tabs here
$tabs = array( 'company', 'taxrates', 'markup', 'notifications', 'mail-templates' );

$active_tab = ! empty( $_GET['tab'] ) && in_array( $_GET['tab'], $tabs ) ? $_GET['tab'] : 'company'; ?>

<section class="wrapper">
	<?php if ( SC_INVITE_ONLY ): ?>
		<?php $unused_codes = sc_get_unused_viral_codes( get_current_user_id() ); ?>
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i> Your Invitation Codes</h4>
					<span> Stemcounter is in public beta by invite only. Once each of these codes is used, it cannot be used again. Share with friends who deserve it! </span><br><br>
					<ul>
						<?php foreach ( $unused_codes as $code ) : ?>
							<li><?php echo $code; ?></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<div class="content-panel-profile">
		<!-- Nav tabs -->
		<ul class="horizontal-tab-nav nav" role="tablist" id="myTabs">
			<li role="presentation" class="<?php echo 'company' == $active_tab ? 'active' : ''; ?>"><a href="#profile-company" aria-controls="profile-company" role="tab" data-toggle="tab">Settings</a></li>
			<li role="presentation" class="<?php echo 'taxrates' == $active_tab ? 'active' : ''; ?>"><a href="#profile-taxrates" aria-controls="settings" role="tab" data-toggle="tab">Tax Rates</a></li>
			<li role="presentation" class="<?php echo 'markup' == $active_tab ? 'active' : ''; ?>"><a href="#profile-markup" aria-controls="settings" role="tab" data-toggle="tab">Mark-up Profiles</a></li>
			<li role="presentation" class="<?php echo 'notifications' == $active_tab ? 'active' : ''; ?>"><a href="#profile-notifications" aria-controls="settings" role="tab" data-toggle="tab">Notifications</a></li>
			<li role="presentation" class="<?php echo 'mail-templates' == $active_tab ? 'active' : ''; ?>"><a href="#profile-mail-templates" aria-controls="settings" role="tab" data-toggle="tab">Email Templates</a></li>
		</ul>

		<div class="tab-content">
			<div class="form-panel tab-pane <?php echo 'company' == $active_tab ? 'active' : ''; ?>" id="profile-company" role="tabpanel"></div>
			<!-- End of general settings tab -->
			
			<!-- Start of Tax Rates tab -->
			<div class="form-panel tab-pane <?php echo 'taxrates' == $active_tab ? 'active' : ''; ?>" id="profile-taxrates" role="tabpanel">
				<div class="edit-tax-rates-form-wrapper" data-rate-count="<?php echo esc_attr( sc_get_visible_taxes_count() ); ?>" data-value="<?php echo esc_attr( json_encode( $tax_rates ) ); ?>"><!-- JS --></div>
			</div>
			<!-- End of Tax Rates tab -->

			<!-- Start of Invoice categories tab -->
			<div class="form-panel tab-pane <?php echo 'markup' == $active_tab ? 'active' : ''; ?>" id="profile-markup" role="tabpanel">
				<div id="invoice-categories" class="edit-invoice-categories"></div>
			</div>
			<!-- End of Invoice categories tab -->

			<!-- Start of Notifications tab -->
			<div class="form-panel tab-pane <?php echo 'notifications' == $active_tab ? 'active' : ''; ?>" id="profile-notifications" role="tabpanel">
				<div id="profile_notifications" class="edit-notifications-form-wrapper" ></div>
			</div>
			<!-- End of Notifications tab -->

			<!-- Start of Template tab -->
			<div class="form-panel tab-pane <?php echo 'mail-templates' == $active_tab ? 'active' : ''; ?>" id="profile-mail-templates" role="tabpanel">
				<div id="edit_mail_templates" class="edit-mail-templates-form-wrapper" ></div>
			</div>
			<!-- End of Template tab -->

		</div> <!-- /tab-content -->
	</div> <!-- content-panel -->
</section>  <!--wrapper end-->

<script type="text/javascript">
(function($){

$(document).ready(function(){
	stemcounter.floatInput($('#hardgoodMultiple, #freshFlowerMultiple, #chargeCardRate, #salesTax, #delivery, #labor'));

	/*$(document).tooltip();*/

	var settings = <?php echo json_encode( $profile_settings_args ); ?>;
	settings.node = $('#profile-company').get(0);

	$(document).trigger( 'stemcounter.action.renderProfileSettingsForm', settings );

	$(document).trigger('stemcounter.action.renderInvoicingCategoriesProfile', {
		layout: 'new_profile',
		itemTypes: <?php echo json_encode($item_types); ?>,
		categoriesList: <?php echo json_encode($invoice_categories_list) ?>,
		defaultCategoryId: <?php echo json_encode(get_user_meta(get_current_user_id(), 'default_invoice_category_id', true)); ?>,
		node: $('#invoice-categories')
	});
	

	$(document).trigger('stemcounter.action.renderNotifications', {
		notifications: <?php echo json_encode( $notifications ); ?>,
		user_email: <?php echo json_encode( ( empty( $user_notification_info['email'] ) ? $current_user->user_email : $user_notification_info['email'] ) ); ?>,
		allow_notifications: <?php echo json_encode( ( 'no' == $user_notification_info['allow'] ? 'no' : 'yes' ) ); ?>,
		dateFormat: <?php echo json_encode($dateFormat); ?>,
		node: $('#profile_notifications')
	});

	$(document).trigger('stemcounter.action.renderEmailTemplates', {
		templates: <?php echo json_encode( $user_email_templates ); ?>,
		node: $('#edit_mail_templates')
	});
});

})(jQuery)
</script>

<?php get_footer(); ?>