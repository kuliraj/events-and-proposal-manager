<?php
/*
Template Name: User - Studio
*/
get_header(); ?>
	<section class="wrapper">
		<?php the_content(); ?>
	</section>
<?php get_footer(); ?>