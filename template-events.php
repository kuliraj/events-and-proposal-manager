<?php 
/*
Template Name: User - Events
*/

get_header(); ?>

	<section class="wrapper">
		<?php while( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
		<div id="feedback"></div>
	</section><!--/wrapper -->

<script type="text/javascript">
function event_archive(event_id) {
	alertify.confirm('Are you sure you wish to archive this event?', function () {
		jQuery.ajax({
			type:"post",
			url: window.stemcounter.ajax_url,
			data: {
				action: 'sc_archive_user_event_id',
				id: event_id,
				method: 'archive'
			},
			success:function(edata){
				stemcounter.gaEvent('event', 'archived');
				location.reload();
			}
		});
	});
}

function event_unarchive(event_id) {
	//We will eventually put some kind of limitation here based on how many live events they have.
	alertify.confirm('Are you sure you wish to make this one of your live events?', function () {
		jQuery.ajax({
			type:"post",
			url: window.stemcounter.ajax_url,
			data: {
				action: 'sc_archive_user_event_id',
				id: event_id,
				method: 'publish'
			},
			success:function(edata){
				stemcounter.gaEvent('event', 'unarchived');
				location.reload();
			}
		});
	});
}

(function($){

$(document).ready(function(){
	$('.add-event-button').on('click', function(e){
		e.preventDefault();

		var url = window.stemcounter.aurl({
			action: 'sc_edit_event_form',
			id: 0
		});

		stemcounter.openAjaxModal('Add Event', url, 'new-modal edit-event-modal');
	});

	$('tbody').on('click', '.edit-event-button', function(e){
		e.preventDefault();

		var url = window.stemcounter.aurl({
			action: 'sc_edit_event_form',
			id: $(this).attr('data-id')
		});

		stemcounter.openAjaxModal('Edit Event', url,  'new-modal edit-event-modal');
	});

	$('tbody').on('click', '.duplicate-event-button', function(e){
		e.preventDefault();

		var url = window.stemcounter.aurl({
			action: 'sc_edit_event_form',
			duplicate: 1,
			id: $(this).attr('data-id')
		});

		stemcounter.openAjaxModal('Add Event', url, 'new-modal edit-event-modal');
	});

	//$('.delEvent').click(function(e){
	$('tbody').on('click', '.delEvent',function(e){
		e.preventDefault();
		
		var del_eid = $(this).attr("rel");
		var del_etitle = $(this).attr("title");
		var del_erow = $(this).parents("tr");

		alertify.confirm('Are you sure you wish to delete the ' + del_etitle + ' event?', function () {
		    jQuery.ajax({
				type:"post",
				url: window.stemcounter.ajax_url,
				data: {
					action: 'sc_delete_user_event',
					id: del_eid
				},
				success:function(response){
					stemcounter.JSONResponse(response, function(r) {
						stemcounter.gaEvent('event', 'deleted');
						if(r.payload.event_id == del_eid) {
							del_erow.fadeOut(500).remove();
						}
					});
				}
			});
		});
	});
});

})(jQuery);
</script>

<?php get_footer(); ?>