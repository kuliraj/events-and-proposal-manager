<?php
/*
Template Name: User - Event Shopping
*/

get_header( 'new-design' ); ?>

	<section class="wrapper">
		<?php the_content(); ?>
	</section><!--/wrapper -->

<?php get_footer(); ?>