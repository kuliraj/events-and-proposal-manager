<?php if ( has_nav_menu( 'main-menu' ) && !is_404()) : ?>

<!--sidebar start-->
<aside class="___Sidebar_Event">
	<div id="sidebar" class="nav-collapse">
		<!-- site logo -->
		<div class="sidebar-top">
			<a id="sidebar-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"></a>
		</div>
		<!-- sidebar menu start-->
		<?php
		// Social links navigation menu.
		wp_nav_menu( array(
			'theme_location'	=> 'main-menu',
			'container' 		=> false,
			'menu_class'		=> 'sidebar-menu',
			'menu_id'			=> 'nav-accordion',
			'depth'     		=> 1,
			'link_before'  		=> '<span>',
			'link_after'   		=> '</span>'
		) );
		?>

		<?php 
		//delete_user_meta(get_current_user_id(), '_sc_profile_photo');

			$img = get_avatar(get_current_user_id(), 80);
		?>

		<div id="sidebar-profile-photo" class="hidden">
			<div class="photo"><?php echo $img; ?></div>
			<div class="greeting">Hey 
				<span class="first-name"><?php echo wp_get_current_user()->first_name ?>!</span>
			</div>
		</div>

	</div><!-- #sidebar -->

	<div class="sidebar-sharing-wrap">
		<div id="share-by-email-container"></div>
		<?php $coupon = sc_get_affiliate_coupon( get_current_user_id() ); ?>
		<div class="sidebar-sharing" data-coupon="<?php echo $coupon ? esc_attr( $coupon->post_title ) : ''; ?>">
			<?php if ( $coupon ) : ?>
				<div class="sharing-lbl">
					Share and get $47
					<span class="sc-tooltip-wrap">
						<span class="tooltip-msg"><i class="fa fa-question-circle"></i></span>
						<span class="tooltip-content">Share Stemcounter with a friend! They'll get their second month for free and you'll get $47 when they sign up. Make sure they use your coupon code: <code><?php echo $coupon->post_title; ?></code></span>
					</span>
				</div>
			<?php else : ?>
				<div class="sharing-lbl">Share Stemcounter</div>
			<?php endif; ?>
			<a href="javascript:void(0)" title="Share Stemcounter by Email" class="share-email sc-primary-color">
				<i class="fa fa-envelope" aria-hidden="true"></i>
			</a>
			<a href="javascript:void(0)" title="Share Stemcounter on Facebook" class="share-fb sc-primary-color">
				<i class="fa fa-facebook-square" aria-hidden="true"></i>
			</a>
		</div>

		<?php if ( defined( 'OPENVOYCE_TOOL' ) && OPENVOYCE_TOOL ) :
			add_action( 'wp_footer', 'sc_render_openvoyce_js', 9999 ); ?>
			<div class="openvoyce-custom-sharing"><a href="#" class="sc-openvoyce-trigger">Make a suggestion</a></div>
		<?php endif; ?>
	</div> <!-- /.sidebar-sharing-wrap -->
</aside><!-- .___Sidebar_Event -->

<?php endif; ?>