<?php
/*
Template Name: User - Flowers
*/

get_header(); ?>

	<section class="wrapper">
		<?php the_content(); ?>
	</section><!--/wrapper -->

<?php get_footer(); ?>