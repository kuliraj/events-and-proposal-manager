<?php
get_header();
the_post(); ?>

  <section class="wrapper">
	
	<?php the_content(); ?>
		
	</section>

<?php get_footer(); ?>