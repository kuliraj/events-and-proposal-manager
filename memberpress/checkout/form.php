<?php if(!defined('ABSPATH')) {die('You are not allowed to call this page directly.');} ?>

<div class="mp_wrapper">
  <form class="mepr-signup-form mepr-form" method="post" novalidate>
    <input type="hidden" id="mepr_process_signup_form" name="mepr_process_signup_form" value="Y" />
    <input type="hidden" id="mepr_product_id" name="mepr_product_id" value="<?php echo $product->ID; ?>" />

    <?php if(MeprUtils::is_user_logged_in()): ?>
      <input type="hidden" name="logged_in_purchase" value="1" />
    <?php endif; ?>

    <?php
      if(MeprUtils::is_user_logged_in() && $mepr_options->show_fields_logged_in_purchases) {
        MeprUsersHelper::render_custom_fields($product);
      }
      elseif(!MeprUtils::is_user_logged_in()) { // We only pass the 'signup' flag on initial Signup
        MeprUsersHelper::render_custom_fields($product, true);
      }
    ?>

    <?php if(MeprUtils::is_user_logged_in()): ?>
      <input type="hidden" name="user_email" id="user_email" value="<?php echo stripslashes($mepr_current_user->user_email); ?>" />
    <?php else: ?>
      <input type="hidden" class="mepr-geo-country" name="mepr-geo-country" value="" />

      <?php if(!$mepr_options->username_is_email): ?>
        <div class="mp-form-row mepr_username">
          <div class="mp-form-label">
            <label><?php _e('Username:*', 'memberpress'); ?></label>
          </div>
          <input type="text" name="user_login" id="user_login" class="mepr-form-input" value="<?php echo (isset($user_login))?esc_attr(stripslashes($user_login)):''; ?>" required />
        </div>
      <?php endif; ?>
      <div class="mp-form-row mepr_email">
        <input type="email" name="user_email" id="user_email" class="mepr-form-input" value="<?php echo (isset($user_email))?esc_attr(stripslashes($user_email)):''; ?>" required placeholder="<?php esc_attr_e('Email:*', 'memberpress'); ?>" />
      </div>
      <div class="mp-form-row">
        <div class="mp-form-label">
          <!-- <label><?php _e('Password:*', 'memberpress'); ?></label> -->
        </div>
        <input type="password" name="mepr_user_password" id="mepr_user_password" class="mepr-form-input mepr-password" value="<?php echo (isset($mepr_user_password))?esc_attr(stripslashes($mepr_user_password)):''; ?>" required placeholder="<?php esc_attr_e('Password:*', 'memberpress'); ?>" />
      </div>
      <div class="mp-form-row">
        <div class="mp-form-label">
          <!-- <label><?php _e('Password Confirmation:*', 'memberpress'); ?></label> -->
        </div>
        <input type="password" name="mepr_user_password_confirm" id="mepr_user_password_confirm" class="mepr-form-input mepr-password-confirm" value="<?php echo (isset($mepr_user_password_confirm))?esc_attr(stripslashes($mepr_user_password_confirm)):''; ?>" required placeholder="<?php esc_attr_e('Password confirmation:*', 'memberpress'); ?>" />
      </div>
      <?php MeprHooks::do_action('mepr-after-password-fields'); ?>
    <?php endif; ?>
    <?php if($product->adjusted_price() > 0.00): ?>
      <?php if($mepr_options->coupon_field_enabled): ?>
        <div class="mp-form-row mepr_coupon">
          <div class="mp-form-label">
            <label><?php _e('Coupon Code:', 'memberpress'); ?></label>
          </div>
          <input type="text" id="mepr_coupon_code" class="mepr-form-input" name="mepr_coupon_code" value="<?php echo (isset($mepr_coupon_code))?esc_attr(stripslashes($mepr_coupon_code)):''; ?>" />
        </div>
      <?php else: ?>
        <input type="hidden" id="mepr_coupon_code" name="mepr_coupon_code" value="<?php echo (isset($mepr_coupon_code))?esc_attr(stripslashes($mepr_coupon_code)):''; ?>" />
      <?php endif; ?>
      <?php $active_pms = $product->payment_methods(); ?>
      <?php $pms = $product->payment_methods(); ?>
      <div class="mp-form-row mepr_payment_method">
        <?php echo MeprOptionsHelper::payment_methods_dropdown('mepr_payment_method', $active_pms); ?>
      </div>
    <?php endif; ?>

    <?php if(!MeprUtils::is_user_logged_in()): ?>
      <?php if($mepr_options->require_tos): ?>
        <div class="mp-form-row mepr_tos">
          <label for="mepr_agree_to_tos" class="mepr-checkbox-field mepr-form-input" required>
            <input type="checkbox" name="mepr_agree_to_tos" id="mepr_agree_to_tos" <?php checked(isset($mepr_agree_to_tos)); ?> />
            <a href="<?php echo stripslashes($mepr_options->tos_url); ?>" target="_blank"><?php echo stripslashes($mepr_options->tos_title); ?></a>*
          </label>
        </div>
      <?php endif; ?>

      <?php // This thing needs to be hidden in order for this to work so we do it explicitly as a style ?>
      <input type="text" id="mepr_no_val" style="display: none;" name="mepr_no_val" class="mepr-form-input" autocomplete="off" />
    <?php endif; ?>

    <?php MeprHooks::do_action('mepr-user-signup-fields'); ?>

    <div class="mepr_spacer">&nbsp;</div>

    <input type="submit" class="mepr-submit button button-primary button-large btn btn-theme btn-block" value="<?php echo stripslashes($product->signup_button_text); ?>" />
    <img src="<?php echo admin_url('images/loading.gif'); ?>" style="display: none;" class="mepr-loading-gif" />
  </form>
</div>
<script type="text/javascript">
$(function() {
  //alert('asdf');
$(document).tooltip();

//add checkbox
  $("#custom-reg-field-hardgood-multiple").hide();
  $("#custom-reg-field-hardgood-multiple").val('1');
  $("#custom-reg-field-hardgood-multiple").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forHardgood">');

  $("#custom-reg-field-fresh-flower-multiple").hide();
  $("#custom-reg-field-fresh-flower-multiple").val('1');
  $("#custom-reg-field-fresh-flower-multiple").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forFreshFlower">');

  $("#custom-reg-field---charge-card-rate").hide();
  $("#custom-reg-field---charge-card-rate").val('0');
  $("#custom-reg-field---charge-card-rate").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forChangeCard">');

  $("#custom-reg-field-sales-tax--").hide();
  $("#custom-reg-field-sales-tax--").val('0');
  $("#custom-reg-field-sales-tax--").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forSalesTax">');

  $("#custom-reg-field---delivery-cost").hide();
  $("#custom-reg-field---delivery-cost").val('0');
  $("#custom-reg-field---delivery-cost").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forDeliveryCost">');

  $("#custom-reg-field---labor").hide();
  $("#custom-reg-field---labor").val('0'); 
  $("#custom-reg-field---labor").prev().prev().children('.use-option').before('<input type="checkbox" class="checkboxRegistration" id="forLabor" name="add_labor_costs">');

  $("#forHardgood").click(function(){
    if($('#forHardgood').prop("checked")==true){
      $("#custom-reg-field-hardgood-multiple").show();
    }else{
      $("#custom-reg-field-hardgood-multiple").hide();
      $("#custom-reg-field-hardgood-multiple").val('1');
    }
  });

  $("#forFreshFlower").click(function(){
    if($('#forFreshFlower').prop("checked")==true){
      $("#custom-reg-field-fresh-flower-multiple").show();
    }else{
      $("#custom-reg-field-fresh-flower-multiple").hide();
      $("#custom-reg-field-fresh-flower-multiple").val('1');
    }
  });

  $("#forChangeCard").click(function(){
    if($('#forChangeCard').prop("checked")==true){
      $("#custom-reg-field---charge-card-rate").show();
    }else{
      $("#custom-reg-field---charge-card-rate").hide();
      $("#custom-reg-field---charge-card-rate").val('0');
    }
  });

  $("#forSalesTax").click(function(){
    if($('#forSalesTax').prop("checked")==true){
      $("#custom-reg-field-sales-tax--").show();
    }else{
      $("#custom-reg-field-sales-tax--").hide();
      $("#custom-reg-field-sales-tax--").val('0');
    }
  });

  $("#forDeliveryCost").click(function(){
    if($('#forDeliveryCost').prop("checked")==true){
      $("#custom-reg-field---delivery-cost").show();
    }else{
      $("#custom-reg-field---delivery-cost").hide();
      $("#custom-reg-field---delivery-cost").val('0')
    }
  });

  $("#forLabor").click(function(){
    if($('#forLabor').prop("checked")==true){
      $("#custom-reg-field---labor").show();
    }else{
      $("#custom-reg-field---labor").hide();
      $("#custom-reg-field---labor").val('0')
    }
  });
});

(function($){

$(document).ready(function(){
  var inputs = '\
    #custom-reg-field-hardgood-multiple,\
    #custom-reg-field-fresh-flower-multiple,\
    #custom-reg-field---charge-card-rate,\
    #custom-reg-field-sales-tax--,\
    #custom-reg-field---delivery-cost,\
    #custom-reg-field---labor\
  ';
  stemcounter.floatInput($(inputs));
});

})(jQuery);
</script>
</script>

