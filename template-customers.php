<?php 
/*
Template Name: User - Customers
*/

get_header(); ?>

<section class="wrapper">
	<?php while( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>
	<div id="feedback"></div>
</section><!--/wrapper -->

<?php
get_footer();