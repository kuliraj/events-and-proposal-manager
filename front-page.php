<?php
if ( is_user_logged_in() && ( empty($_SERVER['HTTP_REFERER']) || !stristr($_SERVER['HTTP_REFERER'], home_url()) ) ) {
	wp_redirect(sc_get_events_page_url());
	exit;
}
?>
<?php get_header('home') ?>
<section class="wrapper">
	<?php
		// Start the Loop.
		while ( have_posts() ) : the_post();
			// Include the page content template.
	 the_content();
		endwhile;
	?>
</section><!-- /.wrapper -->

<!-- CTA -->
<div class="full-row darkgrey">
	<div class="row home-footer">
		<div class="large-12 columns video">
			<p>Our missions statement is: Accurate, on-the-spot wedding flower proposals in less time than it takes to make a toss bouquet.</p>
			<a href="http://info.stemcounter.com/wedding-florist-contract-template" class="button">Download a free wedding florist contract template</a>
		</div>
	</div> <!--End row-->

	<div class="blog-cta">
		<p>Visit our <a href="http://info.stemcounter.com/blog">Blog</a></p>
	</div>

	<!-- Social Media Icons-->
	<div class="getting-social">
		<a href="https://www.instagram.com/stemcounter/">
			<img width="60" height="61" src="<?php echo get_template_directory_uri(); ?>/img/Instagram.png" alt="Instagram icon" class="twitter hvr-shrink">
		</a>
		<a href="https://www.facebook.com/stemcounter">
			<img width="60" height="60" src="<?php echo get_template_directory_uri(); ?>/img/Facebook.png" alt="Facebook icon" class="facebook hvr-shrink">
		</a>
		<a href="https://twitter.com/stemcounter?lang=en">
			<img width="60" height="61" src="<?php echo get_template_directory_uri(); ?>/img/Twitter.png" alt="Twitter icon" class="twitter hvr-shrink">
		</a>
	</div> <!--End Getting Social-->
	<div class="row">
		<div class="home-footer-cta">
			<?php wp_nav_menu( array( 'menu' => 'Footer-home' ) ); ?>
		</div>
	</div>

</div>
<!--End CTA-->

<?php get_footer('home'); ?>