	<?php get_template_part('blocks/ajax-modal-template'); ?>

	<!--footer start-->
	<footer class="site-footer">
		<div class="text-center print-only">
			<p>Made using Stemcounter.com</p>
		</div>
		<div class="text-center">
			<?php echo date('Y'); ?> - StemCounter.com
		</div>
	</footer>
	<!--footer end-->
		
	<?php wp_footer(); ?>

	<!--custom switch-->
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-switch.js"></script>
    
    <!-- Google Code for Remarketing Tag -->
	<!--------------------------------------------------
	Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive 			categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
	--------------------------------------------------->
	<script type="text/javascript">
	/* <![CDATA[ */
		var google_conversion_id = 983161589;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src=		"//googleads.g.doubleclick.net/pagead/viewthroughconversion/983161589/?value=0&guid=ON&script=0"/>
		</div>
	</noscript> 
</body>
</html>