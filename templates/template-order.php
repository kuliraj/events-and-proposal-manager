<?php
/*
The following variables will be available when this template is loaded:
 - $order Stemcounter\Order The order record.
 - $company_info array Information about the current user.
 - $page_title string
 - $notes string Any notes the florist wants to add to the order
 - $user WP_User The currently logged-in user
 */

$from = array();
if ( ! empty( $user->display_name ) ) {
	$from[] = $user->display_name;
}

$shopping_list = sc_get_shopping_list_from_order_items( $order->items, $with_photos ); ?>

	<div class="wrapper order no_photos" id="table_wrap">
		<h3><?php echo $page_title; ?></h3>

		<p style="width: 100%; text-align: left; margin-top: 0;">From: <?php echo $user->display_name ? esc_html( $user->display_name ) : esc_html( $user->first_name . ' ' . $user->last_name ); ?></p>

		<p style="width: 100%; text-align: left; margin-top: 0;">Order Name: <?php echo esc_html( $order->name ); ?></p>

		<?php if ( $order->fulfilment_date ) : ?>
			<p style="width: 100%; text-align: left; margin-top: 0;">Fulfilment Date: <?php echo sc_get_user_date_time( $order->user_id, $order->fulfilment_date->toDateTimeString() ); ?>
		<?php endif; ?>

		<?php if ( ! empty( $company_info['address'] ) ) : ?>
			<p style="width: 100%; text-align: left; margin-top: 0;">Address: <?php echo esc_html( $company_info['address'] ); ?></p>
		<?php endif; ?>

		<?php if ( ! empty( $company_info['email'] ) ) : ?>
			<p style="width: 100%; text-align: left; margin-top: 0;">Email Address: <?php echo esc_html( $company_info['email'] ); ?></p>
		<?php endif; ?>

		<?php if ( ! empty( $company_info['phone'] ) ) : ?>
			<p style="width: 100%; text-align: left; margin-top: 0;">Phone Number: <?php echo esc_html( $company_info['phone'] ); ?></p>
		<?php endif; ?>

		<?php if ( ! empty( $_GET['email'] ) ) : ?>
			<div id="notes-wrap">
				<?php echo str_ireplace( '<p', '<p style="width: 100%; text-align: left; margin-top: 0;"', wp_kses_post( $notes ) ); ?>
			</div>
		<?php else : ?>
			<?php if ( ! empty( $notes ) ) : ?>
				<?php echo str_ireplace( '<p style="width: 100%; text-align: left; margin-top: 0;"', '', wp_kses_post( $notes ) ); ?>
			<?php endif; ?>
		<?php endif; ?>
		
		<div class="" style="clear:both;"></div>
		<table style="width:100% !important; color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse; border-spacing: 0; margin: 15px 0 15px 0; padding: 25px; ">
			<thead>
				<tr>
					<th style="border: 1px solid #DFDFDF; height: 30px; background: #DFDFDF; font-weight: bold; text-align: left; padding-left: 10px;">Type</th>
					<th style="border: 1px solid #DFDFDF; height: 30px; background: #DFDFDF; font-weight: bold; text-align: left; padding-left: 10px; width: 50%;">Item</th>
					<?php if ( $with_photos ): ?>
						<th class="photo" style="border: 1px solid #DFDFDF; height: 30px; background: #DFDFDF; font-weight: bold; text-align: left; padding-left: 10px; width: 10%;">Photo</th>
					<?php endif ?>
					<th style="border: 1px solid #DFDFDF; height: 30px; background: #DFDFDF; font-weight: bold; text-align: center; width: 10%;">Qty</th>
					<th style="border: 1px solid #DFDFDF; height: 30px; background: #DFDFDF; font-weight: bold; text-align: center; width: 10%;">PU Expected</th>
				</tr>
			</thead>
			<?php foreach ( $shopping_list['included'] as $item ) : ?>
				<tr>
					<td style="border: 1px solid #DFDFDF; height: 30px; background: #FAFAFA; text-align: left; text-align: left; padding-left: 10px;"><?php echo $item['type']; ?></td>
					<td style="border: 1px solid #DFDFDF; height: 30px; background: #FAFAFA; text-align: left; text-align: left; padding-left: 10px;"><?php echo $item['name']; ?></td>
					<?php if ( $with_photos ): ?>
						<td class="photo" style="border: 1px solid #DFDFDF; height: 30px; background: #FAFAFA; text-align: left; text-align: left; padding-left: 10px;">
							<div style="background-image: url('<?php echo $item['photo']['imageThumbURL']; ?>'); background-size: contain; background-repeat: no-repeat; background-position: center; width: 100%; height: 100%" >
								<a target="_blank" href="<?php echo( $item['photo']['imageURL'] ); ?>" style="width: 100%; height: 100%; display: inline-block"></a>
							</div>
						</td>
					<?php endif; ?>
					<td style="border: 1px solid #DFDFDF; height: 30px; background: #FAFAFA; text-align: center; text-align: center;"><?php echo sc_format_quantity( $item['quantity'] ); ?></td>
					<td style="border: 1px solid #DFDFDF; height: 30px; background: #FAFAFA; text-align: center; text-align: center;"><?php echo $item['su_needed']; ?></td>
	 			</tr>
			<?php endforeach; ?>
		</table>
		<div class="clearfix" style="clear: both;"></div>
		
		<div style="float: right; padding: 20px 0px;">
			<div style="text-align: center;">This wholesaler list was powered by <a href="<?php echo esc_url( home_url( '/' ) ); ?>">Stemcounter.com</a></div>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" style="width=300px; height=77px; display: block;"><img src="<?php echo get_template_directory_uri(); ?>/img/sc-logo-horizontal.png" width=300px; height=77px; /></a>
		</div>

	</div>

<?php if ( ! empty( $_GET['email'] ) ) : ?>
	<style type="text/css">
		#table_wrap.no_photos td.photo,
		#table_wrap.no_photos th.photo {
			display: none;
		}
	</style>
	<script>
		if ( window.parent ) {
			var arrFrames = window.parent.document.getElementsByTagName( 'IFRAME' ),
				notesDiv = document.getElementById( 'notes-wrap' );

			for (var i = 0; i < arrFrames.length; i++) {
				if ( arrFrames[i].contentWindow === window ) {
					arrFrames[i].height = arrFrames[i].contentWindow.document.body.scrollHeight;
					break;
				};
			}

			window.updateNotes = function( notes ){
				if ( '' !== notes ) {
					notesDiv.innerHTML = '<p style="width: 100%; text-align: left; margin-top: 0;">' + notes.replace( /\n/g, '<br />' ) + '</p>';
				} else {
					notesDiv.innerHTML = '';
				}
			}

			window.togglePhotosColumn = function( toggled ) {
				if ( toggled ) {
					document.getElementById( 'table_wrap' ).classList.remove( 'no_photos' );
				} else {
					document.getElementById( 'table_wrap' ).classList.add( 'no_photos' );
				}
			}

		}
	</script>
	<style>
		body {
			margin: 0;
		}
		body .wrapper {
			padding: 20px 0 0 0;
		}
		body .wrapper h3 {
			margin-top: 0;
		}
	</style>
<?php endif; ?>
