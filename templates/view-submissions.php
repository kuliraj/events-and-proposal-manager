
<?php if ( $submissions && 0 < $submissions->count() ) : ?>
	<table id="eventsTable" class="table table-striped table-advance table-hover user_ev_meta form-submissions-table" data-page-length="<?php echo ! empty( $table_len ) ? $table_len : 10 ; ?>">
		<thead>
			<tr>
				<th>Form</th>
				<th>Customer</th>
				<th class="hidden-xs">Submitted on</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $submissions as $submission ) : ?>
				<tr data-submission="<?php echo esc_attr( $submission->id ); ?>" class="submission-row <?php echo $submission->seen ? '' : 'not-seen'; ?>">
					<td><?php echo $submission->form->name; ?></td>
					<td><?php echo $submission->customer_name ? esc_html( $submission->customer_name ) : 'N/A'; ?></td>
					<td>
						<?php echo date( $date_format, strtotime( $submission->created_at ) ); ?>
						<?php if ( ! $is_archived ) : ?>
							<div class="pull-right">
								<a href="#" class="archive-submission" title="Archive"><i class="fa fa-folder-o"></i></a>
							</div>
						<?php else: ?>
							<div class="pull-right">
								<a href="#" class="unarchive-submission" title="Unarchive"><i class="fa fa-folder-open-o"></i></a>
							</div>
						<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php if ( $is_archived ) : ?>
		<div class="form-submissions-links">
			<a href="<?php echo esc_url( remove_query_arg( 'archived' ) ); ?>">All Submissions</a>
		</div>
	<?php else : ?>
		<div class="form-submissions-links">
			<a href="<?php echo esc_url( add_query_arg( 'archived', 1 ) ); ?>">Archived</a>
		</div>
	<?php endif; ?>
<?php else : ?>
	<?php if ( $is_archived ) : ?>
		<h3>There are no archived form submissions.</h3>
		<div class="form-submissions-links">
			<a href="<?php echo esc_url( remove_query_arg( 'archived' ) ); ?>">All Submissions</a>
		</div>
	<?php else : ?>
		<h3>There are no form submissions yet.</h3>
	<?php endif; ?>
<?php endif; ?>
