<!DOCTYPE html>
<html>
<head>
	<title><?php echo $page_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/html-template.css?v=2.0">
</head>
<body <?php echo isset( $_GET['bg-color'] ) ? 'class="bg-color"' : ''; ?>>