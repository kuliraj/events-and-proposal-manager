<?php
/*
The following variables will be available when this template is loaded:
 - $event Event - the event object(latest created version)
 - $payments - payments based on the version.
 - $company_info - array with user's stored info.
 - $page_title - 'sting'
 - $customer - array with users's customer info.
 - $created_at - time of versioning.
 - $eventCost - object with proposal costs.
 */

use Stemcounter\Event;
use Stemcounter\Payment;

$payments_made = 0;

foreach ( $payments as $payment ) {
	if ( 'not-paid' != $payment->status ) {
		if ( ! empty( $payment->payment_percentage ) && is_null( $payment->amount ) ) {
			$payments_made += $eventCost->total * ( $payment->payment_percentage / 100 );
		} else {
			$payments_made += $payment->amount;
		}
	}
}

$logo = null;
if ( sc_get_profile_logo_on_proposal( $user_id ) ) {
	$logo_id = sc_get_profile_logo_id( $user_id );
	$logo = wp_get_attachment_image_src( $logo_id, 'medium' );
}

$current_user = get_userdata( $event->user_id );
$additionalInfo = unserialize( $current_user->additionalInfo ); //Get User Details
$company_info = (object) get_company_info( $event->user_id );
$company_info->phone = isset( $additionalInfo['companyMobile'] ) ? $additionalInfo['companyMobile'] : '';
$company_info->email = isset( $current_user->user_email ) ? $current_user->user_email : ''; ?>

	<div class="wrapper invoice" >
		<div class="col-left" style="display: block; text-align: center;">
			<div class="company-logo" ><img src="<?php echo $logo[0]; ?>" width="150" alt=""></div>
			<div>
				<ul class="company-info" style="list-style: none; line-height: 23px; padding: 0; margin-top: 5px">
					<li><?php echo $company_info->company; ?></li>
					<li><?php echo $company_info->address; ?></li>
					<li ><?php echo $company_info->phone; ?></li>
					<li style="padding-top:15px;"><a href="mailto:<?php echo $company_info->email; ?>" style="text-decoration: none; color: #000000;"><?php echo $company_info->email; ?></a></li>
					<li><a href="<?php echo $company_info->website; ?>" target="_blank"  style="text-decoration: none; color: #000000;"><?php echo $company_info->website; ?></a></li>
				</ul>
			</div>
			
		</div>
		<div class="col-right" style=" display: block; padding: 0; text-align: center;">
			<h3 style="color: #797979; font-size: 25px; min-width: 200px; margin-top: 0; margin-bottom: 15px;">Invoice</h3>
				<table style="width:100% !important; color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse; border-spacing: 0; margin: 15px 0 15px 0; padding: 25px; ">
					<tr>
						<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold; text-align: left; padding: 2px 5px;">Date</th>
					</tr>
					<tr>
						<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: left; padding: 2px 5px;"><?php echo sc_get_user_date_time( $user_id, $created_at ); ?></td>
		 			</tr>
				</table>

			<div class="customer-info">
				<table style="width:100% !important; color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse; border-spacing: 0; margin: 15px 0 15px 0; padding: 25px; ">
					<tr>
						<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold; text-align: left; padding: 2px 5px;">Prepared For</th>
					</tr>
					<?php foreach ( $customers as $customer ) : ?>
						<tr>
							<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: left; padding: 2px 5px;">
								<?php if ( ! empty( $customer->name ) ): ?>
									<?php echo $customer->name . '<br>'; ?>
								<?php endif; ?>
								<?php if ( ! empty( $customer->address ) ): ?>
									<?php echo $customer->address . '<br>'; ?>
								<?php endif; ?>
								<?php if ( ! empty( $customer->phone ) ): ?>
									<?php echo $customer->phone . '<br>'; ?>
								<?php endif; ?>
								<?php if ( ! empty( $customer->email ) ): ?>
									<a href="mailto:<?php echo $customer->email; ?>" style="text-decoration: none; color: #000000;"><?php echo $customer->email; ?></a>
								<?php endif; ?>
							</td>
			 			</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
		<div class="" style="clear:both;"></div>
		<table style="width:100% !important; color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse; border-spacing: 0; margin: 15px 0 15px 0; padding: 25px; ">
			<thead>
				<tr>
					<th style="border: 1px solid #DFDFDF; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold; text-align: left; padding-left: 10px;">Name</th>
					<th style="border: 1px solid #DFDFDF; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold; text-align: right; padding-right: 10px; width: 12%;">Qty</th>
					<th style="border: 1px solid #DFDFDF; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold; text-align: right; padding-right: 10px; width: 12%;">Amt</th>
					<th style="border: 1px solid #DFDFDF; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold; text-align: right; padding-right: 10px; width: 12%;">Total</th>
				</tr>
			</thead>
			<?php foreach ( $event->arrangements as $arrangement ) :
				if ( intval( $arrangement->addon ) || ! intval( $arrangement->include ) ) {
					continue;
				} ?>
				<tr>
					<td style="border: 1px solid #DFDFDF; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center; text-align: left; padding-left: 10px;"><?php echo $arrangement->name; ?></td>
					<td style="border: 1px solid #DFDFDF; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center; text-align: right; padding-right: 10px;"><?php echo $arrangement->quantity; ?></td>
					<td style="border: 1px solid #DFDFDF; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center; text-align: right; padding-right: 10px;"><?php echo sc_format_price( $arrangement->total, true, $event->user_id ); ?></td>
					<td style="border: 1px solid #DFDFDF; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center; text-align: right; padding-right: 10px;"><?php echo sc_format_price( $arrangement->subTotal, true, $event->user_id ); ?></td>
	 			</tr>
			<?php endforeach; ?>
		</table>
		<div class="col-reft overview" style="display: block; padding: 0; text-align: center; float:left;">
			<p style="width: 270px; text-align: left; margin-top: 0;">Payments Made: <span style="float: right;"><?php echo sc_format_price( $payments_made, true, $event->user_id ); ?></span></p>
			<p style="width: 270px; text-align: left; margin-bottom: 0;">Balance Due: <span style="float: right;"><?php echo sc_format_price( $eventCost->total - $payments_made, true, $event->user_id ); ?></span></p>
		</div>
		<div class="col-right costs" style=" display: block; padding: 0; text-align: center;">
			<p style="width: 270px; text-align: left; margin-top: 0;">Delivery: <span style="float: right;"><?php echo sc_format_price( $eventCost->delivery, true, $event->user_id ); ?></span></p>
			<?php if ( 0 < $eventCost->discount ): ?>
				<p style="width: 270px; text-align: left; margin-bottom: 0;">Discount: <span style="float: right;">(<?php echo sc_format_price( $eventCost->discount, true, $event->user_id ); ?>)</span></p>
			<?php endif; ?>
			<p style="width: 270px; text-align: left; margin-bottom: 0;">Subtotal: <span style="float: right;"><?php echo sc_format_price( $eventCost->subtotal, true, $event->user_id ); ?></span></p>
			<p style="width: 270px; text-align: left; margin-bottom: 0;">Tax: <span style="float: right;"><?php echo sc_format_price( $eventCost->tax, true, $event->user_id ); ?></span></p>
			<p style="width: 270px; text-align: left; margin-bottom: 0;">Total: <span style="float: right;"><?php echo sc_format_price( $eventCost->total, true, $event->user_id ); ?></span></p>
		</div>
		<div class="clearfix" style="clear: both;"></div>

	</div>
