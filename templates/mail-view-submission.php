
	<table style="width:100% !important; color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse; border-spacing: 0; margin: 15px 0 15px 0; padding: 25px; " cellpadding="5">
		<tbody>
			<?php foreach ( $submission->answers as $answer ) : ?>
				<tr bgcolor="#FAFAFA">
					<td colspan="2" style="height: 30px; background: #FAFAFA; text-align: left; padding-left: 10px; font-weight: bold;">
						<strong><?php echo $answer->question->label; ?></strong>
					</td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td width="20" style="height: 30px; background: #FFFFFF;">&nbsp;</td>
					<td style="height: 30px; background: #FFFFFF; text-align: left; padding-left: 10px;">
						<?php echo $answer->answer->map( 'wpautop' )->implode( "\n" ); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
