<?php
get_header( 'new-design' ); ?>

<div class="view-form-wrap content-panel">
	<div class="container-fluid">
		<?php if ( ! empty( $company ) || ! empty( $logo ) ) : ?>
			<div class="row">
				<div class="col-xs-12">
					<?php if ( ! empty( $logo ) ) : ?>
						<img src="<?php echo esc_url( $logo ); ?>" alt="company logo" class="company-logo" />
					<?php endif; ?>

					<?php if ( ! empty( $company ) ) : ?>
						<h2 class="company-name"><?php echo $company; ?></h2>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="row">
			<div class="col-sm-12">
				<div id="form-container" class="builder-form col-sm-12 center sc-loader-container"></div>
			</div>
		</div>
	</div>
</div><!-- /.view-form-wrap -->

<script>
	(function($){
		$(document).ready(function(){
			var settings = {};
			settings.schema = <?php echo json_encode( $form->prep_for_js() ); ?>;
			settings.dateFormat = <?php echo json_encode( $dateFormat ); ?>;
			settings.node = $('#form-container');
			console.log( settings );
			$(document).trigger( 'stemcounter.action.renderICForm', settings );
		});
	})(jQuery);
</script>

<?php

get_footer();
