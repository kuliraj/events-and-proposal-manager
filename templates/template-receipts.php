<?php
/*
The following variables will be available when this template is loaded:
 - $receipt Receipt - the receipt object
 - $event Event - the event object
 - $company_info - array with user's stored info.
 - $page_title - 'sting'
 - $customer - array with users's customer info.
 */

use Stemcounter\Event;
$logo = null;
if ( sc_get_profile_logo_on_proposal( $event->user_id ) ) {
	$logo_id = sc_get_profile_logo_id( $event->user_id );
	$logo = wp_get_attachment_image_src( $logo_id, 'medium' );
}

if ( 'not-paid' == $receipt->status ) {
	$receipt->payment_type = '';
	$receipt->status = 'unpaid';
}
 // var_dump($receipt);
$current_user = get_userdata( $event->user_id );
$additionalInfo = unserialize( $current_user->additionalInfo ); //Get User Details
$company_info['phone'] = isset( $additionalInfo['companyMobile'] ) ? $additionalInfo['companyMobile'] : '';
$company_info['email'] = isset( $current_user->user_email ) ? $current_user->user_email : '';
$date_format = get_user_meta( $event->user_id, '__date_format', true ); ?>

	<div class="wrapper" ">
		<div class="col-left" style="display: block; text-align: center;">
			<div class="company-logo" ><img src="<?php echo $logo[0]; ?>" width="200" alt=""></div>
			<div>
				<ul class="company-info" style="list-style: none; line-height: 23px; padding: 0;">
					<li><?php echo $company_info['company']; ?></li>
					<li><?php echo $company_info['address']; ?></li>
					<li ><?php echo $company_info['phone']; ?></li>
					<li style="padding-top:15px;"><a href="mailto:<?php echo $company_info['email']; ?>" style="text-decoration: none; color: #000000;"><?php echo $company_info['email']; ?></a></li>
					<li><a href="<?php echo $company_info['website']; ?>" target="_blank"  style="text-decoration: none; color: #000000;"><?php echo $company_info['website']; ?></a></li>
				</ul>
			</div>
			
		</div>
		<div class="col-right" style=" display: block; padding: 0; text-align: center;">
			<h3 style="color: #797979; font-size: 25px;">Payment Receipt</h3>
				<table style="width:100% !important; color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse; border-spacing: 0; margin: 15px 0 15px 0; padding: 25px; ">
					<tr>
						<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold;">Event</th>
					</tr>
					<tr>
						<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center; ">
							<?php echo $event->name; ?>
						</td>
		 			</tr>
	 			</table>
				<table style="width:100% !important; color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse; border-spacing: 0; margin: 15px 0 15px 0; padding: 25px; ">
					<tr>
						<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold;">Due Date</th>
						<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold;">Payment Date</th>
						<?php if ( ! empty( $receipt->payment_date ) && 'complete' == $receipt->status ) : ?>
							<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold;">Payment ID</th>
						<?php endif; ?>
					</tr>
					<tr>
						<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center;"><?php echo date( $date_format, strtotime( $receipt->due_date ) ); ?></td>
						<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center;"><?php echo ! empty( $receipt->payment_date ) && 'complete' == $receipt->status ? date( $date_format, strtotime( $receipt->payment_date )) : 'Unpaid'; ?></td>
						<?php if ( ! empty( $receipt->payment_date ) && 'complete' == $receipt->status ) : ?>
							<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center;"><?php echo $receipt->id; ?></td>
						<?php endif; ?>
		 			</tr>
				</table>

			<span class="payment-date"></span>
			<div class="customer-info">

				<table style="width:100% !important; color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse; border-spacing: 0; margin: 15px 0 15px 0; padding: 25px; ">
					<tr>
						<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold;">Bill To</th>
					</tr>
					<?php if ( ! empty( $customer['name'] ) ): ?>
						<tr>
							<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center; ">
								<?php echo $customer['name']; ?>
							</td>
			 			</tr>
					<?php endif; ?>
					<?php if ( ! empty( $customer['address'] ) ): ?>
			 			<tr>
							<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center; ">
								<?php echo $customer['address']; ?>
							</td>
			 			</tr>
					<?php endif; ?>
					<?php if ( ! empty( $customer['phone'] ) ): ?>
			 			<tr>
							<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center; ">
								<?php echo $customer['phone']; ?>
							</td>
			 			</tr>
					<?php endif; ?>
					<?php if ( ! empty( $customer['email'] ) ): ?>
			 			<tr>
							<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center; ">
								<a href="mailto:<?php echo $customer['email']; ?>" style="text-decoration: none; color: #000000;"><?php echo $customer['email']; ?></a>
							</td>
			 			</tr>
					<?php endif; ?>
				</table>
			</div>
		</div>
		<div class="" style="clear:both;"></div>
		<table style="width:100% !important; color: #333; font-family: Helvetica, Arial, sans-serif; width: 640px; border-collapse: collapse; border-spacing: 0; margin: 5px 0 15px 0; padding: 25px; ">
			<tr>
				<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold;">Status</th>
				<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold;">Payment Type</th>
				<th style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #DFDFDF; font-weight: bold;">Amount</th>
			</tr>
			<tr>
				<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center;"><?php echo 'complete' == $receipt->status ? 'Paid' : ucwords( $receipt->status ); ?></td>
				<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center;"><?php echo $receipt->payment_type; ?></td>
				<td style="border: 1px solid transparent; height: 30px; transition: all 0.3s; background: #FAFAFA; text-align: center;"><?php echo sc_format_price( $receipt->amount, true ); ?></td>
 			</tr>
		</table>

	</div>
