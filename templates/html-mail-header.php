<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $page_title; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/html-template.css?v=1.1">
	<style type="text/css">
		@media screen and (min-device-width: 567px) {
			div[class="col-left"] {
				display: inline-block;
				text-align: left !important;
			}

			div[class="col-right"] {
				display: inline-block;
			}
		}
	</style>
</head>
<body <?php echo isset( $_GET['bg-color'] ) ? 'class="bg-color"' : ''; ?>>