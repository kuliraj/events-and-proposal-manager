<?php 
/*
Template Name: Event - Breakdown
*/

use Stemcounter\Event;
use Stemcounter\Vendor;
use Stemcounter\Item;
use Stemcounter\Item_Variation;
use Stemcounter\Arrangement;
use Stemcounter\Arrangement_Item;

if ( ! isset( $_GET['eid'] ) ) {
	echo 'Event was not found.';
	die;
}

$event = Event::where(array(
	'id' => $_GET['eid'],
))->with(array(
	'arrangements' => function($query) {
		$query->where('include', '=', 1)->where('addon', '=', 0)->orderBy( 'order', 'asc' );
	},
), 'arrangements.items', 'arrangements.items.item_variation' )->firstOrFail();

$event->arrangements->sortBy( function($arrangement){
	return $arrangement->order;
} );

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php wp_title(' ', true, 'right'); ?></title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
<section id="container">
	<section id="main-content" class="main-content">
		<section class="wrapper">
			<div class="proposal-container new-proposal-container container">

			<div id="event-proposal">
				<div class="proposal-wrapper">

					<div class="proposal-toolbar">
						<div class="row">
							<div class="col-sm-12">
								<div class="pull-left event-breadcrumbs">
									<a href="<?php echo esc_url( sc_get_permalink_by_template( 'template-events.php' ) ); ?>" class="events-link">Events</a> &gt; <span class="event-name"><?php echo $event->name; ?></span>
								</div>
								<div class="toolbar-action pull-right"><a href="<?php echo esc_url( add_query_arg( 'eid', $event->id, sc_get_proposal_2_0_page_url() ) ); ?>">Back to the proposal</a></div>
							</div>
						</div>
					</div>

					<div class="container event-breakdown-container">
						<?php

						//Collect data
						$items_data = array();
						$types_data = array();
						$arrangements_data = array();
						$event_data = array();

						foreach ( $event->arrangements as $arr_key => $arrangement ) :
							if ( ! $arrangement->include ) {
								continue;
							}
							//Arrangements calculations
							$id = $arrangement->id;
							$arrangements_data[$id]['name'] = $arrangement->name;
							$arrangements_data[$id]['quantity'] = $arrangement->quantity;
							$arrangements_data[$id]['cost'] = $arrangement->get_cost() * $arrangement->quantity;
							$arrangements_data[$id]['labor'] = $arrangement->get_labor();
							$arrangements_data[$id]['markup'] = $arrangement->get_markup();
							$arrangements_data[$id]['price'] = $arrangement->get_final_price( true );
							$arrangements_data[$id]['profit'] = $arrangements_data[$id]['price'] - $arrangements_data[$id]['cost'] - $arrangements_data[$id]['labor'];
							$arrangements_data[$id]['tax'] = $arrangement->tax;

							@$event_data['cost'] += $arrangements_data[$id]['cost'];
							@$event_data['labor'] += $arrangements_data[$id]['labor'];

						 	foreach ( $arrangement->items as $arrangement_item ) {
						 		//Recipe items calculations
								$type = sc_get_item_type($arrangement_item->item, true, $arrangement_item);
								//$type = sc_get_simple_item_type($arrangement_item->item, false, $arrangement_item);

								$id = $arrangement_item->item_variation_id;
								if ( empty( $items_data[$type][$id] ) ) {
									$items_data[$type][$id]['name'] = $arrangement_item->get_full_name();
								}

								$item_cost = $arrangement_item->get_cost_total() * $arrangement->quantity;
								$item_price = $arrangement_item->get_calculated_price( true ) * $arrangement->quantity;
								$item_labor = $arrangement_item->get_labor() * $arrangement->quantity;
								$item_profit = $item_price - $item_cost - $item_labor;

								@$items_data[$type][$id]['cost'] += $item_cost;
								@$items_data[$type][$id]['labor'] += $item_labor;
								@$items_data[$type][$id]['price'] += $item_price;
								@$items_data[$type][$id]['profit'] += $item_profit;

								@$types_data[$type]['cost'] += $item_cost;
								@$types_data[$type]['labor'] += $item_labor;
								@$types_data[$type]['price'] += $item_price;
								@$types_data[$type]['profit'] += $item_profit;
								/* <span class="arr-item-cost">Markup: <?php echo sc_format_price( $arrangement_item->get_markup(), true ); ?></span> */
						 	}
						endforeach;

						$event_data['price'] = $event->get_total();
						$event_data['tax'] = $event->get_tax() + $event->get_tax2();
						$event_data['profit'] = $event_data['price'] - $event_data['cost'] - $event_data['tax'] - $event_data['labor'];

						//Show data ?>
						<div class="content-panel">
							<h4><i class="fa fa-angle-right"></i> Event</h4>
							<table class="table table-bordered table-condensed event-breakdown">
							<thead>
								<tr>
									<th>Cost</th>
									<th>Tax</th>
									<th>Labor</th>
									<th>Price</th>
									<th>Profit</th>
								</tr>
							</thead>	
							<tbody>
								<tr>
									<td><?php echo sc_format_price( $event_data['cost'], true ); ?></td>
									<td><?php echo sc_format_price( $event_data['tax'], true ); ?></td>
									<td><?php echo sc_format_price( $event_data['labor'], true ); ?></td>
									<td><?php echo sc_format_price( $event_data['price'], true ) ?></td>
									<td><?php echo sc_format_price( $event_data['profit'], true ); ?></td>
								</tr>
							</tbody>
							</table>
						</div>

						<div class="content-panel">
							<h4><i class="fa fa-angle-right"></i> Arrangements</h4>
							<table class="table table-bordered table-condensed event-breakdown">
							<thead>
								<tr>
									<th>Arrangement</th>
									<th>Qty</th>
									<th>Taxable</th>
									<th>Cost</th>
									<th>Labor</th>
									<!-- <th>Markup</th> -->
									<th>Price</th>
									<th>Profit</th>
								</tr>
							</thead>	
							<tbody>
							<?php
							foreach ($arrangements_data as $arr_key => $arrangement) : ?>
								<tr>
									<td><?php echo $arrangement['name']; ?></td>
									<td><?php echo intval( $arrangement['quantity'] ); ?></td>
									<td><?php echo ( $arrangement['tax'] ) ? 'Yes' : 'No'; ?></td>
									<td><?php echo sc_format_price( $arrangement['cost'], true ); ?></td>
									<td><?php echo sc_format_price( $arrangement['labor'], true ); ?></td>
									<!-- <td><?php echo sc_format_price( $arrangement['markup'], true ); ?></td> -->
									<td><?php echo sc_format_price( $arrangement['price'], true ); ?></td>
									<td><?php echo sc_format_price( $arrangement['profit'], true ); ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
							</table>
						</div>

						<div class="content-panel">
							<h4>
							<i class="fa fa-angle-right"></i>
							Items
							<span class="sc-tooltip-wrap">
									<span class="tooltip-msg"><i class="fa fa-question-circle"></i></span>
									<span class="tooltip-content">Items prices will not match with manually entered arrangement prices</span>
								</span>
							</h4>
							<table class="table table-bordered table-condensed event-breakdown">
							<thead>
								<tr>
									<th>Type</th>
									<th>Item</th>
									<th>Cost</th>
									<th>Labor</th>
									<th>Price</th>
									<th>Profit</th>
								</tr>
							</thead>	
							<tbody>
							<?php
							foreach ($types_data as $type=>$items) : ?>
								<tr class="row-type">
									<td><?php echo $type; ?></td>
									<td>&nbsp;</td>
									<td><?php echo sc_format_price( $items['cost'], true ); ?></td>
									<td><?php echo sc_format_price( $items['labor'], true ); ?></td>
									<td><?php echo sc_format_price( $items['price'], true ); ?></td>
									<td><?php echo sc_format_price( $items['profit'], true ); ?></td>
								</tr>
								<?php foreach( $items_data[$type] as $item ) : ?>
									<tr class="row-item">
										<td>&nbsp;</td>
										<td><?php echo $item['name']; ?></td>
										<td><?php echo sc_format_price( $item['cost'], true ); ?></td>
										<td><?php echo sc_format_price( $item['labor'], true ); ?></td>
										<td><?php echo sc_format_price( $item['price'], true ); ?></td>
										<td><?php echo sc_format_price( $item['profit'], true ); ?></td>
									</tr>

								<?php endforeach; ?>
							<?php endforeach; ?>
							</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
			</div>
		</section>
	</section>
</section>



<?php wp_footer(); ?>
<script type="text/javascript">
(function($){
	$( document ).ready(function() {

	});
})(jQuery);	
</script>

</body>

</html>

