<?php
/*
Template Name: User - Onboarding
*/
get_header(); ?>

	<section class="wrapper">
		<?php the_content(); ?>
	</section><!--/wrapper -->

<?php if ( ! get_user_meta( get_current_user_id(), '_sc_tracked_initial_purchase', true ) ) :
	$user = new MeprUser( get_current_user_id() );
	$purchases = $user->active_product_subscriptions( 'transactions' );
	if ( $purchases ) {
		$last_purchase = $purchases[0]->total <= 0.00 ? $purchases[0]->amount : $purchases[0]->total;
		$last_purchase = floatval( sc_format_price( $last_purchase ) );
		if ( defined( 'FB_PIXEL_ID' ) && FB_PIXEL_ID ) : ?>
			<script>
				fbq(
					'track',
					'Purchase', {
						value: <?php echo json_encode( $last_purchase ); ?>,
						currency: 'USD'
					}
				);
			</script>
		<?php endif;
		
		if ( defined( 'GOOGLE_CONVERSION_ID' ) && GOOGLE_CONVERSION_ID ) : ?>
			<!-- Google Code for Sign up Conversion Page -->
			<script type="text/javascript">
				/* <![CDATA[ */
				var google_conversion_id = <?php echo json_encode( GOOGLE_CONVERSION_ID ); ?>;
				var google_conversion_language = "en";
				var google_conversion_format = "3";
				var google_conversion_color = "ffffff";
				var google_conversion_label = "a1TSCLOcynEQ9bXn1AM";
				var google_conversion_value = <?php echo json_encode( $last_purchase ); ?>;
				var google_conversion_currency = "USD";
				var google_remarketing_only = false;
				/* ]]> */
			</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
			<noscript>
				<div style="display:inline;">
					<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/<?php echo GOOGLE_CONVERSION_ID; ?>/?value=<?php echo $last_purchase; ?>&amp;currency_code=USD&amp;label=a1TSCLOcynEQ9bXn1AM&amp;guid=ON&amp;script=0"/>
				</div>
			</noscript>
		<?php endif;
		update_user_meta( get_current_user_id(), '_sc_tracked_initial_purchase', 'yes' );
	}
endif; ?>

<?php get_footer(); ?>