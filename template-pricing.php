<?php
/*
Template Name: User - Pricing
*/
get_header( 'pricing' );
the_post();
?>
<section id="main-content">
	<section class="wrapper">
		<?php the_content(); ?>
	</section>
</section>

<?php get_footer( 'blog' ); ?>

