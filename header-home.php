<!DOCTYPE html PUBLIC>
<!--[if gt IE 8]>
<!-->
<html class="no-js" lang="en" >
<!--<![endif]-->
<head>
  <?php get_template_part('head'); ?>
  <meta name="description" content="Event and wedding florist software to create floral design proposals, generate detailed wholesaler order for flowers, wedding management systems, stem count." />
</head>
<body <?php body_class(); ?>>
  <!-- Begin Menu Bar -->
  <header id="home" class="header-homepage">
    <div class="full-row navbar-fixed-top clearfix">
      <div class="container">
        <div class="home-logo" title="Floral Software Logo of <?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/sc-logo-horizontal.png')"></div>
        <div class="home-header-cta">
          <div class="mobile-menu">
            <a href="#" class="menu-link"><i class="fa fa-bars" aria-hidden="true"></i></a>
            <?php wp_nav_menu('theme_location=home-header&container=&fallback_cb=&depth=1') ?>
          </div>
        </div>
      </div>
    </div>
  <!-- End Top Navigation Bar -->
  </header>