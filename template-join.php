
<?php
/*
Template Name: Public - Memberpress join
 */

get_header('blog'); ?>

<section id="main-content">
  <section class="wrapper">
  <?php
  while ( have_posts() ) : the_post();
    the_content();
  endwhile;
  ?>
  </section>
</section>

<?php get_footer('blog'); ?>
