<?php
// sleep(60);
define('SC_INVITE_ONLY', false);

define('MYSQL_DATE_FORMAT', 'Y-m-d H:i:s');
define('SC_DATE_FORMAT', 'm/d/Y');

define('STEMCOUNTER_MCRYPT_IV', 'XOB^rV*Z');
define('ENCRYPTION_KEY', '0ae7976dda775fd5f45c36187d938c5f4c61b7c833bf241efdc8ca28');

require_once('dependencies/composer/autoload.php');
require_once('includes/database/database.php');
require_once('includes/order-management.php');
require_once('includes/vendors-management.php');
require_once('includes/customers-management.php');
require_once('includes/event-management.php');
require_once('includes/item-management.php');
require_once('includes/arrangement-management.php');
require_once('includes/profile-management.php');
require_once('includes/security.php');
require_once('includes/new-users.php');
require_once('includes/migrations/migrations.php');
require_once('includes/migrations/cli.php');
require_once('includes/reports/init.php');
require_once('includes/bb-modules/stemcounter-bb-modules.php');
require_once('includes/quickbooks/init.php');
require_once('includes/stripe-payments/init.php');
require_once('includes/notification-management.php');
require_once('includes/bb-modules/stemcounter-bb-modules.php');
require_once('includes/recipe-management.php');
require_once('includes/studio-management.php');
require_once('includes/shortcode-stemcounter-registration.php');
require_once('includes/report-management.php');

require_once('includes/docraptor/autoload.php');
require_once('includes/google/init.php');
require_once('includes/quickbooks-online/init.php');

use Illuminate\Http\Request;
use Stemcounter\Ajax_Response;

add_theme_support('post-thumbnails');
add_image_size('blog_thumb', 650, 380, true);
add_image_size('logo_small', 160, 90, false);
add_image_size('logo_medium', 320, 180, false);
add_image_size('logo_big', 640, 360, false);

//detailed pdf image sizes
add_image_size('d_pdf_big', 204, 204, true);
add_image_size('d_pdf_small', 102, 102, true);
add_image_size('d_pdf_wide', 204, 102, true);
add_image_size('d_pdf_narrow', 102, 204, true);

if ( defined( 'MIXPANEL_TOKEN' ) && MIXPANEL_TOKEN ) {
	add_action( 'wp_head', 'sc_render_mixpanel_js', 9999 );
}

if ( defined( 'DRIFT_TOKEN' ) && DRIFT_TOKEN ) {
	add_action( 'wp_head', 'sc_render_drift_js', 9999 );
}

if ( defined( 'FB_PIXEL_ID' ) && FB_PIXEL_ID ) {
	add_action( 'wp_head', 'sc_render_facebook_pixel_js', 9999 );
}

//THESE ARE THE THEME SCRIPTS. WE'RE GOING TO WANT TO ADJUST THESE TO ENQUEUE THE PIECES AS NEEDED PER PAGE.
function theme_scripts() {
	global $wp_scripts;

	$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

	wp_register_style( 'proposal-style-2', get_bloginfo('template_directory') . '/css/proposal-style-2.css', array( 'theme-styles', 'font-arimo' ), 'v1.0.3' );
	wp_register_style( 'pt-sans-caption-font-face', get_bloginfo( 'template_directory' ) . '/css/pt-sans-caption-fonts.css', array(), 'v1.0.0' );
	wp_register_style( 'font-arimo', '//fonts.googleapis.com/css?family=Arimo:400,700&amp;subset=latin-ext' );
	wp_add_inline_style( 'font-arimo', sc_get_google_font_inline_style( 'Arimo, sans-serif' ) );
	wp_register_style( 'font-cormorant-garamond', '//fonts.googleapis.com/css?family=Cormorant+Garamond:400,600,700&amp;subset=latin-ext' );
	wp_add_inline_style( 'font-cormorant-garamond', sc_get_google_font_inline_style( 'Cormorant Garamond, sans-serif' ) );
	wp_register_style( 'font-patua-one', '//fonts.googleapis.com/css?family=Patua+One' );
	wp_add_inline_style( 'font-patua-one', sc_get_google_font_inline_style( 'Patua One, sans-serif' ) );
	wp_register_style( 'font-courgette', '//fonts.googleapis.com/css?family=Courgette&amp;subset=latin-ext' );
	wp_add_inline_style( 'font-courgette', sc_get_google_font_inline_style( 'Courgette, sans-serif' ) );

	wp_register_style( 'to-do', get_bloginfo( 'template_directory' ) . '/css/to-do.css', false, 'v1.0.0' );
	wp_register_style( 'sc-responsive', get_bloginfo( 'template_directory' ) . '/css/style-responsive.css', false, 'v1.0.0' );

	// Styles
	if (!is_front_page()) {
		wp_enqueue_style( 'dashicons' );
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
		wp_enqueue_style('fontawesome', get_template_directory_uri() . '/dependencies/bower/fontawesome/css/font-awesome.css');
		wp_enqueue_style( 'quill-css', get_template_directory_uri() . '/node_modules/react-quill/dist/quill.snow.css', array(), 'v1.2.6' );
		wp_enqueue_style('select2', get_template_directory_uri() . '/dependencies/bower/select2/dist/css/select2.min.css');
		wp_enqueue_style('ruda-font', "//fonts.googleapis.com/css?family=Ruda:400,700,900");
		wp_enqueue_style('open-sans-condensed', "//fonts.googleapis.com/css?family=Open+Sans+Condensed:300normal,700normal");
		wp_enqueue_style('source-sans-pro', "//fonts.googleapis.com/css?family=Source+Sans+Pro:regular,bold");
		wp_enqueue_style('query-ui-css', get_template_directory_uri() . '/dependencies/bower/jquery-ui/themes/smoothness/jquery-ui.min.css');
		wp_enqueue_style('datepicker-css', get_template_directory_uri() . '/css/jquery.fullPage.css', array('jquery'));
		wp_enqueue_style('qtip2', get_template_directory_uri() . '/dependencies/bower/qtip2/jquery.qtip.min.css');
		wp_enqueue_style('alertify', get_template_directory_uri() . '/dependencies/bower/alertify-js/build/css/alertify.min.css');
		wp_enqueue_style( 'pt-sans-caption-font-face' );

		if ( is_page_template( 'template-proposal-2.0.php' )  ) {
			wp_enqueue_style( 'great-vibes-font', 'https://fonts.googleapis.com/css?family=Great+Vibes&subset=latin-ext', array(), '' );
			wp_enqueue_style( 'pt-sans-caption-font-face' );
			wp_enqueue_style( 'theme-styles', get_bloginfo('stylesheet_url'), array( 'sc-responsive' ), '1.8.6' );
			wp_enqueue_style( 'proposal-print', get_template_directory_uri() . '/css/proposal-print.css', array(), '1.1.8', 'print' );
			wp_enqueue_style( 'react-crop-css', get_template_directory_uri() . '/css/ReactCrop.css', array(), '1.0.0' );
		} else if ( is_page_template( 'template-recipe-sheet.php' ) ) {
			wp_enqueue_style( 'theme-styles', get_bloginfo('stylesheet_url'), array( 'sc-responsive' ), '1.8.6' );
		} else {
			wp_enqueue_style( 'theme-styles', get_bloginfo('stylesheet_url'), array( 'sc-responsive' ), '1.8.6', 'screen' );
		}

		wp_enqueue_style('theme-styles-print', get_template_directory_uri() . '/print.css', array(), 'v1.1.6', 'print');

		wp_enqueue_style('theme-styles-responsive', get_template_directory_uri() . '/css/style-responsive.css', array(), 'v1.0.9', 'screen');

		wp_enqueue_style('photoswipe-css', get_template_directory_uri() . '/css/photoswipe/photoswipe.css' );

		wp_enqueue_style('photoswipe-skin', get_template_directory_uri() . '/css/photoswipe/default-skin/default-skin.css' );
	}

	// Scripts
	//TODO FIGURE OUT HOW TO GET THESE SET UP IN THE FOOTER
	wp_deregister_script('jquery'); // Deregister WordPress jQuery
	wp_register_script('jquery', get_template_directory_uri() . '/dependencies/bower/jquery/dist/jquery' . $min . '.js', array(), 'v1.11.3');


	// Register scripts - we will enqueue based on the current page further down

	wp_register_script('jquery-ui', get_template_directory_uri() . '/dependencies/bower/jquery-ui/jquery-ui.min.js', array('jquery'), false, true);
	wp_add_inline_script( 'jquery-ui', 'jQuery.fn.tooltip = jQuery.fn.bstooltip || jQuery.fn.tooltip;', 'after' );
	wp_register_script('bootstrap', get_template_directory_uri() . '/dependencies/bower/bootstrap/dist/js/bootstrap.min.js', array('jquery'), false, true);
	wp_add_inline_script( 'bootstrap', 'jQuery.fn.bstooltip = jQuery.fn.tooltip;', 'after' );
	wp_register_script('lodash', get_template_directory_uri() . '/dependencies/bower/lodash/lodash.min.js', array('jquery', 'underscore'), false, true);
	// wp_enqueue_script('select2', get_template_directory_uri() . '/dependencies/bower/select2/dist/js/select2.full.min.js');
	wp_register_script('select2-custom', get_template_directory_uri() . '/dependencies/bower/select2/dist/js/select2.full.min.js', false, true);

	wp_register_script( 'react', get_template_directory_uri() . '/node_modules/react/dist/react-with-addons' . $min . '.js', array(), 'v15.5.4', true );
	wp_register_script( 'react-prop-types', get_template_directory_uri() . '/node_modules/prop-types/prop-types' . $min . '.js', array( 'react' ), 'v15.5.7', true );
	wp_register_script( 'react-dom', get_template_directory_uri() . '/node_modules/react-dom/dist/react-dom' . $min . '.js', array( 'react', 'react-dom-server', 'react-prop-types' ), 'v15.5.4', true );
	wp_register_script( 'react-dom-server', get_template_directory_uri() . '/node_modules/react-dom/dist/react-dom-server' . $min . '.js', array( 'react' ), 'v15.5.4', true );


	wp_register_script('datatables', get_template_directory_uri() . '/js/jquery.dataTables.js', array('jquery'), 'v1.9.4', true);
	wp_register_script('datatables-bootstrap', get_template_directory_uri() . '/js/dataTables.bootstrap.js', array('jquery', 'datatables', 'bootstrap'), 'v1.0.6', true);

	wp_add_inline_script( 'datatables-bootstrap', '/* Set the defaults for DataTables initialisation */
$.extend( true, $.fn.dataTable.defaults, {
    "sDom":
        "<\'row\'<\'col-sm-12 col-md-6\'l><\'col-sm-12 col-md-6\'f>r>"+
        "t"+
        "<\'row\'<\'col-sm-12 col-md-6\'i><\'col-sm-12 col-md-6\'p>>"
} );' );

	wp_register_script('isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', '2.2.0', array('jquery'), false, true);

	wp_register_script('accordion', get_template_directory_uri() . '/js/jquery.dcjqaccordion.2.7.js', array('jquery'), false, true);

	wp_register_script('scrollto', get_template_directory_uri() . '/js/jquery.scrollTo.min.js', array('jquery'), false, true);

	wp_register_script('qtip2', get_template_directory_uri() . '/dependencies/bower/qtip2/jquery.qtip.min.js', array('jquery'), false, true);

	wp_register_script('alertify', get_template_directory_uri() . '/dependencies/bower/alertify-js/build/alertify.min.js', array('jquery'), false, true);


	wp_register_script('jquery-form', get_template_directory_uri() . '/dependencies/bower/jquery-form/jquery.form.js', array('jquery'), false, true);

	wp_register_script('blockUI', get_template_directory_uri() . '/dependencies/bower/blockUI/jquery.blockUI.js', array('jquery'), false, true);

	// Date pickers
	wp_register_script('moment', get_template_directory_uri() . '/js/moment.js', array('jquery'), false, true);
	wp_register_script('datepicker', get_template_directory_uri() . '/js/bootstrap-datepicker.js', array('jquery', 'moment', 'bootstrap'));

	// Custom Tagsinput
	wp_register_script('tagsinput', get_template_directory_uri() . '/js/jquery.tagsinput.js', array('jquery'));
	wp_register_script('inputmask', get_template_directory_uri() . '/js/bootstrap-inputmask/bootstrap-inputmask.min.js', array('jquery'));
	wp_register_script('formcomponent', get_template_directory_uri() . '/js/form-component.js', array('jquery', 'tagsinput', 'inputmask', 'datepicker', 'jquery-ui'));

	wp_register_script('modernizr', get_template_directory_uri() . '/js/vendor/custom.modernizr.js', array('jquery'));

	wp_register_script( 'theme-scripts', get_template_directory_uri() . '/js/common-scripts.js', array('jquery'), 'v1.0.6', true );

	// STEMCOUNTER FILES
	wp_register_script('theme-plupload', get_template_directory_uri() . '/js/plupload/plupload.full.min.js', 'v1.3.4', array('jquery'), false, true);
	wp_register_script( 'theme-generic', get_template_directory_uri() . '/js/theme-generic.js', array( 'jquery', 'qtip2', 'blockUI', 'jquery-ui', 'media-views' ), 'v1.1.3', true );
	wp_register_script( 'theme-ajax', get_template_directory_uri() . '/js/theme-ajax.js', array( 'jquery', 'alertify' ), 'v.1.0.1', true );
	wp_register_script( 'theme-react-components', get_template_directory_uri() . '/react/build/theme-react-components.js', array( 'jquery', 'react-dom', 'theme-ajax', 'theme-generic', 'react-virtualized-select' ), 'v2.2.4', true );

	wp_register_script('theme-react-edit-event-form', get_template_directory_uri() . '/react/build/edit-event-form.js', array('jquery', 'react-dom', 'jquery-ui'), 'v1.1.11', true);
	wp_register_script('theme-react-tax-rates', get_template_directory_uri() . '/react/build/tax-rates-form.js', array('jquery', 'react-dom'), 'v1.0.4', true);

	wp_register_script('theme-react-discounts', get_template_directory_uri() . '/react/build/discounts_form.js', array('jquery', 'react-dom'), 'v1.0.3', true);

	wp_register_script('theme-studio-settings-form', get_template_directory_uri() . '/react/build/studio-settings-form.js', array('jquery', 'react'), 'v1.0.7', true);
	wp_register_script('theme-profile-tags-form', get_template_directory_uri() . '/react/build/profile-tags-form.js', array('jquery', 'react'), 'v1.0.3', true);
	wp_register_script('theme-react-deliveries', get_template_directory_uri() . '/react/build/deliveries_form.js', array('jquery', 'react-dom'), 'v1.0.4', true);

	wp_register_script('theme-react-onboarding', get_template_directory_uri() . '/react/build/onboarding.js', array('jquery', 'react-dom', 'jquery-ui'), 'v1.1.7', true);

	wp_register_script('theme-react-edit-order-form', get_template_directory_uri() . '/react/build/edit-order-form.js', array('jquery', 'react-dom', 'jquery-ui'), 'v1.0.3', true);

	wp_register_script('theme-react-single-order', get_template_directory_uri() . '/react/build/single-order.js', array('jquery', 'react-dom', 'theme-react-components', 'jquery-ui', 'underscore', 'theme-plupload'), 'v1.0.14', true);

	wp_register_script('theme-react-edit-vendor-form', get_template_directory_uri() . '/react/build/edit-vendor-form.js', array('jquery', 'react-dom'), 'v1.0.10', true);

	wp_register_script( 'theme-react-edit-customer-form', get_template_directory_uri() . '/react/build/edit-customer-form.js', array('jquery', 'react-dom', 'theme-react-forms' ), 'v1.1.1', true);

	wp_register_script('react-prop-types', get_template_directory_uri() . '/js/prop-types.min.js', array('jquery', 'react-dom'), 'v1.0.0', true);

	wp_register_script('theme-react-quill', get_template_directory_uri() . '/node_modules/react-quill/dist/react-quill' . $min . '.js', array('jquery', 'react-dom'), 'v1.0.1', true);

	wp_register_script( 'theme-react-edit-arrangement-form', get_template_directory_uri() . '/react/build/edit-arrangement-form.js', array( 'jquery', 'react-dom', 'lodash' ), 'v1.3.14', true );

	wp_register_script('theme-react-edit-item-form', get_template_directory_uri() . '/react/build/edit-item-form.js', array('jquery', 'react-dom'), 'v3.1.7', true);

	wp_register_script('theme-react-edit-profile-password', get_template_directory_uri() . '/react/build/edit-profile-password.js', array('jquery', 'react-dom'), 'v1.0.4', true);

	wp_register_script( 'theme-react-profile-settings-form', get_template_directory_uri() . '/react/build/profile-settings-form.js', array( 'jquery', 'react-dom', 'theme-plupload' ), 'v1.0.2', true );

	wp_register_script('theme-react-notifications-form', get_template_directory_uri() . '/react/build/notifications-form.js', array('jquery', 'react-dom'), 'v1.0.2', true);

	wp_register_script( 'photoswipe-js', get_template_directory_uri() . '/js/photoswipe-ui/photoswipe.js', array(), 'v4.4.1', true );

	wp_register_script( 'photoswipe-ui', get_template_directory_uri() . '/js/photoswipe-ui/photoswipe-ui-default.js', array(), 'v4.4.1', true );

	wp_register_script( 'react-crop-image', get_template_directory_uri() . '/react/build/react-crop.js', array(), 'v1.0.1', true );

	wp_register_script( 'edit-recipe-form', get_template_directory_uri() . '/react/build/edit-recipe-form.js', array('jquery', 'react-dom', 'theme-react-edit-item-form', 'photoswipe-js', 'photoswipe-ui'), 'v1.0.5', true );

	wp_register_script('theme-react-proposal', get_template_directory_uri() . '/react/build/proposal.js', array('jquery', 'react-dom', 'theme-react-edit-arrangement-form', 'theme-react-edit-customer-form', 'theme-react-edit-vendor-form', 'theme-react-edit-item-form', 'theme-react-quill', 'photoswipe-js', 'photoswipe-ui', 'react-crop-image', 'theme-react-forms' ), 'v2.7.19', true);

	wp_register_script( 'theme-react-standalone-recipes', get_template_directory_uri() . '/react/build/standalone-recipes.js', array( 'jquery', 'react-dom', 'theme-react-edit-arrangement-form', 'theme-react-edit-item-form', 'photoswipe-js', 'photoswipe-ui', 'react-crop-image' ), 'v1.0.9', true );

	wp_register_script( 'react-virtualized-select', get_template_directory_uri() . '/js/react-virtualized-select.js', array( 'jquery', 'react-dom' ), 'v2.4.0', true );

	wp_register_script( 'theme-react-report', get_template_directory_uri() . '/react/build/report-tab.js', array( 'jquery', 'react-dom' ), 'v1.0.4', true );

	wp_register_script( 'theme-react-forms', get_template_directory_uri() . '/react/build/forms-tab.js', array( 'jquery', 'react-dom', 'theme-react-quill' ), 'v1.1.0', true );

	wp_register_script( 'view-form-submissions', get_template_directory_uri() . '/react/build/view-submissions.js', array( 'jquery', 'react-dom', 'theme-react-components' ), 'v1.0.3', true );

	wp_register_script( 'profile-email-templates', get_template_directory_uri() . '/react/build/profile-email-templates.js', array( 'jquery', 'react-dom', 'theme-react-components', 'theme-react-quill' ), 'v1.0.1', true );

	wp_enqueue_script( 'modernizr' );
	wp_enqueue_script( 'react-prop-types' );

	wp_enqueue_script( 'theme-scripts' );

	if ( ! is_front_page() ) {
		wp_enqueue_script( 'bootstrap' );
		wp_enqueue_script( 'lodash' );
		wp_enqueue_script( 'accordion' );

		wp_enqueue_script( 'select2-custom' );

		wp_enqueue_script( 'datepicker' );

		wp_enqueue_script( 'formcomponent' );

		// STEMCOUNTER FILES
		wp_enqueue_script( 'theme-generic' );
		wp_enqueue_script( 'theme-ajax' );
		wp_enqueue_script( 'theme-react-components' );
	}

	if ( is_page_template( 'template-events.php' ) || is_page_template( 'template-archive.php' ) ) {
		wp_enqueue_script('theme-react-edit-vendor-form');
		wp_enqueue_script('theme-react-edit-customer-form');
		wp_enqueue_script( 'theme-react-edit-event-form' );
		wp_enqueue_script( 'theme-react-tax-rates' );
		wp_enqueue_script( 'datatables-bootstrap' );
	} elseif ( is_page_template( 'template-stemcounter.php' ) ) {
		wp_enqueue_media();
		wp_enqueue_script( 'theme-react-edit-arrangement-form' );
		wp_enqueue_script( 'theme-react-edit-item-form' );
		wp_enqueue_script( 'isotope' );
	} elseif ( is_page_template( 'template-details.php' ) ) {
		wp_enqueue_script('theme-react-edit-vendor-form');
		wp_enqueue_script( 'theme-react-edit-event-form' );
		wp_enqueue_script('theme-react-edit-customer-form');
		wp_enqueue_script( 'isotope' );
	} elseif ( is_page_template( 'template-profile.php' ) ) {
		wp_enqueue_script( 'theme-react-tax-rates' );
		wp_enqueue_script( 'theme-react-profile-settings-form' );
		wp_enqueue_script( 'theme-react-notifications-form' );
		wp_enqueue_script( 'profile-email-templates' );
	} elseif ( is_page_template( 'template-services.php' ) || is_page_template( 'template-flowers.php' ) ) {
		wp_enqueue_script( 'datatables-bootstrap' );
		wp_enqueue_script( 'theme-react-edit-item-form' );
		wp_enqueue_script( 'photoswipe-ui');
		wp_enqueue_script( 'photoswipe-js');
		wp_enqueue_media();
	} elseif (is_page_template('template-order.php')) {
		wp_enqueue_script( 'datatables-bootstrap' );
		wp_enqueue_script('theme-react-edit-order-form');
	} elseif (is_page_template('template-vendors.php')) {
		wp_enqueue_script( 'datatables-bootstrap' );
		wp_enqueue_script('theme-react-edit-vendor-form');
	} elseif (is_page_template('template-customers.php')) {
		wp_enqueue_script( 'datatables-bootstrap' );
		wp_enqueue_script('theme-react-edit-customer-form');
	} elseif (is_page_template('template-proposal-2.0.php')  ) {
		wp_enqueue_media();
		wp_dequeue_style( 'ruda-font' );
		wp_enqueue_script( 'theme-react-proposal' );
	} elseif (is_page_template('template-onboarding.php')) {
		wp_enqueue_script( 'theme-plupload' );
		wp_enqueue_script( 'theme-react-edit-event-form' );
		wp_enqueue_script('theme-react-edit-vendor-form');
		wp_enqueue_script('theme-react-edit-customer-form');
		wp_enqueue_script('theme-react-onboarding');
	} elseif ( is_page_template( 'template-recipes.php' ) ) {
		wp_enqueue_media();
		wp_dequeue_style( 'ruda-font' );
		wp_enqueue_script( 'datatables-bootstrap' );
		wp_enqueue_script( 'theme-react-standalone-recipes' );
	} elseif ( is_page_template( 'template-studio.php' ) ) {
		wp_dequeue_style( 'ruda-font' );
		wp_enqueue_script( 'theme-studio-settings-form' );
		wp_enqueue_script( 'theme-profile-tags-form' );
		wp_enqueue_script( 'theme-react-report' );
	} elseif ( is_page_template( 'template-forms.php' ) ) {
		wp_dequeue_style( 'ruda-font' );
		wp_enqueue_script( 'theme-react-forms' );
		wp_enqueue_script( 'view-form-submissions' );
	}

	if ( get_query_var( 'form-uid' ) ) {
		wp_enqueue_script( 'theme-react-forms' );
	}

}
add_action('wp_enqueue_scripts', 'theme_scripts');

function sc_proposal_docratop_scriptfix() {
	if ( is_page_template( 'template-proposal-2.0.php' ) ) {
		?>
		<script>
		if (!Function.prototype.bind) {
			Function.prototype.bind = function(oThis) {
				if (typeof this !== 'function') {
					// closest thing possible to the ECMAScript 5
					// internal IsCallable function
					throw new TypeError('Function.prototype.bind - what is trying to be bound is notcallable');
				}
				var aArgs = Array.prototype.slice.call(arguments, 1),
				fToBind = this,
				fNOP = function() {},
				fBound = function() {
					return fToBind.apply(this instanceof fNOP ? this : oThis,
					aArgs.concat(Array.prototype.slice.call(arguments)));
				};
				if (this.prototype) {
					// Function.prototype doesn't have a prototype property
					fNOP.prototype = this.prototype;
				}
				fBound.prototype = new fNOP();
				return fBound;
			};
		}
 	</script>
	<?php
	}
}
add_action( 'wp_head', 'sc_proposal_docratop_scriptfix', 10 );

// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

add_filter('show_admin_bar', '__return_false', 9000);

function allow_user_uploads($allcaps, $cap, $args) {

	if (is_user_logged_in()) {
		$allcaps['upload_files'] = true;
		$allcaps['delete_posts'] = true;

		// Allow users to edit their attachments
		if ( 'edit_post' == $args[0] ) {
			if ( 'attachment' == get_post_type( $args[2] ) ) {
				$post = get_post( $args[2] );
				if ( get_current_user_id() == $post->post_author ) {
					$allcaps[ $cap[0] ] = true;
				}
			} else {
				$allcaps[$cap[0]] = true;
			}
		}
	}

	return $allcaps;
}
add_filter('user_has_cap', 'allow_user_uploads', 0, 3);

function show_current_user_attachments($query) {
	/*if ( ! is_super_admin() ) {
		$query['author'] = intval( get_current_user_id() );
	}*/

	// return $query;

    $user_id = intval( get_current_user_id() );
    if ( $user_id && (
		( ! empty( $_REQUEST['query']['author'] ) && intval( $_REQUEST['query']['author'] ) == $user_id ) ||
		( ! empty( $_REQUEST['query']['post_author'] ) && intval( $_REQUEST['query']['post_author'] ) == $user_id )
	) ) {
        $query['author'] = $user_id;
    }
    return $query;
}
add_filter( 'ajax_query_attachments_args', 'show_current_user_attachments' );


/*Remove Slashes Function*/
function stripslashes_full($input) {
	if (is_array($input)) {
		$input = array_map('stripslashes_full', $input);
	} elseif (is_object($input)) {
		$vars = get_object_vars($input);
		foreach ($vars as $k=>$v) {
			$input->{$k} = stripslashes_full($v);
		}
	} else {
		$input = stripslashes($input);
	}
	return $input;
}

// Cleans $_GET $_POST and $_REQUEST from slashes
function sc_clean_input_slashes() {
	static $called = false;
	if ($called) {
		return false;
	}
	$called = true;

	$_GET = stripslashes_deep($_GET);
	$_POST = stripslashes_deep($_POST);
	$_REQUEST = stripslashes_deep($_REQUEST);

	return true;
}

function check_wp_login(){
	// First check the nonce, if it fails the function will break
	//check_ajax_referer('ajax-login-nonce', 'security');
	// Nonce is checked, get the POST data and sign user on
	$info = array();
	$info['user_login'] = $_POST['user'];
	$info['user_password'] = $_POST['pass'];
	$info['remember'] = true;
	$user_signon = wp_signon( $info );
	if(is_wp_error($user_signon)){
		//json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
		echo "0";
		die;
	}
	echo "1";
	die();
}
add_action('wp_ajax_check_wp_login', 'check_wp_login');
add_action('wp_ajax_nopriv_check_wp_login', 'check_wp_login');

// Custom logo on login form
function custom_login_logo() {
	echo '
	<style type="text/css">
		h1 a { background-image:url('. get_bloginfo( 'template_directory' ) .'/img/Logo_StemCounter.com_.png) !important; }
	</style>
	';
}
add_action('login_head', 'custom_login_logo');

function sc_prevent_users_to_see_dashboard() {
	if (is_admin() &&
		!is_super_admin() &&
		!(defined('DOING_AJAX') && DOING_AJAX)) {

		wp_redirect(home_url());
		exit;
	}
}
add_action('admin_init', 'sc_prevent_users_to_see_dashboard');

/*
* Adding the option for the theme to have a menu
*/
function register_my_menus() {
  register_nav_menus(
	array(
		'event-menu' => __( 'Event Menu', 'stemcounter' ),
		'main-menu' => __( 'Main Menu', 'stemcounter' ),
		'blog-header' => __( 'Blog Header', 'stemcounter' ),
		'home-header' => __( 'Home Header', 'stemcounter' ),
		'pricing-header' => __( 'Pricing Header', 'stemcounter' ),
	)
  );
}
add_action( 'init', 'register_my_menus' );

/**
 * Redirects visitors to `wp-login.php?action=register` to
 * `site.com/register`
 */
function catch_register() {
	wp_redirect(home_url('register/'));
	exit(); // always call `exit()` after `wp_redirect`
}
add_action( 'login_form_register', 'catch_register' );

// Adding custom html to the menu
function menu_filter( $items, $args ) {
	if ($args->theme_location == 'event-menu') {

		$eid = isset($_GET['eid']) ? $_GET['eid'] : '';
		$out = array();
		foreach ( $items as $item ) {

			if ($item->url !== '/events') {
				if ( isset($item->url) ) {
					$item->url = add_query_arg( 'eid', $eid, $item->url );
				}

				$out[] = $item;
			}
		}
	}
	return $items;
}
add_filter( 'wp_nav_menu_objects', 'menu_filter', 10, 2 );

// Creating a call for unused invites codes
function sc_get_unused_viral_codes($user_id) {
	global $wpdb;
	$table_name = $wpdb->prefix . "viralinvites";
	$query = $wpdb->prepare('
		SELECT `invite_code`
		FROM `' . $table_name . '`
		WHERE `user` = %d
		AND `used` = 0
	', $user_id);
	$codes = $wpdb->get_col($query);
	isset($codes) ? $codes : $codes = 'Nice! All of your florist friends have used up your invites. Keep an eye open for more.';
	return $codes;
}

function sc_block($template, $args=array()) {
	$template = locate_template('blocks/' . $template . '.php');
	if ($template) {
		extract($args);
		include($template);
	}
}

function sc_excerpt_length($length) {
	return 90;
}
add_filter('excerpt_length', 'sc_excerpt_length');

function sc_excerpt_more($more) {
	return ' ...';
}
add_filter('excerpt_more', 'sc_excerpt_more');

function get_company_info( $user_ID = false ) {
	$user_ID			=	$user_ID ? $user_ID : get_current_user_id();
	$user_meta			=	sc_get_user_meta( $user_ID );

	//Vendor Details
	$companyArray = array(
		'company'	=>	isset( $user_meta['company_name'] ) ? $user_meta['company_name'] : '',
		'address'	=>	isset( $user_meta['company_address'] ) ? $user_meta['company_address'] : '',
		'website'	=>	isset( $user_meta['company_website'] ) ? $user_meta['company_website'] : '',
		// 'color'		=>	isset( $user_meta['proposal_color'] ) ? $user_meta['proposal_color'] : ''
	);

	return $companyArray;
}
add_action('wp_enqueue_script','get_company_info');

function sc_format_quantity( $quantity ) {
	$quantity = number_format( $quantity, 2, '.', '' );

	return preg_replace( '~\.00$~', '', $quantity );
}

function sc_format_price( $price, $add_currency = false, $user_id = false ) {
	$formatted_price = number_format($price, 2, '.', '');

	if ( $add_currency ) {
		$user_id = $user_id ? $user_id : get_current_user_id();
		$symbol = sc_get_pref_currency_symbol( $user_id );
		$formatted_price = $symbol . number_format( $formatted_price, 2, '.', ',' );
	}

	return $formatted_price;
}

function sc_get_pref_currency_symbol( $user_id ) {
	$pref_currencies = array(
		'AED' => '&#x62f;&#x2e;&#x625;',
		'AUD' => '&#36;',
		'CAD' => '&#36;',
		'EUR' => '&#8364;',
		'GBP' => '&#163;',
		'USD' => '&#36;',
		'ZAR' => '&#82;',
	);

	$user_currency = sc_get_user_saved_pref_currency( $user_id );
	return $pref_currencies[ $user_currency ];
}

function sc_get_user_meta($user_id) {
	$user_meta = array();
	$user_meta['company_website'] = get_user_meta(
		$user_id,
		'company_website',
		true
	);

	$user_meta['company_name'] = get_user_meta(
		$user_id,
		'company_name',
		true
	);

	$user_meta['company_address'] = get_user_meta(
		$user_id,
		'company_address',
		true
	);

	// $user_meta['proposal_color'] = get_user_meta(
	// 	$user_id,
	// 	'proposal_color',
	// 	true
	// );

	$user_meta['hardgood_multiple'] = get_user_meta(
		$user_id,
		'hardgood_multiple',
		true
	);

	$user_meta['fresh_flower_multiple'] = get_user_meta(
		$user_id,
		'fresh_flower_multiple',
		true
	);

	$user_meta['__charge_card_rate'] = get_user_meta(
		$user_id,
		'__charge_card_rate',
		true
	);

	$user_meta['__delivery_cost'] = get_user_meta(
		$user_id,
		'__delivery_cost',
		true
	);

	$user_meta['__labor'] = get_user_meta(
		$user_id,
		'__labor',
		true
	);

	$user_meta['__date_format'] = get_user_meta(
		$user_id,
		'__date_format',
		true
	);

	$user_meta['__pref_currency'] = get_user_meta(
		$user_id,
		'__pref_currency',
		true
	);

	return $user_meta;
}

function sc_get_profile_settings() {
	$user_id = get_current_user_id();
	$user_meta = sc_get_user_meta($user_id);

	// Defining the invoice factors. If they are empty, defining them as standard. This protection eventually needs to be moved to the profile page.
	$hardgood_multiple = empty($user_meta['hardgood_multiple']) ? 1 : $user_meta['hardgood_multiple'];

	$flower_multiple = empty($user_meta['fresh_flower_multiple']) ? 1 : $user_meta['fresh_flower_multiple'];

	$card_rate = empty($user_meta['__charge_card_rate']) ? 0 : $user_meta['__charge_card_rate'];

	$delivery_cost = empty($user_meta['__delivery_cost']) ? 0 : $user_meta['__delivery_cost'];

	$date_format = empty($user_meta['__date_format']) ? 'm/d/Y' : $user_meta['__date_format'];

	$pref_currency = empty($user_meta['__pref_currency']) ? 'USD' : $user_meta['__pref_currency'];

	$labor_cost = empty($user_meta['__labor']) ? 0 : $user_meta['__labor'];

	$add_labor_costs = get_user_meta($user_id, '_sc_add_labor_costs', true);
	$add_labor_costs = (bool)($add_labor_costs == 'y');
	$apply_labor_to = get_user_meta($user_id, '_sc_apply_labor_to', false);

	$pdf_sheet_size = get_user_meta( $user_id, '_sc_pdf_settings', true );

	$prices_separator = get_user_meta( get_current_user_id(), '_sc_prices_separator', true );

	$settings = array (
		'hardgood'			=>	floatval($hardgood_multiple),
		'flower'			=>	floatval($flower_multiple),
		'card'				=>	floatval($card_rate),
		'delivery'			=>	floatval($delivery_cost),
		'date_format'		=>  $date_format,
		'pref_currency'	    =>  $pref_currency,
		'labor'				=>	floatval($labor_cost),
		'add_labor_costs'	=>	$add_labor_costs,
		'apply_labor_to'	=>	$apply_labor_to,
		'pdf_sheet_size'	=> 	! empty( $pdf_sheet_size ) ? $pdf_sheet_size : 'US-Letter',
		'prices_separator'		=> ! empty( $prices_separator ) ? $prices_separator : 'comma_dot',
	);

	return $settings;
}

function sc_user_onboarding_redirect() {
	$user_id = get_current_user_id();
	$onboarding_tutorial = get_user_meta($user_id, 'onboarding_tutorial', true);
	$taken_tutorial = empty($onboarding_tutorial) ? false : true;
	$user = wp_get_current_user();

	if ( is_user_logged_in() && is_singular( 'memberpressproduct' ) ) {
		$prd = new MeprProduct( get_the_ID() );
		if ( ! $prd->can_you_buy_me() ) {
			wp_redirect( sc_get_events_page_url() );
			exit();
		}
	}

	if ( is_array( $onboarding_tutorial ) ) {
		foreach ( $onboarding_tutorial as $tutorial ) {
			if ( $tutorial == false ) {
				$taken_tutorial = false;
				break;
			}
		}
	}

	if ( $taken_tutorial === true && is_page_template( 'template-onboarding.php' ) ) {
		wp_redirect( sc_get_events_page_url() );
        exit();
	}

    if ( is_user_logged_in() &&
    	$taken_tutorial == false &&
    	! is_page_template( 'template-onboarding.php' ) &&
    	! is_page( 'logon' ) &&
		! is_page( 'register' ) &&
		! is_page( 'terms' ) &&
		! is_page( 'faq' ) &&
		! is_home() &&
		! is_single() &&
		! is_front_page() &&
		! is_search() &&
		! is_404() &&
		! is_archive() &&
        ! is_page_template( 'template-pricing.php' ) &&
        ! is_page_template( 'template-membership.php' ) &&
        ! is_page_template( 'template-join.php' ) ) {

        wp_redirect( sc_get_onboarding_page_url() );
        exit();
    }
}
add_action( 'template_redirect', 'sc_user_onboarding_redirect' );

function sc_get_page_with_template($template) {
	$suffix = '.php';
	$template = substr($template, -strlen($suffix)) == $suffix ? $template : $template . $suffix;
	$pages = get_pages(array(
		'meta_key' => '_wp_page_template',
		'meta_value' => $template,
	));
	if (empty($pages)) {
		return false;
	}

	return $pages[0];
}

function sc_get_arrangements_page_url() {
	$page = sc_get_page_with_template('template-stemcounter.php');
	return get_permalink($page);
}

function sc_get_events_page_url() {
	$page = sc_get_page_with_template('template-events.php');
	return get_permalink($page);
}

function sc_get_onboarding_page_url() {
	$page = sc_get_page_with_template('template-onboarding.php');
	return get_permalink($page);
}

function sc_get_details_page_url() {
	$page = sc_get_page_with_template('template-details.php');
	return get_permalink($page);
}

function sc_get_proposal_page_url() {
	$page = sc_get_page_with_template('template-proposal.php');
	return get_permalink($page);
}

function sc_get_proposal_2_0_page_url() {
	$page = sc_get_page_with_template('template-proposal-2.0.php');
	return get_permalink($page);
}

function sc_get_shopping_page_url() {
	$page = sc_get_page_with_template('template-shopping.php');
	return get_permalink($page);
}

function sc_get_events_archive_page_url() {
	$page = sc_get_page_with_template('template-archive.php');
	return get_permalink($page);
}

function sc_get_profile_page_url() {
	$page = sc_get_page_with_template('template-profile.php');
	return get_permalink($page);
}

function sc_stripe_template_redirect() {
    if (isset($_GET['charge'])) {

		require_once('dependencies/composer/stripe/stripe-php/init.php');

		$s2member_options = get_option('ws_plugin__s2member_options');
		$stripe_sk = $s2member_options['pro_stripe_api_secret_key'];
		$page_id = (int)$_POST['page_id'];
		$amount = (int)$_POST['amount'];
		$token = $_POST['stripeToken'];

		\Stripe\Stripe::setApiKey($stripe_sk);

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
		  $charge = \Stripe\Charge::create(array(
		    "amount" => $amount, // amount in cents, again
		    "currency" => "usd",
		    "source" => $token,
		    "description" => "StemCounter charge"
		    ));

		  	if (!empty($page_id)) {
		  		wp_redirect(get_permalink($page_id));
		  		exit;
		  	} else {
		  		wp_redirect(home_url('/'));
		  		exit;
		  	}

		} catch(\Stripe\Error\Card $e) {

			wp_redirect(home_url('/'));
			exit;
		}
    }
}
add_action( 'template_redirect', 'sc_stripe_template_redirect' );

function stripe_payment_form($atts) {
	$atts = shortcode_atts( array(
		'amount' => 5000,
		'label' => 'Pay With Card',
		'page-id' => ''
	), $atts );

	$s2member_options = get_option('ws_plugin__s2member_options');
	$stripe_pk = $s2member_options['pro_stripe_api_publishable_key'];
	ob_start(); ?>

	<form action="<?php echo home_url('/') . '?charge=1'; ?>" method="POST">

		<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"     data-key="<?php echo $stripe_pk; ?>"     data-image="<?php echo get_template_directory_uri() . '/img/favicon.png' ?>"     data-name="StemCounter.com"  data-amount="<?php echo $atts['amount']; ?>" data-label="<?php echo strtoupper($atts['label']); ?>"    data-locale="auto">
		</script>
		<input type="hidden" name="amount" value="<?php echo $atts['amount']; ?>">
		<input type="hidden" name="page_id" value="<?php echo $atts['page-id']; ?>">
	</form>


	<?php
	return ob_get_clean();
}
add_shortcode( 'stripe-form','stripe_payment_form' );

function sc_keep_pdf_image_sizes_on_server( $file_paths ) {
	$needed_files = array( '204x204', '102x102', '204x102', '102x204' );

	// Go through all paths and unset the ones that match the $needed_files
	foreach ( $file_paths as $i => $path ) {
		$found = false;
		foreach ( $needed_files as $needle ) {
			$found = $found || false !== stripos( $path, $needle );
		}

		if ( $found ) {
			unset( $file_paths[ $i ] );
		}
	}

	return $file_paths;
}
add_filter( 'as3cf_upload_attachment_local_files_to_remove', 'sc_keep_pdf_image_sizes_on_server', 10 );

function maybe_attempt_s3_transfer(){
	if ( ! isset( $_GET['upload_attachments_to_s3'] ) || ! is_super_admin() ) {
		return;
	}

	global $as3cf;
	set_time_limit( 0 );
	$attachments = get_posts( array( 'post_type' => 'attachment', 'post_mime_type' => 'image',  'numberposts' => -1, 'fields' => 'ids' ) );
	foreach ( $attachments as $attachm_id ) {
		var_dump( $as3cf->upload_attachment_to_s3( $attachm_id, null, null, true ) );
	}
	var_dump( $attachments, $as3cf );
	exit;
}
add_action( 'template_redirect', 'maybe_attempt_s3_transfer' );

function maybe_attempt_s3_download(){
	if ( ! isset( $_GET['download_attachments_from_s3'] ) || ! is_super_admin() ) {
		return;
	}

	global $as3cf;
	set_time_limit( 0 );

	$attachments = get_posts( array( 'post_type' => 'attachment', 'post_mime_type' => 'image',  'numberposts' => -1, 'fields' => 'ids', 'meta_query' => array( array( 'key' => 'amazonS3_info', 'value' => '', 'compare' => '!=' ) ) ) );

	$needed_files = array( '204x204', '102x102', '204x102', '102x204' );

	$downloads_count = 0;

	foreach ( $attachments as $attachm_id ) {
		$args = get_post_meta( $attachm_id, 'amazonS3_info', true );
		if ( ! isset( $args['acl'] ) ) {
			$args['acl'] = Amazon_S3_And_CloudFront::DEFAULT_ACL;
		}
		$s3client = $as3cf->get_s3client( $args['region'] );
		$prefix = str_ireplace( basename( $args['key'] ), '', $args['key'] );
		$paths_to_retrieve = array();
		$data = wp_get_attachment_metadata( $attachm_id );
		$file_paths = $as3cf->get_attachment_file_paths( $attachm_id, false, $data );
		foreach ( $file_paths as $i => $path ) {
			if ( file_exists( $path ) ) {
				unset( $file_paths[ $i ] );
			} else {
				$found = false;
				foreach ( $needed_files as $needle ) {
					$found = $found || false !== stripos( $path, $needle );
				}

				if ( ! $found ) {
					unset( $file_paths[ $i ] );
				}
			}
		}

		$_args = array(
			'Bucket' => $args['bucket'],
			'ACL' => $args['acl'],
		);

		foreach ( $file_paths as $path ) {
			// var_dump( $path );
			$file = basename( $path );
			$_args['Key'] = $prefix . $file;
			$_args['SaveAs'] = $path;

			try {
				$s3client->getObject( $_args );
				printf( '<p>Successfully downloaded the file <code>%s</code></p>', $path );

				$downloads_count ++;
				if ( 50 <= $downloads_count ) {
					break 2;
				}
			} catch (Exception $e) {
				printf( '<p style="color: red; font-weight: bold;">Error downloading the file <code>%s</code>. Error is: %s</p>', $path, $e->getMessage() );
			}
			// exit;
		}
		// var_dump( $prefix, $file_paths, $data );
		// var_dump( get_post_meta( $attachm_id, 'amazonS3_info', true ) );
	}

	if ( 50 > $downloads_count ) {
		echo '<h2>All Done!</h2>';
	} else {
		echo 'Going to next batch. <script>setTimeout(function(){window.location = ' . json_encode( add_query_arg( 'ts', microtime( true ) ) ) . ';}, 500);</script>';
	}

	// var_dump( $attachments, $as3cf );
	exit;
}
add_action( 'template_redirect', 'maybe_attempt_s3_download' );

function sc_migrate_user_meta() {
	if ( ! isset( $_GET['migrate_user_meta'] ) || ! is_super_admin() ) {
		return;
	}

	print_r('User Meta Migration START<br>==================================================<br><br>');

	$args = array(
		'orderby' => 'name',
		'order'	  => 'ASC'
	);

	$users = get_users($args);

	foreach ($users as $user) {
		print_r('<br/>==================================================<br/>');
		print_r('Migrating: ');
		print_r('Name: ' . $user->data->display_name . ', Username: <strong>' . $user->data->user_login . '</strong>');
		print_r('<br/>');

		$s2_user_meta = get_user_meta($user->data->ID, 'wp_s2member_custom_fields', true);

		if (!empty($s2_user_meta)) {
			$company_name = isset($s2_user_meta['company_name']) ? $s2_user_meta['company_name'] : '';
			$company_address = isset($s2_user_meta['company_address']) ? $s2_user_meta['company_address'] : '';
			$company_website = isset($s2_user_meta['company_website']) ? $s2_user_meta['company_website'] : '';
			$hardgood_multiple = isset($s2_user_meta['hardgood_multiple']) ? $s2_user_meta['hardgood_multiple'] : 1;
			$fresh_flower_multiple = isset($s2_user_meta['fresh_flower_multiple']) ? $s2_user_meta['fresh_flower_multiple'] : 1;
			$__charge_card_rate = isset($s2_user_meta['__charge_card_rate']) ? $s2_user_meta['__charge_card_rate'] : 0;
			$__delivery_cost = isset($s2_user_meta['__delivery_cost']) ? $s2_user_meta['__delivery_cost'] : 0;
			$__labor = isset($s2_user_meta['__labor']) ? $s2_user_meta['__labor'] : 0;
			$__date_format = isset($s2_user_meta['__date_format']) ? $s2_user_meta['__date_format'] : 'm/d/Y';
			$__pref_currency = isset($s2_user_meta['__pref_currency']) ? $s2_user_meta['__pref_currency'] : 'USD';

			$s2_user_meta_fields = array(
				'company_name'          => $company_name,
				'company_address'       => $company_address,
				'company_website'       => $company_website,
				'hardgood_multiple'     => $hardgood_multiple,
				'fresh_flower_multiple' => $fresh_flower_multiple,
				'__charge_card_rate'    => $__charge_card_rate,
				'__delivery_cost'       => $__delivery_cost,
				'__labor'               => $__labor,
				'__date_format'         => $__date_format,
				'__pref_currency'       => $__pref_currency
			);

			//Migrates the s2member values to an individual user meta
			foreach ($s2_user_meta_fields as $name => $value) {
				if (isset($value)) {
					update_user_meta($user->ID, $name, $value);
				}
			}

			//Deletes the old s2member meta fields
			delete_user_meta($user->data->ID, 'wp_s2member_custom_fields');
			print_r('<br>DONE!');
		} else {
			print_r('Already Migrated.');
		}

		print_r('<br/>==================================================<br/>');
	}

	print_r('<br/><br/>==================================================<br/>User Meta Migration END');
}
add_action( 'template_redirect', 'sc_migrate_user_meta' );

function sc_migrate_users_to_memberpress() {
	if ( ! isset( $_GET['migrate_users_to_memberpress'] ) || ! isset($_GET['membership_id']) || ! is_super_admin() ) {
		return;
	}

	print_r('User Memberpress Migration START<br>==================================================<br><br>');

	$args = array(
		'orderby' => 'name',
		'order'	  => 'ASC'
	);

	$users = get_users($args);

	foreach ($users as $user) {
		print_r('<br/>==================================================<br/>');
		print_r('Migrating: ');
		print_r('Name: ' . $user->data->display_name . ', Username: <strong>' . $user->data->user_login .'</strong>');
		print_r('<br/>');

		$membership = 'memberpress_product_authorized_' . $_GET['membership_id'];

		$usr = new MeprUser($user->ID);

    	$membership_ids = $usr->active_product_subscriptions();

		if(!empty($membership_ids)) {
			print_r('<strong>' . $user->user_login . '</strong> has a membership already');
			continue;
		}


		// Create a new transaction and set our new membership details
	    $txn = new MeprTransaction();
	    $txn->user_id = $user->ID;

	    // Get the membership in place
	    $txn->product_id = $_GET['membership_id'];
	    $prd = $txn->product();

	    // Set default price, adjust it later if coupon applies
	    $price = $prd->adjusted_price();

	    // Default coupon object
	    $cpn = (object)array('ID' => 0, 'post_title' => null);

	    // Adjust membership price from the coupon code
	    $txn->set_subtotal($price);

	    // Set the coupon id of the transaction
	    $txn->coupon_id = $cpn->ID;

	    $txn->status = MeprTransaction::$confirmed_str;

	    // Figure out the Payment Method
	    if(isset($_GET['mepr_payment_method']) && !empty($_GET['mepr_payment_method'])) {
	      $txn->gateway = $_GET['mepr_payment_method'];
	    } else {
	      $txn->gateway = MeprTransaction::$free_gateway_str;
	    }

	    // Let's checkout now
	    if($txn->gateway === MeprTransaction::$free_gateway_str) {
	      $signup_type = 'free';
	    }

	    $txn->store();

	    if(empty($txn->id)) {
	      // Don't want any loose ends here if the $txn didn't save for some reason
	      if($signup_type==='recurring' && ($sub instanceof MeprSubscription)) {
	        $sub->destroy();
	      }
	      print_r('Sorry, we were unable to create a transaction.', 'memberpress');
	    }

	    try {
	      if(('free' !== $signup_type) && ($pm instanceof MeprBaseRealGateway)) {
	        $pm->process_signup_form($txn);
	      }

	      SC_MEPR_Transaction::sc_create_free_transaction($txn);

	      print_r('<br>DONE!');
	    } catch(Exception $e) {
	      print_r($e->getMessage());
	    }

		print_r('<br/>==================================================<br/>');
	}

	print_r('<br/><br/>==================================================<br/>User Memberpress Migration END');
}
add_action( 'template_redirect', 'sc_migrate_users_to_memberpress' );

if ( class_exists('MeprTransaction') ) {
	class SC_MEPR_Transaction extends MeprTransaction {

		public static function sc_create_free_transaction($txn) {
		    $mepr_options = MeprOptions::fetch();
		    $mepr_blogname = get_option('blogname');

		    // Just short circuit if the transaction has already completed
		    if($txn->status == self::$complete_str)
		      return;

		    $product = new MeprProduct($txn->product_id);

		    //Expires at is now more difficult to calculate with our new membership terms
		    if($product->period_type != 'lifetime') { //A free recurring subscription? Nope - let's make it lifetime for free here folks
		      $expires_at = MeprUtils::mysql_lifetime();
		    }
		    else {
		      $product_expiration = $product->get_expires_at(strtotime($txn->created_at));

		      if(is_null($product_expiration))
		        $expires_at = MeprUtils::mysql_lifetime();
		      else
		        $expires_at = MeprUtils::ts_to_mysql_date($product_expiration, 'Y-m-d 23:59:59');
		    }

		    $txn->trans_num  = uniqid();
		    $txn->status     = self::$complete_str;
		    $txn->gateway    = self::$free_gateway_str;
		    $txn->expires_at = $expires_at;

		    // This will only work before maybe_cancel_old_sub is run
		    $upgrade = $txn->is_upgrade();
		    $downgrade = $txn->is_downgrade();

		    $txn->maybe_cancel_old_sub();
		    $txn->store();

		    // No such thing as a free subscription in MemberPress
		    // So let's clean up this mess right now
		    if(!empty($txn->subscription_id) && (int)$txn->subscription_id > 0) {
		      $sub = new MeprSubscription($txn->subscription_id);

		      $txn->subscription_id = 0;
		      $txn->store(); //Store txn here, otherwise it will get deleted during $sub->destroy()

		      $sub->destroy();
		    }

		    $free_gateway = new MeprBaseStaticGateway(self::$free_gateway_str, __('Free', 'memberpress'), __('Free', 'memberpress'));

		    if($upgrade) {
		      $free_gateway->upgraded_sub($txn);
		      $free_gateway->send_upgraded_txn_notices($txn);
		    }
		    elseif($downgrade) {
		      $free_gateway->downgraded_sub($txn);
		      $free_gateway->send_downgraded_txn_notices($txn);
		    }

		    $free_gateway->send_product_welcome_notices($txn);
		    $free_gateway->send_signup_notices($txn);
		    // $free_gateway->send_transaction_receipt_notices($txn); //Maybe don't need to send a receipt for a free txn

		  }
	}
}

// Encrypt Function
function sc_encrypted_string( $pure_string ) {
	$dirty = array( '+', '/', '=' );
	$clean = array( '_PLS_', '_SLSH_', '_EQLS_' );

	$encrypted_string = mcrypt_encrypt( MCRYPT_BLOWFISH, ENCRYPTION_KEY, utf8_encode( $pure_string ), MCRYPT_MODE_ECB, STEMCOUNTER_MCRYPT_IV );
	$encrypted_string = base64_encode( $encrypted_string );

	return str_replace( $dirty, $clean, $encrypted_string );
}

// Decrypt Function
function sc_decrypted_string( $encrypted_string ) {
	$dirty = array( '+', '/', '=' );
	$clean = array( '_PLS_', '_SLSH_', '_EQLS_' );

	$string = base64_decode( str_replace( $clean, $dirty, $encrypted_string ) );

	$decrypted_string = @mcrypt_decrypt( MCRYPT_BLOWFISH, ENCRYPTION_KEY, $string, MCRYPT_MODE_ECB, STEMCOUNTER_MCRYPT_IV );

	return $decrypted_string;
}

// Returns the permalink of the first page with the $template template, or false if not found.
function sc_get_permalink_by_template( $template ) {
	$page_id = sc_get_id_by_meta( '_wp_page_template', $template );

	if ( $page_id != false ) {
		return get_permalink( $page_id );
	}

	return false;
}

// Returns the id of the first page with the $template template, or false if not found.
function sc_get_id_by_template( $template ) {
	return sc_get_id_by_meta( '_wp_page_template', $template );
}

// Returns the id of the first page with the $template template, or false if not found.
function sc_get_id_by_meta( $key, $value, $meta_query = false, $pt = 'page', $compare = '=' ) {
	if ( empty( $meta_query ) ) {
		$meta_query = array(
			array(
				'key' => $key,
				'value' => $value,
				'compare' => $compare
			)
		);
	}

	$page = get_posts(
		array(
			'post_type' => $pt,
			'numberposts' => 1,
			'order_by' => 'menu_order',
			'order' => 'ASC',
			'fields' => 'ids',
			'meta_query' => $meta_query
		)
	);

	if ( $page && ! empty( $page ) ) {
		return $page[0];
	}

	return false;
}

function sc_photoswipe_html() {
	?>
	<div class="pswp" id="parent_pswp" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="pswp__bg"></div>
		<div class="pswp__scroll-wrap">
			<div class="pswp__container">
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
				<div class="pswp__item"></div>
			</div>
			<div class="pswp__ui pswp__ui--hidden">

				<div class="pswp__top-bar">
					<div class="pswp__counter"></div>

					<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

					<button class="pswp__button pswp__button--share" title="Share"></button>

					<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

					<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
					<div class="pswp__preloader">
						<div class="pswp__preloader__icn">
							<div class="pswp__preloader__cut">
								<div class="pswp__preloader__donut"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
				<div class="pswp__share-tooltip"></div>
				</div>

				<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
				</button>

				<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
				</button>

				<div class="pswp__caption">
					<div class="pswp__caption__center"></div>
				</div>

			</div>

		</div>
	</div>

	<?php
}
add_action( 'sc/app/footer/event_proposal_2_0', 'sc_photoswipe_html', 10 );

function sc_onboarding_tacking() {
	if ( is_page_template('template-onboarding.php') ) { ?>
		<script type="text/javascript">
			var capterra_vkey = '304b4593fba4e4ded7ac537144398133',
			capterra_vid = '2107169',
			capterra_prefix = (('https:' == document.location.protocol) ? 'https://ct.capterra.com' : 'http://ct.capterra.com');

			(function() {
				var ct = document.createElement('script'); ct.type = 'text/javascript'; ct.async = true;
				ct.src = capterra_prefix + '/capterra_tracker.js?vid=' + capterra_vid + '&vkey=' + capterra_vkey;
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s);
			})();
		</script>
	<?php }
}
add_action( 'wp_footer', 'sc_onboarding_tacking', 10 );

function sc_filter_body_class( $classes ) {
	if ( is_page_template( 'template-proposal-2.0.php' )
		|| is_page_template( 'template-recipe-sheet.php' )
		|| is_page_template( 'template-shopping.php' )
		|| is_page_template( 'template-event-breakdown.php' ) ) {
		$classes[] = 'stemcounter-new-design';
	}

	return $classes;

}
add_filter( 'body_class', 'sc_filter_body_class', 10 );

function sc_object_to_array( $obj ) {
	if ( is_object( $obj ) ) {
		$obj = (array) $obj;
	}

	if ( is_array( $obj ) ) {
		$new = array();
		foreach ( $obj as $key => $val ) {
			$new[ $key ] = sc_object_to_array( $val );
		}
	} else {
		$new = $obj;
	}

	return $new;
}

function sc_set_html_mail_content_type() {
    return 'text/html';
}

function sc_filter_main_menu_item_css_class( $classes, $item, $args ) {
	if ( ! isset( $args->theme_location ) || 'main-menu' != $args->theme_location ) {
		return $classes;
	}

	foreach ( $classes as $i => $class ) {
		if ( 0 === stripos( $class, 'fa' ) ) {
			unset( $classes[ $i ] );
		}
	}

	return $classes;
}
add_filter( 'nav_menu_css_class', 'sc_filter_main_menu_item_css_class', 10, 3 );

function sc_filter_menu_link_attributes( $atts, $item, $args ) {
	if ( ! isset( $args->theme_location ) || 'main-menu' != $args->theme_location ) {
		return $atts;
	}

	$atts['class'] = isset( $atts['class'] ) ? $atts['class'] : '';

	foreach ( $item->classes as $class ) {
		if ( 0 === stripos( $class, 'fa' ) ) {
			$atts['class'] .= ' ' . $class;
		}
	}

	return $atts;
}
add_filter( 'nav_menu_link_attributes', 'sc_filter_menu_link_attributes', 10, 3 );

function sc_five_min_crop_schedule( $schedules ){
	if( ! isset( $schedules['5min'] ) ) {
		$schedules['5min'] = array(
			'interval' => 5 * 60,
			'display' => __( 'Once every 5 minutes' )
		);
	}
	return $schedules;
}
add_filter('cron_schedules','sc_five_min_crop_schedule');

if( ! wp_get_schedule( 'sc_schedule_email_notifications' ) ) {
	add_action( 'init', 'sc_check_scheduled_emails' );
}

function sc_shopping_list_sort($a, $b) {
	//Sort the non-floral items by their type using demanded order
	$items_order = array('Flower', '', 'Hardgood', 'Rental', 'Base price', 'Fee');
	$a_pos = array_search($a["type"], $items_order, false);
	$b_pos = array_search($b["type"], $items_order, false);
	$a_pos = (false === $a_pos) ? 0 :  $a_pos;
	$b_pos = (false === $b_pos) ? 0 :  $b_pos;
	if ($a_pos < $b_pos) return -1;
	if ($a_pos > $b_pos) return 1;
	return strcasecmp( $a["name"], $b["name"] );
}

function sc_recipe_list_sort($a, $b) {
	//Sort items by their type using demanded order
	$items_order = array('fresh_flower', 'hardgood', '', 'rental', 'base_price', 'fee');
	$a_pos = array_search($a['item_pricing_category'], $items_order, false);
	$b_pos = array_search($b['item_pricing_category'], $items_order, false);
	$a_pos = (false === $a_pos) ? 0 :  $a_pos;
	$b_pos = (false === $b_pos) ? 0 :  $b_pos;
	if ($a_pos < $b_pos) return -1;
	if ($a_pos > $b_pos) return 1;
	$name_a = empty( $a['item'] ) ? $a['name'] : $a['item']['name'];
	$name_b = empty( $b['item'] ) ? $b['name'] : $b['item']['name'];
	return strcasecmp( $name_a.$a['item_variation_name'], $name_b.$a['item_variation_name'] );
}

function sc_get_user_js_date_format( $user_id = false, $date_format = false ) {
	$user_id = $user_id ? $user_id : get_current_user_id();

	if ( ! $date_format ) {
		$date_format = $js_date_format = get_user_meta( $user_id, '__date_format', true );
	} else {
		$js_date_format = $date_format;
	}

	if ( $date_format == 'd/m/Y' ) {
		$js_date_format = 'dd/mm/yy';
	} else if ( $date_format == 'm/d/Y' ) {
		$js_date_format = 'mm/dd/yy';
	} else if ( $date_format == 'd.m.Y' ) {
		$js_date_format = 'dd.mm.yy';
	} else if ( $date_format == 'm.d.Y' ) {
		$js_date_format = 'mm.dd.yy';
	}

	return $js_date_format;
}

function sc_is_free_trial_user( $user_id = false ) {
	if ( ! class_exists( 'MeprUser' ) ) {
		return false;
	}

	$user_id = $user_id ? $user_id : get_current_user_id();
	$usr = new MeprUser( $user_id );
	$is_free_trial = true;

	$membership_ids = $usr->active_product_subscriptions();
	foreach ($membership_ids as $_id) {
		$prod = new MeprProduct( $_id );
		if ( floatval( $prod->adjusted_price() ) != 0 )
			$is_free_trial = false;
			//echo '<!-- 99999999999'; /*var_dump( floatval( $prod->adjusted_price() );*/	echo '-->';
	}

	return $is_free_trial;
}

function sc_check_for_upgrade_menu( $items, $menu, $args ) {
	if ( is_admin() )
		return $items;

	foreach ( $items as $i => $item ) {
		if ( in_array( 'show-to-free-users', $item->classes ) ) {
			if ( ! is_user_logged_in() || ! sc_is_free_trial_user() ) {
				unset( $items[ $i ] );
			}
		}
	}
	return $items;
}
add_filter( 'wp_get_nav_menu_items', 'sc_check_for_upgrade_menu', 10, 3 );

/**
* Modify php mailer body with final email
*/
function sc_send_email( $phpmailer ) {
	$home_url = preg_quote( home_url( '/' ), '~' );
	if ( ! preg_match( '~<' . $home_url . '.*>~', $phpmailer->Body ) ) {
		$phpmailer->AltBody = preg_replace( '~<a[^href]*href=["\']([^"\']*)[\'"]([^>]+)?>(.*)<\/a>~', '$3($1)', $phpmailer->Body );
		$phpmailer->AltBody = strip_tags( $phpmailer->AltBody );
	}
}
add_action( 'phpmailer_init', 'sc_send_email' );

function sc_maybe_do_custom_redirects() {
	if ( ! is_404() ) {
		return;
	}

	$redirect_to = false;
	$add_get_params = true;
	$current_slug = get_query_var( 'name' );

	// Redirect the old proposal-2-0 URL to the new permalink of the page
	if ( 'proposal-2-0' == $current_slug ) {
		$redirect_to = sc_get_permalink_by_template( 'template-proposal-2.0.php' );
	} elseif ( 'venues' == $current_slug ) {
		$redirect_to = sc_get_permalink_by_template( 'template-vendors.php' );
	}

	if ( $redirect_to ) {
		if ( $add_get_params ) {
			$data = $_GET;
			unset( $data['q'] );
			$redirect_to = add_query_arg( $data, $redirect_to );
		}

		wp_redirect( $redirect_to, 301 );
		exit;
	}
}
add_action( 'template_redirect', 'sc_maybe_do_custom_redirects', 10 );

/**
* Change default 'WordPress' email name to 'StemCounter.com'
*/
function sc_filter_wp_mail_from_name( $from_name ){
	if ( 'WordPress' == $from_name ) {
		return 'StemCounter.com';
	} else {
		return $from_name;
	}
}
add_filter( 'wp_mail_from_name', 'sc_filter_wp_mail_from_name' );

function sc_prepare_email_recipients_for_mandrill( $to, $cc_email = false, $cc_name = false ) {
	if ( ! class_exists( 'wpMandrill' ) ) {
		return $to;
	}

	$to = array_map( 'trim', explode( ',', $to ) );

	$_to = array();

	foreach ( $to as $email ) {
		if ( ! $email ) {
			continue;
		}
		$_to[] = array(
			'email' => $email,
			'type'  => 'to',
		);
	}

	if ( $cc_email ) {
		$_to[] = array(
			'email' => $cc_email,
			'name'  => $cc_name,
			'type'  => 'cc',
		);
	}

	return $_to;
}

function sc_render_mixpanel_js() { ?>
	<!-- start Mixpanel -->
	<script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init(<?php echo json_encode( MIXPANEL_TOKEN ) ?>);
<?php if ( is_user_logged_in() ) :
	$current_user = wp_get_current_user();
	if ( ! get_user_meta( $current_user->ID, '_sc_mixpanel_aliased', true ) ) :
		update_user_meta( $current_user->ID, '_sc_mixpanel_aliased', 'yes' ); ?>
		mixpanel.alias( <?php echo json_encode( $current_user->user_email ); ?> );
	<?php else :
		$additionalInfo = maybe_unserialize( $current_user->additionalInfo );
		$additionalInfo = is_array( $additionalInfo ) ? $additionalInfo : array(); ?>
		mixpanel.people.set({
			"$first_name": <?php echo json_encode( $current_user->first_name ); ?>,
            "$last_name": <?php echo json_encode( $current_user->last_name ); ?>,
			"$email": <?php echo json_encode( $current_user->user_email ); ?>,
			"$created": <?php echo json_encode( $current_user->user_registered ); ?>,
			"$phone": <?php echo json_encode( ! empty( $additionalInfo['companyMobile'] ) ? $additionalInfo['companyMobile'] : '' ); ?>

		});
		mixpanel.identify( <?php echo json_encode( $current_user->user_email ); ?> );
	<?php endif; ?>
<?php endif; ?>
</script><!-- end Mixpanel -->
	<?php
}

function sc_render_drift_js() { ?>
<!-- Start of Async Drift Code -->
<script>
!function() {
	var t;
	if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error('Drift snippet included twice.')) : (t.invoked = !0,
	t.methods = [ 'identify', 'config', 'track', 'reset', 'debug', 'show', 'ping', 'page', 'hide', 'off', 'on' ],
	t.factory = function(e) {
	return function() {
		var n;
		return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
	};
	}, t.methods.forEach(function(e) {
	t[e] = t.factory(e);
	}), t.load = function(t) {
	var e, n, o, i;
	e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement('script'),
	o.type = 'text/javascript', o.async = !0, o.crossorigin = 'anonymous', o.src = 'https://js.driftt.com/include/' + i + '/' + t + '.js',
	n = document.getElementsByTagName('script')[0], n.parentNode.insertBefore(o, n);
	});
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('<?php echo DRIFT_TOKEN; ?>');
</script>
<script>
	<?php if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		$additionalInfo = maybe_unserialize( $current_user->additionalInfo );
		$additionalInfo = is_array( $additionalInfo ) ? $additionalInfo : array();
		$attributes = array(
            'email'			=> $current_user->user_email,
			'name'			=> $current_user->display_name,
			'phoneNumber'	=> ! empty( $additionalInfo['companyMobile'] ) ? $additionalInfo['companyMobile'] : '',
			"startDate"		=> $current_user->user_registered,
			); ?>
		drift.identify('<?php echo $current_user->ID; ?>', <?php echo json_encode( $attributes ); ?>);
	<?php } ?>
</script>
<!-- End of Async Drift Code -->
<?php
}

function sc_custom_library_role() {
	if ( null == get_role( 'librarian' ) ) {
		add_role( 'librarian', 'Librarian', array(
			'read'			=> true,
			'upload_files'	=> true,
			'edit_post'		=> false,
			'delete_posts'	=> false,
		) );
	}
}
add_action( 'init', 'sc_custom_library_role', 10 );


/**
 * Add new fields above 'Update' button.
 *
 * @param WP_User $user User object.
 */
function sc_additional_profile_fields( $user ) {
	$logo_url = get_user_meta( $user->ID, 'sc_user_library_logo', true );
	?>
	<table class="form-table">
		<tr>
			<th>
				<label for="library_logo"><?php _e( 'Library Logo URL' ); ?></label>
			</th>
			<td>
				<input type="text" name="sc_library_logo" id="sc_library_logo" value="<?php echo esc_attr( $logo_url ); ?>" class="regular-text" />
			</td>
		</tr>
	</table>
	<?php
}
add_action( 'show_user_profile', 'sc_additional_profile_fields' );
add_action( 'edit_user_profile', 'sc_additional_profile_fields' );

function sc_save_custom_user_profile_fields( $user_id ) {

	if ( ! current_user_can( 'edit_user', $user_id ) )
		return false;

	update_user_meta( $user_id, 'sc_user_library_logo', $_POST['sc_library_logo'] );
}
add_action( 'personal_options_update', 'sc_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'sc_save_custom_user_profile_fields' );

/**
 * Add the nl2br filter to only the password reset email
 *
 * @param  bool $nl2br
 * @param  array $message
 * @return bool
 */
function sc_fix_wpmandrill_pw_reset_link( $nl2br, $message ) {
	if ( in_array( 'wp_retrieve_password', $message['tags']['automatic'] ) ) {
		$nl2br = true;
	}
	return $nl2br;
}
add_filter( 'mandrill_nl2br', 'sc_fix_wpmandrill_pw_reset_link', 10, 2 );

function sc_render_facebook_pixel_js() { ?>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window,document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', <?php echo json_encode( FB_PIXEL_ID ) ?>);
	fbq('track', 'PageView');
	<?php if ( is_page() ) {
		$post = get_post();
		if ( ! is_user_logged_in() && false !== stripos( $post->post_content, '[stemcounter-registration' ) ) { ?>
			fbq('track', 'Lead');
		<?php
		} elseif ( is_user_logged_in() && ! get_user_meta( get_current_user_id(), '_sc_fb_pixel_complete_reg', true ) ) {
			update_user_meta( get_current_user_id(), '_sc_fb_pixel_complete_reg', 'yes' ); ?>
			fbq('track', 'CompleteRegistration');
		<?php
		}
	} ?>
	</script>
	<noscript>
	<img height="1" width="1" src="https://www.facebook.com/tr?id=<?php echo FB_PIXEL_ID; ?>&ev=PageView&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	<?php
}

function sc_render_openvoyce_js() { ?>
	<script src="//openvoyce.com/products/stemcounter/script.js"></script>
<?php }

function sc_override_membership_link( $link, $post ) {

	if ( 'memberpressproduct' != get_post_type( $post ) ) {
		return $link;
	}

	$mepr_access_url = sc_get_permalink_by_template('template-join.php');
	if ( $mepr_access_url ) {
		return add_query_arg('membership_id', $post->ID, $mepr_access_url);
	} else {
		return $link;
	}

}
add_filter( 'post_type_link', 'sc_override_membership_link', 100, 2 );

function sc_maybe_redirect_memberpress_posts() {
	if ( ! is_singular( 'memberpressproduct' ) ) {
		return;
	}

	$mepr_access_url = sc_get_permalink_by_template( 'template-join.php' );
	if ( $mepr_access_url ) {
		wp_redirect( add_query_arg( 'membership_id', $post->ID, $mepr_access_url ) );
		exit;
	}
}
add_action( 'template_redirect', 'sc_maybe_redirect_memberpress_posts', 10 );

function sc_get_header_og_image() { ?>
	<meta name="og:image" content="<?php bloginfo( 'template_directory' ); ?>/img/fbimage.png" />
	<meta name="og:image:width" content="1050" />
	<meta name="og:image:height" content="700" />
<?php
}
add_action( 'wp_head', 'sc_get_header_og_image', 0 );

function sc_hex_to_rgb( $hex ) {
	$hex = str_replace( '#', '', $hex );
	if ( strlen( $hex ) == 3 ) {
		$hex = str_repeat( substr( $hex, 0, 1 ), 2 ) . str_repeat( substr( $hex, 1, 1 ), 2 ) . str_repeat( substr( $hex, 2, 1 ), 2 );
	}
	$rgb = hexdec( $hex );

	return array(
		( $rgb >> 16 ) & 0xff, // red
		( $rgb >> 8 ) & 0xff, // green
		( $rgb >> 0 ) & 0xff, // blue
	);
}

function sc_adjust_brightness( $hex, $steps ) {
	$steps = max( -255, min( 255, $steps ) );
	$color_parts = sc_hex_to_rgb( $hex );
	$return = '#';

	foreach ( $color_parts as $color ) {
		$color   = max( 0, min( 255, $color + $steps ) ); // Adjust color
		$return .= str_pad( dechex( $color ), 2, '0', STR_PAD_LEFT ); // Make two char hex code
	}

	return $return;
}

function sc_get_hex_luma( $hex ) {
	$rgb = sc_hex_to_rgb( $hex );

	return 0.2126 * $rgb[0] + 0.7152 * $rgb[1] + 0.0722 * $rgb[2]; // per ITU-R BT.709
}

function sc_get_google_font_inline_style( $font_family ) {
	return
		".event-new-design h1,
		.event-new-design h2,
		.event-new-design h3,
		.event-new-design h4,
		.event-new-design h5,
		.event-new-design h6,
		.event-new-design p,
		body.proposal-style-2,
		.new-proposal-container {
			font-family: $font_family;
		}";
}

function sc_ajax_get_image_upload_nonce( $action ) {
	return wp_create_nonce( 'sc_image_upload_' . $action );
}

function sc_ajax_handle_image_upload() {
	sc_clean_input_slashes();
	$request = Request::capture();
	$r = new Ajax_Response();

	if ( ! wp_verify_nonce( $request->input( 'upload_nonce' ), 'sc_image_upload_' . $request->input( 'upload_action' ) ) ) {
		$r->fail('There was a problem uploading your image.');
	}

	$attachment_id = media_handle_upload( 'file', 0 );
	if ( is_wp_error( $attachment_id ) ) {
		$r->fail('There was a problem uploading your logo. Please try a smaller file.' . $attachment_id->get_error_message());
	}

	$image = wp_get_attachment_image_src( $attachment_id, $request->input( 'image_size' ) );

	$r->add_payload( 'image', $image[0] );
	$r->add_payload( 'image_id', $attachment_id );
	$r->respond();
}
add_action( 'wp_ajax_sc_upload_image', 'sc_ajax_handle_image_upload' );

function price_format( $number, $decimals = 2 ) {
	return floatval(number_format($number, $decimals, '.', ''));
}

function sc_get_ip_address() {
	if ( isset( $_SERVER['X-Real-IP'] ) ) {
		return $_SERVER['X-Real-IP'];
	} elseif ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		// Proxy servers can send through this header like this: X-Forwarded-For: client1, proxy1, proxy2
		// Make sure we always only send through the first IP in the list which should always be the client IP.
		return trim( current( explode( ',', $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) );
	} elseif ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
		return $_SERVER['REMOTE_ADDR'];
	}

	return '';
}

function sc_get_user_datetime_format( $user_id = false ) {
	static $formats = array();

	$user_id = $user_id ? $user_id : get_current_user_id();

	if ( ! isset( $formats[ $user_id ] ) ) {
		$format = get_user_meta( $user_id, '__date_format', true );
		$format = $format ? $format : 'm/d/Y';
		$format .= ' h:ia';

		$formats[ $user_id ] = $format;
	}

	return $formats[ $user_id ];
}

function sc_setup_affiliate( $user_id ) {
	global $wpdb;

	if ( ! function_exists( 'affiliate_wp' ) ) {
		return new WP_Error( 'no_affwp', 'Affiliate WP is not active' );
	}

	$affiliate_id = affiliate_wp()->affiliates->get_by( 'user_id', $user_id );
	if ( ! $affiliate_id ) {
		$data = array(
			'status'	=> 'active',
			'user_id'	=> $user_id,
			'notes'		=> 'Added automatically',
		);

		$affiliate_id = affiliate_wp()->affiliates->add( $data );

		if ( $affiliate_id ) {
			affwp_set_affiliate_status( $affiliate_id, $data['status'] );
		} else {
			return new WP_Error( 'cant_create_affiliate', 'Failed to create an affiliate' );
		}
	} else {
		$affiliate_id = $affiliate_id->affiliate_id;
	}

	$existing_coupon = sc_get_affiliate_coupon( $user_id, $affiliate_id );

	// An auto-generated coupon for this user already exists, so we have nothing else to do
	if ( $existing_coupon ) {
		return $affiliate_id;
	}

	$data = array(
		'post_type'		=> MeprCoupon::$cpt,
		'post_title'	=> MeprUtils::random_string( 10, false, true ),
		'post_content'	=> '',
		'post_author'	=> 9,
		'post_status'	=> 'publish',
	);

	// Begin duplicate titles handling - taken from MeprCouponsCtrl::sanitize_coupon_title()
	// As the original function only runs when is_admin() is true and we need it to run regardless
	// of context
	$q1 = "SELECT ID FROM {$wpdb->posts} WHERE post_title = %s AND post_type = %s LIMIT 1";
	$q2 = $wpdb->prepare( $q1, $data['post_title'], MeprCoupon::$cpt );
	$count = 0;

	while ( ( $id = $wpdb->get_var( $q2 ) ) ) {
		++ $count; //Want to increment before running the query, so when we exit the loop $data['post_title'] . "-{$count}" is stil valid
		$q2 = $wpdb->prepare( $q1, $data['post_title'] . "-{$count}", MeprCoupon::$cpt );
	}

	if ( $count > 0 ) {
		$data['post_title'] .= "-{$count}";
	}

	$coupon_id = wp_insert_post( $data );
	if ( ! $coupon_id ) {
		return new WP_Error( 'cant_create_coupon', 'Failed to create an coupon' );
	}

	update_post_meta( $coupon_id, 'affwp_discount_affiliate', $affiliate_id );
	update_post_meta( $coupon_id, '_sc_is_auto_generated_coupon', 'yes' );

	$coupon = new MeprCoupon( $coupon_id );

	$coupon->should_expire = false;
	$coupon->expires_on = 0;
	$coupon->usage_amount = 0;

	$coupon->discount_type = 'percent';
	$coupon->discount_amount = 0;
	$coupon->valid_products = array();
	$coupon->trial = true;
	$coupon->trial_days = 30;
	$coupon->trial_amount = 0.00;
	$coupon->store_meta();

	MeprHooks::do_action( 'mepr-coupon-save-meta', $coupon );

	return $affiliate_id;
}

/*
 * @return bool|MeprCoupon
 */
function sc_get_affiliate_coupon( $user_id, $affiliate_id = false ) {
	if ( ! function_exists( 'affiliate_wp' ) ) {
		return false;
	}

	if ( ! $affiliate_id ) {
		$affiliate_id = affiliate_wp()->affiliates->get_by( 'user_id', $user_id );
		$affiliate_id = $affiliate_id ? $affiliate_id->affiliate_id : false;
	}

	if ( ! $affiliate_id ) {
		return false;
	}

	$existing_coupon = get_posts( array(
		'post_type' => MeprCoupon::$cpt,
		'numberposts' => 1,
		'fields' => 'ids',
		'meta_query' => array(
			array(
				'key' => 'affwp_discount_affiliate',
				'value' => $affiliate_id,
				'compare' => '=',
			),
		),
	) );

	if ( $existing_coupon ) {
		return new MeprCoupon( array_shift( $existing_coupon ) );
	} else {
		return false;
	}
}

function sc_maybe_get_coupon_code_from_referral() {
	if ( ! function_exists( 'affiliate_wp' ) ) {
		return;
	}

	$affiliate_id = affiliate_wp()->tracking->get_affiliate_id();
	if ( $affiliate_id ) {
		$coupon = sc_get_affiliate_coupon( false, $affiliate_id );
		if ( $coupon ) {
			return $coupon->post_title;
		}
	}

	return false;
}

function sc_maybe_filter_coupon_applies_on_memberships( $metadata, $post_id, $meta_key, $single ) {
	if ( '' == $meta_key && MeprCoupon::$cpt == get_post_type( $post_id ) ) {
		$metadata = wp_cache_get( $post_id, 'post_meta' );

		if ( ! $metadata ) {
			$metadata = update_meta_cache( 'post', array( $post_id ) );
			$metadata = $metadata[ $post_id ];
		}

		$metadata[ MeprCoupon::$valid_products_str ] = array( sc_get_auto_generated_coupon_applies_to() );
	} elseif ( MeprCoupon::$valid_products_str == $meta_key && get_post_meta( $post_id, '_sc_is_auto_generated_coupon', true ) ) {
		$metadata = array( sc_get_auto_generated_coupon_applies_to() );
	}

	return $metadata;
}
if ( class_exists( 'MeprCoupon' ) ) {
	add_filter( 'get_post_metadata', 'sc_maybe_filter_coupon_applies_on_memberships', 10, 4 );
}

function sc_get_auto_generated_coupon_applies_to() {
	return array( 10089, 23812, 385, 33160 );
}

function dfkdflksdlkfnklfslfndfksn() {
	// var_dump( sc_setup_affiliate( 302 ) );
}
add_action( 'template_redirect', 'dfkdflksdlkfnklfslfndfksn', 10 );

function sc_start_transaction() {
    global $wpdb;

    $wpdb->query( "START TRANSACTION" );
}

function sc_commit_transaction() {
    global $wpdb;

    $wpdb->query( "COMMIT" );
}

function sc_rollback_transaction() {
    global $wpdb;

    $wpdb->query( "ROLLBACK" );
}


function wp_render_rotate_photo_script() {
	?>
	<script type="text/html" id="tmpl-rotate-photos">
	<# if ( 'image' === data.type && data.sizes ) {  #>
		<div class="rotate-photo sc-loader-container">

			<div class="sc-loader-wrap " <# if ( data.sc_saving ) { #>style="display: block"<# } #>>
				<div class="spinner-wrap">
					<i class="fa fa-spinner fa-pulse fa-3x fa-fw sc-primary-color"></i>
					<span class="sr-only">Loading...</span>
				</div>
			</div>

			<h2>
				<?php _e( 'Rotate photo' ); ?>
				<span class="settings-save-status">
					<span class="spinner"></span>
					<span class="saved"><?php esc_html_e('Saved.'); ?></span>
				</span>
			</h2>
			<div class="rotate-preview">
				<div class="rotate-thumbnail">
					<# if ( data.uploading ) { #>
						<div class="media-progress-bar"><div></div></div>
					<# } else { #>
						<img src="{{ data.size.url }}" draggable="false" alt="" style="transform: rotate({{ data.rotation }}deg)" />
					<# } #>
				</div>
			</div>

			<div class="rotate-controls">
				<button class="button image-rotate-left" title="<?php _e('Rotate left'); ?>"><i class="dashicons dashicons-image-rotate-left" /></button>
				<button class="button image-rotate-right" title="<?php _e('Rotate right'); ?>"><i class="dashicons dashicons-image-rotate-right" /></button>
				<button class="button media-button button-primary rotate-save" title="<?php _e('Save rotated photo'); ?>"><?php _e('Save'); ?></button>
			</div>
		</div>

	<# } #>
	</script>
<?php
}

function wp_add_rotate_photo() {
	if ( is_admin() ) {
		return;
	} else {
		add_action( 'wp_footer', 'wp_render_rotate_photo_script' );
	}
}
add_action( 'wp_enqueue_media', 'wp_add_rotate_photo' );


function sc_rotate_attachment() {
	if ( isset( $_REQUEST['changes']['rotation'] ) ) {

		if ( ! isset( $_REQUEST['id'] ) || ! isset( $_REQUEST['changes'] ) )
			wp_send_json_error();

		if ( ! $id = absint( $_REQUEST['id'] ) )
			wp_send_json_error();

		if ( ! $rotation= absint( $_REQUEST['changes']['rotation'] ) )
			wp_send_json_error();

		$file = get_attached_file( $id );
		$img = wp_get_image_editor( $file );

		if ( ! is_wp_error( $img ) ) {
			$img->rotate( $rotation );
			$saved = $img->save( $file );

			if ( wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $file ) ) )
				wp_send_json_success();
			else
				wp_send_json_error();
		}
	}

}
add_action( 'wp_ajax_save-attachment', 'sc_rotate_attachment', 0 );
